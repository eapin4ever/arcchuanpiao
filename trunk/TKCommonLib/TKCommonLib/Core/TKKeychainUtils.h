
#import <Foundation/Foundation.h>

extern NSString* const userKeyChainIdentifier;
extern NSString* const uservoIdentifier;
extern NSString* const alertNewTips;
extern NSString* Userid;

@interface TKKeychainUtils : NSObject {
}

+ (BOOL)createKeychainValueForUserid:(NSString *)userid 
                             withToken:(NSString *)token 
                           withName:(NSString *)name 
                         withAccountType:(NSUInteger)accountType
                          withAvtarURL:(NSString *)mobile
                         forIdentifier:(NSString *)identifier;

+ (void)deleteKeychainValue:(NSString *)identifier;

+ (NSString *)getToken:(NSString *)identifier;
+ (NSString *)getUserid:(NSString *)identifier;
+ (NSString *)getUsername:(NSString *)identifier;
+ (NSUInteger)getAccountType:(NSString *)identifier;
+ (NSString *)getAvtarURL:(NSString *)identifier;
//+ (NSDictionary *)getUserAcounts:(NSString *)identifier;
+ (void) setUserid:(NSString *)identifier;
+ (NSString *)getUserid;

+ (BOOL)updateUsername:(NSString *)name identifier:(NSString *)identifier;
@end
