
#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface TKSQLite : NSObject {
    NSInteger busyRetryTimeout;
    NSString *dbPath;
    sqlite3 *db;
}

@property (readwrite) NSInteger busyRetryTimeout;
@property (readonly) NSString *dbPath;

- (id)initWithDBPath:(NSString *)aDBPath;

- (BOOL)open:(NSString *)aDBPath;
- (void)close;

/*error info*/
- (NSString*)lastErrorMessage;
- (NSInteger)lastErrorCode;

/*query, select data*/
- (NSArray *)executeQuery:(NSString *)sql, ...;
- (NSArray *)executeQuery:(NSString *)sql arguments:(NSArray *)args;

/*update methos, update/insert/delete data*/
- (BOOL)executeUpdate:(NSString *)sql, ...;
- (BOOL)executeUpdate:(NSString *)sql arguments:(NSArray *)args;

/*transaction*/
- (BOOL)commit;
- (BOOL)rollback;
- (BOOL)beginTransaction;
- (BOOL)beginDeferredTransaction;

/*helper methods*/
- (BOOL)tableExists:(NSString*)tableName;
- (BOOL)indexExists:(NSString*)indexName;

@end
