

//NSString *const URL_SERVER_PATH_IMG = @"http://img.here.cn/";
//NSString *const URL_SERVER_PATH_USERPHOTO = @"face/";
//NSString *const URL_SERVER_PATH_UPLOAD_FACEICON = @"setface/";
//NSString *const URL_LOCAL_PATH_USERPHOTO = @"face/";
//
//NSString *const URL_SERVER_PATH_USER_COVER = @"setbackground/";
//NSString *const URL_SERVER_PATH_FORMPOSTAPI = @"upload/";
//NSString *const URL_SERVER_PATH_FORMPOSTAPI_V2 = @"hereupload/";
//NSString *const URL_SERVER_PATH_UPLOAD_CAT = @"uploadcat/";



// No-ops for non-retaining objects.
static const void* TTRetainNoOp(CFAllocatorRef allocator, const void *value) { return value; }
static void TTReleaseNoOp(CFAllocatorRef allocator, const void *value) { }


///////////////////////////////////////////////////////////////////////////////////////////////////
NSMutableArray* TTCreateNonRetainingArray() {
    CFArrayCallBacks callbacks = kCFTypeArrayCallBacks;
    callbacks.retain = TTRetainNoOp;
    callbacks.release = TTReleaseNoOp;
    return (NSMutableArray*)CFArrayCreateMutable(nil, 0, &callbacks);
}


///////////////////////////////////////////////////////////////////////////////////////////////////
NSMutableDictionary* TTCreateNonRetainingDictionary() {
    CFDictionaryKeyCallBacks keyCallbacks = kCFTypeDictionaryKeyCallBacks;
    CFDictionaryValueCallBacks callbacks = kCFTypeDictionaryValueCallBacks;
    callbacks.retain = TTRetainNoOp;
    callbacks.release = TTReleaseNoOp;
    return (NSMutableDictionary*)CFDictionaryCreateMutable(nil, 0, &keyCallbacks, &callbacks);
}