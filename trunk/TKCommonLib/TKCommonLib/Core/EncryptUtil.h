//
//  EncryptUtil.h
//  des
//
//  Created by lizhenqu on 12-12-28.
//  Copyright (c) 2012年 lizhenqu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCryptor.h>

@interface EncryptUtil : NSObject

+ (NSString *) encryptUseDES:(NSString *)plainText key:(NSString *)key;
+ (NSString *) decryptUseDES:(NSString *)cipherText key:(NSString*)key;

@end

@interface NSData (DES)

- (NSString *)base64Encoding;
+ (id)dataWithBase64EncodedString:(NSString *)string;

@end