//
//  TKCPUOperation.h
//  TKCommonLib
//
//  Created by luobin on 4/27/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TKCPUOperation : NSOperation

@property (copy,   readonly ) NSError *error;

@end
