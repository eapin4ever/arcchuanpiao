//
//  TKCPUOperation.m
//  TKCommonLib
//
//  Created by luobin on 4/27/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "TKCPUOperation.h"

@interface TKCPUOperation()

// read/write variants of public properties

@property (copy,   readwrite) NSError *error;

@end

@implementation TKCPUOperation

@synthesize error;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

@end
