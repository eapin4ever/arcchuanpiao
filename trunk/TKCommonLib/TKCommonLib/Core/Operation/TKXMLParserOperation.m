



#import "TKXMLParserOperation.h"

#include <xlocale.h>                                    // for strptime_l

@interface TKXMLParserOperation () <NSXMLParserDelegate>

// read/write variants of public properties

@property (copy,   readwrite) NSError *                 error;

// private properties

#if ! defined(NDEBUG)
@property (assign, readwrite) NSTimeInterval            debugDelaySoFar;
#endif

@property (retain, readonly ) NSMutableArray *          mutableResults;
@property (retain, readwrite) NSXMLParser *             parser;
@property (retain, readonly ) NSMutableDictionary *     itemProperties;

@end

@implementation TKXMLParserOperation

- (id)initWithData:(NSData *)data
    // See comment in header.
{
    assert(data != nil);
    self = [super init];
    if (self != nil) {
        self->_data = [data copy];
        self->_mutableResults  = [[NSMutableArray alloc] init];
        assert(self->_mutableResults != nil);
        self->_itemProperties = [[NSMutableDictionary alloc] init];
        assert(self->_itemProperties != nil);
    }
    return self;
}

- (void)dealloc
{
    [self->_data release];
    [self->_error release];
    [self->_parser release];
    [self->_mutableResults release];
    [self->_itemProperties release];
    [super dealloc];
}

#if ! defined(NDEBUG)
@synthesize debugDelay      = _debugDelay;
@synthesize debugDelaySoFar = _debugDelaySoFar;
#endif

@synthesize data            = _data;
@synthesize error           = _error;

@synthesize mutableResults  = _mutableResults;
@synthesize parser          = _parser;
@synthesize itemProperties  = _itemProperties;

+ (NSDate *)dateFromDateString:(NSString *)string
    // Parses the supplied XML date string and returns an NSDate object. 
    // We avoid NSDateFormatter here and do the work using the much lighter 
    // weight strptime_l.
{
/*
    Dates are of the form "2006-07-30T07:47:17Z".
*/
    struct tm   now;
    NSDate *    result;
    BOOL        success;
    
    result = nil;
    success = strptime_l([string UTF8String], "%Y-%m-%dT%H:%M:%SZ", &now, NULL) != NULL;
    if (success) {
        result = [NSDate dateWithTimeIntervalSince1970:timelocal(&now)];
    }
    
    return result;
}

- (NSArray *)results
    // Returns a copy of the current results.
{
    return [[self->_mutableResults copy] autorelease];
}

- (void)main
{
    BOOL        success;
    
    // Set up the parser.  We keep this in a property so that our delegate callbacks 
    // have access to it.
    
    assert(self.data != nil);
    self.parser = [[[NSXMLParser alloc] initWithData:self.data] autorelease];
    assert(self.parser != nil);
    
    self.parser.delegate = self;
    
    // Do the parse.

    TKDINFO(@"xml parse start");
    
    success = [self.parser parse];
    if ( ! success ) {
    
        // If our parser delegate callbacks already set an error, we ignore the error 
        // coming back from NSXMLParser.  Our delegate callbacks have the most accurate 
        // error info.
    
        if (self.error == nil) {
            self.error = [self.parser parserError];
            assert(self.error != nil);
        }
    }
    
    // In the debug version, if we've been told to delay, do so.  This gives 
    // us time to test the cancellation path.
    
    #if ! defined(NDEBUG)
        {
            while (self.debugDelaySoFar < self.debugDelay) {
                // We always sleep in one second intervals.  I could do the maths to 
                // sleep for the remaining amount of time or one second, whichever 
                // is the least, but hey, this is debugging code.
                
                [NSThread sleepForTimeInterval:1.0];
                self.debugDelaySoFar += 1.0;
                
                if ( [self isCancelled] ) {
                    // If we notice the cancel, we override any error we got from the XML.
                    self.error = [NSError errorWithDomain:NSCocoaErrorDomain code:NSUserCancelledError userInfo:nil];
                    break;
                }
            }
        }
    #endif

    if (self.error == nil) {
        TKDINFO(@"xml parse success");
    } else {
        TKDERROR(@"xml parse failed %@", self.error);
    }
    
    self.parser = nil;
}

/*
    Here's an example of a "photo" element in our XML:
    
    <photo name="Kids In A Box" date="2006-07-30T07:47:17Z" id="12345">
        <image kind="original" src="originals/IMG_1282.JPG" srcURL="originals/IMG_1282.JPG" srcname="IMG_1282.JPG" size="1241626" sizeText="1.2 MB" type="image"></image>
        <image kind="image" src="images/IMG_1282.JPG" srcURL="images/IMG_1282.JPG" srcname="IMG_1282.JPG" size="1129805" sizeText="1 MB" type="image" width="2048" height="1536"></image>
        <image kind="thumbnail" src="thumbnails/IMG_1282.jpg" srcURL="thumbnails/IMG_1282.jpg" srcname="IMG_1282.jpg" size="29295" sizeText="28.6 KB" type="image" width="300" height="225"></image>
    </photo>
*/

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    assert(parser == self.parser);
    #pragma unused(parser)
    #pragma unused(namespaceURI)
    #pragma unused(qName)
    #pragma unused(attributeDict)
    
    // In the debug build, if we've been told to delay, and we haven't already delayed 
    // enough, just sleep for 0.1 seconds.
    
    #if ! defined(NDEBUG)
        if (self.debugDelaySoFar < self.debugDelay) {
            [NSThread sleepForTimeInterval:0.1];
            self.debugDelaySoFar += 0.1;
        }
    #endif
    
    // Check for cancellation at the start of each element.
    
    if ( [self isCancelled] ) {
        self.error = [NSError errorWithDomain:NSCocoaErrorDomain code:NSUserCancelledError userInfo:nil];
        [self.parser abortParsing];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    assert(parser == self.parser);
    #pragma unused(parser)
    #pragma unused(namespaceURI)
    #pragma unused(qName)
    
    // At the end of the "photo" element, check to see we got all of the required 
    // properties and, if so, add an item to the result.
}

@end