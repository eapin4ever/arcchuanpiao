

/*
 *
 *  数据缓存 适合快速存储少量数据，数据必须符合Json规范
 *
 */
@interface TKDataCache : NSObject {
}

+ (id)getCacheDataByURLKey:(NSString *)key;

+ (void)setCacheData:(id)data andURLKey:(NSString *)key;

+ (void)removeCacheDataByURLKey:(NSString *)key;

@end