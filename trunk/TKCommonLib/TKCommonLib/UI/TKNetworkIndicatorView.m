//
//  ZSTNetworkStatusView.m
//  ECA
//
//  Created by luobin on 12-10-10.
//
//

#import "TKNetworkIndicatorView.h"
#import <objc/runtime.h>

static char networkIndicatorViewKey;

TK_FIX_CATEGORY_BUG(ZSTNetworkIndicatorView)

@implementation UIView (ZSTNetworkIndicatorView)

- (TKNetworkIndicatorView *)newNetworkIndicatorViewWithRefreshLoadBlock:(TKRefreshLoadBlock)refreshLoadBlock {
    [self.currentNetworkIndicatorView removeView];
    TKNetworkIndicatorView *networkIndicatorView = [TKNetworkIndicatorView newNetworkIndicatorViewForView:self refreshLoadBlock:refreshLoadBlock];
    objc_setAssociatedObject(self, &networkIndicatorViewKey, networkIndicatorView, OBJC_ASSOCIATION_RETAIN);
    return networkIndicatorView;
}

- (TKNetworkIndicatorView *)currentNetworkIndicatorView
{
    return objc_getAssociatedObject(self, &networkIndicatorViewKey);
}

- (void)refreshFailed
{
    [self.currentNetworkIndicatorView refreshFailed];
}

- (void)removeNetworkIndicatorView
{
    [self.currentNetworkIndicatorView removeView];
    objc_setAssociatedObject(self, &networkIndicatorViewKey, nil, OBJC_ASSOCIATION_RETAIN);
}

@end

@implementation TKNetworkIndicatorView
@synthesize indicatorView;
@synthesize refreshLabel;
@synthesize loadingLabel;
@synthesize refreshLoadBlock;

- (id)initForView:(UIView *)addToView
{
    self = [super initWithFrame:addToView.bounds];
    if (self) {
        // Initialization code
        [self addTarget:self action:@selector(refreshLoadData) forControlEvents:UIControlEventTouchUpInside];
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        self.backgroundColor = RGBACOLOR(239, 239, 239, 1);
        
        self.indicatorView = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
        self.indicatorView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:self.indicatorView];
        
        self.loadingLabel = [[[UILabel alloc] initWithFrame:CGRectMake(60, 205, 60, 20)] autorelease];
        loadingLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        loadingLabel.font = [UIFont systemFontOfSize:14];
        loadingLabel.text = NSLocalizedString(@"加载中...", nil);
        loadingLabel.textAlignment = NSTextAlignmentCenter;
        loadingLabel.textColor = RGBACOLOR(50.0, 50.0, 50.0, 1);
		loadingLabel.shadowColor = [UIColor whiteColor];
		loadingLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
        loadingLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:loadingLabel];
        
        self.refreshLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 30)] autorelease];
        self.refreshLabel.hidden = YES;
        refreshLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
        refreshLabel.font = [UIFont boldSystemFontOfSize:16];
        refreshLabel.text = NSLocalizedString(@"加载失败，点击重新加载", nil);
        refreshLabel.textAlignment = NSTextAlignmentCenter;
        refreshLabel.textColor = RGBACOLOR(200, 200, 200, 1);
        refreshLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:refreshLabel];
        
        [addToView addSubview:self];
    }
    return self;
}

+ (TKNetworkIndicatorView *)newNetworkIndicatorViewForView:(UIView *)addToView refreshLoadBlock:(TKRefreshLoadBlock)refreshLoadBlock
{
    TKNetworkIndicatorView *networkIndicatorView2 = [[[TKNetworkIndicatorView alloc] initForView:addToView] autorelease];
    networkIndicatorView2.refreshLoadBlock = refreshLoadBlock;
    [networkIndicatorView2 refreshLoadData];
    return networkIndicatorView2;
}

- (void)refreshFailed
{
    [self.indicatorView stopAnimating];
    self.loadingLabel.hidden = YES;
    self.refreshLabel.hidden = NO;
}

- (void)removeView
{
    if (!self.superview)
        return;
    
    [self removeFromSuperview];
}

- (void)refreshLoadData
{
    if (![self.indicatorView isAnimating]) {
        [self startRefreshLoad];
        [self performSelector:@selector(invokeRefreshLoadBlock) withObject:self afterDelay:0];
    }
}

- (void)invokeRefreshLoadBlock
{
    if (self.refreshLoadBlock) {
        self.refreshLoadBlock(self);
    }
}

- (void)dealloc
{
    self.refreshLoadBlock = nil;
    self.indicatorView = nil;
    self.loadingLabel = nil;
    self.refreshLabel = nil;
    [super dealloc];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.indicatorView.center = CGPointMake(self.width/2 - 20, self.height/2);
    self.loadingLabel.center = CGPointMake(self.width/2 + 20, self.height/2);
    self.refreshLabel.center = CGPointMake(self.width/2, self.height/2);
}

- (void)startRefreshLoad
{
    [self.indicatorView startAnimating];
    self.loadingLabel.hidden = NO;
    self.refreshLabel.hidden = YES;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
