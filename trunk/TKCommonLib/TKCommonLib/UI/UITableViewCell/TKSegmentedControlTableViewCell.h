


#import <UIKit/UIKit.h>
#import "TKGridTableViewCell.h"

/** `TKSegmentedControlTableViewCell` is a `TKGridTableViewCell` that
 behaves like a UISegmentedControl.

 ### Usage
 
 Just change the NSArray `titles` property, and the cell will be automatically 
 adapted to the array.
 
 You can manually select a given index by changing the `selectedIndex` property.
 
 ### Callback
 
 If you want to receive a notification when an element is selected, change the
 `actionBlock` property, inherited from `TKGridTableViewCell`.
 
 ### Example 
 
 ![](../docs/Screenshots/segmented_control.png)
 
    segmentedCell.titles = [NSArray arrayWithObjects:@"1", @"2", @"3", @"4", nil];
 
 
 */

@interface TKSegmentedControlTableViewCell : TKGridTableViewCell


@property (nonatomic, retain) NSArray *titles;
@property (nonatomic, assign) NSInteger selectedIndex;

@end
