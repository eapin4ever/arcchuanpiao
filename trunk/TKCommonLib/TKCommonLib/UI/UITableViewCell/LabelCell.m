// Copyright 2011 Cooliris, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#import "LabelCell.h"

#define kMargin 10.0
#define kOffset 0

@implementation LabelCell

@synthesize label=_label, labelWidth=_labelWidth;

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        _label = [[UILabel alloc] init];
        _label.adjustsFontSizeToFitWidth = YES;
        [self.contentView addSubview:_label];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void) dealloc {
    [_label release];
    
    [super dealloc];
}

- (void) layoutSubviews {
    [super layoutSubviews];
    
    CGRect contentBounds = self.contentView.bounds;
    
    CGRect labelFrame = self.textLabel.frame;
    if (_labelWidth > 0.0) {
        labelFrame.size.width = _labelWidth;
    } else {
        CGSize size = [self.textLabel sizeThatFits:contentBounds.size];
        labelFrame.size.width = size.width;
    }
    self.textLabel.frame = labelFrame;
    
    CGRect fieldFrame;
    fieldFrame.origin.x = labelFrame.origin.x + labelFrame.size.width + kMargin - 25;
    fieldFrame.origin.y = kOffset;
    fieldFrame.size.width = contentBounds.size.width - fieldFrame.origin.x - kMargin;
    fieldFrame.size.height = contentBounds.size.height - kOffset;
    _label.frame = fieldFrame;
}

- (void) setLabelWidth:(CGFloat)width {
    _labelWidth = width;
    [self setNeedsLayout];
}

@end
