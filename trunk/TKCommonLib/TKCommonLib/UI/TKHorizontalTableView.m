

//
//  TKHorizontalTableView.m
//  F3
//  
//  Created by luobin on 12-38.
//  Copyright (c) __MyCompanyName__. All rights reserved.
//


#import <QuartzCore/QuartzCore.h>
#import "TKHorizontalTableView.h"

#define kTKCellDefaultWidth 320

@implementation TKHorizontalTableViewCell

@end

@implementation TKHorizontalTableView
@synthesize containerView;
@synthesize dataSource;
@synthesize numberOfCells;
@synthesize delegate;
@synthesize headerView;
@synthesize footerView;

- (void)initData
{
    self.autoresizesSubviews = YES;
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
	self.showsHorizontalScrollIndicator = YES;
	self.showsVerticalScrollIndicator = NO;
    self.alwaysBounceVertical = NO;
    
    if (reusableCells == nil) {
        reusableCells = [[NSMutableSet alloc] init];
    }
    if (widths == nil) {
        widths = [[NSMutableArray alloc] init];
    }
    
    if (visibleCellIndexes == nil) {
        visibleCellIndexes  = [[NSMutableSet alloc] init];
    }
    
    if (containerView == nil) {
        containerView = [[UIView alloc] initWithFrame:CGRectZero];
        [containerView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:containerView];
    }
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if (self) {
        [self initData];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initData];
    }
    return self;
}

- (void)dealloc {
    self.headerView = nil;
    self.footerView = nil;
    [widths release];
    [reusableCells release];
    [containerView release];
    [visibleCellIndexes release];
    [super dealloc];
}

- (void)setDelegate:(id<TKHorizontalTableViewDelegate>)theDelegate
{
    [super setDelegate:theDelegate];
    delegate = theDelegate;
}

- (TKHorizontalTableViewCell *)cellForIndex:(NSInteger)index
{
    return (TKHorizontalTableViewCell *)[containerView viewWithTag:1000+index];
}

- (TKHorizontalTableViewCell *)dequeueReusableCell {
    TKHorizontalTableViewCell *cell = [reusableCells anyObject];
    if (cell) {
        [[cell retain] autorelease];
        [reusableCells removeObject:cell];
    }
    return cell;
}

- (void)setHeaderView:(UIView *)theHeaderView
{
    [self addSubview:theHeaderView];
    headerView = theHeaderView;
}

- (void)setFooterView:(UIView *)theFooterView
{
    [self addSubview:theFooterView];
    footerView = theFooterView;
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    if (newSuperview) {
        [self reloadData];
    }
}

- (void)reloadData {
    // 重用所有cell
    
    for (TKHorizontalTableViewCell *view in self.visibleCells) {
        [reusableCells addObject:view];
        [view removeFromSuperview];
    }
    [visibleCellIndexes removeAllObjects];
    [widths removeAllObjects];
    numberOfCells = [self.dataSource numberOfRowsInTableView:self];
    
    if ([self.delegate respondsToSelector:@selector(tableView:widthForCellAtIndex:)]) {
        for (int i = 0; i < numberOfCells; i++) {
            NSUInteger height = [self.delegate tableView:self widthForCellAtIndex:i];
            [widths addObject:[NSNumber numberWithDouble:height]];
        }
    } else {
        for (int i = 0; i < numberOfCells; i++) {
            [widths addObject:[NSNumber numberWithDouble:kTKCellDefaultWidth]];
        }
    }
    
    [self setNeedsLayout];
}

- (NSArray *)visibleCells
{
    return [containerView subviews];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect visibleBounds = CGRectMake(self.contentOffset.x, 0, self.bounds.size.width, self.bounds.size.height);
    
    // 重用不可见的cell
    for (TKHorizontalTableViewCell *cell in self.visibleCells) {
        
        if (! CGRectIntersectsRect(cell.frame, visibleBounds)) {
            [reusableCells addObject:cell];
            [cell removeFromSuperview];
        }
    }
    
    CGFloat height = self.bounds.size.height;
    CGFloat totalWidth = 0;
    
    NSMutableSet *theVisibleCellIndexes = [NSMutableSet set];
    
    //循环cell，计算出新增的cell
    for (NSUInteger row = 0; row < numberOfCells; row++) {

        double width = [[widths objectAtIndex:row] doubleValue];
        CGRect rect = CGRectMake(totalWidth, 0, width, height);
        
        if (CGRectIntersectsRect(visibleBounds, rect)) {
            
            [theVisibleCellIndexes addObject:[NSNumber numberWithUnsignedInteger:row]];

            BOOL cellIsMissing = ![visibleCellIndexes containsObject:[NSNumber numberWithUnsignedInteger:row]];
            
            if (cellIsMissing) {
                TKHorizontalTableViewCell *cell = [dataSource tableView:self cellAtIndex:row];
                cell.tag = 1000+row;
                // set the cell's frame so we insert it at the correct position
                [cell setFrame:rect];
                [containerView addSubview:cell];
            }
        }
        totalWidth += width;
    }
    [visibleCellIndexes release];
    visibleCellIndexes = [theVisibleCellIndexes retain];
    
    CGRect contentRect = CGRectMake(0, 0, totalWidth, height);
    [containerView setFrame:contentRect];
    
    CGFloat widthForHeader = 0;
    if ([delegate respondsToSelector:@selector(widthForHeaderInTableView:)]) {
        widthForHeader = [delegate widthForHeaderInTableView:self];
    }
    self.headerView.frame = CGRectMake(0, 0, widthForHeader, height);
    
    CGFloat widthForFooter = 0;
    if ([delegate respondsToSelector:@selector(widthForFooterInTableView:)]) {
        widthForFooter = [delegate widthForFooterInTableView:self];
    }
    self.footerView.frame = CGRectMake(widthForHeader + totalWidth, 0, widthForFooter, height);
    
    self.contentSize = CGSizeMake(widthForHeader + totalWidth + widthForFooter, height);
}
@end
