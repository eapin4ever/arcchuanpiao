//
//  UIImage+cut.m
//  VoiceChina
//
//  Created by wangguangzhao on 13-2-24.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "UIImage+cut.h"

@implementation UIImage (cut)

+(UIImage *)imageZoom:(UIImage *)image andLength:(CGFloat)length
{
    CGSize  imageSize=image.size;
    if (imageSize.width<length&&imageSize.height<length)
    {
        return image;
    }
    CGFloat widthRate = length/imageSize.width;
    CGFloat heightRate = length/imageSize.height;
    CGFloat rate=widthRate<heightRate ? widthRate:heightRate;//按照长的一边适应
    CGFloat width=imageSize.width*rate;
    CGFloat height=imageSize.height*rate;
    UIImage *newImage=[image resizedImage:CGSizeMake(width, height) interpolationQuality:kCGInterpolationDefault];
    return newImage;
}

-(UIImage*)scaleToSize:(CGSize)size
{
    UIGraphicsBeginImageContext(size);
    [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

- (CGFloat)getRatioWith:(CGSize)oldSize newSize:(CGSize)newSize
{
    CGFloat widthRatio = newSize.width / oldSize.width;
    CGFloat heightRadio = newSize.height / oldSize.height;
    return (widthRatio > heightRadio) ? widthRatio : heightRadio;
}

- (UIImage *)cutInFrame:(CGRect)targetFrame
{
    CGSize imageSize = self.size;
    
    CGFloat ratio = [self getRatioWith:self.size newSize:targetFrame.size];
    
    UIImage *superImage = [self scaleToSize:CGSizeMake(imageSize.width * ratio, imageSize.height*ratio)];
    
    CGImageRef imgrefout = CGImageCreateWithImageInRect([superImage CGImage], CGRectMake((superImage.size.width-targetFrame.size.width)/2, (superImage.size.height-targetFrame.size.height)/2, targetFrame.size.width, targetFrame.size.height));

    UIImage *img_ret = [[UIImage alloc]initWithCGImage:imgrefout];
    CGImageRelease( imgrefout );
    return img_ret;
}

- (UIImage *)cutInFrame:(CGRect)targetFrame cutMode:(ImageCutMode)cutMode
{
    CGSize imageSize = self.size;
    
    CGFloat ratio = [self getRatioWith:self.size newSize:targetFrame.size];
    
    UIImage *superImage = [self scaleToSize:CGSizeMake(imageSize.width * ratio, imageSize.height*ratio)];
    
    CGRect rect = CGRectMake(0, 0, 0, 0);
    if (cutMode == ImageCutFromTop) {
        rect = CGRectMake((superImage.size.width-targetFrame.size.width)/2, 0, targetFrame.size.width, targetFrame.size.height);
    } else if (cutMode == ImageCutFromCenter) {
        rect = CGRectMake((superImage.size.width-targetFrame.size.width)/2, (superImage.size.height-targetFrame.size.height)/2, targetFrame.size.width, targetFrame.size.height);
    } else {
        rect = CGRectMake((superImage.size.width-targetFrame.size.width)/2, superImage.size.height-targetFrame.size.height, targetFrame.size.width, targetFrame.size.height);
    }

    CGImageRef imgrefout = CGImageCreateWithImageInRect([superImage CGImage], rect);
    UIImage *img_ret = [[UIImage alloc]initWithCGImage:imgrefout];
    CGImageRelease( imgrefout );
    return img_ret;
}


@end
