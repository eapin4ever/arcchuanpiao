


//
//  TKHorizontalTableView.h
//  F3
//
//  Created by luobin on 12-38.
//  Copyright (c) __MyCompanyName__. All rights reserved.
//

#import "ZSTModuleBaseView.h"

@interface TKHorizontalTableViewCell : ZSTModuleBaseView

    

@end

@class TKHorizontalTableView;
@class TKHorizontalTableViewCell;

@protocol TKHorizontalTableViewDataSource <NSObject>

- (NSInteger)numberOfRowsInTableView:(TKHorizontalTableView *)tableView;

- (TKHorizontalTableViewCell *)tableView:(TKHorizontalTableView *)tableView cellAtIndex:(NSUInteger)index;

@end


@protocol TKHorizontalTableViewDelegate<UIScrollViewDelegate>

@optional

// Variable height support

- (CGFloat)tableView:(TKHorizontalTableView *)tableView widthForCellAtIndex:(NSUInteger)index;
- (CGFloat)widthForHeaderInTableView:(TKHorizontalTableView *)tableView;
- (CGFloat)widthForFooterInTableView:(TKHorizontalTableView *)tableView;

// Display customization

/* todo 
- (void)waterFlowView:(TKWaterFlowView *)waterFlowView willDisplayCell:(TKWaterFlowViewCell *)cell forRowAtIndex:(NSUInteger)index;

// Selection

// Called before the user changes the selection. Return a new indexPath, or nil, to change the proposed selection.
- (NSUInteger)waterFlowView:(TKWaterFlowView *)waterFlowView willSelectRowAtIndex:(NSUInteger)index;
- (NSUInteger)waterFlowView:(TKWaterFlowView *)waterFlowView willDeselectRowAtIndex:(NSUInteger)index;

// Called after the user changes the selection.
- (void)waterFlowView:(TKWaterFlowView *)waterFlowView didSelectRowAtIndex:(NSUInteger)index;
- (void)waterFlowView:(TKWaterFlowView *)waterFlowView didDeselectRowAtIndex:(NSUInteger)index;
*/
@end

@interface TKHorizontalTableView : UIScrollView{
    id <TKHorizontalTableViewDataSource>  dataSource;
    UIView                          *containerView;
    NSMutableSet                    *reusableCells;    
    NSMutableArray                  *widths;
    NSMutableSet                    *visibleCellIndexes;
    
}

@property (nonatomic, assign) IBOutlet id <TKHorizontalTableViewDataSource> dataSource;
@property (nonatomic, assign) IBOutlet id <TKHorizontalTableViewDelegate> delegate;
@property (nonatomic, readonly) NSUInteger numberOfCells;
@property (nonatomic, readonly) NSArray *visibleCells;
@property (nonatomic, readonly) UIView *containerView;
@property (nonatomic, retain) UIView *headerView;                 //default is nil
@property (nonatomic, retain) UIView *footerView;                 //default is nil

- (TKHorizontalTableViewCell *)cellForIndex:(NSInteger)index;

- (TKHorizontalTableViewCell *)dequeueReusableCell;
- (void)reloadData;

@end




