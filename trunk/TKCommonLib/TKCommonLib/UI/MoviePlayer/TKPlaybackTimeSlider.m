//
//  TKPlaybackTimeSlider.m
//  VideoTest
//
//  Created by luobin on 12-9-20.
//  Copyright (c) 2012年 luobin. All rights reserved.
//

#import "TKPlaybackTimeSlider.h"

@implementation TKPlaybackTimeSlider

@synthesize totalVideoTime;
@synthesize IsTouch;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setThumbImage:[UIImage imageNamed:@"TKCommonLib.bundle/MoviePlayer/slider_ThumbImage.png"] forState:UIControlStateNormal];
//        [self setMinimumTrackImage:[UIImage imageNamed:@"TKCommonLib.bundle/MoviePlayer/slider_minTrackImage.png"] forState:UIControlStateNormal];
//        [self setMaximumTrackImage:[UIImage imageNamed:@"TKCommonLib.bundle/MoviePlayer/slider_maxTrackImage.png"] forState:UIControlStateNormal];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setThumbImage:[UIImage imageNamed:@"TKCommonLib.bundle/MoviePlayer/slider_ThumbImage.png"] forState:UIControlStateNormal];
        [self setMinimumTrackImage:[[UIImage imageNamed:@"TKCommonLib.bundle/MoviePlayer/slider_minTrackImage.png"] stretchableImageWithLeftCapWidth:54 topCapHeight:6] forState:UIControlStateNormal];
        [self setMaximumTrackImage:[[UIImage imageNamed:@"TKCommonLib.bundle/MoviePlayer/slider_maxTrackImage.png"] stretchableImageWithLeftCapWidth:54 topCapHeight:6] forState:UIControlStateNormal];
    }
    return self;
}

- (CGRect)thumbRect {
    CGRect trackRect = [self trackRectForBounds:self.bounds];
    CGRect thumbR = [self thumbRectForBounds:self.bounds
                                   trackRect:trackRect
                                       value:self.value];
    return thumbR;
}

- (BOOL)beginTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
           
    CGPoint touchPoint = [touch locationInView:self];
    IsTouch = YES;
    
    CGFloat touchDivider = self.frame.size.width/self.totalVideoTime;
    CGFloat touchedLocation = touchPoint.x/touchDivider;
       
    [self setValue:touchedLocation animated:NO];
    
    return [super beginTrackingWithTouch:touch withEvent:event];
}

- (BOOL)continueTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event 
{    
    NSLog(@"in continuetracking");    
    
    return [super continueTrackingWithTouch:touch withEvent:event];
}

- (void)cancelTrackingWithEvent:(UIEvent *)event {
    [super cancelTrackingWithEvent:event];
}

- (void)endTrackingWithTouch:(UITouch *)touch withEvent:(UIEvent *)event {
   
    NSLog(@"in endtracking");
    
    IsTouch = NO;   
    [super endTrackingWithTouch:touch withEvent:event];
}

- (void)dealloc
{
    [super dealloc];
}

@end