//
// Copyright 2009-2011 Facebook
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#import "TTTableSubtitleItemCell.h"

#import "TTTableSubtitleItem.h"
#import "UIViewAdditions.h"
#import "UIFontAdditions.h"

// Style
#import "TTGlobalStyle.h"
#import "TTDefaultStyleSheet.h"

const NSInteger kTableMessageTextLineCount = 2;

const CGFloat   kTableCellSmallMargin = 6;
const CGFloat   kTableCellSpacing     = 8;
const CGFloat   kTableCellMargin      = 10;
const CGFloat   kTableCellHPadding    = 10;
const CGFloat   kTableCellVPadding    = 10;

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
@implementation TTTableSubtitleItemCell

@synthesize object;

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString*)identifier {
	self = [super initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:identifier];
    if (self) {
        self.textLabel.font = TTSTYLEVAR(tableFont);
        self.textLabel.textColor = TTSTYLEVAR(textColor);
        self.textLabel.highlightedTextColor = TTSTYLEVAR(highlightedTextColor);
        self.textLabel.backgroundColor = TTSTYLEVAR(backgroundTextColor);
        self.textLabel.textAlignment = NSTextAlignmentLeft;
        self.textLabel.lineBreakMode = UILineBreakModeTailTruncation;
        self.textLabel.adjustsFontSizeToFitWidth = YES;
        
        self.detailTextLabel.font = TTSTYLEVAR(font);
        self.detailTextLabel.textColor = TTSTYLEVAR(tableSubTextColor);
        self.detailTextLabel.highlightedTextColor = TTSTYLEVAR(highlightedTextColor);
        self.detailTextLabel.backgroundColor = TTSTYLEVAR(backgroundTextColor);
        self.detailTextLabel.textAlignment = NSTextAlignmentLeft;
        self.detailTextLabel.contentMode = UIViewContentModeTop;
        self.detailTextLabel.lineBreakMode = UILineBreakModeTailTruncation;
        self.detailTextLabel.numberOfLines = kTableMessageTextLineCount;
    }
    
    return self;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)dealloc {
    self.object = nil;
    [super dealloc];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark TTTableViewCell class public


///////////////////////////////////////////////////////////////////////////////////////////////////
+ (CGFloat)tableView:(UITableView*)tableView rowHeightForObject:(id)object {
    TTTableSubtitleItem* item = object;
    
    CGFloat height = TTSTYLEVAR(tableFont).tkLineHeight + kTableCellVPadding*2;
    if (item.subtitle) {
        height += TTSTYLEVAR(font).tkLineHeight;
    }
    
    return height;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UIView


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat height = self.contentView.height;
    CGFloat width = self.contentView.width - (height + kTableCellSmallMargin);
    CGFloat left = kTableCellHPadding;
    
    if (self.detailTextLabel.text.length) {
        CGFloat textHeight = self.textLabel.font.tkLineHeight;
        CGFloat subtitleHeight = self.detailTextLabel.font.tkLineHeight;
        CGFloat paddingY = floor((height - (textHeight + subtitleHeight))/2);
        
        self.textLabel.frame = CGRectMake(left, paddingY, width, textHeight);
        self.detailTextLabel.frame = CGRectMake(left, self.textLabel.bottom, width, subtitleHeight);
        
    } else {
        self.textLabel.frame = CGRectMake(kTableCellSmallMargin, 0, width, height);
        self.detailTextLabel.frame = CGRectZero;
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark TTTableViewCell


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setObject:(TTTableSubtitleItem *)theObject {
    if (object != theObject) {
        
        [object release];
        object = [theObject retain];
        
        if (nil != object.delegate && nil != object.selector) {
            self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            self.selectionStyle = TTSTYLEVAR(tableSelectionStyle);
            
        } else {
            self.accessoryType = UITableViewCellAccessoryNone;
//            self.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        if (object.text.length) {
            self.textLabel.text = object.text;
        }
        if (object.subtitle.length) {
            self.detailTextLabel.text = object.subtitle;
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Public


///////////////////////////////////////////////////////////////////////////////////////////////////
- (UILabel*)subtitleLabel {
    return self.detailTextLabel;
}


///////////////////////////////////////////////////////////////////////////////////////////////////

@end
