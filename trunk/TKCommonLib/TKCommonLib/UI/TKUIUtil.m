//
//  TKUIUtil.m
//  
//
//  Created by luo bin on 12-3-4.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "TKUIUtil.h"
#import "MBProgressHUD.h"
#import "HJManagedImageV.h"
#import "TKImageCacheManager.h"

#define kButtonPadding 5

//CGPathRef newPathWithRoundRect(CGRect rect, CGFloat cornerRadius,int roundCornerStyle) {
//	//
//	// Create the boundary path
//	//
//	CGMutablePathRef path = CGPathCreateMutable();
//	CGPathMoveToPoint(path, NULL,
//					  rect.origin.x,
//					  rect.origin.y + rect.size.height - cornerRadius);
//	
//	// Top left corner
//	//cornerStyle
//	CGPathAddArcToPoint(path, NULL,
//						rect.origin.x,
//						rect.origin.y,
//						rect.origin.x + rect.size.width,
//						rect.origin.y,
//						roundCornerStyle&ROUND_CORNER_STYLE_TOP_LEFT?cornerRadius:0);
//	// Top right corner
//	CGPathAddArcToPoint(path, NULL,
//						rect.origin.x + rect.size.width,
//						rect.origin.y,
//						rect.origin.x + rect.size.width,
//						rect.origin.y + rect.size.height,
//						roundCornerStyle&ROUND_CORNER_STYLE_TOP_RIGHT?cornerRadius:0);
//	
//	// Bottom right corner
//	CGPathAddArcToPoint(path, NULL,
//						rect.origin.x + rect.size.width,
//						rect.origin.y + rect.size.height,
//						rect.origin.x,
//						rect.origin.y + rect.size.height,
//						roundCornerStyle&ROUND_CORNER_STYLE_BOTTOM_RIGHT?cornerRadius:0);
//	
//	// Bottom left corner
//	CGPathAddArcToPoint(path, NULL,
//						rect.origin.x,
//						rect.origin.y + rect.size.height,
//						rect.origin.x,
//						rect.origin.y,
//						roundCornerStyle&ROUND_CORNER_STYLE_BOTTOM_LEFT?cornerRadius:0);
//	
//	// Close the path at the rounded rect
//	CGPathCloseSubpath(path);
//	
//	return path;
//}

@implementation TKUIUtil

+ (void)promptInView:(UIView *)view 
          withTitle:(NSString *)title
         withCenter:(CGPoint)center 
           delegate:(id<MBProgressHUDDelegate>)delegate
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:view animated:YES];//等待提示
    [HUD setCenter:center];
    HUD.tag = 1000;
    HUD.delegate = delegate;
    HUD.margin = 10;
//    HUD.radius = 4;
    HUD.labelFont = [UIFont systemFontOfSize:12];
    HUD.removeFromSuperViewOnHide = YES;
    HUD.mode = MBProgressHUDModeCustomView;
    HUD.labelText = title;
    [HUD hide:YES afterDelay:3];
}

+ (void)promptInView:(UIView *)view withTitle:(NSString *)title withCenter:(CGPoint)center
{
    [self promptInView:view withTitle:title withCenter:center delegate:nil];
}

+ (void)clearPromptInView:(UIView *)view 
{
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[MBProgressHUD class]] && subView.tag == 1000) {
            ((MBProgressHUD *)subView).delegate = nil;
            [subView removeFromSuperview];
        }
    }
}

+ (BOOL)isActivity:(UIView *)view
{
    for (UIView *subView in view.subviews) {
        if ([subView isKindOfClass:[MBProgressHUD class]]) {
            return YES;
        }
    }
    return NO;
}

+ (void)alertInView:(UIView *)view withTitle:(NSString *)title message:(NSString *)message cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitles:(NSString *)otherButtonTitles,...NS_REQUIRES_NIL_TERMINATION
{
    CGPoint viewCenter = CGPointMake(CGRectGetMidX(view.frame)-view.frame.origin.x, CGRectGetMidY(view.frame));
    CGSize size = [message sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(245, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
    float height = size.height;
    if(height > 220){
        height = 220;
    }
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:view animated:YES];
    [HUD setCenter:viewCenter];
    HUD.removeFromSuperViewOnHide = YES;

    UIView *bgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, height+30+40)];
    bgView.backgroundColor = [UIColor clearColor];

    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0,200,30)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = title;
    titleLabel.font = [UIFont systemFontOfSize:16];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:titleLabel];
    [titleLabel release];

    UIScrollView *messageView = [[UIScrollView alloc] initWithFrame:CGRectMake(0,CGRectGetMaxY(titleLabel.frame),200,height)];
    messageView.backgroundColor = [UIColor clearColor];
    messageView.contentSize = CGSizeMake(200,size.height);
    [bgView addSubview:messageView];

    UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,0,200,size.height)];
    messageLabel.backgroundColor = [UIColor clearColor];
    messageLabel.text = message;
    messageLabel.numberOfLines = 0;
    messageLabel.font = [UIFont systemFontOfSize:14];
    messageLabel.textColor = [UIColor whiteColor];
    messageLabel.textAlignment = NSTextAlignmentLeft;
    [messageView addSubview:messageLabel];

    [messageLabel release];
    [messageView release];

    UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [btn setTitle:cancelButtonTitle forState:UIControlStateNormal];
    btn.frame = CGRectMake(65,CGRectGetMaxY(messageView.frame)+10,70,30);
    [btn addTarget:HUD action:@selector(hide:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:btn];


    HUD.customView = [bgView autorelease];
    HUD.mode = MBProgressHUDModeCustomView;

}

+ (void)alertInView:(UIView *)view withTitle:(NSString *)title withImage:(UIImage *)image withCenter:(CGPoint)center
{
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:view animated:YES];//等待提示
    [HUD setCenter:center];
    HUD.removeFromSuperViewOnHide = YES;
    HUD.customView = [[[UIImageView alloc] initWithImage:image] autorelease];
    HUD.mode = MBProgressHUDModeCustomView;
    HUD.labelText = title;
    [HUD hide:YES afterDelay:2];
}

+ (void)alertInView:(UIView *)view withTitle:(NSString *)title withImage:(UIImage *)image
{
    CGPoint viewCenter = CGPointMake(CGRectGetMidX(view.frame)-view.frame.origin.x, CGRectGetMidY(view.frame));
    [self alertInView:view withTitle:title withImage:image withCenter:viewCenter];
}

+ (void)alertInWindow:(NSString *)title withImage:(UIImage *)image withCenter:(CGPoint)center
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    if (!window) {
        window = [[UIApplication sharedApplication].windows objectAtIndex:0];
    }
    [self alertInView:window withTitle:title withImage:image withCenter:center];
}

+ (void)alertInWindow:(NSString *)title withImage:(UIImage *)image
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    if (!window) {
        window = [[UIApplication sharedApplication].windows objectAtIndex:0];
    }
    [self alertInView:window withTitle:title withImage:image];
}

MBProgressHUD* _HUD;


+ (void)showHUDInView:(UIView *)view withText:(NSString*)text withImage:(UIImage *)image withCenter:(CGPoint)center delegate:(id)delegate{
    if (_HUD==nil) {
        _HUD = [MBProgressHUD showHUDAddedTo:view animated:YES];//等待提示
        _HUD.removeFromSuperViewOnHide = YES;
        _HUD.minShowTime = 1;
    }
    
    _HUD.center = center;
    _HUD.delegate = delegate;
    
    if (image) {
        _HUD.customView = [[[UIImageView alloc] initWithImage:image] autorelease];
    }
    
    if (text) {
        _HUD.labelText = text;
        _HUD.labelFont = [UIFont systemFontOfSize:12];
    }
    
    if (image) {
        _HUD.mode = MBProgressHUDModeCustomView;
    }
}

+ (void)showHUDInView:(UIView *)view withText:(NSString*)text withImage:(UIImage *)image withCenter:(CGPoint)center
{
    [self showHUDInView:view withText:text withImage:image withCenter:center delegate:nil];
}

+ (void)showHUDInView:(UIView *)view withText:(NSString*)text withCenter:(CGPoint)center {
    [self showHUDInView:view withText:text withImage:nil withCenter:center];
}

+ (void)showHUDInView:(UIView *)view withText:(NSString*)text withImage:(UIImage *)image {
    [self showHUDInView:view withText:text withImage:image withCenter:view.center];
}

+ (void)showHUDInView:(UIView *)view withText:(NSString*)text withImage:(UIImage *)image delegate:(id)delegate{
    [self showHUDInView:view withText:text withImage:image withCenter:view.center delegate:delegate];
}

+ (void)showHUD:(UIView *)view withText:(NSString*)text {
    [self showHUDInView:view withText:text withImage:nil];
}

+ (void)showHUD:(UIView *)view {
    [self showHUDInView:view withText:nil withImage:nil];
}

+ (void)showHUDInView:(UIView *)view withCenter:(CGPoint)center
{
    [self showHUDInView:view withText:nil withCenter:center];
}

+ (void)hiddenHUD {
    if (_HUD != nil) {
        [_HUD hide:YES];
        _HUD.delegate = nil;
        _HUD=nil;
    }
}

+ (void)hiddenHUDAfterDelay:(NSTimeInterval)delay {
    if (_HUD != nil) {
        [_HUD hide:YES afterDelay:delay];
        _HUD = nil;
    }
}


+ (UIBarButtonItem *)itemForNavigationWithTitle:(NSString *)title target:(id)target selector:(SEL)selector
{
    CGSize size = [title sizeWithFont:[UIFont systemFontOfSize:14]
                    constrainedToSize:CGSizeMake(121, 28)
                        lineBreakMode:UILineBreakModeTailTruncation];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, size.width + 20, 30);
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    btn.titleLabel.shadowOffset = CGSizeMake(0, -0.5f);
    [btn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [btn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[[UIImage imageNamed:@"TKCommonLib.bundle/NaivagationBar/btn_navigationBaritemAction_normal.png"] stretchableImageWithLeftCapWidth:25 topCapHeight:0] forState:UIControlStateNormal];
    [btn setBackgroundImage:[[UIImage imageNamed:@"TKCommonLib.bundle/NaivagationBar/btn_navigationBaritemAction_pressed.png"] stretchableImageWithLeftCapWidth:25 topCapHeight:0] forState:UIControlStateHighlighted];
    [btn setBackgroundImage:[[UIImage imageNamed:@"TKCommonLib.bundle/NaivagationBar/btn_navigationBaritemAction_disable.png"] stretchableImageWithLeftCapWidth:25 topCapHeight:0] forState:UIControlStateDisabled];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:14];
    
    return [[[UIBarButtonItem alloc] initWithCustomView:btn] autorelease];
}

+ (UIBarButtonItem *)itemForNavigationWithTitle:(NSString *)title withImage:(NSString *)imageURL target:(id)target selector:(SEL)selector
{
    CGSize size = [title sizeWithFont:[UIFont systemFontOfSize:12]
                    constrainedToSize:CGSizeMake(121, 28)
                        lineBreakMode:UILineBreakModeTailTruncation];
    
    HJManagedImageV *imageView = [[[HJManagedImageV alloc] initWithFrame:CGRectMake(kButtonPadding, kButtonPadding, 20, 20)] autorelease];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.defaultImage = [UIImage imageNamed:@"pull-refresh-cat.png"];
    imageView.url = [NSURL URLWithString:imageURL];
    [[TKImageCacheManager shared] manage:imageView];
    
    CALayer * imageViewLayer = [imageView layer];
    [imageViewLayer setMasksToBounds:YES];
    [imageViewLayer setCornerRadius:5.0];
    
    UILabel *btnLabel = [[[UILabel alloc] initWithFrame:CGRectMake(2*kButtonPadding + 20, 1, size.width, 28)] autorelease];
    btnLabel.font = [UIFont systemFontOfSize:12];
    btnLabel.backgroundColor = [UIColor clearColor];
    btnLabel.lineBreakMode = UILineBreakModeTailTruncation;
    btnLabel.text = title;
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 3*kButtonPadding + 20 + size.width, 30);
    [btn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [btn addSubview:imageView];
    [btn addSubview:btnLabel];
    UIImage *backgroundImage = [[UIImage imageNamed:@"btn_navigationBaritemAction_normal.png"] stretchableImageWithLeftCapWidth:5 topCapHeight:0];
    [btn setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    return [[[UIBarButtonItem alloc] initWithCustomView:btn] autorelease];
}

+ (UIBarButtonItem *)backItemForNavigationWithTitle:(NSString *)title target:(id)target selector:(SEL)selector
{
    CGSize size = [title sizeWithFont:[UIFont systemFontOfSize:14]
                    constrainedToSize:CGSizeMake(121, 28)
                        lineBreakMode:UILineBreakModeTailTruncation];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = [UIColor clearColor];
    btn.frame = CGRectMake(0, 0, size.width + 25, 30);
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 7, 0, 0);
    btn.titleLabel.shadowOffset = CGSizeMake(0, -0.5f);
    [btn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [btn addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[[UIImage imageNamed:@"TKCommonLib.bundle/NaivagationBar/btn_navigationBaritemBack_normal.png"] stretchableImageWithLeftCapWidth:30 topCapHeight:9] forState:UIControlStateNormal];
    [btn setBackgroundImage:[[UIImage imageNamed:@"TKCommonLib.bundle/NaivagationBar/btn_navigationBaritemBack_pressed.png"] stretchableImageWithLeftCapWidth:30 topCapHeight:9] forState:UIControlStateHighlighted];
    [btn setBackgroundImage:[[UIImage imageNamed:@"TKCommonLib.bundle/NaivagationBar/btn_navigationBaritemBack_disable.png"] stretchableImageWithLeftCapWidth:30 topCapHeight:9] forState:UIControlStateDisabled];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:14];
    return [[[UIBarButtonItem alloc] initWithCustomView:btn] autorelease];
}

+ (UIView *)titleViewForNavigationWithTitle:(NSString *)title
{
    return [self titleViewForNavigationWithTitle:title withSize:CGSizeMake(80, 44)];
}

+ (UIView *)titleViewForNavigationWithTitle:(NSString *)title withSize:(CGSize)size
{
    return [self titleViewForNavigationWithTitle:title 
                                        withSize:size 
                                       withLines:1 
                                        withFont:[UIFont boldSystemFontOfSize:20]];
}

+ (UIView *)titleViewForNavigationWithTitle:(NSString *)title withSize:(CGSize)size withLines:(NSUInteger)numbers withFont:(UIFont *)font
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 60, size.width, size.height)];
    label.numberOfLines = numbers;
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = title;
    label.font = font;
    label.adjustsFontSizeToFitWidth = YES;
    return [label autorelease];
}

@end
