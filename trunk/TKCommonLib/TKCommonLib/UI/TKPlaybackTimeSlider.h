//
//  TKPlaybackTimeSlider.h
//  VideoTest
//
//  Created by luobin on 12-9-20.
//  Copyright (c) 2012年 luobin. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TKPlaybackTimeSlider : UISlider {
    
    NSTimeInterval totalVideoTime;
    BOOL IsTouch;
}

@property(nonatomic, assign) NSTimeInterval totalVideoTime;
@property(nonatomic, assign) BOOL IsTouch;

- (CGRect)thumbRect;

@end