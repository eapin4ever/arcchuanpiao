//
//  HHAsynImageView.m
//  HHCommonLib
//
//  Created by luobin on 4/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "TKAsynImageView.h"
#import "TKImageCacheManager.h"

@implementation TKAsynImageView

@synthesize adorn;
@synthesize imageInset;
@synthesize bmiddle_pic_url;
@synthesize original_pic_url;
@synthesize asynImageDelegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        
        adornImageView = [[UIImageView alloc] init];
        adornImageView.contentMode = UIViewContentModeScaleToFill;
        adornImageView.backgroundColor = [UIColor clearColor];
        [self insertSubview:adornImageView atIndex:0];
        [self addTarget:self action:@selector(didSelected:) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

- (void)dealloc
{
    [adornImageView release];
    self.adorn = nil;
    [super dealloc];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    adornImageView.frame = self.bounds;
    
    if (self.adorn) {
        if (self.imageView.superview == self) {
            self.imageView.backgroundColor = [UIColor clearColor];
//            self.imageView.frame = CGRectOffset(CGRectInset(self.bounds, 4, 4), 0, -1);
            self.imageView.frame = UIEdgeInsetsInsetRect(self.bounds, self.imageInset);
        }
    } else {
        if (self.imageView.superview == self) {
            self.imageView.frame = self.bounds;
        }
    }
}

- (void)setAdorn:(UIImage *)theAdorn
{
    if (adorn != theAdorn) {
        [adorn release];
        adorn = [theAdorn retain];
        
//        adornImageView.image = adorn;
    }
}

- (void)loadImage
{
    if (self.showLoadingWheel) {
        [loadingWheel startAnimating];
    }
    
    self.oid = [[self.url absoluteString] md5];
    
    //tell the object manager to manage the managed image view, 
	//this causes the cached image to display, or the image to be loaded, cached, and displayed
	[[TKImageCacheManager shared] manage:self];
}

- (void)didSelected:(TKAsynImageView *)sender
{
    if ([asynImageDelegate respondsToSelector:@selector(asynImageViewDidClicked:)]) {
        [asynImageDelegate asynImageViewDidClicked:self];
    }
}
@end
