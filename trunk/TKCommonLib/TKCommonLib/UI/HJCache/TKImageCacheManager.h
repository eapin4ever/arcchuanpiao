//
//  TKImageCacheManager.h
//  TKCommonLib
//
//  Created by luobin on 4/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class HJObjManager;
@protocol HJMOUser;

@interface TKImageCacheManager : NSObject
{
    HJObjManager *_objMan;
}

SINGLETON_INTERFACE(TKImageCacheManager);

-(BOOL) manage:(id<HJMOUser>)user;

- (void)clearMemCache;

@end
