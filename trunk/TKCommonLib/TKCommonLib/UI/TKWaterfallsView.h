
//
//  TKWaterFlowView.h
//  
//
//  Created by luobin on 12-38.
//  Copyright (c) __MyCompanyName__. All rights reserved.
//  update by xuhuijun 


@interface TKWaterfallsViewCell : UIView

    

@end

@class TKWaterfallsView;
@class TKWaterfallsViewCell;

@protocol TKWaterfallsViewDataSource <NSObject>


- (NSInteger)numberOfColumsInWaterfallsView:(TKWaterfallsView *)waterfallsView;

- (NSInteger)numberOfRowsInWaterfallsView:(TKWaterfallsView *)waterfallsView;

- (TKWaterfallsViewCell *)waterfallsView:(TKWaterfallsView *)waterfallsView cellAtIndex:(NSUInteger)index;

@end


@protocol TKWaterfallsViewDelegate<UIScrollViewDelegate>

@optional

// Variable height support

- (CGFloat)waterfallsView:(TKWaterfallsView *)waterfallsView heightForCellAtIndex:(NSUInteger)index;
- (CGFloat)heightForHeaderInWaterfallsView:(TKWaterfallsView *)waterfallsView;
- (CGFloat)heightForFooterInWaterfallsView:(TKWaterfallsView *)waterfallsView;

- (void)waterFlowView:(TKWaterfallsView *)waterFlowView didSelectRowAtIndex:(NSUInteger)index;

// Display customization

/* todo 
- (void)waterFlowView:(TKWaterFlowView *)waterFlowView willDisplayCell:(TKWaterFlowViewCell *)cell forRowAtIndex:(NSUInteger)index;

// Selection

// Called before the user changes the selection. Return a new indexPath, or nil, to change the proposed selection.
- (NSUInteger)waterFlowView:(TKWaterFlowView *)waterFlowView willSelectRowAtIndex:(NSUInteger)index;
- (NSUInteger)waterFlowView:(TKWaterFlowView *)waterFlowView willDeselectRowAtIndex:(NSUInteger)index;

// Called after the user changes the selection.
- (void)waterFlowView:(TKWaterFlowView *)waterFlowView didDeselectRowAtIndex:(NSUInteger)index;
*/
@end


@interface TKWaterfallsView : UIScrollView {
    id <TKWaterfallsViewDataSource>  dataSource;
    UIView                          *containerView;
    NSMutableSet                    *reusableCells;    
    NSMutableArray                  *heights;
    NSMutableSet                    *visibleCellIndexes;
    float _cellWidth;   //每列的宽度

}

@property (nonatomic, assign) IBOutlet id <TKWaterfallsViewDataSource> dataSource;
@property (nonatomic, assign) IBOutlet id <TKWaterfallsViewDelegate> delegate;
@property (nonatomic, readonly) NSUInteger numberOfCells;
@property (nonatomic, readonly) NSArray *visibleCells;
@property (nonatomic, assign) CGFloat separaterWidth;                 //default is nil
@property (nonatomic, readonly) UIView *containerView;
@property (nonatomic, retain) UIView *headerView;                 //default is nil
@property (nonatomic, retain) UIView *footerView;                 //default is nil
@property (nonatomic,assign) float cellWidth;


- (TKWaterfallsViewCell *)dequeueReusableCell;
- (void)reloadData;

@end




