//
//  ZSTPageListController.m
//  YouYun
//
//  Created by luobin on 6/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "TKPageTableViewController.h"
#import <objc/runtime.h>

#define kOneLimit 30


@implementation TKPageTableViewCell
@synthesize object;
@synthesize row;

+ (CGFloat)tableView:(UITableView*)tableView rowHeightForObject:(id)object
{
    return TK_ROW_HEIGHT;
}

- (void)objectUpdate
{
    
}


- (void)setObject:(id)theObject
{
    if (object != theObject) {
        [object release];
        object = [theObject retain];
        [self objectUpdate];
    }
}

- (void)dealloc
{
    self.object = nil;
    [super dealloc];
}

@end


@interface TKPageTableViewController()

- (void)autoRefresh;

- (void)appendData:(NSArray*)data;

- (void)finishLoading;

@end

@implementation TKPageTableViewController

@synthesize data = _datas;

- (void)dealloc
{
    self.data = nil;
    TKRELEASE(_loadMoreView);
    TKRELEASE(_refreshHeaderView);
    TKRELEASE(_datas);
    [super dealloc];
}

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _hasMore = NO;
    _isLoadingMore = NO;
    _isRefreshing = NO;
    
    if (_refreshHeaderView == nil) {
        
        _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f-250.0, self.view.frame.size.width, 250.0)];
        _refreshHeaderView.layer.contents = (id) [UIImage imageNamed:@"TKCommonLib.bundle/EGOTableViewPullRefresh/pulltorefresh_background.png"].CGImage;

        _refreshHeaderView.delegate = self;
        [self.tableView addSubview:_refreshHeaderView];
        
        [_refreshHeaderView refreshLastUpdatedDate];
        [self performSelector:@selector(autoRefresh) 
                   withObject:nil 
                   afterDelay:0.6];
    }

//    self.tableView.tableFooterView = [[[UIView alloc] init] autorelease];
//    UIImageView *footerView = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"general_bg.png")];
//    
//    footerView.frame = CGRectMake(0, 0, 320, 480);
//    [self.tableView.tableFooterView addSubview:footerView];
    
    if (_loadMoreView == nil) {
        _loadMoreView = [[TKLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
        _loadMoreView.delegate = self;
        if (_hasMore) {
            self.tableView.tableFooterView = _loadMoreView;
        }
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (Class)tableView:(UITableView*)tableView cellClassForObject:(id)object
{
    return [TKPageTableViewCell class];
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.data objectAtIndex:indexPath.row];
    Class cls = [self tableView:tableView cellClassForObject:object];
    return [cls tableView:tableView rowHeightForObject:object];
}

#pragma mark - UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_datas count];
}

#pragma mark - UITableViewDatasource 

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id object = [self.data objectAtIndex:indexPath.row];
    Class cellClass = [self tableView:tableView cellClassForObject:object];
    const char* className = class_getName(cellClass);
    NSString* identifier = [[NSString alloc] initWithBytesNoCopy:(void*)className
                                                          length:strlen(className)
                                                        encoding:NSASCIIStringEncoding freeWhenDone:NO];
    
    UITableViewCell* cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[[cellClass alloc] initWithStyle:UITableViewCellStyleDefault
                                 reuseIdentifier:identifier] autorelease];
    }
    [identifier release];
    
    if ([cell isKindOfClass:[TKPageTableViewCell class]]) {
        ((TKPageTableViewCell *)cell).row = indexPath.row;
        [(TKPageTableViewCell*)cell setObject:[self.data objectAtIndex:indexPath.row]];
    }
    return cell;
}

- (void)autoRefresh
{
    [_refreshHeaderView autoRefreshOnScroll:self.tableView animated:YES];
}

- (void)refreshNewestData {
    
    
}

- (void)loadMoreData {
    
    
}

- (void)appendData:(NSArray*)dataArray
{
    [_datas addObjectsFromArray:dataArray];
}

#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    if (!_isLoadingMore) {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
    if (!_isRefreshing) {
        [_loadMoreView loadMoreScrollViewDidEndDragging:scrollView];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (!_isLoadingMore) {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
    if (!_isRefreshing) {
        [_loadMoreView loadMoreScrollViewDidEndDragging:scrollView];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!_isLoadingMore) {
        [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
    }
    if (!_isRefreshing) {
        [_loadMoreView loadMoreScrollViewDidScroll:scrollView];
    }
}

#pragma mark EGORefreshTableHeaderDelegate Methods
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view {
    _isRefreshing = YES;
    [self refreshNewestData];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView *)view {
    return _isRefreshing;
}

- (NSDate *)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView *)view {
    return [NSDate date];
}

#pragma mark HHLoadMoreViewDelegate Methods
- (void)loadMoreDidTriggerRefresh:(TKLoadMoreView*)view
{
    if (_hasMore) {
        _isLoadingMore = YES;
        [self loadMoreData];
    }
}

- (BOOL)loadMoreDataSourceIsLoading:(TKLoadMoreView*)view
{
    return _isLoadingMore;
}

- (void)finishLoading
{
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    [_loadMoreView loadMoreScrollViewDataSourceDidFinishedLoading:self.tableView];
    
    _isRefreshing = NO;
    _isLoadingMore = NO;
}

- (void)fetchDataSuccess:(NSArray *)theData hasMore:(BOOL)hasMore
{
    _hasMore = hasMore;
    if (hasMore) {
        self.tableView.tableFooterView = _loadMoreView;
    } else {
        self.tableView.tableFooterView = nil;
    }
    
    if (_isRefreshing) {
        
        [self setData:[theData mutableCopy]];
        
    } else if(_isLoadingMore)
    {
        [self appendData:theData];
    }
    
    [self.tableView reloadData];
    
    [self finishLoading];
}

- (void)fetchDataFailed
{
    [self finishLoading];
}

@end
