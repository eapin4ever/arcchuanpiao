//
//  ZSTBubbleView.m
//  TKCommonLib
//
//  Created by xuhuijun  on 12-12-30.
//  Copyright (c) 2012年 zhangshangtong. All rights reserved.
//

#import "ZSTBubbleView.h"

#define KFacialSizeWidth  18
#define KFacialSizeHeight 18
#define MAX_WIDTH 165

@implementation ZSTBubbleView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}


#define BEGIN_FLAG @"[/"
#define END_FLAG @"]"

//图文混排
+ (void)getImageRange:(NSString*)message : (NSMutableArray*)array {
    NSRange range=[message rangeOfString: BEGIN_FLAG];
    NSRange range1=[message rangeOfString: END_FLAG];
    //判断当前字符串是否还有表情的标志。
    if (range.length>0 && range1.length>0) {
        if (range.location > 0) {
            [array addObject:[message substringToIndex:range.location]];
            [array addObject:[message substringWithRange:NSMakeRange(range.location, range1.location+1-range.location)]];
            NSString *str=[message substringFromIndex:range1.location+1];
            [self getImageRange:str :array];
        }else {
            NSString *nextstr=[message substringWithRange:NSMakeRange(range.location, range1.location+1-range.location)];
            //排除文字是“”的
            if (![nextstr isEqualToString:@""]) {
                [array addObject:nextstr];
                NSString *str=[message substringFromIndex:range1.location+1];
                [self getImageRange:str :array];
            }else {
                return;
            }
        }
        
    } else if (message != nil) {
        [array addObject:message];
    }
}

+ (UIView *)assembleMessageAtIndex : (NSString *) message from:(BOOL)fromself
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    [self getImageRange:message :array];
    UIView *returnView = [[UIView alloc] initWithFrame:CGRectZero];
    NSArray *data = array;
    UIFont *fon = [UIFont systemFontOfSize:14.0f];
    CGFloat upX = 0;
    CGFloat upY = 0;
    CGFloat X = 0;
    CGFloat Y = 0;
    if (data) {
        
        UILabel *fromlabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, 50, 20)];
        fromlabel.font = [UIFont systemFontOfSize:14.0f];
        fromlabel.backgroundColor = [UIColor clearColor];
        fromlabel.tag = 1200;
        
        UIImageView *timeImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TKCommonLib.bundle/BubbleView/bubble-time.png"]];
        timeImageView.frame = CGRectMake(CGRectGetMaxX(fromlabel.frame)+5, 4+2, 12, 12);
        
        UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(timeImageView.frame)+5, 2, 115, 20)];
        dateLabel.font = [UIFont systemFontOfSize:11];
        dateLabel.textColor = [UIColor colorWithRed:145/255.0 green:145/255.0 blue:145/255.0 alpha:1];
        dateLabel.backgroundColor = [UIColor clearColor];
        NSDateFormatter  *formatter = [[NSDateFormatter alloc] init];
		[formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
		NSString *timeString = [formatter stringFromDate:[NSDate date]];
		[formatter release];
        dateLabel.tag = 1201;
		[dateLabel setText:timeString];
        
        if (fromself) {
            fromlabel.textColor = [UIColor colorWithRed:199/255.0 green:156/255.0 blue:54/255.0 alpha:1];
            fromlabel.text = NSLocalizedString(@"用户：", nil);
        }else {
            
            fromlabel.textColor = [UIColor colorWithRed:47/255.0 green:137/255.0 blue:198/255.0 alpha:1];
            fromlabel.text = NSLocalizedString(@"客服：", nil);
        }
        
        [returnView addSubview:fromlabel];
        [returnView addSubview:timeImageView];
        [returnView addSubview:dateLabel];
        [fromlabel release];
        [timeImageView release];
        [dateLabel release];
        
        for (int i=0;i < [data count];i++) {
            NSString *str=[data objectAtIndex:i];
            if ([str hasPrefix: BEGIN_FLAG] && [str hasSuffix: END_FLAG])
            {
                if (upX >= MAX_WIDTH)
                {
                    upY = upY + KFacialSizeHeight;
                    upX = 0;
                    X = 150;
                    Y = upY;
                }
                NSString *imageName=[str substringWithRange:NSMakeRange(2, str.length - 3)];
                UIImageView *img=[[UIImageView alloc]initWithImage:[UIImage imageNamed:imageName]];
                img.frame = CGRectMake(upX, upY+30, KFacialSizeWidth, KFacialSizeHeight);
                [returnView addSubview:img];
                [img release];
                upX=KFacialSizeWidth+upX;
                if (X<150) X = upX;
                
                
            } else {
                for (int j = 0; j < [str length]; j++) {
                    NSString *temp = [str substringWithRange:NSMakeRange(j, 1)];
                    if (upX >= MAX_WIDTH)
                    {
                        upY = upY + KFacialSizeHeight;
                        upX = 0;
                        X = 150;
                        Y =upY;
                    }
                    CGSize size=[temp sizeWithFont:fon constrainedToSize:CGSizeMake(150, 40)];
                    UILabel *la = [[UILabel alloc] initWithFrame:CGRectMake(upX,upY+30,size.width,size.height)];
                    la.font = fon;
                    la.text = temp;
                    la.backgroundColor = [UIColor clearColor];
                    [returnView addSubview:la];
                    [la release];
                    upX=upX+size.width;
                    if (X<150) {
                        X = upX;
                    }
                }
            }
        }
    }
    returnView.frame = CGRectMake(15.0f,1.0f, MAX_WIDTH+10, Y+35); //@ 需要将该view的尺寸记下，方便以后使用
    //    NSLog(@"%.1f %.1f", X, Y);
    return returnView;
}



UIActivityIndicatorView *bubbleIndicatorView;
UIImageView *errorImage;

+ (UIView *)bubbleView:(NSString *)text from:(BOOL)fromSelf {

    // build single chat bubble cell with given text
    UIView *returnView =  [self assembleMessageAtIndex:text from:fromSelf];
    returnView.backgroundColor = [UIColor clearColor];
    UIView *cellView = [[UIView alloc] initWithFrame:CGRectZero];
    cellView.backgroundColor = [UIColor clearColor];
    
    NSString *imgPath=[[NSBundle mainBundle] pathForResource:fromSelf?@"TKCommonLib.bundle/BubbleView/bubble-myself-bg":@"TKCommonLib.bundle/BubbleView/bubble-other-bg" ofType:@"png"];
    UIImage *bubble = [UIImage imageWithContentsOfFile:imgPath];
	UIImageView *bubbleImageView = [[UIImageView alloc] initWithImage:[bubble stretchableImageWithLeftCapWidth:20 topCapHeight:26]];
    
    TKAsynImageView *headImageView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(24, 12, 23, 23)];
    headImageView.tag = 1202;
    headImageView.adjustsImageWhenHighlighted = NO;
    
    bubbleIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    bubbleIndicatorView.backgroundColor = [UIColor clearColor];
    errorImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TKCommonLib.bundle/BubbleView/bubble_error.png"]];
    
    if(fromSelf){
        headImageView.defaultImage = [UIImage imageNamed:@"TKCommonLib.bundle/BubbleView/face-myself.png"];
        returnView.frame= CGRectMake(29.0f, 12.0f, returnView.frame.size.width, returnView.frame.size.height);
        bubbleImageView.frame = CGRectMake(20.0f, 10.0f, returnView.frame.size.width+24.0f, returnView.frame.size.height+24.0f );
        bubbleIndicatorView.frame = CGRectMake(4.0f, CGRectGetMidY(bubbleImageView.frame)-4.0f, 8, 8);
        errorImage.frame = CGRectMake(.0f, CGRectGetMidY(bubbleImageView.frame)-8.0f, 16, 16);
        cellView.frame = CGRectMake(260.0f-bubbleImageView.frame.size.width-20.0f, 0.0f,bubbleImageView.frame.size.width+60.0f+20.0f, bubbleImageView.frame.size.height+30.0f);
        headImageView.frame = CGRectMake(bubbleImageView.frame.size.width + 30.0f, 10.0f, 40.0f, 40.0f);
        
        
        [cellView addSubview:bubbleIndicatorView];
        [bubbleIndicatorView startAnimating];
        [cellView addSubview:errorImage];
        errorImage.hidden = YES;
        [bubbleIndicatorView release];
        [errorImage release];
    }
	else{
        headImageView.defaultImage = [UIImage imageNamed:@"TKCommonLib.bundle/BubbleView/face-other.png"];
        returnView.frame= CGRectMake(75.0f, 12.0f, returnView.frame.size.width, returnView.frame.size.height);
        bubbleImageView.frame = CGRectMake(60.0f, 10.0f, returnView.frame.size.width+24.0f, returnView.frame.size.height+24.0f);
		cellView.frame = CGRectMake(0.0f, 0.0f, CGRectGetMaxX(bubbleImageView.frame),bubbleImageView.frame.size.height+30.0f);
        headImageView.frame = CGRectMake(10.0f, 10.0f, 40.0f, 40.0f);
    }
    
    
    [cellView addSubview:bubbleImageView];
    [cellView addSubview:headImageView];
    [cellView addSubview:returnView];
    [bubbleImageView release];
    [returnView release];
    [headImageView release];
	return [cellView autorelease];
    
}

+ (void)bubleViewIndicatorOK
{
    [bubbleIndicatorView stopAnimating];
}

+ (void)bubleViewIndicatorError
{
    [bubbleIndicatorView stopAnimating];
    errorImage.hidden = NO;
}
@end
