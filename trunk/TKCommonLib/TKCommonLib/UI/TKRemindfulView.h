//
//  TKRemindfulView.h
//  
//
//  Created by Jey on 11-9-18.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TKRemindfulView : UIWindow {
    NSString *_text;
    UILabel *_textLabel;
    UIActivityIndicatorView *_indicatorView;
    BOOL show;
}

@property (nonatomic, retain) NSString *text;
@property (nonatomic, readonly, getter = isShow) BOOL show;
@property (nonatomic, readonly, assign) UILabel *textLabel;
@property (nonatomic, readonly, assign) UIActivityIndicatorView *indicatorView;

- (void)show:(BOOL)animated;
- (void)hide:(BOOL)animated;
@end
