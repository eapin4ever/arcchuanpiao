//
//  CarouselView.m
//  TKCommonLib
//
//  Created by xuhuijun on 13-7-24.
//
//

#import "CarouselView.h"
#import "UIImageView+WebCache.h"
#define maxItems 3

@implementation CarouselView

@synthesize carouselDelegate = _carouselDelegate;
@synthesize carouselDataSource = _carouselDataSource;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];
        _scrollView = [[UIScrollView alloc] initWithFrame:frame];
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        [self addSubview:_scrollView];
        
        UIImageView *titleView = [[[UIImageView alloc] initWithFrame:CGRectMake( 0,frame.size.height - 30, frame.size.width, 30)] autorelease];
        titleView.image = [UIImage imageNamed:@"TKCommonLib.bundle/CarouselView/CarouselView_scroll_txt_bg.png"];
        titleView.opaque = NO;
        [self addSubview:titleView];
        
        _describeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, titleView.frame.size.width-80, titleView.frame.size.height)];
        _describeLabel.text = @"";
        _describeLabel.backgroundColor = [UIColor clearColor];
        _describeLabel.font = [UIFont systemFontOfSize:13];
        _describeLabel.textColor = [UIColor whiteColor];
        [titleView addSubview:_describeLabel];
        
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_describeLabel.frame), 0, 80, titleView.frame.size.height)];
        _pageControl.numberOfPages = 0;
        _pageControl.backgroundColor = [UIColor clearColor];
        _pageControl.userInteractionEnabled = NO;
        [titleView addSubview:_pageControl];
        
        _curPage = 0;//轮播图得第一张
        
        _carouselTimer = [[NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(pageTurn) userInfo:nil repeats:YES] retain];
        
    }
    return self;
}

- (NSInteger)validPageValue:(NSInteger)value {
    
    if(value == -1) value = _totalPages - 1; //最后一也
    if(value == _totalPages) value = 0; //第一页
    //中间页
    
    return value;
}

- (void)getDisplayImagesWithCurpage:(NSInteger)page {
    
    NSInteger pre = [self validPageValue:_curPage-1];
    NSInteger last = [self validPageValue:_curPage+1];
    
    if (!_curSource) {
        _curSource = [[NSMutableArray alloc] init];
    }
    [_curSource removeAllObjects];
    
    id d = [self.carouselDataSource carouselView:self infoForViewAtIndex:pre];
    [_curSource addObject:d];
    [_curSource addObject:[self.carouselDataSource carouselView:self infoForViewAtIndex:_curPage]];
    [_curSource addObject:[self.carouselDataSource carouselView:self infoForViewAtIndex:last]];
}


- (void)loadData
{
    
    _pageControl.currentPage = _curPage;
    
    if(_scrollView.subviews && _scrollView.subviews.count > 0) {
		[_scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    [self getDisplayImagesWithCurpage:_curPage];
    
    for (int i = 0; i < 3; i ++) {
        
        NSDictionary *dataDic = [_curSource objectAtIndex:i];
        
        UIImageView *asynImageView = [[[UIImageView alloc] initWithFrame:_scrollView.frame] autorelease];
        [asynImageView setImageWithURL:[NSURL URLWithString:[dataDic safeObjectForKey:@"carouselurl"]] placeholderImage:[UIImage imageNamed:@"TKCommonLib.bundle/CarouselView/CarouselView_loadingImg_320_106.png"]];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        asynImageView.userInteractionEnabled = YES;
        _scrollView.userInteractionEnabled = YES;
        [asynImageView addGestureRecognizer:tap];
        asynImageView.frame = CGRectOffset(asynImageView.frame, asynImageView.frame.size.width * i, 0);
        [_scrollView addSubview:asynImageView];
    }
    
    NSDictionary *dataDic = [self.carouselDataSource carouselView:self infoForViewAtIndex:_pageControl.currentPage];
    _describeLabel.text = [dataDic objectForKey:@"title"];
    
    [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width, 0)];
    
}


- (void)goToDetail:(UITapGestureRecognizer *)tap
{
    if ([_carouselDelegate respondsToSelector:@selector(carouselView:infoForViewAtIndex:)]) {
        [_carouselDelegate carouselView:self didSelectedViewAtIndex:_pageControl.currentPage];
    }
}

- (void)reloadData
{
    _totalPages = [_carouselDataSource numberOfViewsInCarouselView:self];
    if (_totalPages == 0) {
        return;
    }
    _curPage = 0;
    if (_totalPages > maxItems) {
        _totalPages = maxItems;
    }
    if (_totalPages != 1) {
        _pageControl.numberOfPages = _totalPages;
    }
    _scrollView.contentSize = CGSizeMake((self.frame.size.width) * 3, self.frame.size.height);
    
    [self loadData];
}


#pragma mark - TKAsynImageViewDelegate

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{
    //根据_pageControl.currentPage 打开响应的详细页
    
    if ([_carouselDelegate respondsToSelector:@selector(carouselView:infoForViewAtIndex:)]) {
        [_carouselDelegate carouselView:self didSelectedViewAtIndex:_pageControl.currentPage];
    }
}

#pragma mark - UIScrollViewDelegate


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [scrollView setContentOffset:CGPointMake(scrollView.frame.size.width, 0) animated:YES];
}


- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
    int x = aScrollView.contentOffset.x;
    if(x >= (2*aScrollView.frame.size.width)) {
        _curPage = [self validPageValue:_curPage+1];
        [self loadData];
    }
    if(x <= 0) {
        _curPage = [self validPageValue:_curPage-1];
        [self loadData];
    }
    
}

//当页数变化时，改变scrollView的内容大小
- (void) pageTurn
{
    NSDictionary *dataDic = [self.carouselDataSource carouselView:self infoForViewAtIndex:_pageControl.currentPage];
    _describeLabel.text = [dataDic objectForKey:@"title"];
    
    if (_carouselDataSource == nil) {
        return;
    }
    [_scrollView setContentOffset:CGPointMake((_scrollView.frame.size.width) * 2, 0.0f) animated:YES];
    
}

- (void)carouselViewStopAnimation
{
    if ([_carouselTimer isValid]) {
        [_carouselTimer invalidate];
    }
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    UIImage *image = [UIImage imageNamed:@"TKCommonLib.bundle/CarouselView/CarouselView_scroll_default_img.png"];
    [image drawInRect:rect];
}

- (void)dealloc
{
    [_carouselTimer invalidate];
    [_carouselTimer release];
    [_describeLabel release];
    [_scrollView release];
    [_pageControl release];
    [super dealloc];
}
@end
