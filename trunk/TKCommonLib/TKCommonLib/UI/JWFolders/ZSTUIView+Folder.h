//
//  UIView+Folder.h
//  F3
//
//  Created by luobin on 12-9-27.
//
//

#import <UIKit/UIKit.h>
#import "ZSTJWFolders.h"

@interface UIView (Folder)

- (void)openFolderWithContentView:(UIView *)view
                         position:(CGPoint)position
                        openBlock:(JWFoldersOpenBlock)openBlock
                       closeBlock:(JWFoldersCloseBlock)closeBlock
                  completionBlock:(JWFoldersCompletionBlock)completionBlock;

@end

@interface UITableView (Folder)

- (void)openFolderWithContentView:(UIView *)view
                        openBlock:(JWFoldersOpenBlock)openBlock
                       closeBlock:(JWFoldersCloseBlock)closeBlock
                  completionBlock:(JWFoldersCompletionBlock)completionBlock;

- (void)openFollowWithContentView:(UIView *)view
                        openBlock:(JWFoldersOpenBlock)openBlock
                       closeBlock:(JWFoldersCloseBlock)closeBlock
                  completionBlock:(JWFoldersCompletionBlock)completionBlock;

@end