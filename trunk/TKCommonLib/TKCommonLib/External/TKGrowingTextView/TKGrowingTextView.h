//
//  TKGrowingTextView.h


#import <UIKit/UIKit.h>

@class TKGrowingTextView;
@class TKTextViewInternal;

@protocol TKGrowingTextViewDelegate

@optional
- (BOOL)growingTextViewShouldBeginEditing:(TKGrowingTextView *)growingTextView;
- (BOOL)growingTextViewShouldEndEditing:(TKGrowingTextView *)growingTextView;

- (void)growingTextViewDidBeginEditing:(TKGrowingTextView *)growingTextView;
- (void)growingTextViewDidEndEditing:(TKGrowingTextView *)growingTextView;

- (BOOL)growingTextView:(TKGrowingTextView *)growingTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text;
- (void)growingTextViewDidChange:(TKGrowingTextView *)growingTextView;

- (void)growingTextView:(TKGrowingTextView *)growingTextView willChangeHeight:(float)height;
- (void)growingTextView:(TKGrowingTextView *)growingTextView didChangeHeight:(float)height;

- (void)growingTextViewDidChangeSelection:(TKGrowingTextView *)growingTextView;
- (BOOL)growingTextViewShouldReturn:(TKGrowingTextView *)growingTextView;
@end

@interface TKGrowingTextView : UIView <UITextViewDelegate> {
    TKTextViewInternal *internalTextView;    
    
    int minHeight;
    int maxHeight;
    
    //class properties
    int maxNumberOfLines;
    int minNumberOfLines;
    
    BOOL animateHeightChange;
    
    //uitextview properties
    NSObject <TKGrowingTextViewDelegate> *delegate;
    NSString *text;
    UIFont *font;
    UIColor *textColor;
    NSTextAlignment textAlignment; 
    NSRange selectedRange;
    BOOL editable;
    UIDataDetectorTypes dataDetectorTypes;
    UIReturnKeyType returnKeyType;
    
    UIEdgeInsets contentInset;
}

//real class properties
@property int maxNumberOfLines;
@property int minNumberOfLines;
@property BOOL animateHeightChange;
@property (retain) UITextView *internalTextView;  



//uitextview properties
@property(assign) NSObject<TKGrowingTextViewDelegate> *delegate;
@property(nonatomic,assign) NSString *text;
@property(nonatomic,assign) UIFont *font;
@property(nonatomic,assign) UIColor *textColor;
@property(nonatomic) NSTextAlignment textAlignment;    // default is NSTextAlignmentLeft
@property(nonatomic) NSRange selectedRange;            // only ranges of length 0 are supported
@property(nonatomic,getter=isEditable) BOOL editable;
@property(nonatomic) UIDataDetectorTypes dataDetectorTypes __OSX_AVAILABLE_STARTING(__MAC_NA, __IPHONE_3_0);
@property (nonatomic) UIReturnKeyType returnKeyType;
@property (assign) UIEdgeInsets contentInset;

//uitextview methods
//need others? use .internalTextView
- (BOOL)becomeFirstResponder;
- (BOOL)resignFirstResponder;

- (BOOL)hasText;
- (void)scrollRangeToVisible:(NSRange)range;

- (void)setPlaceholder:(NSString *)placeholder;
- (void)setbackgroudImage;

@end
