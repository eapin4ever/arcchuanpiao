/*
 * This file is part of the SDWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import "SDWebImageCompat.h"
#import "SDWebFileDownloaderDelegate.h"
#import "SDWebFileManagerDelegate.h"
#import "SDFileCacheDelegate.h"

typedef enum
{
    SDWebImageRetryFailed = 1 << 0,
    SDWebImageLowPriority = 1 << 1,
    SDWebImageCacheMemoryOnly = 1 << 2
} SDWebImageOptions;

@interface SDWebFileManager : NSObject <SDWebFileDownloaderDelegate, SDFileCacheDelegate>
{
    NSMutableArray *downloadDelegates;
    NSMutableArray *downloaders;
    NSMutableArray *cacheDelegates;
    NSMutableArray *cacheURLs;
    NSMutableDictionary *downloaderForURL;
    NSMutableArray *failedURLs;
    //    NSString *filePathString;
}
//@property (nonatomic, retain) NSString *filePathString;

+ (id)sharedManager;
- (NSData *)fileDataWithURL:(NSURL *)url;
- (void)downloadWithURL:(NSURL *)url delegate:(id<SDWebFileManagerDelegate>)delegate withFilePath:(NSString *)filePath;
- (void)downloadWithURL:(NSURL *)url delegate:(id<SDWebFileManagerDelegate>)delegate options:(SDWebImageOptions)options withFilePath:(NSString *)filePath;
- (void)downloadWithURL:(NSURL *)url delegate:(id<SDWebFileManagerDelegate>)delegate withFilePath:(NSString *)filePath retryFailed:(BOOL)retryFailed __attribute__ ((deprecated)); // use options:SDWebImageRetryFailed instead
- (void)downloadWithURL:(NSURL *)url delegate:(id<SDWebFileManagerDelegate>)delegate retryFailed:(BOOL)retryFailed withFilePath:(NSString *)filePath lowPriority:(BOOL)lowPriority __attribute__ ((deprecated)); // use options:SDWebImageRetryFailed|SDWebImageLowPriority instead
#if NS_BLOCKS_AVAILABLE
- (void)downloadWithURL:(NSURL *)url delegate:(id)delegate options:(SDWebImageOptions)options success:(void (^)(NSData *data))success failure:(void (^)(NSError *error))failure withFilePath:(NSString *)filePath;
#endif
- (void)cancelForDelegate:(id<SDWebFileManagerDelegate>)delegate;

@end
