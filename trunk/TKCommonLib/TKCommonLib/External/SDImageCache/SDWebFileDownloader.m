/*
 * This file is part of the SDWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import "SDWebFileDownloader.h"

NSString *const SDWebFileDownloadStartNotification = @"SDWebFileDownloadStartNotification";
NSString *const SDWebFileDownloadStopNotification = @"SDWebFileDownloadStopNotification";

@interface SDWebFileDownloader ()
@property (nonatomic, retain) NSURLConnection *connection;
@end

@implementation SDWebFileDownloader
@synthesize url, delegate, connection, fileData, userInfo, lowPriority;

#pragma mark Public Methods

+ (id)downloaderWithURL:(NSURL *)url delegate:(id<SDWebFileDownloaderDelegate>)delegate
{
    return [self downloaderWithURL:url delegate:delegate userInfo:nil];
}

+ (id)downloaderWithURL:(NSURL *)url delegate:(id<SDWebFileDownloaderDelegate>)delegate userInfo:(id)userInfo
{
    
    return [self downloaderWithURL:url delegate:delegate userInfo:userInfo lowPriority:NO];
}

+ (id)downloaderWithURL:(NSURL *)url delegate:(id<SDWebFileDownloaderDelegate>)delegate userInfo:(id)userInfo lowPriority:(BOOL)lowPriority
{
    // Bind SDNetworkActivityIndicator if available (download it here: http://github.com/rs/SDNetworkActivityIndicator )
    // To use it, just add #import "SDNetworkActivityIndicator.h" in addition to the SDWebImage import
    if (NSClassFromString(@"SDNetworkActivityIndicator"))
    {
        
        id activityIndicator = [NSClassFromString(@"SDNetworkActivityIndicator") performSelector:NSSelectorFromString(@"sharedActivityIndicator")];
        
        // Remove observer in case it was previously added.
        [[NSNotificationCenter defaultCenter] removeObserver:activityIndicator name:SDWebFileDownloadStartNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:activityIndicator name:SDWebFileDownloadStopNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:activityIndicator
                                                 selector:NSSelectorFromString(@"startActivity")
                                                     name:SDWebFileDownloadStartNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:activityIndicator
                                                 selector:NSSelectorFromString(@"stopActivity")
                                                     name:SDWebFileDownloadStopNotification object:nil];
    }
    
    SDWebFileDownloader *downloader = SDWIReturnAutoreleased([[SDWebFileDownloader alloc] init]);
    downloader.url = url;
    downloader.delegate = delegate;
    downloader.userInfo = userInfo;
    downloader.lowPriority = lowPriority;
    [downloader performSelectorOnMainThread:@selector(start) withObject:nil waitUntilDone:YES];
    return downloader;
}

+ (void)setMaxConcurrentDownloads:(NSUInteger)max
{
    // NOOP
}

- (void)start
{
    // In order to prevent from potential duplicate caching (NSURLCache + SDImageCache) we disable the cache for image requests
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:15];
    self.connection = SDWIReturnAutoreleased([[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:NO]);
    
    // If not in low priority mode, ensure we aren't blocked by UI manipulations (default runloop mode for NSURLConnection is NSEventTrackingRunLoopMode)
    if (!lowPriority)
    {
        [connection scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
    }
    [connection start];
    SDWIRelease(request);
    
    if (connection)
    {
        self.fileData = [NSMutableData data];
        [[NSNotificationCenter defaultCenter] postNotificationName:SDWebFileDownloadStartNotification object:nil];
    }
    else
    {
        if ([delegate respondsToSelector:@selector(fileDownloader:didFailWithError:)])
        {
            [delegate performSelector:@selector(fileDownloader:didFailWithError:) withObject:self withObject:nil];
        }
    }
}

- (void)cancel
{
    if (connection)
    {
        [connection cancel];
        self.connection = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:SDWebFileDownloadStopNotification object:nil];
    }
}

#pragma mark NSURLConnection (delegate)

- (void)connection:(NSURLConnection *)aConnection didReceiveResponse:(NSURLResponse *)response
{
    if ([((NSHTTPURLResponse *)response) statusCode] >= 400)
    {
        [aConnection cancel];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:SDWebFileDownloadStopNotification object:nil];
        
        if ([delegate respondsToSelector:@selector(fileDownloader:didFailWithError:)])
        {
            NSError *error = [[NSError alloc] initWithDomain:NSURLErrorDomain
                                                        code:[((NSHTTPURLResponse *)response) statusCode]
                                                    userInfo:nil];
            [delegate performSelector:@selector(fileDownloader:didFailWithError:) withObject:self withObject:error];
            SDWIRelease(error);
        }
        
        self.connection = nil;
        self.fileData = nil;
    }
}

- (void)connection:(NSURLConnection *)aConnection didReceiveData:(NSData *)data
{
    [fileData appendData:data];
}

#pragma GCC diagnostic ignored "-Wundeclared-selector"
- (void)connectionDidFinishLoading:(NSURLConnection *)aConnection
{
    self.connection = nil;
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SDWebFileDownloadStopNotification object:nil];
    
    if ([delegate respondsToSelector:@selector(fileDownloaderDidFinish:)])
    {
        [delegate performSelector:@selector(fileDownloaderDidFinish:) withObject:self];
    }
    
    if ([delegate respondsToSelector:@selector(fileDownloader:didFinishWithFile:)])
    {
        
        [delegate performSelector:@selector(fileDownloader:didFinishWithFile:) withObject:self withObject:fileData ];
        
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [[NSNotificationCenter defaultCenter] postNotificationName:SDWebFileDownloadStopNotification object:nil];
    
    if ([delegate respondsToSelector:@selector(fileDownloader:didFailWithError:)])
    {
        [delegate performSelector:@selector(fileDownloader:didFailWithError:) withObject:self withObject:error];
    }
    
    self.connection = nil;
    self.fileData = nil;
}

#pragma mark NSObject

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    SDWISafeRelease(url);
    SDWISafeRelease(connection);
    SDWISafeRelease(fileData);
    SDWISafeRelease(userInfo);
    SDWISuperDealoc;
}


@end
