//
//  ZSTWeiBoInfo.m
//  F3
//
//  Created by xuhuijun on 12-6-11.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTWeiBoInfo.h"

@implementation ZSTWeiBoInfo
@synthesize avatarString;
@synthesize name;
@synthesize time;
@synthesize content;
@synthesize thumbnail_pic;
@synthesize bmiddle_pic;
@synthesize original_pic;
@synthesize Platform;
@synthesize forwardCount;
@synthesize commentCount;
@synthesize attachmentInfo;
@synthesize MID;
@synthesize ID;
@synthesize bisforward;
@synthesize boriginalcontent;
@synthesize type;


- (void)dealloc
{
    self.avatarString = nil;
    self.name = nil;
    self.time = nil;
    self.content = nil;
    self.thumbnail_pic = nil;
    self.bmiddle_pic = nil;
    self.original_pic = nil;
    self.Platform = nil;
    self.forwardCount = nil;
    self.commentCount = nil;
    self.attachmentInfo = nil;
    self.boriginalcontent = nil;
    self.type = nil;
    [super dealloc];
}
@end
