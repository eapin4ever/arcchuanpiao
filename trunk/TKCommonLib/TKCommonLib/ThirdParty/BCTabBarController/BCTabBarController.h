#import "BCTabBar.h"

#define kBCTabBarControllerKey      @"kBCTabBarControllerKey"


@class BCTabBarView;
@class BCTabBarController;

@protocol BCTabBarControllerDelegate <NSObject>

- (void)tabbarController:(BCTabBarController*)controller didSelectTabAtIndex:(NSInteger)index;

@end

@interface BCTabBarController : UIViewController <BCTabBarDelegate,UINavigationControllerDelegate>

@property (nonatomic, retain) NSArray *viewControllers;
@property (nonatomic, retain) BCTabBar *tabBar;
@property (nonatomic, retain) UIViewController *selectedViewController;
@property (nonatomic, retain) BCTabBarView *tabBarView;
@property (nonatomic) NSUInteger selectedIndex;
@property (nonatomic, readonly) BOOL visible;
@property (assign, nonatomic) id<BCTabBarControllerDelegate> mDelegate;

- (void)replaceControllerAtIndex:(int)index withController:(id)controller;

@end
