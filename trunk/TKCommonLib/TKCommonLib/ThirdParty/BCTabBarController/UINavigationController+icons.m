#import "UINavigationController+icons.h"


@implementation UINavigationController (BCTabBarController)

- (NSString *)iconImageName {
    if (!self.viewControllers || self.viewControllers.count <= 0) {
        
        return nil;
    }
	return [[self.viewControllers objectAtIndex:0] iconImageName];
}

- (NSString*)tabTitle {
    
    if (!self.viewControllers || self.viewControllers.count <= 0) {
        
        return nil;
    }
    return [[self.viewControllers objectAtIndex:0] tabTitle];
}

- (NSString*)titleColor {
    
    if (!self.viewControllers || self.viewControllers.count <= 0) {
        
        return nil;
    }
    return [[self.viewControllers objectAtIndex:0] titleColor];
}

@end
