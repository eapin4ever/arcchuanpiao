

@interface UIViewController (BCTabBarController)

- (NSString *)iconImageName;
- (NSString *)tabTitle;
- (NSString *)titleColor;

@end
