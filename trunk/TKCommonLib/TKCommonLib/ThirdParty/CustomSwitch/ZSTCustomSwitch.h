//
//  CustomSwitch.h
//  InfantCloud
//
//  Created by LiZhenQu on 14-7-17.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, ZSTCustomSwitchStatus)
{
    ZSTCustomSwitchStatusOn = 0,//开启
    ZSTCustomSwitchStatusOff = 1//关闭
};

typedef NS_ENUM(NSUInteger, ZSTCustomSwitchArrange)
{
    ZSTCustomSwitchArrangeONLeftOFFRight = 0,//左边是开启,右边是关闭，默认
    ZSTCustomSwitchArrangeOFFLeftONRight = 1//左边是关闭，右边是开启
};

@protocol ZSTCustomSwitchDelegate <NSObject>

-(void)zstcustomSwitchSetStatus:(ZSTCustomSwitchStatus)status;
@end

@interface ZSTCustomSwitch : UIControl
{
    UIImage *_onImage;
    UIImage *_offImage;
    id<ZSTCustomSwitchDelegate> _delegate;
    ZSTCustomSwitchArrange _arrange;
    
}
@property(nonatomic,retain) UIImage *onImage;
@property(nonatomic,retain) UIImage *offImage;
@property(nonatomic,retain) IBOutlet id<ZSTCustomSwitchDelegate> delegate;
@property(nonatomic) ZSTCustomSwitchArrange arrange;
@property(nonatomic) ZSTCustomSwitchStatus status;
@end