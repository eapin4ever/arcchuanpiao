//
//  ZSTShellGRootViewController.m
//  ShellG
//
//  Created by zhangwanqiang on 14-5-7.
//  Copyright (c) 2014年 zhangwanqiang. All rights reserved.
//

#import "ZSTShellGRootViewController.h"
#import "ZSTLogUtil.h"
#import "ZSTUtils.h"
#import "ZSTShell.h"
#import "ZSTGlobal+F3.h"

@interface ZSTShellGRootViewController ()

@end

@implementation ZSTShellGRootViewController



@synthesize application;
@synthesize navigationController;
@synthesize rootView;

//解决ios4.1下 viewWillAppear失效问题
#pragma mark ----------UINavigationControllerDelegate--------

//- (void )navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL )animated
//{
//	[viewController viewWillAppear:animated];
//}
//
//- (void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
//	[viewController viewDidAppear:animated];
//}

- (void)setSelectedViewController:(UIViewController *)selectedViewController
{
    if (selectedViewController != self.selectedViewController) {
        CATransition* animation = [CATransition animation];
        [animation setDuration:0.3f];
        [animation setType:kCATransitionFade];
        [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [[self.view layer] addAnimation:animation forKey:@"fadeView"];
    }
    [super setSelectedViewController:selectedViewController];
}

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(pushBMessageAction:)
                                                 name: NotificationName_PushBMessage
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(pushViewController)
                                                 name: NotificationName_PushViewController
                                               object: nil];
    
    
    //设置tabbar背景图片
    UIImage *image = [[UIImage imageNamed:@"framework_bottom_bg.png"] stretchableImageWithLeftCapWidth:0 topCapHeight:25];
    
    NSString *minimumSystemVersion = @"4.0";
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    if ([systemVersion compare:minimumSystemVersion options:NSNumericSearch] != NSOrderedAscending)
    {
        [[self tabBar] setBackgroundImage:image];
    }else{
        self.tabBar.layer.contents = (id)image.CGImage;
        
    }
    
    self.delegate = self;
    //Tabbar配置信息从config.xml加载
    NSArray *viewControllers = [ZSTShell viewControllerForShellGTabBarController];
    for (UINavigationController *navController in viewControllers) {
        navController.delegate = self;
    }
    self.viewControllers = viewControllers;
    
    //设置默认选中
    NSUInteger selectedIndex = [NSLocalizedString(@"GP_DefaultBottomTabIndex", @"1") intValue];
    if (selectedIndex < 1 && selectedIndex > 3) {
        selectedIndex = 1;
    }
    self.selectedIndex = selectedIndex - 1;
}

- (BOOL)tabBarController:(UITabBarController *)tbc shouldSelectViewController:(UIViewController *)vc {
    UIViewController *tbSelectedController = tbc.selectedViewController;
    if ([tbSelectedController isEqual:vc]) {
        return NO;
    }
    return YES;
}

- (void)pushBMessageAction:(NSNotification *)noti
{
    NSDictionary *dic = [noti object];
    self.selectedIndex = [[dic objectForKey:@"selectIndex"] intValue];
}

- (void)pushViewController
{
    ZSTF3Preferences *per = [ZSTF3Preferences shared];
    UIViewController *controller = per.pushViewController;
    controller.hidesBottomBarWhenPushed = YES;
    [((UINavigationController *)self.selectedViewController) pushViewController:controller animated:YES];
    controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    per.isInPush = YES;
    
}

- (void)popViewController {
    
    ZSTF3Preferences *per = [ZSTF3Preferences shared];
    per.isInPush = NO;
    [((UINavigationController *)self.selectedViewController) popViewControllerAnimated:YES];
    
}
-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return (1 << UIInterfaceOrientationPortrait);
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIViewController *)rootViewController;
{
    return self;
}

- (void)dealloc
{
	[super dealloc];
}

@end

