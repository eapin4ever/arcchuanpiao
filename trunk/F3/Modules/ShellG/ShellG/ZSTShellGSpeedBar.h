//
//  ZSTShellGSpeedBar.h
//  ShellG
//
//  Created by xuhuijun on 13-1-10.
//
//

#import <UIKit/UIKit.h>

@class ZSTShellGSpeedBar;

@protocol ZSTShellGSpeedBarDelegate <NSObject>

- (void)zstShellGSpeedBar:(ZSTShellGSpeedBar *)speedBar withParam:(NSDictionary *)param;

@end


@interface ZSTShellGSpeedBar : UIView

@property(strong)UILabel *titleLabel;
@property(strong)UIButton *speedBtn;
@property(strong,nonatomic)NSDictionary *moduleParams;


@property(strong)id<ZSTShellGSpeedBarDelegate>delegate;

- (void)configSpeedBarNormalImage:(UIImage *)normalImage
                    selectedImage:(UIImage *)selectedImage
                            param:(NSDictionary *)param;

- (void)speedBarInerSpace:(float) space;

@end
