//
//  ZSTF3Engine+ShellG.m
//  ShellG
//
//  Created by xuhuijun on 13-1-11.
//
//

#import "ZSTF3Engine+ShellG.h"
#import "ZSTCommunicator.h"
#import "ZSTGlobal+ShellG.h"

@implementation ZSTF3Engine (ShellG)


- (void)getShellGAD
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    [params setSafeObject:[NSString stringWithFormat:@"%ld",[ZSTF3Preferences shared].shellId] forKey:@"moduleid"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:GetShellGAD
                                         params:params
                                         target:self
                                       selector:@selector(getShellGADResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:YES];
}


- (void)getShellGADResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id ShellGList = [response.jsonResponse safeObjectForKey:@"info"];
        if (![ShellGList isKindOfClass:[NSArray class]]) {
            ShellGList = [NSArray array];
        }
        if ([self isValidDelegateForSelector:@selector(getShellGADDidSucceed:)]) {
            [self.delegate getShellGADDidSucceed:ShellGList];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getShellGADDidFailed:)]) {
            [self.delegate getShellGADDidFailed:response.resultCode];
        }
    }
    
}

@end
