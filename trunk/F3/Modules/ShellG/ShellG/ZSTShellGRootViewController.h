//
//  ZSTShellGRootViewController.h
//  ShellG
//
//  Created by zhangwanqiang on 14-5-7.
//  Copyright (c) 2014年 zhangwanqiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTModule.h"
#import "ZSTModuleDelegate.h"
#import "ZSTDao.h"

@interface ZSTShellGRootViewController : UITabBarController <UINavigationControllerDelegate,UITabBarControllerDelegate,ZSTModuleDelegate>
@property (nonatomic, retain) UINavigationController *navigationController;
@end
