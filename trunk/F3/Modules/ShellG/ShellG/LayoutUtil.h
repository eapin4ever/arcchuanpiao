//
//  LayoutUtil.h
//  ShellG
//
//  Created by zhangwanqiang on 14-5-8.
//  Copyright (c) 2014年 zhangwanqiang. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    Image_0,
    Image_1,
    Image_2,
    Image_3,
    Image_4
} imageID;

//设置数据结构
@interface MyPageSetting : NSObject
@property (nonatomic,assign) float leftSeparation;
@property (nonatomic,assign) float topSeparation;
@property (nonatomic,assign) float widthSeparation;
@property (nonatomic,assign) float hightSeparation;

@end


//frame数据结构
@interface MyFrame : NSObject
@property (nonatomic,assign) float pointX;
@property (nonatomic,assign) float pointY;
@property (nonatomic,assign) float width;
@property (nonatomic,assign) float height;

@end


@interface LayoutUtil : NSObject
@property (nonatomic,retain)NSMutableArray * frameArray;
@property (nonatomic,retain)MyPageSetting * pageSettings;
@property (nonatomic,assign)float  pageHight;
+(LayoutUtil *) shareLayoutUtil;
@end




