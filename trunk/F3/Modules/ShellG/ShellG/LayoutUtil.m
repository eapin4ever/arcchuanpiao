//
//  LayoutUtil.m
//  ShellG
//
//  Created by zhangwanqiang on 14-5-8.
//  Copyright (c) 2014年 zhangwanqiang. All rights reserved.
//

#import "LayoutUtil.h"
#import "ElementParser.h"
@implementation LayoutUtil
-(id)init
{
    self = [super init];
    if (self) {
        _frameArray = [[NSMutableArray alloc]init];
        [self calculateFrames];
    }
    return self;
}
-(void)calculateFrames
{
    
    float currPointX = 0.0f;
    float currPointY = 0.0f;
    float preWidth = 0.0f;
    float preHight = 0.0f;
    float orgionalX = 0.0f;
    float orgionalY = 0.0f;
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Manifest" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    
    NSArray * settings = [root selectElements:@"PageModel Settings"];
    Element * settingElement = [settings objectAtIndex:0];
    if (settingElement) {
        _pageSettings = [[MyPageSetting alloc]init];
        _pageSettings.leftSeparation = [[settingElement selectElement:@"LeftSeparation"] contentsNumber].floatValue;
        _pageSettings.topSeparation = [[settingElement selectElement:@"TopSeparation"] contentsNumber].floatValue;
        _pageSettings.widthSeparation = [[settingElement selectElement:@"WidthSepartion"] contentsNumber].floatValue;
        _pageSettings.hightSeparation = [[settingElement selectElement:@"HightSeparthon"] contentsNumber].floatValue;
        
        #ifdef DEBUG
         NSLog(@"Manifest --  left = %.2f --  top = %.2f  --  inerwidth = %.2f  --  inerhight = %.2f  --",_pageSettings.leftSeparation,_pageSettings.topSeparation,_pageSettings.widthSeparation,_pageSettings.hightSeparation);
        #endif
    }
    int i = 0;
    NSArray *itemElements = [root selectElements:@"PageModel Item"];
    for (Element * el in itemElements) {
        MyFrame * frame = [[MyFrame alloc] init];
        switch (i) {
            case Image_0:
            {
                frame.pointX = [[el selectElement:@"PointX"] contentsNumber].floatValue;
                frame.pointY = [[el selectElement:@"PointY"] contentsNumber].floatValue;
                frame.width = [[el selectElement:@"Width"] contentsNumber].floatValue;
                frame.height = [[el selectElement:@"Hight"] contentsNumber].floatValue;
                currPointX = frame.pointX + frame.width + _pageSettings.widthSeparation;
                currPointY = frame.pointY;
                orgionalX = frame.pointX;
                orgionalY = frame.pointY;
                preWidth = frame.width;
                preHight = frame.height;
                _pageHight = _pageSettings.topSeparation + _pageSettings.hightSeparation * 2 + 3 * frame.height;
            }break;
            case Image_1:
            {
                frame.pointX = currPointX;
                frame.pointY = currPointY;
                frame.width = [[el selectElement:@"Width"] contentsNumber].floatValue;
                frame.height = [[el selectElement:@"Hight"] contentsNumber].floatValue;
                preWidth = frame.width;
                preHight = frame.height;
            }break;
            case Image_2:
            {
                frame.pointX = orgionalX;
                frame.pointY = orgionalY + preHight + _pageSettings.hightSeparation;
                frame.width = [[el selectElement:@"Width"] contentsNumber].floatValue;
                frame.height = [[el selectElement:@"Hight"] contentsNumber].floatValue;
                preWidth = frame.width;
                preHight = frame.height;
                currPointX = frame.width;
                currPointY = frame.pointY + frame.height + _pageSettings.hightSeparation;
            }break;
            case Image_3:
            {
                frame.pointX = orgionalX;
                frame.pointY = currPointY;
                frame.width = [[el selectElement:@"Width"] contentsNumber].floatValue;
                frame.height = [[el selectElement:@"Hight"] contentsNumber].floatValue;
                preWidth = frame.width;
                preHight = frame.height;
                currPointX = frame.pointX + _pageSettings.widthSeparation + frame.width;
                currPointY = frame.pointY;

            }break;
            case Image_4:
            {
                frame.pointX = currPointX;
                frame.pointY = currPointY;
                frame.width = [[el selectElement:@"Width"] contentsNumber].floatValue;
                frame.height = [[el selectElement:@"Hight"] contentsNumber].floatValue;

            }break;

                
            default:
                break;
        }
        [_frameArray addObject:frame];
        [frame release];
        
    #ifdef DEBUG
        NSLog(@"Manifest --  x = %.2f --  y = %.2f  --  w = %.2f  --  h = %.2f  --",frame.pointX,frame.pointY,frame.width,frame.height);
    #endif
        i++;
    }
    [parser release];
}

static LayoutUtil * sharedLayoutUtil;
+(LayoutUtil *) shareLayoutUtil
{
    @synchronized(self){
        if (!sharedLayoutUtil) {
            sharedLayoutUtil = [[LayoutUtil alloc]init];
        }
    }
    return sharedLayoutUtil;
}
@end

//自定义对象类型的Frame
@implementation MyFrame
@end

@implementation MyPageSetting
@end