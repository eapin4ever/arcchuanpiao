//
//  ZSTNewsCarouselView.h
//  F3
//
//  Created by xuhuijun on 12-7-18.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TKAsynImageView.h"

@class ZSTShellGCarouselView;

@protocol ZSTShellGCarouselViewDataSource <NSObject>

@optional
- (NSInteger)numberOfViewsInCarouselView:(ZSTShellGCarouselView *)newsCarouselView;
- (NSDictionary *)carouselView:(ZSTShellGCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index;

@end

@protocol ZSTShellGCarouselViewDelegate <NSObject>

@optional
- (void)carouselView:(ZSTShellGCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;

@end

@interface ZSTShellGCarouselView : UIView <HJManagedImageVDelegate,UIScrollViewDelegate,TKAsynImageViewDelegate>
{
    UIScrollView *_scrollView;
    UILabel *_describeLabel;
    TKPageControl *_pageControl;
    NSTimer *_carouselTimer;
    
    NSInteger _totalPages;
    NSInteger _curPage;
    
    NSMutableArray *_curSource;
    
    id<ZSTShellGCarouselViewDataSource> _carouselDataSource;
    id<ZSTShellGCarouselViewDelegate>  _carouselDelegate;
}

@property(nonatomic, assign) id<ZSTShellGCarouselViewDataSource> carouselDataSource;
@property(nonatomic, assign) id<ZSTShellGCarouselViewDelegate> carouselDelegate;

- (void)reloadData;
- (void)stopAnimation;
- (void)startAnimation;

@end
