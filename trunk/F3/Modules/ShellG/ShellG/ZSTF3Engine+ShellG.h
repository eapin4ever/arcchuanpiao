//
//  ZSTF3Engine+ShellG.h
//  ShellG
//
//  Created by xuhuijun on 13-1-11.
//
//

#import "ZSTF3Engine.h"

@protocol ZSTF3EngineShellGDelegate <ZSTF3EngineDelegate>

- (void)getShellGADDidSucceed:(NSArray *)ADArray;
- (void)getShellGADDidFailed:(int)resultCode;

@end

@interface ZSTF3Engine (ShellG)

/**
 *	@brief	获取shell广告
 *
 */
- (void)getShellGAD;

@end
