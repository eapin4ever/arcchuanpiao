//
//  ZSTShellGViewController.m
//  ShellG
//
//  Created by xuhuijun on 13-1-11.
//
//

#import "ZSTShellGViewController.h"
#import "ZSTShellGSpeedBar.h"
#import "ZSTF3ClientAppDelegate.h"
#import "ZSTF3Engine+ShellG.h"
#import "ElementParser.h"
#import "ZSTModuleManager.h"
#import "ZSTUtils.h"
#import "ZSTWebViewController.h"
#import "LayoutUtil.h"
#import "ZSTMineViewController.h"

@interface ZSTShellGViewController ()

{
    TKPageControl *_pageControl;
    NSArray * frames;
    MyPageSetting * pageSettings;
    CGFloat  currMaxY;
    CGFloat  _pageHight;
    LayoutUtil * lay;//坐标计算
}
@end

@implementation ZSTShellGViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //    UIImageView *backgroundImg = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_ShellG_bg.png")];
    //    backgroundImg.frame = CGRectMake(0, 0, 320, 480+(iPhone5?88:0));
    //    backgroundImg.contentMode = UIViewContentModeScaleToFill;
    //    [self.view addSubview:backgroundImg];
    //    [backgroundImg release];
    
    //    self.carouselView = [[ZSTShellGCarouselView alloc] initWithFrame:CGRectMake(0, 0, 320, 151+(iPhone5?71:0))];
    self.carouselView = [[ZSTShellGCarouselView alloc] initWithFrame:CGRectMake(0, 0, 320, 160)];
    self.carouselView.carouselDataSource = self;
    self.carouselView.carouselDelegate = self;
    [self.view addSubview:self.carouselView];
    
    //    UIImageView *backgroundImg2 = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_ShellG_firstBackground.png")];
    //    backgroundImg2.frame = CGRectMake(0, CGRectGetMaxY(self.carouselView.frame), 320, 207);
    //    backgroundImg2.contentMode = UIViewContentModeScaleToFill;
    //    [self.view addSubview:backgroundImg2];
    //    [backgroundImg2 release];
    
    UIImageView *backgroundImg3 = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_shellg_mainView_background.png")];
    backgroundImg3.frame = CGRectMake(0, CGRectGetMaxY(self.carouselView.frame)-1, 320, 14);
    backgroundImg3.contentMode = UIViewContentModeScaleToFill;
//    [self.view addSubview:backgroundImg3];
    
    
//    self.speedBarScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.carouselView.frame), 320, (480 - 20 - 44 - 160 - 49)+(iPhone5?88:0))];

    self.speedBarScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, (480 - 20 - 44  - 49)+(iPhone5?88:0))];
    self.speedBarScrollView.delegate = self;
    self.speedBarScrollView.showsHorizontalScrollIndicator = NO;
    self.speedBarScrollView.showsVerticalScrollIndicator = NO;
    self.speedBarScrollView.backgroundColor = [UIColor clearColor];
//    self.speedBarScrollView.pagingEnabled = YES;
    [self.view addSubview:self.speedBarScrollView];
    
    [self.speedBarScrollView addSubview:self.carouselView];
    [self.speedBarScrollView addSubview:backgroundImg3];
    [backgroundImg3 release];
    
    _pageControl = [[TKPageControl alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.speedBarScrollView.frame)-12, 320, 10)];
    _pageControl.type = TKPageControlTypeOnImageOffImage;
    _pageControl.numberOfPages = 0;
    _pageControl.dotSize = 4;
    _pageControl.dotSpacing = 8;
    _pageControl.onImage = ZSTModuleImage(@"module_shellg_pageControlOn.png");
    _pageControl.offImage = ZSTModuleImage(@"module_shellg_pageControlOff.png");
    _pageControl.backgroundColor = [UIColor clearColor];
    _pageControl.userInteractionEnabled = NO;
//    [self.view addSubview:_pageControl];
    _pageControl.hidden = YES;
    
    lay = [LayoutUtil shareLayoutUtil];
    //获得布局信息
    frames = [lay frameArray];
    _pageHight = [lay pageHight];
    pageSettings = [lay pageSettings];
    
    [self imageButtonElementsForView];
    
    [self.engine getShellGAD];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.rightBarButtonItem = [self initWithSubviews];
}

- (UIBarButtonItem *) initWithSubviews
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(mineAction:) forControlEvents:UIControlEventTouchUpInside];
    
    btn.layer.cornerRadius = btn.frame.size.width / 2.0;
    btn.layer.borderColor = [UIColor clearColor].CGColor;
    btn.layer.masksToBounds = YES;
    
    NSString *path = nil;
    UIImage *image = nil;
    NSDictionary *dic = [ZSTF3Preferences shared].avaterName;
    
    if (![ZSTF3Preferences shared].loginMsisdn || [ZSTF3Preferences shared].loginMsisdn.length == 0) {
        
        [ZSTF3Preferences shared].loginMsisdn = @"";
    }
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0) {
        
        image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
    } else {
        
        if (dic || [dic isKindOfClass:[NSDictionary class]]) {
            path = [dic valueForKey:[ZSTF3Preferences shared].loginMsisdn];
        }
        
        if (path && path.length > 0) {
            
            NSString *homePath = NSHomeDirectory();
            NSString *imgTempPath = [path substringFromIndex:[path rangeOfString:@"tmp"].location];
            NSString *finalPath = [NSString stringWithFormat:@"%@/%@",homePath,imgTempPath];
            NSData *reader = [NSData dataWithContentsOfFile:finalPath];
            image = [UIImage imageWithData:reader];
            
            if (!image) {
                
                image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
            }
            
        } else {
            
            image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
        }
    }
    
    [btn setImage:image forState:UIControlStateNormal];
    
    //暂时解决btn图片不能显示的问题
    UIImageView *iv = [[UIImageView alloc] initWithFrame:btn.imageView.frame];
    iv.image = image;
    [btn insertSubview:iv aboveSubview:btn.imageView];
    
    return [[[UIBarButtonItem alloc] initWithCustomView:btn] autorelease];
}


- (void)mineAction:(id)sender
{
    ZSTMineViewController *controller = [[ZSTMineViewController alloc] initWithNibName:@"ZSTMineViewController" bundle:nil];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

- (void) showGuideView
{
    UIView *guideView = [[UIView alloc] initWithFrame:CGRectMake(0,0 , 320, 480+(iPhone5?88:0))];
    guideView.backgroundColor = [UIColor blackColor];
    guideView.tag = 9999999;
    guideView.alpha = 0.7f;
    
    UIImageView *guideTextImgView = [[UIImageView alloc] initWithFrame:CGRectMake(113,250+(iPhone5?88:0) , 93, 44)];
    guideTextImgView.image = ZSTModuleImage(@"module_shellb_BeginnersGuide.png");
    [guideView addSubview:guideTextImgView];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:FIRST_LAUNCH_ShellG];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(removeGuideView)];
    [guideView addGestureRecognizer:singleTap];
    
    [self.view addSubview:guideView];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
- (void) removeGuideView
{
    [[[UIApplication sharedApplication].keyWindow viewWithTag:9999999] removeFromSuperview];
}

- (void)imageButtonElementsForView
{
    
    NSInteger rowCount = 2;
    NSInteger columnCount = 4;
    
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module_shellg_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSArray *itemElements = [root selectElements:@"ImageButton Item"];
    if (![[NSUserDefaults standardUserDefaults] valueForKey:FIRST_LAUNCH_ShellG]
        && ([itemElements count] > (rowCount * columnCount))) {
//        [self showGuideView];
    }
    
//    if ([itemElements count] == 4) {
//        columnCount = 2;
//    }else if ([itemElements count] == 6)
//    {
//        columnCount = 3;
//    }
//    
//    //水平间距
//    float horizontalSpacing = (320.0 - 62.0 * columnCount)/(columnCount+1);
//    //上下间距
//    float verticalSpacing = 7+(iPhone5?26:0);
//    
//    //SeedBar模块的宽高
//    float speedBar_Width = 62.0f;
//    float speedBar_Hight = 90.0f;
//    
//    //SeedBar模块内 按钮 文字的上下距离
    float speedBar_Iner_VertialSpacing = 0.f + iPhone5?2:0;
//
//    
//    // 计算PageCount
    NSUInteger pageCount = [itemElements count] / 5;
    
    // 如果不是边界，则增加1
    if ([itemElements count]  % 5)
    {
        pageCount++;
    }
//    if ([itemElements count] > (rowCount * columnCount)) {
//        _pageControl.hidden = NO;
//        _pageControl.numberOfPages = pageCount;
//    }
//    self.speedBarScrollView.contentSize = CGSizeMake(320, _pageHight * pageCount + pageSettings.topSeparation + pageSettings.hightSeparation * pageCount * 2);
    for (int i = 0; i < [itemElements count]; i++) {
//    for (NSInteger i = 0; i < 5; i++) {
        if (i>= 20) {
            break;
        }
        Element *itemElement = [[itemElements objectAtIndex:i] retain];
//
        NSString *btnName = [[itemElement selectElement:@"Title"] contentsText];
        int moduleID = [[[itemElement selectElement:@"ModuleId"] contentsNumber] intValue];
        NSNumber *moduleType = [[itemElement selectElement:@"ModuleType"] contentsNumber];
        NSString *myTitleColor = [[itemElement selectElement:@"TitleColor"] contentsText];
        if (myTitleColor == nil || [myTitleColor length] == 0 || [myTitleColor isEqualToString:@""]) {
            myTitleColor = @"#000000";
        }
//
        NSDictionary *paramDic = [NSDictionary dictionary];
        if (moduleID == 12 || moduleID == 24) {
            NSString *interfaceUrl = [[itemElement selectElement:@"InterfaceUrl"] contentsText];
            paramDic = [NSDictionary dictionaryWithObjectsAndKeys:btnName,@"Title",moduleType,@"type",interfaceUrl,@"InterfaceUrl",myTitleColor,@"TitleColor", nil];
        }else{
            paramDic = [NSDictionary dictionaryWithObjectsAndKeys:btnName,@"Title",moduleType,@"type",myTitleColor,@"TitleColor", nil];
        }
//
//        int currentPage = i / (rowCount * columnCount);
//        int row = i / columnCount;
//        row = row % rowCount;
//        int col = i % columnCount;
//        
//        float x = 320 * currentPage + horizontalSpacing + (62 + horizontalSpacing)  *col;
//        float y = 18 + (iPhone5?22:0) + (verticalSpacing + speedBar_Hight) * row;
    
        MyFrame * frame = [frames objectAtIndex:i%5];
        ZSTShellGSpeedBar *bar = [[[ZSTShellGSpeedBar alloc] initWithFrame:CGRectMake(frame.pointX  , frame.pointY + i/5 * _pageHight, frame.width, frame.height)] autorelease];
        bar.backgroundColor = [UIColor clearColor];
        bar.speedBtn.tag = moduleID;
        
        bar.delegate = self;
        NSString *normalImagePath = [NSString stringWithFormat:@"module_shellg_icon%d_n.png", i+1];
        NSString *selectedImagePath = [NSString stringWithFormat:@"module_shellg_icon%d_p.png", i+1];
        
        [bar configSpeedBarNormalImage:ZSTModuleImage(normalImagePath)
                         selectedImage:ZSTModuleImage(selectedImagePath)
                                 param:paramDic];
        
        
//        bar.frame = CGRectMake(frame.pointX, frame.pointY, frame.width, frame.height);
        [bar speedBarInerSpace:speedBar_Iner_VertialSpacing];
        
        [self.speedBarScrollView addSubview:bar];
        
        self.speedBarScrollView.contentSize = CGSizeMake(320, CGRectGetMaxY(bar.frame) + 8);
    }
    [parser release];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    if (!_pageControl.hidden) {
        _pageControl.currentPage = aScrollView.contentOffset.x / aScrollView.bounds.size.width;
    }
}


#pragma mark ZSTShellGSpeedBarDelegate

- (void)zstShellGSpeedBar:(ZSTShellGSpeedBar *)speedBar withParam:(NSDictionary *)param
{
    NSMutableDictionary *moduleParams = [NSMutableDictionary dictionaryWithDictionary:param];
    
    ZSTModule *module = [[ZSTModuleManager shared] findModule:speedBar.speedBtn.tag];
    if (module) {
        UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:speedBar.speedBtn.tag withOptions:moduleParams];
        if (controller) {
            controller.hidesBottomBarWhenPushed = YES;
            controller.navigationItem.titleView = [ZSTUtils titleViewWithTitle:speedBar.titleLabel.text];
            controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}


#pragma mark ZSTF3EngineShellGDelegate

- (void)getShellGADDidSucceed:(NSArray *)ADArray
{
    self.ADArray = [ADArray retain];
    [self.carouselView reloadData];
}
- (void)getShellGADDidFailed:(int)resultCode
{
    [TKUIUtil alertInView:self.view withTitle:@"广告暂无数据！" withImage:nil];
}


#pragma mark - ZSTShellGCarouselViewDataSource

- (NSInteger)numberOfViewsInCarouselView:(ZSTShellGCarouselView *)newsCarouselView;
{
    return [self.ADArray count];
}

- (NSDictionary *)carouselView:(ZSTShellGCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index;
{
    if ([self.ADArray  count] != 0) {
        return [self.ADArray  objectAtIndex:index];
    }
    return nil;
}

#pragma mark - ZSTShellGCarouselViewDelegate

- (void)carouselView:(ZSTShellGCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;
{
    NSString *urlPath = [[self.ADArray objectAtIndex:index] safeObjectForKey:@"ad_linkurl"];
    if (urlPath == nil || [urlPath length] == 0 || [urlPath isEmptyOrWhitespace]) {
        return;
    }
    
    ZSTWebViewController *webViewController = [[ZSTWebViewController alloc] init];
    [webViewController setURL:urlPath];
     webViewController.type = self.moduleType;
    webViewController.hidesBottomBarWhenPushed = YES;
    webViewController.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
    [self.navigationController pushViewController:webViewController animated:NO];
    [webViewController release];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc
{
    [_pageControl release];
    [self.carouselView release];
    [self.speedBarScrollView release];
    [super dealloc];
}

@end
