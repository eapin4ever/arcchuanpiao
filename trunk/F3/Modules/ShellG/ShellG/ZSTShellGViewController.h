//
//  ZSTShellGViewController.h
//  ShellG
//
//  Created by xuhuijun on 13-1-11.
//
//

#import "ZSTModuleBaseViewController.h"

#import "ZSTShellGCarouselView.h"
#import "ZSTShellGSpeedBar.h"

@interface ZSTShellGViewController : ZSTModuleBaseViewController<ZSTShellGCarouselViewDataSource,ZSTShellGCarouselViewDelegate,ZSTShellGSpeedBarDelegate,UIScrollViewDelegate>

@property (strong)ZSTShellGCarouselView *carouselView;
@property (strong)NSArray *ADArray;
@property (strong,nonatomic) UIScrollView *speedBarScrollView;
@end
