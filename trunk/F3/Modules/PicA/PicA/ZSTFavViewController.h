//
//  ZSTFavViewController.h
//  PicA
//
//  Created by xuhuijun on 13-8-13.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MoreButton.h"
#import "MWPhotoBrowser.h"

#import "HHPhotosWaterfallCell.h"
#import "TKWaterfallsView.h"

@interface ZSTFavViewController : UIViewController<TKWaterfallsViewDelegate, TKWaterfallsViewDataSource,MWPhotoBrowserDelegate,UIScrollViewDelegate>
{
    int _pageIndex; //当前加载的页数，页面索引
    int _pageSize;  //每页数据条数
    
    BOOL _hasMore;  //是否有更多数据，是否长更多按钮
    
    MoreButton *_moreButton;                        //更多按钮，需要变更加载状态
    MWPhotoBrowser *browser;

}

@property (nonatomic, retain) NSArray *photos;

@property(nonatomic, retain) TKWaterfallsView *waterFlow;          //表视图
@property(nonatomic, retain) NSMutableArray *dataArray;         //数据数组

@property(nonatomic, retain) UITableViewCell *moreCell;         //更多按钮所在的行

@end
