//
//  PicA+Info.m
//  PicA
//
//  Created by xuhuijun on 13-8-9.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "PicA+Info.h"



@implementation ZSTPicACategory

@synthesize categoryid;
@synthesize categoryname;


+ (id)categoryPicaListinfoWithDic:(NSDictionary *)dic
{
    return [[self alloc] initWithDic: dic];
}

- (id)initWithDic:(NSDictionary *)dic
{
    if( (self=[super init])) {
        self.categoryid = [[dic safeObjectForKey:@"categoryid"] intValue];
        self.categoryname = [dic safeObjectForKey:@"categoryname"];
    }
    
    return self;
}

+ (NSMutableArray *)categoryPicaListWithDic:(NSDictionary *)dic
{
    NSArray *array = [dic safeObjectForKey:@"info"];
    if (![array isKindOfClass:[NSArray class]]) {
        return nil;
    }
    
    NSMutableArray *result = [NSMutableArray array];
    for (int i=0; i<array.count; i++) {
        NSDictionary *dictionary = [array objectAtIndex:i];
        if (![dictionary isKindOfClass:[NSDictionary class]]) {
            continue;
        }
        
        ZSTPicACategory *info = [ZSTPicACategory categoryPicaListinfoWithDic:dictionary];
        if (info != nil) {
            [result addObject:info];
        }
    }
    
    return result;
}

@end


@implementation ZSTPicAList

@synthesize categoryid;
@synthesize msgid;
@synthesize title;
@synthesize imgurl;
@synthesize favoritescount;
@synthesize addtime;
@synthesize width;
@synthesize height;

+ (id)picaListInfoWithDic:(NSDictionary *)dic
{
    return [[self alloc] initWithDic: dic];
}

- (id)initWithDic:(NSDictionary *)dic
{
    if( (self=[super init])) {
        self.categoryid = [[dic safeObjectForKey:@"categoryid"] intValue];
        self.msgid =  [[dic safeObjectForKey:@"msgid"] intValue];
        self.title = [dic safeObjectForKey:@"title"];
        self.imgurl = [dic safeObjectForKey:@"imgurl"];
        self.favoritescount = @([[dic safeObjectForKey:@"favoritescount"] integerValue]).stringValue;
        self.addtime = [dic safeObjectForKey:@"addtime"];
        self.width = [[dic safeObjectForKey:@"imgwidth"] floatValue];
        self.height = [[dic safeObjectForKey:@"imgheight"] floatValue];

        
    }
    
    return self;
}

+ (NSMutableArray *)picaListWithDic:(NSDictionary *)dic;
{
    NSArray *array = [dic safeObjectForKey:@"info"];
    if (![array isKindOfClass:[NSArray class]]) {
        return nil;
    }
    
    NSMutableArray *result = [NSMutableArray array];
    for (int i=0; i<array.count; i++) {
        NSDictionary *dictionary = [array objectAtIndex:i];
        if (![dictionary isKindOfClass:[NSDictionary class]]) {
            continue;
        }
        
        ZSTPicAList *info = [ZSTPicAList picaListInfoWithDic:dictionary];
        if (info != nil) {
            [result addObject:info];
        }
    }
    
    return result;
}

@end


@implementation ZSTPicADetailList

@synthesize msgid;
@synthesize title;
@synthesize shareurl;
@synthesize actionurl;
@synthesize detailid;
@synthesize imgurl;
@synthesize description;



+ (id)detailPicaListInfoWithDic:(NSDictionary *)dic
{
    return [[self alloc] initWithDic: dic];
}

- (id)initWithDic:(NSDictionary *)dic
{
    if( (self=[super init])) {
        self.msgid =  [[dic safeObjectForKey:@"msgid"] intValue];
        self.title = [dic safeObjectForKey:@"title"];
        self.shareurl = [dic safeObjectForKey:@"shareurl"];
        self.actionurl = [dic safeObjectForKey:@"actionurl"];
        self.detailid = [[dic safeObjectForKey:@"detailid"] intValue];
        self.imgurl = [dic safeObjectForKey:@"imgurl"];
        self.description = [dic safeObjectForKey:@"description"];
    }
    return self;
}

+ (NSMutableArray *)detailPicaListWithDic:(NSDictionary *)dic
{
    NSArray *array = [dic safeObjectForKey:@"details"];
    if (![array isKindOfClass:[NSArray class]]) {
        return nil;
    }
    
    NSMutableArray *result = [NSMutableArray array];
    for (int i=0; i<array.count; i++) {
        NSDictionary *dictionary = [array objectAtIndex:i];
        if (![dictionary isKindOfClass:[NSDictionary class]]) {
            continue;
        }
        
        ZSTPicADetailList *info = [ZSTPicADetailList detailPicaListInfoWithDic:dictionary];
        if (info != nil) {
            [result addObject:info];
        }
    }
    
    return result;
}

@end



