//
//  ZSTFavViewController.m
//  PicA
//
//  Created by xuhuijun on 13-8-13.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTFavViewController.h"
#import "ZSTDao+PicA.h"
#import "ZSTF3Engine+PicA.h"
#import "ZSTUtils.h"

#define AdditionalHeight 80

@interface ZSTFavViewController ()

@end

@implementation ZSTFavViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed: @"framework_top_bg.png"];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"我的喜欢", nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
    _hasMore = NO;
    
    if (self.waterFlow == nil) {
        TKWaterfallsView * waterflow = [[TKWaterfallsView alloc] initWithFrame:CGRectMake(0, 0, 320, 460-44+(iPhone5?88:0))];
        waterflow.backgroundColor = [UIColor colorWithWhite:246.0f/255 alpha:1];
        waterflow.separaterWidth = 8;
        waterflow.dataSource = self;
        waterflow.delegate = self;
        [self.view addSubview:waterflow];
        self.waterFlow = waterflow;

    }
    
    if (_moreButton == nil) {
        _moreButton = [MoreButton button];
        [_moreButton addTarget:self action:@selector(loadMore) forControlEvents:UIControlEventTouchUpInside];
        self.waterFlow.footerView = _moreButton;
    }
    
    NSArray *tempLocalNews = [NSMutableArray arrayWithArray:[self.dao getFavPicAList]];
    NSMutableArray *result = [NSMutableArray array];
    for (NSDictionary *dic in tempLocalNews) {
        
        ZSTPicAList *info = [ZSTPicAList picaListInfoWithDic:dic];
        if (info != nil) {
            [result addObject:info];
        }
    }
    
    self.dataArray = [NSMutableArray arrayWithArray:result];
    if ([self.dataArray count]) {
        [self.waterFlow reloadData];
    }else{
        [self noticeView];
    }
    _moreButton.hidden = !_hasMore;

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - 

-(void)noticeView
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0,200,320,50)];
    label.text = @"暂无收藏";
    label.textColor = [UIColor grayColor];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label];
}

#pragma mark - TKWaterfallsViewDataSource

- (NSInteger)numberOfColumsInWaterfallsView:(TKWaterfallsView *)waterfallsView{
    
    return 2;//目前紧支持2列
}

- (NSInteger)numberOfRowsInWaterfallsView:(TKWaterfallsView *)waterfallsView
{
    return [self.dataArray count];
}

- (TKWaterfallsViewCell *)waterfallsView:(TKWaterfallsView *)waterfallsView cellAtIndex:(NSUInteger)index
{
    HHPhotosWaterfallCell *cell = (HHPhotosWaterfallCell *)[waterfallsView dequeueReusableCell];
    
    if (cell == nil) {
        cell = [[HHPhotosWaterfallCell alloc] init];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    ZSTPicAList *object = [self.dataArray objectAtIndex:index];
    cell.addtionalHeight = AdditionalHeight;
    cell.tag = index;
    cell.adorn = [ZSTModuleImage(@"module_pica_album_card_bg.png") stretchableImageWithLeftCapWidth:20 topCapHeight:16];
    [cell setDateText:object.addtime];
    [cell setDescription:object.title];
	cell.url = [NSURL URLWithString:object.imgurl] ;
    cell.isFav = YES;


    return cell;
}

#pragma mark - TKWaterfallsViewDelegate

- (CGFloat)waterfallsView:(TKWaterfallsView *)waterfallsView heightForCellAtIndex:(NSUInteger)index
{
    ZSTPicAList *object = [self.dataArray objectAtIndex:index];
    float width = 0.0f;
    float height = 0.0f;
    
    if (object) {
        
        width = object.width ? object.width: 80;
        height = object.height ? object.height : 80;
        
    }
    float resultheight = waterfallsView.cellWidth * (height/width) + AdditionalHeight;
    return resultheight; //介绍文字高度
}

- (CGFloat)heightForFooterInWaterfallsView:(TKWaterfallsView *)waterfallsView
{
    return _hasMore ? 40 : 0;
}

- (void)waterFlowView:(TKWaterfallsView *)waterFlowView didSelectRowAtIndex:(NSUInteger)index
{
    ZSTPicAList *object = [self.dataArray objectAtIndex:index];
    
    [self.engine getPicADetailListWithMessageId:[NSString stringWithFormat:@"%@",[NSNumber numberWithInteger:object.msgid]]];
    
    browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.piclist = object;
    browser.displayActionButton = YES;
    [self.navigationController pushViewController:browser animated:YES];
}


#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}


- (void)getDetailListDidSucceed:(NSArray *)detaillist title:(NSString *)title shareurl:(NSString *)shareurl actionurl:(NSString *)actionurl msgid:(NSString *)msgid
{
    NSMutableArray *photos = [NSMutableArray array];
    
    for (int i=0; i< detaillist.count; i++) {
        
        ZSTPicADetailList *object = [detaillist objectAtIndex:i];
        NSURL *URL = [NSURL URLWithString:object.imgurl];
        
        MWPhoto *photo;
        if (URL != nil) {
            photo = [MWPhoto photoWithURL:URL];
            photo.caption = object.description == nil ? @"":object.description;
            photo.title = title == nil ? @"":title;
            photo.linkText = actionurl == nil ? @"":actionurl;
            photo.msgid = msgid == nil ? @"":msgid;
            photo.shareurl = shareurl == nil ? @"":shareurl;

    		[photos addObject:photo];
        }
    }
    
    self.photos = photos;
    [browser reloadData];
    if ([self.photos count] == 0) {
        [browser showPhotosIsEmpty:@"此图集为空"];
    }
}

- (void)getDetailListDidFailed:(id)notice
{
    NSMutableArray *photos = [NSMutableArray array];
    self.photos = photos;
    [browser reloadData];
    [browser showPhotosIsEmpty:notice];
}


@end
