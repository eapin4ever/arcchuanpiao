//
//  ZSTPicAViewController.m
//  PicA
//
//  Created by xuhuijun on 13-8-8.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTPicAViewController.h"
#import "ZSTF3Engine+PicA.h"
#import "ZSTDao+PicA.h"
#import "PicA+Info.h"
#import "ZSTUtils.h"
#import "ZSTFavViewController.h"
#import "BaseNavgationController.h"

#define AdditionalHeight 80
#define intervalTime 1   //小时

@interface ZSTPicAViewController ()
{
    NSMutableDictionary *_timeDic;//计时字典
    BOOL _isFirstLoading;
    NSInteger categoryid;
    MWPhotoBrowser *browser;
}
@end

@implementation ZSTPicAViewController
@synthesize waterFlow;
@synthesize topbar,categories,moreCell,dataArray;
@synthesize photos = _photos;


- (void)moduleApplication:(ZSTModuleApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [application.dao createTableIfNotExistForPicAModule];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed: @"framework_top_bg.png"];
}

- (void)openMyFav
{
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0)
    {
        ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
        controller.isFromSetting = YES;
        controller.delegate = self;
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
        }];

        
        return;
    }
    ZSTFavViewController *fav = [[ZSTFavViewController alloc] init];
    fav.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:fav animated:YES];

}

- (void)viewDidLoad
{
    [super viewDidLoad];
     self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"我的", nil) target:self selector:@selector (openMyFav)];
    _hasMore = NO;
    _loading = NO;
    if (self.topbar == nil) {
        //ColumnBar用法
        ColumnBar * bar = [[ColumnBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 38)];
        bar.topbarType = TopBarType_Slider;
        bar.dataSource = self;
        bar.delegate = self;
        bar.leftCapImage = ZSTModuleImage(@"module_pica_leftCapImage.png");
        bar.rightCapImage = ZSTModuleImage(@"module_pica_rightCapImage.png");
        bar.moverImage = ZSTModuleImage(@"module_pica_category_selected.png");
        bar.backgroundColor = [UIColor colorWithWhite:0.96 alpha:1];
        self.topbar = bar;
        [self.view addSubview:self.topbar];
        self.topbar.hidden = NO;
    }
    
    if (self.waterFlow == nil) {
        TKWaterfallsView * waterflow = [[TKWaterfallsView alloc] initWithFrame:CGRectMake(0, 0, 320, 460-44+(iPhone5?88:0))];
        waterflow.backgroundColor = [UIColor whiteColor];//[UIColor colorWithWhite:246.0f/255 alpha:1];
        waterflow.separaterWidth = 8;
        waterflow.dataSource = self;
        waterflow.delegate = self;
        [self.view addSubview:waterflow];
        self.waterFlow = waterflow;

    }
    
    if (_refreshHeaderView == nil)
    {
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f,  0.0f-250.0, self.view.frame.size.width,  250.0)];
		view.delegate = self;
		[self.waterFlow addSubview:view];
		_refreshHeaderView = view;

	}
	
    [_refreshHeaderView refreshLastUpdatedDate];
    
    if (_moreButton == nil) {
        _moreButton = [MoreButton button];
        [_moreButton addTarget:self action:@selector(loadMore) forControlEvents:UIControlEventTouchUpInside];
        self.waterFlow.footerView = _moreButton;
    }
    
    NSArray *tempLocalNews = [NSMutableArray arrayWithArray:[self.dao getPicACategories]];
    NSMutableArray *result = [NSMutableArray array];
    for (NSDictionary *dic in tempLocalNews) {
        
        ZSTPicACategory *info = [ZSTPicACategory categoryPicaListinfoWithDic:dic];
        if (info != nil) {
            [result addObject:info];
        }
    }
    self.categories = [NSMutableArray arrayWithArray:result];
    
    if ([self.categories count] == 1) {
        self.topbar.hidden = YES;
    }
    _isFirstLoading = NO;
    [self.topbar reloadData];
    [self.topbar selectTabAtIndex:0];
    
    [self reloadView];
    
    if ([self.categories count]) {
        [self.view newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkIndicatorView) {
            
            [self.engine getPicACategory];
        }];
    }else{
        [self.engine getPicACategory];
    }
    
    _moreButton.hidden = !_hasMore;
}

- (void)reloadView
{
    self.waterFlow.frame = CGRectMake(0, self.topbar.hidden ? 0 : self.topbar.size.height, self.view.size.width, self.view.frame.size.height-(self.topbar.hidden ? 0 : self.topbar.size.height));
    self.waterFlow.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    _refreshHeaderView.frame = CGRectMake(0.0f, 0.0f - self.waterFlow.bounds.size.height, self.view.frame.size.width, self.waterFlow.bounds.size.height);
}

- (void)updateListDataAtIndex:(NSInteger)index
{
    NSInteger categoryID = ((ZSTPicACategory *)[self.categories objectAtIndex:index]).categoryid ;
    categoryid = categoryID;
    _hasMore = ([self.dao picACountOfCategory:[NSString stringWithFormat:@"%ld",(long)categoryID]] >= 10);
    _moreButton.hidden = !_hasMore;
    _pageIndex = 1;
    NSArray *tempLocalNews = [NSMutableArray arrayWithArray:[self.dao picAListAtPage:_pageIndex pageCount:10 category:[NSString stringWithFormat:@"%ld",(long)categoryid]]];
    
    NSMutableArray *result = [NSMutableArray array];
    for (NSDictionary *dic in tempLocalNews) {
        
        ZSTPicAList *info = [ZSTPicAList picaListInfoWithDic:dic];
        if (info != nil) {
            [result addObject:info];
        }
    }
    self.dataArray = [NSMutableArray arrayWithArray:result];
}

#pragma mark - ColumnBarDataSource

- (NSInteger)numberOfTabsInColumnBar:(ColumnBar *)columnBar //tab的数量
{

    return [self.categories count];
}

- (NSString *)columnBar:(ColumnBar *)columnBar titleForTabAtIndex:(int)index // tab的名称
{
    return ((ZSTPicACategory *)[self.categories objectAtIndex:index]).categoryname;
}

#pragma mark - ColumnBarDelegate

- (BOOL)compareTimerkey:(NSString *)key timeDic:(NSMutableDictionary *)timerDic
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:timerDic];
    NSDate *compareDate = [dic objectForKey:key];
    if (compareDate == nil || [[compareDate description] length] == 0) {
        [dic setValue:[[NSDate date] dateByAddingHours:intervalTime] forKey:key];//第一次计时
        [_timeDic removeAllObjects];

        _timeDic = [NSMutableDictionary dictionaryWithDictionary:dic];
        return YES;
    }else {
        if ([compareDate isEqualToDate:[NSDate date]] || [compareDate isEarlierThanDate:[NSDate date]]) {
            [dic removeObjectForKey:key];
            [dic setValue:[[NSDate date] dateByAddingHours:intervalTime] forKey:key];//第n次计时
            [_timeDic removeAllObjects];

            _timeDic = [NSMutableDictionary dictionaryWithDictionary:dic];
            return YES;
        }else {
            return NO;
        }
    }
}

- (void)columnBar:(ColumnBar *)columnBar didSelectedTabAtIndex:(NSInteger)index
{
    self.dataArray = [NSMutableArray array];
    
    [self updateListDataAtIndex:index];
    [self doneLoadingData];
    
    if (!_isFirstLoading) {
        
        BOOL isAutoRefreshTime = NO;
        isAutoRefreshTime = [self compareTimerkey:@(((ZSTPicACategory *)[self.categories objectAtIndex:index]).categoryid).stringValue timeDic:_timeDic];
        if (isAutoRefreshTime) {
                        
            //此处检查当前网络连接，如果是wifi则自动更新，非wifi就不自动更新//还没有做
            [self performSelector:@selector(aotuLoadData)
                       withObject:nil
                       afterDelay:0.3];
        }
    }
    
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

- (void)loadDataForPage:(int)pageIndex
{
    [self.engine getPicAListWithCategoryId:[NSString stringWithFormat:@"%ld",(long)categoryid] size:10 pageindex:pageIndex];
}

//顶上下拽刷新，开始加载第1页数据
- (void)reloadTableViewDataSource
{
    //加载第1页
	_loading = YES;
    _pageIndex = 1;
	[self loadDataForPage:_pageIndex];
}

//更多按钮点击，或者底部上拽加载更多
- (void)loadMore
{
    if (_hasMore)
    {
        _loading = YES;
        [_moreButton displayIndicator];
        
        [self loadDataForPage:++_pageIndex];
    }
}

- (void)aotuLoadData
{
    [_refreshHeaderView autoRefreshOnScroll:self.waterFlow animated:NO];
    
}

//完成加载数据
- (void)doneLoadingData
{
    _loading = NO;
    [_moreButton hideIndicator];
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.waterFlow];
    [self.waterFlow reloadData];
}


//这个是处理数据条数不够一屏的情况
- (float)tableViewHeight
{
    if (self.waterFlow.contentSize.height < self.waterFlow.frame.size.height)
    {
        return self.waterFlow.frame.size.height;
    }
    else
    {
        return self.waterFlow.contentSize.height;
    }
}

//这个是处理数据条数不够一屏的情况
- (float)endOfTableView:(UIScrollView *)scrollView
{
    return [self tableViewHeight] - scrollView.bounds.size.height - scrollView.bounds.origin.y;
}



#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];

}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
{
    if ([self endOfTableView:scrollView] <= -65.0f && !_loading)
    {
        [self loadMore];
    }
    else
    {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods


- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
	[self reloadTableViewDataSource];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
	return _loading;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
	return [NSDate date];
}


#pragma mark - TKWaterfallsViewDataSource

- (NSInteger)numberOfColumsInWaterfallsView:(TKWaterfallsView *)waterfallsView{
    
    return 2;//目前紧支持2列
}

- (NSInteger)numberOfRowsInWaterfallsView:(TKWaterfallsView *)waterfallsView
{
    return [self.dataArray count];
}

- (TKWaterfallsViewCell *)waterfallsView:(TKWaterfallsView *)waterfallsView cellAtIndex:(NSUInteger)index
{
    HHPhotosWaterfallCell *cell = (HHPhotosWaterfallCell *)[waterfallsView dequeueReusableCell];
    
    if (cell == nil) {
        cell = [[HHPhotosWaterfallCell alloc] init];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    ZSTPicAList *object = [self.dataArray objectAtIndex:index];
    cell.addtionalHeight = AdditionalHeight;
    cell.tag = index;
    cell.adorn = [ZSTModuleImage(@"module_pica_album_card_bg.png") stretchableImageWithLeftCapWidth:20 topCapHeight:16];
    [cell setFavorCout:object.favoritescount];
    [cell setDateText:object.addtime];
    [cell setDescription:object.title];
	cell.url = [NSURL URLWithString:object.imgurl] ;
    return cell;
}

#pragma mark - TKWaterfallsViewDelegate

- (CGFloat)waterfallsView:(TKWaterfallsView *)waterfallsView heightForCellAtIndex:(NSUInteger)index
{
    ZSTPicAList *object = [self.dataArray objectAtIndex:index];    
    float width = 0.0f;
    float height = 0.0f;
    
    if (object) {
        
        width = object.width ? object.width: 80;
        height = object.height ? object.height : 80;
        
    }
    return waterfallsView.cellWidth * (height/width) + AdditionalHeight; //介绍文字高度
}

- (CGFloat)heightForFooterInWaterfallsView:(TKWaterfallsView *)waterfallsView
{
    return _hasMore ? 40 : 0;
}

- (void)waterFlowView:(TKWaterfallsView *)waterFlowView didSelectRowAtIndex:(NSUInteger)index
{
    ZSTPicAList *object = [self.dataArray objectAtIndex:index];
    
    [self.engine getPicADetailListWithMessageId:@(object.msgid).stringValue];
    
    browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.piclist = object;
    browser.displayActionButton = YES;
    [self.navigationController pushViewController:browser animated:YES];
    self.photos = nil;
    [browser reloadData];
    [browser showProgressHUDWithMessage:@""];
    
}


#pragma mark - MWPhotoBrowserDelegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return _photos.count;
}

- (MWPhoto *)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < _photos.count)
        return [_photos objectAtIndex:index];
    return nil;
}


#pragma mark -----------f3engine------------------

- (void)getCategoryDidSucceed:(NSArray *)thecategories
{
    [self.view removeNetworkIndicatorView];
    self.categories = [NSMutableArray arrayWithArray:thecategories];
    if ([self.categories count] == 1) {
        self.topbar.hidden = YES;
    }
    _isFirstLoading = NO;
    [self.topbar reloadData];
    [self.topbar selectTabAtIndex:0];
    
    [self reloadView];
}

- (void)getCategoryDidFailed:(id)notice
{
    [self.view refreshFailed];

}

- (void)getListDidSucceed:(NSArray *)list hasmore:(BOOL)hasmore
{
    _hasMore = hasmore;
    
    _moreButton.hidden = !_hasMore;
    
    if(_pageIndex == 1)
    {
        self.dataArray = [NSMutableArray arrayWithArray:list];
    }
    else
    {
        
        [self.dataArray addObjectsFromArray:list];
    }
    
    
    [self doneLoadingData];
}

- (void)getListDidFailed:(id)notice
{
    [self doneLoadingData];

}

- (void)getDetailListDidSucceed:(NSArray *)detaillist title:(NSString *)title shareurl:(NSString *)shareurl actionurl:(NSString *)actionurl msgid:(NSString *)msgid
{
    [browser hideProgressHUD:NO];
    NSMutableArray *photos = [NSMutableArray array];
    
    for (int i=0; i< detaillist.count; i++) {
        
        ZSTPicADetailList *object = [detaillist objectAtIndex:i];
        NSURL *URL = [NSURL URLWithString:object.imgurl];
        
        MWPhoto *photo;
        if (URL != nil) {
            photo = [MWPhoto photoWithURL:URL];
            photo.caption = object.description == nil ? @"":object.description;
            photo.title = title == nil ? @"":title;
            photo.linkText = actionurl == nil ? @"":actionurl;
            photo.msgid = msgid == nil ? @"":msgid;
            photo.shareurl = shareurl == nil ? @"":shareurl;
    		[photos addObject:photo];
        }
    }
    
    self.photos = photos;
    if ([self.photos count] == 0) {
        [browser showPhotosIsEmpty:@"此图集为空"];
    }else{
        [browser reloadData];
    }
}

- (void)getDetailListDidFailed:(id)notice
{
    NSMutableArray *photos = [NSMutableArray array];
    self.photos = photos;
    [browser reloadData];
    [browser showPhotosIsEmpty:notice];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
