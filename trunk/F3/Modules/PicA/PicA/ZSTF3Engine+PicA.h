//
//  ZSTF3Engine+PicA.h
//  PicA
//
//  Created by xuhuijun on 13-8-8.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTF3Engine.h"
#import "ZSTGlobal+PicA.h"


@protocol ZSTF3EnginePicADelegate <ZSTF3EngineDelegate>

@optional

- (void)getCategoryDidSucceed:(NSArray *)categories;
- (void)getCategoryDidFailed:(id)notice;

- (void)getListDidSucceed:(NSArray *)list hasmore:(BOOL)hasmore;
- (void)getListDidFailed:(id)notice;

- (void)getDetailListDidSucceed:(NSArray *)detaillist title:(NSString *)title shareurl:(NSString *)shareurl actionurl:(NSString *)actionurl msgid:(NSString *)msgid;
- (void)getDetailListDidFailed:(id)notice;

- (void)addCountDidSucceed:(NSInteger)count;
- (void)addCountDidFailed:(id)notice;

@end


@interface ZSTF3Engine (PicA)

- (void)getPicACategory;

- (void)getPicAListWithCategoryId:(NSString *)categoryid size:(int)size pageindex:(int)pageindex;

- (void)getPicADetailListWithMessageId:(NSString *)messageid;

- (void)addPicACountWithMessageId:(NSString *)messageid type:(AddType)type;

@end
