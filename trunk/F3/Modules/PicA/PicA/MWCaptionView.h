//
//  MWCaptionView.h
//  MWPhotoBrowser
//
//  Created by Michael Waterfall on 30/12/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoProtocol.h"
#import "ZSTDao+PicA.h"

@class MWCaptionView;

@protocol MWCaptionViewDelegate <NSObject>

- (void)captionView:(MWCaptionView *)captionview photo:(id<MWPhoto>)photo downloadBtnDidSelected:(UIButton *)sender;
- (void)captionView:(MWCaptionView *)captionview photo:(id<MWPhoto>)photo shareBtnDidSelected:(UIButton *)sender;
- (void)captionView:(MWCaptionView *)captionview photo:(id<MWPhoto>)photo favBtnDidSelected:(UIButton *)sender isAdd:(BOOL)isadd;
- (void)captionView:(MWCaptionView *)captionview photo:(id<MWPhoto>)photo linkBtnDidSelected:(UIButton *)sender;


@end

@interface MWCaptionView : UIView

@property(nonatomic,assign)id<MWCaptionViewDelegate>delegate;
@property(nonatomic,retain)ZSTDao *zstDao;

// Init
- (id)initWithPhoto:(id<MWPhoto>)photo;

// To create your own custom caption view, subclass this view
// and override the following two methods (as well as any other
// UIView methods that you see fit):

// Override -setupCaption so setup your subviews and customise the appearance
// of your custom caption
// You can access the photo's data by accessing the _photo ivar
// If you need more data per photo then simply subclass MWPhoto and return your
// subclass to the photo browsers -photoBrowser:photoAtIndex: delegate method
- (void)setupCaption;

// Override -sizeThatFits: and return a CGSize specifying the height of your
// custom caption view. With width property is ignored and the caption is displayed
// the full width of the screen
- (CGSize)sizeThatFits:(CGSize)size;

- (void)setIndexWithText:(NSString *)indexText;


@end
