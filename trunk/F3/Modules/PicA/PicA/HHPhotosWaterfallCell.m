//
//  PhotosWaterfallCell.m
//  HHelloCat
//
//  Created by luobin on 4/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "HHPhotosWaterfallCell.h"
#import "TKAsynImageView.h"
#import <SDWebImage/UIImageView+WebCache.h>


#define TOPMARGIN 8.0f
#define LEFTMARGIN 8.0f

#define IMAGEVIEWBG [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1.0]
#define SMALLTEXTWBG [UIColor colorWithRed:0.75 green:0.75 blue:0.75 alpha:1.0]
#define BIGTEXTWBG [UIColor colorWithRed:0.55 green:0.55 blue:0.55 alpha:1.0]



@implementation HHPhotosWaterfallCell

@synthesize mi,adorn,columnCount,description,dateText;
@synthesize url;
@synthesize favorCountLabel,descriptionLabel,dateLabel;
@synthesize favorOverlay,adornImageView;
@synthesize favorCout;
@synthesize addtionalHeight,isFav;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        self.adornImageView = [[UIImageView alloc] init];
        self.adornImageView.contentMode = UIViewContentModeScaleToFill;
        self.adornImageView.backgroundColor = [UIColor clearColor];
        [self insertSubview:adornImageView atIndex:0];
        
        self.descriptionLabel = [[UILabel alloc] init];
        self.descriptionLabel.backgroundColor = [UIColor clearColor];
        self.descriptionLabel.text = @"好懒呀,都没有介绍一下人家～";
        self.descriptionLabel.textColor = BIGTEXTWBG;
        self.descriptionLabel.font = [UIFont systemFontOfSize:12];
        self.descriptionLabel.numberOfLines = 0;
        [self.adornImageView addSubview:self.descriptionLabel];
        
        self.dateLabel = [[UILabel alloc] init];
        self.dateLabel.backgroundColor = [UIColor clearColor];
        self.dateLabel.text = @"00:00:00";
        self.dateLabel.textColor = SMALLTEXTWBG;
        self.dateLabel.font = [UIFont systemFontOfSize:10];
        self.dateLabel.textAlignment = NSTextAlignmentLeft;
        [self.adornImageView addSubview:self.dateLabel];

        
        self.favorOverlay = [[UIImageView alloc] init];
        self.favorOverlay.image = ZSTModuleImage(@"module_pica_poster_like.png");
        [self.adornImageView addSubview:favorOverlay];
        
        self.favorCountLabel = [[UILabel alloc] init];
        self.favorCountLabel.text = @"0";
        self.favorCountLabel.textAlignment = NSTextAlignmentRight;
        self.favorCountLabel.font = [UIFont systemFontOfSize:10];
        self.favorCountLabel.textColor = SMALLTEXTWBG;
        self.favorCountLabel.backgroundColor = [UIColor clearColor];
        [self.adornImageView addSubview:self.favorCountLabel];
    
        
//        self.mi = [[[TKAsynImageView alloc] init] autorelease];
        self.mi = [[UIImageView alloc] init];
		[self.adornImageView addSubview:self.mi];
     
   }
    return self;
}


- (void)setAdorn:(UIImage *)theAdorn
{
    if (adorn != theAdorn) {

        adorn = theAdorn;
        
        adornImageView.image = adorn;
    }
}

- (void)setUrl:(NSURL *)theUrl
{
    if (theUrl != url) {

        url = theUrl;

        [mi setImageWithURL:url];
        
    }
}   

- (void)setFavorCout:(NSString *)theFavorCout
{
    if (theFavorCout != favorCout) {

        favorCout = theFavorCout;
        
        if ([favorCout length] >= 5) {
            favorCout = @"9999+";
        }
        
        self.favorCountLabel.text = favorCout;        
    }
}

- (void)setDescription:(NSString *)theDescription
{
    if (theDescription != description) {
        description = theDescription;
        descriptionLabel.text = description;
    }
}


- (void)setDateText:(NSString *)theDateText
{
    if (theDateText != dateText) {
        dateText = theDateText;
        dateLabel.text = dateText;
    }
}


- (void)layoutSubviews
{
    self.adornImageView.frame  = CGRectMake( 0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame));
    self.mi.frame  = CGRectMake( 3, 1, CGRectGetWidth(self.frame)-6, CGRectGetHeight(self.frame)-4-addtionalHeight);

    CAShapeLayer *styleLayer = [CAShapeLayer layer];
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRoundedRect:mi.bounds byRoundingCorners:(UIRectCornerTopLeft|UIRectCornerTopRight) cornerRadii:CGSizeMake(4.0, 4.0)];
    styleLayer.path = shadowPath.CGPath;
    self.mi.layer.mask = styleLayer;
    
    if (adorn) {
        
        if (addtionalHeight) {
            
            self.descriptionLabel.frame = CGRectMake(8, CGRectGetHeight(mi.frame)+3, CGRectGetWidth(adornImageView.frame)-16, addtionalHeight - 25);
            self.dateLabel.frame = CGRectMake(8, CGRectGetHeight(adornImageView.frame)-23, 100, 11);
            
            CGSize theStringSize = [self.favorCountLabel.text sizeWithFont:self.favorCountLabel.font
                                                         constrainedToSize:CGSizeMake(adornImageView.frame.size.width/2, CGFLOAT_MAX)
                                                             lineBreakMode:NSLineBreakByCharWrapping];
            
            self.favorCountLabel.frame = CGRectMake(CGRectGetWidth(adornImageView.frame)-10 - theStringSize.width, CGRectGetHeight(adornImageView.frame)-23, theStringSize.width, 11);
            
            self.favorOverlay.frame = CGRectMake(CGRectGetMinX(self.favorCountLabel.frame) - 13, CGRectGetMinY(self.favorCountLabel.frame)+1, 10, 9);

            self.descriptionLabel.hidden = NO;
            self.favorCountLabel.hidden = isFav ? YES : NO;
            self.favorOverlay.hidden = isFav ? YES : NO;
            self.dateLabel.hidden = NO;
        }
    }
    
    [super layoutSubviews];
}

@end
