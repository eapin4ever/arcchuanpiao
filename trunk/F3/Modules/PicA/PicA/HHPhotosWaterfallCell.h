//
//  PhotosWaterfallCell.h
//  HHelloCat
//
//  Created by luobin on 4/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "TKWaterfallsView.h"



@class TKAsynImageView;
@interface HHPhotosWaterfallCell : TKWaterfallsViewCell{
    UIImageView *adornImageView;
    UILabel *descriptionLabel;
    UILabel *dateLabel;
}
@property (nonatomic, assign) int addtionalHeight;
@property (nonatomic,assign) int columnCount;
@property (nonatomic, retain) UIImage *adorn;//装饰
@property (nonatomic, retain) NSString *description;//描述
@property (nonatomic, retain) NSString *dateText;//日期
//@property (nonatomic, retain) TKAsynImageView *mi;
@property (nonatomic,retain) UIImageView *mi;
@property (nonatomic, retain) NSURL *url;

@property (nonatomic, retain) UILabel *descriptionLabel;
@property (nonatomic, retain) UILabel *dateLabel;
@property (nonatomic, retain) UIImageView *adornImageView;

@property (nonatomic, retain) UILabel *favorCountLabel;
@property (nonatomic, retain) UIImageView *favorOverlay;

@property (nonatomic, assign) NSString *favorCout;
@property (nonatomic, assign) BOOL isFav;



@end
