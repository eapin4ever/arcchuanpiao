//
//  NewsContentVO.m
//  News
//
//  Created by admin on 12-7-25.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTArticleDContentVO.h"

@implementation ZSTArticleDContentVO

@synthesize title;
@synthesize summary;
@synthesize keyword;
@synthesize source;
@synthesize addtime;
@synthesize isfavorites;
@synthesize detailid;
@synthesize letter;
@synthesize imageurl;
@synthesize imagedesc;
@synthesize ordernum;
@synthesize officialurl;
@synthesize onlineservice;
@synthesize ServicePhone;


+ (id)voWithDic:(NSDictionary *)dic
{
    return [[[self alloc] initWithDic: dic] autorelease];
}

- (id)initWithDic:(NSDictionary *)dic
{
    if( (self=[super init])) {
        self.title =  [dic safeObjectForKey:@"title"];
        self.summary =  [dic safeObjectForKey:@"summary"];
        self.keyword =  [dic safeObjectForKey:@"keyword"];
        self.source =  [dic safeObjectForKey:@"source"];
        self.addtime =  [dic safeObjectForKey:@"addtime"];
        self.isfavorites =  [[dic safeObjectForKey:@"isfavorites"] boolValue];
        self.detailid =  [[dic safeObjectForKey:@"detailid"] intValue];
        self.letter =  [dic safeObjectForKey:@"content"];
        self.imageurl =  [dic safeObjectForKey:@"imageurl"];
        self.imagedesc =  [dic safeObjectForKey:@"imagedesc"];
        self.ordernum =  [[dic safeObjectForKey:@"ordernum"] intValue];
        self.officialurl =  [dic safeObjectForKey:@"officialurl"];
        self.onlineservice =  [dic safeObjectForKey:@"onlineservice"];
        self.ServicePhone =  [dic safeObjectForKey:@"ServicePhone"];


    }
    return self;
}


- (void)dealloc 
{
    self.title = nil;
    self.summary = nil;
    self.keyword = nil;
    self.source = nil;
    self.addtime = nil;
    self.imageurl = nil;
    self.imagedesc = nil;
    self.officialurl = nil;
    self.onlineservice = nil;
    self.ServicePhone = nil;
    
    [super dealloc];
}

@end
