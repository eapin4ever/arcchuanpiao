//
//  ZSTGlobal+ArticleB.h
//  ArticleB
//
//  Created by xuhuijun on 13-1-28.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <UIKit/UIKit.h>

#define ArticleB_PAGE_SIZE 10

#define SortType_Desc @"Desc"               //向新取
#define SortType_Asc @"Asc"                 //向旧取


//////////////////////////////////////////////////////////////////////////////////////////

#define ArticleDBaseURL @"http://mod.pmit.cn/ArticleD"
//#define ArticleDBaseURL @"http://192.168.21.10:90/ArticleD"

#define GetSingleMessage ArticleDBaseURL @"/GetSingleMessage"

