//
//  ZSTCustomLinkViewController.m
//  CustomLink
//
//  Created by xuhuijun on 13-6-18.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTCustomLinkViewController.h"
#import "ZSTModuleManager.h"
#import "ZSTUtils.h"

#define isRetina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)

#define kCurrntWidth 320
#define kCurrntHeight 480
#define kRetinaWidth 640
#define kRetinaHeight 960

@interface ZSTCustomLinkViewController ()
{
    NSString *url;
     BOOL isSecond;
}

@end

@implementation ZSTCustomLinkViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.titleView = [ZSTUtils logoView];
    
//    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"浏览器", nil) target:self selector:@selector (openMore)];
    
    self.type = self.moduleType;
    [self initWithData];
}

- (void)initWithData
{
    ZSTF3Preferences *dataManager = [ZSTF3Preferences shared];
    NSString *baseUrl = [self.application.launchOptions safeObjectForKey:@"InterfaceUrl"];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:dataManager.loginMsisdn forKey:@"msisdn"];
    [params setObject:dataManager.imsi forKey:@"imei"];
    if ([baseUrl rangeOfString:@"ecid"].location == NSNotFound && [baseUrl rangeOfString:@"ECID"].location == NSNotFound) {
        [params setObject:dataManager.ECECCID forKey:@"ecid"];
    }
    if ([baseUrl rangeOfString:@"module_type"].location == NSNotFound) {
        [params setObject:[NSNumber numberWithInteger:self.moduleType] forKey:@"module_type"];
    }
    if ([baseUrl rangeOfString:@"moduletype"].location == NSNotFound) {
        [params setObject:[NSNumber numberWithInteger:self.moduleType] forKey:@"moduletype"];
    }
    NSMutableDictionary *params2 = [NSMutableDictionary dictionary];
    [params2 setObject:[UIDevice machine] forKey:@"ua"];
    [params2 setObject:[NSNumber numberWithInt:isRetina?kRetinaWidth:kCurrntWidth] forKey:@"w"];
    [params2 setObject:[NSNumber numberWithInt:isRetina?kRetinaHeight:kCurrntHeight] forKey:@"h"];
    [params2 setObject:dataManager.platform forKey:@"platform"];
    
    NSString *currentAppUrlStr = [[[baseUrl stringByAddingQuery:params2] stringByAddingQuery:params] stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys: [ZSTUtils md5Signature:params], @"Md5Verify", nil]];
    
    if ([currentAppUrlStr rangeOfString:@"${termCookieStr}&"].location != NSNotFound) {
        
        currentAppUrlStr = [currentAppUrlStr stringByReplacingOccurrencesOfString:@"${termCookieStr}&" withString:@""];
    }
    
    [self setURL:currentAppUrlStr];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)webViewDidStartLoad:(UIWebView*)webView {
    [super webViewDidStartLoad:webView];
    self.navigationItem.titleView = [ZSTUtils logoView];
}

- (void)webViewDidFinishLoad:(UIWebView*)webView {
    [super webViewDidFinishLoad:webView];
    self.navigationItem.titleView = [ZSTUtils logoView];
    
    if (isSecond) {
        
        _scrollToolBarView.hidden = NO;
    } else {
        
        _scrollToolBarView.hidden = YES;
    }
}

- (void)webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
    [super webView:webView didFailLoadWithError:error];
    self.navigationItem.titleView = [ZSTUtils logoView];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    [super webView:webView shouldStartLoadWithRequest:request navigationType:navigationType];
    
    _scrollToolBarView.hidden = YES;
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
#ifdef DEBUG
        NSLog(@"－－－点击调用－－－－");
#endif
        
        _scrollToolBarView.hidden = NO;
        isSecond = YES;
    }
    
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
