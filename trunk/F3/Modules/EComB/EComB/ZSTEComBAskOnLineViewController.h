//
//  ZSTEComBAskOnLineViewController.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-5.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTEComBAskOnLineTableViewCell.h"
@interface ZSTEComBAskOnLineViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,ZSTF3EngineECBDelegate,UIAlertViewDelegate>
{
    UITextField * _searchField;
}
@property (nonatomic,retain) NSString * productid;
@property (nonatomic,retain) NSArray * dataArray;
@property (nonatomic,retain) UITableView * tableView;
@property (nonatomic,retain) UIView * footView;
@end
