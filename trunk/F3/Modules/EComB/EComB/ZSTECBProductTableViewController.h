//
//  ZSTECBProductTableViewController.h
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine+EComB.h"
#import "ZSTModuleBaseViewController.h"
#import "ZSTECBHomeCarouselView.h"
#import "ZSTECBProductTableImageCell.h"
#import "EGORefreshTableHeaderView.h"
#import "ZSTLoginViewController.h"

@interface ZSTECBProductTableViewController : ZSTModuleBaseViewController<UITableViewDataSource,UITableViewDelegate,ZSTECBHomeCarouselViewDataSource,TKLoadMoreViewDelegate,ZSTECBHomeCarouselViewDelegate,ZSTECBProductTableImageCellDelegate,ZSTF3EngineECBDelegate,LoginDelegate>
{
    TKLoadMoreView *_loadMoreView;
    BOOL _isRefreshing;
    BOOL _isLoadingMore;
    BOOL _hasMore;
    NSInteger _pagenum;
}
@property (strong,nonatomic) UIView * headView;
@property (strong,nonatomic) UITableView * tableView;
@property (strong,nonatomic) NSMutableArray * btnTextArr;
@property (strong,nonatomic) ZSTECBHomeCarouselView * carouselView;
@property (strong,nonatomic) NSArray * carouseData;
@property (strong,nonatomic) NSMutableArray * resultData;

@end