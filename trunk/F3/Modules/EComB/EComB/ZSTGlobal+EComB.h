//
//  ZSTGlobal+EComB.h
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <Foundation/Foundation.h>
/*
 *
 *测试服务器
 *
 */
//#define ECommerceBaseURL  @"http://192.168.21.10:90/EComB/ashx/"

#define ECommerceURL      @"http://mod.pmit.cn/EcomB/WuLiu.html"

/*
 *
 *正式服务器
 *
 */
#define ECommerceBaseURL @"http://mod.pmit.cn/EComB/ashx/"

#define GetCatagory ECommerceBaseURL @"/GetCategory"

// category
#define GETSORTLIST ECommerceBaseURL @"GetSortList.ashx?moduletype=%ld&ecid=%@"

// 获取收货地址
#define GETADDRESS ECommerceBaseURL @"AddAddress.ashx?action=list&msisdn=%@&moduletype=%ld&ecid=%@"

// 修改收货地址
#define ADDADDRESS ECommerceBaseURL @"AddAddress.ashx?action=add&msisdn=%@&username=%@&mobile=%@&address=%@&postcode=%@&moduletype=%ld&ecid=%@&provincecity=%@&cityid=%@"

// 获取订单详情
#define GETORDERPRODUCTLIST ECommerceBaseURL @"GetOrderProductList.ashx?orderid=%@&moduletype=%ld&ecid=%@"

// 获取订单详情金额
#define GETORDERAMOUNT ECommerceBaseURL @"GetOrder.ashx?orderid=%@&moduletype=%ld&ecid=%@"

#define GetFREIGHTTEMPLATE ECommerceBaseURL @"GetFreightTemplate.ashx?orderid=%@&moduletype=%ld&ecid=%@"


// 获取积分
#define JIFENPAY ECommerceBaseURL @"JiFenPay.ashx?action=query_jifen&orderid=%@&moduletype=%ld&ecid=%@&costamount=%f"

// 积分支付
#define JIFENALLPAY ECommerceBaseURL @"OrderSubmit.ashx?paytype=10&orderid=%@"

// 积分消耗
#define JIFENUSEPAY ECommerceBaseURL @"JiFenPay.ashx?action=expend_jifen&orderid=%@&moduletype=%ld&ecid=%@&costamount=%f&pointnum=%ld"

// 提交订单
#define SUBMITORDER ECommerceBaseURL @"ShoppingCarSubmit.ashx?msisdn=%@&moduletype=%ld&ecid=%@&userid=%@"

// 获取主页数据
#define GETPRODUCTLIST ECommerceBaseURL @"GetProductList.ashx?moduletype=%ld&ecid=%@&pagesize=%ld&pageindex=%ld"

//获取主页轮播图/
#define GETBANNERLIST ECommerceBaseURL @"GetBannerList.ashx?moduletype=%ld&ecid=%@"

///商品搜索
#define SEARCH ECommerceBaseURL @"Search.ashx?moduletype=%ld&ecid=%@&s=%@&pagesize=6&pageindex=%ld"

//在线咨询内容
#define CONSULTATION ECommerceBaseURL @"Consultation.ashx?action=list&productid=%@&moduletype=%ld&ecid=%@&msisdn=%@"


//发送在线咨询内容
#define CONSULTATIONADD ECommerceBaseURL @"Consultation.ashx?action=add&productid=%@&moduletype=%ld&ecid=%@&msisdn=%@&content=%@"


//获取商品详情页内容
#define GETPRODUCTINFOBYID ECommerceBaseURL @"/GetProductinfoByID.ashx?moduletype=%ld&ecid=%@&productid=%ld"


//获取商品展示内容
#define GETPRODUCTBYID ECommerceBaseURL @"GetProductByID.ashx?moduletype=%ld&ecid=%@&productid=%@"


//获取商品展示页轮播图
#define GETBANNERLISTSHOW ECommerceBaseURL @"GetBannerList.ashx?moduletype=%ld&ecid=%@&productid=%@"


//属性选择页
#define GETATTRIBUTE ECommerceBaseURL @"GetAttribute.ashx?moduletype=%ld&ecid=%@&msisdn=%@&productid=%@"

//分类显示
#define SEARCHCATEGORY ECommerceBaseURL @"Search.ashx?moduletype=%ld&ecid=%@&categoryid=%@&pagesize=6&pageindex=%ld"


//获取购物车中内容
#define GETSHOPPINGCAR ECommerceBaseURL @"GetShoppingCar.ashx?moduletype=%ld&ecid=%@&msisdn=%@"


//商品加入购物车
#define ADDPROTOCAR ECommerceBaseURL @"AddProToCar.ashx?moduletype=%ld&ecid=%@&msisdn=%@&productid=%@&propertyid=%@"

#define ADDNEWSPROTOCAR ECommerceBaseURL @"AddProToCar.ashx?moduletype=%ld&ecid=%@&msisdn=%@&productid=%@&propertyid=%@&prodetail=%@"

//我的订单页数据
#define GETMYORDER ECommerceBaseURL @"GetMyOrder.ashx?moduletype=%ld&ecid=%@&msisdn=%@"


//货到付款提交数据
#define HUODAOPAY ECommerceBaseURL @"OrderSubmit.ashx?orderid=%@&moduletype=%ld&ecid=%@&msisdn=%@&paytype=2"

//支付宝支付接口
#define ALIPAYTRADE ECommerceBaseURL @"AlipayTrade.ashx?orderid=%@"

//银联支付
#define YINLIANPAY @"http://mod.pmit.cn/EcomB/Order.html?paysource=app&"

//支付成功页面
#define PAYRESULTS @"http://192.168.21.10:90/ecomb/PayResult.html?t=%d&ecid=%@&moduletype=%@&orderid=%@"
