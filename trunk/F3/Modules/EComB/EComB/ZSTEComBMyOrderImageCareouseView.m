//
//  ZSTEComBMyOrderImageCareouseView.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-13.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTEComBMyOrderImageCareouseView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ZSTEComBMyOrderImageCareouseView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _scrollView.delegate = self;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        //_scrollView.backgroundColor = [UIColor redColor];
        _scrollView.contentSize = CGSizeMake(frame.size.width*2, frame.size.height);
        [self addSubview:_scrollView];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void)clean
{
    NSArray * imageArr = [_scrollView subviews];
    for (UIView * view in imageArr) {
        [view removeFromSuperview];
    }
}
-(void)reloadData
{
    [self clean];
    NSInteger total = [_careouseDataSouce imageCareouse:self numberOfTotal:0];//CGRectGetHeight(self.frame) * total + 3
    _scrollView.contentSize = CGSizeMake(CGRectGetHeight(self.frame) * total + 3 + 20, CGRectGetHeight(self.frame));
    for (int i = 0; i<total; i++) {
        [self addImage:[_careouseDataSouce imageCareouse:self imageAtIndex:i] tag:i];
    }
}
-(void)addImage:(NSString *)imageName tag:(NSInteger )tag
{

//    TKAsynImageView *asynImageView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(3 + CGRectGetHeight(self.frame) * tag, 2, CGRectGetHeight(self.frame) - 3, CGRectGetHeight(self.frame) - 3)];
//    asynImageView.defaultImage = [UIImage imageNamed:@"Module.bundle/module_articlea_scroll_default_img.png"];
//    asynImageView.asynImageDelegate = self;
//    asynImageView.adorn = ZSTModuleImage(@"module_articlea_scroll_default_img.png");
//    asynImageView.tag = tag;
//    [asynImageView clear];
//    asynImageView.url = [NSURL URLWithString:imageName];
//    [asynImageView loadImage];
    UIImageView *asynImageView = [[UIImageView alloc] initWithFrame:CGRectMake(3 + CGRectGetHeight(self.frame) * tag, 2, CGRectGetHeight(self.frame) - 3, CGRectGetHeight(self.frame) - 3)];
    [asynImageView setImageWithURL:[NSURL URLWithString:imageName] placeholderImage:[UIImage imageNamed:@"Module.bundle/module_articlea_scroll_default_img.png"]];
    asynImageView.layer.cornerRadius = 6;
    asynImageView.clipsToBounds = YES;
    [_scrollView addSubview:asynImageView];
}
-(void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{
    NSLog(@"图片被点击");
    [_careouseDelegate  imageCareouse:self didSelectIndex:asynImageView.tag];
}
@end
