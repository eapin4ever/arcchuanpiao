//
//  ZSTEcomBCategoriesSearchViewController.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-11.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//


// ZSTEcomBCategoriesSearchViewController : UIViewController

#import "ZSTECBProductTableImageCell.h"

@interface ZSTEcomBCategoriesSearchViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,ZSTECBProductTableImageCellDelegate,ZSTF3EngineECBDelegate>
@property (retain,nonatomic)UIView * headView;
@property (retain,nonatomic)UITableView * tableView;
@property (retain,nonatomic)NSMutableArray * resultData;
@end