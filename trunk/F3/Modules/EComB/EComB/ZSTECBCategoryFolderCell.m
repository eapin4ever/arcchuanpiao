//
//  ZSTECBCategoryFolderCell.m
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTECBCategoryFolderCell.h"
#import "ZSTECBIconView.h"

@implementation ZSTECBCategoryFolderCell

@synthesize iconViewes;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.iconViewes = [NSMutableArray arrayWithCapacity:4];
        
        // Initialization code
        for (int i = 0; i < 3; i++) {
            ZSTECBIconView *iconView = [[ZSTECBIconView alloc] initWithFrame:CGRectMake(16*(i + 1) + 85*i, 5, 85, [[NSUserDefaults standardUserDefaults] boolForKey:@"showImgKey"]?62:30)];
            iconView.backgroundColor = [UIColor clearColor];
            [self.contentView addSubview:iconView];
            [self.iconViewes addObject:iconView];
            
            self.backgroundColor = [UIColor clearColor];
            
        }
    }
    return self;
}


- (void)configCell:(NSArray *)categories forRowAtIndex:(NSIndexPath *)indexPath
{
    for (int i = 0; i < 3; i++) {
        ZSTECBIconView *iconView = [self.iconViewes objectAtIndex:i];
        if (i < [categories count]) {
            NSDictionary *category = [categories objectAtIndex:i];
            iconView.hidden = NO;
            [iconView setTitle:[category safeObjectForKey:@"CategoryName"]];
            [iconView setCategoryID:[category safeObjectForKey:@"CategoryID"]];
        } else {
            iconView.hidden = YES;
        }
    }
}

@end
