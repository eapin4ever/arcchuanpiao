//
//  ZSTECBProductTableImageCell.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTECBProductTableImageCellView.h"

@protocol ZSTECBProductTableImageCellDelegate <NSObject>

-(void)ZSTECBProductTableImageCellDidSelect:(NSInteger) row;

@end

@interface ZSTECBProductTableImageCell : UITableViewCell
{
    ZSTECBProductTableImageCellView * _leftView;
    ZSTECBProductTableImageCellView * _rightView;
}

@property (assign,nonatomic) id <ZSTECBProductTableImageCellDelegate> delegate;
@property (retain,nonatomic)ZSTECBProductTableImageCellView * leftView;
@property (retain,nonatomic)ZSTECBProductTableImageCellView * rightView;


-(BOOL)reloadDataWithLeft:(EComBHomePageInfo *) left Right:(EComBHomePageInfo *) right;
@end
