//
//  STGlobal+EComB.m
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTDao+EComB.h"
#import "ZSTSqlManager.h"
#import "ZSTUtils.h"
#import "ZSTGlobal+EComB.h"
#import "TKUtil.h"

#define CREATE_TABLE_CMD(moduleType) [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [EComB_Category_%ld] (\
                                                                    ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                    CategoryID INTEGER DEFAULT 0 UNIQUE, \
                                                                    CategoryName VARCHAR DEFAULT '', \
                                                                    ParentID INTEGER DEFAULT 0, \
                                                                    OrderNum INTEGER  DEFAULT 0, \
                                                                    Description VARCHAR  DEFAULT ''\
                                                                    );", moduleType]

@implementation ZSTDao(EComB)

- (void)createTableIfNotExistForECBModule
{
    [ZSTSqlManager executeSqlWithSqlStrings:CREATE_TABLE_CMD(self.moduleType)];
}

- (BOOL)addecbCategory:(NSInteger)categoryID
       categoryName:(NSString *)categoryName
           parentID:(NSInteger)parentID
           orderNum:(NSInteger)orderNum
        description:(NSString *)description
{
    BOOL success = [ZSTSqlManager executeUpdate:
                    [NSString stringWithFormat:@"INSERT OR REPLACE INTO EComB_Category_%ld (CategoryID, CategoryName, ParentID, OrderNum, Description) VALUES (?,?,?,?,?)", self.moduleType],
                    [NSNumber numberWithInteger:categoryID],
                    [TKUtil wrapNilObject:categoryName],
                    [NSNumber numberWithInteger:parentID],
                    [NSNumber numberWithInteger:orderNum],
                    [TKUtil wrapNilObject:description]
                    ];
    return success;
}

- (BOOL)deleteAllProductBCategories;
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM EComB_Category_%ld", self.moduleType];
    if (![ZSTSqlManager executeUpdate:deleteSQL]) {
        
        return NO;
    }
    return YES;
}

- (NSArray *)getChildrenBOfCategory:(NSInteger)parentID
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, CategoryID, CategoryName, ParentID, OrderNum, Description FROM EComB_Category_%ld where parentID = ? ORDER BY OrderNum desc, CategoryID desc", self.moduleType];
    NSArray *results = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:[NSNumber numberWithInteger:parentID], nil]];
    return results;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


@end
