//
//  ZSTRoundRectView.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-5.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTRoundRectView.h"

@implementation ZSTRoundRectView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        //    _titleImgView = [[UIImageView alloc]init];
        //    _titleImgView.frame = CGRectMake(0, 0, 320, 160);
        //    [self.view addSubview:_titleImgView];
        //    _titleImgView.layer.borderWidth = 0.57;
        //    _titleImgView.layer.cornerRadius = 8.0;
        //    _titleImgView.layer.borderColor = [UIColor redColor].CGColor;
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame radius:(CGFloat) radius  borderWidth:(CGFloat) border borderColor:(UIColor *) color
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.borderWidth = border;
        self.layer.cornerRadius = radius;
        self.layer.borderColor = color.CGColor;
    }
    return self;
}
- (id)initWithPoint:(CGPoint) beginPoint toPoint:(CGPoint) endPoint borderWidth:(CGFloat) border borderColor:(UIColor *) color
{
    
    self = [super initWithFrame:CGRectMake(beginPoint.x, beginPoint.y, endPoint.x - beginPoint.x, border)];
    if (self) {
        self.backgroundColor = color;
    }
    return  self;
}
- (void) showImage:(CGRect)frame image:(UIImage *) image
{
    UIImageView * imageView = [[UIImageView alloc]initWithImage:image];
    imageView.frame = frame;
    [self addSubview:imageView];
}

- (void) showText:(CGRect) frame  text:(NSString *) text
{
    UILabel * textLabel = [[UILabel alloc]initWithFrame:frame];
    textLabel.text = text;
    [self addSubview:textLabel];
}

- (void) showText:(CGRect) frame  text:(NSString *) text font:(UIFont *)font
{
    UILabel * textLabel = [[UILabel alloc]initWithFrame:frame];
    textLabel.text = text;
    textLabel.font = font;
    [self addSubview:textLabel];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
