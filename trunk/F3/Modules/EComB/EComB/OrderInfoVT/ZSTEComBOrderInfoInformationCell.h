//
//  ZSTEComBOrderInfoInformationCell.h
//  EComB
//
//  Created by qiuguian on 15/7/27.
//  Copyright (c) 2015年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTEComBOrderInfoInformationCell : UITableViewCell

@property(strong,nonatomic) UILabel * goalInfoLabel;
@property(strong,nonatomic) UILabel * goalPriceLabel;
@property(strong,nonatomic) UILabel * goalCountLabel;

@property (strong, nonatomic) UILabel *pointLabel;

-(void)reloadData:(EcombOrderGools *)gools;

@end
