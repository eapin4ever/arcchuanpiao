//
//  ZSTEComBOrderInfomationViewController.h
//  EComB
//
//  Created by qiuguian on 15/7/29.
//  Copyright (c) 2015年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTEComBOrderInfoViewControllerAddressCell.h"
#import "ZSTEComBOrderInfoViewControllerPayCell.h"
#import "ZSTEComBOrderInfoViewControllerInformationCell.h"
#import "ZSTEComBOrderInfoViewControllerResponseCell.h"
#import "ZSTWebViewController.h"
#import "ZSTECBPopOver.h"
#import "ZSTEComBOrderInfoAddressCell.h"
#import "ZSTEComBOrderInfoPayCell.h"

typedef enum
{
    typezhifubaon = 1,
    typehuodaon,
    typetotaln,
    typeyinliann
}PAYTYPEN;

//订单状态，0：未支付；1：支付成功；2：支付失败; 3:已发货;4:已完成
typedef enum
{
    stateweizhifun = 0,
    statezhifuchenggongn,
    statezhifushibain,
    stateyifahuon,
    stateyiwanchengn
}STATEN;

@interface ZSTEComBOrderInfomationViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,ZSTEComBOrderInfoAddressCellDelegateNew,ZSTF3EngineECBDelegate,ZSTEComBOrderInfoPayCellDelegate,ZSTWebViewControllerDelegate,ECBPopOverDelegate>{
    
    EcomBAddress * _address;
    NSArray * _orderGools;
    
    float  _totalAmount;
    int _totalFreight;
    payWayn _payWay;
    BOOL _hideSubmit;
    
    NSIndexPath *selectedIndexPath;
    
}

@property (nonatomic,strong)UITableView *tableView;
@property (strong,nonatomic) NSString * orderID;
@property (strong,nonatomic) NSArray *payNameArray;

-(void) hideSubmit:(BOOL) hide;

@end
