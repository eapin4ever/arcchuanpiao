//
//  ZSTEComBOrderInfoAddressCell.h
//  EComB
//
//  Created by anan on 7/26/15.
//  Copyright (c) 2015 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZSTEComBOrderInfoAddressCellDelegateNew <NSObject>

-(void)addressDidPressedAction;

@end

@interface ZSTEComBOrderInfoAddressCell : UITableViewCell
@property (weak,nonatomic) id<ZSTEComBOrderInfoAddressCellDelegateNew> delegate;
@property (strong,nonatomic) UILabel * addressLabel;
@property (strong,nonatomic) UILabel * tellLabel;

-(void) initData:(EcomBAddress *)address;
@end
