//
//  ZSTEComBOrderInfoJifenCell.h
//  EComB
//
//  Created by qiuguian on 2015/7/27.
//  Copyright (c) 2015年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMCustomSwitch.h"

@class ZSTEComBOrderInfoJifenCell;

typedef  void (^EcbinfoStatesBlock1)(ZSTEComBOrderInfoJifenCell *cell,int states);
@interface ZSTEComBOrderInfoJifenCell : UITableViewCell<PMCustomSwitchDelegate>{

     EcbinfoStatesBlock1 statesBlock;

}

@property (nonatomic, strong) UILabel *jifenLabel;
@property (nonatomic,strong) PMCustomSwitch *switchOne;
- (void)setInfoStatesBlock:(EcbinfoStatesBlock1)block;

- (void)jifenpay:(int)state pointnum:(int)pointnum deductionamount:(float)deductionamount;

@end
