//
//  ZSTEComBOrderInfoPayCell.m
//  EComB
//
//  Created by anan on 7/26/15.
//  Copyright (c) 2015 zhangshangtong. All rights reserved.
//

#import "ZSTEComBOrderInfoPayCell.h"

@implementation ZSTEComBOrderInfoPayCell{

    UIView * _zhifubaoView;
    ZSTRoundRectView * _round;

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _defaultPayTag = 0;
        
        _round = [[ZSTRoundRectView alloc]initWithFrame:CGRectMake(5, 3, 310, 38) radius:4.0 borderWidth:1.0f borderColor:RGBCOLOR(239, 239, 239)];
        _round.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_round];
        
        _zhifubaoView = [[UIView alloc]init];
        _zhifubaoView.frame = CGRectMake(0, 0, 310, 38);
        [_round addSubview:_zhifubaoView];
        
        _payWayImageView = [[UIImageView alloc] init];
        _payWayImageView.frame = CGRectMake(12, 10, 53, 18);
        [_zhifubaoView addSubview:_payWayImageView];
        
        _payWayLabel = [[UILabel alloc] init];
        _payWayLabel.frame = CGRectMake(12, 10, 150, 18);
        [_zhifubaoView addSubview:_payWayLabel];
        _payWayLabel.hidden = YES;
        
        _statusLabel = [[UILabel alloc] initWithFrame:CGRectMake(320 - 15 - 15-15,10, 15, 15)];
        _statusLabel.hidden = YES;
        [_zhifubaoView addSubview:_statusLabel];
        
        _selectedImageView = [[UIImageView alloc]init];
        _selectedImageView.frame = CGRectMake(320 - 15 - 15,10, 15, 15);
        [_zhifubaoView addSubview:_selectedImageView];

    }
    return self;
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

@end
