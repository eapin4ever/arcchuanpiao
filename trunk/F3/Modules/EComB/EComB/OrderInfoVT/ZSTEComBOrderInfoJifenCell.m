//
//  ZSTEComBOrderInfoJifenCell.m
//  EComB
//
//  Created by 阮飞 on 15/7/27.
//  Copyright (c) 2015年 zhangshangtong. All rights reserved.
//

#import "ZSTEComBOrderInfoJifenCell.h"

@implementation ZSTEComBOrderInfoJifenCell{

    UIView * _jifenView;
    ZSTRoundRectView *_round;
}

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
   
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        
        _round = [[ZSTRoundRectView alloc]initWithFrame:CGRectMake(5, 3, 310, 65) radius:4.0 borderWidth:1.0f borderColor:RGBCOLOR(239, 239, 239)];
        _round.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_round];
        
        
        UILabel * l3 = [[UILabel alloc]init];
        l3.font = [UIFont systemFontOfSize:14];
        l3.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
        l3.text = @"积分支付";
        [_round addSubview:l3];
        CGSize size3 = [l3.text sizeWithFont:l3.font constrainedToSize:CGSizeMake(235, 44) lineBreakMode:NSLineBreakByWordWrapping];
        l3.frame = CGRectMake(12, 8, size3.width, 22);
        
        UIControl *jifenControll = [[UIControl alloc]initWithFrame:_round.bounds];
        [_round addSubview:jifenControll];
        
        
        _jifenLabel = [[UILabel alloc] initWithFrame:CGRectMake(12, 30, 280, 30)];
        _jifenLabel.backgroundColor = [UIColor clearColor];
        _jifenLabel.textColor = RGBCOLOR(166, 166, 166);
        _jifenLabel.font = [UIFont systemFontOfSize:12];
        [_round addSubview:_jifenLabel];
        
        _switchOne = [[PMCustomSwitch alloc] initWithFrame:CGRectMake(250, (self.frame.size.height - 21)/2+2, 42, 21)];
        //        _switchOne.arrange = ZSTCustomSwitchArrangeONLeftOFFRight;
        //        _switchOne.onImage = ZSTModuleImage(@"module_ecomb_switch_on.png");
        //        _switchOne.offImage = ZSTModuleImage(@"module_ecomb_switch_off.png");
        _switchOne.status = PMCustomSwitchStatusOff;
        _switchOne.delegate = self;
        _switchOne.userInteractionEnabled = YES;
       
        [_round addSubview:_switchOne];
        
        
    }

    return self;
}


- (void)jifenpay:(int)state pointnum:(int)pointnum deductionamount:(float)deductionamount
{
  
    
    if (state == 2) {
        
        _switchOne.userInteractionEnabled = NO;
        
        _jifenLabel.text = @"部分商品不支持积分换购，暂不可用";
    }
    
    
        _jifenLabel.text = [NSString stringWithFormat:@"可用%d积分，可抵¥%.2f",pointnum,deductionamount];
    
        _switchOne.userInteractionEnabled = YES;
    
}




-(void)zstcustomSwitchSetStatus:(PMCustomSwitchStatus)status{

    if (!statesBlock) {
        
        return;
    }
    
    statesBlock(self,status);

}

-(void)setInfoStatesBlock:(EcbinfoStatesBlock1)block{

   statesBlock = [block copy];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
