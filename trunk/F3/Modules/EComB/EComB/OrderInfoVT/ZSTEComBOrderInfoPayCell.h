//
//  ZSTEComBOrderInfoPayCell.h
//  EComB
//
//  Created by anan on 7/26/15.
//  Copyright (c) 2015 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PMCustomSwitch.h>

typedef enum
{
    zhifubaon = 0,
    huodaon = 1,
    yinlianpayn
}payWayn;

@protocol ZSTEComBOrderInfoPayCellDelegate <NSObject>
-(void)selectedPayWay:(payWayn) payWay;
@end

@interface ZSTEComBOrderInfoPayCell : UITableViewCell

@property (retain,nonatomic) UIImageView * selectedImageView;
@property (retain,nonatomic) UIImageView * huodaoImageView;
@property (retain,nonatomic) UIImageView * yinlianImageView;
@property (retain,nonatomic) UIImageView * payWayImageView;
@property (retain,nonatomic) UILabel * payWayLabel;
@property (retain,nonatomic) UILabel *statusLabel;

@property (nonatomic) NSInteger defaultPayTag;
@property(nonatomic,strong) NSString *defaultImage;

@property (nonatomic,assign) id <ZSTEComBOrderInfoPayCellDelegate> delegate;

@property (nonatomic,strong) PMCustomSwitch *switchOne;

@property (retain,nonatomic) UIControl * payWayControl;

@end
