//
//  ZSTGoodsShowViewController.h
//  EComB
//
//  Created by pmit on 15/7/14.
//  Copyright (c) 2015年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTECBHomeCarouselView.h"
#import "ZSTECBPropertyViewController.h"
#import "ZSTEComBPutInCarButton.h"
#import "EGOPhotoViewController.h"
//#import "EGOPhotoGlobal.h"
#import "ZSTLoginViewController.h"
#import <PMRepairButton.h>

@interface ZSTGoodsShowViewController : UIViewController <ZSTF3EngineECBDelegate,ZSTECBHomeCarouselViewDelegate,ZSTECBHomeCarouselViewDataSource,LoginDelegate>
{
    EcomBPropertyInfo * _propertyInfo;
}

@property (strong,nonatomic)EComBHomePageInfo *pageInfo;
@property (strong,nonatomic)ZSTECBHomeCarouselView *carouseView;
@property (strong,nonatomic)UILabel *titleLabel;
@property (strong,nonatomic)UILabel *priceLabel;
@property (strong,nonatomic)UILabel *originalPriceLabel;
@property (strong,nonatomic)NSArray *carouseData;
@property (strong,nonatomic)UILabel *freightPriceLabel;
@property (strong,nonatomic)UILabel * slip;
@property (strong,nonatomic) UILabel *pointLabel;


@end
