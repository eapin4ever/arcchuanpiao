//
//  ZSTNewsCarouselView.h
//  F3
//
//  Created by xuhuijun on 12-7-18.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TKAsynImageView.h"

typedef enum
{
    pageControlStateLEFT,
    pageControlStateCENTER,
    pageControlStateRIGHT
}pageControlState;

@class ZSTECBHomeCarouselView;

@protocol ZSTECBHomeCarouselViewDataSource <NSObject>

@optional
- (NSInteger)numberOfViewsInCarouselView:(ZSTECBHomeCarouselView *)newsCarouselView;
- (EcomBCarouseInfo *)carouselView:(ZSTECBHomeCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index;

@end

@protocol ZSTECBHomeCarouselViewDelegate <NSObject>

@optional
- (void)carouselView:(ZSTECBHomeCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;

@end

@interface ZSTECBHomeCarouselView : UIView <HJManagedImageVDelegate,UIScrollViewDelegate,TKAsynImageViewDelegate>
{
    UIScrollView *_scrollView;
    UILabel *_describeLabel;
    TKPageControl *_pageControl;
    NSTimer *_carouselTimer;
    
    NSInteger _totalPages;
    NSInteger _curPage;
    
    NSMutableArray *_curSource;
    
//    id<ZSTECBHomeCarouselViewDataSource> _carouselDataSource;
//    id<ZSTECBHomeCarouselViewDelegate>  _carouselDelegate;
    UILabel * _titleLabel;
    NSString * _carousBgImg;
}

@property(nonatomic, weak) id<ZSTECBHomeCarouselViewDataSource> carouselDataSource;
@property(nonatomic, weak) id<ZSTECBHomeCarouselViewDelegate> carouselDelegate;
@property(nonatomic, assign) BOOL haveTitle;
- (void)reloadData;
- (void)stopAnimation;
- (void)startAnimation;
- (void)hideStateBar;
- (void)pageControlState:(pageControlState) state;
- (void)setCarouselViewBackgroundImg:(NSString *)img;
@end
