//
//  ZSTEComBInputAddressViewController.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTModuleBaseViewController.h"
#import "ZSTF3Engine+EComB.h"

@interface ZSTEComBInputAddressViewController : UIViewController <UITextFieldDelegate,UIScrollViewDelegate,ZSTF3EngineECBDelegate>
@property (nonatomic,retain) EcomBAddress * address;
@property (nonatomic,retain) UITextField * nameField;
@property (nonatomic,retain) UITextField * phoneField;
@property (nonatomic,retain) UITextField * addressField;
@property (nonatomic,retain) UITextField * addressNumberField;
@property (nonatomic,retain) UIScrollView * scrollView;
@property (nonatomic,retain) UITextField * detailAddressField;
@property (retain,nonatomic) NSString * orderID;
@property (retain,nonatomic) NSString *areaValue;
@property (nonatomic,assign) BOOL isNullFrom;

@end
