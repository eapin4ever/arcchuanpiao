//
//  ZSTF3Engine+EComB.h
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZSTECBProduct.h"
#import "ZSTGlobal+EComB.h"

@protocol ZSTF3EngineECBDelegate <ZSTF3EngineDelegate>

@optional

/**
 *	@brief	获取产品分类
 */
- (void)getProductCategoryDidSucceed:(NSArray *)array;;
- (void)getProductCategoryDidFailed:(int)resultCode;

/**
 *	@brief	获取主页轮播列表
 */
- (void)getEcomBCarouselDidSucceed:(NSArray *)carouseles;
- (void)getEcomBCarouselDidFailed:(int)resultCode;

/**
 *	@brief	获取商品展示页轮播列表
 */
- (void)getEcomBGoodsInfoCarouselDidSucceed:(NSArray *)carouseles;
- (void)getEcomBGoodsInfoCarouselDidFailed:(int)resultCode;

/**
 *	@brief	选择属性页数据获取
 */
-(void)getPropertyViewControllerDataDidSucceed:(NSDictionary *)dic;
-(void)getPropertyViewControllerDataDidFailed:(int)resultCode;
 /**
 *	@brief	加入购物车
 */
- (void)putGoodsIntoCarDidSucceed:(NSArray *)carouseles;
- (void)putGoodsIntoCarDidFailed:(int)resultCode;
/**
 *	@brief	获取购物车数据
 */
- (void)getShoppingCartDidSucceed:(NSArray *)carouseles;
- (void)getShoppingCartDidFailed:(int)resultCode;

/**

 *	@brief	获取产品列表
 */
- (void)getProductDidSucceed:(NSArray *)newsList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish;
- (void)getProductDidFailed:(int)resultCode;

/**
 *	@brief	获取产品内容
 */
- (void)getProductsContentDidSucceed:(NSString *)newsContent;
- (void)getProductsContentDidFailed:(int)resultCode;

- (void)getProductsShowDidSucceed:(ZSTECBProduct *)dic;
- (void)getProductsShowFaild:(int)resultCode;

/**
 *	@brief	获取产品订单
 */
- (void)getProductsOrderDidSucceed:(NSDictionary *)orderDic;
- (void)getProductsOrderDidFailed:(int)resultCode;

/**
 *	@brief	获取订单列表
 */
- (void)getPocketShoppingDidSucceed:(NSArray *)list isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish;
- (void)getPocketShoppingDidFailed:(int)resultCode;

/**
 *	@brief	获取订单详情
 */
- (void)getOrderDetailDidSucceed:(NSDictionary *)orderDetail;
- (void)getOrderDetailDidFailed:(int)resultCode;

/**
 *	@brief	获取订单列表
 */
- (void)getMyOrderDidSucceed:(NSArray *)list isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish;
- (void)getMyOrderDidFailed:(int)resultCode;


/**
 *
 *  @brief 获取收获地址
 **/
- (void)getEComBAddressdidSucceed:(NSDictionary *)addressDic;
- (void)getEComBAddressdidFailed:(int)resultCode;

/**
 *
 *  @brief 提交收获地址
 **/
- (void)saveEComBAddressdidSucceed;
- (void)saveEComBAddressdidFailed:(int)resultCode;

/**
 *
 *  @brief 获取订单详情页
 **/
- (void)getEcomBOrderInfoViewdidSucceed:(NSArray *) array payType:(int) payType status:(int) status yinlian:(BOOL)yinlian jifenpay:(int)jifenpay;
- (void)getEcomBOrderInfoViewdidFailed:(int)resultCode;

/**
 *
 *  @brief 获取订单详情页金额
 **/
- (void)getEcomBOrderAmountdidSucceed:(float) amount totalfreight:(float) freight;
- (void)getEcomBOrderAmountdidFailed:(int)resultCode;

- (void)getEcomBFreightInfoViewDidSucceed:(double)amount AndFreight:(double)totalFreight;
- (void)getEcomBFreightInfoViewDidFailed:(int)resultCode;

//获取积分
- (void)getjifenPayDidSucceed:(int)pointnum deductionamount:(float)deductionamount;
- (void)getjifenPayDidFailed:(int)resultCode;

/**
 *	@brief	获取主页列表
 */
- (void)getPageDidSucceed:(NSArray *)carouseles hasMore:(BOOL) hasMore;
- (void)getPageDidFailed:(int)resultCode;


/**
 *	@brief	获取搜索列表
 */
- (void)getEcomBSearchDidSucceed:(NSArray *)str hasMore:(BOOL) hasMore;
- (void)getEcomBSearchDidFailed:(int)resultCode hasMore:(BOOL) hasMore;


/**
 *	@brief	获取在线咨询
 */
- (void)getEcomBAskDidSucceed:(NSArray *)arr;
- (void)getEcomBAskDidFailed:(int)resultCode;


/**
 *	@brief	发送在线咨询
 */
- (void)sendEcomBAskDidSucceed:(NSArray *)arr;
- (void)sendEcomBAskDidFailed:(int)resultCode;


/**
 *	@brief	获取分类展示
 */
- (void)getCategoriesShowDidSucceed:(NSArray *)arr hasMore:(BOOL) hasMore;
- (void)getCategoriesShowDidFailed:(int)resultCode;

/**
 *	@brief	提交购物车
 */
- (void)submitOrderdidSucceed:(NSString *)orderID;
- (void)submitOrderDidFailed:(NSString *)result;


/**
 *	@brief	货到付款提交数据
 */
- (void)huodaoPayDidSucceed;
- (void)huodaoPayDidFailed:(int)resultCode;

// 积分付款
- (void)jifenpayDidSucceed;
- (void)jifenpayDidFailed:(int)resultCode;

// 积分消耗
- (void)jifenUsePayDidSucceed;
- (void)jifenUsePayDidFailed:(int)resultCode;

@end

@interface ZSTF3Engine (EComB)






/**
 *	@brief	获取新闻类别
 *
 */
- (void)getProductBCategory;


///////////////////////////////////////////////////////////////////////// Private ///////////////////////////////////////////////////////////////////////

/**
 *	@brief	获取新闻列表
 *
 *	@param 	categoryID 	分类ID
 *	@param 	sinceID 	起始新闻ID
 *	@param 	orderType 	当OrderType为Desc时（刷新），从服务器获取MsgID大于SinceID的至多Size条记录；当OrderType为Asc时（更多），从服务器获取MsgID小于SinceID的至多Size条记录
 *	@param 	fetchCount 	获取最大条数
 *	@param 	iconType 	ICon图片类别，1：列表ICon；2：轮播ICon
 */
//- (void)getProduct:(NSInteger)categoryID
//           sinceID:(NSInteger)sinceID
//         orderType:(NSString *)orderType
//        fetchCount:(NSUInteger)fetchCount
//          iconType:(ZSTECBIconType)iconType;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief	获取主页轮播
 *
 */
- (void)getEcomBCarousel;

- (void)getFreightTemplate:(NSString *)orderID;

/**
 *	@brief	获取商品展示页轮播
 *
 */
- (void)getEcomBGoodsInfoCarousel:(NSString *) protecteid;

/**
 *	@brief	加入购物车
 *
 */
- (void)putGoodsIntoCar:(NSString *) productid propertyId:(NSString *) propertyid;
- (void)putNewsGoodsIntoCar:(NSString *) productid propertyId:(NSString *) propertyid;

/**
 *	@brief	获取购物车数据
 *
 */
- (void)getShoppingCartData;


/**
 *	@brief	获取主页
 *
 */
- (void)getPage:(NSInteger) pageSize pageIndex:(NSInteger) pageIndex;

/**
 *	@brief	获取收获地址
 *
 */
- (void)getEcomBAddress;

/**
 *	@brief	保存收获地址
 *
 */
- (void)saveEcomBAddress:(EcomBAddress *)addr;

/**
 *	@brief	获取订单详情页数据
 *
 */
- (void)getEcomBOrderInfoViewWithOrderID:(NSString *)orderID;
/**
 *	@brief	搜索商品
 *
 */
- (void)ecomBSearch:(NSString *) str pageIndex:(NSInteger)index;

/**
 *	@brief	获取在线咨询内容
 *
 */
- (void)getAskOnLineData:(NSString *)productid;


/**
 *	@brief	发送在线咨询内容
 *
 */
- (void)sendAskOnLineData:(NSString *)str productid:(NSString *) productid;

/**
 *	@brief	属性选择页数据
 *
 */
-(void)getPropertyViewControllerData:(NSString *) productid;

/**
 *	@brief	获取分类展示页
 *
 */
- (void)getCategoriesShowData:(NSString *)categoryid pageIndex:(NSInteger) index;


/**
 *	@brief 获取我的订单
 *
 *	@param
 */
-(void)getMyOrderData;

- (void) getjifenPayInfoWithOrderId:(NSString *)orderid costamount:(float)amount;

/**
 *	@brief 提交订单
 *
 *	@param
 */
- (void)submitOrder:(NSDictionary *)dic;

/**
 *	@brief 获取订单总额
 *
 *	@param
 */
- (void)getEcomBOrderAmountWithOrderID:(NSString *)orderID;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *	@brief	获取指定圈子下pageNum页的新闻
 *
 *	@param 	pageNum 	页数
 *	@param 	productName 产品名称
 *	@param 	categoryID 	分类
 */
//- (void)getProductAtPage:(int)pageNum category:(NSInteger)categoryID productName:(NSString *)productName sinceId:(int)sinceId;

/**
 *	@brief	货到付款提交接口
 */

- (void)huodaoPay:(NSString *) _orderID;

- (void)jifenPay:(NSString *)orderid;

- (void)jifenUsePay:(NSString *)orderid costamount:(float)amount pointnum:(NSInteger)pointnum;

/**
 *	@brief	获取新闻正文
 *
 *	@param 	newsID 	新闻ID
 */

- (void)getProductsDetail:(NSInteger)newsID;

/**
 *	@brief	获取订单信息
 *
 *	@param 	newsID 	新闻ID
 */

//- (void)getProductsUserInfo:(NSString *)userName
//                    address:(NSString *)address
//                    zipCode:(NSString *)zipCode
//                     mobile:(NSString *)mobile
//                       memo:(NSString *)memo
//                      count:(NSString *)count
//               productsInfo:(ZSTECBProduct *)productsInfo;

//           productsContenInfo:(ZSTECAProductDetail *)productsContentInfo;


/**
 *	@brief	获取商品展示信息
 *
 *	@param 	productID 	商品ID
 */

- (void) getProductsShowInfo:(NSString *)productId;

/**
 *
 *  @brief	获取购物清单
 *
 *	@param 	msisdn 	    手机号
 *	@param 	sinceID 	起始商品编号
 *	@param 	orderType 	当OrderType为Desc时（刷新），从服务器获取MsgID大于SinceID的至多Size条记录；当OrderType为Asc时（更多），从服务器获取MsgID小于SinceID的至多Size条记录
 *	@param 	fetchCount 	获取最大条数
 *	@param 	orderField 	排序字段如：添加时间
 */

//- (void)getPocketShopping:(NSString *)msisdn
//                  sinceID:(NSInteger)sinceID
//                orderType:(NSString *)orderType
//               fetchCount:(NSUInteger)fetchCount
//               orderField:(NSString *)orderField;


/**
 *	@brief	获取该用户购物清单
 *
 *	@param 	pageNum 	页数
 *	@param 	msisdn 	    手机号码
 *	@param 	sinceId
 */
//- (void)getPocketShoppingAtPage:(int)pageNum msisdn:(NSString *)msisdn sinceId:(int)sinceId;


/**
 *
 *  @brief	获取用户收藏列表
 *
 *	@param 	msisdn 	    手机号
 *	@param 	sinceID 	起始商品编号
 *	@param 	orderType 	当OrderType为Desc时（刷新），从服务器获取MsgID大于SinceID的至多Size条记录；当OrderType为Asc时（更多），从服务器获取MsgID小于SinceID的至多Size条记录
 *	@param 	fetchCount 	获取最大条数
 *	@param 	orderField 	排序字段如：添加时间
 */

//- (void)getMyFavorites:(NSString *)msisdn sinceID:(NSInteger)sinceID orderType:(NSString *)orderType fetchCount:(NSUInteger)fetchCount orderField:(NSString *)orderField;


/**
 *	@brief	获取用户收藏列表
 *
 *	@param 	pageNum 	页数
 *	@param 	msisdn 	    手机号码
 *	@param 	sinceId
 */
//- (void)getMyFavoritesAtPage:(int)pageNum msisdn:(NSString *)msisdn sinceId:(int)sinceId;


/**
 *	@brief	获取订单详情
 *
 *	@param 	orderId 	订单号
 */

//- (void)getOrderDetail:(NSString *)orderId;


/**
 *	@brief 投诉
 *
 *	@param 	content 	投诉内容
 *	@param 	productId 	产品id
 *	@param 	msisdn 	    手机号
 */
//- (void)addComplaints:(NSString *)content productId:(NSString *)productId msisdn:(NSString *)msisdn;

//- (void)manageFavorites:(NSString *)productID msisdn:(NSString *)msisdn opType:(NSString *)opType;



@end
