//
//  ZSTEComBMyOrderViewController.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTEComBMyOrderViewController.h"
#import "ZSTEComBMyOrderViewControllerCell.h"
#import "ZSTEComBOrderInfoViewController.h"
#import "ZSTWuLiuViewController.h"
#import "ZSTEComBOrderInfomationViewController.h"

@interface ZSTEComBMyOrderViewController ()

@end

@implementation ZSTEComBMyOrderViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
     self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"订单", nil)];
    
    CGRect rect = self.view.bounds;
    
    _tableView = [[UITableView alloc]initWithFrame:rect style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.allowsSelection = YES;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:_tableView];
    
    [self.engine getMyOrderData];
}
-(void)getMyOrderDidSucceed:(NSArray *)list isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish
{
    _dataArr = [EcomBOrderInfo EcomBMyOrderWithArray:list];
    
    [_tableView reloadData];
//    [TKUIUtil alertInWindow:@"获取成功" withImage:nil];
}
-(void)getMyOrderDidFailed:(int)resultCode
{
    NSLog(@"获取我的订单失败");
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 360/2.0f + 15;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1f;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArr.count;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString * ZSTEComBMyOrderViewControllerCellName = @"ZSTEComBMyOrderViewControllerCell";
    ZSTEComBMyOrderViewControllerCell * cell = [tableView dequeueReusableCellWithIdentifier:ZSTEComBMyOrderViewControllerCellName];
    if (!cell) {
        cell = [[ZSTEComBMyOrderViewControllerCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ZSTEComBMyOrderViewControllerCellName];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
//        UIImageView * bg = [[UIImageView alloc]initWithImage:ZSTModuleImage(@"module_ecomb_myordercellbg.png")];
//        bg.frame = CGRectMake(10, 10, 300, 170);
//        cell.backgroundView = bg;
        
    }
    
    [cell setCheckBlock:^(ZSTEComBMyOrderViewControllerCell * cell,NSString *string) {
        
        ZSTWuLiuViewController *controller = [[ZSTWuLiuViewController alloc] init];
        controller.orderId = string;
        controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
    }];
    
    [cell initWithOrderInfo:[_dataArr objectAtIndex:indexPath.row]];
    return  cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //旧订单详情界面
//    ZSTEComBOrderInfoViewController * infoViewController = [[ZSTEComBOrderInfoViewController alloc]init];
    //新订单详情界面
    ZSTEComBOrderInfomationViewController * infoViewController = [[ZSTEComBOrderInfomationViewController alloc]init];
    [infoViewController hideSubmit:YES];
    infoViewController.orderID = [[_dataArr objectAtIndex:indexPath.row] orderOrderid];
    infoViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:infoViewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
