//
//  ZSTEcomBCategoriesSearchViewController.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTEcomBCategoriesSearchViewController.h"
#import "ZSTEComBGoodsShowViewController.h"
#import "ZSTGoodsShowViewController.h"

@interface ZSTEcomBCategoriesSearchViewController ()

@end

@implementation ZSTEcomBCategoriesSearchViewController
{
    UITextField * searchField;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"商品搜索", nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];

    _headView = [[UIView alloc]init];
    _headView.frame = CGRectMake(0, 0, 320, 40);
    _headView.backgroundColor = RGBCOLOR(244, 244, 244);
    
    ZSTRoundRectView * round = [[ZSTRoundRectView alloc]initWithFrame:CGRectMake(CGRectGetMinX(_headView.frame) + 5, 5, 310, 30) radius:4.0 borderWidth:1.0f borderColor:RGBCOLOR(239, 239, 239)];
    round.backgroundColor = [UIColor whiteColor];
    [_headView addSubview:round];
    
    searchField = [[UITextField alloc] initWithFrame:CGRectMake(30, (round.size.height - 20)/2, 310 - 30, 20)];
    searchField.borderStyle = UITextBorderStyleNone;
    searchField.backgroundColor = [UIColor clearColor];
    searchField.delegate = self;
    searchField.clearButtonMode = UITextFieldViewModeAlways;
    searchField.font = [UIFont systemFontOfSize:14];
    searchField.placeholder = @"请在此输入搜索内容";
    searchField.returnKeyType = UIReturnKeySearch;
    [searchField becomeFirstResponder];
    [round addSubview:searchField];
    
    UIImageView * searchImg = [[UIImageView alloc]initWithImage:ZSTModuleImage(@"module_ecomb_goolssearch_search.png")];
    searchImg.frame = CGRectMake((30 - 14)/2, (round.size.height - 15)/2, 15, 15);
    [round addSubview:searchImg];
    
    CGRect rect = self.view.bounds;
    if (IS_IOS_7) {
        rect.size.height -= 64;
    }
    
    _tableView = [[UITableView alloc]initWithFrame:rect style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.allowsSelection = NO;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    _resultData = [[NSMutableArray alloc] init];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.engine ecomBSearch:textField.text pageIndex:1];
    return YES;
}
-(void)getEcomBSearchDidFailed:(int)resultCode
{
    NSLog(@"获取失败");
    [TKUIUtil alertInWindow:@"暂无数据" withImage:nil];

}

-(void)getEcomBSearchDidSucceed:(NSArray *)str
{
    _resultData = [EComBHomePageInfo homePageWithArray:str];
    [_tableView reloadData];
    [searchField resignFirstResponder];
    NSLog(@"获取成功");
//    [TKUIUtil alertInWindow:@"获取成功" withImage:nil];

}
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}
#pragma mark tableDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (_resultData.count + 1)/2;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 210.f;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString * ECBProductTableViewCell = @"ECBProductTableViewCell";
    ZSTECBProductTableImageCell * cell = [tableView dequeueReusableCellWithIdentifier:ECBProductTableViewCell];
    if (!cell) {
        cell = [[ZSTECBProductTableImageCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ECBProductTableViewCell];
    }
    if (_resultData.count) {
        
        if ((_resultData.count - indexPath.row  * 2) > 1) {
            [cell reloadDataWithLeft:[_resultData objectAtIndex:(indexPath.row * 2)]  Right:[_resultData objectAtIndex:(indexPath.row * 2 +1)]];
        }
        else
        {
            [cell reloadDataWithLeft:[_resultData objectAtIndex:(indexPath.row * 2)]  Right:nil];
        }
    }
    cell.delegate = self;
    //    cell.textLabel.text = [NSString stringWithFormat:@"第%d",indexPath.row];
    cell.leftView.tag = indexPath.row * 2;
    cell.rightView.tag = indexPath.row * 2 + 1;
    return  cell;
}
-(void)ZSTECBProductTableImageCellDidSelect:(NSInteger)row
{
    ZSTGoodsShowViewController *showViewController = [[ZSTGoodsShowViewController alloc] init];
    showViewController.pageInfo = [_resultData objectAtIndex:row];
    showViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:showViewController animated:YES];
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0f;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return _headView;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//-(void)dealloc
//{
//    [_headView release];
//    [_tableView release];
//    [super dealloc];
//}
@end
