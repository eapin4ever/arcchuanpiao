//
//  ZSTEComBMyOrderViewControllerCell.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTEComBMyOrderImageCareouseView.h"

@class ZSTEComBMyOrderViewControllerCell;
typedef  void (^checkBlocked)(ZSTEComBMyOrderViewControllerCell *cell,NSString *orderid);

@interface ZSTEComBMyOrderViewControllerCell : UITableViewCell<ZSTEComBMyOrderImageCareouseViewDelegate,ZSTEComBMyOrderImageCareouseViewDataSouce,UIActionSheetDelegate>
{
    checkBlocked logisticsBlcok;
}

@property (strong,nonatomic) UILabel *  numberLabel;//订单号
@property (strong,nonatomic) UILabel *  totalLabel;//总金额
@property (strong,nonatomic) UILabel *  timeLabel;//下单时间
@property (strong,nonatomic) UILabel *  countLabel;//总数
@property (strong,nonatomic) UILabel *  stateLabel;//订单状态
@property (strong,nonatomic) UIButton*  logisticsbtn;
@property (strong,nonatomic) UILabel *  callSailerLabel;//联系卖家
@property (strong,nonatomic) ZSTEComBMyOrderImageCareouseView * careouseView;//照片图

-(void)initWithOrderInfo:(EcomBOrderInfo *) orderInfo;

-(void)setCheckBlock:(checkBlocked)block;

@end
