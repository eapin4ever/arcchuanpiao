//
//  ZSTEComBOrderInfoViewControllerInformationCell.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTEComBOrderInfoViewControllerInformationCell : UITableViewCell
@property(retain,nonatomic) UILabel * goalInfoLabel;
@property(retain,nonatomic) UILabel * goalPriceLabel;
@property(retain,nonatomic) UILabel * goalCountLabel;

@property (retain, nonatomic) UILabel *pointLabel;

-(void)reloadData:(EcombOrderGools *)gools;
@end
