//
//  ZSTECBCategoryFolderView.m
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTECBCategoryFolderView.h"
#import "ZSTECBCategoryFolderCell.h"

@interface ZSTECBCategoryFolderView()

@property (nonatomic, retain) UITableView *tableView;

@end


@implementation ZSTECBCategoryFolderView

@synthesize categories;
@synthesize tableView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        bgImageView.backgroundColor = RGBCOLOR(243, 243, 243);
        [self addSubview:bgImageView];
        
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) style:UITableViewStylePlain];
        self.tableView.contentInset = UIEdgeInsetsMake(5, 0, 5, 0);
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self addSubview:self.tableView];
    }
    return self;
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    self.tableView.frame = CGRectMake(0, 0, self.width, self.height);
}

- (void)setCategories:(NSArray *)theCategories
{
    if (categories != theCategories) {
        categories = theCategories;
        [self.tableView reloadData];
    }
}

#pragma mark -
#pragma mark ---Table Data Source Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.categories count]/3 + (([self.categories count]%3)? 1 : 0);
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *reuseIdentifier = @"ZSTECBCategoryFolderCell";
    ZSTECBCategoryFolderCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (cell == nil) {
        cell = [[ZSTECBCategoryFolderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSInteger len = 3;
    if ((indexPath.row + 1) * 3 >= self.categories.count) {
        len = self.categories.count - indexPath.row * 3;
    }
    NSArray *categoriesForCell = [self.categories subarrayWithRange:NSMakeRange(indexPath.row * 3, len)];
    [cell configCell:categoriesForCell forRowAtIndex:indexPath];
    return cell;
}

#pragma mark ---UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:@"showImgKey"]?72:40;
}


@end
