//
//  ZSTNewsCarouselView.m
//  F3
//
//  Created by xuhuijun on 12-7-18.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTECBHomeCarouselView.h"
#import "TKAsynImageView.h"
#import "UIImage+cut.h"
#import <SDWebImage/UIImageView+WebCache.h>

#define maxItems 5

@implementation ZSTECBHomeCarouselView
{
    UIView *titleView;
}
@synthesize carouselDelegate = _carouselDelegate;
@synthesize carouselDataSource = _carouselDataSource;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        _haveTitle = NO;
        _carousBgImg = @"module_shellb_scoll_pic.png";
        
        self.backgroundColor = [UIColor clearColor];
        _scrollView = [[UIScrollView alloc] initWithFrame:frame];
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        [self addSubview:_scrollView];
        
        titleView = [[UIView alloc] initWithFrame:CGRectMake(0, frame.size.height - 20, frame.size.width, 20)];
        titleView.backgroundColor = RGBACOLOR(125, 125, 125, 0.7);
        titleView.opaque = NO;
        [self addSubview:titleView];
        
        _pageControl = [[TKPageControl alloc] initWithFrame:CGRectMake(100, 5, 80, titleView.frame.size.height)];
        _pageControl.type = TKPageControlTypeOnImageOffImage;
        _pageControl.numberOfPages = 0;
        _pageControl.dotSize = 7;
        _pageControl.onImage = ZSTModuleImage(@"module_shellb_carousel_pageControlOn.png");
        _pageControl.offImage = ZSTModuleImage(@"module_shellb_carousel_pageControlOff.png");
        _pageControl.backgroundColor = [UIColor clearColor];
        _pageControl.userInteractionEnabled = NO;
        [titleView addSubview:_pageControl];
        
        
        _titleLabel = [[UILabel alloc]init];
        _titleLabel.frame = CGRectMake(10, 0, 150, titleView.frame.size.height);
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.textAlignment = NSTextAlignmentLeft;
        _titleLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = [UIFont systemFontOfSize:14];
        [titleView addSubview:_titleLabel];
        
        
        _curPage = 0;//轮播图得第一张
        
        [self startAnimation];
    }
    return self;
}
- (void)setCarouselViewBackgroundImg:(NSString *)img
{
    _carousBgImg = img;
}
-(void)hideStateBar
{
    titleView.hidden = YES;
    titleView.userInteractionEnabled = NO;
}
-(void)pageControlState:(pageControlState)state
{
    if (state == pageControlStateLEFT) {
        _pageControl.frame = CGRectMake(-100, 5, 80, titleView.frame.size.height);
    }
    else if (state == pageControlStateCENTER) {
        _pageControl.frame = CGRectMake(0, 5, 80, titleView.frame.size.height);
    }
    else if (state == pageControlStateRIGHT) {
        _pageControl.frame = CGRectMake(100, 5, 80, titleView.frame.size.height);
    }
}
- (void)reloadData
{
    _totalPages = [_carouselDataSource numberOfViewsInCarouselView:self];
    if (_totalPages == 0) {
        return;
        //        _totalPages = 3;
    }
    if (_totalPages == 1) {
        [self stopAnimation];
        _scrollView.scrollEnabled = NO;
    }
    if (_totalPages > maxItems) {
        _totalPages = maxItems;
    }
    _pageControl.numberOfPages = _totalPages;
    _pageControl.hidden = NO;
    
    if (_totalPages == 1) {
        _pageControl.hidden = YES;
    }
    _scrollView.contentSize = CGSizeMake((self.frame.size.width) * 3, self.frame.size.height);
    
    [self loadData];
}

- (void)loadData
{
    _pageControl.currentPage = _curPage;
    
    if(_scrollView.subviews && _scrollView.subviews.count > 0) {
		[_scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    [self getDisplayImagesWithCurpage:_curPage];
    
    if ([_curSource count]) {
        for (int i = 0; i < 3; i ++) {
            
            EcomBCarouseInfo *carouseInfo = [_curSource objectAtIndex:i];
            
//            TKAsynImageView *asynImageView = [[[TKAsynImageView alloc] initWithFrame:_scrollView.frame] autorelease];
//            asynImageView.adjustsImageWhenHighlighted = NO;
//            asynImageView.asynImageDelegate = self;
//            asynImageView.callbackOnSetImage = self;
//            asynImageView.adorn = ZSTModuleImage(_carousBgImg);
//            //asynImageView.defaultImage = ZSTModuleImage(@"module_ecomb_default_img.png");
//            [asynImageView clear];
            
            UIImageView *asynImageView = [[UIImageView alloc] initWithFrame:_scrollView.frame];
            asynImageView.userInteractionEnabled = YES;
            
            if ([carouseInfo ecomBCarouseInfoCarouselurl].length) {
//                asynImageView.url = [NSURL URLWithString:[carouseInfo ecomBCarouseInfoCarouselurl]];
//                [asynImageView loadImage];
                [asynImageView setImageWithURL:[NSURL URLWithString:[carouseInfo ecomBCarouseInfoCarouselurl]] placeholderImage:ZSTModuleImage(@"module_ecomb_default_img.png")];
                }
            asynImageView.frame = CGRectOffset(asynImageView.frame, asynImageView.frame.size.width * i, 0);
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImageView:)];
            [asynImageView addGestureRecognizer:tap];
            [_scrollView addSubview:asynImageView];
        }
    }
    else
    {
        TKAsynImageView *asynImageView = [[TKAsynImageView alloc] initWithFrame:_scrollView.frame];
        asynImageView.adjustsImageWhenHighlighted = NO;
        asynImageView.asynImageDelegate = self;
        asynImageView.callbackOnSetImage = self;
        asynImageView.adorn = ZSTModuleImage(_carousBgImg);
//        asynImageView.defaultImage = ZSTModuleImage(@"module_ecomb_default_img.png");
        [asynImageView clear];
        //   asynImageView.url = [NSURL URLWithString:[dataDic safeObjectForKey:@"FileUrl"]];
        [asynImageView loadImage];
        asynImageView.userInteractionEnabled = NO;
        [self addSubview:asynImageView];
        
    }
    [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width, 0)];
    
}

- (void)getDisplayImagesWithCurpage:(NSInteger)page {
    
    NSInteger pre = [self validPageValue:_curPage-1];
    NSInteger last = [self validPageValue:_curPage+1];
    
    if (!_curSource) {
        _curSource = [[NSMutableArray alloc] init];
    }
    [_curSource removeAllObjects];
    
   
    if ([_carouselDataSource respondsToSelector: @selector(carouselView:didSelectedViewAtIndex:)]) {
         EcomBCarouseInfo * d = [self.carouselDataSource carouselView:self infoForViewAtIndex:pre];
        if (d == nil) {
            return;
        }
        [_curSource addObject:d];
    }
    
    [_curSource addObject:[self.carouselDataSource carouselView:self infoForViewAtIndex:_curPage]];
    [_curSource addObject:[self.carouselDataSource carouselView:self infoForViewAtIndex:last]];
    
    if (_haveTitle) {
        _titleLabel.text = [[_curSource objectAtIndex:1] ecomBCarouseInfoTitle];
    }
}


#pragma mark - TKAsynImageViewDelegate

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{
    //根据_pageControl.currentPage 打开响应的详细页
    
    if ([_carouselDelegate respondsToSelector:@selector(carouselView:didSelectedViewAtIndex:)]) {
        [_carouselDelegate carouselView:self didSelectedViewAtIndex:_pageControl.currentPage];
    }
    
}

- (void)tapImageView:(UITapGestureRecognizer *)tap
{
    if ([_carouselDelegate respondsToSelector:@selector(carouselView:didSelectedViewAtIndex:)]) {
        [_carouselDelegate carouselView:self didSelectedViewAtIndex:_pageControl.currentPage];
    }
}

#pragma mark HJManagedImageVDelegate
-(void) managedImageSet:(HJManagedImageV*)mi
{
//    NSLog(@"image width = %f,height = %f",mi.imageView.image.size.width,mi.imageView.image.size.height);
//    NSLog(@"imageView width = %f,height = %f",mi.imageView.frame.size.width,mi.imageView.frame.size.height);
    if (!iPhone5) {
        //        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        // 原代码块二
        UIImage *cutImage = [mi.imageView.image cutInFrame:self.frame cutMode:ImageCutFromCenter];
        if (mi.imageView.image != nil) {
            // 原代码块三
            //            dispatch_async(dispatch_get_main_queue(), ^{
            mi.imageView.image = cutImage;
            //                    [mi.imageView setNeedsLayout];
            //                [[SDImageCache sharedImageCache] removeImageForKey:[url absoluteString] fromDisk:NO];
            //            });
        } else {
            
        }
        //        });
    }
}

#pragma mark - UIScrollViewDelegate


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [scrollView setContentOffset:CGPointMake(scrollView.size.width, 0) animated:YES];
}



- (NSInteger)validPageValue:(NSInteger)value {
    
    if(value == -1) value = _totalPages - 1; //最后一也
    if(value == _totalPages) value = 0; //第一页
    //中间页
    
    return value;
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
    int x = aScrollView.contentOffset.x;
    if(x >= (2*aScrollView.size.width)) {
        _curPage = [self validPageValue:_curPage+1];
        [self loadData];
    }
    if(x <= 0) {
        _curPage = [self validPageValue:_curPage-1];
        [self loadData];
    }
    
}
//当页数变化时，改变scrollView的内容大小
- (void) pageTurn
{
    if (_carouselDataSource == nil) {
        return;
    }
    [_scrollView setContentOffset:CGPointMake((_scrollView.frame.size.width) * 2, 0.0f) animated:YES];
}

- (void)stopAnimation
{
    if ([_carouselTimer isValid]) {
        [_carouselTimer invalidate];
    }
}

- (void)startAnimation
{
    if (_carouselTimer == nil) {
        _carouselTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(pageTurn) userInfo:nil repeats:YES];
    }
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    UIImage *image = ZSTModuleImage(@"module_shellb_scoll_pic.png") ;
    [image drawInRect:rect];
}

//- (void)dealloc
//{
////    TKRELEASE(_carouselTimer);
////    TKRELEASE(_describeLabel);
////    TKRELEASE(_scrollView);
////    TKRELEASE(_pageControl);
////    TKRELEASE(_curSource);
////    self.carouselDataSource = nil;
////    self.carouselDelegate = nil;
//    [super dealloc];
//}

@end
