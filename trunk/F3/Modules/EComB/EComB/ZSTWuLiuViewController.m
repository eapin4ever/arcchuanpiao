//
//  ZSTWuLiuViewController.m
//  EComB
//
//  Created by LiZhenQu on 14-8-21.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTWuLiuViewController.h"

@interface ZSTWuLiuViewController ()

@end

@implementation ZSTWuLiuViewController
@synthesize orderId = _orderId;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (IS_IOS_7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"物流", nil)];
    
    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 480-15+(iPhone5?88:0)+(IS_IOS_7?20:0))];
    self.webView.backgroundColor = [UIColor clearColor];
    
    NSString *url = [NSString stringWithFormat:@"%@?ecid=%@&moduletype=%@&orderid=%@",ECommerceURL,[ZSTF3Preferences shared].ECECCID,[NSString stringWithFormat:@"%ld",(long)self.moduleType],self.orderId];

    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    [self.webView setUserInteractionEnabled:YES];  //是否支持交互
    [self.webView setDelegate:self];
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.webView];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [_webView setDataDetectorTypes:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
