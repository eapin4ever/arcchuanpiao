//
//  ZSTEComBOrderInfoViewController.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTEComBOrderInfoViewController.h"
#import "ZSTEComBInputAddressViewController.h"
#import "ZSTEComBPaymentViewController.h"

@interface ZSTEComBOrderInfoViewController ()
{
    BOOL isyinlian;
    int isjifen;
    
    int pointNum;
    float deductionAmount;
    
    BOOL isjifenpay;
    
    float tmpprice;
}

@end

@implementation ZSTEComBOrderInfoViewController
{
    PAYTYPE _paytype;
    STATE _state;
    BOOL _haveAddress;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
     _payWay = zhifubao;
    _haveAddress = NO;
    _paytype = 0;
    _address = nil;
    _totalFreight = 0.0f;
    _totalAmount = 0.0f;
    tmpprice = 0.0f;
	// Do any additional setup after loading the view.
     self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
     self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"订单详情", nil)];
    CGRect rect = self.view.bounds;
    _tableView = [[UITableView alloc]initWithFrame:rect style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.allowsSelection = YES;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.separatorColor = [UIColor clearColor];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:_tableView];
    
}
-(void)hideSubmit:(BOOL)hide
{
    _hideSubmit = hide;
    [_tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [TKUIUtil showHUD:self.view];
     _payWay = zhifubao;
    [self.engine getEcomBAddress];
    [self.engine getEcomBOrderInfoViewWithOrderID:_orderID];
    [self.engine getEcomBOrderAmountWithOrderID:_orderID];
}


- (void) backViewController
{
    ZSTECBPopOver *popOver = [[ZSTECBPopOver alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view.window addSubview:popOver];
    
    [popOver showPopOverWithTitle:@"提示"
                          message:@"亲，是否确定取消订单？"
                  backgroundImage:nil
                         delegate:self
                cancelButtonTitle:@"取消"
                otherButtonTitles:@"确定", nil];
    return;
}

- (void)getjifenPayDidSucceed:(int)pointnum deductionamount:(float)deductionamount
{
    [TKUIUtil hiddenHUD];
    pointNum = pointnum;
    deductionAmount = deductionamount;
    
    [self.tableView reloadData];
}

- (void)getjifenPayDidFailed:(int)resultCode
{
    [TKUIUtil hiddenHUD];
}

-(void)getEcomBOrderAmountdidSucceed:(float)amount totalfreight:(float)freight
{
    tmpprice = amount;
    _totalAmount = amount;
    _totalFreight = freight;
    [_tableView reloadData];
   
    ZSTEComBOrderInfoViewControllerPayCell *tableCell = (ZSTEComBOrderInfoViewControllerPayCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    [self reloadViewWith:tableCell.switchOne.status];
    
     [self.engine getjifenPayInfoWithOrderId:_orderID costamount:amount];
}
-(void)getEcomBOrderAmountdidFailed:(int)resultCode
{
    NSLog(@"获取订单总金额失败");
}
-(void)getEcomBOrderInfoViewdidSucceed:(NSArray *)array payType:(int)payType status:(int)status yinlian:(BOOL)yinlian jifenpay:(int)jifenpay
{
//    _payWay = (payType == typezhifubao||payType == typetotal)?zhifubao:huodao;
    [TKUIUtil hiddenHUD];
    switch (payType) {
        case typezhifubao:
            _payWay = zhifubao;
            break;
        case typehuodao:
            _payWay = huodao;
            break;
        case 3:
            _payWay = zhifubao;
            break;
        default:
            _payWay = huodao;
            break;
    }
    
    isyinlian = yinlian;
    isjifen = jifenpay;
    
    _paytype = payType;
    _state = status;
    _orderGools = [EcombOrderGools ecombOrderGoolsWithArray:array];
    [_tableView reloadData];
    NSLog(@"订单详情成功");
}
-(void)getEcomBOrderInfoViewdidFailed:(int)resultCode
{
    [TKUIUtil hiddenHUD];
    NSLog(@"订单详情失败");
}
-(void)getEComBAddressdidSucceed:(NSDictionary *)addressDic
{
    [TKUIUtil hiddenHUD];
    _address = (EcomBAddress *)[EcomBAddress addressWithDictonary:addressDic];
    if (_address.ecomAddress.length > 0) {
        _haveAddress = YES;
    }
    [self.tableView reloadData];
}

-(void)getEComBAddressdidFailed:(int)resultCode
{
    [TKUIUtil hiddenHUD];
    NSLog(@"EcomB 地址请求失败");
}
-(void)addressDidPressed
{
    ZSTEComBInputAddressViewController * addInputViewController  = [[ZSTEComBInputAddressViewController alloc]init];
    addInputViewController.address = _address;
    addInputViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:addInputViewController animated:YES];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return 125.0f;
    }
    else if(indexPath.section == 1)
    {
        return 250.0f;
    }
    else if(indexPath.section == 2)
    {
        return 50.0f;
    }
    else if(indexPath.section == 3)
    {
        return 100.0f;
    }
    return 0.0f;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;//0.1f;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    else if(section == 1)
    {
        return 1;
    }
    else if(section == 2)
    {
        return _orderGools.count;
    }
    else if(section == 3)
    {
        return 1;
    }
    return 0;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 4;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static NSString * ZSTEComBMyOrderViewControllerAddressCellName = @"ZSTEComBMyOrderViewControllerAddressCellName";
        ZSTEComBOrderInfoViewControllerAddressCell * cell = [tableView dequeueReusableCellWithIdentifier:ZSTEComBMyOrderViewControllerAddressCellName];
        if (!cell) {
            cell = [[ZSTEComBOrderInfoViewControllerAddressCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ZSTEComBMyOrderViewControllerAddressCellName];
            cell.delegate = self;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if (_address) {
            [cell initData:_address];
        }
        
        return  cell;
    }
    else if(indexPath.section == 1)
    {
        static NSString * ZSTEComBOrderInfoViewControllerPayCellName = @"ZSTEComBOrderInfoViewControllerPayCellName";
        ZSTEComBOrderInfoViewControllerPayCell * cell = [tableView dequeueReusableCellWithIdentifier:ZSTEComBOrderInfoViewControllerPayCellName];
        if (!cell) {
            cell = [[ZSTEComBOrderInfoViewControllerPayCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ZSTEComBOrderInfoViewControllerPayCellName];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if (_paytype > 0) {
            [cell hidden:_paytype];
        }
        
        [cell payState:isyinlian];
        
        [cell jifenpay:isjifen pointnum:pointNum deductionamount:deductionAmount];
        
         [cell setInfoStatesBlock:^(ZSTEComBOrderInfoViewControllerPayCell *cell,int states) {
             
             [self reloadViewWith:states];
         }];
        
        if (_state == statezhifuchenggong || _state == stateyiwancheng || _state == stateyifahuo) {
            
//            cell.switchOne.enabled = NO;
            cell.switchOne.userInteractionEnabled = NO;
        }
        
        cell.delegate = self;
        return  cell;
    }
    else if(indexPath.section == 2)
    {
        static NSString * ZSTEComBOrderInfoViewControllerInformationCellName = @"ZSTEComBOrderInfoViewControllerInformationCellName";
        ZSTEComBOrderInfoViewControllerInformationCell * cell = [tableView dequeueReusableCellWithIdentifier:ZSTEComBOrderInfoViewControllerInformationCellName];
        if (!cell) {
            cell = [[ZSTEComBOrderInfoViewControllerInformationCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ZSTEComBOrderInfoViewControllerInformationCellName];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        [cell reloadData:[_orderGools objectAtIndex:indexPath.row]];
        return  cell;
    }
    else if(indexPath.section == 3)
    {
        static NSString * ZSTEComBOrderInfoViewControllerResponseCellName = @"ZSTEComBOrderInfoViewControllerResponseCellName";
        ZSTEComBOrderInfoViewControllerResponseCell * cell = [tableView dequeueReusableCellWithIdentifier:ZSTEComBOrderInfoViewControllerResponseCellName];
        if (!cell) {
            cell = [[ZSTEComBOrderInfoViewControllerResponseCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ZSTEComBOrderInfoViewControllerResponseCellName];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.delegate = self;
        if (_totalFreight >= 0) {
             cell.otherLabel.text = [NSString stringWithFormat:@"（含运费%d元)",_totalFreight];
        }
        if (_totalAmount >= 0) {
                cell.priceLabel.text = [NSString stringWithFormat:@"￥%.2f",_totalAmount];
        }
        if (_state == statezhifuchenggong || _state == stateyiwancheng || _state == stateyifahuo) {
            [cell hideSubmit];
        }
        
        return  cell;
    }
    return nil;
}


- (void) reloadViewWith:(int)states
{
    if (states == 0) {
        
        isjifenpay = YES;
        
        _totalAmount = tmpprice - deductionAmount;
        
    } else {
        
        isjifenpay = NO;
        
        _totalAmount = tmpprice;
    }
    
     ZSTEComBOrderInfoViewControllerResponseCell *tableCell = (ZSTEComBOrderInfoViewControllerResponseCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:3]];
     tableCell.priceLabel.text = [NSString stringWithFormat:@"￥%.2f",_totalAmount];
}

-(void)selectedPayWay:(payWay)payWay
{
    _payWay = payWay;
}

-(void)submitBtnDidClicked
{
    if (!_haveAddress) {
        [TKUIUtil alertInWindow:@"请填写地址" withImage:nil];
        return;
    }
    
    if (isjifenpay) {
        
         [self.engine jifenUsePay:_orderID costamount:deductionAmount pointnum:pointNum];
        
        if (deductionAmount >= _totalAmount) {
            
            [self.engine jifenPay:_orderID];
            
            return;
        }
    }
    
    if (_payWay == zhifubao) {
        ZSTEComBPaymentViewController *webView = [[ZSTEComBPaymentViewController alloc] init];
        [webView setURL:[NSString stringWithFormat:ALIPAYTRADE,_orderID]];
        webView.hidesBottomBarWhenPushed = YES;
        webView.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
        [self.navigationController pushViewController:webView animated:YES];
    }
    else if(_payWay == huodao){
        NSLog(@"货到付款");
        [self.engine huodaoPay:_orderID];
    } else if(_payWay == yinlianpay){
        ZSTEComBPaymentViewController *webView = [[ZSTEComBPaymentViewController alloc] init];
        [webView setURL:[NSString stringWithFormat:@"%@orderid=%@&moduletype=%@&ecid=%@",YINLIANPAY,_orderID,@(self.moduleType),[ZSTF3Preferences shared].ECECCID]];
        webView.hidesBottomBarWhenPushed = YES;
        webView.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
        [self.navigationController pushViewController:webView animated:YES];

    }
}

- (void)jifenpayDidSucceed
{
    ZSTEComBPaymentViewController *webView = [[ZSTEComBPaymentViewController alloc] init];
    [webView setURL:[NSString stringWithFormat:PAYRESULTS,3,[ZSTF3Preferences shared].ECECCID,@(self.moduleType),_orderID]];
    webView.hidesBottomBarWhenPushed = YES;
    webView.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    [self.navigationController pushViewController:webView animated:YES];
}

- (void)jifenpayDidFailed:(int)resultCode
{
    ZSTEComBPaymentViewController *webView = [[ZSTEComBPaymentViewController alloc] init];
    [webView setURL:[NSString stringWithFormat:PAYRESULTS,2,[ZSTF3Preferences shared].ECECCID,@(self.moduleType),_orderID]];
    webView.hidesBottomBarWhenPushed = YES;
    webView.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    [self.navigationController pushViewController:webView animated:YES];

}
//-(BOOL)webController:(ZSTWebViewController *)controller webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
//{
//    if (request) {
//      NSURL * _loadingURL = [request.URL retain];
//    }
//    return YES;
//}
-(void)huodaoPayDidSucceed
{
    NSLog(@"货到付款成功");
//  [TKUIUtil alertInWindow:@"取货时带上现金哦~" withImage:nil];
    
//    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
    
    ZSTEComBPaymentViewController *webView = [[ZSTEComBPaymentViewController alloc] init];
    [webView setURL:[NSString stringWithFormat:PAYRESULTS,3,[ZSTF3Preferences shared].ECECCID,@(self.moduleType),_orderID]];
    webView.hidesBottomBarWhenPushed = YES;
    webView.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    [self.navigationController pushViewController:webView animated:YES];
}

#pragma mark - UIAlertView delegate

- (void)popOver:(ZSTECBPopOver *)popOver clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}


-(void)huodaoPayDidFailed:(int)resultCode
{
    NSLog(@"货到付款失败");
//    [TKUIUtil alertInWindow:@"货到付款失败~再试一下~" withImage:nil];
    ZSTEComBPaymentViewController *webView = [[ZSTEComBPaymentViewController alloc] init];
    [webView setURL:[NSString stringWithFormat:PAYRESULTS,2,[ZSTF3Preferences shared].ECECCID,@(self.moduleType),_orderID]];
    webView.hidesBottomBarWhenPushed = YES;
    webView.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    [self.navigationController pushViewController:webView animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//-(void)dealloc
//{
//    [_address release];
//    [_tableView release];
//    [super dealloc];
//}

@end
