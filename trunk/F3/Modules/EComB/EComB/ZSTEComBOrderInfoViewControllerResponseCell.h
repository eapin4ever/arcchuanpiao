//
//  ZSTEComBOrderInfoViewControllerResponseCell.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PMRepairButton.h>

@protocol ZSTEComBOrderInfoViewControllerResponseCellDelegate <NSObject>

-(void)submitBtnDidClicked;

@end

@interface ZSTEComBOrderInfoViewControllerResponseCell : UITableViewCell

@property (weak,nonatomic) id <ZSTEComBOrderInfoViewControllerResponseCellDelegate> delegate;
@property (strong,nonatomic) PMRepairButton * payBtn;
@property (strong,nonatomic) UILabel * priceLabel;
@property (strong,nonatomic) UILabel * otherLabel;

-(void)hideSubmit;
@end
