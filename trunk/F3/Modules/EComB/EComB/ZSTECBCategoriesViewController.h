//
//  ZSTECBCategoriesViewController.h
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECBCategoriesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@end
