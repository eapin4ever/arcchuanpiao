//
//  ZSTECBShoppingCartCell.m
//  EComB
//
//  Created by LiZhenQu on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTECBShoppingCartCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ZSTECBShoppingCartCell

@synthesize checked = _isSelected;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        int minute = 7-7;
//        CGRect indicatorFrame = CGRectMake(10, abs(self.frame.size.height - 15)/ 2, 15, 15);
//        m_checkImageView = [[[UIImageView alloc] initWithFrame:indicatorFrame] autorelease];
//        m_checkImageView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
//        [m_checkImageView setImage:ZSTModuleImage(@"module_ecomb_btn_unsel@2x.png")];
//        [self.contentView addSubview:m_checkImageView];
        
        _imgSelectionMark = [[PMRepairImageView alloc] initWithFrame:CGRectMake(25-minute, 27, 15, 15)];
        _imgSelectionMark.alpha = 1.0f;
        [_imgSelectionMark setImage:ZSTModuleImage(@"module_ecomb_btn_sel.png")];
        [self addSubview:_imgSelectionMark];
        
        _selectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _selectBtn.frame = CGRectMake(-minute, 10, 65, 50);
        _selectBtn.backgroundColor = [UIColor clearColor];
        [self addSubview:_selectBtn];

//        _productImgView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(20-minute, 8, 55, 55)] autorelease];
//        _productImgView.adorn = ZSTModuleImage(@"module_ecoma_iconImageBg.png");
//        _productImgView.asynImageDelegate = self;
        _productImgView = [[UIImageView alloc] initWithFrame:CGRectMake(20-minute, 8, 55, 55)];
        _productImgView.userInteractionEnabled = NO;
        _productImgView.backgroundColor = [UIColor clearColor];
        _productImgView.layer.cornerRadius = 4.0f;
        _productImgView.clipsToBounds = YES;
        [self.contentView addSubview:_productImgView];
        
        _productNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(88-minute, 8, 200, 20)];
        _productNameLabel.backgroundColor = [UIColor clearColor];
        _productNameLabel.textColor = [UIColor blackColor];
        _productNameLabel.font = [UIFont systemFontOfSize:13];
        _productNameLabel.text = @"iphone5s";
        [self.contentView addSubview:_productNameLabel];
        
        UILabel *markLabel = [[UILabel alloc] initWithFrame:CGRectMake(88-minute, 45, 15, 20)];
        markLabel.backgroundColor = [UIColor clearColor];
        markLabel.textColor = RGBCOLOR(214, 84, 88);
        markLabel.font = [UIFont systemFontOfSize:15];
        markLabel.text = @"￥";
        [self.contentView addSubview:markLabel];
        
        _priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(markLabel.frame), 43, 100, 20)];
        _priceLabel.backgroundColor = [UIColor clearColor];
        _priceLabel.textColor = RGBCOLOR(214, 84, 88);
        _priceLabel.font = [UIFont systemFontOfSize:13];
        _priceLabel.text = @"1.00";
        [self.contentView addSubview:_priceLabel];
        
        _pointLabel = [[UILabel alloc] init];
        _pointLabel.hidden = YES;
        _pointLabel.backgroundColor = RGBCOLOR(248, 121, 41);
        _pointLabel.textColor = [UIColor whiteColor];
        _pointLabel.textAlignment = NSTextAlignmentCenter;
        _pointLabel.font = [UIFont systemFontOfSize:11];
        _pointLabel.text = @"积分换购";
        _pointLabel.frame = CGRectMake(CGRectGetMaxX(_priceLabel.frame)+2, _priceLabel.frame.origin.y, 50, 18);
        [self.contentView addSubview:_pointLabel];
        
        _reductionbtn = [PMRepairButton buttonWithType:UIButtonTypeCustom];
        _reductionbtn.frame = CGRectMake(173-minute+10, 40, 30, 30);
        [_reductionbtn setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 10, 10)];
        _reductionbtn.tag = 2013;
        [_reductionbtn setImage:ZSTModuleImage(@"module_ecomb_btn_reduction.png") forState:UIControlStateNormal];
        [_reductionbtn addTarget:self action:@selector(changeCountAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_reductionbtn];
        
        UIImageView *countbg = [[UIImageView alloc] initWithFrame:CGRectMake(197-minute+10, 45, 35, 15)];
        countbg.backgroundColor = [UIColor whiteColor];
        countbg.layer.cornerRadius = 4.0f;
        countbg.clipsToBounds = YES;
        [countbg.layer setBorderWidth:1];
        [countbg.layer setBorderColor:[[UIColor grayColor] CGColor]];
        [self.contentView addSubview:countbg];

        _countLabel = [[UILabel alloc] initWithFrame:CGRectMake(197-minute+10, 45, 35, 15)];
        _countLabel.backgroundColor = [UIColor clearColor];
        _countLabel.font = [UIFont systemFontOfSize:10];
        _countLabel.textAlignment = NSTextAlignmentCenter;
        _countLabel.text = @"1";
        [self.contentView addSubview:_countLabel];
        
        _addbtn = [PMRepairButton buttonWithType:UIButtonTypeCustom];
        _addbtn.frame = CGRectMake(232-minute+10, 40, 30, 30);
        [_addbtn setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 10, 10)];
        _addbtn.tag = 2014;
        [_addbtn setImage:ZSTModuleImage(@"module_ecomb_btn_add.png") forState:UIControlStateNormal];
        [_addbtn addTarget:self action:@selector(changeCountAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_addbtn];
        
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(-7 - minute, 69, 280, 1)];
        line.backgroundColor =  RGBCOLOR(239, 239, 239);
        [self.contentView addSubview:line];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//- (void) setEditing:(BOOL)editting animated:(BOOL)animated
//{
//    //不要破坏原本的系统动作
//	[super setEditing:editting animated:animated];
//    //进入编辑状态
//    //背景视图生成，以准备设置选中和未选中的不同背景色
//    self.backgroundView = [[UIView alloc] init];
//    self.backgroundView.backgroundColor = [UIColor whiteColor];
//    _imgSelectionMark = [[UIImageView alloc] initWithFrame:CGRectMake(25, abs(self.frame.size.height - 15)/ 2, 15, 15)];
//    _imgSelectionMark.alpha = 1.0f;
//    [self addSubview:_imgSelectionMark];
//    //更新选中与否的界面显示
//    [self setChecked:_isSelected];
//}

- (void)setChecked:(BOOL)checked
{
    //选中
	if (!checked)
	{
        //勾选的图标
		_imgSelectionMark.image = ZSTModuleImage(@"module_ecomb_btn_sel.png");
	}
    //反选
	else
	{
        //反选的图标
		_imgSelectionMark.image = ZSTModuleImage(@"module_ecomb_btn_unsel.png");
	}
	
    //需要记录到成员量中
    _isSelected = checked;
    
}
-(void)loadData:(EcomBShopingCarInfo *)shopCarInfo isAdd:(BOOL)isAdd
{
    _shopCarInfo = shopCarInfo;
    
    _priceLabel.text = [NSString stringWithFormat:@"%.2f",shopCarInfo.shopingCarInfoPrice];
//    _productImgView.url = [NSURL URLWithString:shopCarInfo.shopingCarInfoImageStr];
//    [_productImgView loadImage];
    [_productImgView setImageWithURL:[NSURL URLWithString:shopCarInfo.shopingCarInfoImageStr]];
    _productNameLabel.text = shopCarInfo.shopingCarInfoProductName;
    _pointLabel.hidden = !_shopCarInfo.jifenflag;
    _countLabel.text = [NSString stringWithFormat:@"%@",@(shopCarInfo.shopingCarInfoCount)];
    
    CGSize size = [_priceLabel.text sizeWithFont:[UIFont systemFontOfSize:13]];
    _priceLabel.frame = CGRectMake(_priceLabel.frame.origin.x, _priceLabel.frame.origin.y, size.width, 18);
    _pointLabel.frame = CGRectMake(CGRectGetMaxX(_priceLabel.frame)+2, _priceLabel.frame.origin.y, 50, 18);
    
//    if ((_delegate != nil) && [_delegate respondsToSelector:@selector(shoppingCartCell:price:)] && isAdd) {
//        [_delegate shoppingCartCell:self price:-1 * price];
//    }
    if (shopCarInfo.shopingCarInfoCount > 0) {
        [self setChecked:NO];
    }
    else
    {
        [self setChecked:YES];
    }
}
- (void)changeCountAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    int count = [_countLabel.text intValue];
    if (button.tag == 2013) {
        _addbtn.enabled = YES;
        count --;
    } else if (button.tag == 2014) {
        _reductionbtn.enabled = YES;
        if (count == _shopCarInfo.shopingCarInfoStock) {
            [TKUIUtil alertInWindow:@"这已经是最大数量了" withImage:nil];
            _addbtn.enabled = NO;
            return;
        }
        count ++;
    }
    
    if (count <= 1) {
        count = 1;
        _reductionbtn.enabled = NO;
    }
    
    float valueprice = ([_countLabel.text intValue] - count) * [_priceLabel.text floatValue];
    _countLabel.text = [NSString stringWithFormat:@"%d",count];
    _shopCarInfo.shopingCarInfoCount = count;

    
    if (!_isSelected) {
    
        if ((_delegate != nil) && [_delegate respondsToSelector:@selector(shoppingCartCell:price:)]) {
            [_delegate shoppingCartCell:self price:valueprice];
        }
    }
}

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{
    
}


@end
