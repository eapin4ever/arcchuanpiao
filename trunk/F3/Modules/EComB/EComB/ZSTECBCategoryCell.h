//
//  ZSTECBCategoryCell.h
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    CustomCellPositionTop,
    CustomCellPositionMiddle,
    CustomCellPositionBottom,
    CustomCellPositionSingle,
    CustomCellPositionNone
} CustomCellPosition;

@interface ZSTECBCategoryCell : UITableViewCell
{
    BOOL        _isSelected;
}

@property (nonatomic) CustomCellPosition position;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *iconImg;
@property (nonatomic, assign) BOOL checked;

- (void)setChecked:(BOOL)checked;

- (void)configCell:(NSDictionary *)category forRowAtIndex:(NSIndexPath *)indexPath;

@end
