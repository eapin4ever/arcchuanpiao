//
//  ZSTECBProductTableImageCellView.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECBProductTableImageCellView : UIView
//@property (retain,nonatomic) TKAsynImageView * imageView;
@property (strong,nonatomic) UIImageView *imageView;
@property (strong,nonatomic) UILabel * textLabel;
@property (strong,nonatomic) UILabel * priceLabel;
@property (strong, nonatomic) UILabel * originalPriceLabel;
@property (strong,nonatomic)UILabel * slip;
@property (strong, nonatomic) UIView *pointview;

-(void)reloadData:(EComBHomePageInfo *) info;
@end
