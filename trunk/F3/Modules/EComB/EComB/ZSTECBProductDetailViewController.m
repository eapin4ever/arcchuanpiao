//
//  ZSTECBProductDetailViewController.m
//  EComB
//
//  Created by LiZhenQu on 14-3-6.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTECBProductDetailViewController.h"
#import "ZSTF3Engine+EComB.h"

@interface ZSTECBProductDetailViewController ()<UIWebViewDelegate>
{
    UIWebView *webView;
}

@property (nonatomic, retain) UIWebView *webView;

@end

@implementation ZSTECBProductDetailViewController
@synthesize webView = _webView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"商品详情", nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];

    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 480-20-44+(iPhone5?88:0))];
    _webView.backgroundColor = [UIColor clearColor];
    _webView.delegate = self;
    [self.view addSubview:_webView];
    
    [self.engine getProductsDetail:_productID];
}

- (void)webViewDidStartLoad:(UIWebView *)webView //网页加载时调用
{
}

- (void)webViewDidFinishLoad:(UIWebView *)webView //网页完成加载时调用
{
}

- (void)getProductsContentDidSucceed:(NSString *)newsContent
{
    NSString *html = [NSString stringWithFormat:@"<html><head><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'><meta http-equiv='Cache-Control' content='max-age=0'><meta http-equiv='Cache-Control' content='must-revalidate'><meta http-equiv='Cache-Control' content='no-cache'>%@</body><ml>",newsContent];
    
     [_webView loadHTMLString:html baseURL:nil];
}

- (void)getProductsContentDidFailed:(int)resultCode
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) dealloc
{
    [_webView setDelegate:nil];
}

@end
