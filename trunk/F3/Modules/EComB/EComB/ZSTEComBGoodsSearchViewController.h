//
//  ZSTEComBGoodsSearchViewController.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTECBProductTableImageCell.h"

@interface ZSTEComBGoodsSearchViewController :UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,ZSTECBProductTableImageCellDelegate,ZSTF3EngineECBDelegate,TKLoadMoreViewDelegate>
@property (strong,nonatomic)UIView * headView;
@property (strong,nonatomic)UITableView * tableView;
@property (strong,nonatomic)NSMutableArray * resultData;
@end
