//
//  ZSTECBProductTableImageButton.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTECBProductTableImageButton.h"

@implementation ZSTECBProductTableImageButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.titleLabel.font = [UIFont systemFontOfSize:11];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self setTitleColor:[UIColor colorWithRed:43.0/255.0f green:43.0/255.0f blue:43.0/255.0f alpha:1.0f] forState:UIControlStateNormal];
    }
    return self;
}
-(CGRect)titleRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(0, 60 - 18 - 10 , 60, 18);
}

-(CGRect)imageRectForContentRect:(CGRect)contentRect
{
    return CGRectMake((60 - 17)/2, 10, 17, 17);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
