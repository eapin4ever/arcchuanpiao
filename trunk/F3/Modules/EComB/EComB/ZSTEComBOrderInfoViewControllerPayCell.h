//
//  ZSTEComBOrderInfoViewControllerPayCell.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTCustomSwitch.h"
#import <PMCustomSwitch.h>

typedef enum
{
    zhifubao = 0,
    huodao = 1,
    yinlianpay
}payWay;
@protocol ZSTEComBOrderInfoViewControllerPayCellDelegate <NSObject>
-(void)selectedPayWay:(payWay) payWay;
@end

@class ZSTEComBOrderInfoViewControllerPayCell;
typedef  void (^EcbinfoStatesBlock)(ZSTEComBOrderInfoViewControllerPayCell *cell,int states);

@interface ZSTEComBOrderInfoViewControllerPayCell : UITableViewCell<PMCustomSwitchDelegate>
{
     EcbinfoStatesBlock statesBlock;
}

@property (strong,nonatomic) UIImageView * zhifubaoImageView;
@property (strong,nonatomic) UIImageView * huodaoImageView;
@property (strong,nonatomic) UIImageView * yinlianImageView;
//@property (nonatomic, strong) ZSTCustomSwitch *switchOne;
@property (nonatomic,strong) PMCustomSwitch *switchOne;
@property (nonatomic, strong) UILabel *jifenLabel;
@property (nonatomic,weak) id <ZSTEComBOrderInfoViewControllerPayCellDelegate> delegate;
@property (nonatomic, assign) CGFloat cellHeight;

-(void)hidden:(int) state;

- (void)payState:(BOOL)state;

- (void)jifenpay:(int)state pointnum:(int)pointnum deductionamount:(float)deductionamount;

- (void)setInfoStatesBlock:(EcbinfoStatesBlock)block;

@end
