//
//  ZSTECBProductTableImageCell.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTECBProductTableImageCell.h"

@implementation ZSTECBProductTableImageCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        _leftView = [[ZSTECBProductTableImageCellView alloc]initWithFrame:CGRectMake(13, 10, 140, 200)];
//        _leftView.backgroundColor = [UIColor yellowColor];
        [self.contentView addSubview:_leftView];
        
        UIControl * controlLeft = [[UIControl alloc]initWithFrame:_leftView.bounds];
        controlLeft.tag = 0;
        [controlLeft addTarget:self action:@selector(viewDidClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_leftView addSubview:controlLeft];
        
        _rightView = [[ZSTECBProductTableImageCellView alloc]initWithFrame:CGRectMake(320 - 13 - 140, 10, 140, 200)];
//        _rightView.backgroundColor = [UIColor yellowColor];
        [self.contentView addSubview:_rightView];
        
        UIControl * controlRight = [[UIControl alloc]initWithFrame:_rightView.bounds];
        controlRight.tag = 1;
        [controlRight addTarget:self action:@selector(viewDidClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_rightView addSubview:controlRight];
        
        _leftView.hidden = YES;
        _leftView.userInteractionEnabled = NO;
        
        _rightView.hidden = YES;
        _rightView.userInteractionEnabled = NO;
    }
    return self;
}

-(void)viewDidClicked:(UIControl * )control
{
    if (control.tag == 0) {
        [_delegate ZSTECBProductTableImageCellDidSelect:_leftView.tag];
        //NSLog(@"_left %d",_leftView.tag);
    }
    else
    {
        [_delegate ZSTECBProductTableImageCellDidSelect:_rightView.tag];
       // NSLog(@"_right %d",_rightView.tag);
    }
}

-(BOOL)reloadDataWithLeft:(EComBHomePageInfo *) left Right:(EComBHomePageInfo *) right
{
    _leftView.hidden = YES;
    _leftView.userInteractionEnabled = NO;
    
    _rightView.hidden = YES;
    _rightView.userInteractionEnabled = NO;
    
    if (left) {
        _leftView.hidden = NO;
        _leftView.userInteractionEnabled = YES;
        [_leftView reloadData:left];
    } else {
        
        _leftView.hidden = YES;
        _leftView.userInteractionEnabled = NO;
    }
    if (right) {
        
        [_rightView reloadData:right];
        _rightView.hidden = NO;
        _rightView.userInteractionEnabled = YES;
    } else {
        
        _rightView.hidden = YES;
        _rightView.userInteractionEnabled = NO;
        
    }
    return NO;
}

//-(void)dealloc
//{
//    [_leftView release];
//    [_rightView release];
//    [super dealloc];
//}
//


@end
