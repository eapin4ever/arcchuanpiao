//
//  ZSTECBIconView.m
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTECBIconView.h"
#import "ZSTEComBCategoriesShowViewController.h"

@implementation ZSTECBIconView

@synthesize titleLabel;
@synthesize category;

- (void)initUI
{
    self.backgroundColor = [UIColor redColor];
    
    self.iconView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(5, 0, 80, 25)];
    self.iconView.adorn = ZSTModuleImage(@"module_ecomb_iconImageBg.png");
    self.iconView.asynImageDelegate = self;
    self.iconView.userInteractionEnabled = NO;
    self.iconView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconView];
    self.iconView.layer.cornerRadius = 4.0f;
    self.iconView.clipsToBounds = YES;
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.frame = self.iconView.frame;
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = RGBCOLOR(173, 173, 173);
    self.titleLabel.highlightedTextColor = RGBCOLOR(120, 120, 120);
    self.titleLabel.font = [UIFont systemFontOfSize:13];
//    self.titleLabel.shadowColor = [UIColor blackColor];
//    self.titleLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
    [self addSubview:self.titleLabel];
    
    [self addTarget:self action:@selector(asynImageViewDidClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.category = [[NSNumber alloc] initWithInteger:0];
}

- (id)init
{
    self = [super init];
    if (self) {
        [self initUI];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:CGRectMake(frame.origin.x, frame.origin.y, 60, [[NSUserDefaults standardUserDefaults] boolForKey:@"showImgKey"]?62:30)];
}

- (CGRect)frame
{
    return CGRectMake(super.frame.origin.x, super.frame.origin.y, 60, [[NSUserDefaults standardUserDefaults] boolForKey:@"showImgKey"]?62:30);
}

- (void)layoutSubviews
{
    [super layoutSubviews];
//    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"showImgKey"]) {
//        self.showsTouchWhenHighlighted = NO;
//        self.iconView.hidden = NO;
//        self.iconView.frame = CGRectMake(5, 0, 80, 25);
//        self.titleLabel.frame = CGRectMake(5, 0, 80, 25);
//        self.titleLabel.font = [UIFont systemFontOfSize:13];
//    } else {
//        self.showsTouchWhenHighlighted = YES;
////        self.iconView.hidden = YES;
//        self.titleLabel.frame = CGRectMake(0, 10, 60, 12);
//        self.titleLabel.font = [UIFont systemFontOfSize:12];
//    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    self.iconView.highlighted = highlighted;
    self.titleLabel.highlighted = highlighted;
    [self setNeedsDisplay];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    self.iconView.selected = selected;
    [self setNeedsDisplay];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];
    self.iconView.enabled = enabled;
    [self setNeedsDisplay];
}

-(void)setIcon:(NSString *)imageUrl
{
    [self.iconView clear];
    self.iconView.url = [NSURL URLWithString:imageUrl];
    [self.iconView loadImage];
}

- (void)setTitle:(NSString *)title;
{
    titleLabel.text = title;
}

- (void)setCategoryID:(NSNumber *)number
{
    self.category = number;
}

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{
    ZSTEcomBCategoriesShowViewController * categoriesShowViewController = [[ZSTEcomBCategoriesShowViewController alloc] init];
//    productTableViewController.categoryID = [self.category integerValue];
    categoriesShowViewController.categoryid = [self.category stringValue];
    categoriesShowViewController.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(self.titleLabel.text, nil)];
    categoriesShowViewController.hidesBottomBarWhenPushed = YES;
    [self.viewController.navigationController pushViewController:categoriesShowViewController animated:YES];
}

@end
