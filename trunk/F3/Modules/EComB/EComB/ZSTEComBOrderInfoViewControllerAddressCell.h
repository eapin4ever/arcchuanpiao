//
//  ZSTEComBOrderInfoViewControllerAddressCell.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZSTEComBOrderInfoViewControllerAddressCellDelegate <NSObject>

-(void)addressDidPressed;

@end
@interface ZSTEComBOrderInfoViewControllerAddressCell : UITableViewCell
@property (assign,nonatomic) id<ZSTEComBOrderInfoViewControllerAddressCellDelegate> delegate;
@property (retain,nonatomic) UILabel * addressLabel;
@property (retain,nonatomic) UILabel * tellLabel;

-(void) initData:(EcomBAddress *)address;
@end
