//
//  ZSTRoundRectView.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-5.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTRoundRectView : UIView

//画圆角边框
- (id)initWithFrame:(CGRect)frame radius:(CGFloat) radius  borderWidth:(CGFloat) border borderColor:(UIColor *) color;
//画线
- (id)initWithPoint:(CGPoint) beginPoint toPoint:(CGPoint) endPoint borderWidth:(CGFloat) border borderColor:(UIColor *) color;
- (void) showImage:(CGRect)frame image:(UIImage *) image;
- (void) showText:(CGRect) frame  text:(NSString *) text;
- (void) showText:(CGRect) frame  text:(NSString *) text font:(UIFont *)font;
@end
