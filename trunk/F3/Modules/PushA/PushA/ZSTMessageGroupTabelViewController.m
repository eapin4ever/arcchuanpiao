//
//  MessageGroupTabelViewController.m
//  F3_UI
//
//  Created by mac on 11-7-2.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "ZSTMessageGroupTabelViewController.h"
#import "ZSTMessageTableViewCell.h"
#import "TKUIUtil.h"
#import "MessageInfo.h"
#import "ZSTMessageDetailViewController.h"
#import "ZSTLauncherButtonView.h"
#import <QuartzCore/QuartzCore.h>
#import "ZSTCreateNMSDialogViewController.h"
#import "MsgTypeInfo.h"
#import "MessageDao.h"
#import "ZSTUtils.h"
#import "ZSTLogUtil.h"
#import "ZSTEditView.h"
#import <PMRepairButton.h>
#import "ZSTMessageTypeViewController.h"

@implementation ZSTMessageGroupTabelViewController

@synthesize info = _info;

#define BARBUTTON(TITLE, SELECTOR)	[[[UIBarButtonItem alloc] initWithTitle:TITLE style:UIBarButtonItemStylePlain target:self action:SELECTOR] autorelease]//UIBarButtonItem

- (id)init
{
    self = [super init];
    if (self) {
//        UIBarButtonItem *editButton = BARBUTTON(@"选项", @selector(edit));
//        self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:@"选项" target:self selector:@selector (edit)];;
        [self setRightBarButtonItem];
    }
    return self;
}

- (void)setRightBarButtonItem
{
//    UIButton *optionsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    PMRepairButton *optionsBtn = [[PMRepairButton alloc] init];
    optionsBtn.frame = CGRectMake(280, 7, 30, 30);
    optionsBtn.imageEdgeInsets = UIEdgeInsetsMake(6, 6, 6, 6);
    [optionsBtn setImage:[UIImage imageNamed:@"icon_edit_n.png"] forState:UIControlStateNormal];
    [optionsBtn setImage:[UIImage imageNamed:@"icon_edit_p.png"] forState:UIControlStateHighlighted];
    [optionsBtn addTarget:self action:@selector(edit) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:optionsBtn] autorelease];
}

- (void)updateFooterView
{
    if ([_receiveArray count] == 0) {
        _backgroundView.image = [UIImage imageNamed:@"nullNMS.png"];
        
    } else {
     
        _backgroundView.image = [UIImage imageNamed:@"bg.png"];
    }
}

- (void)updateTableView
{
    [_receiveArray release];
    _receiveArray = nil;
    _receiveArray = (NSMutableArray *)[[[MessageDao shareMessageDao] getInMessagesOfType:_info.typeID] retain];
    
    [_groupTaleView reloadData];
    [self updateFooterView];
}

-(void)bannerClick
{
//    if (![self.info.typeID isEqualToString:@"0"]) {
//        WebBrowserController *webBrowser = [[WebBrowserController alloc] initWithURL:[NSURL URLWithString: 
//                                                                                      [NSString stringWithFormat:@"http://ec.pmit.cn/%@",self.info.typeID]]];
//        
//        [self presentModalViewController:webBrowser animated:YES];
//        
//        [webBrowser release];
//    }
   
}
- (void)onNMSUpdate:(NSNotification *) notification
{
    [self performSelectorOnMainThread:@selector(updateTableView) withObject:nil waitUntilDone:NO];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _isLock = NO;
    _editStateMark = NO;
    _isEditing = NO;
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
	self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:self.info.typeName];
    
    _groupTaleView = [[UITableView alloc] initWithFrame: CGRectMake(0, 0, 320, self.view.height+(IS_IOS_7?20:0)) style: UITableViewStylePlain];
    _groupTaleView.backgroundColor = [UIColor clearColor];
    _groupTaleView.separatorColor = [UIColor clearColor];
    _groupTaleView.dataSource = self;
    _groupTaleView.delegate = self;
    _groupTaleView.autoresizingMask = UIViewAutoresizingFlexibleHeight;

    _backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nullNMS.png"]];
    _backgroundView.frame = CGRectMake(0, 0, 320, self.view.height);
    _backgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight;

    [self.view addSubview:_backgroundView];

    [self.view addSubview: _groupTaleView];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(onNMSUpdate:) 
                                                 name: NotificationName_SwitchNMSListUpdated 
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(onNMSUpdate:) 
                                                 name: NotificationName_InboxUnreadNMSChanged 
                                               object: nil];
    
    [ZSTLogUtil logUserAction:NSStringFromClass([ZSTMessageGroupTabelViewController class])];
}

#pragma mark ---------------------  EditView ------------------------------------------

-(void)delSingleMSGAction
{
    [_editView dismiss];
    [self setEditing:NO animated:YES];
    [self setEditing:YES animated:YES]; 
    _isEditing = YES;
    
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"完成", nil) target:self selector:@selector (endDelete)];
    [self updateTableView];
    
}
-(void)confirmDelAllMSGAction
{
    [ZSTF3Engine deleteMessagesOfType:self.info.typeID];
    
    postNotificationOnMainThreadNoWait(NotificationName_NMSListUpdated);
    
    [self updateFooterView];
    [self updateTableView];
    [_confirmDelView dismiss];
    
}
-(void)cancelDelAllMSGAction
{
    [_confirmDelView dismiss];
}
-(void)delAllMSGAction
{
    [_editView dismiss];
    [self setEditing:NO animated:YES]; 
    _isEditing = NO;
    
    UIImage *image = [UIImage imageNamed:@"confirmDelAll.png"];
    _confirmDelView  = [[ZSTEditView alloc] initWithImage:image];
    _confirmDelView.center = CGPointMake(320/2, (480-20-44)/2);
    
    UIButton *confirmDelMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmDelMSGBtn.frame = CGRectMake(6, 50, 98, 55);
    [confirmDelMSGBtn addTarget:self action:@selector(confirmDelAllMSGAction) forControlEvents:UIControlEventTouchUpInside];
    confirmDelMSGBtn.showsTouchWhenHighlighted = YES;
    confirmDelMSGBtn.backgroundColor = [UIColor clearColor];
    [_confirmDelView addSubview:confirmDelMSGBtn];
    
    UIButton *cancelDelAllMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelDelAllMSGBtn.frame = CGRectMake(106, 50, 98, 55);
    [cancelDelAllMSGBtn addTarget:self action:@selector(cancelDelAllMSGAction) forControlEvents:UIControlEventTouchUpInside];
    cancelDelAllMSGBtn.showsTouchWhenHighlighted = YES;
    cancelDelAllMSGBtn.backgroundColor = [UIColor clearColor];
    [_confirmDelView addSubview:cancelDelAllMSGBtn];
    
    [_confirmDelView show];
    [self updateTableView];
}
-(void)lockMSGAction
{
    [_editView dismiss];
    [self setEditing:NO animated:YES]; 
    [self setEditing:YES animated:YES]; 
    _isEditing = YES;
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"完成", nil) target:self selector:@selector (endDelete)];
    
    //编辑锁定状态
    _isLock = YES;
    [self updateTableView];
    
}
-(void)statusMSGAction
{
    [_editView dismiss];
    [self setEditing:NO animated:YES]; 
    [self setEditing:YES animated:YES]; 
    _isEditing = YES;
    self.navigationItem.rightBarButtonItem = nil;

    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"完成", nil) target:self selector:@selector (endDelete)];
    
    //编辑已读未读状态
    _editStateMark = YES;
    [self updateTableView];
    
}

-(void)endDelete
{
    [self setEditing:NO animated:YES];
    self.navigationItem.rightBarButtonItem = nil;

    [self setRightBarButtonItem];
    
    _isLock = NO;
    _editStateMark = NO;
    _isEditing = NO;
    [_groupTaleView reloadData];
    
    
}
-(void)cancelMSGAction
{
    [_editView dismiss];
    [self setEditing:NO animated:YES]; 
    [self setEditing:YES animated:YES]; 
    _isEditing = YES;
    
    [self endDelete];
    [self updateTableView];
    
}

-(void) edit
{  
    UIImage *image = nil;
    
    NSString *GP_Show_NewMessage = NSLocalizedString(@"GP_Show_NewMessage", nil);
    if ([GP_Show_NewMessage isEqualToString:@"0"]) {
        image = [UIImage imageNamed:@"edit_right_cancel.png"];
        
    }else{
        image = [UIImage imageNamed:@"edit_right_new.png"];
    }
    _editView = [[ZSTEditView alloc] initWithImage:image];
    _editView.frame = CGRectMake(175, 51, image.size.width, image.size.height);
    
    UIButton *lockMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    lockMSGBtn.frame = CGRectMake(6, 20, 65, 55);
    [lockMSGBtn addTarget:self action:@selector(lockMSGAction) forControlEvents:UIControlEventTouchUpInside];
    lockMSGBtn.showsTouchWhenHighlighted = YES;
    lockMSGBtn.backgroundColor = [UIColor clearColor];
    [_editView addSubview:lockMSGBtn];
    
    UIButton *statusMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    statusMSGBtn.frame = CGRectMake(73, 20, 65, 55);
    [statusMSGBtn addTarget:self action:@selector(statusMSGAction) forControlEvents:UIControlEventTouchUpInside];
    statusMSGBtn.showsTouchWhenHighlighted = YES;
    statusMSGBtn.backgroundColor = [UIColor clearColor];
    [_editView addSubview:statusMSGBtn];
    
    
    UIButton *delAllMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    delAllMSGBtn.frame = CGRectMake(6, 76, 65, 55);
    [delAllMSGBtn addTarget:self action:@selector(delAllMSGAction) forControlEvents:UIControlEventTouchUpInside];
    delAllMSGBtn.showsTouchWhenHighlighted = YES;
    delAllMSGBtn.backgroundColor = [UIColor clearColor];
    [_editView addSubview:delAllMSGBtn];
    
    UIButton *createNMS_cancel_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    createNMS_cancel_Btn.frame = CGRectMake(73, 76, 65, 55);
    
    if ([GP_Show_NewMessage isEqualToString:@"0"]) {
        
        [createNMS_cancel_Btn addTarget:self action:@selector(cancelMSGAction) forControlEvents:UIControlEventTouchUpInside];
        
    }else{
        [createNMS_cancel_Btn addTarget:self action:@selector(createNMS) forControlEvents:UIControlEventTouchUpInside];
    }
    
    createNMS_cancel_Btn.showsTouchWhenHighlighted = YES;
    createNMS_cancel_Btn.backgroundColor = [UIColor clearColor];
    [_editView addSubview:createNMS_cancel_Btn];
    
    [_editView show];
    
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
	[_groupTaleView setEditing:editing animated:animated];
}


#pragma mark -------------------------UITableViewDataSource---------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CustomTableViewCell";
    
    ZSTMessageTableViewCell *cell = (ZSTMessageTableViewCell *)[tableView dequeueReusableCellWithIdentifier: cellIdentifier];
    
    if (!cell)
    {
        cell = [[[ZSTMessageTableViewCell alloc] initWithStyle: UITableViewCellStyleDefault 
                                   reuseIdentifier: cellIdentifier] autorelease];
        
        cell.backgroundView = [[[UIView alloc] init] autorelease];
        cell.backgroundView.backgroundColor = [UIColor whiteColor];
    }
    
    NSString *GP_Show_NewMessage = NSLocalizedString(@"GP_Show_NewMessage", nil);
    if ([GP_Show_NewMessage isEqualToString:@"0"]) {
        [cell reLayout];
    }
    
    cell.isLock = _isLock;
    cell.editStateMark = _editStateMark;
    cell.inEditing = _isEditing;
    
    MessageInfo *nmsInfo = [_receiveArray objectAtIndex:indexPath.row];
    [cell setNMSInfo: nmsInfo isLock:_isLock editStateMark:_editStateMark];
    
    return cell;
        
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_receiveArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return CELL_HEIGHT;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTMessageTableViewCell *cell = (ZSTMessageTableViewCell *)[tableView cellForRowAtIndexPath: indexPath];
    
    [self.typeDelegate refresh];

    MessageInfo *nmsInfo = [_receiveArray objectAtIndex: indexPath.row];
    
    if (cell.selectionStyle == UITableViewCellSelectionStyleNone || nmsInfo.state == NMSStateReceiving )
    {
        return;
    }else if (nmsInfo.state == NMSStateReceivedFailed)
    {
        cell.labelIsRead.text = NSLocalizedString(@"[正在下载]" , nil);
        [self.engine reReceiveMessage:nmsInfo.pushId];
        return;
    }
    
    BOOL shouldSendNotification = (nmsInfo.state == NMSStateNotRead);
    
    nmsInfo.state = NMSStateRead;
    
    if (shouldSendNotification) {
        [[MessageDao shareMessageDao] updateMessageById:nmsInfo];
        postNotificationOnMainThreadNoWait(NotificationName_UnreadNMSChanged);
        postNotificationOnMainThreadNoWait(NotificationName_GroupUnreadNMSChanged);
    }
    
	NSString *inboxPath = [ZSTUtils pathForInbox];
	NSString * str = [inboxPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",nmsInfo.pushId]];
	NSString * displayString = [str stringByAppendingPathComponent:@"Index.htm"];
    
	ZSTMessageDetailViewController * messageViewController = [[ZSTMessageDetailViewController alloc] init];
    messageViewController.subject = nmsInfo.subject;
	[messageViewController view];  ////////////////@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
    messageViewController.userPhoneNumber = nmsInfo.userID;
    messageViewController.msgID = nmsInfo.MSGID;
    messageViewController.pushId = nmsInfo.pushId;

	[messageViewController  setFilePathString:displayString];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:NSLocalizedString(@"MM月dd日 HH:mm", nil)];
    NSString *dateString = [formatter stringFromDate:nmsInfo.time];
    
    messageViewController.sendDate.text = dateString;
    
    [formatter release];
    
    self.navigationController.navigationBarHidden = NO;
    messageViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:messageViewController animated:YES ];

	[messageViewController release];
    

    [cell setNMSInfo: nmsInfo isLock:NO editStateMark:NO];
    messageViewController.inboxName.text = cell.labelName.text;
    messageViewController.iconImageView.image = cell.leftImageView.image;

}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTMessageTableViewCell *cell = (ZSTMessageTableViewCell *)[tableView cellForRowAtIndexPath: indexPath];
    MessageInfo *nmsInfo = [_receiveArray objectAtIndex: indexPath.row];
    
	if (editingStyle == UITableViewCellEditingStyleDelete && !_editStateMark && !_isLock) 
	{
        [ZSTF3Engine deleteMessage: nmsInfo];
		
		[_receiveArray removeObjectAtIndex:indexPath.row];
        [tableView setEditing:NO animated:YES];

		[tableView deleteRowsAtIndexPaths: [NSArray arrayWithObject: indexPath] withRowAnimation: UITableViewRowAnimationFade];
        
        [self updateTableView];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_UnreadNMSChanged object:nil];        
	}
	else if (editingStyle == UITableViewCellEditingStyleDelete && _editStateMark)
	{
        if (nmsInfo.state == NMSStateRead) {
            nmsInfo.state = NMSStateNotRead;

        }else if(nmsInfo.state == NMSStateNotRead) 
        {
            nmsInfo.state = NMSStateRead;
            
        }
        [[MessageDao shareMessageDao] updateMessageById:nmsInfo];
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_UnreadNMSChanged object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_GroupUnreadNMSChanged object:nil];
        
        
        [cell setNMSInfo: nmsInfo isLock:_isLock editStateMark:_editStateMark];
        
	}else if (editingStyle == UITableViewCellEditingStyleDelete && _isLock)
    {
        
        MessageInfo *nmsInfo = [_receiveArray objectAtIndex: indexPath.row];
        
        
        if (nmsInfo.isLocked == NO) {
            nmsInfo.isLocked = YES;
        }else if( nmsInfo.isLocked == YES) 
        {
            nmsInfo.isLocked = NO;
        }
        
        [[MessageDao shareMessageDao] updateMessageById:nmsInfo];
        
        [cell setNMSInfo: nmsInfo isLock:_isLock editStateMark:_editStateMark];
        
        postNotificationOnMainThreadNoWait(NotificationName_NMSListUpdated);
    }
    
    [UIView beginAnimations:@"endEditingCell" context:nil];
    [UIView setAnimationDuration:0.5];
    
    CGRect labelNameFrame = cell.labelName.frame;
    labelNameFrame.size.width += 85;
    cell.labelName.frame = labelNameFrame;
    
    CGRect labelDateFrame = cell.labelDate.frame;
    labelDateFrame.origin.x += 85;
    cell.labelDate.frame = labelDateFrame;
    
    CGRect labelTitleFrame = cell.labelTitle.frame;
    labelTitleFrame.size.width += 85;
    cell.labelTitle.frame = labelTitleFrame;
    
    [UIView commitAnimations];
    
    [tableView reloadData];
    
}
#pragma mark-----UITableViewDelegate-----------------

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MessageInfo *nmsInfo = [_receiveArray objectAtIndex: indexPath.row];
    
    if (!_isLock && !_editStateMark && nmsInfo.isLocked == YES) 
    {
        return UITableViewCellEditingStyleNone;
    }
    return UITableViewCellEditingStyleDelete;
    
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MessageInfo *nmsInfo = [_receiveArray objectAtIndex: indexPath.row];
    
    if (_editStateMark && nmsInfo.state == NMSStateNotRead) {
        
        return NSLocalizedString(@"设为已读" , nil);
    }else if(_editStateMark && nmsInfo.state == NMSStateRead)
    {
        return NSLocalizedString(@"设为未读" , nil);
    }else if(_isLock && nmsInfo.isLocked == NO)
    {
        return NSLocalizedString(@"锁定信息" , nil);
    }else if(_isLock && nmsInfo.isLocked == YES)
    {
        return NSLocalizedString(@"解除锁定" , nil);
    }else if(_editStateMark && (nmsInfo.state == NMSStateReceivedFailed || nmsInfo.state == NMSStateReceiving || nmsInfo.state == 0))
    {
        return NSLocalizedString(@"无法更改" , nil);
    }
    return NSLocalizedString(@"删除" , nil);
}

-(void)backAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateTableView];
}

-(void) createNMS
{  
    [_editView dismiss];
    [self setEditing:NO animated:YES];
    _isLock = NO;
    _editStateMark = NO;
    _isEditing = NO;
    
    ZSTCreateNMSDialogViewController *creatNMSDialog = [[ZSTCreateNMSDialogViewController alloc] init];
    creatNMSDialog.shouldSaveToDraft = NO;
    creatNMSDialog.isDraftEdit = NO;
    [creatNMSDialog pushInNavigationViewController:self];
    [creatNMSDialog release];
}
- (void)dealloc
{
    [_backgroundView release];
    [_receiveArray release];
    [_info release];
    [_newNMSButton release];
    [_groupTaleView release];
    [super dealloc];
}

@end

