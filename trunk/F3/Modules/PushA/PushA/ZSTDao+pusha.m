//
//  ZSTDao.m
//  News
//  
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTDao+pusha.h"
#import "ZSTSqlManager.h"
#import "ZSTUtils.h"

#define CREATE_TABLE_CMD                                        @"CREATE TABLE IF NOT EXISTS [MessageInfo] (\
                                                                [ID] INTEGER PRIMARY KEY,\
                                                                [MSGID] TEXT  NULL DEFAULT '',\
                                                                [pushId] TEXT NULL DEFAULT '',\
                                                                [typeId] TEXT NULL DEFAULT '',\
                                                                [userID] TEXT NULL DEFAULT '',\
                                                                [userName] TEXT NULL DEFAULT '',\
                                                                [subject] TEXT NULL DEFAULT '',\
                                                                [time] double NULL DEFAULT 0,\
                                                                [content] TEXT NULL DEFAULT '',\
                                                                [forwarded] TEXT NULL DEFAULT '',\
                                                                [encryptKey] TEXT NULL DEFAULT '',\
                                                                [isReceived] INTEGER DEFAULT 0,\
                                                                [isPrivate] INTEGER DEFAULT 0,\
                                                                [isLocked] INTEGER DEFAULT 0,\
                                                                [state] INTEGER NULL DEFAULT 0,\
                                                                [report] INTEGER NULL DEFAULT 0,\
                                                                [notifyFrom] INTEGER NULL DEFAULT 0,\
                                                                [remark] TEXT NULL DEFAULT '',\
                                                                [fileId] TEXT NULL DEFAULT ''\
                                                                ); \
                                                                CREATE TABLE IF NOT EXISTS [MsgTypeInfo]\
                                                                (\
                                                                [ID] INTEGER PRIMARY KEY,\
                                                                [typeId] TEXT NOT NULL UNIQUE DEFAULT '',\
                                                                [typeName] TEXT NOT NULL DEFAULT '',\
                                                                [ecId] TEXT NULL DEFAULT '',\
                                                                [iconKey] TEXT NULL DEFAULT '',\
                                                                [version] INTEGER NULL DEFAULT 0,\
                                                                [isShielded] INTEGER NULL DEFAULT 0,\
                                                                [orderMum] INTEGER NULL DEFAULT 0\
                                                );"

TK_FIX_CATEGORY_BUG(pusha)

@implementation ZSTDao(pusha)

+ (void)createTableIfNotExistForPushaModule
{
    [ZSTSqlManager executeSqlWithSqlStrings:CREATE_TABLE_CMD];
}

@end
