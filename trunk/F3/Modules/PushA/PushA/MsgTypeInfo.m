//
//  ECECCInfo.m
//  F3_UI
//
//  Created by  on 11-8-24.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "MsgTypeInfo.h"

@implementation MsgTypeInfo

@synthesize ID;
@synthesize typeID;
@synthesize typeName;
@synthesize ecId;
@synthesize iconKey;
@synthesize isShielded;
@synthesize version;
@synthesize orderMum;

-(NSString *) description
{
	NSMutableString *desc = [[NSMutableString alloc] initWithFormat: 
                             @"ID = %@, typeID = %@, typeName = %@, ecId = %@, iconKey = %@, isShielded = %@, version = %@",
                               @(ID),typeID,typeName,ecId,iconKey,@(isShielded),@(version)];
	return [desc autorelease];
}

- (void)dealloc
{
    self.typeID = nil;
    self.typeName = nil;
    self.ecId = nil;
    self.iconKey = nil;
    [super dealloc];
}

@end
