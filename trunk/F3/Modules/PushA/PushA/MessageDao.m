//
//  InboxDao.m
//  F3
//
//  Created by 9588 on 9/26/11.
//  Copyright 2011 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "MessageDao.h"
#import "ZSTSqlManager.h"
#import "ZSTF3Preferences.h"
#import "ZSTUtils.h"

@implementation MessageDao

+ (MessageDao *)shareMessageDao
{
    static MessageDao *dao;
    if (dao == nil) {
        @synchronized(self) {
            if (dao == nil) {
                dao = [[MessageDao alloc] init];
            }
        }
    }
    return dao;
}

- (NSInteger)insertMessage:(MessageInfo *)nmsInfo
{
    if (nmsInfo.ID > 0) {
        return [ZSTSqlManager executeInsert:@"insert or replace into messageInfo (MSGID, pushId, typeId, state, userID, userName, encryptKey, subject, time, forwarded, isReceived, isPrivate, isLocked, content, fileId, report, ID) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                [ZSTUtils objectForArray:nmsInfo.MSGID],
                [ZSTUtils objectForArray:nmsInfo.pushId],
                [ZSTUtils objectForArray:nmsInfo.typeId],
                [NSNumber numberWithInt:nmsInfo.state],
                nmsInfo.userID,
                [ZSTUtils objectForArray:nmsInfo.userName],
                [ZSTUtils objectForArray:nmsInfo.encryptKey],
                [ZSTUtils objectForArray:nmsInfo.subject],
                [ZSTUtils objectForArray:nmsInfo.time],
                [ZSTUtils objectForArray:nmsInfo.forwarded],
                [NSNumber numberWithInt:nmsInfo.isReceived],
                [NSNumber numberWithInt:nmsInfo.isPrivate],
                [NSNumber numberWithInt:nmsInfo.isLocked],
                [ZSTUtils objectForArray:nmsInfo.content],
                [ZSTUtils objectForArray:nmsInfo.fileId],
                [NSNumber numberWithBool:nmsInfo.report],
                [NSNumber numberWithInteger:nmsInfo.ID]];
        
    } else {
        return [ZSTSqlManager executeInsert:@"insert or replace into messageInfo (MSGID, pushId, typeId, state, userID, userName, encryptKey, subject, time, forwarded, isReceived, isPrivate, isLocked, content, fileId, report) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                [ZSTUtils objectForArray:nmsInfo.MSGID],
                [ZSTUtils objectForArray:nmsInfo.pushId],
                [ZSTUtils objectForArray:nmsInfo.typeId],
                [NSNumber numberWithInt:nmsInfo.state],
                nmsInfo.userID,
                [ZSTUtils objectForArray:nmsInfo.userName],
                [ZSTUtils objectForArray:nmsInfo.encryptKey],
                [ZSTUtils objectForArray:nmsInfo.subject],
                [ZSTUtils objectForArray:nmsInfo.time],
                [ZSTUtils objectForArray:nmsInfo.forwarded],
                [NSNumber numberWithInt:nmsInfo.isReceived],
                [NSNumber numberWithInt:nmsInfo.isPrivate],
                [NSNumber numberWithInt:nmsInfo.isLocked],
                [ZSTUtils objectForArray:nmsInfo.content],
                [ZSTUtils objectForArray:nmsInfo.fileId],
                [NSNumber numberWithBool:nmsInfo.report]];
    }
}

-(void) deleteAllMessage
{
    // 从数据库中删除
    NSString * sql = @"Delete from messageInfo where isLocked = 0" ;
    [ZSTSqlManager executeUpdate:sql];
}

-(void) deleteMessage: (NSInteger) ID
{
    // 从数据库中删除
    NSString * sql = [NSString stringWithFormat: @"Delete from messageInfo where ID = %ld" , (long)ID];
    [ZSTSqlManager executeUpdate:sql];
    
}

-(BOOL) deleteMessageBeforeDate:(NSDate *)date;
{
    NSString *sql = @"Delete from messageInfo where time < ? and isLocked = 0";//只保留最近的15天的记录
    
    return [ZSTSqlManager executeUpdate:sql, date];
}

-(void)deleteMessagesOfType:(NSString *) typeId
{
    //根据ECECCID来删除信息
    NSString * sql = @"Delete from messageInfo where typeId = ? and isLocked = 0";
    [ZSTSqlManager executeUpdate:sql, typeId];
}

-(BOOL) setInMessage: (NSString *)pushId state:(NMSState)state
{
    NSString *sql = @"Update messageInfo Set state = ? where pushId = ?";
    
    return [ZSTSqlManager executeUpdate:sql, [NSNumber numberWithInt:state], pushId];
}

- (BOOL)updateMessageById:(MessageInfo *)nmsInfo
{
    NSString *sql = @"update messageInfo set MSGID = ?, State = ?, userID = ?, userName = ?, encryptKey = ?, subject = ?, time = ?,  typeId = ?, isLocked = ?, report = ?, pushId = ?, isReceived = ?, forwarded = ?, content = ?, fileId = ?, isPrivate = ? where ID =  ?";
    
    return [ZSTSqlManager executeUpdate:sql,
            [ZSTUtils objectForArray:nmsInfo.MSGID],
            [NSNumber numberWithInt:nmsInfo.state],
            [ZSTUtils objectForArray:nmsInfo.userID],
            [ZSTUtils objectForArray:nmsInfo.userName],
            [ZSTUtils objectForArray:nmsInfo.encryptKey],
            [ZSTUtils objectForArray:nmsInfo.subject],
            nmsInfo.time,
            [ZSTUtils objectForArray:nmsInfo.typeId],
            [NSNumber numberWithInt:nmsInfo.isLocked],
            [NSNumber numberWithBool:nmsInfo.report],
            [ZSTUtils objectForArray:nmsInfo.pushId],
            [NSNumber numberWithInt:nmsInfo.isReceived],
            [ZSTUtils objectForArray:nmsInfo.forwarded],
            [ZSTUtils objectForArray:nmsInfo.content],
            [ZSTUtils objectForArray:nmsInfo.fileId],
            [NSNumber numberWithInt:nmsInfo.isPrivate],
            [NSNumber numberWithInteger:nmsInfo.ID]];
}

- (BOOL)updateInMessageByPushId:(MessageInfo *)nmsInfo
{
    NSString *sql = @"update messageInfo set MSGID = ?, State = ?, userID = ?, userName = ?, encryptKey = ?, subject = ?, time = ?,  typeId = ?, isLocked = ?, report = ? where pushId = ? and isReceived = 1";
    return [ZSTSqlManager executeUpdate:sql,
            nmsInfo.MSGID,
            [NSNumber numberWithInt:nmsInfo.state],
            nmsInfo.userID,
            [ZSTUtils objectForArray:nmsInfo.userName],
            [ZSTUtils objectForArray:nmsInfo.encryptKey],
            nmsInfo.subject,
            nmsInfo.time,
            nmsInfo.typeId,
            [NSNumber numberWithInt:nmsInfo.isLocked],
            [NSNumber numberWithBool:nmsInfo.report],
            nmsInfo.pushId];
}

- (MessageInfo *)_populateMessageFromResultSet:(NSDictionary *)rs
{
    MessageInfo *messageInfo = [[MessageInfo alloc] init];
    messageInfo.ID = [[rs safeObjectForKey:@"ID"] integerValue];
    messageInfo.MSGID = [rs safeObjectForKey:@"MSGID"];
    messageInfo.pushId = [rs safeObjectForKey:@"pushId"];
    messageInfo.state  = [[rs safeObjectForKey:@"state"] integerValue];
    messageInfo.userID = [rs safeObjectForKey:@"userID"];
    messageInfo.userName = [rs safeObjectForKey:@"userName"];
    messageInfo.encryptKey = [rs safeObjectForKey: @"encryptKey"];
    messageInfo.typeId = [rs safeObjectForKey:@"typeId"];
    messageInfo.subject =  [rs safeObjectForKey: @"subject"];
    messageInfo.time = [NSDate dateWithTimeIntervalSince1970:[[rs safeObjectForKey:@"time"] floatValue]];
    messageInfo.isReceived = [[rs safeObjectForKey:@"isReceived"] boolValue];
    messageInfo.report = [[rs safeObjectForKey:@"report"] boolValue];
    messageInfo.isLocked = [[rs safeObjectForKey:@"isLocked"] boolValue];
    messageInfo.isPrivate = [[rs safeObjectForKey:@"isPrivate"] boolValue];
    messageInfo.forwarded = [rs safeObjectForKey:@"forwarded"];
    messageInfo.content = [rs safeObjectForKey:@"content"];
    messageInfo.fileId = [rs safeObjectForKey:@"fileId"];
    return [messageInfo autorelease];
}

-(NSArray *) getMessages
{
    NSMutableArray *nmsList = [NSMutableArray array];
    NSString *sql = @"Select id, MSGID, pushId, state, userID, userName, encryptKey, typeId, subject, time, isLocked, isReceived, report, isPrivate,forwarded, content, fileId from messageInfo order by time desc, id desc";
    NSArray *resultSet = [ZSTSqlManager executeQuery:sql];
    
    for (NSDictionary *rs in resultSet) {
        [nmsList addObject: [self _populateMessageFromResultSet:rs]];
    }
    return nmsList;
}

-(MessageInfo *) getInMessageByPushId: (NSString *)pushId
{
    NSString *sql = @"Select id, MSGID, pushId, state, userID, userName, encryptKey, typeId, subject, time, isLocked, isReceived, report, isPrivate,forwarded, content, fileId from messageInfo where isReceived = 1 and pushId = ?";
    NSArray * resultSet = [ZSTSqlManager executeQuery:sql, pushId];
    for (NSDictionary *rs in resultSet) {
        return [self _populateMessageFromResultSet:rs];
    }
    return nil;
}

-(NSArray *) getInMessagesOfType: (NSString *)typeId
{
    NSMutableArray *nmsListArray =[NSMutableArray array];
    NSString *sql;
    //个人f3信息
    sql = @"Select id, MSGID, pushId, state, userID, userName, encryptKey, typeId,  subject, time, isLocked, isReceived, report, isPrivate,forwarded, content, fileId from messageInfo where typeId = ? and isReceived = 1 order by time desc, pushId desc";
    NSArray * resultSet = [ZSTSqlManager executeQuery:sql, typeId];
    for (NSDictionary *rs in resultSet) {
        [nmsListArray addObject: [self _populateMessageFromResultSet:rs]];
    }
    return nmsListArray;
}

-(NSArray *) getMessagesBeforeDate:(NSDate *)date
{
    NSMutableArray *nmsListArrayOfExpired =[NSMutableArray array];
    
    NSString *sql = @"Select id, MSGID, pushId, State, userID, userName, encryptKey, typeId,  subject, time, isLocked, isReceived ,report, isPrivate,forwarded, content, fileId from messageInfo where isLocked = 0";
    
    NSArray *resultSet = [ZSTSqlManager executeQuery:sql];
    
    for (NSDictionary *rs in resultSet) {
        [nmsListArrayOfExpired addObject: [self _populateMessageFromResultSet:rs]];
    }
    return nmsListArrayOfExpired;
}

-(NSArray *) getMessagesOfState: (NMSState)state
{
    NSMutableArray *nmsListArrayOfExpired =[NSMutableArray array];
    NSString *sql = @"Select id, MSGID, pushId, State, userID, userName, encryptKey, typeId,  subject, time, isLocked, isReceived,report, isPrivate,forwarded, content, fileId from messageInfo where State = ? order by time desc, pushId desc";
    NSArray *resultSet = [ZSTSqlManager executeQuery:sql, [NSNumber numberWithInt:state]];
    for (NSDictionary *rs in resultSet) {
        [nmsListArrayOfExpired addObject: [self _populateMessageFromResultSet:rs]];
    }
    return nmsListArrayOfExpired;
}

-(NSUInteger) getCountOfType: (NSString *)typeId state:(NMSState)state
{
    NSString *sql = @"Select count(pushId) from messageInfo where state = ? and  typeId = ? and isReceived = 1";
    return [ZSTSqlManager intForQuery:sql,[NSNumber numberWithInt:state], typeId];
}

-(NSUInteger) getCount:(NMSState)state
{
    NSString *sql = @"Select count(id) from messageInfo where state = ?";
    
    return [ZSTSqlManager intForQuery:sql, [NSNumber numberWithInt:state]];
}

-(NSUInteger) getCount
{
    NSString *sql = @"Select count(id) from messageInfo";
    
    return [ZSTSqlManager intForQuery:sql];
}

- (BOOL)InMessageExist:(NSString *)pushId
{
    NSString *sql = @"Select count(pushId) from messageInfo where pushId = ? and isReceived = 1";
    
    return [ZSTSqlManager intForQuery:sql, pushId] != 0;
}

//设置发件状态
- (BOOL)setOutMessage:(NSInteger)ID MSGID:(NSString *)MSGID withState:(NMSState)state
{
    NSString *sql = @"Update messageInfo Set MSGID = ?,state = ? where ID = ? and isReceived = 0";
    
    return [ZSTSqlManager executeUpdate:sql, [ZSTUtils objectForArray:MSGID], [NSNumber numberWithInt:state], [NSNumber numberWithInteger:ID]];
}

// 修改发件状态
- (BOOL) updateOutMessageMSGID:(NSString *)MSGID withState:(NMSState)state
{
    NSString *sql = @"Update messageInfo Set state = ? where MSGID = ? and isReceived = 0";
    
    return [ZSTSqlManager executeUpdate:sql, [NSNumber numberWithInt:state],[ZSTUtils objectForArray:MSGID]];
}

//设置群发消息状态
- (BOOL)setOutGroupMessage:(int)ID withState:(NMSState)state
{
    NSString *sql = @"Update messageInfo Set state = ? where ID = ? and isReceived = 0 and report = 1";
    
    return [ZSTSqlManager executeUpdate:sql, [NSNumber numberWithInt:state], [NSNumber numberWithInt:ID]];
}

//获取重发信息的info
- (MessageInfo *)getOutMessageInfoByID:(int)ID
{
    NSString *sql = @"Select id, MSGID, pushId, State, userID, userName, encryptKey, typeId, subject, time, isLocked, isReceived, report, isPrivate,forwarded, content, fileId from messageInfo where isReceived = 0 and ID = ?";
    
    NSArray *resultSet = [ZSTSqlManager executeQuery:sql, [NSNumber numberWithInt:ID]];
    
    for (NSDictionary *rs in resultSet) {
        return [self _populateMessageFromResultSet:rs];
    }
    return nil;
}

@end
