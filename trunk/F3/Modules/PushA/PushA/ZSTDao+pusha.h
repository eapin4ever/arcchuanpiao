//
//  ZSTDao.h
//  News
//  
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTDao(pusha)

+ (void)createTableIfNotExistForPushaModule;

@end
