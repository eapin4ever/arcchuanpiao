//
//  Create_Dialog.m
//  Net_Information
//
//  Created by huqinghe on 11-6-9.
//  Copyright 2011 Shinetechchina.com. All rights reserved.
//

#import "ZSTCreateNMSDialogViewController.h"
#import "ContactPickerViewController.h"
#import "ContactInfo.h"
#import "TKUIUtil.h"
#import <QuartzCore/QuartzCore.h>
#import "MessageInfo.h"
#import "EGOTextView.h"
#import "ContactData.h"
#import "ImageEdgeView.h"
#import "PickerDataSource.h"
#import "ZSTLimitedPickerTextField.h"
#import "TTPickerViewCell.h"
#import "ZSTUtils.h"
#import "ZSTLogUtil.h"
#import "ZSTAudioAverageView.h"
#import "ZSTAttachmentView.h"
#import "ZSTEditView.h"
#import "TTTableSubtitleItem.h"
#import "ZSTPictureViewer.h"
#import "ZSTF3ClientAppDelegate.h"
#import "UIImage+Compress.h"

#import<MobileCoreServices/MobileCoreServices.h>
#import <MediaPlayer/MediaPlayer.h>


#define choosePhotoBtn 0
#define chooseVideoBtn 1
#define chooseAudioBtn 2

#define attachmentHeight 40

#define ROOTFILEPATH [ZSTUtils pathForTempFinderOfOutbox]
#define IMAFEPATH [ROOTFILEPATH stringByAppendingPathComponent:[ZSTUtils imageDateString]]
#define AUDIOPATH [ROOTFILEPATH stringByAppendingPathComponent:[ZSTUtils audioDateString]]
#define VIDEOPATH [ROOTFILEPATH stringByAppendingPathComponent:[ZSTUtils videoDateString]]
#define isRetina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)


#define YMIN	10.0f

@implementation ZSTCreateNMSDialogViewController

@synthesize subject = _subject;
@synthesize contactTextField=_contactTextField;
@synthesize pushId = _pushId;
@synthesize image = _image;
@synthesize textview ;
@synthesize shouldSaveToDraft = _shouldSaveToDraft;
@synthesize isDraftEdit = _isDraftEdit;
@synthesize nmsID = _nmsID;
@synthesize background1 = _background1;
@synthesize attachmentViewArray = _attachmentViewArray;


@synthesize session;
@synthesize recorder;
//@synthesize audioPlayer;


#pragma mark --------------AttachmentView-----------------------
#pragma mark -

- (void)hideEmoticonsView{
    
    [emoticonsView dismissAsKeyboard:YES];
    [emoticonsView release];
    emoticonsView = nil;
}

-(void)customTextViewResignFirstResponder
{
    [textview resignFirstResponder];
    if ([_backgruodTextField isFirstResponder]) {
        [_backgruodTextField resignFirstResponder];
    }
}

#pragma mark ----------------- 表情——————————————————————————————————————————————————————————————————————————————————————————

- (void)didSelectAFace:(id)sender{
    
    UIButton *button = (UIButton *)sender;
    [textview insertText:[_keysArray objectAtIndex:button.tag]];
    //textview.text = [NSString stringWithFormat:@"%@%@",textview.text,[_keysArray objectAtIndex:button.tag]];
}
////////////添加表情

-(void)expressionAction:(id) sender      
{
    if (emoticonsView && [emoticonsView superview]) {
        return;
    }
    
    [self customTextViewResignFirstResponder];
    if ([_contactTextField isFirstResponder]) {
        [_contactTextField resignFirstResponder];
    }
    
    self.navigationItem.rightBarButtonItem = nil;
    
    UIButton *doneButton = [ZSTUtils customButtonTitle:nil
                                                nImage:[UIImage imageNamed:@"keyboard.png"]
                                                pImage:[UIImage imageNamed:@"keyboard.png"] 
                                         titleFontSize:13
                                           conerRsdius:5.0];
    
    [doneButton addTarget:self action:@selector(leaveEditMode) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
    self.navigationItem.rightBarButtonItem = doneBarButton;
    [doneBarButton release];
    
    emoticonsView = [[UIView alloc] initWithFrame:CGRectMake(0,480, 320.0f, 216.0f)];
    
    [emoticonsView presentAsKeyboardInView:self.view];
    
    emoticonsView.backgroundColor = [UIColor whiteColor];
    [emoticonsView.layer setMasksToBounds:YES];
    
    emoticonsScrollView = [[UIScrollView alloc] init];
    emoticonsScrollView.delegate = self;
    emoticonsScrollView.pagingEnabled = YES;//按页显示
    emoticonsScrollView.showsHorizontalScrollIndicator = NO;
    
    emoticonsScrollView.frame = CGRectMake(0, 0 ,320.0f, 196.0f);
    emoticonsScrollView.backgroundColor = [UIColor clearColor];
    
    [emoticonsView addSubview:emoticonsScrollView];
    
    ImageEdgeView *imageEdgeView = [[ImageEdgeView alloc] initWithFrame:CGRectMake(0, 0, 320*4, 196)];
    [imageEdgeView setNeedsDisplay];
    [emoticonsScrollView addSubview:imageEdgeView];
    [imageEdgeView release];
    
    for (int i = 0 ; i<4 ; i++ ) {
        
        
        for (int j = 0; j < 24  ; j++) {
            
            if ( 24* i + j < [_imagesArray count]) {
                UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                
                button.frame = CGRectMake(1+j*40, 6+i*49, 40.0f, 40.0f);
                button.imageEdgeInsets = UIEdgeInsetsMake(5, 5, 5, 5);
                NSString *imageName = [_imagesArray objectAtIndex:24*i+j];
                
                UIImage *image = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imageName ofType:@"png"]];
                
                [button setImage:image forState:UIControlStateNormal];
                
                [image release];
                
                button.tag = 24*i+j;
                
                [button addTarget:self action:@selector(didSelectAFace:)forControlEvents:UIControlEventTouchUpInside];
                
                [emoticonsScrollView addSubview:button];
            }
            
        }
    }
    
    [emoticonsScrollView setContentSize:CGSizeMake(320.0f*3, 196)];
    
    _pageControl = [[UIPageControl alloc] initWithFrame: CGRectMake(0, 196, 320, 20)];
    _pageControl.numberOfPages = 3;
    _pageControl.backgroundColor = [UIColor colorWithRed:35/255.0 green:118/255.0 blue:187/255.0 alpha:1];
    
    [_pageControl addTarget:self action:@selector(pageTurn:) forControlEvents:UIControlEventValueChanged];
    [emoticonsView addSubview: _pageControl]; 
    
}

- (CGFloat)topOfLine:(NSInteger)lineNumber {
    if (lineNumber == 0) {
        return 0;
        
    } else {
        CGFloat lineTop =  attachmentHeight*lineNumber;
        return lineTop ;
    }
}

- (void)scrollToEditingLine:(BOOL)animated {
    
    if (_attachmentScrollView != nil) {
        NSInteger _lineCount = [_attachmentViewArray count];
        CGFloat offset = (_lineCount == 1||_lineCount == 2) ? 0 : [self topOfLine:_lineCount-3];
        [_attachmentScrollView setContentOffset:CGPointMake(0,offset) animated:animated];
    }
}

- (void)updateAttachmentView
{    
    if (_filePath == nil) {
        return;
    }
    
    if (!_attachmentScrollView) {
        _attachmentScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 355, 320, attachmentHeight)];
        _attachmentScrollView.backgroundColor = [UIColor clearColor];
        _attachmentScrollView.delegate = self;
        _attachmentScrollView.showsVerticalScrollIndicator = YES;
        [self.view addSubview:_attachmentScrollView];  
    }
    
    NSArray *attachmentFileName = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[ZSTUtils pathForTempFinderOfOutbox] error: nil];
    NSInteger attachmentViewCount = [attachmentFileName count];
    
    if (attachmentViewCount <= 3) {
        _attachmentScrollView.frame = CGRectMake(0, 355-attachmentHeight*(attachmentViewCount-1), 320, attachmentHeight*attachmentViewCount);
    }else{
        _attachmentScrollView.contentSize = CGSizeMake(320, attachmentHeight*attachmentViewCount);
    }
    
    if (_keyboardIsShowing) {
        [self customTextViewResignFirstResponder];
        [_contactTextField resignFirstResponder];
        if (emoticonsView != nil) {
            [self hideEmoticonsView];
        }
    }
    
    CGRect attchmetViewFrame = CGRectMake(0, 0+(attachmentViewCount-1)*attachmentHeight, 330, attachmentHeight);
    
    ZSTAttachmentView *attachmentView = [[ZSTAttachmentView alloc] initWithFrame:attchmetViewFrame];
    attachmentView.delegate = self;
    attachmentView.imageView.tag = attachmentViewCount-1;
    //    attachmentView.count.text = [NSString stringWithFormat:@"%d",attachmentViewCount];
    attachmentView.imageNameLabel.text = [NSString stringWithFormat:@"大小:%@KB",@([ZSTUtils fileSize:_filePath])];
    
    if ([[_filePath lastPathComponent] hasSuffix:@"jpg"]) {
        
        [attachmentView.imageView addTarget:self action:@selector(handleImageViewSingleTap:) forControlEvents:UIControlEventTouchUpInside]; 
        [attachmentView.imageView setBackgroundImage:_image forState:UIControlStateNormal];
        
    }else if ([[_filePath lastPathComponent] hasSuffix:@"wav"]) {
        
        [attachmentView.imageView setBackgroundImage:[UIImage imageNamed:@"voice_attachmet_icon.png"] forState:UIControlStateNormal];
        [attachmentView.imageView addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
        
    }else if ([[_filePath lastPathComponent] hasSuffix:@"mp4"]) {
        [attachmentView.imageView setBackgroundImage:[UIImage imageNamed:@"video_attachment_icon.png"] forState:UIControlStateNormal];
        [attachmentView.imageView addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [_attachmentViewArray addObject:attachmentView];
    [_attachmentScrollView addSubview:attachmentView];
    [attachmentView release];
    
    float textFieldOriginHeight = _subjectBackgroundView.hidden ? 338:294;
    
    CGRect backgruodTextFieldFrame = _backgruodTextField.frame;
    backgruodTextFieldFrame.size.height = (textFieldOriginHeight - _attachmentScrollView.frame.size.height);
    _backgruodTextField.frame = backgruodTextFieldFrame;
    
    CGRect  background1_frame = _background1.frame;
    background1_frame.origin.y = 396+(iPhone5?88:0);
    _background1.frame = background1_frame;
    
    if (attachmentViewCount >3) {
        [self scrollToEditingLine:YES];
    }
}

- (void)reAddAttachment
{
    NSString *sendPath = [ZSTUtils pathForSendFileOfOutbox:[self.nmsID intValue]];
    NSString *tempPath = [ZSTUtils pathForTempFinderOfOutbox];
    NSArray *attachmentNameArray = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:sendPath error:nil];
    
    for (NSString *attachmentName in attachmentNameArray) {
        
        NSString *attachmentPathStr = [sendPath stringByAppendingPathComponent:attachmentName];
        NSString *attachmentTempPath = [tempPath stringByAppendingPathComponent:attachmentName];
        [[NSFileManager defaultManager] copyItemAtPath:attachmentPathStr toPath:attachmentTempPath error:nil];
        
        _filePath = [attachmentPathStr retain];
        
        if ([attachmentName hasSuffix:@"jpg"]) {
            [_image release];
            _image = [[UIImage imageWithContentsOfFile:attachmentTempPath] retain];
        }
        
        [self updateAttachmentView];
    }
}

#pragma mark ----------------------------------------------------------------------

- (NSString *) formatTime: (int) num
{
	int secs = num % 60;
	int min = num / 60;
	if (num < 60) return [NSString stringWithFormat:@"0:%02d", num];
	return	[NSString stringWithFormat:@"%d:%02d", min, secs];
}

//-(void)updateMetersForAudioPlayer:(NSTimer *)timer
//{	
//	[self.audioPlayer updateMeters];
//   ((AttachmentView *)timer.userInfo).imageNameLabel.text = [NSString stringWithFormat:@"%@", [self formatTime:self.audioPlayer.currentTime]];
//}

- (void) updateMeters
{
	[self.recorder updateMeters];
	float avg = [self.recorder averagePowerForChannel:0];
    float topImageWidth = meter.topImageView.image.size.width;
    float topImageHeight = meter.topImageOriginHeight;
    
    //这里需要适配一下，如果是高分辨率得就是两倍
    
    if (isRetina)
    {
        avg = 2*avg;
        topImageWidth = 2*topImageWidth;
        topImageHeight = 2*topImageHeight;
    }
    
    [meter updateTopImageViewRect:CGRectMake(0, -avg, topImageWidth, topImageHeight+avg)];

    NSString *recorderCurrenttime = [self formatTime:self.recorder.currentTime];
    
	meter.timelabel.text = recorderCurrenttime;
}

#pragma mark -------------------AudioButtonAction-----------------------------------------------------

- (void) continueRecording
{
	[self.recorder record];
    recordTimer = [NSTimer scheduledTimerWithTimeInterval:0.0f target:self selector:@selector(updateMeters) userInfo:nil repeats:YES];
    meter.pauseButton.hidden = NO;
    meter.continueButton.hidden = YES;
}

- (void) pauseRecording
{
	[self.recorder pause];
    
    //    NSInteger recorderCurrenttime = [[[NSString stringWithFormat:@"%@", [self formatTime:self.recorder.currentTime]] substringFromIndex:2] intValue];
    //    NSString *remainingTime = [NSString stringWithFormat:@"剩余 0:%02d" ,(59-recorderCurrenttime)];
    NSString *recorderCurrenttime = [self formatTime:self.recorder.currentTime];
    
	meter.timelabel.text = recorderCurrenttime;
    
    [recordTimer invalidate];
    recordTimer = nil;
    meter.pauseButton.hidden = YES;
    meter.continueButton.hidden = NO;
}
- (void) stopRecording
{   
    if (recordTimer != nil) {
        [recordTimer invalidate];
        recordTimer = nil;
    }
    
    [meter dismiss];
    meter = nil;
	[self.recorder stop];
    
    recorder = nil;
    [[AVAudioSession sharedInstance] setActive: NO error: nil];
}

- (BOOL) recordAudio
{
	NSError *error;
	
	// Recording settings//PCM脉冲编码调制
	NSMutableDictionary *settings = [[NSMutableDictionary alloc] init];
	[settings setValue: [NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];//音频格式（此处为非常大的文件）
	[settings setValue: [NSNumber numberWithFloat:8000.0] forKey:AVSampleRateKey];//每秒采样8000次（采样率）
	[settings setValue: [NSNumber numberWithInt: 1] forKey:AVNumberOfChannelsKey]; // mono单声道
	[settings setValue: [NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];//线性pcm位深度（采样位数）
	[settings setValue: [NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
	[settings setValue: [NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];//采样信号是整数还是浮点数
	
    [_filePath release];
    _filePath = [AUDIOPATH retain];
    
	NSURL *url = [NSURL fileURLWithPath:_filePath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:_filePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:_filePath error:nil];
    }
    
	self.recorder = [[[AVAudioRecorder alloc] initWithURL:url settings:settings error:&error] autorelease];
    [settings release];
    //    [self.recorder recordForDuration:59];//设置录音上限为59秒
	if (!self.recorder)
	{
		NSLog(@"Error: %@", [error localizedDescription]);
		return NO;
	}
    
	self.recorder.delegate = self;
	self.recorder.meteringEnabled = YES;
    
	
	if (![self.recorder prepareToRecord])//搭建环境
	{
		NSLog(@"Error: Prepare to record failed");
		return NO;
	}
	
	if (![self.recorder record])
	{
		NSLog(@"Error: Record failed");
		return NO;
	}
    meter = [[ZSTAudioAverageView alloc] initWithFrame:CGRectMake(0, 0, 150, 150)];
    meter.center = self.view.center;
    [meter.stopButton addTarget:self action:@selector(stopRecording) forControlEvents:UIControlEventTouchUpInside];
    [meter.pauseButton addTarget:self action:@selector(pauseRecording) forControlEvents:UIControlEventTouchUpInside];
    [meter.continueButton addTarget:self action:@selector(continueRecording) forControlEvents:UIControlEventTouchUpInside];
    [meter show];    
	recordTimer = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(updateMeters) userInfo:nil repeats:YES];
    
	return YES;
}

#pragma mark ---------- 检测是否有可用设备（相机，麦克风）-----------------

- (BOOL) startAudioSession//检测是否可以访问麦克风
{
	// Prepare the audio session
	NSError *error;
	self.session = [AVAudioSession sharedInstance];
	
	if (![self.session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error])
	{
		NSLog(@"Error: %@", [error localizedDescription]);
		return NO;
	}//设置会话环境
    
    
	
	if (![self.session setActive:YES error:&error])
	{
		NSLog(@"Error: %@", [error localizedDescription]);
		return NO;
	}//激活会话
	
	return self.session.inputIsAvailable;
}
- (BOOL) videoRecordingAvailable//检测是否有可用的相机
{
	if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) return NO;
	return [[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera] containsObject:@"public.movie"];
}

#pragma mark -------------------VideoButtonAction-----------------------------------------------------

-(void)recordVideo
{
    UIImagePickerController *ipc = [[UIImagePickerController alloc] init];
	ipc.sourceType =  UIImagePickerControllerSourceTypeCamera;
	ipc.delegate = self;
	ipc.allowsEditing = YES;
	ipc.videoQuality = UIImagePickerControllerQualityTypeMedium;
    //	ipc.videoMaximumDuration = 30.0f; // 30 seconds//可以持续录制多久
	ipc.mediaTypes = [NSArray arrayWithObject:@"public.movie"];
	[self presentModalViewController:ipc animated:YES];	
}


- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag

{      
    if (recordTimer != nil) {
        [recordTimer invalidate];
        recordTimer = nil;
    }	
    if (meter != nil) {
        [meter dismiss];
        meter = nil;
    }
    
    [[AVAudioSession sharedInstance] setActive:NO error:nil];
    
    [self updateAttachmentView];
}
#pragma mark ------------UIImagePickerControllerDelegate-----------------

-(void)myMovieFinishedCallback:(NSNotification*)aNotification
{
    MPMoviePlayerController* theMoviePlayer=[aNotification object];
	CFShow([aNotification userInfo]);
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:theMoviePlayer];
    [theMoviePlayer stop];
}

-(void)playVideo:(UIButton *)attachmentButton
{
    NSArray *attachmentFileName = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[ZSTUtils pathForTempFinderOfOutbox] error: nil];
    NSString *attachmentName = [attachmentFileName objectAtIndex:attachmentButton.tag];
    NSString *attachmentPath = [[ZSTUtils pathForTempFinderOfOutbox]  stringByAppendingPathComponent:attachmentName];
    
   	MPMoviePlayerViewController* theMoviePlayer=[[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL fileURLWithPath:attachmentPath]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myMovieFinishedCallback:) name:MPMoviePlayerPlaybackDidFinishNotification object:[theMoviePlayer moviePlayer]];
    [self presentModalViewController:theMoviePlayer animated:YES];
    [[theMoviePlayer moviePlayer] play];
    [theMoviePlayer release];
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{    
    NSString *mediaType = [info objectForKey:UIImagePickerControllerMediaType];
    
    if ([mediaType isEqualToString:@"public.image"]){
        
        UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
//        UIImage* newImage = [image compressedImage];
        UIImage *newImage = [UIImage imageWithData:[image compressedData:0.1]];

        [_image release];
        _image = [newImage retain];
        [_filePath release];
        _filePath = [IMAFEPATH retain];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:_filePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:_filePath error:nil];
        }
        [UIImageJPEGRepresentation(_image, 1.0f) writeToFile:_filePath atomically:YES];
        
        
    }else if([mediaType isEqualToString:@"public.movie"]){
        
        videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        NSData *videoData = [NSData dataWithContentsOfURL:videoURL];
        [_filePath release];
        _filePath = [VIDEOPATH retain];
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:_filePath]) {
            [[NSFileManager defaultManager] removeItemAtPath:_filePath error:nil];
        }
        [videoData writeToFile:_filePath atomically:YES];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    [self updateAttachmentView];
}

- (void) imagePickerControllerDidCancel: (UIImagePickerController *) picker
{
	[self dismissViewControllerAnimated:YES completion:nil];
}


-(void)updateContactButtonOrigin
{
    [UIView beginAnimations:@"contactButtonOrigin" context:nil];
    [UIView setAnimationDuration:0.3];
    CGRect contactButtonFrame = _contactButton.frame;
    contactButtonFrame.origin.y =[_contactTextField heightWithLines:_contactTextField.lineCount]-40;
    _contactButton.frame = contactButtonFrame;
    [UIView commitAnimations];
}

#pragma mark -
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    _pageControl.currentPage = scrollView.contentOffset.x / scrollView.bounds.size.width;//是页面控制器的按钮根据页数改变显示
    
}
- (void) pageTurn: (UIPageControl *) aPageControl
{
	NSInteger whichPage = aPageControl.currentPage;
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:1.0f];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	emoticonsScrollView.contentOffset = CGPointMake(320.0f * whichPage, 0.0f);
	[UIView commitAnimations];
}

-(void)camera_buttonClick
{
    UIImagePickerController * picker = [[[UIImagePickerController alloc] init] autorelease];
	picker.delegate = self;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [picker setAllowsEditing:NO];
    
    [self presentModalViewController:picker animated:YES];
}

-(void)clearButtonViewDisappear:(UIView *)view finished:(NSNumber*)finished context:(void *)context
{
    [_copyImageView removeFromSuperview];
    _copyImageView = nil;
    
}

-(void)clearButtonAction:(UIButton *)sender
{
    [UIView beginAnimations:@"Zoom out" context:nil];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(clearButtonViewDisappear:finished:context:)];
    
    ZSTAttachmentView *attachment = (ZSTAttachmentView *)[_attachmentViewArray objectAtIndex:sender.tag];
    CGPoint ImageViewPoint = [self.view convertPoint:attachment.imageView.frame.origin fromView:attachment.imageView];
    
    _copyImageView.frame  = CGRectMake(ImageViewPoint.x, ImageViewPoint.y, attachment.imageView.frame.size.width, attachment.imageView.frame.size.height);
    
    [UIView commitAnimations];
    
    UIButton *clearButton = (UIButton *)sender;
    [clearButton removeFromSuperview];
    
}

-(void)handleImageViewSingleTap:(UIButton *)attachmentButton
{
    
    ZSTF3ClientAppDelegate *appDelegate = (ZSTF3ClientAppDelegate *)[UIApplication sharedApplication].delegate;
    
    NSArray *attachmentFileName = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[ZSTUtils pathForTempFinderOfOutbox] error: nil];
    NSString *attachmentName = [attachmentFileName objectAtIndex:attachmentButton.tag];
    
    NSString *attachmentPath = [[ZSTUtils pathForTempFinderOfOutbox]  stringByAppendingPathComponent:attachmentName];
    
    [ZSTPictureViewer showPicture:[UIImage imageWithContentsOfFile:attachmentPath] addedTo:appDelegate.rootController.view animated:YES];
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (void)attachmentViewWillDisappear:(ZSTAttachmentView *)attachmentView
{
    NSArray *attachmentFileName = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[ZSTUtils pathForTempFinderOfOutbox] error: nil];
    NSString *attachmentPath = [[ZSTUtils pathForTempFinderOfOutbox]stringByAppendingPathComponent:[attachmentFileName objectAtIndex:attachmentView.imageView.tag]];
    [_attachmentViewArray removeObjectAtIndex:attachmentView.imageView.tag];
    
    [[ZSTF3Preferences shared] deleteAttachmentFromFileManagerByPath:attachmentPath];
    
    attachmentFileName = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[ZSTUtils pathForTempFinderOfOutbox] error: nil];
    
    NSInteger attachmentViewCount = [attachmentFileName count];
    if (attachmentViewCount <= 3) {
        _attachmentScrollView.frame = CGRectMake(0, 355-attachmentHeight*(attachmentViewCount-1), 320, attachmentHeight*attachmentViewCount);
        _attachmentScrollView.contentSize = CGSizeMake(320, attachmentHeight*attachmentViewCount);
        
    }else{
        _attachmentScrollView.contentSize = CGSizeMake(320, attachmentHeight*attachmentViewCount);
    }
    
    if (_attachmentScrollView.contentSize.height < attachmentHeight*3) {
        CGRect backgruodTextFieldFrame = _backgruodTextField.frame;
        backgruodTextFieldFrame.size.height += attachmentHeight;
        
        [UIView beginAnimations:@"textView" context:nil];
        [UIView setAnimationDuration:0.5];
        _backgruodTextField.frame = backgruodTextFieldFrame;
        [UIView commitAnimations];
    }
    
    for (int i= 0; i<[attachmentFileName count]; i++) {
        ZSTAttachmentView *attachment = [_attachmentViewArray objectAtIndex:i];
        attachment.imageView.tag = i;
        //        attachment.count.text = [NSString stringWithFormat:@"%d",(i+1)];
        [UIView beginAnimations:@"attachment" context:nil];
        [UIView setAnimationDuration:0.5];
        attachment.frame = CGRectMake(0, i*attachmentHeight, 330, attachmentHeight);
        [UIView commitAnimations];
    }
}

-(void)pictureAction
{
    UIActionSheet *picActionSheet = nil;
    picActionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"取消",@"取消") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"拍照",nil), NSLocalizedString(@"本地图片",nil), nil];
    picActionSheet.tag = 102;
    [picActionSheet showInView:self.view.window];
    [picActionSheet release];
}

-(void)choseAttachmentFromSystem
{
    if ([self videoRecordingAvailable]) {
        UIActionSheet *picActionSheet = nil;
        picActionSheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:NSLocalizedString(@"取消",@"取消") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"本地图片",nil), NSLocalizedString(@"本地视频",nil), nil];
        picActionSheet.tag = 101;
        [picActionSheet showInView:self.view.window];
        [picActionSheet release];
    }else{
        UIImagePickerController * picker = [[[UIImagePickerController alloc] init] autorelease];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [picker setAllowsEditing:NO];
        [self presentModalViewController:picker animated:YES];
    }
}
#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    UIImagePickerController * picker = [[[UIImagePickerController alloc] init] autorelease];
    picker.delegate = self;
    
    if (actionSheet.tag == 101) {
        if (buttonIndex != choosePhotoBtn && buttonIndex != chooseVideoBtn && buttonIndex != chooseAudioBtn) {
            return;
        }
        if (buttonIndex == 0) {
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [picker setAllowsEditing:NO];
        }else if (buttonIndex == 1)
        {		
            picker.sourceType =UIImagePickerControllerSourceTypeSavedPhotosAlbum;
            picker.mediaTypes = [NSArray arrayWithObject:(NSString *)kUTTypeMovie];
        }else if (buttonIndex == 2)
        {
            return;
        }
    }else if (actionSheet.tag == 102)
    {
        [picker setAllowsEditing:NO];
        if (buttonIndex == 0) {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }else if (buttonIndex == 1)
        {
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }else if (buttonIndex == 2)
        {
            return;
        }
    }
    [self presentModalViewController:picker animated:YES];
}
#pragma mark -------------------------- sendMessage-----------------------

- (NSArray *)getContactPhoneNumbers
{
    NSMutableArray *phoneNumMutableArray = [NSMutableArray array];
    NSArray *tempArray =  _contactTextField.cellViews ;
    
    for (TTPickerViewCell *cellView in tempArray) 
    {
        TTTableSubtitleItem *object = cellView.object;
        if ([object.subtitle length] != 0) 
        {
            [phoneNumMutableArray addObject:object.subtitle];
        }
    }
    NSArray *array = [NSArray arrayWithArray:phoneNumMutableArray];
    return (NSArray *)array;
}
-(void)cancelSendItOnView
{
    [_isSendItOnView dismiss];
}
-(void)confirmPopAlert
{
    [_isSendItOnView release];
    _isSendItOnView = [[ZSTEditView alloc] initWithImage:[UIImage imageNamed:@"sendItOn.png"]];
    _isSendItOnView.center = CGPointMake(320/2, (480-20-44)/2);
    
    UIButton *continueBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    continueBtn.frame = CGRectMake(6, 52, 135, 55);
    [continueBtn addTarget:self action:@selector(sendMessage) forControlEvents:UIControlEventTouchUpInside];
    continueBtn.showsTouchWhenHighlighted = YES;
    continueBtn.backgroundColor = [UIColor clearColor];
    [_isSendItOnView addSubview:continueBtn];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(140, 52, 135, 55);
    [cancelBtn addTarget:self action:@selector(cancelSendItOnView) forControlEvents:UIControlEventTouchUpInside];
    cancelBtn.showsTouchWhenHighlighted = YES;
    cancelBtn.backgroundColor = [UIColor clearColor];
    [_isSendItOnView addSubview:cancelBtn];
    
    [_isSendItOnView show];
}

-(void)setNoChange
{
    [_initPhoneNumberArray release];
    _initPhoneNumberArray = [[self getContactPhoneNumbers] retain];
    
    [_initContent release];
    _initContent = [textview.text retain];
    
    [_initAttachment release];
    _initAttachment = [_attachmentViewArray retain];
}

-(BOOL)hasChanged
{
    NSArray *phoneNumber = [self getContactPhoneNumbers];
    NSString *content = textview.text;
    NSArray *attachmentView = [NSArray arrayWithArray:_attachmentViewArray];
    
    if (![phoneNumber isEqualToArray:_initPhoneNumberArray]) {
        return YES;
    }
    
    if (![content isEqualToString:_initContent]) {
        return YES;
    }
    if (![attachmentView isEqualToArray:_initAttachment]) {
        return YES;
    }
    
    return NO;
}

-(void) sendMessage
{
    [_contactTextField autoDetectPhoneNumber];
    
    // 判断是主动发的，还是转发的choseAttachmentFromSystem
    
    if([_contactTextField.cellViews count] == 0 )
    {
        [TKUIUtil showHUDInView:self.view withText:@"请选择收件人" withImage:[UIImage imageNamed:@"icon_warning.png"]];
        [TKUIUtil hiddenHUDAfterDelay:2];
        
    }
    else
    {
        if (_pushId==nil) {
            self.pushId = @"";
        }
        
        NSString *Subject = @"";
        
        NSArray *phoneNumbers = [self getContactPhoneNumbers];
        
        NSString *groupPhoneNumbers = [phoneNumbers componentsJoinedByString:@","];
        
        NSString * loginMsisdn = [ZSTF3Preferences shared].loginMsisdn;
        if (loginMsisdn == nil || [loginMsisdn isEqualToString:@""])
        {
            [TKUIUtil alertInWindow:NSLocalizedString(@"尚未绑定\n不能发送信息", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]];
            return;
        }
        
        MessageInfo *info = [[MessageInfo alloc] init];
        info.userID = groupPhoneNumbers;
        info.forwarded = _pushId;
        info.subject = Subject;
        info.report = [ZSTF3Preferences shared].reportState;
        info.content = textview.text;
        [ZSTF3Engine sendF3:info alertWhenFinish:YES];
        
        //删除草稿
        if (self.isDraftEdit) {
            info.ID = [self.nmsID intValue];
            [ZSTF3Engine deleteMessage:info];
            postNotificationOnMainThreadNoWait(NotificationName_NMSListUpdated);
        }
        [info release];
        [self setNoChange];
        [self popViewControllerDirectly:NO];
    }
    
    if (_isSendItOnView != nil) {
        [_isSendItOnView dismiss];
    }
}

-(void)isConfirmToSendMessage
{
    NSArray *phoneNuberArray = [self getContactPhoneNumbers];
    NSArray *attachmentFileName = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[ZSTUtils pathForTempFinderOfOutbox] error: nil];
    
    for (NSString *name in attachmentFileName) {
        for (NSString *phoneNumber in phoneNuberArray) {
            if ([name hasSuffix:@"mp4"] && [phoneNumber length]==11) {
                [self confirmPopAlert];
                return;
            }
        }
    }
    [self sendMessage];
}

- (void)initEmoticons
{
    NSMutableArray *keysArray = [NSMutableArray array];
    
    NSMutableArray *imagesArray = [NSMutableArray array];
    
    for (int i = 0; i<= 91; i++) {
        
        NSString *name = [NSString stringWithFormat:@"f%03d", i];
        
        [keysArray addObject:[NSString stringWithFormat:@"[/fac=%@]", name]];
        [imagesArray addObject:[NSString stringWithFormat:@"%@", name]];
    }
    
    _keysArray = [keysArray retain];
    _imagesArray = [imagesArray retain];
}


- (void)openForwardedMessageView
{
    CATransition  * transition = [CATransition animation];
    transition.duration = 0.5;// kAnimationDuration;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]; 
    transition.type = @"reveal";
    transition.subtype = kCATransitionFromBottom;
    [self.navigationController.view.layer addAnimation:transition forKey:nil];
    [self popViewController];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed:@"framework_top_bg.png"];
}

-(id)init
{
    self = [super init];
    if (self)
    {
        _shouldSaveToDraft = NO;
        _isDraftEdit = NO;
        _keyboardIsShowing = NO;
        self.navigationItem.titleView = [ZSTUtils logoView];
        [self initEmoticons];
        
        self.view.frame = CGRectMake(0, 0, 320, 480 - 44+(iPhone5?88:0));
        _attachmentViewArray = [[NSMutableArray array] retain];
        _buttonArray = [NSMutableArray array];
        
        UIImageView *imageview =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]];
        imageview.frame = CGRectMake(0, 0, 320, 480+(iPhone5?88:0));
        [self.view addSubview:imageview];
        [imageview release];
        
        _subjectBackgroundView = [[UIImageView alloc] initWithFrame:CGRectMake(-5, 43+4, 330, 40)];
        _subjectBackgroundView.hidden = YES;
        _subjectBackgroundView.userInteractionEnabled = YES;
        _subjectBackgroundView.image = [UIImage imageNamed:@"createback.png"];
        [self.view addSubview:_subjectBackgroundView];
        [_subjectBackgroundView release];
        
        UIControl *subjectControl = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, 250, 32)];
        [subjectControl addTarget:self action:@selector(openForwardedMessageView) forControlEvents:UIControlEventTouchUpInside];
        
        _subjectContent =[[UILabel alloc] initWithFrame:CGRectMake(15, 0, 250, 40)];
        
        _subjectContent.backgroundColor = [UIColor clearColor];
        _subjectContent.textAlignment = NSTextAlignmentLeft;
        _subjectContent.font = [UIFont systemFontOfSize:15];
        [subjectControl addSubview:_subjectContent];
        [_subjectBackgroundView addSubview:subjectControl];
        [subjectControl release];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(280, 0, 40, 40);
        [btn addTarget:self action:@selector(delSubject) forControlEvents:UIControlEventTouchUpInside];
        [btn setTitle:@"X" forState:UIControlStateNormal];
        [btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_subjectBackgroundView addSubview:btn];
        
        
        _backgruodTextField = [[UITextField alloc] initWithFrame:CGRectMake(5, 52, 310, 338+(iPhone5?88:0))];
        _backgruodTextField.backgroundColor = [UIColor clearColor];
        _backgruodTextField.borderStyle = UITextBorderStyleRoundedRect;
        _backgruodTextField.userInteractionEnabled = YES;
        [self.view addSubview:_backgruodTextField];

        textview = [[EGOTextView alloc] initWithFrame:CGRectMake(1, 2, 308, 334+(iPhone5?88:0))];
        textview.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        textview.backgroundColor = [UIColor clearColor];
        
        CALayer * layer = [textview layer];
        [layer setMasksToBounds:YES];
        [layer setCornerRadius:6.0];
        [layer setBorderWidth:1.0];
        [layer setBorderColor:[[UIColor clearColor] CGColor]];
        
        textview.delegate = self;
        textview.backgroundColor = [UIColor whiteColor];
        textview.userInteractionEnabled= YES;
        [_backgruodTextField addSubview:textview];
        
        _contactScrollView = [[[UIScrollView alloc] init] autorelease];
        _contactScrollView.backgroundColor = [UIColor whiteColor];
        _contactScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
        _contactScrollView.showsVerticalScrollIndicator = YES;
        _contactScrollView.showsHorizontalScrollIndicator = NO;
        [self.view addSubview:_contactScrollView];
        
        _contactTextField = [[[ZSTLimitedPickerTextField alloc] init] autorelease];
        _contactTextField.backgroundColor = [UIColor clearColor];
        _contactTextField.superviewForSearchResultsTableView = _contactScrollView.superview;
        _contactTextField.dataSource = [[[PickerDataSource alloc] init] autorelease];
        _contactTextField.autocorrectionType = UITextAutocorrectionTypeNo;
        _contactTextField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        _contactTextField.rightViewMode = UITextFieldViewModeAlways;
        _contactTextField.delegate = self;
        _contactTextField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _contactTextField.frame = CGRectMake(0, 0, 320, 45);
        
        UILabel *label = [[[UILabel alloc] init] autorelease];
        label.text = @"收件人:";
        label.font = [UIFont systemFontOfSize:14];
        label.textColor = [UIColor colorWithWhite:0.5 alpha:1];
        [label sizeToFit];
        _contactTextField.leftView = label;
        _contactTextField.leftViewMode = UITextFieldViewModeAlways;
        
        _contactScrollView.frame = CGRectMake(0, 0, 320, 45);
        
        [_contactScrollView addSubview:_contactTextField];
        
        _contactButton = [UIButton buttonWithType:UIButtonTypeContactAdd];
        _contactButton.frame = CGRectMake(276, 5, 40, 40);
        [_contactButton addTarget:self action:@selector(openContacter) forControlEvents:UIControlEventTouchUpInside];
        [_contactTextField addSubview:_contactButton];
        
        _background1 =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"createback.png"]];
        _background1.frame = CGRectMake(-5, 396+(iPhone5?88:0), 330, 40);
        _background1.userInteractionEnabled = YES;
        
        [self.view addSubview:_background1];
        
        if (NSClassFromString(@"NSAttributedString") != nil) {
            UIButton *expression_button = [UIButton buttonWithType:UIButtonTypeCustom];
            expression_button.frame = CGRectMake(20, 4, 33, 33);
            //            expression_button.backgroundColor = [UIColor redColor];
            [expression_button setImage:[UIImage imageNamed:@"btn_expression_n.png"] forState:UIControlStateNormal];
            [expression_button setImage:[UIImage imageNamed:@"btn_expression_p.png"] forState:UIControlStateHighlighted];
            [expression_button addTarget:self action:@selector(expressionAction:) forControlEvents:UIControlEventTouchUpInside];
            [_buttonArray addObject:expression_button];
        }
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            
            UIButton *camera_button = [UIButton buttonWithType:UIButtonTypeCustom];
            camera_button.frame = CGRectMake(55, 2, 33, 33);
            [camera_button setImage:[UIImage imageNamed:@"btn_camera_n.png"] forState:UIControlStateNormal];
            [camera_button setImage:[UIImage imageNamed:@"btn_camera_p.png"] forState:UIControlStateHighlighted];
            [camera_button addTarget:self action:@selector(camera_buttonClick) forControlEvents:UIControlEventTouchUpInside];
            [_buttonArray addObject:camera_button];
            
        }
        
        if ([self startAudioSession])
        {   
            UIButton *audio_button = [UIButton buttonWithType:UIButtonTypeCustom];
            audio_button.frame = CGRectMake(110, 8, 33, 33);
            [audio_button setImage:[UIImage imageNamed:@"btn_audio_n.png"] forState:UIControlStateNormal];
            [audio_button setImage:[UIImage imageNamed:@"btn_audio_p.png"] forState:UIControlStateHighlighted];      
            [audio_button addTarget:self action:@selector(recordAudio) forControlEvents:UIControlEventTouchUpInside];
            [_buttonArray addObject:audio_button];
        }
        
        if ([self videoRecordingAvailable])
        {
            UIButton *video_button = [UIButton buttonWithType:UIButtonTypeCustom];
            video_button.frame = CGRectMake(150, 8, 33, 33);
            [video_button setImage:[UIImage imageNamed:@"btn_video_n.png"] forState:UIControlStateNormal];
            [video_button setImage:[UIImage imageNamed:@"btn_video_p.png"] forState:UIControlStateHighlighted];      
            [video_button addTarget:self action:@selector(recordVideo) forControlEvents:UIControlEventTouchUpInside];
            [_buttonArray addObject:video_button];
        }
        
        UIButton *attchment_button = [UIButton buttonWithType:UIButtonTypeCustom];
        attchment_button.frame = CGRectMake(190, 8, 33, 33);
        [attchment_button setImage:[UIImage imageNamed:@"btn_attachment_n.png"] forState:UIControlStateNormal];
        [attchment_button setImage:[UIImage imageNamed:@"btn_attachment_p.png"] forState:UIControlStateHighlighted];      
        [attchment_button addTarget:self action:@selector(choseAttachmentFromSystem) forControlEvents:UIControlEventTouchUpInside];
        [_buttonArray addObject:attchment_button];    
        
        for (int i=0 ; i<[_buttonArray count]; i++) {
            UIButton *btn = [_buttonArray objectAtIndex:i];
            CGRect btnFrame = btn.frame;
            btnFrame.origin = CGPointMake(20+i*43, 4);
            btn.frame = btnFrame;
            btn.imageEdgeInsets = UIEdgeInsetsMake(2.2, 2.2, 2.2, 2.2);
            [_background1 addSubview:btn];
        } 
        
        sed_button = [ZSTUtils customButtonTitle:NSLocalizedString(@"发 送" , nil)
                                          nImage:[UIImage imageNamed:@"btn_commonbg_n.png"] 
                                          pImage:[UIImage imageNamed:@"btn_commonbg_p.png"] 
                                   titleFontSize:15 
                                     conerRsdius:3.0];
        
        [sed_button addTarget:self action:@selector(isConfirmToSendMessage) forControlEvents:UIControlEventTouchUpInside];
        sed_button.frame = CGRectMake(255, 5, 60, 30);
        
        [_background1 addSubview:sed_button];
        
    }
    [ZSTLogUtil logUserAction:NSStringFromClass([ZSTCreateNMSDialogViewController class])];
    
    //绑定键盘通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHidden:) name:UIKeyboardWillHideNotification object:nil];
    return self;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - #pragma mark Responding to keyboard events  

- (void)keyboardWillShow:(NSNotification *)notification {  
    NSNumber *              durationObj;
    NSNumber *              curveObj;
    NSTimeInterval          duration = TK_TRANSITION_DURATION;
    UIViewAnimationCurve    curve = UIViewAnimationCurveEaseInOut;
    CGFloat         keyboardHeight = TK_KEYBOARD_HEIGHT;
    
    NSDictionary *userInfo = [notification userInfo];
    
    durationObj = (NSNumber *) [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    curveObj    = (NSNumber *) [userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    if ( (durationObj != nil) && (curveObj != nil)) {
        assert([durationObj isKindOfClass:[NSNumber class]]);
        assert([curveObj isKindOfClass:[NSNumber class]]);
        
        duration = (NSTimeInterval)       [durationObj doubleValue];
        curve    = (UIViewAnimationCurve) [curveObj    unsignedIntegerValue];
    }
    
    if (TKRuntimeOSVersionIsAtLeast(@"3.2")) {
        NSValue *keyboardSizeObj = (NSValue *) [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
        if (keyboardSizeObj) {
            CGSize keyboardSize = [keyboardSizeObj CGRectValue].size;
            keyboardHeight = keyboardSize.height;
        }
    }
    
    [UIView beginAnimations:@"disappearKeyboard" context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    
    CGRect backgruodTextFieldFrame = _backgruodTextField.frame;
    float textFieldHeight = _subjectBackgroundView.hidden ? (338+(iPhone5?88:0)):(294+(iPhone5?88:0));
    backgruodTextFieldFrame.size.height = textFieldHeight-keyboardHeight;
    
    _backgruodTextField.frame = backgruodTextFieldFrame;
    
    CGRect  background1_frame = _background1.frame;
    background1_frame.origin.y = 396 - keyboardHeight+(iPhone5?88:0);
    _background1.frame = background1_frame;
    
    [UIView commitAnimations];
    
    _keyboardIsShowing = YES;
}

- (void)keyboardWillHidden:(NSNotification *)notification {  
    NSNumber *              durationObj;
    NSNumber *              curveObj;
    NSTimeInterval          duration = TK_TRANSITION_DURATION;
    UIViewAnimationCurve    curve = UIViewAnimationCurveEaseInOut;
    CGFloat         keyboardHeight = TK_KEYBOARD_HEIGHT;
    
    NSDictionary *userInfo = [notification userInfo];
    
    durationObj = (NSNumber *) [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    curveObj    = (NSNumber *) [userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    if ( (durationObj != nil) && (curveObj != nil)) {
        assert([durationObj isKindOfClass:[NSNumber class]]);
        assert([curveObj isKindOfClass:[NSNumber class]]);
        
        duration = (NSTimeInterval)       [durationObj doubleValue];
        curve    = (UIViewAnimationCurve) [curveObj    unsignedIntegerValue];
    }
    
    if (TKRuntimeOSVersionIsAtLeast(@"3.2")) {
        NSValue *keyboardSizeObj = (NSValue *) [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
        if (keyboardSizeObj) {
            CGSize keyboardSize = [keyboardSizeObj CGRectValue].size;
            keyboardHeight = keyboardSize.height;
        }
    }
    
    [UIView beginAnimations:@"disappearKeyboard" context:nil];
    [UIView setAnimationDuration:duration];
    [UIView setAnimationCurve:curve];
    
    float textFieldOriginHeight = _subjectBackgroundView.hidden ? (338+(iPhone5?88:0)):(294+(iPhone5?88:0));
    
    CGRect backgruodTextFieldFrame = _backgruodTextField.frame;
    backgruodTextFieldFrame.size.height = (textFieldOriginHeight - _attachmentScrollView.frame.size.height);
    _backgruodTextField.frame = backgruodTextFieldFrame;
    
    CGRect  background1_frame = _background1.frame;
    background1_frame.origin.y = 396+(iPhone5?88:0);
    _background1.frame = background1_frame;
    
    [UIView commitAnimations];
    
    _keyboardIsShowing = NO;
}

#pragma mark - UITextViewDelegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    [self egoTextViewDidBeginEditing:textview];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    [self egoTextViewDidEndEditing:textview];
}

#pragma mark - egoTextViewDeleagte


- (NSUInteger)numberOfEmoticonsInEgoTextView
{
    return [_imagesArray count];
}


- (UIImage *)egoTextView:(EGOTextView*)textView imageForEmoticonsAtIndex:(NSUInteger)index
{
    NSString *imageName = [_imagesArray objectAtIndex:index];
    return [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:imageName ofType:@"png"]];
}

- (NSString *)egoTextView:(EGOTextView*)textView keyForEmoticonsAtIndex:(NSUInteger)index{
    return [_keysArray objectAtIndex:index];
}

- (void)egoTextViewDidBeginEditing:(EGOTextView *)textView {
    
    if (emoticonsView != nil) {
        [self hideEmoticonsView];
    }
    
    if (_shouldSaveToDraft) {
        self.navigationItem.rightBarButtonItem = nil;
    }
    if (_contactTextField.lineCount >= 2 && _contactScrollView.frame.size.height != [_contactTextField heightWithLines:1]) {
        
        
        [UIView beginAnimations:@"contactTextFieldFrame" context:nil];
        [UIView setAnimationDuration:0.3];
        
        CGRect frame = _contactScrollView.frame;
        frame.size.height = [_contactTextField heightWithLines:1];
        _contactScrollView.frame = frame;  
        [_contactTextField scrollToVisibleLine:YES];
        
        CGRect backgruodTextFieldFrame = _backgruodTextField.frame;
        backgruodTextFieldFrame.origin.y -= 31;
        backgruodTextFieldFrame.size.height +=31; 
        _backgruodTextField.frame = backgruodTextFieldFrame;
        
        [UIView commitAnimations];
        
        if (_subjectBackgroundView.hidden == NO) {
            
            CGRect subjectBackgroundViewFrame = _subjectBackgroundView.frame;
            subjectBackgroundViewFrame.origin.y -= 32;
            _subjectBackgroundView.frame = subjectBackgroundViewFrame;
        }
    }
    
    UIButton *doneButton = [ZSTUtils customButtonTitle:nil
                                                nImage:[UIImage imageNamed:@"keyboard.png"]
                                                pImage:[UIImage imageNamed:@"keyboard.png"] 
                                         titleFontSize:13
                                           conerRsdius:5.0];
    
    [doneButton addTarget:self action:@selector(leaveEditMode) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *doneBarButton = [[UIBarButtonItem alloc] initWithCustomView:doneButton];
    self.navigationItem.rightBarButtonItem = doneBarButton;
    [doneBarButton release];
    
}


- (void)egoTextViewDidEndEditing:(EGOTextView *)textView {
    
    if (_shouldSaveToDraft) {
        self.navigationItem.rightBarButtonItem = nil;
    }
    self.navigationItem.rightBarButtonItem = _saveNMSBarButton;
    if (emoticonsView) {
        [self hideEmoticonsView];
    }
    
}


#pragma mark - TTPickerTextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (emoticonsView != nil) {
        [self hideEmoticonsView];
    }
    
    //    if (_shouldSaveToDraft) {
    //        self.navigationItem.rightBarButtonItem = nil;
    //    }
    //    UIBarButtonItem *done = [[[UIBarButtonItem alloc] initWithTitle:@"隐藏键盘" style:UIBarButtonItemStylePlain target:self action:@selector(leaveEditMode)] autorelease];
    //    self.navigationItem.rightBarButtonItem = done;  
    
    
    if (_contactTextField.lineCount >= 2 && _contactScrollView.frame.size.height != [_contactTextField heightWithLines:2]) {
        
        [UIView beginAnimations:@"contactTextFieldFrame" context:nil];
        [UIView setAnimationDuration:0.4];
        
        
        
        CGRect frame = _contactScrollView.frame;
        frame.size.height = [_contactTextField heightWithLines:2];
        _contactScrollView.frame = frame;  
        
        CGRect backgruodTextFieldFrame = _backgruodTextField.frame;
        backgruodTextFieldFrame.origin.y += 31;
        backgruodTextFieldFrame.size.height -=31; 
        _backgruodTextField.frame = backgruodTextFieldFrame;
        
        [UIView commitAnimations]; 
        
        if (_subjectBackgroundView.hidden == NO) {
            
            CGRect subjectBackgroundViewFrame = _subjectBackgroundView.frame;
            subjectBackgroundViewFrame.origin.y += 32;
            _subjectBackgroundView.frame = subjectBackgroundViewFrame;
        }
        
    }
    
}

- (void)textFieldDidEndEditing:(ZSTLimitedPickerTextField *)textField
{
    [textField autoDetectPhoneNumber];
    
    if (textField.lineCount >= 2 && _contactScrollView.frame.size.height != [textField heightWithLines:1]) {
        
        [UIView beginAnimations:@"contactTextFieldFrame" context:nil];
        [UIView setAnimationDuration:0.4];
        
        CGRect frame = _contactScrollView.frame;
        frame.size.height = [textField heightWithLines:1];
        _contactScrollView.frame = frame;  
        [textField scrollToVisibleLine:YES];
        
        CGRect backgruodTextFieldFrame = _backgruodTextField.frame;
        backgruodTextFieldFrame.origin.y -= 31;
        backgruodTextFieldFrame.size.height +=31; 
        _backgruodTextField.frame = backgruodTextFieldFrame;
        
        [UIView commitAnimations];
    }
    
}
- (BOOL)textField:(ZSTLimitedPickerTextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *s = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (s == nil||[s isEqualToString: @""]) {
        textField.myState = kEmpty;
    } else {
        textField.myState = kValue;
    }
    return YES;
}

- (void)textFieldDidResize:(ZSTLimitedPickerTextField*)textField
{
    
    if (textField.lineCount <= 2) {
        CGRect frame = _contactScrollView.frame;
        frame.size.height = textField.frame.size.height;
        _contactScrollView.frame = frame;
        
    } else {
        CGRect frame = _contactScrollView.frame;
        frame.size.height = [textField heightWithLines:2];
        _contactScrollView.frame = frame;
    }
    
    
    CGFloat y = CGRectGetMaxY(_contactScrollView.frame);
    
    [UIView beginAnimations:@"contactTextFieldFrame" context:nil];
    [UIView setAnimationDuration:0.4];
    
    CGRect backgruodTextFieldFrame = _backgruodTextField.frame;
    backgruodTextFieldFrame.size.height = (_subjectBackgroundView.hidden ? (backgruodTextFieldFrame.size.height - ((y + 7 ) - backgruodTextFieldFrame.origin.y)):(backgruodTextFieldFrame.size.height - ((y+7+40) - backgruodTextFieldFrame.origin.y))); 
    backgruodTextFieldFrame.origin.y  = (_subjectBackgroundView.hidden ? (y + 7) : (y+7+40));
    _backgruodTextField.frame = backgruodTextFieldFrame;
    
    [UIView commitAnimations];
    
    if (_subjectBackgroundView.hidden == NO) {
        
        CGRect subjectBackgroundViewFrame = _subjectBackgroundView.frame;
        subjectBackgroundViewFrame.origin.y = y+1;
        _subjectBackgroundView.frame = subjectBackgroundViewFrame;
    }
    
    _contactScrollView.contentSize = CGSizeMake(textField.frame.size.width, textField.frame.size.height);
    _contactScrollView.contentMode = UIViewContentModeScaleAspectFit;
    
    [self updateContactButtonOrigin];
}

- (BOOL)textFieldShouldReturn:(ZSTLimitedPickerTextField *)textField
{
    
    //    [UIView beginAnimations:@"disappearKeyboard" context:nil];
    //    [UIView setAnimationDuration:0.3];
    //    
    //    CGRect backgruodTextFieldFrame = _backgruodTextField.frame;
    //    backgruodTextFieldFrame.size.height += (_keyboardHeight- _attachmentScrollView.frame.size.height);
    //    _backgruodTextField.frame = backgruodTextFieldFrame;
    //    
    //    
    //    CGRect  background1_frame = _background1.frame;
    //    background1_frame.origin.y += _keyboardHeight;
    //    _background1.frame = background1_frame;
    //    [UIView commitAnimations];
    
    
    if (textField.lineCount >= 2 && _contactScrollView.frame.size.height != [textField heightWithLines:1]) {
        
        
        [UIView beginAnimations:@"contactTextFieldFrame" context:nil];
        [UIView setAnimationDuration:0.4];
        
        CGRect frame = _contactScrollView.frame;
        frame.size.height = [textField heightWithLines:1];
        _contactScrollView.frame = frame;  
        [textField scrollToVisibleLine:YES];
        
        CGRect backgruodTextFieldFrame = _backgruodTextField.frame;
        backgruodTextFieldFrame.origin.y -= 31;
        backgruodTextFieldFrame.size.height +=31; 
        _backgruodTextField.frame = backgruodTextFieldFrame;
        
        [UIView commitAnimations];
    }
    
    return YES;
}

-(void)leaveEditMode
{
    [_contactTextField resignFirstResponder];
    
    [self customTextViewResignFirstResponder];
    
    self.navigationItem.rightBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = _saveNMSBarButton;
    
    
    if (emoticonsView != nil) {
        
        [self hideEmoticonsView];
    }
}
#pragma mark - 


- (void)delSubject
{
    _subjectBackgroundView.hidden = YES;
    
    if (_contactTextField.lineCount >= 2 && _contactScrollView.frame.size.height != [_contactTextField heightWithLines:1])
    {
        
        [UIView beginAnimations:@"textView" context:nil];
        [UIView setAnimationDuration:0.5];
        
        CGRect backgruodTextFieldFrame = _backgruodTextField.frame;
        backgruodTextFieldFrame.origin.y -= 41;
        backgruodTextFieldFrame.size.height +=41; 
        _backgruodTextField.frame = backgruodTextFieldFrame;
        
        [UIView commitAnimations];
    }else{
        CGRect backgruodTextFieldFrame = _backgruodTextField.frame; 
        
        backgruodTextFieldFrame.origin.y = 51;
        backgruodTextFieldFrame.size.height += 44; 
        
        [UIView beginAnimations:@"textView" context:nil];
        [UIView setAnimationDuration:0.5];
        
        _backgruodTextField.frame = backgruodTextFieldFrame;
        
        [UIView commitAnimations];
    }
    
    self.pushId = nil;
    self.subject = nil;
}

- (void)updateSubjectView{
    
    CGRect backgruodTextFieldFrame = _backgruodTextField.frame; 
    if(_pushId==nil||[_pushId length] == 0){
        
        _subject = @"";
    }else {
        _subjectBackgroundView.hidden = NO;
        _subjectContent.text = _subject;
        
        backgruodTextFieldFrame.origin.y = 84+10;
        backgruodTextFieldFrame.size.height -= 44; 
    }
    
    _backgruodTextField.frame = backgruodTextFieldFrame;
}

#pragma mark---------------------------editView------------------------------------

-(void)draftMSG
{      
    NSMutableArray *phoneNumMutableArray = [NSMutableArray array];
    NSArray *tempArray =  _contactTextField.cellViews ;
    
    for (TTPickerViewCell *cellView in tempArray) 
    {
        TTTableSubtitleItem *object = cellView.object;
        if ([object.subtitle length] != 0) 
        {
            ContactInfo *contactInfo = [[[ContactInfo alloc] init] autorelease];
            contactInfo.phoneNum = object.subtitle;
            [phoneNumMutableArray addObject:contactInfo.phoneNum];
        }
    }
    
    NSString *groupPhoneNumber = [phoneNumMutableArray componentsJoinedByString:@","];
    NSString *Subject =_subject ? _subject : @"";
    NSString *Content = textview.text ? textview.text : @"";
    
    //保存到数据库
    MessageInfo *nmsInfo = [[MessageInfo alloc] init];
    
    nmsInfo.MSGID = 0;
    nmsInfo.userID = groupPhoneNumber;
    nmsInfo.subject = Subject;
    nmsInfo.content = Content;
    nmsInfo.isReceived = NO;
    nmsInfo.time = [NSDate date];
    nmsInfo.state = NMSStateDraft;
    
    NSError *theError = nil;
    NSInteger ID = [ZSTF3Engine saveF3Message:nmsInfo error:&theError];
    if (ID != -1)
    { 
        postNotificationOnMainThreadNoWait(NotificationName_NMSListUpdated);
        [self popViewControllerDirectly:YES];
    }
    [_exitOrDraftView dismiss];
    [nmsInfo release];

}
-(void)exitNewCreat
{
    [_exitOrDraftView dismiss];
    [self popViewControllerDirectly:YES];
}
-(void)cancelOperate
{
    [_exitOrDraftView dismiss];
}

- (void)draft
{
    [_exitOrDraftView release];
    _exitOrDraftView = [[ZSTEditView alloc] initWithImage:[UIImage imageNamed:@"exitOrDraft.png"]];
    _exitOrDraftView.center = CGPointMake(320/2, (480-20-44)/2);
    
    UIButton *draftMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    draftMSGBtn.frame = CGRectMake(6, 10, 65, 55);
    [draftMSGBtn addTarget:self action:@selector(draftMSG) forControlEvents:UIControlEventTouchUpInside];
    draftMSGBtn.showsTouchWhenHighlighted = YES;
    draftMSGBtn.backgroundColor = [UIColor clearColor];
    [_exitOrDraftView addSubview:draftMSGBtn];
    
    UIButton *exitNewCreatBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    exitNewCreatBtn.frame = CGRectMake(73, 10, 65, 55);
    [exitNewCreatBtn addTarget:self action:@selector(exitNewCreat) forControlEvents:UIControlEventTouchUpInside];
    exitNewCreatBtn.showsTouchWhenHighlighted = YES;
    exitNewCreatBtn.backgroundColor = [UIColor clearColor];
    [_exitOrDraftView addSubview:exitNewCreatBtn];
    
    UIButton *cancelOperateBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelOperateBtn.frame = CGRectMake(140,10, 65, 55);
    [cancelOperateBtn addTarget:self action:@selector(cancelOperate) forControlEvents:UIControlEventTouchUpInside];
    cancelOperateBtn.showsTouchWhenHighlighted = YES;
    cancelOperateBtn.backgroundColor = [UIColor clearColor];
    [_exitOrDraftView addSubview:cancelOperateBtn];
    
    [_exitOrDraftView show];
    
}

- (void)popViewControllerDirectly:(BOOL)removeTempFinder
{
    UIApplication *application = [UIApplication sharedApplication];
    [application setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    application.statusBarStyle = UIStatusBarStyleDefault;
    
    [_attachmentViewArray removeAllObjects];
    
    if (removeTempFinder) {
        [[NSFileManager defaultManager] removeItemAtPath: [ZSTUtils pathForTempFinderOfOutbox] error: nil];
    }
    [self dismissModalViewController];
}

- (void)popViewController
{
    if ([self hasChanged]) {
        [self draft];
    } else {
        [self popViewControllerDirectly:YES];
    }
}

- (void)recordInitalValue
{
    _initPhoneNumberArray =  [[self getContactPhoneNumbers] retain];
    _initContent = [textview.text retain];
    _initAttachment = [[NSArray arrayWithArray:_attachmentViewArray] retain];
}

- (void)pushInNavigationViewController:(UIViewController *)controller
{
    UIApplication *application = [UIApplication sharedApplication];
    [application setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
    
    [controller presentModalViewController:[[[UINavigationController alloc] initWithRootViewController:self] autorelease] animated:YES];
    
    if (_shouldSaveToDraft) {
        
        _saveNMSBarButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"保存",@"") style:UIBarButtonItemStyleBordered target:self action:@selector(draft)];
        self.navigationItem.rightBarButtonItem = _saveNMSBarButton;
    }

    self.navigationItem.leftBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"取消",@"取消") target:self selector:@selector (popViewController)];
    
    if (self.nmsID != nil && [self.nmsID length] != 0 && [self.pushId length] == 0 && self.pushId == nil) {
        [self reAddAttachment];
    }
    [self recordInitalValue];
}

-(void) openContacter
{
    ContactPickerViewController *contactPickerViewController = [[ContactPickerViewController alloc] init];
    contactPickerViewController.delegate = self;
    
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:contactPickerViewController];
    [self presentModalViewController:nav animated:YES];
    [nav release];
    [contactPickerViewController release];
}

#pragma mark
#pragma mark--ContactViewControllerDelegate------------------------------


-(void)contactPickerViewDidBeginSelect:(ContactPickerViewController *)contactViewController
{
    NSMutableArray *tempMutableArray = [[NSMutableArray alloc] init];
    NSArray *tempArray =  _contactTextField.cellViews ;
    
    for (TTPickerViewCell *cellView in tempArray) 
    {
        TTTableSubtitleItem *object = cellView.object;
        if ([object.subtitle length] != 0) 
        {
            ContactInfo *contactInfo = [[[ContactInfo alloc] init] autorelease];
            contactInfo.name = object.text;
            contactInfo.phoneNum = object.subtitle;
            [tempMutableArray addObject:contactInfo];
        }
    }
    
    contactViewController.contactInfoArray = tempMutableArray;
    
    [tempMutableArray release];
    
}

-(void)contactPickerViewDidFinishSelect:(ContactPickerViewController *)contactViewController
{
    [_contactTextField removeAllCells];
    
    for (ContactInfo *contactInfo in contactViewController.contactInfoArray)
    {
        TTTableSubtitleItem *item;
        if (![contactInfo.name isEqualToString:@""] && contactInfo.name != nil) {
            item = [TTTableSubtitleItem itemWithText:contactInfo.name subtitle:contactInfo.phoneNum];
        }else
        {
            item = [TTTableSubtitleItem itemWithText:contactInfo.phoneNum subtitle:contactInfo.phoneNum];
            
        }
        
        [_contactTextField addCellWithObject:item];
        
        [self updateContactButtonOrigin];
    }
}

- (void)getforwardMessage:(NSString *) forwardMessageString
{
    
    textview.text = forwardMessageString;
}

- (void)getReplyUserNumber:(NSString *) userNumberString userName:(NSString *)name
{
    [_contactTextField removeAllCells];
    TTTableSubtitleItem *item = [TTTableSubtitleItem itemWithText:name subtitle:userNumberString];
    
    [_contactTextField addCellWithObject:item];
    
    [self updateContactButtonOrigin];
}

- (void)getOutContactNumber:(NSArray *) contactArray
{
    [_contactTextField removeAllCells];
    
    for (NSString *contactNumber in contactArray)
    {
        if (![contactNumber isEqualToString:@""] && contactNumber != nil) {
            TTTableSubtitleItem *item = [TTTableSubtitleItem itemWithText:contactNumber subtitle:contactNumber];
            
            [_contactTextField addCellWithObject:item];
            
            [self updateContactButtonOrigin];
        }
    }
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)setPushId:(NSString *)pushId
{
    [_pushId release];
    _pushId = nil;
    _pushId = [pushId retain];
    
    [self updateSubjectView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [TKUIUtil hiddenHUD];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    
    [_exitOrDraftView release];
    
    [_background1 release];
    [_saveNMSBarButton release];
    [_copyImageView release];
    [_subject release];
    [_pushId release];
    [_keysArray release];
    [_imagesArray release];
    [_image release];
    [_subjectContent release];
    [textview release];
    [_pageControl release];   
    
    [recorder release];
	[session release];
    //    [audioPlayer release];
    [meter release];
    
    [_filePath release];
    [_attachmentViewArray release];
    
    [_initPhoneNumberArray release];
    [_initContent release];
    
    [super dealloc];

}

@end
