//
//  StatesModel.h
//  F3_UI
//
//  Created by  on 11-9-1.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TTModel.h"

@interface StatesModel : NSObject<TTModel> {
    NSMutableArray *_delegates;
    NSMutableArray *_contacts;
    NSMutableArray *_allContacts;
}
@property (nonatomic, retain) NSMutableArray *contacts;

+ (NSMutableArray *)theContacts;
- (id)initWithContacts:(NSArray *)theContacts;
- (void)loadContacts;
- (void)search:(NSString *)text;

@end

