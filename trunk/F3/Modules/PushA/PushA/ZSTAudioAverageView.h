//
//  AudioAverageView.h
//  AudioVideo
//
//  Created by xuhuijun on 11-12-9.
//  Copyright 2011年 掌上通. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ZSTAudioAverageView : UIView

{
    UIWindow *_window;
    CGRect _rect;
    UIImageView *_topImageView;
    
    UILabel *_timeLabel;
    
    UIButton *_stopButton;
    UIButton *_pauseButton;
    UIButton *_continueButton;
    
    CGFloat _topImageOriginHeight;
}

@property(nonatomic)CGRect rect;
@property(nonatomic,retain)UIImageView *topImageView;
@property(nonatomic,retain)UILabel *timelabel;
@property(nonatomic,retain)UIButton *stopButton;
@property(nonatomic,retain)UIButton *pauseButton;
@property(nonatomic,retain)UIButton *continueButton;
@property(nonatomic)CGFloat topImageOriginHeight;

-(void)updateTopImageViewRect:(CGRect)rect;
- (void)dismiss;
- (void)show;
@end
