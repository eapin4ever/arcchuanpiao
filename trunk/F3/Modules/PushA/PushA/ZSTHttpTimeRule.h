//
//  ZSTPointsInTime.h
//  TestImageCache
//
//  Created by xuhuijun on 12-4-13.
//  Copyright 2012年 掌上通. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTTimeRange : NSObject 

@property (nonatomic ,retain) NSString *startTime;
@property (nonatomic ,retain) NSString *endTime;
@property (nonatomic ,assign) NSTimeInterval interval;

@end


@interface ZSTHttpTimeRule : NSObject

@property (nonatomic, retain, readonly) NSArray *timeRanges;

- (id)initWithHTTPPollTimeRule:(NSString *)httpPollTimeRule;

- (NSArray *)getTimePointsForDate:(NSDate *)date;

@end
