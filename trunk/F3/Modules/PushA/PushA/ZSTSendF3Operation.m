//
//  ZSTSendF3Operation.m
//  F3Engine
//
//  Created by luobin on 4/23/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTSendF3Operation.h"
#import "ZSTF3Preferences.h"
#import "MessageDao.h"
#import "ZSTF3Engine.h"
#import "ZipArchive.h"
#import "ZSTUtils.h"


@interface ZSTSendF3Operation()

- (BOOL)attachmentExist;

- (NSString *)zipMessageAttachments;

@end

@implementation ZSTSendF3Operation

@synthesize messageInfo;
@synthesize alertWhenFinish;
@synthesize error;

- (void)setError:(NSError *)theError
{
    if (error != theError) {
        [error release];
        error = [theError retain];
    }
}

- (id)initWithF3Message:(MessageInfo *)theMessageInfo
{
    self = [super init];
    if (self) {
        // Initialization code here.
        
        messageInfo = [theMessageInfo copy];
        self.alertWhenFinish = YES;
    }
    return self;
}

- (void)dealloc
{
    TKRELEASE(messageInfo);
    [super dealloc];
}

- (BOOL)attachmentExist
{
    NSArray *attachmentFileName = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[ZSTUtils pathForSendFileOfOutbox:messageInfo.ID] error: nil];
    return [attachmentFileName count];
}

- (NSArray *)attachments
{
    NSMutableArray *attachments = [NSMutableArray array];
    
    NSString *sendPath = [ZSTUtils pathForSendFileOfOutbox:messageInfo.ID];
    NSArray *attachmentFileNames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:sendPath error:nil];
    
    for (NSString *name in attachmentFileNames) {
        NSString *attachmentFilePath = [[ZSTUtils pathForSendFileOfOutbox:messageInfo.ID] stringByAppendingPathComponent:name];
        [attachments addObject:attachmentFilePath];
    }
    return attachments;
}

- (NSString *)zipMessageAttachments
{
    NSString *attachmentZipPath = [[ZSTUtils pathForOutbox] stringByAppendingPathComponent:@"nms_sending_attachment.zip"];
    
    ZipArchive *zipArchive = [[ZipArchive alloc] init];
    [zipArchive CreateZipFile2:attachmentZipPath];
    
    NSString *sendPath = [ZSTUtils pathForSendFileOfOutbox:messageInfo.ID];
    NSArray * attachmentFileName = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:sendPath error:nil];
    
    for (NSString *name in attachmentFileName) {
        
        NSString *attachmentFilePath = [[ZSTUtils pathForSendFileOfOutbox:messageInfo.ID] stringByAppendingPathComponent:name];
        [zipArchive addFileToZip: attachmentFilePath newname: name];
    }
    
    [zipArchive CloseZipFile2];
    [zipArchive release];
    return attachmentZipPath;
}

- (void)failedWithError:(NSError *)theError
{
    TKDERROR(@"Send F3 message Error. \n%@", theError);
    
    messageInfo.state = NMSStateSentFailed;
    [ZSTF3Engine saveF3Message:messageInfo error:nil];
    postNotificationOnMainThreadNoWait(NotificationName_NMSListUpdated);
    [self performSelectorOnMainThread:@selector(alertWithFailed:) withObject:@"信息发送失败" waitUntilDone:NO];
}

- (void)alertWithFailed:(NSString *)title
{
    if (alertWhenFinish) {
        //        [TKUIUtil alertInWindow:title withImage:[UIImage imageNamed:@"icon_warning.png"]];
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_MessageSendFaild object:title];
    }
}

- (void)alertWithSuccess:(NSString *)title
{
    if (alertWhenFinish) {
        
        //        [TKUIUtil alertInWindow:title withImage:[UIImage imageNamed:@"icon_smile_face.png"]];
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_MessageSendSucessed object:title];
    }
}

- (void)main
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    
    NSString * loginMsisdn = preferences.loginMsisdn;
    if (loginMsisdn == nil || [loginMsisdn isEqualToString:@""])
    {
        [self performSelectorOnMainThread:@selector(alertWithFailed:) withObject:NSLocalizedString(@"尚未绑定\n不能发送信息", nil) waitUntilDone:NO];
        
        return;
    }
    
    //保存到数据库
    if (!messageInfo.fileId) {
        messageInfo.fileId = @"0";
    }
    messageInfo.state = NMSStateSending;
    messageInfo.isReceived = NO;
    messageInfo.typeId = PERSONAL_MSGTYPE;
    messageInfo.time = [NSDate date];
    
    NSError *theError = nil;
    NSInteger ID = [ZSTF3Engine saveF3Message:messageInfo error:&theError];
    if (ID == -1) {
        //保存失败, 不可恢复
        [self failedWithError:theError];
        return;
    }
    postNotificationOnMainThreadNoWait(NotificationName_NMSListUpdated);
    messageInfo.ID = ID;
    
    //获取断点位置
    NSString *attachmentZipPath = nil;
    NSString *FileId = @"";
    unsigned long long Location = 0;
    unsigned long long fileSize = 0;
    BOOL attachmentExist = [self attachmentExist];
    if (attachmentExist) {
        attachmentZipPath = [self zipMessageAttachments];
        fileSize = [[[NSFileManager defaultManager] attributesOfItemAtPath:attachmentZipPath error:nil] fileSize];
        
        if(![ZSTF3Engine getUploadFileInfo:fileSize fileId:&FileId location:&Location])
        {
            [self failedWithError:theError];
            return;
        }
    }
    messageInfo.fileId = FileId;
    
    //如果有附件，上传附件
    if (attachmentExist && fileSize > Location) {
        
        if (![ZSTF3Engine uploadFileBreakPoint:FileId data:[NSData dataWithContentsOfFile:attachmentZipPath] location:Location progressDelegate:self]) {
            
            [self failedWithError:theError];
            return;
        };
    }
    
    //发送f3信息
    NSString *msgId = [ZSTF3Engine sendF3:messageInfo.userID 
                                  forward:messageInfo.forwarded 
                                  subject:messageInfo.subject 
                                  content:messageInfo.content 
                                   fileId:messageInfo.fileId 
                              attachments:[self attachments] 
                                   report:messageInfo.report];
    if (!msgId) {
        [self failedWithError:theError];
        return;
    } else {
        messageInfo.state = NMSStateSended;
        messageInfo.MSGID = msgId;
    }
    
    theError = nil;
    [ZSTF3Engine saveF3Message:messageInfo error:&theError];
    postNotificationOnMainThreadNoWait(NotificationName_NMSListUpdated);
    [self performSelectorOnMainThread:@selector(alertWithSuccess:) withObject:@"信息发送成功" waitUntilDone:NO];
}

- (void)request:(ASIHTTPRequest *)request didSendBytes:(long long)bytes
{
    unsigned long long progress = (unsigned long long)[request totalBytesSent];
    unsigned long long total = (unsigned long long)[request postLength];
    
    float progressAmount = (progress*1.0f)/(total*1.0f);
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithFloat:progressAmount],
                              USERINFO_KEY_PROGRESSAMOUNT,
                              [NSNumber numberWithInteger:messageInfo.ID],
                              USERINFO_KEY_ID,
                              nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_Progress object:userInfo];
}
@end
