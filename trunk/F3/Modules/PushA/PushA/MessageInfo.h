//
//  NSMInfo.h
//  F3_UI
//
//  Created by huang austin on 11-6-17.
//  Copyright 2011 北航. All rights reserved.
//

#import <Foundation/Foundation.h>

//typedef enum NMSState
//{
//    NMSStateNotRead = 1,
//    NMSStateRead    = 2,
//    NMSStateReceiving = 3,
//    NMSStateReceivedButException = 70,
//    NMSStateReceivedFailed = 4,
//    
//    NMSStateDraft   = 10,
//    NMSStateSending    = 11,
//    NMSStateSended    = 12,
//    NMSStateSentFailed = 13,
//    NMSStateHaveArrived          = 35,
//    NMSStateHaveRead             = 45
//
//} NMSState;


typedef NS_ENUM(NSInteger, NMSState)
{
    NMSStateNotRead = 1,
    NMSStateRead    = 2,
    NMSStateReceiving = 3,
    NMSStateReceivedButException = 70,
    NMSStateReceivedFailed = 4,
    
    NMSStateDraft   = 10,
    NMSStateSending    = 11,
    NMSStateSended    = 12,
    NMSStateSentFailed = 13,
    NMSStateHaveArrived          = 35,
    NMSStateHaveRead             = 45
};


@interface MessageInfo : NSObject<NSCopying>

@property(assign) NSInteger ID;
@property(copy)  NSString *MSGID;
@property(copy)  NSString *pushId;
@property(copy) NSString *typeId;

@property(copy)  NSString *userID;
@property(copy)  NSString *userName;

@property(copy) NSString *encryptKey;

@property(copy) NSString *subject;
@property(copy) NSDate *time;

@property(nonatomic, copy) NSString *content;
//
@property(nonatomic, copy) NSString *fileId;

@property (assign) NMSState state;   // 网信状态

@property(copy) NSString *forwarded;

@property(assign) BOOL isReceived;
@property(assign) BOOL isPrivate;
@property(assign) BOOL isLocked;
@property(assign) BOOL report;


@end

