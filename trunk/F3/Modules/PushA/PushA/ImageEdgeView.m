//
//  ChartView.m
//  Chart
//
//  Created by fangyp on 11-6-15.
//  Copyright 2011 EmatChina. All rights reserved.
//

#import "ImageEdgeView.h"


@implementation ImageEdgeView


- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    if (self) {
		self.opaque = YES;
		self.clearsContextBeforeDrawing = YES;
		self.backgroundColor = [UIColor clearColor];
		
    }
    return self;
}

- (void)drawRect:(CGRect)rect;
{  
    for(int i = 0 ; i < 4; i++){
        //横线
        CGContextRef context = UIGraphicsGetCurrentContext(); 
        CGContextSetRGBStrokeColor(context, 175/255.0, 177/255.0, 178/255.0, 1.0); // blue line
        CGContextMoveToPoint(context, 0.0, 49+49*i); //start point
        CGContextAddLineToPoint(context, 320.0*3, 49+49*i);
        CGContextSetLineWidth(context, 0.5);
        CGContextStrokePath(context); 
        
        for (int j = 0; j<24; j++) {
            //竖线
            CGContextSetRGBStrokeColor(context, 175/255.0, 177/255.0, 178/255.0, 1.0); // blue line
            CGContextMoveToPoint(context, 40.0+40*j, 0); //start point
            CGContextAddLineToPoint(context, 40.0+40*j, 216);
            CGContextSetLineWidth(context, 0.5);
            CGContextStrokePath(context); 
        }
    }

}
- (void)dealloc {
    [super dealloc];
}
@end
