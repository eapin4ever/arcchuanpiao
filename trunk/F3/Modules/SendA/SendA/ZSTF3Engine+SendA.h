//
//  ZSTF3Engine-News.h
//  News
//
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTF3Engine.h"
#import "ZSTSendAMessageInfo.h"

@protocol ZSTF3EngineSendADelegate <ZSTF3EngineDelegate>

@end

@interface ZSTF3Engine (SendA)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 =======
 *	@brief	保存新消息或替换已有的消息，如果是保存新消息，复制附件到对应的发送文件夹下
 *
 *	@param 	messageInfo
 *  @param 	error
 *
 *	@return	ID, 如果保存失败返回－1
 */
- (NSInteger)sendA_SaveF3Message:(ZSTSendAMessageInfo *)messageInfo error:(NSError **)error;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief	发送f3信息
 *
 *	@param 	messageInfo 	要发送的f3信息
 *	@param 	flag 	失败或成功后是否需要提示
 */
- (void)sendA_SendF3:(ZSTSendAMessageInfo *)messageInfo alertWhenFinish:(BOOL)flag;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief	获取上传文件断点位置信息
 *
 *	@param 	fileSize 	输入参数，文件总大小
 *	@param 	fileId      服务器为每个上传的文件指定一个id，以便断点续传, 为空时或者@"0"时，服务器会重新指定。输出或输入参数
 *	@param 	location 	已上传文件大小。输出参数
 *
 *	@return	操作是否成功
 */
- (BOOL)sendA_GetUploadFileInfo:(unsigned long long)fileSize fileId:(NSString **)fileId  location:(unsigned long long *)location;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief	断点续传
 *
 *	@param 	fileId 	<#fileId description#>
 *	@param 	data 	<#data description#>
 *	@param 	location 	上次上传到的位置
 *
 *	@return	操作是否成功
 */
- (BOOL)sendA_UploadFileBreakPoint:(NSString *)fileId data:(NSData *)data location:(unsigned long long)location progressDelegate:(id)progress;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *	@brief  按指令上传日志文件
 *
 *	@param 	data 	内容
 *	@param 	cmdID 	指令
 *	@param 	extension 文件类型
 *
 *	@return	操作是否成功
 */

- (NSString *)sendA_UploadFile:(NSData *)data cmdId:(int)cmdID extension:(NSString *)extension;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief	发送f3信息, 不应该直接调用此方法
 *
 *	@param 	forwardId       被转发信息的id，可以为空
 *	@param 	fileId          附件id
 *	@param 	attachments 	附件路径列表
 *	@param 	report          是否报告状态
 *
 *	@return	msgId 如果失败返回nil
 */
- (NSString *)sendA_SendF3:(NSString *)user
             subject:(NSString *)subject
             content:(NSString *)content
              fileId:(NSString *)fileId
         attachments:(NSArray *)attachments
              report:(BOOL)report;


/**
 *	@brief	恢复下载状态为"发送中"的信息
 */
- (void)sendA_ResumeSendMessage;


//######################################################## URL asynchrounous request methods ###################################################


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void) sendA_DeleteAllMessage;

- (void) sendA_DeleteMessage:(ZSTSendAMessageInfo *) nmsInfo;

- (void) sendA_DeleteExpiredMessage;
    
@end
