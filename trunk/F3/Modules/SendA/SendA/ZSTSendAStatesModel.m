//
//  StatesModel.m
//  F3_UI
//
//  Created by  on 11-9-1.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//
#import "ZSTSendAStatesModel.h"
#import "ContactInfo.h"
#import "ABContactsHelper.h"
#import "ChineseToPinyin.h"
#import "ContactData.h"

#import <TTModelViewController.h>

@implementation ZSTSendAStatesModel

@synthesize contacts = _contacts;

+ (NSMutableArray *)theContacts {
    
    NSMutableArray *theContactscontacts = [NSMutableArray arrayWithCapacity: 10];
	
    NSArray *contacts = [ABContactsHelper contacts];//接收电话簿所有信息
    
    
	for (ABContact *contact in contacts)
	{
		for (NSString *number in contact.phoneArray) 
		{
			ContactInfo *contactInfo = [[ContactInfo alloc] init];
            
			contactInfo.name = contact.contactName;
			
            number = [number stringByReplacingOccurrencesOfString: @"(" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @")" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @"\r" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @"\n" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @"\t" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @"-" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @" " withString: @""];
            number = [number stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            contactInfo.phoneNum = number;
            
			[theContactscontacts addObject: contactInfo];
			[contactInfo release];
        }
    }
    return theContactscontacts;
   
}

- (id)initWithContacts:(NSArray *)theContacts {
    if (self = [super init]) {
        _delegates = nil;
        _allContacts = [theContacts copy];
        _contacts = nil;
    }
    return self;
}

- (void)dealloc {
    TKRELEASE(_delegates);
    TKRELEASE(_allContacts);
    TKRELEASE(_contacts);
    [super dealloc];
}


- (void)loadContacts {
    TKRELEASE(_contacts);
    _contacts= [_allContacts mutableCopy];
}

- (void)search:(NSString*)text {
    [self cancel];
    self.contacts = [NSMutableArray array];
    
    [_delegates perform:@selector(modelDidStartLoad:) withObject:self];
    
    if (text.length) {
        text = [text lowercaseString];
        
        BOOL isAlpha = [text isAlpha];
        
        BOOL isNumber = [text isNumber];
        
        for (ContactInfo *contactInfo in _allContacts) 
        { 
            if(contactInfo.name !=nil && [ContactData searchResult:contactInfo.name searchText:text]){
                
                [_contacts addObject:contactInfo];
                
            }else if(isNumber && [ContactData searchResult:contactInfo.phoneNum searchText:text] ){
                [_contacts addObject:contactInfo];
                
            }else if (isAlpha && contactInfo.name != nil) 
            {
                NSString *pinYin = [ChineseToPinyin pinyinFromChiniseString:contactInfo.name];
                if (pinYin !=nil && [ContactData searchResult:pinYin searchText:[text uppercaseString]]) {
                    [_contacts addObject:contactInfo]; 
                }
            }
        }
    }
    [_delegates perform:@selector(modelDidFinishLoad:) withObject:self];
        
}

#pragma mark -
#pragma mark TTModel methods

- (NSMutableArray *)delegates {
    if (!_delegates) {
        _delegates = TKCreateNonRetainingArray();
    }
    return _delegates;
}

- (BOOL)isLoadingMore {
    return NO;
}

- (BOOL)isOutdated {
    return NO;
}

- (BOOL)isLoaded {
    return !!_contacts;
}

- (BOOL)isLoading {
    return NO;
}

- (BOOL)isEmpty {
    return !_contacts.count;
}

- (void)load:(NSURLRequestCachePolicy)cachePolicy more:(BOOL)more {
}

- (void)invalidate:(BOOL)erase {
}

- (void)cancel {
}

@end
