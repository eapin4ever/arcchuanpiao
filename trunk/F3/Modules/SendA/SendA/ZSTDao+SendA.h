//
//  ZSTDao.h
//  News
//  
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTDao(SendA)

+ (void)sendA_CreateTableIfNotExistForSendAModule;

//插入信息
- (NSInteger)sendA_InsertMessage:(ZSTSendAMessageInfo *)nmsInfo;

// 删除所有信息
-(void) sendA_DeleteAllMessage;

// 删除一条网信
-(void) sendA_DeleteMessage: (NSInteger) ID;

// 删除过期的信息
-(BOOL) sendA_DeleteMessageBeforeDate:(NSDate *)date;

//修改信息
- (BOOL)sendA_UpdateMessageById:(ZSTSendAMessageInfo *)nmsInfo;

// 获取网信列表
-(NSArray *) sendA_GetMessages;

//获取过期信息列表
-(NSArray *) sendA_GetMessagesBeforeDate:(NSDate *)date;

//获取商户网信列表
-(NSArray *) sendA_GetMessagesOfState: (NMSState)state;

//获取所有未读f3信息条数
-(NSUInteger) getCount:(NMSState)state;

//获取所有f3信息条数
-(NSUInteger) getCount;

//设置发件状态
- (BOOL)setOutMessage:(NSInteger)ID MSGID:(NSString *)MSGID withState:(NMSState)state;


@end
