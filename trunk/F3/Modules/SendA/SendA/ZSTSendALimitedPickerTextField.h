//
//  LimitedPickerTextField.h
//  F3
//
//  Created by 9588 on 9/14/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "TTPickerTextField.h"
#import <UIKit/UIKit.h>


#define kEmpty 1
#define kSelected 2
#define kValue 3

@interface ZSTSendALimitedPickerTextField : TTPickerTextField 
{
    
@private
    NSInteger _state;
    
}

@property (retain ,nonatomic) UIView *superviewForSearchResultsTableView;

@property (nonatomic, assign) NSInteger maxLineOfShow;

@property (nonatomic, assign) NSInteger states;

//- (CGFloat)heightWithLines:(NSInteger)lines;

//- (CGFloat)layoutCells;

-(void)autoDetectPhoneNumber;

@end
