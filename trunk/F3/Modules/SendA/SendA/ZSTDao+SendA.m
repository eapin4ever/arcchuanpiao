//
//  ZSTDao.m
//  News
//  
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTDao+SendA.h"
#import "ZSTSqlManager.h"
#import "ZSTUtils.h"

#define CREATE_TABLE_CMD                                        @"CREATE TABLE IF NOT EXISTS [SendA_MessageInfo] (\
                                                                [ID] INTEGER PRIMARY KEY,\
                                                                [MSGID] TEXT  NULL DEFAULT '',\
                                                                [userID] TEXT NULL DEFAULT '',\
                                                                [userName] TEXT NULL DEFAULT '',\
                                                                [subject] TEXT NULL DEFAULT '',\
                                                                [time] double NULL DEFAULT 0,\
                                                                [content] TEXT NULL DEFAULT '',\
                                                                [forwarded] TEXT NULL DEFAULT '',\
                                                                [encryptKey] TEXT NULL DEFAULT '',\
                                                                [isLocked] INTEGER DEFAULT 0,\
                                                                [state] INTEGER NULL DEFAULT 0,\
                                                                [report] INTEGER NULL DEFAULT 0,\
                                                                [notifyFrom] INTEGER NULL DEFAULT 0,\
                                                                [fileId] TEXT NULL DEFAULT ''\
                                                                );"

TK_FIX_CATEGORY_BUG(SendA)

@implementation ZSTDao(SendA)

+ (void)sendA_CreateTableIfNotExistForSendAModule
{
    [ZSTSqlManager executeSqlWithSqlStrings:CREATE_TABLE_CMD];
}

- (NSInteger)sendA_InsertMessage:(ZSTSendAMessageInfo *)nmsInfo
{
    if (nmsInfo.ID > 0) {
        return [ZSTSqlManager executeInsert:@"insert or replace into SendA_MessageInfo (MSGID, state, userID, userName, encryptKey, subject, time, isLocked, content, fileId, report, ID) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                [ZSTUtils objectForArray:nmsInfo.MSGID],
                [NSNumber numberWithInt:nmsInfo.state],
                nmsInfo.userID,
                [ZSTUtils objectForArray:nmsInfo.userName],
                [ZSTUtils objectForArray:nmsInfo.encryptKey],
                [ZSTUtils objectForArray:nmsInfo.subject],
                [ZSTUtils objectForArray:nmsInfo.time],
                [NSNumber numberWithInt:nmsInfo.isLocked],
                [ZSTUtils objectForArray:nmsInfo.content],
                [ZSTUtils objectForArray:nmsInfo.fileId],
                [NSNumber numberWithBool:nmsInfo.report],
                [NSNumber numberWithInteger:nmsInfo.ID]];
        
    } else {
        return [ZSTSqlManager executeInsert:@"insert or replace into SendA_MessageInfo (MSGID, state, userID, userName, encryptKey, subject, time, isLocked, content, fileId, report) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                [ZSTUtils objectForArray:nmsInfo.MSGID],
                [NSNumber numberWithInt:nmsInfo.state],
                nmsInfo.userID,
                [ZSTUtils objectForArray:nmsInfo.userName],
                [ZSTUtils objectForArray:nmsInfo.encryptKey],
                [ZSTUtils objectForArray:nmsInfo.subject],
                [ZSTUtils objectForArray:nmsInfo.time],
                [NSNumber numberWithInt:nmsInfo.isLocked],
                [ZSTUtils objectForArray:nmsInfo.content],
                [ZSTUtils objectForArray:nmsInfo.fileId],
                [NSNumber numberWithBool:nmsInfo.report]];
    }
}

-(void) sendA_DeleteAllMessage
{
    // 从数据库中删除
    NSString * sql = @"Delete from SendA_MessageInfo where isLocked = 0" ;
    [ZSTSqlManager executeUpdate:sql];
}

-(void) sendA_DeleteMessage: (NSInteger)ID
{
    // 从数据库中删除
    NSString * sql = [NSString stringWithFormat: @"Delete from SendA_MessageInfo where ID = %ld" , ID];
    [ZSTSqlManager executeUpdate:sql];
    
}

-(BOOL) sendA_DeleteMessageBeforeDate:(NSDate *)date;
{
    NSString *sql = @"Delete from SendA_MessageInfo where time < ? and isLocked = 0";//只保留最近的15天的记录
    
    return [ZSTSqlManager executeUpdate:sql, date];
}


- (BOOL)sendA_UpdateMessageById:(ZSTSendAMessageInfo *)nmsInfo
{
    NSString *sql = @"update SendA_MessageInfo set MSGID = ?, State = ?, userID = ?, userName = ?, encryptKey = ?, subject = ?, time = ?, isLocked = ?, report = ?, content = ?, fileId = ? where ID =  ?";
    
    return [ZSTSqlManager executeUpdate:sql,
            [ZSTUtils objectForArray:nmsInfo.MSGID],
            [NSNumber numberWithInt:nmsInfo.state],
            [ZSTUtils objectForArray:nmsInfo.userID],
            [ZSTUtils objectForArray:nmsInfo.userName],
            [ZSTUtils objectForArray:nmsInfo.encryptKey],
            [ZSTUtils objectForArray:nmsInfo.subject],
            nmsInfo.time,
            [NSNumber numberWithInt:nmsInfo.isLocked],
            [NSNumber numberWithBool:nmsInfo.report],
            [ZSTUtils objectForArray:nmsInfo.content],
            [ZSTUtils objectForArray:nmsInfo.fileId],
            [NSNumber numberWithInteger:nmsInfo.ID]];
}


- (ZSTSendAMessageInfo *)_populateMessageFromResultSet:(NSDictionary *)rs
{
    ZSTSendAMessageInfo *messageInfo = [[ZSTSendAMessageInfo alloc] init];
    messageInfo.ID = [[rs safeObjectForKey:@"ID"] integerValue];
    messageInfo.MSGID = [rs safeObjectForKey:@"MSGID"];
    messageInfo.state  = [[rs safeObjectForKey:@"state"] integerValue];
    messageInfo.userID = [rs safeObjectForKey:@"userID"];
    messageInfo.userName = [rs safeObjectForKey:@"userName"];
    messageInfo.encryptKey = [rs safeObjectForKey: @"encryptKey"];
    messageInfo.subject =  [rs safeObjectForKey: @"subject"];
    messageInfo.time = [NSDate dateWithTimeIntervalSince1970:[[rs safeObjectForKey:@"time"] floatValue]];
    messageInfo.report = [[rs safeObjectForKey:@"report"] boolValue];
    messageInfo.isLocked = [[rs safeObjectForKey:@"isLocked"] boolValue];
    messageInfo.content = [rs safeObjectForKey:@"content"];
    messageInfo.fileId = [rs safeObjectForKey:@"fileId"];
    return [messageInfo autorelease];
}

-(NSArray *) sendA_GetMessages
{
    NSMutableArray *nmsList = [NSMutableArray array];
    NSString *sql = @"Select id, MSGID, state, userID, userName, encryptKey, subject, time, isLocked, report,forwarded, content, fileId from SendA_MessageInfo order by time desc, id desc";
    NSArray *resultSet = [ZSTSqlManager executeQuery:sql];
    
    for (NSDictionary *rs in resultSet) {
        [nmsList addObject: [self _populateMessageFromResultSet:rs]];
    }
    return nmsList;
}

-(NSArray *) sendA_GetMessagesBeforeDate:(NSDate *)date
{
    NSMutableArray *nmsListArrayOfExpired =[NSMutableArray array];
    
    NSString *sql = @"Select id, MSGID, State, userID, userName, encryptKey,  subject, time, isLocked ,report,forwarded, content, fileId from SendA_MessageInfo where isLocked = 0";
    
    NSArray *resultSet = [ZSTSqlManager executeQuery:sql];
    
    for (NSDictionary *rs in resultSet) {
        [nmsListArrayOfExpired addObject: [self _populateMessageFromResultSet:rs]];
    }
    return nmsListArrayOfExpired;
}

-(NSArray *) sendA_GetMessagesOfState: (NMSState)state
{
    NSMutableArray *nmsListArrayOfExpired =[NSMutableArray array];
    NSString *sql = @"Select id, MSGID, State, userID, userName, encryptKey,  subject, time, isLocked,report,forwarded, content, fileId from SendA_MessageInfo where State = ? order by time desc";
    NSArray *resultSet = [ZSTSqlManager executeQuery:sql, [NSNumber numberWithInt:state]];
    for (NSDictionary *rs in resultSet) {
        [nmsListArrayOfExpired addObject: [self _populateMessageFromResultSet:rs]];
    }
    return nmsListArrayOfExpired;
}

-(NSUInteger) getCount:(NMSState)state
{
    NSString *sql = @"Select count(id) from SendA_MessageInfo where state = ?";
    
    return [ZSTSqlManager intForQuery:sql, [NSNumber numberWithInt:state]];
}

-(NSUInteger) getCount
{
    NSString *sql = @"Select count(id) from SendA_MessageInfo";
    
    return [ZSTSqlManager intForQuery:sql];
}

//设置发件状态
- (BOOL)setOutMessage:(NSInteger)ID MSGID:(NSString *)MSGID withState:(NMSState)state
{
    NSString *sql = @"Update SendA_MessageInfo Set MSGID = ?,state = ? where ID = ?";
    
    return [ZSTSqlManager executeUpdate:sql, [ZSTUtils objectForArray:MSGID], [NSNumber numberWithInt:state], [NSNumber numberWithInteger:ID]];
}

@end
