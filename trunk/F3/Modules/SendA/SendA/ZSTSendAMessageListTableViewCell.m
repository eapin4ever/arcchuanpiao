//
//  CustomTableViewCell.m
//  TestTableView
//
//  Created by Xie Wei on 11-5-16.
//  Copyright 2011年 e-linkway.com. All rights reserved.
//

#import "ZSTSendAMessageListTableViewCell.h"
#import "ZSTUtils.h"
#import "TKUIUtil.h"

@implementation ZSTSendAMessageListTableViewCell

@synthesize leftImageView = _leftImageView;
@synthesize labelDate = _labelDate;
@synthesize labelName = _labelName;
@synthesize labelIsRead = _labelIsRead;
@synthesize labelTitle = _labelTitle;
@synthesize labelIsSend = _labelIsSend;
@synthesize separatorLabel= _separatorLabel;
@synthesize lockLabel = _lockLabel;
@synthesize isLock = _isLock;
@synthesize editStateMark = _editStateMark;
@synthesize inEditing = _inEditing;
@synthesize transceivingLabel = _transceivingLabel;
@synthesize progressLabel = _progressLabel;
@synthesize percentLabel = _percentLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        _leftImageView = [[TKAsynImageView alloc] initWithFrame: CGRectMake(5, 7.5, 45, 45)];
        _leftImageView.backgroundColor = [UIColor whiteColor];
        _leftImageView.defaultImage = [UIImage imageNamed:@"icon.png"];
        _leftImageView.adjustsImageWhenHighlighted = NO;
        [self.contentView addSubview: _leftImageView];
        
        _progressLabel = [[UILabel alloc] initWithFrame: CGRectMake(0, 0, 45, 0)];
        _progressLabel.backgroundColor = [UIColor blackColor];
        _progressLabel.alpha = 0.3;
        _progressLabel.hidden = YES;
        [_leftImageView addSubview:_progressLabel];
        
        _percentLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 30, 45, 15)];
        _percentLabel.backgroundColor = [UIColor clearColor];
        _percentLabel.textColor = [UIColor whiteColor];
        _percentLabel.textAlignment = NSTextAlignmentCenter;
        _percentLabel.hidden = YES;
        [_leftImageView addSubview:_percentLabel];
        
        CALayer * layer1 = [_percentLabel layer];
        [layer1 setMasksToBounds:YES];
        [layer1 setCornerRadius:8.3];
        [layer1 setBorderWidth:1.0];
        [layer1 setBorderColor:[[UIColor clearColor] CGColor]];
        
        _labelDate = [[UILabel alloc] initWithFrame: CGRectMake(240, 8, 70, 20)];
        _labelDate.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        _labelDate.font = [UIFont systemFontOfSize: 10];
        _labelDate.backgroundColor = [UIColor clearColor];
        _labelDate.textColor = [UIColor grayColor];
        _labelDate.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview: _labelDate];
        [_labelDate release];
        
		_labelName = [[UILabel alloc] initWithFrame:CGRectMake(80, 8, 150, 20)];
        _labelName.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _labelName.backgroundColor = [UIColor clearColor];
		_labelName.font = [UIFont boldSystemFontOfSize:14];
        [self.contentView addSubview: _labelName];
        [_labelName release];
        
        _labelIsRead = [[UILabel alloc] initWithFrame:CGRectMake(60, 32, 60, 20)];
		_labelIsRead.font = [UIFont systemFontOfSize:13];
        _labelIsRead.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview: _labelIsRead];
        [_labelIsRead release];
		
        _labelTitle = [[UILabel alloc] initWithFrame:CGRectMake(100, 33, 210, 20)];
        _labelTitle.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _labelTitle.backgroundColor = [UIColor clearColor];
		_labelTitle.font = [UIFont systemFontOfSize:13];
        _labelTitle.text = @"";
        _labelTitle.textColor = [UIColor grayColor];
        [self.contentView addSubview: _labelTitle];
        [_labelTitle release];
        
        _lockLabel = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_lock.png"]];
        _lockLabel.frame = CGRectMake(CGRectGetMaxX(_leftImageView.frame)-20, CGRectGetMaxY(_leftImageView.frame)-25, 16, 16);
        _lockLabel.hidden = YES;
        _lockLabel.userInteractionEnabled = NO;
        _lockLabel.tag = 0;
        [_leftImageView addSubview:_lockLabel];
        [_lockLabel release];
        
        _transceivingLabel = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_send.png"]];
        _transceivingLabel.frame = CGRectMake(60, 10, 16, 16);
        _transceivingLabel.hidden = NO;
        [self.contentView addSubview:_transceivingLabel];
        [_transceivingLabel release];
        
    }
    return self;
}

- (UIImage*)imageNamed:(NSString*)imageNamed cache:(BOOL)cache
{
    UIImage* retImage = [_staticImageDictionary objectForKey:imageNamed];
    if (retImage == nil)
    {
        retImage = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:imageNamed]]];
        if (cache)
        {
            if (_staticImageDictionary == nil)
                _staticImageDictionary = [NSMutableDictionary new];
            
            [_staticImageDictionary setObject:retImage forKey:imageNamed];
        }
    }
    return retImage;
}

- (void)setCell:(ZSTSendAMessageInfo *) nmsInfo
{
    self.leftImageView.image = [UIImage imageNamed:@"personal.png"];
    self.labelName.text = nmsInfo.userID;
}

-(void) setNMSInfo: (ZSTSendAMessageInfo *) nmsInfo isLock:(BOOL)isLock editStateMark:(BOOL)editStateMark
{
    self.labelTitle.text = nmsInfo.content;
    
    self.labelDate.text = [ZSTUtils convertToTime: [NSString stringWithFormat:@"%f",[nmsInfo.time timeIntervalSince1970]]];
    
    
    if (nmsInfo.isLocked) {
        self.lockLabel.hidden = NO;
        self.lockLabel.image = [UIImage imageNamed:@"icon_lock.png"];
    } else if (isLock) {
        self.lockLabel.hidden = YES;
    } else {
        self.lockLabel.hidden = YES;
    }
    
    if (isLock || editStateMark || nmsInfo.state == NMSStateReceiving) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }else{
        self.selectionStyle = UITableViewCellSelectionStyleBlue;
    }
    
    self.lockLabel.userInteractionEnabled = isLock;
    
    switch (nmsInfo.state)
    {
        case NMSStateRead:
            
            self.labelIsRead.text = NSLocalizedString(@"[已读]" , nil);
            
            self.labelIsRead.textColor = [UIColor colorWithRed:11/255.0 green:76/255.0 blue:173/255.0 alpha:1];
            self.transceivingLabel.image = [UIImage imageNamed:@"icon_recived.png"];
            
            [self setCell:nmsInfo];
            
            break;
            
        case NMSStateNotRead:
            
            self.labelIsRead.text = NSLocalizedString(@"[未读]" , nil);
            
            self.labelIsRead.textColor = [UIColor colorWithRed:198/255.0 green:8/255.0 blue:21/255.0 alpha:1];
            self.transceivingLabel.image = [UIImage imageNamed:@"icon_recived.png"];
            
            [self setCell:nmsInfo];
            break;
            
        case NMSStateReceiving:
            
            self.labelIsRead.text = NSLocalizedString(@"[正在下载]" , nil);
            
            self.labelIsRead.textColor = [UIColor colorWithRed:198/255.0 green:8/255.0 blue:21/255.0 alpha:1];
            self.transceivingLabel.image = [UIImage imageNamed:@"icon_recived.png"];
            
            [self setCell:nmsInfo];
            break;
            
        case NMSStateReceivedFailed:
            
            self.labelIsRead.text = NSLocalizedString(@"[点击接收]" , nil);
            
            self.labelIsRead.textColor = [UIColor colorWithRed:198/255.0 green:8/255.0 blue:21/255.0 alpha:1];
            self.transceivingLabel.image = [UIImage imageNamed:@"icon_recived.png"];
            
            [self setCell:nmsInfo];
            break;
            
        case NMSStateDraft:
            
            self.labelIsRead.text = NSLocalizedString(@"[草稿]" , nil);
            
            self.labelIsRead.textColor = [UIColor colorWithRed:9/255.0 green:130/255.0 blue:49/255.0 alpha:1];
            self.transceivingLabel.image = [UIImage imageNamed:@"icon_draft.png"];
            
            [self setCell:nmsInfo];
            break;
            
        case NMSStateSending:
            
            self.labelIsRead.text = NSLocalizedString(@"[发送中]" , nil);
            
            self.labelIsRead.textColor = [UIColor grayColor];
            self.transceivingLabel.image = [UIImage imageNamed:@"icon_send.png"];
            [self setCell:nmsInfo];
            
            break;
            
        case NMSStateSended:
            
            
            self.labelIsRead.text = NSLocalizedString(@"[已发]" , nil);
            
            self.labelIsRead.textColor = [UIColor colorWithRed:11/255.0 green:76/255.0 blue:173/255.0 alpha:1];
            self.transceivingLabel.image = [UIImage imageNamed:@"icon_send.png"];
            [self setCell:nmsInfo];
            
            break;
            
        case NMSStateHaveArrived:
            
            self.labelIsRead.text = NSLocalizedString(@"[已送达]" , nil);
            
            self.labelIsRead.textColor = [UIColor colorWithRed:11/255.0 green:76/255.0 blue:173/255.0 alpha:1];
            self.transceivingLabel.image = [UIImage imageNamed:@"icon_send.png"];
            [self setCell:nmsInfo];
            
            break;
            
        case NMSStateSentFailed:
            
            self.labelIsRead.text = NSLocalizedString(@"[发送失败]" , nil);
            
            self.labelIsRead.textColor = [UIColor redColor];
            self.transceivingLabel.image = [UIImage imageNamed:@"icon_send.png"];
            [self setCell:nmsInfo];
            
            break;
            
        case NMSStateHaveRead:
            
            self.labelIsRead.text = NSLocalizedString(@"[已阅读]" , nil);
            
            self.labelIsRead.textColor = [UIColor colorWithRed:11/255.0 green:76/255.0 blue:173/255.0 alpha:1];
            self.transceivingLabel.image = [UIImage imageNamed:@"icon_send.png"];
            [self setCell:nmsInfo];
            
            break;

        default:
            
            self.labelIsRead.text = NSLocalizedString(@"[异常]" , nil);
            
            self.labelIsRead.textColor = [UIColor redColor];
            
            break;
    }
    
    CGRect labelTitleFrame = self.labelTitle.frame;
    labelTitleFrame.origin.x = 95 + ([self.labelIsRead.text length]-4)*13;
    self.labelTitle.frame = labelTitleFrame;
    
}


- (void)drawRect:(CGRect)rect {

    [ZSTModuleImage(@"module_senda_comment_topline.png") drawInRect:CGRectMake(rect.origin.x, CGRectGetMaxY(rect) - 2, rect.size.width, 2)];
    
    [super drawRect:rect];  
}

@end
