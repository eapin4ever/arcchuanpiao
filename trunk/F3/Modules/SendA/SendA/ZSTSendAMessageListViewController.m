//
//  my_f3.m
//  Net_Information
//
//  Created by huqinghe on 11-6-5.
//  Copyright 2011 Shinetechchina.com. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>

#import "ZSTSendAMessageListViewController.h"

#import "ZSTUtils.h"
#import "TKUIUtil.h"

#import "ZSTLogUtil.h"

#import "ZSTDao+SendA.h"

#import "ZSTSendAMessageListTableViewCell.h"
#import "ZSTEditView.h"
#import "ZSTSendACreateNMSDialogViewController.h"
#import "EGORefreshTableHeaderView.h"
#import "BaseNavgationController.h"

@implementation ZSTSendAMessageListViewController

@synthesize tableView = _tableView;

+(void)initialize
{
    [ZSTDao sendA_CreateTableIfNotExistForSendAModule];
}

- (void)moduleApplication:(ZSTModuleApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
}

#pragma mark - UIScrollViewDelegate

- (void)updateFooterView
{
    if ([_nmsArray count] == 0) {
        _backgroundView.hidden = NO;
        _backgroundView.image = [UIImage imageNamed:@"nullNMS.png"];

    } else {
        
        _backgroundView.hidden = NO;
        _backgroundView.image = [UIImage imageNamed:@"bg.png"];
    }
}

-(void) updateTableView
{   
    [_nmsArray release];
    _nmsArray = (NSMutableArray *)[[self.dao sendA_GetMessages] retain];
    
    [self updateFooterView];
    if (_isEditing) {
        [self setEditing:NO animated:YES];
        [self setEditing:YES animated:YES];
    }
    [_tableView reloadData];
}


#pragma mark UDP方面要集成进来
#pragma mark TODO: 使用子线程来获取 网信列表
// 这个函数，需要起一个子线程来处理。
// 子线程下载完数据后，将数据插入数据库中
// 然后发送一个通知消息，给主线程，说，有数据更新了。 (使用NSNotificationCenter来做会比较方便)
// 主线程接收到更新消息后，查询数据库，更新Array列表，更新UI。
// 这里查询数据库会牵扯到一个问题: 数据的损耗问题，因为插入数据库的数据我是知道的，现在又要将插入数据库的数据读取出来，然后再Update，造成资源浪费。
// 解决方案: 将增量到的数据，想办法直接给主线程使用。

-(void) createNMS
{  
    [_editView dismiss];
    [self setEditing:NO animated:YES];
    _isLock = NO;
    _editStateMark = NO;
    _isEditing = NO;
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[ZSTUtils pathForTempFinderOfOutbox]]) {
        [[NSFileManager defaultManager] removeItemAtPath:[ZSTUtils pathForTempFinderOfOutbox] error:nil];
    }
    
    ZSTSendACreateNMSDialogViewController *creatNMSDialog = [[ZSTSendACreateNMSDialogViewController alloc] init];
    creatNMSDialog.shouldSaveToDraft = NO;
    creatNMSDialog.isDraftEdit = NO;
    BOOL isCoverEdit = [NSLocalizedString(@"GP_EditType",@"0") intValue] == 1?YES:NO;
    if (isCoverEdit) {
        ZSTF3Preferences *dataManager = [ZSTF3Preferences shared];
        [creatNMSDialog getReplyUserNumber:dataManager.ECECCID userName:dataManager.ECECCID];
    }
    creatNMSDialog.hidesBottomBarWhenPushed = YES;
    [creatNMSDialog pushInNavigationViewController:self];
    [creatNMSDialog release];
}

-(void)setRightBarButtonItem
{
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"新建" , nil)  target:self selector:@selector(createNMS)];
}

- (void)viewDidLoad 
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor clearColor];
    self.hidesBottomBarWhenPushed = NO;
    self.navigationItem.titleView = [ZSTUtils logoView];
    [self setRightBarButtonItem];
    
    _isLock = NO;
    _editStateMark = NO;
    _isEditing = NO;
    
    UIView *inboxView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.height)];
    inboxView.tag = 99;
    [self.view addSubview:inboxView];
    inboxView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [inboxView release];

    _tableView = [[UITableView alloc] initWithFrame: CGRectMake(0, 0, 320, self.view.height) style: UITableViewStylePlain];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.dataSource = self;                                                  
    _tableView.delegate = self;
    _tableView.separatorColor = [UIColor clearColor];
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    
    _backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"nullNMS.png"]] autorelease];
    _backgroundView.frame = CGRectMake(0, 0, 320, self.view.height);
    _backgroundView.hidden = YES;
    _backgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [inboxView addSubview:_backgroundView];//一定要注意顺序
    [inboxView addSubview: _tableView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onNMSUpdate:) 
                                                 name:[NSString stringWithFormat:@"%@_%ld", NotificationName_MessageListUpdated, (long)self.moduleType]
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateProgress:) 
                                                 name:[NSString stringWithFormat:@"%@_%ld", NotificationName_Progress, (long)self.moduleType]
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(messageSendSuccess:) 
                                                 name: [NSString stringWithFormat:@"%@_%ld", NotificationName_MessageSendSucessed, (long)self.moduleType]
                                               object: nil];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(messageSendFailed:) 
                                                 name: [NSString stringWithFormat:@"%@_%ld", NotificationName_MessageSendFaild, (long)self.moduleType]
                                               object: nil];
    
    [self.engine sendA_ResumeSendMessage];
}


- (void)loginDidCancel
{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        if (![controller isKindOfClass:[self.rootViewController class]]) {
            
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
    NSNumber *shellId = [NSNumber numberWithInteger:[ZSTF3Preferences shared].shellId];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLoginCancel object:shellId];
}

- (void)loginFinish
{
    
}

- (void)messageSendFailed:(NSNotification *)notification
{   
    NSString *title = [notification object];  
    [TKUIUtil alertInView:self.view withTitle:title withImage:[UIImage imageNamed:@"icon_warning.png"]];
    
}

- (void)messageSendSuccess:(NSNotification *)notification
{  
    NSString *title = [notification object];  
    [TKUIUtil alertInView:self.view withTitle:title withImage:[UIImage imageNamed:@"icon_smile_face.png"]];
}

-(void)updateProgress:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification object];
    float progress = [[userInfo objectForKey:USERINFO_KEY_PROGRESSAMOUNT] floatValue];
    int ID = [[userInfo objectForKey:USERINFO_KEY_ID] intValue];
    
    ZSTSendAMessageListTableViewCell *cell = (ZSTSendAMessageListTableViewCell *)[_tableView viewWithTag:ID+100];
    if (cell != nil) {
        
        if (cell.progressLabel.frame.size.height != 0.0f) {
            [UIView beginAnimations:@"progress" context:nil];
            [UIView setAnimationDuration:0.4];
            
            cell.progressLabel.frame = CGRectMake(0, 45*progress, 45, 45*(1-progress));
            NSString *percent = [[[NSString stringWithFormat:@"%f",progress*100] substringToIndex:2] stringByAppendingString:@"%"];
            cell.percentLabel.text = percent;
            cell.percentLabel.hidden = NO;
            cell.percentLabel.font = [UIFont systemFontOfSize:10];
            [UIView commitAnimations];
         if (progress == 1.0f) {
                cell.percentLabel.hidden = YES;
            }
        }  else {
            cell.progressLabel.frame = CGRectMake(0, 45*progress, 45, 45*(1-progress));
        }
    }
}

- (void)onNMSUpdate:(NSNotification *) notification
{
    [self updateTableView];
}

#pragma mark ---------------------  EditView ------------------------------------------

-(void)delSingleMSGAction
{
    [_editView dismiss];
    [self setEditing:NO animated:YES];
    [self setEditing:YES animated:YES]; 
    _isEditing = YES;
    _isLock = NO;
    _editStateMark = NO;
    
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"完成", nil) target:self selector:@selector (endDelete)];
    [self updateTableView];
}

-(void)confirmDelAllMSGAction
{
    [self.engine sendA_DeleteAllMessage];
    
    [self updateFooterView];
    [self updateTableView];
    [_confirmDelView dismiss];
}

-(void)cancelDelAllMSGAction
{
    [_confirmDelView dismiss];
}

-(void)delAllMSGAction
{
   [_editView dismiss];
   [self setEditing:NO animated:YES]; 
    _isEditing = NO;

    UIImage *image = [UIImage imageNamed:@"confirmDelAll.png"];
    _confirmDelView  = [[ZSTEditView alloc] initWithImage:image];
    _confirmDelView.center = CGPointMake(320/2, (480+(iPhone5?88:0)-20-44)/2);
    
    UIButton *confirmDelMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmDelMSGBtn.frame = CGRectMake(6, 50, 98, 55);
    [confirmDelMSGBtn addTarget:self action:@selector(confirmDelAllMSGAction) forControlEvents:UIControlEventTouchUpInside];
    confirmDelMSGBtn.showsTouchWhenHighlighted = YES;
    confirmDelMSGBtn.backgroundColor = [UIColor clearColor];
    [_confirmDelView addSubview:confirmDelMSGBtn];
    
    UIButton *cancelDelAllMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelDelAllMSGBtn.frame = CGRectMake(106, 50, 98, 55);
    [cancelDelAllMSGBtn addTarget:self action:@selector(cancelDelAllMSGAction) forControlEvents:UIControlEventTouchUpInside];
    cancelDelAllMSGBtn.showsTouchWhenHighlighted = YES;
    cancelDelAllMSGBtn.backgroundColor = [UIColor clearColor];
    [_confirmDelView addSubview:cancelDelAllMSGBtn];
    
    [_confirmDelView show];
    [self updateTableView];
}

-(void)lockMSGAction
{
    [_editView dismiss];
    [self setEditing:NO animated:YES]; 
    [self setEditing:YES animated:YES]; 
    _isEditing = YES;
    self.navigationItem.rightBarButtonItem = nil;

    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"完成", nil) target:self selector:@selector (endDelete)];
    
    //编辑锁定状态
    _isLock = YES;
    _editStateMark = NO;
    [self updateTableView];

}

-(void)statusMSGAction
{
    [_editView dismiss];
    [self setEditing:NO animated:YES]; 
    [self setEditing:YES animated:YES]; 
    _isEditing = YES;
    self.navigationItem.rightBarButtonItem = nil;

    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"完成", nil) target:self selector:@selector (endDelete)];
    
    //编辑已读未读状态
    _editStateMark = YES;
    _isLock = NO;
    [self updateTableView];
}

-(void)endDelete
{
    [self setEditing:NO animated:YES];
    self.navigationItem.rightBarButtonItem = nil;
    
    [self setRightBarButtonItem];
    
    _isLock = NO;
    _editStateMark = NO;
    _isEditing = NO;
    [_tableView reloadData];
}

-(void)cancelMSGAction
{
    [_editView dismiss];
    [self setEditing:NO animated:YES]; 
    [self setEditing:YES animated:YES];    
    _isEditing = YES;
    _isLock = NO;
    _editStateMark = NO;
    
    [self endDelete];
    [self updateTableView];

}

-(void) edit
{  
    UIImage *image = nil;
    
    NSString *GP_Show_NewMessage = NSLocalizedString(@"GP_Show_NewMessage", nil);
    if ([GP_Show_NewMessage isEqualToString:@"0"]) {
        image = [UIImage imageNamed:@"edit_right_cancel.png"];
        
    }else{
        image = [UIImage imageNamed:@"edit_right_new.png"];
    }
    _editView = [[ZSTEditView alloc] initWithImage:image];
    _editView.frame = CGRectMake(175, 51, image.size.width, image.size.height);
    
    UIButton *lockMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    lockMSGBtn.frame = CGRectMake(6, 20, 65, 55);
    [lockMSGBtn addTarget:self action:@selector(lockMSGAction) forControlEvents:UIControlEventTouchUpInside];
    lockMSGBtn.showsTouchWhenHighlighted = YES;
    lockMSGBtn.backgroundColor = [UIColor clearColor];
    [_editView addSubview:lockMSGBtn];
    
    UIButton *statusMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    statusMSGBtn.frame = CGRectMake(73, 20, 65, 55);
    [statusMSGBtn addTarget:self action:@selector(statusMSGAction) forControlEvents:UIControlEventTouchUpInside];
    statusMSGBtn.showsTouchWhenHighlighted = YES;
    statusMSGBtn.backgroundColor = [UIColor clearColor];
    [_editView addSubview:statusMSGBtn];
    
    
    UIButton *delAllMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    delAllMSGBtn.frame = CGRectMake(6, 76, 65, 55);
    [delAllMSGBtn addTarget:self action:@selector(delAllMSGAction) forControlEvents:UIControlEventTouchUpInside];
    delAllMSGBtn.showsTouchWhenHighlighted = YES;
    delAllMSGBtn.backgroundColor = [UIColor clearColor];
    [_editView addSubview:delAllMSGBtn];
    
    UIButton *createNMS_cancel_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    createNMS_cancel_Btn.frame = CGRectMake(73, 76, 65, 55);
    
    if ([GP_Show_NewMessage isEqualToString:@"0"]) {
        
        [createNMS_cancel_Btn addTarget:self action:@selector(cancelMSGAction) forControlEvents:UIControlEventTouchUpInside];
        
    }else{
        [createNMS_cancel_Btn addTarget:self action:@selector(createNMS) forControlEvents:UIControlEventTouchUpInside];
    }
    
    createNMS_cancel_Btn.showsTouchWhenHighlighted = YES;
    createNMS_cancel_Btn.backgroundColor = [UIColor clearColor];
    [_editView addSubview:createNMS_cancel_Btn];
    
    [_editView show];
    
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
	[_tableView setEditing:editing animated:animated];

}

#pragma mark-----------------------UITableViewDataSource---------------------------------

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"CustomTableViewCell";
    
    ZSTSendAMessageListTableViewCell *cell = (ZSTSendAMessageListTableViewCell *)[tableView dequeueReusableCellWithIdentifier: cellIdentifier];
    ZSTSendAMessageInfo *nmsInfo = [_nmsArray objectAtIndex: indexPath.row];
    if (!cell)
    {
        cell = [[[ZSTSendAMessageListTableViewCell alloc] initWithStyle: UITableViewCellStyleDefault 
                                           reuseIdentifier: cellIdentifier] autorelease];
        cell.backgroundView = [[[UIView alloc] init] autorelease];
        cell.backgroundView.backgroundColor = [UIColor whiteColor];
    }
    cell.tag = nmsInfo.ID + 100;
    cell.isLock = _isLock;
    cell.editStateMark = _editStateMark;
    cell.inEditing = _isEditing;
   
    [cell setNMSInfo:nmsInfo isLock:_isLock editStateMark:_editStateMark];
    if (nmsInfo.state == NMSStateSending) {
     
        cell.progressLabel.hidden = NO;
    }
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_nmsArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 61;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTSendAMessageListTableViewCell *cell = (ZSTSendAMessageListTableViewCell *)[tableView cellForRowAtIndexPath: indexPath];
    ZSTSendAMessageInfo *nmsInfo = [_nmsArray objectAtIndex: indexPath.row];
    
    if (cell.selectionStyle == UITableViewCellSelectionStyleNone || nmsInfo.state == NMSStateSending)
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        return;
    } else if (nmsInfo.state == NMSStateSentFailed)
    {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        cell.labelIsRead.text = NSLocalizedString(@"[发送中]" , nil);
        cell.labelIsRead.textColor = [UIColor grayColor];
        [self.dao setOutMessage:nmsInfo.ID MSGID:nmsInfo.MSGID withState:NMSStateSending];
        [self.engine sendA_SendF3:nmsInfo alertWhenFinish:NO];
        return;
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

   if (nmsInfo.state == NMSStateSended || nmsInfo.state == NMSStateDraft 
              || nmsInfo.state == NMSStateHaveArrived || nmsInfo.state == NMSStateHaveRead)
    {
        if ([[NSFileManager defaultManager] fileExistsAtPath:[ZSTUtils pathForTempFinderOfOutbox]]) {
            [[NSFileManager defaultManager] removeItemAtPath:[ZSTUtils pathForTempFinderOfOutbox] error:nil];
        }
        ZSTSendACreateNMSDialogViewController * replyCreateNMSDialogViewController = [[ZSTSendACreateNMSDialogViewController alloc] init];
        replyCreateNMSDialogViewController.shouldSaveToDraft = NO;
        
        if (nmsInfo.state == NMSStateDraft) {
        replyCreateNMSDialogViewController.isDraftEdit = YES;
        }else{
            replyCreateNMSDialogViewController.isDraftEdit = NO;
        }
        replyCreateNMSDialogViewController.hidesBottomBarWhenPushed = YES;
        NSArray *phoneNumberArray = [nmsInfo.userID  componentsSeparatedByString:@","];
        [replyCreateNMSDialogViewController getOutContactNumber:phoneNumberArray];
        replyCreateNMSDialogViewController.textview.text = nmsInfo.content;
        replyCreateNMSDialogViewController.nmsID = [NSString stringWithFormat:@"%ld",(long)nmsInfo.ID];
        [replyCreateNMSDialogViewController pushInNavigationViewController:self];
        [replyCreateNMSDialogViewController  release];
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ZSTSendAMessageListTableViewCell *cell = (ZSTSendAMessageListTableViewCell *)[tableView cellForRowAtIndexPath: indexPath];
    ZSTSendAMessageInfo *nmsInfo = [_nmsArray objectAtIndex: indexPath.row];


	if (editingStyle == UITableViewCellEditingStyleDelete && !_editStateMark && !_isLock) 
	{

        [self.engine sendA_DeleteMessage: nmsInfo];
		
		[_nmsArray removeObjectAtIndex:indexPath.row];
        
        [tableView setEditing:NO animated:YES];
		[tableView deleteRowsAtIndexPaths: [NSArray arrayWithObject: indexPath] withRowAnimation: UITableViewRowAnimationFade];
        
        [self updateTableView];
        
        return;
	}
	else if (editingStyle == UITableViewCellEditingStyleDelete && _editStateMark)
	{
        
        if (nmsInfo.state == NMSStateRead) {
            nmsInfo.state = NMSStateNotRead;
            
        }else if(nmsInfo.state == NMSStateNotRead) 
        {
            nmsInfo.state = NMSStateRead;
            
        }
        [self.dao sendA_UpdateMessageById:nmsInfo];
        
        [cell setNMSInfo: nmsInfo isLock:_isLock editStateMark:_editStateMark];

	}else if (editingStyle == UITableViewCellEditingStyleDelete && _isLock)
    {
                
        if (nmsInfo.isLocked == NO) {
            nmsInfo.isLocked = YES;
        }else if( nmsInfo.isLocked == YES) 
        {
            nmsInfo.isLocked = NO;
        }
        [self.dao sendA_UpdateMessageById:nmsInfo];
   
        [cell setNMSInfo: nmsInfo isLock:_isLock editStateMark:_editStateMark];

        postNotificationOnMainThreadNoWait(([NSString stringWithFormat:@"%@_%ld", NotificationName_MessageListUpdated, (long)self.moduleType]));
    }
    
    [UIView beginAnimations:@"endEditingCell" context:nil];
    [UIView setAnimationDuration:0.5];
    
    CGRect labelNameFrame = cell.labelName.frame;
    labelNameFrame.size.width += 85;
    cell.labelName.frame = labelNameFrame;
    
    CGRect labelDateFrame = cell.labelDate.frame;
       labelDateFrame.origin.x += 85;
    cell.labelDate.frame = labelDateFrame;
    
    CGRect labelTitleFrame = cell.labelTitle.frame;
    labelTitleFrame.size.width += 85;
    cell.labelTitle.frame = labelTitleFrame;
    
    [UIView commitAnimations];
    
    [tableView reloadData];
}

#pragma mark-----UITableViewDelegate-----------------

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTSendAMessageInfo *nmsInfo = [_nmsArray objectAtIndex: indexPath.row];

    if (!_isLock && !_editStateMark && nmsInfo.isLocked == YES) 
    {
        return UITableViewCellEditingStyleNone;
    }else if (!_isLock && _editStateMark && 
             (nmsInfo.state == NMSStateSended 
                  || nmsInfo.state == NMSStateSending 
                  || nmsInfo.state == NMSStateSentFailed 
                  || nmsInfo.state == NMSStateDraft
                  || nmsInfo.state == NMSStateHaveArrived
                  || nmsInfo.state == NMSStateHaveRead
                  || nmsInfo.state == NMSStateReceiving))
    {
        return UITableViewCellEditingStyleNone;
    }else if (!_isLock && !_editStateMark &&
              (nmsInfo.state == NMSStateSending
               || nmsInfo.state == NMSStateReceiving))
    {
        return UITableViewCellEditingStyleNone;
    }
    
    return UITableViewCellEditingStyleDelete;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ZSTSendAMessageInfo *nmsInfo = [_nmsArray objectAtIndex: indexPath.row];

    if (_editStateMark && nmsInfo.state == NMSStateNotRead) {
        
        return NSLocalizedString(@"设为已读" , nil);
    }else if(_editStateMark && nmsInfo.state == NMSStateRead)
    {
        return NSLocalizedString(@"设为未读" , nil);
    }else if(_isLock && nmsInfo.isLocked == NO)
    {
        return NSLocalizedString(@"锁定信息" , nil);
    }else if(_isLock && nmsInfo.isLocked == YES)
    {
        return NSLocalizedString(@"解除锁定" , nil);
    }else if(_editStateMark && (nmsInfo.state == NMSStateReceivedFailed || nmsInfo.state == NMSStateReceiving || nmsInfo.state == 0))
    {
        return NSLocalizedString(@"无法更改" , nil);
    }
      return NSLocalizedString(@"删除" , nil);
}

- (void)viewWillAppear:(BOOL)animated
{
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0)
    {
        ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
        controller.isFromSetting = YES;
        controller.delegate = self;
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
        }];
        [navicontroller release];
        [controller release];
        
        return;
    }
    
    [self setEditing:NO animated:YES];
    [self updateTableView];
    self.navigationItem.hidesBackButton = YES;
}

#pragma mark -

- (void)dealloc 
{
    [_tableView release];
    [_nmsArray release];
    [_editView release];
    [_confirmDelView release];
    [super dealloc];
}

@end
