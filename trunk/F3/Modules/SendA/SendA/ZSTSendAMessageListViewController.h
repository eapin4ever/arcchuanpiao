//
//  my_f3.h
//  Net_Information
//
//  Created by huqinghe on 11-6-5.
//  Copyright 2011 Shinetechchina.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTSendACreateNMSDialogViewController.h"
#import "EGORefreshTableHeaderView.h"
#import "ZSTModuleBaseViewController.h"
#import "ZSTLoginViewController.h"

@class ZSTEditView;
// TODO:
// 初始化时，体现出来该ViewController是哪一个ViewController
@interface ZSTSendAMessageListViewController : ZSTModuleBaseViewController<UITableViewDelegate, UITableViewDataSource, ZSTF3EngineDelegate,LoginDelegate>
{
	UITableView *_tableView;
    
    // 发件箱时指向
	NSMutableArray *_nmsArray;
    
    UIImageView *_backgroundView;
    
    BOOL _isLock;
    BOOL _editStateMark;
    BOOL _isEditing;

    ZSTEditView *_editView;
    ZSTEditView *_confirmDelView;
}

@property (nonatomic, readonly) UITableView *tableView;

-(void) updateTableView;
-(void) updateFooterView;
- (void)onNMSUpdate:(NSNotification *) notification;
@end
