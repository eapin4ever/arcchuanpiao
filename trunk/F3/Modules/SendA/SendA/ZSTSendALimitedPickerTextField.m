

#import "ZSTSendALimitedPickerTextField.h"
#import "TTPickerTextField.h"
#import "UIViewAdditions.h"
#import "TTPickerViewCell.h"
#import "ContactData.h"
#import "ZSTUtils.h"
#import "TTTableSubtitleItem.h"

//static const CGFloat kShadowHeight    = 24;
//static const CGFloat kClearButtonSize = 38;
//
//static const CGFloat kPaddingX        = 8;

@implementation ZSTSendALimitedPickerTextField

@synthesize superviewForSearchResultsTableView;

@synthesize maxLineOfShow;

@synthesize states = _state;

- (id)initWithFrame:(CGRect)frame {
	self = [super initWithFrame:frame];
    if (self) {
        _state = kEmpty;
    }
    
    return self;
}

- (BOOL)hasText {
    BOOL b = self.text.length && _state != kEmpty && _state != kSelected;
    return b;
}

- (void)setSelectedCell:(TTPickerViewCell*)cell {
    
    if (self.selectedCell) {
        self.selectedCell.selected = NO;
    }
    
    [self setValue:cell forKey:@"_selectedCell"];
    
    //self.selectedCell = cell;
    
    if (self.selectedCell) {
        self.selectedCell.selected = YES;
        _state = kSelected;
        
    } else if (self.cells.count) {
        _state = kEmpty;
    }
}

- (void)addCellWithObject:(id)object {
    TTPickerViewCell* cell = [[[TTPickerViewCell alloc] init] autorelease];
    
    NSString* label = [self performSelector:@selector(labelForObject:) withObject:object];
    
    cell.object = object;
    cell.label = label;
    cell.font = self.font;
    
    if (self.cellViews != nil) 
    {
        for (TTPickerViewCell *pickerCell in self.cellViews)
        {
            
            if ([pickerCell.object isKindOfClass:[TTTableSubtitleItem class]] && [object isKindOfClass:[TTTableSubtitleItem class]])
            {

                TTTableSubtitleItem *pickerCell_object = pickerCell.object;
                TTTableSubtitleItem *cell_object = object;
                if ([pickerCell_object.subtitle isEqualToString:cell_object.subtitle]) {
                    // Reset text so the cursor moves to be at the end of the cellViews
                    self.text = @" ";
                    self.states = kEmpty;
                    return;
                }
            }else{
                NSString *objectString = [NSString stringWithFormat:@"%@", object];
                NSString *pickerCell_objectString = [NSString stringWithFormat:@"%@", pickerCell.object];
                if ([pickerCell_objectString isEqualToString:objectString]) 
                {
                    // Reset text so the cursor moves to be at the end of the cellViews
                    self.text = @" ";
                    self.states = kEmpty;
                    return;
                }
            }
        }
    }
    [(NSMutableArray *)self.cellViews addObject:cell];
    [self addSubview:cell];
    
    // Reset text so the cursor moves to be at the end of the cellViews
    self.text = @" ";
    self.states = kEmpty;

}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)removeSelectedCell {
    if (self.selectedCell) {
        [self removeCellWithObject:self.selectedCell.object];
        self.selectedCell = nil;
        
        if (self.cellViews.count) {
            _state = kEmpty;
            
        } else {
            
            self.text = @" ";
            _state = kEmpty;
        }
    }
}
- (void)removeAllCells {
    while (self.cellViews.count) {
        TTPickerViewCell* cell = [self.cellViews objectAtIndex:0];
        [cell removeFromSuperview];
        [(NSMutableArray *)self.cellViews removeObjectAtIndex:0];
    }
    
    self.selectedCell = nil;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    if (self.dataSource && _state == kSelected) {
        // Hide the cursor while a cell is selected
        return CGRectMake(-10, 0, 0, 0);
        
    } else {
        
        return [super textRectForBounds:bounds];
    }
}
- (UIView*)superviewForSearchResults {
    if (superviewForSearchResultsTableView) {
        
        return superviewForSearchResultsTableView;
    }
    
    return [super superviewForSearchResults];}

- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event {
    
    if (self.dataSource) {
        UITouch* touch = [touches anyObject];
        if (touch.view == self) {
            self.selectedCell = nil;
            
        } else {
            if ([touch.view isKindOfClass:[TTPickerViewCell class]]) {
                self.selectedCell = (TTPickerViewCell*)touch.view;
                [self becomeFirstResponder];
            }
        }
    }
}


- (CGRect)rectForSearchResults:(BOOL)withKeyboard {
    
    UIScrollView* scrollView = (UIScrollView*)[self ancestorOrSelfWithClass:[UIScrollView class]];
    UIView* superview = self.superviewForSearchResults;
    CGFloat y = superview.screenY;
    CGFloat visibleHeight = [self heightWithLines:1];
    CGFloat keyboardHeight = withKeyboard ? TKKeyboardHeight() : 0;
    
    CGFloat tableHeight = TKScreenBounds().size.height - (y + visibleHeight + keyboardHeight);
    if (scrollView) {
        return CGRectMake(0, scrollView.bottom -1, 320, tableHeight+1);
    }else{
        return CGRectMake(0, visibleHeight -1, 320, tableHeight+1);
        
    }
    
}
///////////////////////////////////////////////////////////////////////////////////////////////////

- (void)doneAction {
    
    [self autoDetectPhoneNumber];
    
    if (self.searchesAutomatically) {
        [self resignFirstResponder];
        
        if (self.dataSource) {
            self.text = @" ";
            self.states = kEmpty;
        }
    }
    
    self.searchesAutomatically = YES;
}

-(void)autoDetectPhoneNumber
{
    NSString *str = [self.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([str length] != 0 && str!= nil) {
        
        ABContact *contact = nil;
        if ((contact = [ContactData byPhoneNumberlToGetContact:str withLabel:NSLocalizedString(@"未知", @"")]) != nil) {
            TTTableSubtitleItem *item = [TTTableSubtitleItem itemWithText:contact.contactName subtitle:str];
            [self addCellWithObject:item];
        }
        else if ((contact = [ContactData byNameToGetContact:str]) != nil)
        {
            NSArray *array = [ContactData getPhoneNumberAndPhoneLabelArray:contact];
            NSDictionary *dic = [array objectAtIndex:0];
            TTTableSubtitleItem *item = [TTTableSubtitleItem itemWithText:[ContactData getPhoneNumberFromDic:dic] subtitle:str];
            [self addCellWithObject:item];
        }
        else
        {
            if ([str isNumber]) {
                TTTableSubtitleItem *item = [TTTableSubtitleItem itemWithText:str subtitle:str];
                [self addCellWithObject:item];
            }
            else
            {
                self.text = @" ";
                self.states = kEmpty;
            }
        }
    }
}

- (void)didBeginEditing {
//    父类没有didBeginEditing这个方法，并不会执行以下代码
//    if ([super respondsToSelector:@selector(didBeginEditing)]) {
//        [super didBeginEditing];
//    }
    
    if (self.dataSource) {
        UIScrollView* scrollView = (UIScrollView*)[self ancestorOrSelfWithClass:[UIScrollView class]];
        scrollView.scrollEnabled = YES;
        scrollView.scrollsToTop = NO;
    
        // Reset text so the cursor moves to be at the end of the cellViews
        self.text = @" ";
        self.states = kEmpty;
      
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////

- (void)dealloc
{
    self.superviewForSearchResultsTableView = nil;
    [super dealloc];
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    if (TKIsPad()) {
        UIImage *shadowImage = [[UIImage imageNamed:@"t_separator_black.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:0];
        [shadowImage drawInRect:CGRectMake(0, rect.size.height - 1, rect.size.width, 1)];
    }
}

@end
