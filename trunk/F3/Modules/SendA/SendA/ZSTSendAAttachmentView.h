//
//  AttachmentView.h
//  F3
//
//  Created by xuhuijun on 11-12-14.
//  Copyright 2011年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZSTSendAAttachmentView;
@protocol ZSTAttachmentViewDelegate <NSObject>

@optional

-(void)attachmentViewWillDisappear:(ZSTSendAAttachmentView *)attachmentView;

@end

@interface ZSTSendAAttachmentView : UIView 

{
    UIImageView *_attachmentBackgroundView;
    UIButton *_imageView;
    
    UILabel *_imageNameLabel;
//    UILabel *_count;
    
    UIButton *_deleteBtn;

    NSString *_filePath;

    id<ZSTAttachmentViewDelegate> _delegate;

}
@property (nonatomic ,retain) UIImageView *attachmentBackgroundView;
@property (nonatomic ,retain) UIButton *imageView;
@property (nonatomic ,retain) UILabel *imageNameLabel;
@property (nonatomic ,retain) UILabel *count;
@property (nonatomic ,retain) UIButton *deleteBtn;
@property (nonatomic ,retain) NSString *filePath;
@property (nonatomic ,assign) id<ZSTAttachmentViewDelegate> delegate;




@end
