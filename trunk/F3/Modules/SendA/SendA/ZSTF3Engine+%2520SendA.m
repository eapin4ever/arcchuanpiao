//
//  ZSTF3Engine+News.m
//  News
//
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTF3Engine+SendA.h"
#import "ZSTJSONCommunicator.h"
#import "ZSTF3Preferences.h"
#import "SDWebFileManager.h"
#import "SDFileCache.h"
#import "ZSTResponse.h"
#import "ZipArchive.h"
#import "ZSTUtils.h"
#import "JSON.h"
#import "ElementParser.h"
#import "ZSTLogUtil.h"
#import "ZSTPushBMessageDao.h"
#import "ZSTUtils.h"
#import "ZipArchive.h"
#import "ZSTPushBSendF3Operation.h"
#import "NSData+Base64.h"

static NSOperationQueue * sendQueue;

static NSDate *lastPollingTime;

@interface ZSTF3Engine(Pusha_private)

/**
 *	@brief	恢复下载状态为"发送中"的信息
 */
+ (void)resumeUploadMessage;

+ (void)reportState:(ZSTPushBMessageInfo *)nmsInfo;

+ (BOOL)cacheFile:(NSString *)pushId;

+ (BOOL)changeHtml:(NSURL *)htmlUrl replaceUrlString:(NSString *)replaceUrlString fileName:(NSString *)name;

+ (NSMutableDictionary *)paramsFromElement:(NSArray *)elements;


/**
 *	@brief       下载文件
 *
 *	@param key   根据iconkey
 *
 *	@returns     下载是否成功
 */
//+ (BOOL) downloadFile:(NSString *)key;


@end

@implementation ZSTF3Engine (SendA)

+(void)initialize
{
    if (sendQueue == nil) {
        sendQueue = [[NSOperationQueue alloc] init];
        sendQueue.maxConcurrentOperationCount = 1;
    }
}

//恢复下载状态为"发送中"的信息
+ (void)resumeUploadMessage
{
    NSArray *messages = [[ZSTPushBMessageDao shareMessageDao] getMessagesOfState:NMSStateSending];
    for (ZSTPushBMessageInfo *message in messages) {
        [self sendF3:message alertWhenFinish:NO];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (void)reportState:(ZSTPushBMessageInfo *)nmsInfo
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSString * content = [NSString stringWithFormat:@"<Log>[%@][VIEW][Iphone][%@][%@][%@][%d]</Log>",
                          [ZSTUtils formatDate:[NSDate date] format:@"yyyy-MM-dd"],[ZSTF3Preferences shared].loginMsisdn, nmsInfo.MSGID, nmsInfo.pushId, nmsInfo.state];
    BOOL bSuccess = [self submitClientLog:content];
    if (!bSuccess) {
        [ZSTLogUtil logUserView:[NSString stringWithFormat:@"%d", nmsInfo.state==NMSStateNotRead?1:2] pushId:nmsInfo.pushId messageId:nmsInfo.MSGID];
    }
    [pool release];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (BOOL)changeHtml:(NSURL *)htmlUrl replaceUrlString:(NSString *)replaceUrlString fileName:(NSString *)name
{
    NSString *htmlData = [NSString stringWithContentsOfURL:htmlUrl encoding:NSUTF8StringEncoding error:nil];
    
    NSURL *replaceUrl = [NSURL URLWithString:replaceUrlString];
    
    if (![replaceUrl isFileURL]) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString * diskCachePath = SDWIReturnRetained([[paths objectAtIndex:0] stringByAppendingPathComponent:@"ImageCache"]);
        
        NSString *fileLocalPath = [diskCachePath stringByAppendingPathComponent:name];
        
        SDWebFileManager *manager = [SDWebFileManager sharedManager];
        SDFileCache *fileCache = [SDFileCache sharedFileCache];
        NSData *fileData = [fileCache fileFromKey:replaceUrlString withFilePath:fileLocalPath];
        
        if ( !fileData) {
            [manager downloadWithURL:[NSURL URLWithString:replaceUrlString] delegate:(id<SDWebFileManagerDelegate>)self withFilePath:fileLocalPath];
        }
        
        htmlData =  [htmlData stringByReplacingOccurrencesOfString:replaceUrlString withString:fileLocalPath];
        
        [[NSFileManager defaultManager] removeItemAtURL:htmlUrl error:nil];
        [htmlData writeToURL:htmlUrl atomically:YES encoding:NSUTF8StringEncoding error:nil];
        
        SDWIRelease(diskCachePath);
    }
    return YES;
    
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (NSMutableDictionary *)paramsFromElement:(NSArray *)elements
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    for (Element *e in elements) {
        NSString *key = [[e attribute:@"Name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (key != nil && [key length] != 0) {
            NSString *value = [[e attribute:@"Value"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            [params setObject:value forKey:key];
        }
    }
    return params;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (int)saveF3Message:(ZSTPushBMessageInfo *)messageInfo error:(NSError **)error
{
    int ID = [[ZSTPushBMessageDao shareMessageDao] insertMessage:messageInfo];
    if (ID > 0 && messageInfo.ID <= 0) {
        NSArray *attachmentFileNames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[ZSTUtils pathForTempFinderOfOutbox] error: nil];
        if ([attachmentFileNames count]) {
            NSError *theError = nil;
            BOOL bSuccess = [[NSFileManager defaultManager] moveItemAtPath:[ZSTUtils pathForTempFinderOfOutbox] toPath:[ZSTUtils pathForSendFileOfOutbox:ID] error:&theError];
            if (!bSuccess) {
                TKDERROR(@"Copy file:'%@' toPath '%@' failed.\n%@", [ZSTUtils pathForTempFinderOfOutbox], [ZSTUtils pathForSendFileOfOutbox:ID], theError);
                if (error) {
                    *error = theError;
                }
                return -1;
            }
        }
    } else if (ID <= 0){
        
        return -1;
    }
    return ID;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
+ (void)sendF3:(ZSTPushBMessageInfo *)messageInfo alertWhenFinish:(BOOL)flag
{
    ZSTPushBSendF3Operation *sender = [[ZSTPushBSendF3Operation alloc] initWithF3Message:messageInfo];
    sender.alertWhenFinish = flag;
    [sendQueue addOperation:sender];
    [sender release];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (BOOL)getUploadFileInfo:(unsigned long long)fileSize fileId:(NSString **)fileId  location:(unsigned long long *)location
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECECCID"];
    [params setSafeObject:*fileId forKey:@"fileId"];
    [params setSafeObject:[NSNumber numberWithInt:fileSize] forKey:@"FileSize"];
    
    ZSTResponse *response = [[ZSTCommunicator shared] openSynAPIGetToMethod:[ZSTUtils httpSever:GETUPLOADFILEINFO_PATH] params:params timeOutSeconds:30];
    DocumentRoot * element = response.xmlResponse;
    if (response.error) {
        [TKUIUtil alertInWindow:@"信息发送失败" withImage:[UIImage imageNamed:@"icon_warning.png"]];
        return NO;
    }
    *fileId = CONTENTSTEXT("Response FileId");
    *location = CONTENTSUNSIGNEDLONGLONG("Response Location");
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (BOOL)uploadFileBreakPoint:(NSString *)fileId data:(NSData *)data location:(unsigned long long)location progressDelegate:(id)progress
{
    NSData *newData = [data subdataWithRange:NSMakeRange(location, [data length]-location)];//断点续传内容获取
    
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSString *url = [ZSTUtils httpSever:UPLOADFILEBREAKPOINT_PATH];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:fileId forKey:@"FileId"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    ZSTResponse *response = [[ZSTCommunicator shared] uploadFileSynToMethod:url data:newData params:params progressDelegate:progress];
    if (response.error) {
        return NO;
    }
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (NSString *)sendF3:(NSString *)user
             forward:(NSString *)forwardId
             subject:(NSString *)subject
             content:(NSString *)content
              fileId:(NSString *)fileId
         attachments:(NSArray *)attachments
              report:(BOOL) report
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECECCID"];
    [params setSafeObject:user forKey:@"ToUserID"];
    [params setSafeObject:subject forKey:@"Subject"];
    [params setSafeObject:(report ? @"1": @"0") forKey:@"Report"];
    [params setSafeObject:content forKey:@"Content"];
    [params setSafeObject:fileId forKey:@"AttachmentId"];
    [params setSafeObject:forwardId?forwardId:@"" forKey:@"OriMSGID"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    NSMutableString *attachmentString = [NSMutableString string];
    for (NSString *name in attachments) {
        [attachmentString appendFormat:@"<Attachment Name=\"%@\"/>\n",name];
    }
    [params setSafeObject:attachmentString forKey:@"Attachments"];
    
    ZSTResponse *response = [[ZSTCommunicator shared] openSynAPIPostToMethod:[ZSTUtils httpSever:SENDNMS_URL_PATH] replacedParams:params];
    if (response.error) {
        //        [[MessageDao shareMessageDao] setOutMessage:ID MSGID:@"" withState:NMSStateSentFailed];
        //        [TKUIUtil alertInWindow:@"信息发送失败" withImage:[UIImage imageNamed:@"icon_warning.png"]];
        return nil;
    }
    
    DocumentRoot *element = response.xmlResponse;
    NSString *MSGID = CONTENTSTEXT("Response MSGID");
    if ([MSGID isEmptyOrWhitespace]) {
        return nil;
    }
    return MSGID;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)requestDidFail:(ZSTResponse *)response
{
    if ([self isValidDelegateForSelector:@selector(requestDidFail:)]) {
        [self.delegate requestDidFail:response];
    }
}

+ (NSString *) uploadFile:(NSData *)data cmdId:(int)cmdID extension:(NSString *)extension
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    
    NSString * CmdIDString= [NSString stringWithFormat:@"%d", cmdID];
	NSString * FileDataString = [data base64Encoding];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECECCID"];
    [params setSafeObject:FileDataString forKey:@"FileData"];
    [params setSafeObject:extension forKey:@"FileExtension"];
    [params setSafeObject:CmdIDString forKey:@"CmdID"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    ZSTResponse *response = [[ZSTCommunicator shared] openSynAPIPostToMethod:[ZSTUtils httpSever:UPLOADFILE_URL_PATH]
                                                              replacedParams:params];
    
	
    DocumentRoot * element = response.xmlResponse;
    
    int Result = CONTENTSINT("Response Result");
    if (Result == REQUEST_CODE_OK) {
        return CONTENTSTEXT("Response FileID");
    }
	return nil;
}

+ (void) deleteMessage:(ZSTPushBMessageInfo *) nmsInfo
{
    // 从文件系统中删除
    NSString *nmsPath = [[ZSTUtils pathForInbox] stringByAppendingPathComponent: [NSString stringWithFormat: @"%@", nmsInfo.pushId]];
    [[NSFileManager defaultManager] removeItemAtPath: nmsPath error: nil];
    
    NSString *attachmentPath = [ZSTUtils pathForSendFileOfOutbox:nmsInfo.ID];
    [[NSFileManager defaultManager] removeItemAtPath: attachmentPath error: nil];
    // 从数据库中删除
    [[ZSTPushBMessageDao shareMessageDao] deleteMessage:nmsInfo.ID];
    
}

+ (void) deleteAllMessage
{
    NSArray *nmss = [[ZSTPushBMessageDao shareMessageDao] getMessages];
    for (ZSTPushBMessageInfo *nmsInfo in nmss) {
        if (!nmsInfo.isLocked) {
            // 从文件系统中删除
            NSString *nmsPath = [[ZSTUtils pathForInbox] stringByAppendingPathComponent: [NSString stringWithFormat: @"%@", nmsInfo.pushId]];
            [[NSFileManager defaultManager] removeItemAtPath: nmsPath error: nil];
            
            NSString *attachmentPath = [ZSTUtils pathForSendFileOfOutbox:nmsInfo.ID];
            [[NSFileManager defaultManager] removeItemAtPath: attachmentPath error: nil];
            
        }
    }
    // 从数据库中删除
    [[ZSTPushBMessageDao shareMessageDao] deleteAllMessage];
}

+ (void) deleteMessagesOfType:(NSString *)typeID
{
    NSArray *nmss = [[ZSTPushBMessageDao shareMessageDao] getInMessagesOfType:typeID];
    for (ZSTPushBMessageInfo *nmsInfo in nmss) {
        
        if (!nmsInfo.isLocked) {
            // 从文件系统中删除
            NSString *nmsPath = [[ZSTUtils pathForInbox] stringByAppendingPathComponent: [NSString stringWithFormat: @"%@", nmsInfo.pushId]];
            
            [[NSFileManager defaultManager] removeItemAtPath: nmsPath error: nil];
            
            NSString *attachmentPath = [ZSTUtils pathForSendFileOfOutbox:nmsInfo.ID];
            [[NSFileManager defaultManager] removeItemAtPath: attachmentPath error: nil];
        }
    }
    [[ZSTPushBMessageDao shareMessageDao] deleteMessagesOfType:typeID];
}

+ (void) deleteExpiredMessage
{
    //这里得到过期短信，在程序内部得到，现在得时间得前15天保留
    NSTimeInterval  interval = 24*60*60*15;
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-interval]; //前15天的日期
    NSArray *nmss = [[ZSTPushBMessageDao shareMessageDao] getMessagesBeforeDate:date];
    for (ZSTPushBMessageInfo *nmsInfo in nmss) {
        
        // 从文件系统中删除
        NSString *nmsPath = [[ZSTUtils pathForInbox] stringByAppendingPathComponent: [NSString stringWithFormat: @"%@", nmsInfo.pushId]];
        [[NSFileManager defaultManager] removeItemAtPath: nmsPath error: nil];
        
        NSString *attachmentPath = [ZSTUtils pathForSendFileOfOutbox:nmsInfo.ID];
        [[NSFileManager defaultManager] removeItemAtPath: attachmentPath error: nil];
        
    }
    
    [[ZSTPushBMessageDao shareMessageDao] deleteMessageBeforeDate:date];
    [TKUIUtil alertInWindow:@"清理完成" withImage:[UIImage imageNamed:@"icon_smile_face.png"]];
}

@end
