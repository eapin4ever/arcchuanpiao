//
//  ZSTGlobal+News.h
//  News
//
//  Created by luobin on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#define postNotificationOnMainThreadNoWait(notificationName) \
[[NSNotificationCenter defaultCenter] performSelectorOnMainThread:@selector(postNotificationName:object:) withObject:(notificationName) waitUntilDone:NO]


#define SendABaseURL @"http://mod.pmit.cn/senda"//最新接口



#define NotificationName_MessageListUpdated  @"MessageListUpdated"

#define NotificationName_MessageSendSucessed @"MessageSendSucessed"

#define NotificationName_MessageSendFaild @"MessageSendFaild"

#define NotificationName_Progress @"Progress"



#define GETUPLOADFILEINFO SendABaseURL @"/GetUploadFileInfo"

#define SENDNMS_URL_PATH SendABaseURL @"/SendF3"

#define SUBMITSYSTEMLOG_URL_PATH SendABaseURL @"/SubmitSystemLog"

#define UPDATECLIENTTOKEN_URL_PATH SendABaseURL @"/UpdateClientToken"

#define GETUPLOADFILEINFO_PATH SendABaseURL @"/GetUploadFileInfo"

#define UPLOADFILEBREAKPOINT_PATH SendABaseURL @"/UploadFileBreakPoint"

#define GETFILEINFO_PATH SendABaseURL @"/GetFile"



