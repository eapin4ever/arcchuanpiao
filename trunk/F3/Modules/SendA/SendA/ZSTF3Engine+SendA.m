//
//  ZSTF3Engine+News.m
//  News
//
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTF3Engine+SendA.h"
#import "ZSTCommunicator.h"
#import "ZSTF3Preferences.h"
#import "SDWebFileManager.h"
#import "SDFileCache.h"
#import "ZSTLegacyResponse.h"
#import "ZipArchive.h"
#import "ZSTDao+SendA.h"
#import "ZSTUtils.h"
#import "JSON.h"
#import "ElementParser.h"
#import "ZSTLogUtil.h"
#import "ZSTUtils.h"
#import "ZipArchive.h"
#import "ZSTSendASendF3Operation.h"

static NSOperationQueue * sendQueue;

@interface ZSTF3Engine(Pusha_private)

+ (BOOL)cacheFile:(NSString *)pushId;

+ (BOOL)changeHtml:(NSURL *)htmlUrl replaceUrlString:(NSString *)replaceUrlString fileName:(NSString *)name;

+ (NSMutableDictionary *)paramsFromElement:(NSArray *)elements;


/**
 *	@brief       下载文件
 *
 *	@param key   根据iconkey
 *
 *	@returns     下载是否成功
 */
//+ (BOOL) downloadFile:(NSString *)key;


@end

@implementation ZSTF3Engine (SendA)

+(void)load
{
    if (sendQueue == nil) {
        sendQueue = [[NSOperationQueue alloc] init];
        sendQueue.maxConcurrentOperationCount = 1;
    }
}

//恢复下载状态为"发送中"的信息
- (void)sendA_ResumeSendMessage
{
    NSArray *messages = [self.dao sendA_GetMessagesOfState:NMSStateSending];
    for (ZSTSendAMessageInfo *message in messages) {
        [self sendA_SendF3:message alertWhenFinish:NO];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (BOOL)changeHtml:(NSURL *)htmlUrl replaceUrlString:(NSString *)replaceUrlString fileName:(NSString *)name
{
    NSString *htmlData = [NSString stringWithContentsOfURL:htmlUrl encoding:NSUTF8StringEncoding error:nil];
    
    NSURL *replaceUrl = [NSURL URLWithString:replaceUrlString];
    
    if (![replaceUrl isFileURL]) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString * diskCachePath = SDWIReturnRetained([[paths objectAtIndex:0] stringByAppendingPathComponent:@"ImageCache"]);
        
        NSString *fileLocalPath = [diskCachePath stringByAppendingPathComponent:name];
        
        SDWebFileManager *manager = [SDWebFileManager sharedManager];
        SDFileCache *fileCache = [SDFileCache sharedFileCache];
        NSData *fileData = [fileCache fileFromKey:replaceUrlString withFilePath:fileLocalPath];
        
        if ( !fileData) {
            [manager downloadWithURL:[NSURL URLWithString:replaceUrlString] delegate:(id<SDWebFileManagerDelegate>)self withFilePath:fileLocalPath];
        }
        
        htmlData =  [htmlData stringByReplacingOccurrencesOfString:replaceUrlString withString:fileLocalPath];
        
        [[NSFileManager defaultManager] removeItemAtURL:htmlUrl error:nil];
        [htmlData writeToURL:htmlUrl atomically:YES encoding:NSUTF8StringEncoding error:nil];
        
        SDWIRelease(diskCachePath);
    }
    return YES;
    
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSMutableDictionary *)paramsFromElement:(NSArray *)elements
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    for (Element *e in elements) {
        NSString *key = [[e attribute:@"Name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (key != nil && [key length] != 0) {
            NSString *value = [[e attribute:@"Value"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            [params setObject:value forKey:key];
        }
    }
    return params;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (NSInteger)sendA_SaveF3Message:(ZSTSendAMessageInfo *)messageInfo error:(NSError **)error
{
    NSInteger ID = [self.dao sendA_InsertMessage:messageInfo];
    if (ID > 0 && messageInfo.ID <= 0) {
        NSArray *attachmentFileNames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[ZSTUtils pathForTempFinderOfOutbox] error: nil];
        if ([attachmentFileNames count]) {
            NSError *theError = nil;
            BOOL bSuccess = [[NSFileManager defaultManager] moveItemAtPath:[ZSTUtils pathForTempFinderOfOutbox] toPath:[ZSTUtils pathForSendFileOfOutbox:ID] error:&theError];
            if (!bSuccess) {
                TKDERROR(@"Copy file:'%@' toPath '%@' failed.\n%@", [ZSTUtils pathForTempFinderOfOutbox], [ZSTUtils pathForSendFileOfOutbox:ID], theError);
                if (error) {
                    *error = theError;
                }
                return -1;
            }
        }
    } else if (ID <= 0){
        
        return -1;
    }
    return ID;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)sendA_SendF3:(ZSTSendAMessageInfo *)messageInfo alertWhenFinish:(BOOL)flag
{
    ZSTSendASendF3Operation *sender = [[ZSTSendASendF3Operation alloc] initWithF3Message:messageInfo];
    sender.engine = self;
    sender.alertWhenFinish = flag;
    [sendQueue addOperation:sender];
    [sender release];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL)sendA_GetUploadFileInfo:(unsigned long long)fileSize fileId:(NSString **)fileId  location:(unsigned long long *)location
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:*fileId forKey:@"fileId"];
    [params setSafeObject:[NSNumber numberWithInteger:fileSize] forKey:@"FileSize"];
    
    
    ZSTLegacyResponse *response = [[ZSTLegicyCommunicator shared] openSynAPIGetToMethod:GETUPLOADFILEINFO_PATH
                                                                                 params:params
                                                                         timeOutSeconds:30];
    DocumentRoot * element = response.xmlResponse;
    if (response.error) {
        [TKUIUtil alertInWindow:@"信息发送失败" withImage:[UIImage imageNamed:@"icon_warning.png"]];
        return NO;
    }
    *fileId = CONTENTSTEXT("Response FileId");
    *location = CONTENTSUNSIGNEDLONGLONG("Response Location");
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL)sendA_UploadFileBreakPoint:(NSString *)fileId data:(NSData *)data location:(unsigned long long)location progressDelegate:(id)progress
{
    NSData *newData = [data subdataWithRange:NSMakeRange(location, [data length]-location)];//断点续传内容获取
    
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSString *url = UPLOADFILEBREAKPOINT_PATH;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:fileId forKey:@"FileId"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    ZSTLegacyResponse *response = [[ZSTLegicyCommunicator shared] uploadFileSynToMethod:url data:newData params:params progressDelegate:progress];
    if (response.error) {
        return NO;
    }
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (NSString *)sendA_SendF3:(NSString *)user
             subject:(NSString *)subject
             content:(NSString *)content
              fileId:(NSString *)fileId
         attachments:(NSArray *)attachments
              report:(BOOL) report
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:preferences.imsi forKey:@"Guid"];
    [params setSafeObject:user forKey:@"ToUserID"];
    [params setSafeObject:subject forKey:@"Subject"];
    [params setSafeObject:(report ? @"1": @"0") forKey:@"Report"];
    [params setSafeObject:content forKey:@"Content"];
    [params setSafeObject:fileId forKey:@"AttachmentId"];
    [params setSafeObject:@"" forKey:@"OriMSGID"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    NSMutableString *attachmentString = [NSMutableString string];
    for (NSString *name in attachments) {
        [attachmentString appendFormat:@"<Attachment Name=\"%@\"/>\n",name];
    }
    [params setSafeObject:attachmentString forKey:@"Attachments"];
    
    ZSTLegacyResponse *response = [[ZSTLegicyCommunicator shared] openSynAPIPostToMethod:SENDNMS_URL_PATH
                                                                          replacedParams:params];
    if (response.error) {
        return nil;
    }
    
    DocumentRoot *element = response.xmlResponse;
    NSString *MSGID = CONTENTSTEXT("Response MSGID");
    return MSGID?MSGID:@"";
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)requestDidFail:(ZSTLegacyResponse *)response
{
    if ([self isValidDelegateForSelector:@selector(requestDidFail:)]) {
        [self.delegate requestDidFail:response];
    }
}

- (NSString *) sendA_UploadFile:(NSData *)data cmdId:(int)cmdID extension:(NSString *)extension
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    
    NSString * CmdIDString= [NSString stringWithFormat:@"%d", cmdID];
	NSString * FileDataString = [data base64Encoding];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:FileDataString forKey:@"FileData"];
    [params setSafeObject:extension forKey:@"FileExtension"];
    [params setSafeObject:CmdIDString forKey:@"CmdID"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    ZSTLegacyResponse *response = [[ZSTLegicyCommunicator shared] openSynAPIPostToMethod:SUBMITSYSTEMLOG_URL_PATH
                                                              replacedParams:params];
    
	
    DocumentRoot * element = response.xmlResponse;
    
    int Result = CONTENTSINT("Response Result");
    if (Result == 1) {
        return CONTENTSTEXT("Response FileID");
    }
	return nil;
}

- (void) sendA_DeleteMessage:(ZSTSendAMessageInfo *) nmsInfo
{
    NSString *attachmentPath = [ZSTUtils pathForSendFileOfOutbox:nmsInfo.ID];
    [[NSFileManager defaultManager] removeItemAtPath: attachmentPath error: nil];
    // 从数据库中删除
    [self.dao sendA_DeleteMessage:nmsInfo.ID];
}

- (void) sendA_DeleteAllMessage
{
    NSArray *nmss = [self.dao sendA_GetMessages];
    for (ZSTSendAMessageInfo *nmsInfo in nmss) {
        if (!nmsInfo.isLocked) {
            // 从文件系统中删除
            
            NSString *attachmentPath = [ZSTUtils pathForSendFileOfOutbox:nmsInfo.ID];
            [[NSFileManager defaultManager] removeItemAtPath: attachmentPath error: nil];
            
        }
    }
    // 从数据库中删除
    [self.dao sendA_DeleteAllMessage];
}

- (void) sendA_DeleteExpiredMessage
{
    //这里得到过期短信，在程序内部得到，现在得时间得前15天保留
    NSTimeInterval  interval = 24*60*60*15;
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-interval]; //前15天的日期
    NSArray *nmss = [self.dao sendA_GetMessagesBeforeDate:date];
    for (ZSTSendAMessageInfo *nmsInfo in nmss) {
        
        NSString *attachmentPath = [ZSTUtils pathForSendFileOfOutbox:nmsInfo.ID];
        [[NSFileManager defaultManager] removeItemAtPath: attachmentPath error: nil];
        
    }
    
    [self.dao sendA_DeleteMessageBeforeDate:date];
    [TKUIUtil alertInWindow:@"清理完成" withImage:[UIImage imageNamed:@"icon_smile_face.png"]];
}

@end
