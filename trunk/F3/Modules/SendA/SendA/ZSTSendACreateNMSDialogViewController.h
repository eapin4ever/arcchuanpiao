//
//  Create_Dialog.h
//  Net_Information
//
//  Created by huqinghe on 11-6-9.
//  Copyright 2011 Shinetechchina.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreAudio/CoreAudioTypes.h>

#import "EGOTextView.h"
#import "ContactPickerViewController.h"
#import "ZSTSendAAttachmentView.h"
#import "TTPickerTextFieldDelegate.h"

@class EGOTextView;
@class ZSTSendALimitedPickerTextField;
@class ZSTSendAAudioAverageView;
@class ZSTEditView;

@interface ZSTSendACreateNMSDialogViewController : UIViewController<UINavigationBarDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,EGOTextViewDelegate,TTPickerTextFieldDelegate, EGOTextViewDelegate,ContactViewControllerDelegate,UIScrollViewDelegate, UITextViewDelegate,UIGestureRecognizerDelegate,AVAudioRecorderDelegate, AVAudioPlayerDelegate,ZSTAttachmentViewDelegate,MPMediaPickerControllerDelegate>
{
	ZSTSendALimitedPickerTextField *_contactTextField;
    
    NSString *_pushId;
    NSString *_subject;
    
	UIButton *sed_button;
    UIButton * emoticonsCloseButton;
    UIButton *_contactButton;
    UIBarButtonItem *_saveNMSBarButton;
    
    UIView * emoticonsView;
    
    UIImageView *_copyImageView;
    UIImageView *_subjectBackgroundView;
    
    UIImage *_image;
    
    NSArray *_imagesArray;
    NSArray *_keysArray;
   
    UILabel *_subjectContent;
    
    BOOL _shouldSaveToDraft;
    BOOL _isDraftEdit;
    BOOL _keyboardIsShowing;
    
    UIImageView *_background1;

    NSString *_nmsID;
    UIPageControl *_pageControl;
    UIScrollView *emoticonsScrollView;
    UIScrollView *_contactScrollView;
    
    UITextField *_backgruodTextField;
    
    EGOTextView * textview;
    
    AVAudioRecorder *recorder;
	AVAudioSession *session;

    ZSTSendAAudioAverageView *meter;
	NSTimer *recordTimer;
    
    NSURL *videoURL;
    
    NSString *_filePath;
    
    NSMutableArray *_attachmentViewArray;
    NSMutableArray *_buttonArray;
    
    UIScrollView *_attachmentScrollView;
    
    NSArray *_initPhoneNumberArray;
    NSString *_initContent;
    NSArray *_initAttachment;
        
    ZSTEditView *_exitOrDraftView;
    ZSTEditView *_isSendItOnView;
}

@property(nonatomic ,retain) EGOTextView * textview;
@property(nonatomic ,retain) ZSTSendALimitedPickerTextField *contactTextField;
@property(nonatomic ,retain) UIImage * image;
@property(nonatomic ,retain) UIImageView *background1;
@property(nonatomic ,retain) NSString *pushId;
@property(nonatomic ,retain) NSString *subject;
@property(nonatomic ,assign) BOOL shouldSaveToDraft;
@property(nonatomic ,assign) BOOL isDraftEdit;

@property (retain) AVAudioSession *session;
@property (retain) AVAudioRecorder *recorder;

@property(nonatomic ,retain) NSString *nmsID;
@property(nonatomic ,retain) NSMutableArray *attachmentViewArray;


- (void) getforwardMessage:(NSString *) forwardMessageString;
- (void) getReplyUserNumber:(NSString *) userNumberString userName:(NSString *)name;
- (void) getOutContactNumber:(NSArray *) contactArray;

- (void)popViewControllerDirectly:(BOOL)removeTempFinder;

- (void) popViewController;
- (void) pushInNavigationViewController:(UIViewController *)controller;
@end
