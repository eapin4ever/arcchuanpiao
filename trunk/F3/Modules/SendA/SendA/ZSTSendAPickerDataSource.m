//
//  PickerDataSource.m
//  F3_UI
//
//  Created by  on 11-9-1.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import "ZSTSendAPickerDataSource.h"

#import "ZSTSendAStatesModel.h"
#import "ContactInfo.h"
#import "TTTableSubtitleItem.h"

@implementation ZSTSendAPickerDataSource

- (id)init {
    if (self = [super init]) {
         _states = [[ZSTSendAStatesModel alloc] initWithContacts:[ZSTSendAStatesModel theContacts]];
        [_states loadContacts];
        self.model = _states;
    }
    return self;
}

- (void)dealloc {
    TKRELEASE(_states);
    [super dealloc];

}

#pragma mark -
#pragma mark UITableViewDataSource methods

- (NSArray *)sectionIndexTitlesForTableView:(UITableView*)tableView {
    return [TTTableViewDataSource lettersForSectionsWithSearch:YES summary:NO];
}

#pragma mark -
#pragma mark TTTableViewDataSource methods

- (void)tableViewDidLoadModel:(UITableView*)tableView {
    self.items = [NSMutableArray array];
    self.sections = [NSMutableArray array];
    
    NSMutableDictionary *groups = [NSMutableDictionary dictionary];
    for (ContactInfo *contactInfo in _states.contacts) {
        NSString *text = nil;
        NSString *letter = nil;
        if (contactInfo.name == nil || [contactInfo.name length] == 0) {
            text = contactInfo.phoneNum;
            letter = @"#";
        } else {
            text = contactInfo.name;
            letter = [contactInfo.name substringToIndex:1];
        }
            
        NSMutableArray *section = [groups objectForKey:letter];
        if (!section) {
            section = [NSMutableArray array];
            [groups setObject:section forKey:letter];
        }
        
        TTTableSubtitleItem *item = [TTTableSubtitleItem itemWithText:text subtitle:contactInfo.phoneNum];
        [section addObject:item];
    }
    
    NSArray *letters = [groups.allKeys sortedArrayUsingSelector:@selector(caseInsensitiveCompare:)];
    for (NSString *letter in letters) {
        NSArray *items = [groups objectForKey:letter];
        [_sections addObject:letter];
        [_items addObject:items];
    }
}

- (void)search:(NSString*)text {
    [_states search:text];
}

@end
