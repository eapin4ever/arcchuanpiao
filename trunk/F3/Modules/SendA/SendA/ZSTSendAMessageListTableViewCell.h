//
//  CustomTableViewCell.h
//  TestTableView
//
//  Created by Xie Wei on 11-5-16.
//  Copyright 2011年 e-linkway.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZSTSendAMessageListViewController.h"

@interface ZSTSendAMessageListTableViewCell : UITableViewCell 
{
    TKAsynImageView *_leftImageView;
    UIImageView *_lockLabel;
    UIImageView *_transceivingLabel;

    UILabel *_progressLabel;
    UILabel *_percentLabel;
    UILabel *_labelDate;
	UILabel *_labelName;
	UILabel *_labelIsRead;
    UILabel *_labelTitle;
	UILabel *_labelIsSend;
    UILabel *_separatorLabel;
    
    int _replaceDistance;
    
    UIView *deleteButtonView;
    
    BOOL _editStateMark;
    BOOL _isLock;
    BOOL _inEditing;
    
    NSMutableDictionary *_staticImageDictionary;
}

-(void) setNMSInfo: (ZSTSendAMessageInfo *) nmsInfo isLock:(BOOL)isLock editStateMark:(BOOL)editStateMark;

@property (nonatomic, readonly) TKAsynImageView *leftImageView;

@property (nonatomic, retain)   UILabel *progressLabel;
@property (nonatomic, retain)   UILabel *percentLabel;
@property (nonatomic, readonly) UILabel *labelDate;
@property (nonatomic, readonly) UILabel *labelName;
@property (nonatomic, readonly) UILabel *labelIsRead;
@property (nonatomic, readonly) UILabel *labelTitle;
@property (nonatomic, readonly) UILabel *labelIsSend;
@property (nonatomic, readonly) UILabel *separatorLabel;
@property (nonatomic, readonly) UIImageView *transceivingLabel;

@property (nonatomic, retain)   UIImageView *lockLabel;
@property (nonatomic, assign)   BOOL editStateMark;
@property (nonatomic, assign)   BOOL isLock;
@property (nonatomic, assign)   BOOL inEditing;
@end
