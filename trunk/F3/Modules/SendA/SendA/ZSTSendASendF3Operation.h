//
//  ZSTSendF3Operation.h
//  F3Engine
//
//  Created by luobin on 4/23/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ZSTSendAMessageInfo;

@interface ZSTSendASendF3Operation : NSOperation

@property (nonatomic, readonly) ZSTSendAMessageInfo *messageInfo;

@property (nonatomic, retain) ZSTF3Engine *engine;

@property (nonatomic, assign) BOOL alertWhenFinish;

@property (nonatomic , retain, readonly) NSError *error;

- (id)initWithF3Message:(ZSTSendAMessageInfo *)messageInfo;

@end
