/*
 *  WeiboConst.h
 *  Weibo
 *
 *  Created by China on 10-1-7.
 *  Copyright 2010 sina. All rights reserved.
 *
 */
typedef	enum{
	TabViewProfile	= 0,
	TabViewFavs	= 1,
	TabViewMblogs	= 2,
	TabViewFollows = 3,
	TabViewFans = 4,
	TabViewTopics = 5,
} TabViewType;

/************** tags *****************/
#define TAG_VIP_IN_CELL 10001
#define TAG_AVATAR_IN_CELL 10002
#define TAG_AVATAR_MASK_IN_CELL 10003
#define TAG_NICK_IN_CELL 10004
#define TAG_LAST_WEIBO_IN_CELL 10005
#define TAG_BACK_IN_CELL 10006

/************** tags *****************/

//#define kAccelerationThreshold        1.8
#define kUpdateInterval               (1.0f/10.0f)
#define DEAL_RESULT_DELAY	1.5

#define DEFAULT_PAGE_SIZE		20	//DEFAULT_TWEET_COUNT

#define kReleaseToReloadStatus 0
#define kPullToReloadStatus 1
#define kLoadingStatus 2
#define PULL_LOAD_BOTTOM_MIN_MOVE 2.0

#define MESSAGES_PER_PAGE 20

//+ 信息流尺寸定义
#define CELL_CONTENT_WIDTH 260.0	//221.0	//信息流气泡文本宽度
#define CELL_AVATAR_WIDTH 45.0	//46.0  38
#define CELL_AVATAR_START_X 9
#define CELL_AVATAR_START_Y 9	//3
#define CELL_AVATAR_MASK_PADDING 2
#define CELL_CONTENT_LEFT 56	//60
#define CELL_AVATAR_VIP_FLAG_START_X CELL_AVATAR_START_X+CELL_AVATAR_WIDTH-CELL_AVATAR_VIP_FLAG_WIDTH/2
#define CELL_AVATAR_VIP_FLAG_START_Y CELL_AVATAR_START_Y+CELL_AVATAR_WIDTH-CELL_AVATAR_VIP_FLAG_WIDTH/2
#define CELL_AVATAR_VIP_FLAG_WIDTH 16
//+ 个人头像尺寸 (和信息流不一样大)
#define PERSON_AVATAR_WIDTH 48.0 //包含边框
#define PERSON_AVATAR_VIP_FLAG_WIDTH 12
//- paddings 
#define FRIENDS_CELL_AVATAR_PADDING 5
#define PROFILE_AVATAR_PADDING 10

//应用navigation title Tint Color
#define APP_NAV_TINT_COLOR [UIColor colorWithRed:0.671 green:0.694 blue:0.749 alpha:1]

#define	CELL_MULTICOL_HEIGHT	50


typedef enum _FeaturedPeopleListType {
	FeaturedPeopleListTypeStar = 1,
	FeaturedPeopleListTypePopular = 2, 
	FeaturedPeopleListTypeInterest = 3,
} FeaturedPeopleListType;

#define TIMELINE_PAGE_SIZE 10
