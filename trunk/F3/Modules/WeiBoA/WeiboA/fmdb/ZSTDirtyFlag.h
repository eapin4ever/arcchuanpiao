//
//  DirtyFlag.h
//  Weibo
//
//  Created by China on 10-3-4.
//  Copyright 2010 Apple Inc. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ZSTDirtyFlag : NSObject {
	int myBlogs,myFavs,myTopics;
	Boolean myBlogsDeleted;
}

@property (nonatomic) int myBlogs;
@property (nonatomic) int myFavs;
@property (nonatomic) int myTopics;
@property (nonatomic) Boolean myBlogsDeleted;

@end
