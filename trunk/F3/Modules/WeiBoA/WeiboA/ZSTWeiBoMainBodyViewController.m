
//
//  ZSTWeiBoMainBodyViewController.m
//  F3
//
//  Created by  on 12-6-14.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTWeiBoMainBodyViewController.h"
#import "ZSTMainBodyCell.h"
#import "ZSTWBToolBar.h"
#import "ZSTHHCommentController.h"
#import "ZSTUtils.h"
#import "util.h"
#import "ZSTActionSheetHint.h"
#import "ZSTWeiBoWebViewController.h"
#import "EGOPhotoSource.h"
#import "EGOPhotoViewController.h"
#import "TKUIUtil.h"

@implementation ZSTWeiBoMainBodyViewController

@synthesize weiBoInfo;
@synthesize commentList;
@synthesize forwardList;
@synthesize list;
@synthesize tableView = _tableView;
@synthesize progressSheet;
@synthesize fav;


- (NSMutableArray *)updateArray:(NSMutableArray *)targetArray source:(NSArray *)sourceArray 
{
    [targetArray removeAllObjects];
    [targetArray addObjectsFromArray:sourceArray];
    return targetArray;
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];

    _commentPage = 1;
    _forwardPage = 1;
    _loading = YES;
    _isForwardList = NO;
    _isCommentList = YES;
//    fav = NO;
    
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 480-20-44-44+(iPhone5?88:0))];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [self.view addSubview:_tableView];
    }
    
    if (_loadMoreView == nil) {
        _loadMoreView = [[TKLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
        _loadMoreView.delegate = self;
        if (_hasMore) {
            self.tableView.tableFooterView = _loadMoreView;
        }
    }
    //工具条
    
    if (_toolBar == nil) {
        _toolBar = [[ZSTWBToolBar alloc] initWithFrame: CGRectMake(0, 480-44-44-20+(iPhone5?88:0), 320, 44) haveFav:fav];
        _toolBar.delegate = self;
        [self.view addSubview:_toolBar];
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)updateTable
{
    [self.list removeAllObjects];
    if (_isCommentList) {
        [self.commentList removeAllObjects];
        [self.sinaWeiboEngine getCommentList:[NSString stringWithFormat:@"%lld",self.weiBoInfo.ID] sinceID:0 startingAtPage:1 count:0];
    }else{
        [self.forwardList removeAllObjects];
        [self.sinaWeiboEngine getForwardList:[NSString stringWithFormat:@"%lld",self.weiBoInfo.ID] sinceID:0 startingAtPage:1 count:0];
    }
}

- (void)viewWillAppear:(BOOL)animated
{    
    if (self.commentList == nil) {
        self.commentList = [NSMutableArray arrayWithCapacity:3];
    }
    
    if (self.forwardList == nil) {
        self.forwardList = [NSMutableArray arrayWithCapacity:3];
    }
    
    if (self.list == nil) {
        self.list = [NSMutableArray arrayWithCapacity:3];
    }
    [self updateTable];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)noList
{
	return !self.list || [self.list count] == 0;
}

- (NSString*)noMessagesString
{
	return NSLocalizedString(@"暂无数据...", @"");
}

- (NSString*)loadingMessagesString
{
    return NSLocalizedString(@"加载中...", @"");
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return 70;
    }else if (indexPath.section == 0 && indexPath.row == 1){
        
        if (_webHeight == 0) {
            CGSize size = [weiBoInfo.content sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(245, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
            CGSize attachmentSize = [weiBoInfo.boriginalcontent sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:CGSizeMake(225, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
            float height = 10 + 20 + 10 + (size.height ? size.height : 0) + (([weiBoInfo.thumbnail_pic length] != 0) ? 190 : 0) + (attachmentSize.height ? attachmentSize.height : 0) +  (weiBoInfo.bisforward ? 90 : 20) ;

            return height;
        }
        return _webHeight;
    }
    
    if (indexPath.section == 1) {
        if ([self noList]) {
            return 40;
        }else{
            ZSTWeiBoInfo *weiboinfo = [self.list objectAtIndex:indexPath.row];
            CGSize size = [weiboinfo.content sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:CGSizeMake(245, 600.0f) lineBreakMode:NSLineBreakByCharWrapping];
            float height = 10 + 20 + 5 + (size.height ? size.height + 5 : 0);
            return height;
        }
    }
    return 0;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return 45;
    }
    return 0;
}



- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        
        _sectionHeader = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 45)] autorelease];
        _sectionHeader.image = ZSTModuleImage(@"module_weiboa_chat_bg.png");
        _sectionHeader.userInteractionEnabled = YES;
        
        forwardCountBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        NSString *forward = NSLocalizedString(@"转发: ", nil);
        forwardCountBtn.selected = _isForwardList;

        [forwardCountBtn setTitle:[forward stringByAppendingString:self.weiBoInfo.forwardCount] forState:UIControlStateNormal];
        forwardCountBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [forwardCountBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [forwardCountBtn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];  
        [forwardCountBtn setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];  

        CGSize size = [forwardCountBtn.titleLabel.text sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(160, 45) lineBreakMode:UILineBreakModeTailTruncation];
        forwardCountBtn.frame = CGRectMake(10, 10.5, size.width, 20);
        [forwardCountBtn addTarget:self action:@selector(openForwardList:) forControlEvents:UIControlEventTouchUpInside];
        [_sectionHeader addSubview:forwardCountBtn];
        
        UILabel *seperate = [[UILabel alloc] initWithFrame:CGRectMake(5 + CGRectGetMaxX(forwardCountBtn.frame), 10.5, 2, 20)];
        seperate.text = @"|";
        seperate.font = [UIFont systemFontOfSize:25];
        seperate.backgroundColor = [UIColor clearColor];
        [_sectionHeader addSubview:seperate];
        
        commentCountBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        commentCountBtn.selected = _isCommentList;
        NSString *comment = NSLocalizedString(@"评论: ", nil);
        
        [commentCountBtn setTitle:[comment stringByAppendingString:self.weiBoInfo.commentCount] forState:UIControlStateNormal];
        commentCountBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [commentCountBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [commentCountBtn setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
        [commentCountBtn setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];  

        CGSize size1 = [commentCountBtn.titleLabel.text sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(160, 45) lineBreakMode:UILineBreakModeTailTruncation];
        commentCountBtn.frame = CGRectMake(5 + CGRectGetMaxX(seperate.frame) , 10.5, size1.width, 20);
        [commentCountBtn addTarget:self action:@selector(openCommentList:) forControlEvents:UIControlEventTouchUpInside];
        [_sectionHeader addSubview:commentCountBtn];
        [seperate release];
        
        return _sectionHeader;
        
    }
    return nil;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 2;
    }

    if ([self noList]) {
        return 1;
    }else{
        return [self.list count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {
        
        static NSString *CellIdentifier = @"MainBodyUserCell";
        
        ZSTMainBodyUserCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[ZSTMainBodyUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        [cell configCell:self.weiBoInfo];
        
        return cell;
    }else if (indexPath.section == 0 && indexPath.row == 1)
    {
        static NSString *CellIdentifier = @"MainBodyUserTextCell";
        
        ZSTMainBodyUserTextCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[[ZSTMainBodyUserTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
            [cell configCell:self.weiBoInfo];
        }
        return cell;
        
    }else if (indexPath.section == 1)
    {
        if ([self noList]) {
            static NSString *CellIdentifier = @"nomarlCell";

            UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            cell.textLabel.font = [UIFont systemFontOfSize:12];
            cell.textLabel.textColor = [UIColor grayColor];
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.textLabel.backgroundColor = [UIColor clearColor];
            cell.textLabel.frame = CGRectMake(0, 0, 320, 60);
            
            if(_errorDesc){
                cell.textLabel.text = NSLocalizedString(@"网络异常", @"") ;
                NSLog(@"_errorDesc = %@",_errorDesc);
            }else{
                cell.textLabel.text = _loading? [self loadingMessagesString]: [self noMessagesString];
            }
            return cell;
            
        }else{
            static NSString *CellIdentifier = @"MainBodyForwardAndCommentCell";

            ZSTMainBodyForwardAndCommentCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            if (cell == nil) {
                cell = [[[ZSTMainBodyForwardAndCommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            ZSTWeiBoInfo *wbinfo = [self.list objectAtIndex:indexPath.row];
            [cell configCell:wbinfo];
            return cell;
        }
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1) {
    
        if (_isForwardList) {
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"请选择需要的操作" , nil)
                                                                     delegate:self
                                                            cancelButtonTitle:NSLocalizedString(@"取消",@"取消")
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles:NSLocalizedString(@"转发" , nil), nil];
            [actionSheet showInView:self.view];
            [actionSheet release];
        }else if (_isCommentList)
        {     
            UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"请选择需要的操作" , nil)
                                                                     delegate:self
                                                            cancelButtonTitle:NSLocalizedString(@"取消",@"取消")
                                                       destructiveButtonTitle:nil
                                                            otherButtonTitles:NSLocalizedString(@"回复" , nil), nil];
            [actionSheet showInView:self.view];
            [actionSheet release];
        }
    }
}

#pragma mark - ZSTMainBodyUserTextCellDelegate

- (void)ZSTMainBodyUserTextCell:(ZSTMainBodyUserTextCell *)cell shouldAssignHeight:(CGFloat)newHeight {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    if (indexPath == nil) {
        return;
    }
    NSArray *indexArray = [NSArray arrayWithObject:indexPath];
    if (newHeight != 0 && newHeight != _webHeight && [indexArray count] != 0) {
        _webHeight = newHeight;
        [self.tableView beginUpdates];
        [self.tableView endUpdates];
    }
}
- (void)ZSTMainBodyUserTextCell:(ZSTMainBodyUserTextCell *)cell didSelectLink:(NSString *)url
{
    NSURLRequest *newRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    UIViewController *webViewCtrl = [[ZSTWeiBoWebViewController alloc] initWithRequest:newRequest];

    UINavigationController* navController = [[UINavigationController alloc] initWithRootViewController:webViewCtrl];
    [self.navigationController presentModalViewController:navController animated:YES];
    [navController release];
    [webViewCtrl release];
}

- (void)ZSTMainBodyUserTextCell:(ZSTMainBodyUserTextCell *)cell didSelectImageLink:(NSString *)url
{
    NSURL *imageUrl = [NSURL URLWithString:url];
    NSMutableArray *photos = [NSMutableArray array];
    
    EGOPhoto *photo = [[EGOPhoto alloc] initWithImageURL:imageUrl];
    [photos addObject:photo];
    [photo release];

    EGOPhotoSource *source = [[EGOPhotoSource alloc] initWithPhotos:photos];
    EGOPhotoViewController *photoController = [[EGOPhotoViewController alloc] initWithPhotoSource:source];
    [source release];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:photoController];
    [self.navigationController presentModalViewController:navController animated:YES];
    
    photoController.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"返回", @"") style:UIBarButtonItemStylePlain target:navController action:@selector(dismissModalViewController)] autorelease];
    
    [navController release];
    [photoController release];

 
}

#pragma mark - UIActionSheetDelegate-

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 100) return;

    NSIndexPath *index = [self.tableView indexPathForSelectedRow];

    ZSTWeiBoInfo *wbinfo = [self.list objectAtIndex:index.row];

	if(buttonIndex == 0){
        
        if (_isCommentList) {

            ZSTHHCommentController *c = [[ZSTHHCommentController alloc] init];
            c.delegate = self;
            c.ID = [NSString stringWithFormat:@"%lld",self.weiBoInfo.ID];
            c.CID = wbinfo.ID;
            c.isReply = YES;
            UINavigationController *n = [[UINavigationController alloc] initWithRootViewController:c];
            [self presentModalViewController:n animated:YES];
            [n release];
            [c release];
        }
	}else if(buttonIndex == 1)
    {
		[self dismissViewControllerAnimated:YES completion:nil];
    }
	
}
- (void)loadMoreData {
    
  
    if (_isCommentList) {
        [self.sinaWeiboEngine getCommentList:[NSString stringWithFormat:@"%lld",self.weiBoInfo.ID] sinceID:0 startingAtPage:_commentPage count:0];

    }else if (_isForwardList){
        [self.sinaWeiboEngine getForwardList:[NSString stringWithFormat:@"%lld",self.weiBoInfo.ID] sinceID:0 startingAtPage:_forwardPage count:0];
    }
    
}

- (void)appendData:(NSArray*)dataArray
{

    [self.list addObjectsFromArray:dataArray];

}

#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {

    if (!_isRefreshing) {
        [_loadMoreView loadMoreScrollViewDidEndDragging:scrollView];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
 
    if (!_isRefreshing) {
        [_loadMoreView loadMoreScrollViewDidEndDragging:scrollView];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {

    if (!_isRefreshing) {
        [_loadMoreView loadMoreScrollViewDidScroll:scrollView];
    }
}


#pragma mark HHLoadMoreViewDelegate Methods
- (void)loadMoreDidTriggerRefresh:(TKLoadMoreView*)view
{
    if (_hasMore) {
        _isLoadingMore = YES;
        [self loadMoreData];
    }
}

- (BOOL)loadMoreDataSourceIsLoading:(TKLoadMoreView*)view
{
    return _isLoadingMore;
}

- (void)finishLoading
{
    [_loadMoreView loadMoreScrollViewDataSourceDidFinishedLoading:self.tableView];
    
    _isRefreshing = NO;
    _isLoadingMore = NO;
}

- (void)fetchDataSuccess:(NSArray *)theData hasMore:(BOOL)hasMore
{
    _hasMore = hasMore;
    if (hasMore) {
        self.tableView.tableFooterView = _loadMoreView;
    } else {
        self.tableView.tableFooterView = nil;
    }
    
    [self appendData:theData];

    
    [self.tableView reloadData];
    
    [self finishLoading];
}

- (void)fetchDataFailed
{
    [self finishLoading];
}

#pragma mark - WBEngineDelegate -

- (void)closeAfterHaveResult {
	if(self.progressSheet)
	{
		[self.progressSheet dismissWithClickedButtonIndex:4 animated:YES];
		self.progressSheet = nil;
	}
    
    
    if (_toolBar != nil) {
        [_toolBar removeFromSuperview];
        [_toolBar release];
        _toolBar = [[ZSTWBToolBar alloc] initWithFrame: CGRectMake(0, 480-44-44-20+(iPhone5?88:0), 320, 44) haveFav:fav];
        _toolBar.delegate = self;
        [self.view addSubview:_toolBar];
    }
    
}

- (void)engine:(SinaWeibo *)engine requestDidSucceedWithResult:(id)result
{
    if ([[result objectForKey:@"comments"] count] == 0 && _isCommentList) {
        [self fetchDataSuccess:nil hasMore:NO];    
    }else if ([[result objectForKey:@"reposts"] count] == 0 && _isForwardList)  {
        [self fetchDataSuccess:nil hasMore:NO];    
    }
    
    [self finishLoading];  
}

- (void)engine:(SinaWeibo *)engine parsingSucceededForComments:(id)result
{
    BOOL hasMore = NO;
    
    if ([result count] != 0) {
        hasMore = YES;
        _commentPage += 1;
    }else{
        hasMore = NO;
    }
    if (_isCommentList) {
        [self fetchDataSuccess:result hasMore:hasMore];    
    }else{
        self.commentList = [result retain];
    }
}

- (void)engine:(SinaWeibo *)engine parsingSucceededForReports:(id)result
{
    BOOL hasMore = NO;
    if ([result count] != 0) {
        hasMore = YES;
        _forwardPage += 1;

    }else{
        hasMore = NO;
    }
    if (_isForwardList) {
        [self fetchDataSuccess:result hasMore:hasMore];
    }else{
        self.forwardList = [result retain];
    }
}
- (void)engine:(SinaWeibo *)engine requestDidFailWithError:(NSError *)error
{
    _loading = NO;
    _errorDesc = [[[error localizedDescription] capitalizedString] retain];
    
	if(self.progressSheet)
	{
		[self.progressSheet dismissWithClickedButtonIndex:4 animated:YES];
		self.progressSheet = nil;
	}
    [self finishLoading];
}

- (void)engine:(SinaWeibo *)engine parsingSucceededForFav:(id)result
{
     NSNumber * favorited = [result objectForKey:@"favorited"];
    if ([favorited intValue] == 0) {
        fav = NO;
    }else{
        fav = YES;
    }
    
    if(self.progressSheet && fav == YES)
	{   
        self.progressSheet.title = NSLocalizedString(@"收藏成功！", @"");			
    }else if (self.progressSheet && fav == NO){
        self.progressSheet.title = NSLocalizedString(@"取消收藏成功！", @"");			
    }
    hideActionSheetIndicator(self.progressSheet);
    [self performSelector:@selector(closeAfterHaveResult) withObject:nil afterDelay:1.5];
}

#pragma mark - selector


- (void)openForwardList:(UIButton *)sender
{
    self.commentList = [self updateArray:self.commentList source:self.list];
    self.list = [self updateArray:self.list source:self.forwardList];
    
    if ([self.forwardList count] == 0) {
        [self.sinaWeiboEngine getForwardList:[NSString stringWithFormat:@"%lld",self.weiBoInfo.ID] sinceID:0 startingAtPage:1 count:0];
        [self.tableView reloadData];

    }else{
        [self.tableView reloadData];
    }
    
    if ([self.forwardList count] < [self.weiBoInfo.forwardCount intValue] && [self.forwardList count] != 0) {
        _hasMore = YES;
        [self finishLoading];
        self.tableView.tableFooterView = _loadMoreView;
    }else{
        _hasMore = NO;
        self.tableView.tableFooterView = nil;
    }
    _isCommentList = NO;
    _isForwardList = YES;
}

- (void)openCommentList:(UIButton *)sender
{    
    self.forwardList = [self updateArray:self.forwardList source:self.list];
    self.list = [self updateArray:self.list source:self.commentList];
    
    if ([self.commentList count] == 0) {
        [self.sinaWeiboEngine getCommentList:[NSString stringWithFormat:@"%lld",self.weiBoInfo.ID] sinceID:0 startingAtPage:1 count:0];
    }else{
        [self.tableView reloadData];
    }
    
    if ([self.commentList count] < [self.weiBoInfo.commentCount intValue]) {
        _hasMore = YES;
        [self finishLoading];
        self.tableView.tableFooterView = _loadMoreView;
    }else{
        _hasMore = NO;
        self.tableView.tableFooterView = nil;
    }
    _isCommentList = YES;
    _isForwardList = NO;
    
    commentCountBtn.selected = YES;
    forwardCountBtn.selected = NO;
    [commentCountBtn setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
}

#pragma mark - ZSTWBToolBarDelegate -

- (void)toolBarRefresh:(ZSTWBToolBar *)toolBar
{
    [self updateTable];
}
- (void)toolBarCommentItemAction:(ZSTWBToolBar *)toolBar
{
    ZSTHHCommentController *c = [[ZSTHHCommentController alloc] init];
    c.delegate = self;
    c.ID = [NSString stringWithFormat:@"%lld",self.weiBoInfo.ID];
    c.isComment = YES;
    UINavigationController *n = [[UINavigationController alloc] initWithRootViewController:c];
    [self presentModalViewController:n animated:YES];
    [n release];
    [c release];
}
- (void)toolBarForwardItemAction:(ZSTWBToolBar *)toolBar
{
    ZSTHHCommentController *c = [[ZSTHHCommentController alloc] init];
    c.delegate = self;
    c.ID = [NSString stringWithFormat:@"%lld",self.weiBoInfo.ID];
    c.isForward = YES;
    if (self.weiBoInfo.attachmentInfo) {
        c.forwardString = [[NSString stringWithFormat:@"//@%@: ",self.weiBoInfo.name] stringByAppendingString:self.weiBoInfo.content];
    }
    UINavigationController *n = [[UINavigationController alloc] initWithRootViewController:c];
    [self presentModalViewController:n animated:YES];
    [n release];
    [c release];
}

- (void)toolBarFavItemAction:(ZSTWBToolBar *)toolBar
{
	self.progressSheet = [ZSTActionSheetHint hintWithTitle:NSLocalizedString(@"正在收藏微博...",@"") delegate:self shouldShowCancel:YES engine:self.weiBoEngine parentView:self.view];
    [self.sinaWeiboEngine markUpdate:[NSString stringWithFormat:@"%lld",self.weiBoInfo.ID] asFavorite:YES];
}
- (void)toolBarFavCancelItemAction:(ZSTWBToolBar *)toolBar
{
    self.progressSheet = [ZSTActionSheetHint hintWithTitle:NSLocalizedString(@"取消收藏微博...",@"") delegate:self shouldShowCancel:YES engine:self.weiBoEngine parentView:self.view];

    [self.sinaWeiboEngine markUpdate:[NSString stringWithFormat:@"%lld",self.weiBoInfo.ID] asFavorite:NO];

}

- (void)toolBarOtherItemAction:(ZSTWBToolBar *)toolBar{
	UIActionSheet *actionSheet;
    if ([UIDevice isIPhone]) {
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"请选择需要的操作",@"请选择需要的操作")
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"取消",@"取消")
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:NSLocalizedString(@"短信分享给好友",@"短信分享给好友"),NSLocalizedString(@"复制",@"复制"),nil];
        //todo多语言
    } else {
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"请选择需要的操作",@"")
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"取消",@"")
                                    destructiveButtonTitle:nil
                                         otherButtonTitles: NSLocalizedString(@"复制",@""),nil];
    }	
	actionSheet.tag = 100;
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	[actionSheet showInView:self.view];
	[actionSheet release];
	
	
}
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
	if (actionSheet.tag != 100) return;
	UIPasteboard *pasteboard;
	if ([UIDevice isIPhone]) {
		switch (buttonIndex) {
			case 0:
				;
				UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
				pasteboard.string = self.weiBoInfo.content;			
				
				UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"短信分享",@"")
                                                                message:NSLocalizedString(@"微博内容已复制，请在短信页面粘贴发送。",@"")
															   delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"确定",@"")
                                                      otherButtonTitles:NSLocalizedString(@"取消",@""), nil];
				alert.tag = 10101;
				[alert show];
				[alert release];
				
				break;
			case 1:
				;
				break;
			default:
				break;
		}		
	} else {
		switch (buttonIndex) {
			case 0:
                pasteboard = [UIPasteboard generalPasteboard];
				pasteboard.string = self.weiBoInfo.content;	
				break;
							
            default:
				break;				
		}
	}
}

#pragma mark - ZSTHHCommentControllerDelegate

- (void)commentControllerDidFinish:(ZSTHHCommentController *)commentController
{
//    if (commentController.isComment) {
//        if (_isCommentList) {
//            [self.list removeAllObjects];
//        }
//        [self.commentList removeAllObjects];
//        [self.weiBoEngine getCommentList:[NSString stringWithFormat:@"%lld",self.weiBoInfo.ID] sinceID:0 startingAtPage:1 count:0];
//    }else if(commentController.isForward){
//        if (_isForwardList) {
//            [self.list removeAllObjects];
//        }
//        [self.forwardList removeAllObjects];
//        [self.weiBoEngine getForwardList:[NSString stringWithFormat:@"%lld",self.weiBoInfo.ID] sinceID:0 startingAtPage:1 count:0];
//    }else{
//        [self updateTable];
//    }
 
}

- (void) dealloc
{
    TKRELEASE(_tableView);
//    TKRELEASE(weiBoEngine);
    TKRELEASE(weiBoInfo);
    TKRELEASE(list);
    TKRELEASE(commentList);
    TKRELEASE(forwardList);
    TKRELEASE(progressSheet);
    
    [_loadMoreView release]; _loadMoreView = nil;
    [_sectionHeader release];_sectionHeader = nil;
    
	[super dealloc];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


@end
