//
//  ZSTGroupChatController.h
//  YouYun
//
//  Created by luobin on 5/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKLoadMoreView.h"
#import "ZSTF3Engine.h"
#import "ZSTTopHintView.h"
#import "ZSTModuleBaseViewController.h"
#import "ZSTWeiBoInfo.h"

@interface ZSTWeiBoChatController : ZSTModuleBaseViewController <EGORefreshTableHeaderDelegate,TKLoadMoreViewDelegate,ZSTF3EngineDelegate,UITableViewDelegate,UITableViewDataSource>

{
    UITableView *_tableView;
    
    EGORefreshTableHeaderView *_refreshHeaderView;
    TKLoadMoreView *_loadMoreView;
    NSString *_errorDesc;
    BOOL _isRefreshing;
    BOOL _isLoadingMore;
    BOOL _hasMore;
    BOOL _loading;
    int _pagenum;
    ZSTTopHintView *topHintView;
    ZSTWeiBoInfo *wbInfo;
}

@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *messages;

@property (nonatomic, retain) ZSTTopHintView *topHintView;
@property (nonatomic, retain) ZSTWeiBoInfo *wbInfo;

@end
