//
//  ZSTGlobal+Weibo.h
//  WeiboA
//
//  Created by LiZhenQu on 13-4-9.
//
//

#import <Foundation/Foundation.h>

#define VIDEO_PAGE_SIZE 10

#define SortType_Desc @"Desc"               //向新取
#define SortType_Asc @"Asc"                 //向旧取


//////////////////////////////////////////////////////////////////////////////////////////
#define WeiboABaseURL @"http://mod.pmit.cn/WeiboA"
#define GetWeiboList WeiboABaseURL @"/Home/GetWeiboList"


