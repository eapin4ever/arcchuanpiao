//
//  ZSTF3Engine+WeiBo.h
//  WeiBo
//
//  Created by admin on 12-8-16.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTF3Engine.h"



@protocol ZSTF3EngineWeiBoDelegate <ZSTF3EngineDelegate>


- (void)setWBlistResponse:(id)result hasmore:(BOOL)hasmore;
- (void)getWBlistDidFailed:(NSInteger)code;
@end


@interface ZSTF3Engine (WeiBo)

//- (void)getWeiboListTypeID:(NSString *)typeId sinceID:(NSString *)sinceId order:(int)order count:(int)count page:(int)page url:(NSString *)url;
- (void)getWeiboListSinceID:(NSString *)sinceId orderType:(NSString*)ordertype Size:(int)size page:(int)page;

//- (void)getFollowedWeiboListTypeID:(NSString *)typeId sinceID:(NSString *)sinceId order:(int)order count:(int)count page:(int)page url:(NSString *)url;
- (void) getFollowedWeiboListSinceID:(NSString *)sinceId orderType:(NSString*)ordertype Size:(int)size page:(int)page;

@end
