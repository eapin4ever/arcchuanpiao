//
//  ZSTF3Engine+WeiBo.m
//  WeiBo
//
//  Created by admin on 12-8-16.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTF3Engine+WeiBo.h"
#import "ZSTWeiBoInfo.h"
#import "ZSTWeiboConst.h"
#import "ZSTWeiboDao.h"
#import "ZSTGlobal+Weibo.h"

@implementation ZSTF3Engine (WeiBo)


- (void)parsingSucceededForRequest:(NSString *)identifier withParsedObjects:(NSArray *)parsedObjects userInfo:(id)userInfo hasmore:(BOOL) hasmore
{
    NSString *type_id = [userInfo objectForKey:@"type_id"];
    int page = [[userInfo objectForKey:@"page"] intValue];

    if ([parsedObjects count] == TIMELINE_PAGE_SIZE){
        [[ZSTWeiboDao shared] deleteAllWeibosByType:type_id];
    }
    //入库
    [((ZSTWeiboDao *)[ZSTWeiboDao shared]).db beginTransaction];
    int number = 0;
    

//    if ([parsedObjects count] == 0) {
//        
//        if ([self isValidDelegateForSelector:@selector(setWBlistResponse:hasmore:)]) {
//            [self.delegate setWBlistResponse:parsedObjects hasmore:hasmore];
//        }
//        return;
//    }
    for (ZSTWeiBoInfo *aStatu in parsedObjects){
        
        BOOL isExist = [[ZSTWeiboDao shared] isWeiboExist:[NSString stringWithFormat:@"%lld",aStatu.ID] withType:@"new"];
//        if ([aStatu.time timeIntervalSince1970] < [[ZSTWeiboDao shared] getMinTimeByType:type_id]) //这句话给loadmore使用
//        {
//            if ([self isValidDelegateForSelector:@selector(setWBlistResponse:hasmore:)]) {
//                [self.delegate setWBlistResponse:parsedObjects hasmore:hasmore];
//            } 
//            return;
//        }
        if (!isExist)
            [[ZSTWeiboDao shared] insertWeiboFromDict:aStatu withType:@"new"];
        number++;
    }
    
    [((ZSTWeiboDao *)[ZSTWeiboDao shared]).db commit];
    [[ZSTWeiboDao shared] checkAndAssureWeibosSizeByType:type_id];
    
    if ([self isValidDelegateForSelector:@selector(setWBlistResponse:hasmore:)]) {
//        ZSTF3Preferences *preference = [ZSTF3Preferences shared];
//        preference.newWeiboAmmount  = number;
        NSArray *weibos = [NSArray array];
        weibos = [[ZSTWeiboDao shared] getWeibosAtPage:page pageCount:TIMELINE_PAGE_SIZE type:type_id];
        
        [self.delegate setWBlistResponse:weibos hasmore:hasmore];
    } 
}

- (void)setWBlistResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    
    if (response.resultCode != ZSTResultCode_OK) {
        
        if ([self isValidDelegateForSelector:@selector(getWBlistDidFailed:)]) {
            [self.delegate getWBlistDidFailed:response.resultCode];
        }
        return;
    }
    
    BOOL hasmore = [[response.jsonResponse objectForKey:@"hasmore"] boolValue];
    
    NSArray *dataArray = [response.jsonResponse objectForKey:@"info"];
    
    NSMutableArray *weiBolist = [NSMutableArray array];
    
    for (int i = 0; i < [dataArray count] ; i ++) {   
        
        ZSTWeiBoInfo *weiBoInfo = [[ZSTWeiBoInfo alloc] init];
        NSDictionary *member = [dataArray objectAtIndex:i];
        weiBoInfo.ID = [[member objectForKey:@"bid"] longLongValue];
        weiBoInfo.avatarString = [member objectForKey:@"uicon"];
        weiBoInfo.name = [member objectForKey:@"uname"];        
        NSString *timeStr = [member objectForKey:@"blogtime"];
        weiBoInfo.time = [NSDate dateWithTimeIntervalSince1970:[timeStr doubleValue]];
        weiBoInfo.content = [member objectForKey:@"bforwardothercontent"];
        weiBoInfo.forwardCount = [NSString stringWithFormat:@"%@",[member objectForKey:@"bforwardnumber"]];
        weiBoInfo.commentCount = [NSString stringWithFormat:@"%@",[member objectForKey:@"bcommentnumber"]];
        
        weiBoInfo.thumbnail_pic = [member objectForKey:@"bimgpath"];
        weiBoInfo.bmiddle_pic = [member objectForKey:@"bimgpath2"];
        weiBoInfo.original_pic = [member objectForKey:@"bimgpath3"];
        weiBoInfo.attachmentInfo = nil;
        weiBoInfo.bisforward = [[member objectForKey:@"bisforward"] boolValue];
        weiBoInfo.boriginalcontent = [member objectForKey:@"boriginalcontent"];
        weiBoInfo.type = [member objectForKey:@"btype"];
        [weiBolist addObject:weiBoInfo];
        [weiBoInfo release];
   
    }
    
    [self parsingSucceededForRequest:nil withParsedObjects:weiBolist userInfo:userInfo hasmore:hasmore];
}

- (NSDictionary *)parametersWithSeparator:(NSString *)separator delimiter:(NSString *)delimiter url:(NSString *)str{
    NSArray *parseUrl =[str componentsSeparatedByString:@"?"];
    
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    for (NSString *subStr in parseUrl) {
        
        NSArray *parameterPairs =[subStr componentsSeparatedByString:delimiter];
        for (NSString *currentPair in parameterPairs) {
            NSRange range = [currentPair rangeOfString:separator];
            if(range.location == NSNotFound)
                continue;
            NSString *key = [currentPair substringToIndex:range.location];
            NSString *value =[currentPair substringFromIndex:range.location + 1];
            [parameters setObject:value forKey:key];
        }
    }
    return parameters;

}


//- (void)getWeiboListTypeID:(NSString *)typeId sinceID:(NSString *)sinceId order:(int)order count:(int)count page:(int)page url:(NSString *)url
//{
//
//    NSDictionary *pair = [self parametersWithSeparator:@"=" delimiter:@"&" url:url];
//    NSString *value = [pair objectForKey:@"type_id"];
//
//    [[ZSTCommunicator shared] openAPIPostToPath:[url stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:
//                                                                          [ZSTF3Preferences shared].ECECCID,@"ecid",
//                                                                          [NSNumber numberWithInteger:self.moduleType],@"moduletype",
//                                                                          sinceId,@"since_id",
//                                                                          [NSNumber numberWithInt:order],@"order",
//                                                                          [NSNumber numberWithInt:count],@"count",
//                                                                          [NSNumber numberWithInt:page],@"page_index", nil]]
//                                         params:nil 
//                                         target:self
//                                       selector:@selector(setWBlistResponse:userInfo:) 
//                                       userInfo:value
//                                      matchCase:YES];
//    
//
//    
//    
//}

- (void)getWeiboListSinceID:(NSString *)sinceId orderType:(NSString*)ordertype Size:(int)size page:(int)page
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:sinceId forKey:@"sinceid"];
    [params setObject:[NSNumber numberWithInt:size] forKey:@"size"];
    [params setObject:ordertype forKey:@"ordertype"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetWeiboList stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(setWBlistResponse:userInfo:)
                                       userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%d",self.moduleType], @"type_id",[NSString stringWithFormat:@"%d",page],@"page" ,nil]
                                      matchCase:NO];
}

////loadMore新微博时调用
//- (void)getFollowedWeiboListTypeID:(NSString *)typeId sinceID:(NSString *)sinceId order:(int)order count:(int)count page:(int)page url:(NSString *)url
//{
//    NSArray *weibos;
//    
//    NSDictionary *pair = [self parametersWithSeparator:@"=" delimiter:@"&" url:url];
//    NSString *type_id = [pair objectForKey:@"type_id"];
//    weibos = [[ZSTWeiboDao shared] getWeibosAtPage:page pageCount:count type:type_id];
//    
//    if ([weibos count] == 0) {
////        [self getWeiboListTypeID:typeId sinceID:sinceId order:order count:count page:page url:url];
//        [self getWeiboListSinceID:sinceId orderType:SortType_Desc Size:count];
//        
//    }else{
//        [self.delegate setWBlistResponse:weibos dataFromLocal:YES];
//    }
//}

//- (void) getFollowedWeiboListSinceID:(NSString *)sinceId orderType:(NSString*)ordertype Size:(int)size page:(int)page 
//{
//    NSArray *weibos;
//    
////    NSDictionary *pair = [self parametersWithSeparator:@"=" delimiter:@"&" url:url];
////    NSString *type_id = [pair objectForKey:@"type_id"];
//    weibos = [[ZSTWeiboDao shared] getWeibosAtPage:page pageCount:size type:[NSString stringWithFormat:@"%d",self.moduleType]];
//    
//    if ([weibos count] == 0) {
//        //        [self getWeiboListTypeID:typeId sinceID:sinceId order:order count:count page:page url:url];
//        [self getWeiboListSinceID:sinceId orderType:ordertype Size:size page:page];
//        
//    }else{
//        [self.delegate setWBlistResponse:weibos hasmore:YES];
//    }
//
//}

@end
