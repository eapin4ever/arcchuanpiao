//
//  WeiboDao.m
//  Weibo
//
//  Created by China on 09-11-20.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "ZSTWeiboDao.h"
#import "ZSTWeiboConst.h"
#import "ZSTWeiBoInfo.h"
#import "ZSTF3Preferences.h" 

@implementation ZSTWeiboDao

@synthesize db;

//CREATE TABLE "weibos" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , "mblogid" TEXT, "bisforward" TEXT, "content" TEXT, "avatarString" TEXT, "name" TEXT, "time" double NULL DEFAULT 0, "thumbnail_pic" TEXT, "bmiddle_pic" TEXT, "original_pic" TEXT, "forwardCount" TEXT, "commentCount" TEXT, "boriginalcontent" TEXT, "type" TEXT)

//CREATE TABLE "my_pms" ("id" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , "pm_num" INTEGER, "pm_time" INTEGER, "pm_type" INTEGER, "msg_id" INTEGER, "uid" INTEGER, "nick" VARCHAR(50), "portrait" VARCHAR(255), "content" TEXT)


SINGLETON_IMPLEMENTATION(ZSTWeiboDao);

- (void)createEditableCopyOfDatabaseIfNeededForUser:(NSString *)username{
    // First, test for existence.
    BOOL success;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *dbFullName = [NSString stringWithFormat:@"weibos_%@.dat",username];
    NSString *writableDBPath = [documentsDirectory stringByAppendingPathComponent:dbFullName];
    success = [fileManager fileExistsAtPath:writableDBPath];
    if (success) return;
    // The writable database does not exist, so copy the default to the appropriate location.
    NSString *defaultDBPath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"Module.bundle/module_weiboa_weibo.dat"];
    success = [fileManager copyItemAtPath:defaultDBPath toPath:writableDBPath error:&error];
    if (!success) {
        NSAssert1(0, @"Failed to create writable database file with message '%@'.", [error localizedDescription]);
    }
}


- (id) initWithDbFullNameForUser:(NSString *)username{
    
    [self createEditableCopyOfDatabaseIfNeededForUser:username];
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
	NSString *dbFullName = [NSString stringWithFormat:@"weibos_%@.dat",username];
	NSString *dbPath = [documentsDirectory stringByAppendingPathComponent:dbFullName];
	ZSTFMDatabase *fmdb = [ZSTFMDatabase databaseWithPath:dbPath];
	if (![fmdb open]){
		NSAssert1(0, @"Failed to open database.",nil);
	}
	self.db = fmdb;	//千万注意不要写成 db=fmdb;
	return [super init];
}


-(id)init {
    self = [super init];
    if(self){
        [self initWithDbFullNameForUser:[ZSTF3Preferences shared].imsi];
    } 
    return self;
}


-(BOOL)isWeiboExist:(NSString *)mblogid withType:(NSString *)weiboType{
	//[db setTraceExecution:TRUE];
	//判断postid是否已经存在
	weiboType = weiboType?weiboType:@"new";
	int rowid = 0;
	if (weiboType==@"at")
		rowid = [db intForQuery:@"select id from at_weibos where mblogid = ?", mblogid];
	else
		rowid = [db intForQuery:@"select id from weibos where mblogid = ?", mblogid];
	if (rowid>0){
		return YES;
	}
	return NO;
}
-(double)getMinTimeByType:(NSString *)type
{
    double  deadline = [db doubleForQuery:[NSString stringWithFormat:@"select time from weibos where type = %@ order by time desc limit 19,1",type]];
    return deadline;
}
- (void)debugPrintUtf8:(NSString*)inStr {
}

-(void)insertWeiboFromDict:(ZSTWeiBoInfo *)weibo withType:(NSString *)weiboType {
	NSString *weiboTable = @"weibos";
	if (weiboType==@"at")
		weiboTable = @"at_weibos";
    
    [db executeUpdate:[NSString stringWithFormat:@"Insert or replace into %@ (mblogid,bisforward,content,avatarString,name,time,thumbnail_pic,bmiddle_pic,original_pic,forwardCount,commentCount,boriginalcontent,type) values (?,?,?,?,?,?,?,?,?,?,?,?,?)",weiboTable],
	 [NSString stringWithFormat:@"%lld",weibo.ID],[NSString stringWithFormat:@"%d",weibo.bisforward],weibo.content,weibo.avatarString,
	 weibo.name,weibo.time,weibo.thumbnail_pic,
	 weibo.bmiddle_pic,weibo.original_pic,weibo.forwardCount,
	 weibo.commentCount,weibo.boriginalcontent ,weibo.type];
    
	if ([db hadError])
		//Log(@"ERR %d: %@",[db lastErrorCode],[db lastErrorMessage])
		;
}

-(void)insertMyCommentFromDict:(NSDictionary *)dict {
	NSString *tableName = @"my_comments";
	//	[self debugPrintUtf8:[weibo valueForKey:@"nick"]];	
	[db executeUpdate:[NSString stringWithFormat:@"insert into %@ (mblogid,mbloguid,mblognick,mblogcontent,srcid,srcuid,srcnick,srccontent,commentid,commentuid,commentnick,commentportrait,commentcontent,commenttime) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",tableName],
	 [dict valueForKey:@"mblogid"],[dict valueForKey:@"mbloguid"],[dict valueForKey:@"mblognick"],[dict valueForKey:@"mblogcontent"],
	 [dict valueForKey:@"srcid"],[dict valueForKey:@"srcuid"],[dict valueForKey:@"srcnick"],
	 [dict valueForKey:@"srccontent"],[dict valueForKey:@"commentid"],[dict valueForKey:@"commentuid"],
	 [dict valueForKey:@"commentnick"],[dict valueForKey:@"commentportrait"],[dict valueForKey:@"commentcontent"],
	 [dict valueForKey:@"commenttime"]
	 ];
}
-(void)insertMyPmFromDict:(NSDictionary *)dict {
	NSString *tableName = @"my_pms";
	[db executeUpdate:[NSString stringWithFormat:@"insert into %@ (pm_num,pm_time,pm_type,msg_id,uid,nick,portrait,content) values (?,?,?,?,?,?,?,?)",tableName],
	 [dict valueForKey:@"pm_num"],[dict valueForKey:@"pm_time"],[dict valueForKey:@"pm_type"],[dict valueForKey:@"msg_id"],
	 [dict valueForKey:@"uid"],[dict valueForKey:@"nick"],[dict valueForKey:@"portrait"],
	 [dict valueForKey:@"content"]
	 ];
}

- (NSArray *)getWeibosAtPage:(int)pageNum pageCount:(int)pageCount type:(NSString *)type
{
	NSString *weiboTable = @"weibos";

	pageNum = (pageNum==0)?1:pageNum;
	pageCount = (pageCount<=0)?10:pageCount;
	
	NSMutableArray *weibos = [[NSMutableArray alloc] init];
	int startPos = (pageNum-1)*pageCount;
	ZSTFMResultSet *rs = [db executeQuery:[NSString stringWithFormat:@"select * from %@ order by time desc limit ?,?",weiboTable],
					   [NSNumber numberWithInt:startPos],
					   [NSNumber numberWithInt:pageCount] ];
	while ([rs next]){
        
        ZSTWeiBoInfo *aTweet = [[ZSTWeiBoInfo alloc] init];
		if ([rs stringForColumn:@"mblogid"]) 
			aTweet.ID = [[rs stringForColumn:@"mblogid"] longLongValue];
		if ([rs stringForColumn:@"bisforward"])
			aTweet.bisforward = [[rs stringForColumn:@"bisforward"] boolValue];
		if ([rs stringForColumn:@"content"])
			aTweet.content = [rs stringForColumn:@"content"];
		if ([rs stringForColumn:@"avatarString"])
			aTweet.avatarString = [rs stringForColumn:@"avatarString"];
		if ([rs stringForColumn:@"name"])
			aTweet.name = [rs stringForColumn:@"name"];
		if ([rs dateForColumn:@"time"])
			aTweet.time = [rs dateForColumn:@"time"];
		if ([rs stringForColumn:@"thumbnail_pic"])
			aTweet.thumbnail_pic = [rs stringForColumn:@"thumbnail_pic"];
		if ([rs stringForColumn:@"bmiddle_pic"])
			aTweet.bmiddle_pic = [rs stringForColumn:@"bmiddle_pic"];
		if ([rs stringForColumn:@"original_pic"])
			aTweet.original_pic = [rs stringForColumn:@"original_pic"];
		if ([rs stringForColumn:@"forwardCount"])
			aTweet.forwardCount = [rs stringForColumn:@"forwardCount"];
		if ([rs stringForColumn:@"commentCount"])
			aTweet.commentCount = [rs stringForColumn:@"commentCount"];
		if ([rs stringForColumn:@"boriginalcontent"])
			aTweet.boriginalcontent = [rs stringForColumn:@"boriginalcontent"];
        if ([rs stringForColumn:@"type"])
			aTweet.type = [rs stringForColumn:@"type"];
		[weibos addObject:aTweet];
		[aTweet release];		
	}
	[rs close];	
	return [weibos autorelease];
}

//@我的微博列表
- (NSArray *)getAtWeibosAtPage:(int)pageNum pageCount:(int)pageCount {
	NSString *weiboTable = @"at_weibos";
	
	//dateline = (loadCount==0)?2000000000:loadCount;
	pageNum = (pageNum==0)?1:pageNum;
	pageCount = (pageCount<=0)?10:pageCount;
	
	NSMutableArray *weibos = [[[NSMutableArray alloc] init] autorelease];
	int startPos = (pageNum-1)*pageCount;
	ZSTFMResultSet *rs = [db executeQuery:[NSString stringWithFormat:@"select * from %@ order by dateline desc limit ?,?",weiboTable],
					   [NSNumber numberWithInt:startPos],
					   [NSNumber numberWithInt:pageCount] ];
	while ([rs next]){
		NSMutableDictionary *aTweet = [[NSMutableDictionary alloc] init];
		if ([rs stringForColumn:@"mblogid"])
			[aTweet setObject:[rs stringForColumn:@"mblogid"] forKey:@"mblogid"];
		if ([rs stringForColumn:@"nick"])
			[aTweet setObject:[rs stringForColumn:@"nick"] forKey:@"nick"];
		if ([rs stringForColumn:@"portrait"])
			[aTweet setObject:[rs stringForColumn:@"portrait"] forKey:@"portrait"];
		if ([rs stringForColumn:@"content"])
			[aTweet setObject:[rs stringForColumn:@"content"] forKey:@"content"];
		if ([rs stringForColumn:@"pic"])
			[aTweet setObject:[rs stringForColumn:@"pic"] forKey:@"pic"];
		if ([rs stringForColumn:@"rtrootnick"])
			[aTweet setObject:[rs stringForColumn:@"rtrootnick"] forKey:@"rtrootnick"];
		if ([rs stringForColumn:@"rtreason"])
			[aTweet setObject:[rs stringForColumn:@"rtreason"] forKey:@"rtreason"];
		if ([rs stringForColumn:@"uid"])
			[aTweet setObject:[rs stringForColumn:@"uid"] forKey:@"uid"];
		if ([rs stringForColumn:@"rtnum"])
			[aTweet setObject:[rs stringForColumn:@"rtnum"] forKey:@"rtnum"];
		if ([rs stringForColumn:@"commentnum"])
			[aTweet setObject:[rs stringForColumn:@"commentnum"] forKey:@"commentnum"];
//		if ([rs stringForColumn:@"dateline"]) {
//			[aTweet setObject:[rs stringForColumn:@"dateline"] forKey:@"time"];
//			TweetterAppDelegate *delegate = (TweetterAppDelegate *)[UIApplication sharedApplication].delegate;
//			if ((delegate.lastTimeLineUpdated>=0)&&[[rs stringForColumn:@"dateline"] intValue]>delegate.lastTimeLineUpdated)
//				//if ([[rs stringForColumn:@"dateline"] intValue]>delegate.lastTimeLineUpdated)
//				[aTweet setObject:@"1" forKey:@"isNew"];
//		}
		if ([rs stringForColumn:@"vip"])
			[aTweet setObject:[rs stringForColumn:@"vip"] forKey:@"vip"];
		if ([rs stringForColumn:@"rtrootuid"])
			[aTweet setObject:[rs stringForColumn:@"rtrootuid"] forKey:@"rtrootuid"];
		if ([rs stringForColumn:@"rtrootvip"])
			[aTweet setObject:[rs stringForColumn:@"rtrootvip"] forKey:@"rtrootvip"];
		if ([rs stringForColumn:@"source"])
			[aTweet setObject:[rs stringForColumn:@"source"] forKey:@"source"];
		[weibos addObject:aTweet];
		[aTweet release];		
	}
	[rs close];	
	return weibos;
}
//我的评论列表
- (NSArray *)getMyCommentsAtPage:(int)pageNum pageCount:(int)pageCount {
	NSString *weiboTable = @"my_comments";
	pageNum = (pageNum==0)?1:pageNum;
	pageCount = (pageCount<=0)?10:pageCount;
	
	NSMutableArray *weibos = [[[NSMutableArray alloc] init] autorelease];
	int startPos = (pageNum-1)*pageCount;
	ZSTFMResultSet *rs = [db executeQuery:[NSString stringWithFormat:@"select * from %@ order by commenttime desc limit ?,?",weiboTable],
					   [NSNumber numberWithInt:startPos],
					   [NSNumber numberWithInt:pageCount] ];
	while ([rs next]){
		NSMutableDictionary *aTweet = [[NSMutableDictionary alloc] init];
		if ([rs stringForColumn:@"mblogid"])
			[aTweet setObject:[rs stringForColumn:@"mblogid"] forKey:@"mblogid"];
		if ([rs stringForColumn:@"mbloguid"])
			[aTweet setObject:[rs stringForColumn:@"mbloguid"] forKey:@"mbloguid"];
		if ([rs stringForColumn:@"mblognick"])
			[aTweet setObject:[rs stringForColumn:@"mblognick"] forKey:@"mblognick"];
		if ([rs stringForColumn:@"mblogcontent"])
			[aTweet setObject:[rs stringForColumn:@"mblogcontent"] forKey:@"mblogcontent"];
		if ([rs stringForColumn:@"srcid"])
			[aTweet setObject:[rs stringForColumn:@"srcid"] forKey:@"srcid"];
		if ([rs stringForColumn:@"srcuid"])
			[aTweet setObject:[rs stringForColumn:@"srcuid"] forKey:@"srcuid"];
		if ([rs stringForColumn:@"srcnick"])
			[aTweet setObject:[rs stringForColumn:@"srcnick"] forKey:@"srcnick"];
		if ([rs stringForColumn:@"srccontent"])
			[aTweet setObject:[rs stringForColumn:@"srccontent"] forKey:@"srccontent"];
		if ([rs stringForColumn:@"commentid"])
			[aTweet setObject:[rs stringForColumn:@"commentid"] forKey:@"commentid"];
		if ([rs stringForColumn:@"commentuid"])
			[aTweet setObject:[rs stringForColumn:@"commentuid"] forKey:@"commentuid"];
		if ([rs stringForColumn:@"commentnick"])
			[aTweet setObject:[rs stringForColumn:@"commentnick"] forKey:@"commentnick"];
		if ([rs stringForColumn:@"commentportrait"])
			[aTweet setObject:[rs stringForColumn:@"commentportrait"] forKey:@"commentportrait"];
		if ([rs stringForColumn:@"commentcontent"])
			[aTweet setObject:[rs stringForColumn:@"commentcontent"] forKey:@"commentcontent"];
		if ([rs stringForColumn:@"commenttime"])
			[aTweet setObject:[rs stringForColumn:@"commenttime"] forKey:@"commenttime"];
		[weibos addObject:aTweet];
		[aTweet release];		
	}
	[rs close];	
	return weibos;
}
//我的私信列表
- (NSArray *)getMyPmsAtPage:(int)pageNum pageCount:(int)pageCount {
	NSString *weiboTable = @"my_pms";
	pageNum = (pageNum==0)?1:pageNum;
	pageCount = (pageCount<=0)?10:pageCount;
	
	NSMutableArray *weibos = [[[NSMutableArray alloc] init] autorelease];
	int startPos = (pageNum-1)*pageCount;
	ZSTFMResultSet *rs = [db executeQuery:[NSString stringWithFormat:@"select * from %@ order by id limit ?,?",weiboTable],
					   [NSNumber numberWithInt:startPos],
					   [NSNumber numberWithInt:pageCount] ];
	while ([rs next]){
		NSMutableDictionary *aTweet = [[NSMutableDictionary alloc] init];
		if ([rs stringForColumn:@"pm_num"])
			[aTweet setObject:[rs stringForColumn:@"pm_num"] forKey:@"pm_num"];
		if ([rs stringForColumn:@"pm_time"])
			[aTweet setObject:[rs stringForColumn:@"pm_time"] forKey:@"pm_time"];
		if ([rs stringForColumn:@"pm_type"])
			[aTweet setObject:[rs stringForColumn:@"pm_type"] forKey:@"pm_type"];
		if ([rs stringForColumn:@"msg_id"])
			[aTweet setObject:[rs stringForColumn:@"msg_id"] forKey:@"msg_id"];
		if ([rs stringForColumn:@"uid"])
			[aTweet setObject:[rs stringForColumn:@"uid"] forKey:@"uid"];
		if ([rs stringForColumn:@"nick"])
			[aTweet setObject:[rs stringForColumn:@"nick"] forKey:@"nick"];
		if ([rs stringForColumn:@"portrait"])
			[aTweet setObject:[rs stringForColumn:@"portrait"] forKey:@"portrait"];
		if ([rs stringForColumn:@"content"])
			[aTweet setObject:[rs stringForColumn:@"content"] forKey:@"content"];
		
		[weibos addObject:aTweet];
		[aTweet release];		
	}
	[rs close];	
	return weibos;
}


- (void) checkAndAssureWeibosSizeByType:(NSString *)type
{
//	return;
	int weibosCount = [db intForQuery:[NSString stringWithFormat:@"select count(id) from weibos where type = %@",type]];
	if (weibosCount>20){
		//int maxWeiboId = [db intForQuery:@"select max(id) from weibos"] - 100;
		//[db executeUpdate:@"delete from weibos where id<?",
		double  deadline = [db doubleForQuery:[NSString stringWithFormat:@"select time from weibos where type = %@ order by time desc limit 19,1",type]];
		[db executeUpdate:[NSString stringWithFormat:@"delete from weibos where type = %@ and time<?",type],
		 [NSNumber numberWithInt:deadline]
		 ];
	}
}

- (void) deleteWeiboByMblogid:(NSString *)mblogid{
	[db executeUpdate:@"delete from weibos where mblogid=?",mblogid];
}

- (int) getMblogCount{
	return [db intForQuery:@"select count(id) from weibos"];
}

- (NSString *) getMaxMblodId{
	//不能用max(mblogid)，因为mblogid ascii排序有问题
	return [db stringForQuery:@"select mblogid from weibos order by dateline desc limit 1"];
}

- (int) getLastestDateLine{
	return [db intForQuery:@"select dateline from weibos order by dateline desc limit 1"];
}

- (void) deleteStockWeibosAfterMblogid:(NSString *)mblogid{
	if (!mblogid) return;
	//因为mblogid序号问题，不要直接通过比较mblogid删除，通过dateline比较删除
	[db executeUpdate:@"delete from weibos where mblogid<=?",mblogid];
}
- (void) deleteStockWeibosBeforeMblogid:(NSString *)mblogid{
    
    if (!mblogid) return;
	[db executeUpdate:@"delete from weibos where mblogid>=?",mblogid];

}



/*
- (void) resetWeiboRead {
	[db executeUpdate:@"update weibos set read=1 "];
}*/

- (BOOL) mblogidIsGreater:(NSString *)id1 than:(NSString *)id2{
	if (!id1) return FALSE;
	int length = [id1 length];
	for (int i = 0;i<length;i++){
		unichar char1 = [id1 characterAtIndex:i];
		char1 = (char1>=65)&&(char1<=90)?char1+60:char1;
		unichar char2 = [id2 characterAtIndex:i];
		char2 = (char2>=65)&&(char2<=90)?char2+60:char2;
		if (char1==char2) continue;
		return (char1>char2);
	}
	return TRUE;
}

- (NSArray *) getLocalWeibosBetween:(NSString *)minMblogid max:(NSString *)maxMblogid {
	
	NSMutableArray *weibos = [[[NSMutableArray alloc] init] autorelease];
	ZSTFMResultSet *rs;
	int max_dateline = [db intForQuery:@"select dateline from weibos where mblogid=?",maxMblogid];	
	if ( (minMblogid==nil)|| [self mblogidIsGreater:minMblogid than:maxMblogid] ) {
		rs = [db executeQuery:@"select * from weibos where dateline<? order by dateline desc limit 50",
			  [NSNumber numberWithInt:max_dateline] ];
	} else {
		int min_dateline = [db intForQuery:@"select dateline from weibos where mblogid=?",minMblogid];
		rs = [db executeQuery:@"select * from weibos where dateline<? and dateline>? order by dateline desc limit 50",
			  [NSNumber numberWithInt:max_dateline],[NSNumber numberWithInt:min_dateline] ];	
	}
	
	while ([rs next]){
		NSMutableDictionary *aTweet = [[NSMutableDictionary alloc] init];
		if ([rs stringForColumn:@"mblogid"])
			[aTweet setObject:[rs stringForColumn:@"mblogid"] forKey:@"mblogid"];
		[weibos addObject:aTweet];
		[aTweet release];		
	}
	[rs close];	
	return weibos;
}

- (void) setExtraInfoValue:(NSString *)value forKey:(NSString *)key {
	//todo if insert
	[db executeUpdate:@"update extra_info set value=? where field=?",value,key];
}

- (NSString *) getExtraInfoValueForKey:(NSString *)key {
	return [db stringForQuery:@"select value from extra_info where field=? limit 1",key];	
}

- (void) dealloc{
	if (db) [db release]; 
	[super dealloc];
}

  
- (void) deleteAllWeibosByType:(NSString *)type
{
	[db executeUpdate:[NSString stringWithFormat:@"delete from weibos where type = %@",type]];
}

- (Boolean) tableHasData:(NSString *)tableName {
	int recordsCount = [db intForQuery:[NSString stringWithFormat:@"select count(*) all_count from %@",tableName]];
	//int recordsCount = [db intForQuery:@"select count(*) all_count from ?",tableName];
	return recordsCount>0;
}
- (NSArray *)getCacheOfAtWeibo {
	return [self getAtWeibosAtPage:1 pageCount:DEFAULT_PAGE_SIZE];
}
- (NSArray *)getCacheOfMyComments {
	return [self getMyCommentsAtPage:1 pageCount:DEFAULT_PAGE_SIZE];
}
- (NSArray *)getCacheOfMyPms {
	return [self getMyPmsAtPage:1 pageCount:DEFAULT_PAGE_SIZE];
}
- (void) clearTable:(NSString *)tableName {                                                                                                                                                                                                                                                                                                                       
	[db executeUpdate:[NSString stringWithFormat:@"delete from %@",tableName]];
}

//用户管理 depreted
/*- (void) checkUserTableExist {
	[db executeUpdate:@"create table weibo_users (id INTEGER PRIMARY KEY, nick VARCHAR, uid VARCHAR, thumb VARCHAR, loginname VARCHAR, gsid VARCHAR, sid VARCHAR)"];
}

- (NSArray *)getLocalUsers {
	[self checkUserTableExist];
	NSMutableArray *items = [[NSMutableArray alloc] init];
	ZSTFMResultSet *rs = [db executeQuery:@"select * from weibo_users order by id limit 10"];
	while ([rs next]){
		NSMutableDictionary *aItem = [[NSMutableDictionary alloc] init];
		if ([rs stringForColumn:@"nick"])
			[aItem setObject:[rs stringForColumn:@"nick"] forKey:@"nick"];
		if ([rs stringForColumn:@"uid"])
			[aItem setObject:[rs stringForColumn:@"uid"] forKey:@"uid"];
		if ([rs stringForColumn:@"thumb"])
			[aItem setObject:[rs stringForColumn:@"thumb"] forKey:@"thumb"];
		if ([rs stringForColumn:@"loginname"])
			[aItem setObject:[rs stringForColumn:@"loginname"] forKey:@"loginname"];
		if ([rs stringForColumn:@"gsid"])
			[aItem setObject:[rs stringForColumn:@"gsid"] forKey:@"gsid"];
		if ([rs stringForColumn:@"sid"])
			[aItem setObject:[rs stringForColumn:@"sid"] forKey:@"sid"];
		
		[items addObject:aItem];
		[aItem release];		
	}
	[rs close];	
	return [items autorelease];
}

- (void)saveUserWithNick:(NSString *)nick uid:(NSString *)uid {
	[db executeUpdate:@"insert into weibo_users (nick, uid) values (?, ?)" , nick, uid];
} */

@end
