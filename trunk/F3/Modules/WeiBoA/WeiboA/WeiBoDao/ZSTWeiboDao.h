//
//  WeiboDao.h
//  Weibo
//
//  Created by China on 09-11-20.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZSTFMDatabase.h"
#import "ZSTFMDatabaseAdditions.h"
#import "ZSTWeiBoInfo.h"


@interface ZSTWeiboDao : NSObject {
	ZSTFMDatabase *db;
	
}

SINGLETON_INTERFACE(ZSTWeiboDao);

@property (nonatomic, retain) ZSTFMDatabase *db;

SINGLETON_INTERFACE(ZSTWeiboDao)

- (id) initWithDbFullNameForUser:(NSString *)username;
-(BOOL)isWeiboExist:(NSString *)mblogid withType:(NSString *)weiboType;
- (void)insertWeiboFromDict:(ZSTWeiBoInfo *)weibo withType:(NSString *)weiboType;
- (NSArray *)getWeibosAtPage:(int)pageNum pageCount:(int)pageCount type:(NSString *)type;
-(double)getMinTimeByType:(NSString *)type;
- (void) deleteAllWeibosByType:(NSString *)type;
- (void) checkAndAssureWeibosSizeByType:(NSString *)type;







- (void) deleteStockWeibosAfterMblogid:(NSString *)mblogid;


- (void) deleteStockWeibosBeforeMblogid:(NSString *)mblogid;



- (NSString *) getMaxMblodId;
- (int) getLastestDateLine;
- (int) getMblogCount;
- (void) deleteStockWeibosAfterMblogid:(NSString *)mblogid;
- (void) deleteWeiboByMblogid:(NSString *)mblogid;
- (NSArray *) getLocalWeibosBetween:(NSString *)minMblogid max:(NSString *)maxMblogid;

- (void) setExtraInfoValue:(NSString *)value forKey:(NSString *)key;
- (NSString *) getExtraInfoValueForKey:(NSString *)key;
- (Boolean) tableHasData:(NSString *)tableName;
- (NSArray *)getCacheOfAtWeibo;
- (NSArray *)getCacheOfMyComments;
- (void) clearTable:(NSString *)tableName;
-(void)insertMyCommentFromDict:(NSDictionary *)dict;
- (NSArray *)getCacheOfMyPms;

@end
