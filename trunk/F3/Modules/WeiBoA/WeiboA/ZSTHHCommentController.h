//
//  HHCommentController.h
//  YouYun
//
//  Created by luo bin on 12-6-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKPlaceHolderTextView.h"

@protocol ZSTHHCommentControllerDelegate;

@interface ZSTHHCommentController : UIViewController<UITextViewDelegate>

@property (nonatomic, retain) TKPlaceHolderTextView *textView;

@property (nonatomic, retain) UILabel *countLabel;

@property (nonatomic, retain) UIImageView *backgroundView;

@property (nonatomic, assign) id<ZSTHHCommentControllerDelegate> delegate;

@property (nonatomic, retain) NSString *ID;

@property (nonatomic, retain) NSString *CID;

@property (nonatomic) BOOL isForward;

@property (nonatomic) BOOL isComment;

@property (nonatomic) BOOL isReply;

@property (nonatomic, retain) NSString * forwardString;



- (void)sendButtonAction:(id)sender;

@end

@protocol ZSTHHCommentControllerDelegate <NSObject>

@optional

- (void)commentControllerDidFinish:(ZSTHHCommentController *)commentController;

- (void)commentControllerDidCancel:(ZSTHHCommentController *)commentController;

@end
