//
//  ZSTWeiBoMainBodyViewController.h
//  F3
//
//  Created by  on 12-6-14.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTWeiBoInfo.h"
#import "ZSTWBToolBar.h"
#import "TKLoadMoreView.h"
#import "ZSTMainBodyCell.h"
#import "ZSTHHCommentController.h"

@interface ZSTWeiBoMainBodyViewController : UIViewController<TKLoadMoreViewDelegate,UITableViewDataSource,UITableViewDelegate,ZSTWBToolBarDelegate,UIActionSheetDelegate,ZSTMainBodyUserTextCellDelegate,ZSTHHCommentControllerDelegate>

{
    UITableView *_tableView;
    TKLoadMoreView *_loadMoreView;
    UIImageView *_sectionHeader;
    NSString *_errorDesc;
    BOOL _isRefreshing;
    BOOL _isLoadingMore;
    BOOL _hasMore;
    BOOL _loading;
    BOOL _isForwardList;
    BOOL _isCommentList;
    BOOL fav;
    int _forwardPage;
    int _commentPage;
    ZSTWBToolBar *_toolBar;
    float _webHeight;
    UIButton *forwardCountBtn;
    UIButton *commentCountBtn;
}
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) ZSTWeiBoInfo *weiBoInfo;
@property (nonatomic, retain) NSMutableArray *list;
@property (nonatomic, retain) NSMutableArray *commentList;
@property (nonatomic, retain) NSMutableArray *forwardList;
@property (nonatomic, retain) UIActionSheet *progressSheet;
@property (nonatomic, assign) BOOL fav;

@end
