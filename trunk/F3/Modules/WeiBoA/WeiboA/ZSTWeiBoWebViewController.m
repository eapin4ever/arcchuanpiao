// Copyright (c) 2009 Imageshack Corp.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#import "ZSTWeiBoWebViewController.h"
#import "ZSTMyMsgFooterBar.h"


@interface MapAnnotation : NSObject <MKAnnotation>
{
	CLLocationCoordinate2D coordinate;
	NSString *title;
	NSString *subtitle;
}
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *subtitle;
@end
@implementation MapAnnotation
@synthesize coordinate;
@synthesize title;
@synthesize subtitle;

- (id) initWithCoordinate: (CLLocationCoordinate2D) aCoordinate
{
	if (self = [super init]) coordinate = aCoordinate;
	return self;
}

-(void) dealloc
{
	self.title = nil;
	self.subtitle = nil;
	[super dealloc];
}
@end

@implementation ZSTWeiBoWebViewController

- (id)initWithRequest:(NSURLRequest*)request
{
	self = [super initWithNibName:nil bundle:nil];
	if(self)
	{
		_request = [request retain];
		self.hidesBottomBarWhenPushed = YES;
	}
	
	return self;
}

- (void)dealloc
{
	webView.delegate = nil;
	if(webView.loading)
	{
		[webView stopLoading];
	}
	[_request release];
	[super dealloc];
}

-(void)showIndicator {
    UIBarButtonItem *rightButton = 
        [[UIBarButtonItem alloc] initWithCustomView:[[[UIImageView alloc]initWithImage:ZSTModuleImage(@"module_weiboa_refreshBg.png")] autorelease] ];
	rightButton.width = 0;
    self.navigationItem.rightBarButtonItem = rightButton;
    [rightButton release];
    if(!_indicator) {
        _indicator = [[UIActivityIndicatorView alloc]
                      initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        _indicator.frame = CGRectMake(288, 33, 20, 20);
        [self.navigationController.view addSubview:_indicator];
    }
    [_indicator startAnimating];
}

-(void)hideIndicator {
    [_indicator stopAnimating];
 	self.navigationItem.rightBarButtonItem = nil;
}

- (void)backAction:(id)sender 
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (UIBarButtonItem *)initColorBarItemWithAction:(SEL)action title:(NSString *)aTitle image:(NSString *)buttonImageFile highLightImage:(NSString *)buttonImageFileHightlight {
    
	UIImage *buttonImage = ZSTModuleImage(buttonImageFile);
	UIImage *buttonImageHighlight= ZSTModuleImage(buttonImageFileHightlight);
	UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
	[button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
	[button setImage:buttonImage forState:UIControlStateNormal];
	[button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
	button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
	UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
	return customBarItem;
}

- (void)viewDidLoad 
{
    [super viewDidLoad];
    
    webView = [[UIWebView alloc] initWithFrame:CGRectMake(0.0, 0.0, 320.0, 460-44)];
    webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    webView.clipsToBounds = YES;
    webView.scalesPageToFit = YES;
    webView.delegate = self;
    [self.view addSubview:webView];
    
	[webView loadRequest:_request];
	self.navigationItem.title = @"";//@"Safari";
    
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithTitle:NSLocalizedString(@"返回",@"") style:UIBarButtonItemStylePlain target:self action:@selector(backAction:)];
	self.navigationItem.leftBarButtonItem = backItem;
    [backItem release];
	self.view.autoresizesSubviews = YES;
	self.view.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
	
	
	CGRect toolBarFrame = CGRectMake(0.0, 372.0,320.0, 44.0);
	ZSTMyMsgFooterBar *toolBar = [[ZSTMyMsgFooterBar alloc]initWithFrame:toolBarFrame];
	
	UIBarButtonItem *preItem = [self initColorBarItemWithAction:@selector(previousAction:) title:nil image:@"module_weiboa_bar_left.png" highLightImage:@"module_weiboa_bar_left_down.png"];
	UIBarButtonItem *nextItem = [self initColorBarItemWithAction:@selector(nextAction:) title:nil image:@"module_weiboa_bar_right.png" highLightImage:@"module_weiboa_bar_right_down.png"];
	UIBarButtonItem *refreshItem = [self initColorBarItemWithAction:@selector(refreshAction:) title:nil image:@"module_weiboa_bar_refresh.png" highLightImage:@"module_weiboa_bar_refresh_down.png"];
	UIBarButtonItem *shareItem = [self initColorBarItemWithAction:@selector(otherItemAction:) title:nil image:@"module_weiboa_bar_action.png" highLightImage:@"module_weiboa_bar_action_down.png"];
	UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
	NSArray *toolBarItem = [[NSArray alloc]initWithObjects:flexItem,preItem,flexItem,refreshItem,flexItem,shareItem,flexItem,nextItem,flexItem, nil];
	[toolBar setItems:toolBarItem animated:YES];
	[flexItem release];
	toolBar.barStyle = UIBarStyleBlackOpaque;
	[self.view addSubview:toolBar];
	[toolBar release];
	[toolBarItem release];
	
}
-(void)otherItemAction:(id)sender
{
	
	UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"请选择需要的操作",@"")
															 delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"取消",@"")
                                               destructiveButtonTitle:nil
													otherButtonTitles:NSLocalizedString(@"浏览器中打开",""),NSLocalizedString(@"推荐给朋友",@""), nil];
    
	actionSheet.tag = 1;
	actionSheet.actionSheetStyle = UIActionSheetStyleDefault;
	actionSheet.destructiveButtonIndex = 2;
	//[actionSheet dismissWithClickedButtonIndex:3 animated:YES];
	[actionSheet showInView:self.view];
	[actionSheet release];
	
	
}
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
	switch (buttonIndex) {
		case 2:
			break;
		case 1:
			[self shareAction:self];
			break;
		case 0:
			;
			//open safari
			[[UIApplication sharedApplication] openURL:[_request URL]];
			break;
			
		default:
			break;
	}
}
-(void)shareAction:(id)sender
{
	MFMailComposeViewController *controller = [[MFMailComposeViewController alloc] init];
	controller.mailComposeDelegate = self;
	[controller setSubject:NSLocalizedString(@"分享内容",@"")];
	[controller setMessageBody:[[_request URL]absoluteString] isHTML:NO];
	[self presentModalViewController:controller animated:YES];
	[controller release];
}
-(void)refreshAction:(id)sender
{
	[webView reload];
}
-(void)previousAction:(id)sender
{
	[webView goBack];
}
-(void)nextAction:(id)sender
{
	[webView goForward];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
    [self hideIndicator];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
	// starting the load, show the activity indicator in the status bar
//	[TweetterAppDelegate increaseNetworkActivityIndicator];
    [self showIndicator];
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self hideIndicator];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
//	[TweetterAppDelegate decreaseNetworkActivityIndicator];
    [self hideIndicator];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
//	[TweetterAppDelegate decreaseNetworkActivityIndicator];
    [self hideIndicator];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
//	if([[[request URL] host] isEqualToString:@"maps.google.com"])
//	{
//		//39.982876,+116.303314
//		NSString *mapUrl = [[request URL] absoluteString];
//		//mapUrl = @"http://maps.google.com/maps?q=+28.162941,+112.979851";
//		//http://maps.google.com/maps?q=+28.162941,+112.979851
//		//NSString *mapUrl = @"http://maps.google.com/maps/ms?hl=zh-CN&ie=UTF8&oe=UTF8&msa=0&msid=116411192758648920583.0004815ab44e512ab018d&ll=39.906383,116.45997&spn=0.001988,0.00346&z=18";
//		NSRange queryRange = [mapUrl rangeOfString:@"?q="];
//		if ( (queryRange.location == NSNotFound) ||
//			(queryRange.length == NSNotFound) )
//			return YES;
//		NSString *query = [mapUrl substringFromIndex:(queryRange.location+queryRange.length)];
//		NSArray *degrees = [query componentsSeparatedByString:@","];
//		CLLocationCoordinate2D center = {0,0};
//		if (degrees&&[degrees count]==2) {
//			center.latitude = [[degrees objectAtIndex:0] doubleValue];
//			center.longitude = [[degrees objectAtIndex:1] doubleValue];
//			//CLLocationCoordinate2D center = {39.982876,+116.303314};
//		}
//		MKMapView *mapView = [[MKMapView alloc] init];
//		mapView.frame = webView.frame;
//		MKCoordinateRegion region = MKCoordinateRegionMake(center, MKCoordinateSpanMake(0.1f, 0.1f));
//		//mapView.showsUserLocation = YES;
//		//mapView.zoomEnabled = YES;
//		[mapView setRegion:region animated:TRUE];		
//		mapView.delegate = self;
//		[self.view addSubview:mapView];
//		
//		MapAnnotation *annotation = [[[MapAnnotation alloc] initWithCoordinate:center] autorelease];
//		//annotation.title = CURRENT_STRING;
//		//[annotations addObject:annotation];
//		[mapView addAnnotation:annotation];
//		
//		[mapView release];
//		return NO;
//		
//		/*BOOL success = NO;
//		success = [[UIApplication sharedApplication] openURL: [request URL]];
//		if(success)
//			return NO;*/
//	}

	return YES;
}

//- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation
//{
//	/*if( [[annotation title] isEqualToString:@"Current Location"] )
//	{
//		return nil;
//	}*/
//	
//	MKPinAnnotationView *pin = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:@"currentloc"];
//	
//	if( pin == nil )
//	{
//		pin = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"currentloc"];
//		[pin autorelease];
//	}
//	
//    pin.animatesDrop = YES;
//    pin.canShowCallout = YES;
//	
//	pin.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
//	
//    return pin;
//}
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
	[self dismissViewControllerAnimated:YES completion:nil];
}
@end
