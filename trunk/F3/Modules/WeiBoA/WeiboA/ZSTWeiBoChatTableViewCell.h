//
//  ZSTGroupChatTableViewCell.h
//  YouYun
//
//  Created by luobin on 5/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TKAsynImageView.h"
#import "ZSTWeiBoInfo.h"

@interface ZSTWeiBoChatTableViewCell : UITableViewCell <TKAsynImageViewDelegate,MTZoomWindowDelegate,HJManagedImageVDelegate>

@property (nonatomic, retain) TKAsynImageView *avtarImageView;//用户头像

@property (nonatomic, retain) UILabel *name;//用户名称

@property (nonatomic, retain) UILabel *contentLabel;//用户发表内容

@property (nonatomic, retain) TKAsynImageView *pictureImageView;//用户发表图片

@property (nonatomic, retain) UILabel *dateLabel;//用户发表日期

@property (nonatomic, retain) UIImageView *pictureIcon;//标识是否有图片

@property (nonatomic, retain) UILabel *platformLabel;//用户使用平台

@property (nonatomic, retain) UIImageView *fowardCountLabel;//用户转发计数

@property (nonatomic, retain) UIImageView *commentCountLabel;//用户评论计数

@property (nonatomic, assign) BOOL isAttachment;//是否有转发内容

@property (nonatomic, retain) UILabel *attachmentContentLabel;//转发内容

@property (nonatomic, retain) TKAsynImageView *attachmentPictureImageView;//转发图片

@property (nonatomic, retain) UIImageView *attachmentImageView;//转发背景图

@property (nonatomic, assign) int rowNumber;

@property (nonatomic, assign) BOOL isSelect;

- (void)setWeiBoInfo:(ZSTWeiBoInfo *)weiBoInfo;
@end
