//
//  TopHintView.m
//  Weibo
//
//  Created by China on 09-12-1.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import "ZSTTopHintView.h"


@implementation ZSTTopHintView

@synthesize hintLabel,bgImage;

- (id)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
	self.backgroundColor = [UIColor clearColor];
	self.hidden = TRUE;
	
	UIImageView *_bgImage = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_weiboa_popBg.png")];
	self.bgImage = _bgImage;
	[_bgImage release];
	[self addSubview:_bgImage];
	
	UILabel *_hintLabel = [[UILabel alloc] initWithFrame:CGRectMake(44,5,280,34)];
	_hintLabel.backgroundColor = [UIColor clearColor];
	_hintLabel.font = [UIFont systemFontOfSize:14];
	_hintLabel.textColor = [UIColor whiteColor];
	self.hintLabel = _hintLabel;
	[_hintLabel release];
	[self addSubview:hintLabel];
	}
    return self;
}

-(void)showHint:(NSString*)msg count:(int)count{
	hintLabel.text = msg;
	self.hidden = FALSE;
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.8f];

	CGRect frame = self.frame;
	frame.origin.y += frame.size.height;
	self.frame = frame;
	[UIView commitAnimations];
	[self performSelector:@selector(hideHint) withObject:nil afterDelay:2.0f];
}

-(void)hideHint{	
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.8f];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(onHideHintEnd)];
	CGRect frame = self.frame;
	frame.origin.y -= frame.size.height;
	self.frame = frame;
	[UIView commitAnimations];
}

-(void)onHideHintEnd{
	self.hidden = TRUE;
}

- (void)drawRect:(CGRect)rect {
    // Drawing code
}


- (void)dealloc {
	[hintLabel release],hintLabel = nil;
	[bgImage release],bgImage = nil;
    [super dealloc];
}


@end
