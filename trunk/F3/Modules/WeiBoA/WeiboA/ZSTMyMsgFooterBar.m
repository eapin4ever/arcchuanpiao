//
//  MyMsgFooterBar.m
//  Weibo
//
//  Created by China on 10-5-7.
//  Copyright 2010 Sina. All rights reserved.
//

#import "ZSTMyMsgFooterBar.h"


@implementation ZSTMyMsgFooterBar


- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
	UIImage *bgImage = ZSTModuleImage(@"module_weiboa_bar_msg_bg.png");
	[bgImage drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
}

- (void)dealloc {
    [super dealloc];
}


@end
