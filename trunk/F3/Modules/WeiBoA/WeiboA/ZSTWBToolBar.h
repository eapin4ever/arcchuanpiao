//
//  ZSTWBToolBar.h
//  F3
//
//  Created by  on 12-6-19.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZSTWBToolBar;

@protocol ZSTWBToolBarDelegate <NSObject>

- (void)toolBarRefresh:(ZSTWBToolBar *)toolBar;
- (void)toolBarCommentItemAction:(ZSTWBToolBar *)toolBar;
- (void)toolBarForwardItemAction:(ZSTWBToolBar *)toolBar;
- (void)toolBarFavItemAction:(ZSTWBToolBar *)toolBar;
- (void)toolBarFavCancelItemAction:(ZSTWBToolBar *)toolBar;
- (void)toolBarOtherItemAction:(ZSTWBToolBar *)toolBar;

@end


@interface ZSTWBToolBar : UIToolbar

@property (nonatomic ,assign) id<ZSTWBToolBarDelegate> delegate;

- (id)initWithFrame:(CGRect)frame haveFav:(Boolean)haveFav; 
- (void)initToolbar:(Boolean)haveFav;

@end

