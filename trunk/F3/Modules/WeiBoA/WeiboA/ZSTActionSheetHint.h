//
//  ActionSheetHint.h
//  Weibo
//
//  Created by China on 10-1-25.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sinaweibo.h"

@protocol ZSTActionSheetHintDelegate <NSObject>
@optional
- (void)ActionSheetHintCancel;
@end

@interface ZSTActionSheetHint : UIActionSheet {
	Sinaweibo *twitterEngine;
	id<ZSTActionSheetHintDelegate> canDelegate;
}

@property (nonatomic,retain) Sinaweibo *twitterEngine;
@property (nonatomic,assign) id<ZSTActionSheetHintDelegate> canDelegate;

+ (id) hintWithTitle:(NSString *)hint delegate:(id <UIActionSheetDelegate>)delegate shouldShowCancel:(BOOL)shouldShowCancel engine:(Sinaweibo *)engine parentView:(UIView *)parentView;

@end
