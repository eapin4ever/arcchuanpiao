// Copyright (c) 2009 Imageshack Corp.
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
// 1. Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in the
//    documentation and/or other materials provided with the distribution.
// 3. The name of the author may not be used to endorse or promote products
//    derived from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
// IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
// OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
// INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
// NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
// THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 

#include "util.h"

#define	IMAGE_SCALING_SIZE		320
#define CELL_CONTENT_WIDTH 260.0	//221.0	//信息流气泡文本宽度

NSString* urlEncodeValue(NSString *str) {
	NSString *result = (NSString *) CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)str, NULL, CFSTR(":/?#[]@!$&’()*+,;="), kCFStringEncodingUTF8);
	return [result autorelease];
}
//new 2010.05.05
NSString* urlDecodeValue(NSString *inStr) {
	NSString *result = (NSString *) CFURLCreateStringByReplacingPercentEscapesUsingEncoding(kCFAllocatorDefault, (CFStringRef)inStr, CFSTR(""), kCFStringEncodingUTF8);
	return [result autorelease];
}

void LogDictionaryStringKeys(NSDictionary* dict, NSString* descriptionString)
{

}

void LogStringSet(NSSet* set, NSString* descriptionString)
{

}

UIActionSheet * ShowActionSheet(NSString* title, id <UIActionSheetDelegate> delegate,UIView* forView)
{
	NSString *cancelButtonTitle = nil;
	UIActionSheet* progressSheet = [[UIActionSheet alloc] initWithTitle:title delegate:delegate cancelButtonTitle:cancelButtonTitle destructiveButtonTitle:nil otherButtonTitles:nil];
	progressSheet.actionSheetStyle = UIActionSheetStyleDefault;
	CGFloat height = cancelButtonTitle == nil? -9.0:-4.0;
	
	UIActivityIndicatorView* progressInd = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(-18.5, height, 40.0, 40.0)];
	progressInd.tag = 1001;
	progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
	[progressInd sizeToFit];
	progressInd.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
									UIViewAutoresizingFlexibleRightMargin |
									UIViewAutoresizingFlexibleTopMargin |
								UIViewAutoresizingFlexibleBottomMargin);
	[progressInd startAnimating];
	[progressSheet addSubview:progressInd];
	[progressInd autorelease];
	
	UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
	cancelButton.titleLabel.font = [UIFont systemFontOfSize:13];
	[cancelButton setTitle:NSLocalizedString(@"取消",@"取消") forState:UIControlStateNormal];
	[cancelButton setBackgroundImage:[UIImage imageNamed:@"cancel_action.png"] forState:UIControlStateNormal];
	cancelButton.frame = CGRectMake(20,10,51,28);
	[progressSheet addSubview:cancelButton];
		
	[progressSheet showInView:forView]; 
	return [progressSheet autorelease];
}

void hideActionSheetIndicator(UIActionSheet* sheet) {
	UIActivityIndicatorView *indicator = (UIActivityIndicatorView *)[sheet viewWithTag:1001];
	if (indicator)
		[indicator stopAnimating];
	
	UIButton *cancelButton = (UIButton *)[sheet viewWithTag:1002];
	if (cancelButton)
		[cancelButton setHidden:YES];
}
	
// may cause a crash in non main thead
UIImage* imageScaledToSize(UIImage* image, int maxDimension)
{
	CGImageRef imgRef = image.CGImage;  
	
	CGFloat width = CGImageGetWidth(imgRef);  
	CGFloat height = CGImageGetHeight(imgRef);  
	
	CGAffineTransform transform = CGAffineTransformIdentity;
	CGRect bounds = CGRectMake(0, 0, width, height);  
	
	if(maxDimension > 0) //need scale
	{
		 if (width > maxDimension || height > maxDimension) 
		 {  
			 CGFloat ratio = width/height;  
			 if (ratio > 1)
			 {  
				 bounds.size.width = maxDimension;  
				 bounds.size.height = bounds.size.width / ratio;  
			 }  
			 else
			 {  
				 bounds.size.height = maxDimension;  
				 bounds.size.width = bounds.size.height * ratio;  
			 }  
		 }
	}
	CGFloat scaleRatio = bounds.size.width / width;
	CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));  
    CGFloat boundHeight;  
	
	UIImageOrientation orient = image.imageOrientation;  
    switch(orient) 
	{  
        case UIImageOrientationUp: //EXIF = 1  
            transform = CGAffineTransformIdentity;  
            break;  
			
        case UIImageOrientationUpMirrored: //EXIF = 2  
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);  
            transform = CGAffineTransformScale(transform, -1.0, 1.0);  
            break;  
			
        case UIImageOrientationDown: //EXIF = 3  
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);  
            transform = CGAffineTransformRotate(transform, M_PI);  
            break;  
			
        case UIImageOrientationDownMirrored: //EXIF = 4  
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);  
            transform = CGAffineTransformScale(transform, 1.0, -1.0);  
            break;  
			
        case UIImageOrientationLeftMirrored: //EXIF = 5  
            boundHeight = bounds.size.height;  
            bounds.size.height = bounds.size.width;  
            bounds.size.width = boundHeight;  
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);  
            transform = CGAffineTransformScale(transform, -1.0, 1.0);  
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);  
            break;  
			
        case UIImageOrientationLeft: //EXIF = 6  
            boundHeight = bounds.size.height;  
            bounds.size.height = bounds.size.width;  
            bounds.size.width = boundHeight;  
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);  
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);  
            break;  
			
        case UIImageOrientationRightMirrored: //EXIF = 7  
            boundHeight = bounds.size.height;  
            bounds.size.height = bounds.size.width;  
            bounds.size.width = boundHeight;  
            transform = CGAffineTransformMakeScale(-1.0, 1.0);  
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);  
            break;  
			
        case UIImageOrientationRight: //EXIF = 8  
            boundHeight = bounds.size.height;  
            bounds.size.height = bounds.size.width;  
            bounds.size.width = boundHeight;  
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);  
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);  
            break;  
			
        default:  
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];  
			
    }  
	
    UIGraphicsBeginImageContext(bounds.size);
	
    CGContextRef context = UIGraphicsGetCurrentContext();  
	
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft)
	{
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);  
    }
    else
	{  
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);  
    }  
	
    CGContextConcatCTM(context, transform);  
	
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);  
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();  
    UIGraphicsEndImageContext();  
	
    return imageCopy;
	
}

int isImageNeedToConvert(UIImage* testImage, BOOL *needToResize, BOOL *needToRotate)
{
	*needToResize = [[NSUserDefaults standardUserDefaults] boolForKey:@"ScalePhotosBeforeUploading"] &&
	(testImage.size.width > IMAGE_SCALING_SIZE || testImage.size.height > IMAGE_SCALING_SIZE);
	*needToRotate = testImage.imageOrientation != UIImageOrientationUp;

    if(*needToResize)
		return IMAGE_SCALING_SIZE;
	else 
		return -1;
}

NSString* ValidateYFrogLink(NSString *yfrogUrl)
{
	//if host is not yfrog.com it's not a link to yfrog
	NSURL *url = [NSURL URLWithString:yfrogUrl];
	if([[url host] rangeOfString:@"yfrog." options:NSCaseInsensitiveSearch].location == NSNotFound)
		return nil;
		
		
	NSString *path = [url path];
	if(!path || [path length] <= 1)
		return nil; //it's a path to the site start page
	
	//Image and mp4 video URLs don't end with x and y.
	if([path hasSuffix:@"x"] || [path hasSuffix:@"y"])
		return nil;
			
	//If it's a link to a thunbnail, truncate .th.jpg to get a link to an image page
	NSRange range = [path rangeOfString:@".th.jpg"];
	if(range.location != NSNotFound)
		path = [path substringToIndex:range.location];
	
	//If there is a '.' character, it's not a proper link to picture
	range = [path rangeOfString:@"."];
	if(range.location != NSNotFound)
		return nil;
	
	range = [path rangeOfString:@":"];
	if(range.location != NSNotFound)
		path = [path substringToIndex:range.location];
		
	url = [[[NSURL alloc] initWithScheme:[url scheme]  host:[url host] path:path] autorelease];
	return [url absoluteString];
}

BOOL isVideoLink(NSString *yfrogUrl)
{
	NSURL *url = [NSURL URLWithString:yfrogUrl];
	if([[url host] rangeOfString:@"yfrog." options:NSCaseInsensitiveSearch].location == NSNotFound)
		return NO;
		
		
	NSString *path = [url path];
	if(!path || [path length] <= 1)
		return NO; //it's a path to the site start page
	
	if([path hasSuffix:@"z"])
		return YES; //It's mp4 video
	
	return NO;
}

NSString* getLinkWithTag(NSString *tag)
{
    NSString *link = nil;
    NSRange range = [tag rangeOfString:@" href"];
    if (range.length > 0)
    {
        char buf[1024];
        
        unsigned len = [tag length];
        unsigned idx = range.location + range.length;
        while ([tag characterAtIndex:idx] != '=' && idx < len)
            idx++;
        
        unichar ch, endCh = ' ';
        do {
            idx++;
            ch = [tag characterAtIndex:idx];
            if (ch == '"' || ch == '\'')
                endCh = ch;
        } while (ch == ' ' || ch == '"' || ch == '\'' && idx < len);
        
        unsigned i = 0;
        while (((ch = [tag characterAtIndex:idx]) != endCh) && (idx < len))
        {
            buf[i++] = (char)ch;
            idx++;
        }
        buf[i] = 0;
//        link = [NSString stringWithCString:buf length:i];
        link = [NSString stringWithCString:buf encoding:NSUTF8StringEncoding];
    }
    return link;
}

static struct
{
	NSString *codeForm, *literalForm;
} EntityTable[] = 
{
	{@"&#34;",	@"&quot;"},//	quotation mark
	{@"&#39;",	@"&apos;"},// (does not work in IE)	apostrophe 
	{@"&#38;",	@"&amp;"},//	ampersand
	{@"&#60;",	@"&lt;"},//	less-than
	{@"&#62;",	@"&gt;"},//	greater-than
	{@"&#160;",	@"&nbsp;"},//	non-breaking space
	{@"&#161;",	@"&iexcl;"},//	inverted exclamation mark
	{@"&#162;",	@"&cent;"},//	cent
	{@"&#163;",	@"&pound;"},//	pound
	{@"&#164;",	@"&curren;"},//	currency
	{@"&#165;",	@"&yen;"},//	yen
	{@"&#166;",	@"&brvbar;"},//	broken vertical bar
	{@"&#167;",	@"&sect;"},//	section
	{@"&#168;",	@"&uml;"},//	spacing diaeresis
	{@"&#169;",	@"&copy;"},//	copyright
	{@"&#170;",	@"&ordf;"},//	feminine ordinal indicator
	{@"&#171;",	@"&laquo;"},//	angle quotation mark (left)
	{@"&#172;",	@"&not;"},//	negation
	{@"&#173;",	@"&shy;"},//	soft hyphen
	{@"&#174;",	@"&reg;"},//	registered trademark
	{@"&#175;",	@"&macr;"},//	spacing macron
	{@"&#176;",	@"&deg;"},//	degree
	{@"&#177;",	@"&plusmn;"},//	plus-or-minus 
	{@"&#178;",	@"&sup2;"},//	superscript 2
	{@"&#179;",	@"&sup3;"},//	superscript 3
	{@"&#180;",	@"&acute;"},//	spacing acute
	{@"&#181;",	@"&micro;"},//	micro
	{@"&#182;",	@"&para;"},//	paragraph
	{@"&#183;",	@"&middot;"},//	middle dot
	{@"&#184;",	@"&cedil;"},//	spacing cedilla
	{@"&#185;",	@"&sup1;"},//	superscript 1
	{@"&#186;",	@"&ordm;"},//	masculine ordinal indicator
	{@"&#187;",	@"&raquo;"},//	angle quotation mark (right)
	{@"&#188;",	@"&frac14;"},//	fraction 1/4
	{@"&#189;",	@"&frac12;"},//	fraction 1/2
	{@"&#190;",	@"&frac34;"},//	fraction 3/4
	{@"&#191;",	@"&iquest;"},//	inverted question mark
	{@"&#215;",	@"&times;"},//	multiplication
	{@"&#247;",	@"&divide;"},//	division
	{@"&#192;",	@"&Agrave;"},//	capital a, grave accent
	{@"&#193;",	@"&Aacute;"},//	capital a, acute accent
	{@"&#194;",	@"&Acirc;"},//	capital a, circumflex accent
	{@"&#195;",	@"&Atilde;"},//	capital a, tilde
	{@"&#196;",	@"&Auml;"},//	capital a, umlaut mark
	{@"&#197;",	@"&Aring;"},//	capital a, ring
	{@"&#198;",	@"&AElig;"},//	capital ae
	{@"&#199;",	@"&Ccedil;"},//	capital c, cedilla
	{@"&#200;",	@"&Egrave;"},//	capital e, grave accent
	{@"&#201;",	@"&Eacute;"},//	capital e, acute accent
	{@"&#202;",	@"&Ecirc;"},//	capital e, circumflex accent
	{@"&#203;",	@"&Euml;"},//	capital e, umlaut mark
	{@"&#204;",	@"&Igrave;"},//	capital i, grave accent
	{@"&#205;",	@"&Iacute;"},//	capital i, acute accent
	{@"&#206;",	@"&Icirc;"},//	capital i, circumflex accent
	{@"&#207;",	@"&Iuml;"},//	capital i, umlaut mark
	{@"&#208;",	@"&ETH;"},//	capital eth, Icelandic
	{@"&#209;",	@"&Ntilde;"},//	capital n, tilde
	{@"&#210;",	@"&Ograve;"},//	capital o, grave accent
	{@"&#211;",	@"&Oacute;"},//	capital o, acute accent
	{@"&#212;",	@"&Ocirc;"},//	capital o, circumflex accent
	{@"&#213;",	@"&Otilde;"},//	capital o, tilde
	{@"&#214;",	@"&Ouml;"},//	capital o, umlaut mark
	{@"&#216;",	@"&Oslash;"},//	capital o, slash
	{@"&#217;",	@"&Ugrave;"},//	capital u, grave accent
	{@"&#218;",	@"&Uacute;"},//	capital u, acute accent
	{@"&#219;",	@"&Ucirc;"},//	capital u, circumflex accent
	{@"&#220;",	@"&Uuml;"},//	capital u, umlaut mark
	{@"&#221;",	@"&Yacute;"},//	capital y, acute accent
	{@"&#222;",	@"&THORN;"},//	capital THORN, Icelandic
	{@"&#223;",	@"&szlig;"},//	small sharp s, German
	{@"&#224;",	@"&agrave;"},//	small a, grave accent
	{@"&#225;",	@"&aacute;"},//	small a, acute accent
	{@"&#226;",	@"&acirc;"},//	small a, circumflex accent
	{@"&#227;",	@"&atilde;"},//	small a, tilde
	{@"&#228;",	@"&auml;"},//	small a, umlaut mark
	{@"&#229;",	@"&aring;"},//	small a, ring
	{@"&#230;",	@"&aelig;"},//	small ae
	{@"&#231;",	@"&ccedil;"},//	small c, cedilla
	{@"&#232;",	@"&egrave;"},//	small e, grave accent
	{@"&#233;",	@"&eacute;"},//	small e, acute accent
	{@"&#234;",	@"&ecirc;"},//	small e, circumflex accent
	{@"&#235;",	@"&euml;"},//	small e, umlaut mark
	{@"&#236;",	@"&igrave;"},//	small i, grave accent
	{@"&#237;",	@"&iacute;"},//	small i, acute accent
	{@"&#238;",	@"&icirc;"},//	small i, circumflex accent
	{@"&#239;",	@"&iuml;"},//	small i, umlaut mark
	{@"&#240;",	@"&eth;"},//	small eth, Icelandic
	{@"&#241;",	@"&ntilde;"},//	small n, tilde
	{@"&#242;",	@"&ograve;"},//	small o, grave accent
	{@"&#243;",	@"&oacute;"},//	small o, acute accent
	{@"&#244;",	@"&ocirc;"},//	small o, circumflex accent
	{@"&#245;",	@"&otilde;"},//	small o, tilde
	{@"&#246;",	@"&ouml;"},//	small o, umlaut mark
	{@"&#248;",	@"&oslash;"},//	small o, slash
	{@"&#249;",	@"&ugrave;"},//	small u, grave accent
	{@"&#250;",	@"&uacute;"},//	small u, acute accent
	{@"&#251;",	@"&ucirc;"},//	small u, circumflex accent
	{@"&#252;",	@"&uuml;"},//	small u, umlaut mark
	{@"&#253;",	@"&yacute;"},//	small y, acute accent
	{@"&#254;",	@"&thorn;"},//	small thorn, Icelandic	
	{@"&#255;",	@"&yuml;"},//	small y, umlaut mark
};

static NSString *decodedStringForEntity(int entityIndex)
{
	NSString *codeForm = EntityTable[entityIndex].codeForm;
	int code;
	sscanf([[codeForm substringFromIndex:2] UTF8String], "%d", &code);
	char codeStr[2];
	codeStr[0] = (char)code;
	codeStr[1] = 0;
	return [NSString stringWithCString:codeStr encoding:NSISOLatin1StringEncoding];
}

NSString *DecodeEntities(NSString *str)
{
	str = [str stringByReplacingOccurrencesOfString:@"&lt;" withString:@"<"];
	str = [str stringByReplacingOccurrencesOfString:@"&gt;" withString:@">"];
	str = [str stringByReplacingOccurrencesOfString:@"&quot;" withString:@"\""];
	str = [str stringByReplacingOccurrencesOfString:@"&amp;" withString:@"&"];	//必须再最后
	return str;
    /*str = [str stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"];	
    str = [str stringByReplacingOccurrencesOfString:@"<" withString:@"&lt;"];
    str = [str stringByReplacingOccurrencesOfString:@">" withString:@"&gt;"];*/
	
	str = [str stringByReplacingOccurrencesOfString:@"\\n" withString:@""];
	NSMutableString *bufstr = [[str mutableCopy] autorelease];
	
	for(int i = 0; i < sizeof(EntityTable)/sizeof(*EntityTable); ++i)
	{
		NSRange entRange = [bufstr rangeOfString:EntityTable[i].codeForm];
		if(entRange.location != NSNotFound)
		{
			[bufstr replaceCharactersInRange:entRange withString:decodedStringForEntity(i)];
		}
			 
		entRange = [bufstr rangeOfString:EntityTable[i].literalForm];
		if(entRange.location != NSNotFound)
		{
			[bufstr replaceCharactersInRange:entRange withString:decodedStringForEntity(i)];
		}
	}
	
	return bufstr;
}

NSURLRequest* tweeteroURLRequest(NSURL* url)
{
	NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
//	[req setValue:[WBEngine userAgent] forHTTPHeaderField:@"User-Agent"];
	return req;
}

NSMutableURLRequest* tweeteroMutableURLRequest(NSURL* url)
{
	NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL:url];
//	[req setValue:[WBEngine userAgent] forHTTPHeaderField:@"User-Agent"];
	return req;
}


NSString* convertToTime(NSString* message)
{
    NSString* time;
	NSCalendar* calendar= [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSCalendarUnit unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
	NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];

    
    double dd = [message doubleValue];
    NSDate* createdAt = [NSDate dateWithTimeIntervalSince1970:dd];
    NSDateComponents *nowComponents = [calendar components:unitFlags fromDate:[NSDate date]];
    NSDateComponents *createdAtComponents = [calendar components:unitFlags fromDate:createdAt];
	if([nowComponents year] == [createdAtComponents year] &&
       [nowComponents month] == [createdAtComponents month] &&
       [nowComponents day] == [createdAtComponents day])
    {//今天
		
		int time_long = [createdAt timeIntervalSinceNow];
		
		if (time_long < 0 && time_long >-60*60) {//一小时之内
			int min = -time_long/60;
			if (min == 0) {
				min = 1;
			}
			time = [[NSString alloc]initWithFormat:NSLocalizedString(@"%d分钟前",@""),min];
			
		}else {
			[dateFormatter setDateFormat:NSLocalizedString(@"'今天'HH:mm",@"")];
			time = [dateFormatter stringFromDate:createdAt];
		}
    }
    else
    {//前天及以前
		NSLocale *cnLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
		[dateFormatter setLocale:cnLocale];
		[cnLocale release];
		[dateFormatter setDateFormat:NSLocalizedString(@"MMMMd'日 'HH:mm",@"")];
		time = [dateFormatter stringFromDate:createdAt];
    }
    [calendar release];
    [dateFormatter release];
	
    return time;
}

NSString* getRootPath()
{
	NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
	NSString *documentsDirectory = [paths objectAtIndex:0];
    
	return documentsDirectory;
}

BOOL saveArray(NSArray* array, NSString* fileName)
{
	NSString *path = [NSString stringWithFormat:@"%@/%@", getRootPath(), fileName];
	BOOL success = [array writeToFile:path atomically:YES];
	if(!success) {
	}
	return success;
}

NSArray* getArray(NSString* fileName)
{
	NSString *path = [NSString stringWithFormat:@"%@/%@", getRootPath(), fileName];
	NSArray *dict = [NSArray arrayWithContentsOfFile:path];
	return dict;
}

NSString* handleSpecialCharsForBrowser(NSString* text)
{
    NSMutableString* msg = [NSMutableString stringWithCapacity:0];
    unichar* buffer = calloc([text length], sizeof(unichar));
    [text getCharacters:buffer];
    int idx = 0;
    bool isHuati = false;
    int startHuati = 0;
    bool isFollow = false;
    int startFollow = 0;
    int startNormal = 0;
    
    while (idx < [text length])
    {
        if (buffer[idx] == '#')
        {
            if (isHuati)
            {
                isHuati = false;
                NSString* huati = [NSString stringWithFormat:@"<a href=\"%@%@?gsid=%@\">%@</a>",
								   HUATI_DOMAIN,
                                   [text substringWithRange:NSMakeRange(startHuati+1, idx-startHuati-1)],
								   [SinaWeibo gsid],
								   [text substringWithRange:NSMakeRange(startHuati, (idx-startHuati+1))]];
                [msg appendString:huati];
                startHuati = 0;
                startNormal = idx + 1;
            }
            else
            {
                if (idx > startNormal)
                {
                    NSString* normalStr = [NSString stringWithFormat:@"%@", 
                                           [text substringWithRange:NSMakeRange(startNormal, (idx-startNormal))]];
                    [msg appendString:normalStr];
                }
                startHuati = idx;
                isHuati = true;
            }
        }
        else if (buffer[idx] == '@')
        {
            if (!isHuati) {
                if (idx > startNormal)
                {
                    if (isFollow) {
                        if (idx == startFollow + 1) {
                            [msg appendString:@"@"];
                        } else {
                            NSString* follow = [NSString stringWithFormat:@"<a href=\"%@%@?gsid=%@\">%@</a>",
                                                FOLLOW_DOMAIN,
                                                [text substringWithRange:NSMakeRange(startFollow+1, (idx-startFollow)-1)],
                                                [SinaWeibo gsid],
                                                [text substringWithRange:NSMakeRange(startFollow, (idx-startFollow))]];
                            [msg appendString:follow];
                        }
                    }
                    else {
                        NSString* normalStr = [NSString stringWithFormat:@"%@", 
                                               [text substringWithRange:NSMakeRange(startNormal, (idx-startNormal))]];
                        [msg appendString:normalStr];
                    }
                }
                isFollow = true;
                startFollow = idx;
            }
        }
        else
        {
            if (isFollow)
            {
                int c = (int)buffer[idx];
				NSString *temp = [[NSString alloc]initWithString:@"`~!#$%^&*()-=+[]{}|\';:\"?/><,.　｀～·！◎＃￥％※×（）—＋－＝§÷】【『』‘’“”；：？、》。：《，／＞＜｛｝＼"];
				NSCharacterSet * markStr = [[NSCharacterSet characterSetWithCharactersInString:temp] invertedSet];
				[temp release];
                if (!((c >= 0x4e00 && c <= 0x9fa5)
                      || (c >= 0xff00 && c <= 0xffff)
                      || (c >= 0x0800 && c <= 0x4e00)
                      || (c >= 0x3130 && c <= 0x318f)
                      || (c >= 0xac00 && c <= 0xd7a3)
                      || (c >= 'a' && c <= 'z')
                      || (c >= 'A' && c <= 'Z')
                      || (c >= '0' && c <= '9'))
                    //|| c == ':')
					  || (![markStr characterIsMember:c])
					  || c == ' ' )
                {
					if (c != '_') {
						isFollow = false;
						if (idx == startFollow + 1) {
							[msg appendString:@"@"];
						} else {
							NSString* follow = [NSString stringWithFormat:@"<a href=\"%@%@?gsid=%@\">%@</a>",
												FOLLOW_DOMAIN,
												[text substringWithRange:NSMakeRange(startFollow+1, (idx-startFollow)-1)],
												[SinaWeibo gsid],
												[text substringWithRange:NSMakeRange(startFollow, (idx-startFollow))]];
							[msg appendString:follow];
						}
						
						//startNormal = idx + 1;
						startNormal = idx;// + 1;
					}
				}

            }
            
        }
        
        idx++;
    }
    free(buffer);
    
    if (isHuati)
    {
        NSString* huatiStr = [text substringWithRange:NSMakeRange(startHuati, (idx-startHuati))];
        [msg appendString:huatiStr];
        isHuati = false;
        startHuati = 0;
    }
    else if (isFollow)
    {
        NSString* followStr = [NSString stringWithFormat:@"<a href=\"%@%@?gsid=%@\">%@</a>",
							   FOLLOW_DOMAIN,
                               [text substringWithRange:NSMakeRange(startFollow+1, (idx-startFollow)-1)],
                               [SinaWeibo gsid],
							   [text substringWithRange:NSMakeRange(startFollow, (idx-startFollow))]];
        [msg appendString:followStr];
        isFollow = false;
        startFollow = 0;
    }
    else
    {
        if (idx > startNormal)
        {
            NSString* normalStr = [NSString stringWithFormat:@"%@", 
                                   [text substringWithRange:NSMakeRange(startNormal, (idx-startNormal))]];
            [msg appendString:normalStr];
        }
    }
	return msg;    
}
/*
NSString* stripEntityCode(NSString *inStr) {
	NSString *retStr = [NSString 
    
	return documentsDirectory;
}*/

void playSound(NSString *soundFile) {
    // Get the main bundle for the app
	//	[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"NeedUpdateFlag"];
	BOOL isPlay = [[NSUserDefaults standardUserDefaults]boolForKey:@"AppSoundFlag"];
	if (!isPlay) {
		return;
	}
	CFBundleRef mainBundle;
	SystemSoundID soundFileObject;
	mainBundle = CFBundleGetMainBundle ();
    
	// Get the URL to the sound file to play
	CFStringRef cfFilename= (CFStringRef)soundFile; //CFStringCreateWithCString(NULL, [str UTF8String], NSUTF8StringEncoding);
	CFURLRef soundFileURLRef  = CFBundleCopyResourceURL (
														 mainBundle,
														 cfFilename,
//														 @"msgcome",
														 CFSTR ("wav"),
														 NULL
														 );
	// Create a system sound object representing the sound file
    AudioServicesCreateSystemSoundID (
									  soundFileURLRef,
									  &soundFileObject
									  );
	CFRelease(soundFileURLRef);
	// Add sound completion callback
	AudioServicesAddSystemSoundCompletion (soundFileObject, NULL, NULL,
										   NULL,
										   (void*)NULL);
	// Play the audio
	AudioServicesPlaySystemSound(soundFileObject);
    
}

CGFloat computeWeiboInfo(NSMutableDictionary *weibo) 
{
	int total_height = [[weibo objectForKey:@"total_height"] intValue];
	if (total_height) {
		return total_height;
	}
	
	BOOL isWeiboCollapsed = [[NSUserDefaults standardUserDefaults]boolForKey:@"isWeiboCollapsed"];
	//const float CELL_CONTENT_WIDTH = 225.0;
	
	int totalHeight = 0; int contentHeight = 0; int originHeight = 0;
	
	if ([weibo objectForKey:@"rtreason"]) {
		NSString *originContent = [NSString stringWithFormat:@"%@: %@",[weibo objectForKey:@"rtrootnick"],[weibo objectForKey:@"content"]];
		if (isWeiboCollapsed)
			if ([originContent length]>28)
				originContent = [NSString stringWithFormat:@"%@...",[originContent substringToIndex:26]];
		[weibo setObject:originContent forKey:@"brief_origin_content"];
		NSString *brief_content = [weibo objectForKey:@"rtreason"];
		if (isWeiboCollapsed)
			if ([brief_content length]>42)
				brief_content = [NSString stringWithFormat:@"%@...",[brief_content substringToIndex:40]];
		[weibo setObject:brief_content forKey:@"brief_content"];
	}else {
		NSString *brief_content = [weibo objectForKey:@"content"];
		if (isWeiboCollapsed)
			if ([brief_content length]>42)
				brief_content = [NSString stringWithFormat:@"%@...",[brief_content substringToIndex:40]];
		[weibo setObject:brief_content forKey:@"brief_content"];
	}
	
	/*if ([weibo objectForKey:@"content_height"])
	 contentHeight = [[weibo objectForKey:@"content_height"] intValue];
	 else {*/
	NSString *content = [weibo objectForKey:@"brief_content"];
	CGSize size = [content sizeWithFont:[UIFont systemFontOfSize:16] constrainedToSize:CGSizeMake(CELL_CONTENT_WIDTH, 1000.0f) lineBreakMode:NSLineBreakByCharWrapping];
	contentHeight = size.height;
	[weibo setValue:[NSNumber numberWithInt:contentHeight] forKey:@"content_height"];
	//}
	if ([weibo objectForKey:@"rtreason"]) {
		/*if ([weibo objectForKey:@"origin_height"])
		 originHeight = [[weibo objectForKey:@"origin_height"] intValue];
		 else {*/
		NSString *originContent = [weibo objectForKey:@"brief_origin_content"];
		CGSize size = [originContent sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(CELL_CONTENT_WIDTH - 10, 1000.0f) lineBreakMode:NSLineBreakByCharWrapping];
		originHeight = size.height+10;
		[weibo setValue:[NSNumber numberWithInt:originHeight] forKey:@"origin_height"];
		//}
	}
	totalHeight = contentHeight + originHeight + 50;
	totalHeight = totalHeight>75?totalHeight:75;
//	totalHeight += 5;
	[weibo setValue:[NSNumber numberWithInt:totalHeight] forKey:@"total_height"];
	return totalHeight;
}
