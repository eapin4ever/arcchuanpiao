//
//  ZSTWBToolBar.m
//  F3
//
//  Created by  on 12-6-19.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTWBToolBar.h"

@implementation ZSTWBToolBar
@synthesize delegate;

- (UIBarButtonItem *)initColorBarItemWithAction:(SEL)action title:(NSString *)aTitle image:(NSString *)buttonImageFile highLightImage:(NSString *)buttonImageFileHightlight {

    
    NSString *normal = [NSString stringWithFormat:@"%@",buttonImageFile];
    NSString *Hightlight = [NSString stringWithFormat:@"%@",buttonImageFileHightlight];
    
	UIImage *buttonImage = ZSTModuleImage(normal);
	UIImage *buttonImageHighlight= ZSTModuleImage(Hightlight);

	UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
	[button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
	[button setImage:buttonImage forState:UIControlStateNormal];
	[button setImage:buttonImageHighlight forState:UIControlStateHighlighted];
	button.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
    button.imageEdgeInsets = UIEdgeInsetsMake(buttonImage.size.height*0.1, buttonImage.size.width*0.1, buttonImage.size.height*0.1, buttonImage.size.width*0.1);
    
	UIBarButtonItem *customBarItem = [[UIBarButtonItem alloc] initWithCustomView:button];
	return customBarItem;
}

- (void)initToolbar:(Boolean)haveFav 
{
    UIBarButtonItem *refreshItem = [self initColorBarItemWithAction:@selector(refresh:) title:NSLocalizedString(@"刷新" , nil) image:@"module_weiboa_toolbar_refresh.png" highLightImage:@"module_weiboa_toolbar_refresh_highlighted.png"];
    refreshItem.width = 30;
    UIBarButtonItem *commentItem = [self initColorBarItemWithAction:@selector(commentItemAction:) title:NSLocalizedString(@"评论" , nil) image:@"module_weiboa_toolbar_comment.png" highLightImage:@"module_weiboa_toolbar_comment_highlighted.png"];
    commentItem.width = 86;
    
    UIBarButtonItem *forwardItem = [self initColorBarItemWithAction:@selector(forwardItemAction:) title:NSLocalizedString(@"转发" , nil) image:@"module_weiboa_toolbar_retweet.png" highLightImage:@"module_weiboa_toolbar_retweet_highlighted.png"];
    forwardItem.width = 46;
    
    
    UIBarButtonItem *otherItem = [self initColorBarItemWithAction:@selector(otherItemAction:) title:NSLocalizedString( @"更多", nil) image:@"module_weiboa_toolbar_more.png" highLightImage:@"module_weiboa_toolbar_more_highlighted.png"];
    otherItem.width = 30;
    
    UIBarButtonItem *flexItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    flexItem.width = 64;
    
    NSArray *toolBarItems;
    toolBarItems = haveFav?[[NSArray alloc]initWithObjects:flexItem,refreshItem,flexItem,commentItem,flexItem,forwardItem,flexItem,flexItem,otherItem,flexItem, nil]:[[NSArray alloc]initWithObjects:flexItem,refreshItem,flexItem,commentItem,flexItem,forwardItem,flexItem,flexItem,otherItem,flexItem, nil];
    [self setItems:toolBarItems animated:YES];
    
    [flexItem release];
    [toolBarItems release];
    [refreshItem release];
    [commentItem release];
    [forwardItem release];

    [otherItem release]; 
}

- (id)initWithFrame:(CGRect)frame haveFav:(Boolean)haveFav 
{
    self = [super initWithFrame:frame];
    if (self) {
        
 
        [self initToolbar:haveFav];
    }
    return self;
}

- (void)refresh:(UIButton *)sender
{
    if ([delegate respondsToSelector:@selector(toolBarRefresh:)]) {
        [delegate toolBarRefresh:self];
    }
}
- (void)commentItemAction:(UIButton *)sender
{
    if ([delegate respondsToSelector:@selector(toolBarCommentItemAction:)]) {
        [delegate toolBarCommentItemAction:self];
    }
}

- (void)forwardItemAction:(UIButton *)sender
{
    if ([delegate respondsToSelector:@selector(toolBarForwardItemAction:)]) {
        [delegate toolBarForwardItemAction:self];
    }
}
- (void)favItemAction:(UIButton *)sender
{
    if ([delegate respondsToSelector:@selector(toolBarFavItemAction:)]) {
        [delegate toolBarFavItemAction:self];
    } 
}
- (void)favCancelItemAction:(UIButton *)sender
{
    if ([delegate respondsToSelector:@selector(toolBarFavCancelItemAction:)]) {
    [delegate toolBarFavCancelItemAction:self];
    }
}
- (void)otherItemAction:(UIButton *)sender
{
    if ([delegate respondsToSelector:@selector(toolBarOtherItemAction:)]) {
        [delegate toolBarOtherItemAction:self];
    }
}
- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    UIImage *backgroundImage = [ZSTModuleImage(@"module_weiboa_toolbar_background.png") stretchableImageWithLeftCapWidth:rect.size.width topCapHeight:0];
    [backgroundImage drawInRect:CGRectMake(0, 0, rect.size.width, rect.size.height)];

    
}

@end
