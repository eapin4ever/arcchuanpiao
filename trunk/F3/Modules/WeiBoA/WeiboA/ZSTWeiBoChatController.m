//
//  ZSTGroupChatController.m
//  YouYun
//
//  Created by luobin on 5/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTWeiBoChatController.h"
#import "ZSTWeiBoInfo.h"
#import "WBSDKGlobal.h"
#import "ZSTWeiBoMainBodyViewController.h"
#import "ZSTF3Engine+WeiBo.h"
#import "ZSTUtils.h"
#import "ZSTTopHintView.h"
#import "ZSTWeiBoChatTableViewCell.h"
#import "ZSTWeiboDao.h"
#import "TKGlobal.h"
#import "TKUIUtil.h"
#import "ZSTGlobal+Weibo.h"
#import "ZSTF3Preferences.h"

#define set_shareToSina_tag 7

@interface NSString (Support) 
- (NSComparisonResult) psuedoNumericCompare:(id)otherObject;
@end


@implementation ZSTWeiBoChatController

@synthesize tableView = _tableView;
@synthesize messages;
@synthesize topHintView;
@synthesize wbInfo;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc
{

    [_tableView release];
    [messages release];
    [super dealloc];
}

#pragma mark - View lifecycle
- (void)viewWillAppear:(BOOL)animated
{
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:[self.application.launchOptions objectForKey:@"Name"]];
    if (messages == nil) {
        self.messages = [NSMutableArray arrayWithCapacity:3];
        self.messages = (NSMutableArray *)[[ZSTWeiboDao shared] getWeibosAtPage:0 pageCount:10 type:[NSString stringWithFormat:@"%d",self.moduleType]];//刷新只拿最新的
    }
}



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{   
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    _isLoadingMore = NO;
    _isRefreshing = NO;
    _pagenum = 1;
    
    //微博列表
    if (_tableView == nil) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 480-10+(iPhone5?88:0))];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:_tableView];

    }
    
    //下拉加载更多
    if (_refreshHeaderView == nil) {
        
        _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f-250.0, self.view.frame.size.width, 250.0)];
        _refreshHeaderView.delegate = self;
        [self.tableView addSubview:_refreshHeaderView];
        
        [_refreshHeaderView refreshLastUpdatedDate];
        
        if ([self.messages count] == 0) {
            [self performSelector:@selector(autoRefresh) 
                       withObject:nil 
                       afterDelay:0.3];
        }
    }
    
    //上拉加载更多
    if (_loadMoreView == nil) {
        _loadMoreView = [[TKLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
        _loadMoreView.delegate = self;
        if (_hasMore) {
            self.tableView.tableFooterView = _loadMoreView;
        }
    }
    
    
    //微博更新得到条数提示栏
    ZSTTopHintView *hintView = [[ZSTTopHintView alloc] initWithFrame:CGRectMake(0,0,320,34)];
	CGRect hint_frame = hintView.frame;
	hint_frame.origin.y = -37;
	hintView.frame = hint_frame;
	self.topHintView = hintView;	
    [self.view insertSubview:topHintView atIndex:1];
	[hintView release];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (float)heightForRowAtIndex:(NSInteger)index
{ 
    ZSTWeiBoInfo *weiBoInfo = [self.messages objectAtIndex:index];
    if ([weiBoInfo.content length] != 0) {
        CGSize size = [weiBoInfo.content sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:CGSizeMake(245, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
        CGSize attachmentSize = [weiBoInfo.boriginalcontent sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(225, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
        float height = 10 + 20 + 2 + (size.height ? size.height : 0) + (([weiBoInfo.thumbnail_pic length] != 0) ? 90+5 : 0) + (attachmentSize.height ? attachmentSize.height : 0) +  (weiBoInfo.bisforward ? 40 : 10) + 20;
        return height;
    }
    return 0;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    float height = [self heightForRowAtIndex:indexPath.row];
    return height;
}

#pragma mark - UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.messages count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    
    TKCellBackgroundView *custview = [[[TKCellBackgroundView alloc] init] autorelease];
    custview.fillColor = [UIColor colorWithWhite:0.98 alpha:1];
    custview.borderColor = [UIColor colorWithWhite:0.88 alpha:1];
    custview.shadowColor = [UIColor whiteColor];
    
    if(([self.tableView numberOfRowsInSection:indexPath.section]-1) == 0){
        custview.position = CustomCellBackgroundViewPositionSingle;
    }
    else if(indexPath.row == 0){
        custview.position = CustomCellBackgroundViewPositionTop;
    }
    else if (indexPath.row == ([self.tableView numberOfRowsInSection:indexPath.section]-1)){
        custview.position  = CustomCellBackgroundViewPositionBottom;
    }
    else{
        custview.position = CustomCellBackgroundViewPositionMiddle;
    }

    
    static NSString *identifier = @"ZSTWeiBoChatTableViewCell";
    ZSTWeiBoChatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[[ZSTWeiBoChatTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }     

    cell.backgroundView = custview;
    
    ZSTWeiBoInfo *weiBoInfo = [self.messages objectAtIndex:indexPath.row];
    [cell setWeiBoInfo:weiBoInfo];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (![self.sinaWeiboEngine isLoggedIn]) {
    
    ZSTWeiBoInfo *weiboInfo = [self.messages objectAtIndex:indexPath.row];
    self.wbInfo = weiboInfo;
    
    if ([self.sinaWeiboEngine isLoggedIn]) {
            
        ZSTWeiBoMainBodyViewController *mainBody = [[ZSTWeiBoMainBodyViewController alloc] init];
        mainBody.weiBoInfo = weiboInfo;
        mainBody.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:mainBody animated:YES];
        [mainBody release];
    }else{
        
        if ([ZSTF3Preferences shared].SinaWeiBo) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"您还没有设置同步到新浪微博～", nil)
                                                                message:NSLocalizedString(@"现在就去设置?", nil)
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"取消",@"取消")
                                                      otherButtonTitles:NSLocalizedString(@"设置同步", nil), nil];
            
            alertView.tag = set_shareToSina_tag;
            [alertView show];
            [alertView release];
        }else {
              [self.sinaWeiboEngine logInSecondhand];
        }
    }
 
}

//- (void)autoRefresh
//{
//    [_refreshHeaderView autoRefreshOnScroll:self.tableView animated:YES];
//}
//- (void)finishLoading
//{
//    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
//    [_loadMoreView loadMoreScrollViewDataSourceDidFinishedLoading:self.tableView];
//
//    _isLoadingMore = NO;
//    _isRefreshing = NO;
//}
//
//- (void)refreshNewestData {
//    
//    _pagenum = 1;
//    [self.engine getWeiboListSinceID:@"0" orderType:SortType_Desc Size:10 page:_pagenum];
//}
//
//- (void)loadMoreData {
//
//    [self.engine getWeiboListSinceID:[NSString stringWithFormat:@"%lld",((ZSTWeiBoInfo *)[self.messages lastObject]).ID] orderType:SortType_Asc Size:10 page:_pagenum];
//}
//
//- (void)appendData:(NSArray*)dataArray
//{
//    if ( _isRefreshing && [dataArray count] != 0) {
//        [self.messages removeAllObjects];
//    }
//    [self.messages addObjectsFromArray:dataArray];
//    _pagenum ++;
//}


//- (void)engine:(SinaWeibo *)engine parsingSucceededForStatues:(id)result
//{
//    [self.tableView reloadData];
//    [self finishLoading];
//}
//- (void)engine:(SinaWeibo *)engine requestDidFailWithError:(NSError *)error
//{
//    _errorDesc = [[[error localizedDescription] capitalizedString] retain];
//    [self finishLoading];
//
//}
//
//- (void)engine:(SinaWeibo *)engine parsingSucceededForStatue:(BOOL)result
//{
//    NSIndexPath *index = [self.tableView indexPathForSelectedRow];
//    ZSTWeiBoInfo *weiboInfo = [self.messages objectAtIndex:index.row];
//
//
//}

//#pragma mark - UIScrollViewDelegate Methods
//- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
//    
//    if (!_isLoadingMore) {
//        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
//    }
//    if (!_isRefreshing) {
//        [_loadMoreView loadMoreScrollViewDidEndDragging:scrollView];
//    }
//}
//
//- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
//{
//    if (!_isLoadingMore) {
//        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
//    }
//    if (!_isRefreshing) {
//        [_loadMoreView loadMoreScrollViewDidEndDragging:scrollView];
//    }
//}
//
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    if (!_isLoadingMore) {
//        [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
//    }
//    if (!_isRefreshing) {
//        [_loadMoreView loadMoreScrollViewDidScroll:scrollView];
//    }
//}
//#pragma mark EGORefreshTableHeaderDelegate Methods
//- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view {
//    _isRefreshing = YES;
//    [self refreshNewestData];
//}
//
//- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView *)view {
//    return _isRefreshing;
//}
//
//- (NSDate *)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView *)view {
//    return [NSDate date];
//}
//
//#pragma mark HHLoadMoreViewDelegate Methods
//- (void)loadMoreDidTriggerRefresh:(TKLoadMoreView*)view
//{
//    if (_hasMore) {
//        _isLoadingMore = YES;
//        [self loadMoreData];
//    }
//}
//
//- (BOOL)loadMoreDataSourceIsLoading:(TKLoadMoreView*)view
//{
//    return _isLoadingMore;
//}
//
//- (void)fetchDataSuccess:(NSArray *)theData hasMore:(BOOL)hasMore
//{
//    _hasMore = hasMore;
//    if (hasMore) {
//        self.tableView.tableFooterView = _loadMoreView;
//    } else {
//        self.tableView.tableFooterView = nil;
//    }
//
//    
//    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"time" ascending:NO];
//    [(NSMutableArray *)theData sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
//    [sortDescriptor release];
//    
//    
//    [self appendData:theData];
//    
//    [self.tableView reloadData];
//    
//    [self finishLoading];
//}
//
//- (void)fetchDataFailed
//{
//    [self finishLoading];
//}


#pragma mark - ZSTF3EngineWeiBoDelegate-

//- (void)requestDidFail:(ZSTLegacyResponse *)response
//{
//    _errorDesc = [[[response.error localizedDescription] capitalizedString] retain];
//    [self finishLoading];
//    
//}
//- (void)engine:(SinaWeibo *)engine requestDidSucceedWithResult:(id)result
//{
//    if ([[result objectForKey:@"statuses"] count] == 0) {
//        [self fetchDataSuccess:nil hasMore:_hasMore];
//    }
//    
//    [self finishLoading];  
//}
//- (void)getWBlistDidFailed:(NSInteger)code
//{
//    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"网络联接失败", nil) withImage:nil];
//    [self finishLoading];  
//
//}
//- (void)setWBlistResponse:(id)result hasmore:(BOOL)hasmore
//{
//    _hasMore = hasmore;
//    [self fetchDataSuccess:result hasMore:hasmore];
//}

#pragma mark
#pragma UIAlertViewDelegate

//- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
//{
//    if (buttonIndex == 1) {
//        self.navigationController.tabBarController.selectedIndex = [self.navigationController.tabBarController.viewControllers count] - 1;
//    }
//}

#pragma mark
#pragma WBEngineDelegate 
//
//- (void)engine:(SinaWeibo *)sinaEngine didFailToLogInWithError:(NSError *)error
//{
//    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"绑定失败,请重试!", @"") withImage:nil];    
//}
//
//- (void)engineDidLogIn:(SinaWeibo *)sinaEngine
//{
//    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"绑定成功!", nil) withImage:nil]; 
//    [sinaEngine getUserInfo:sinaEngine.userID];
//    
//    ZSTWeiBoMainBodyViewController *mainBody = [[ZSTWeiBoMainBodyViewController alloc] init];
//    mainBody.weiBoInfo = self.wbInfo;
//    mainBody.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:mainBody animated:YES];
//    [mainBody release];
//    
//}
//
//- (void)engineNotAuthorized:(SinaWeibo *)engine
//{
//    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"该应用没有授权", nil) withImage:nil];   
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"screen_name"];
//    [_tableView reloadData];
//    
//}
//- (void)engineAuthorizeExpired:(SinaWeibo *)engine
//{
//    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"该应用授权过期", nil) withImage:nil];
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"screen_name"];
//    [_tableView reloadData];
//    
//}
//
//- (void)engine:(SinaWeibo *)engine parsingSucceededForScreenName:(NSString *)result
//{
//    [[NSUserDefaults standardUserDefaults] setObject:result forKey:@"screen_name"];
//    [_tableView reloadData];    
//}

@end
