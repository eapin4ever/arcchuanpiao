//
//  ZSTGroupChatTableViewCell.m
//  YouYun
//
//  Created by luobin on 5/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTWeiBoChatTableViewCell.h"
#import "TKAsynImageView.h"
#import "ZSTWeiBoInfo.h"
#import "ZSTPictureViewer.h"
#import "ZSTUtils.h"
#import "MTZoomWindow.h"
#define kZSTMessageMaxHeight 600
#define kZSTParagraphSpace 5

#define isRetina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)


@implementation ZSTWeiBoChatTableViewCell

@synthesize avtarImageView;
@synthesize pictureImageView;
@synthesize contentLabel;
@synthesize dateLabel;
@synthesize pictureIcon;
@synthesize rowNumber;
@synthesize name;
@synthesize fowardCountLabel;
@synthesize commentCountLabel;
@synthesize platformLabel;
@synthesize isAttachment;
@synthesize attachmentContentLabel;
@synthesize attachmentPictureImageView;
@synthesize attachmentImageView;
@synthesize isSelect;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.isAttachment = NO;
        self.isSelect = NO;
        
        self.avtarImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(10, 10, 40, 40)] autorelease];
        CALayer * avtarImageViewLayer = [avtarImageView layer];
        [avtarImageViewLayer setMasksToBounds:YES];
        [avtarImageViewLayer setCornerRadius:5.0];
        self.avtarImageView.defaultImage = ZSTModuleImage(@"module_weiboa_avatar_default.png");
        [self addSubview:avtarImageView];
    
        self.name = [[[UILabel alloc] init] autorelease];
        self.name.backgroundColor = [UIColor clearColor];
        self.name.font = [UIFont systemFontOfSize:12];
        self.name.textAlignment = NSTextAlignmentLeft;
        [self addSubview:self.name];
        
        self.dateLabel = [[[UILabel alloc] init] autorelease];
        self.dateLabel.backgroundColor = [UIColor clearColor];
        self.dateLabel.textAlignment = NSTextAlignmentLeft;
        self.dateLabel.font = [UIFont systemFontOfSize:10];
        self.dateLabel.textColor = [UIColor colorWithWhite:0.7 alpha:1];
        self.dateLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:1];
        self.dateLabel.shadowOffset = CGSizeMake(0, 1.0f);
        [self addSubview:self.dateLabel];
        
//        self.pictureIcon = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"timeline_image_icon.png"]] autorelease];
//        [self.dateLabel addSubview:pictureIcon];
        
        self.contentLabel = [[[UILabel alloc] init] autorelease];
        self.contentLabel.numberOfLines = 0;
        self.contentLabel.backgroundColor = [UIColor clearColor];
        self.contentLabel.font = [UIFont systemFontOfSize:13];
        self.contentLabel.textColor = [UIColor colorWithWhite:0.06 alpha:1];
        self.contentLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:1];
        self.contentLabel.shadowOffset = CGSizeMake(0, 1.0f);
        [self addSubview:self.contentLabel];
        
        self.pictureImageView = [[[TKAsynImageView alloc] init] autorelease];
//        [self.pictureImageView addTarget:self action:@selector(showImage:) forControlEvents:UIControlEventTouchUpInside];
        self.pictureImageView.backgroundColor = [UIColor clearColor];
        self.pictureImageView.asynImageDelegate = self;
        self.pictureImageView.showLoadingWheel = YES;
        self.pictureImageView.defaultImage = ZSTModuleImage(@"module_weiboa_messages_photo_default_image.png");
        self.pictureImageView.asynImageDelegate = self;
        self.pictureImageView.imageView.zoomDelegate = self;
        [self addSubview:self.pictureImageView];
        
//        self.platformLabel = [[[UILabel alloc] init] autorelease];
//        self.platformLabel.font = [UIFont systemFontOfSize:10];
//        self.platformLabel.textColor = [UIColor grayColor];
//        self.platformLabel.backgroundColor = [UIColor clearColor];
//        self.platformLabel.textAlignment = NSTextAlignmentRight;
//        [self addSubview:self.platformLabel];
        

        self.fowardCountLabel = [[[UIImageView alloc] init] autorelease];
        self.fowardCountLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:self.fowardCountLabel];

        
        self.commentCountLabel = [[[UIImageView alloc] init] autorelease];
        self.commentCountLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:self.commentCountLabel];
        
        self.attachmentImageView = [[[UIImageView alloc] init] autorelease];
        self.attachmentImageView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.attachmentImageView];
        
        self.attachmentContentLabel = [[[UILabel alloc] init] autorelease];
        self.attachmentContentLabel.numberOfLines = 0;
        self.attachmentContentLabel.backgroundColor = [UIColor clearColor];
        self.attachmentContentLabel.font = [UIFont systemFontOfSize:12];
        self.attachmentContentLabel.textColor = [UIColor colorWithWhite:0.06 alpha:1];
        self.attachmentContentLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:1];
        self.attachmentContentLabel.shadowOffset = CGSizeMake(0, 1.0f);
        [self addSubview:self.attachmentContentLabel];
        
        self.attachmentPictureImageView = [[[TKAsynImageView alloc] init] autorelease];
//        [self.attachmentPictureImageView addTarget:self action:@selector(showImage:) forControlEvents:UIControlEventTouchUpInside];
        self.attachmentPictureImageView.backgroundColor = [UIColor clearColor];
        self.attachmentPictureImageView.defaultImage = ZSTModuleImage(@"module_weiboa_messages_photo_default_image.png");
        self.attachmentPictureImageView.asynImageDelegate = self;
        self.attachmentPictureImageView.imageView.zoomDelegate = self;
        self.attachmentPictureImageView.callbackOnSetImage = self;

        [self addSubview:self.attachmentPictureImageView];

}
    return self;
}

- (CGSize)displayRectForImage:(TKAsynImageView *)sender isScreenScale:(BOOL)scale
{
    CGSize imageSize = sender.imageView.image.size;

    CGRect screenRect ;
    
    if (scale) {
        screenRect = TKScreenBounds();
        if (imageSize.width/ imageSize.height >= screenRect.size.width/ screenRect.size.height) {
            CGFloat height = screenRect.size.width * imageSize.height / imageSize.width;
            return CGSizeMake(screenRect.size.width, height);
        } else {
            CGFloat width = screenRect.size.height * imageSize.width / imageSize.height;
            return CGSizeMake(width, screenRect.size.height);
        }
    }else {
        screenRect = CGRectMake(0, 0, 80, 90);
        
        if (imageSize.width > screenRect.size.width) {
            CGFloat height = imageSize.height * screenRect.size.width / imageSize.width;
            CGFloat width = screenRect.size.width;
            if (height > screenRect.size.height) {
                width = width * screenRect.size.height/height;
                height = screenRect.size.height;
            }
            return CGSizeMake(width, height);
        }else if (imageSize.height > screenRect.size.height) {
            CGFloat width = imageSize.width * screenRect.size.height / imageSize.height;
            CGFloat height = screenRect.size.height;
            
            return CGSizeMake(width, height);
        } else {
            return CGSizeMake(imageSize.width, imageSize.height); 
        }
    }
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
}

- (UIImage *)createImage:(UIImage *)image withTitle:(NSString *)title size:(CGSize)targetSize
{
   CGSize CurrentTargetSize = CGSizeMake(targetSize.width + 13, targetSize.height);
    
    UIGraphicsBeginImageContextWithOptions(CurrentTargetSize, NO, [UIScreen mainScreen].scale);
    
    CGContextRef context =  UIGraphicsGetCurrentContext();

    [image drawInRect:CGRectMake(0, 0, 12, 12)];

    CGContextSetFillColorWithColor(context, [UIColor grayColor].CGColor);
    [title drawInRect:CGRectMake(13, 0, targetSize.width, 12) withFont:[UIFont systemFontOfSize:10]];
    
    if (isRetina) {
        CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    }

    UIImage* resultImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();

    return resultImage;
}


- (void)setWeiBoInfo:(ZSTWeiBoInfo *)weiBoInfo
{
    isSelect = NO;

    self.name.text = weiBoInfo.name;
    
    [self.avtarImageView clear];
    self.avtarImageView.url = [NSURL URLWithString:weiBoInfo.avatarString];
    [self.avtarImageView loadImage];
    
    self.dateLabel.text = [ZSTUtils convertToTime:[NSString stringWithFormat:@"%f",[[weiBoInfo.time dateByAddingTimeInterval:-8*60*60]timeIntervalSince1970]]];

    self.contentLabel.text = weiBoInfo.content;
    self.platformLabel.text = [NSString stringWithFormat:NSLocalizedString(@"来自%@", nil),weiBoInfo.Platform];
    
    CGSize fowardSize = [weiBoInfo.forwardCount sizeWithFont:[UIFont systemFontOfSize:10] constrainedToSize:CGSizeMake(80, 10) lineBreakMode:UILineBreakModeTailTruncation];
    CGSize commentSize = [weiBoInfo.commentCount sizeWithFont:[UIFont systemFontOfSize:10] constrainedToSize:CGSizeMake(80, 10) lineBreakMode:UILineBreakModeTailTruncation];
    
    if ([weiBoInfo.forwardCount isEqualToString:@"0"]) {
        self.fowardCountLabel.image = nil;
    }else{
        self.fowardCountLabel.image =[self createImage:ZSTModuleImage(@"module_weiboa_timeline_retweet_count_icon.png") withTitle:weiBoInfo.forwardCount size:CGSizeMake(fowardSize.width , 12)]; 
    }
    
    if ([weiBoInfo.commentCount isEqualToString:@"0"]) {
        self.commentCountLabel.image = nil;
    }else{
        self.commentCountLabel.image = [self createImage:ZSTModuleImage(@"module_weiboa_timeline_comment_count_icon.png") withTitle:weiBoInfo.commentCount size:CGSizeMake(commentSize.width , 12)];  
    }
        
    self.pictureImageView.url = nil;
    self.attachmentContentLabel.text = nil;
    self.attachmentPictureImageView.url = nil;
    self.isAttachment = NO;
    self.pictureImageView.defaultImage = ZSTModuleImage(@"module_weiboa_messages_photo_default_image.png");
    self.attachmentPictureImageView.defaultImage = ZSTModuleImage(@"module_weiboa_messages_photo_default_image.png");
    
    if (weiBoInfo.bisforward) {
        
        self.attachmentContentLabel.text = weiBoInfo.boriginalcontent;
        [self.attachmentPictureImageView clear];
        self.attachmentPictureImageView.bmiddle_pic_url = nil;
        self.attachmentPictureImageView.original_pic_url = nil;
        if (weiBoInfo.thumbnail_pic != nil && [weiBoInfo.thumbnail_pic length] != 0) {
            self.attachmentPictureImageView.url = [NSURL URLWithString:weiBoInfo.thumbnail_pic];
            self.attachmentPictureImageView.bmiddle_pic_url = [NSURL URLWithString:weiBoInfo.bmiddle_pic];
            self.attachmentPictureImageView.original_pic_url = [NSURL URLWithString:weiBoInfo.original_pic];
            [self.attachmentPictureImageView loadImage];
        }

        self.isAttachment = YES;  
    }else{
        [self.pictureImageView clear];
        self.pictureImageView.bmiddle_pic_url = nil;
        self.pictureImageView.original_pic_url = nil;
        if (weiBoInfo.thumbnail_pic != nil && [weiBoInfo.thumbnail_pic length] != 0) {
            self.pictureImageView.url = [NSURL URLWithString:weiBoInfo.thumbnail_pic];
            self.pictureImageView.bmiddle_pic_url = [NSURL URLWithString:weiBoInfo.bmiddle_pic];
            self.pictureImageView.original_pic_url = [NSURL URLWithString:weiBoInfo.original_pic];
            [self.pictureImageView loadImage];
        }
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    float height = 0;
    
    self.name.frame = CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 10, 10, 150, 20);
    self.platformLabel.frame = CGRectMake(CGRectGetMaxX(self.name.frame) , 10, 100, 20);

    if (self.pictureImageView.url != nil || self.isAttachment) {
        CGSize dateSize = [self.dateLabel.text sizeWithFont:[UIFont systemFontOfSize:10] constrainedToSize:CGSizeMake(95, 20) lineBreakMode:UILineBreakModeTailTruncation];
        self.pictureIcon.frame = CGRectMake(self.dateLabel.frame.size.width - dateSize.width - 20, 2.5, 15, 15);
    }else{
        self.pictureIcon.frame = CGRectZero;
    }
    
    height = CGRectGetMaxY(self.name.frame);
    
    CGSize size = [self.contentLabel.text sizeWithFont:self.contentLabel.font constrainedToSize:CGSizeMake(245, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
    if (![self.contentLabel.text isEqualToString:@""] && self.contentLabel.text != nil && [self.contentLabel.text length] != 0) {
        self.contentLabel.frame = CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 10, height + 2, 245, size.height);
        height = CGRectGetMaxY(self.contentLabel.frame);
    }else{
        self.contentLabel.frame = CGRectZero;
    }
    
    if (self.pictureImageView.url != nil ) 
    {
        float imageWidth = self.pictureImageView.imageView.size.width;
        float imageHeight = self.pictureImageView.imageView.size.height;
        
        imageWidth = imageWidth ? imageWidth : 80;
        imageHeight = imageHeight ? imageHeight:90;
        
        imageWidth = (imageWidth > 80) ? 80 : imageWidth;
        imageHeight = (imageHeight > 90) ? 90 : imageHeight;
        
        float marginX = (80 - imageWidth)/4;
        float marginY = (90 - imageHeight)/2;
        
        self.pictureImageView.frame = CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 25 + marginX,  height + kZSTParagraphSpace + marginY, imageWidth, imageHeight);
        height += 95;
    }else {
        self.pictureImageView.frame = CGRectZero;
    }  
    
    float attachmentTop = height;

    CGSize attachmentContentSize = [self.attachmentContentLabel.text sizeWithFont:self.attachmentContentLabel.font constrainedToSize:CGSizeMake(225, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];

    if (![self.attachmentContentLabel.text isEqualToString:@""] && self.attachmentContentLabel.text != nil && [self.attachmentContentLabel.text length] != 0) {
        self.attachmentContentLabel.frame = CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 20, height + 20, 225, attachmentContentSize.height);
        height = CGRectGetMaxY(self.attachmentContentLabel.frame);
    }else{
        self.attachmentContentLabel.frame = CGRectZero;
    }
    
    if (self.attachmentPictureImageView.url != nil ) 
    { 
        float imageWidth = self.attachmentPictureImageView.imageView.size.width;
        float imageHeight = self.attachmentPictureImageView.imageView.size.height;
        
        imageWidth = imageWidth ? imageWidth : 80;
        imageHeight = imageHeight ? imageHeight:90;

        imageWidth = (imageWidth > 80) ? 80 : imageWidth;
        imageHeight = (imageHeight > 90) ? 90 : imageHeight;

        float marginX = (80 - imageWidth)/4;
        float marginY = (90 - imageHeight)/2;
        
        self.attachmentPictureImageView.frame = CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 35 + marginX,  height + kZSTParagraphSpace + marginY, imageWidth, imageHeight);
        height += 95 ;
    }else 
    {
        self.attachmentPictureImageView.frame = CGRectZero;
    } 
    
    if (self.isAttachment) {
        self.attachmentImageView.frame = CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 10, attachmentTop + 10, 245, height - attachmentTop);
        self.attachmentImageView.image = [ZSTModuleImage(@"module_weiboa_chat_top_arrow.png") stretchableImageWithLeftCapWidth:18 topCapHeight:12]; 
    }else{
        self.attachmentImageView.frame  = CGRectZero;
    }
    
    self.dateLabel.frame =  CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 10, height + (self.isAttachment ? 20 : 10), 95, 10);

    if (self.commentCountLabel.image != nil) {
        self.commentCountLabel.frame = CGRectMake(320 - self.commentCountLabel.image.size.width - 10,height + (self.isAttachment ? 20 : 10), self.commentCountLabel.image.size.width, 10);
    }
    if (self.fowardCountLabel.image != nil) {
        self.fowardCountLabel.frame = CGRectMake(320 - self.commentCountLabel.image.size.width - self.fowardCountLabel.image.size.width - 15, height + (self.isAttachment ? 20 : 10), self.fowardCountLabel.image.size.width, 10);
    }

 }


-(void) managedImageSet:(HJManagedImageV*)mi
{
    if (!isSelect) {
        mi.imageView.size = [self displayRectForImage:(TKAsynImageView *)mi isScreenScale:NO];
    }
    isSelect = NO;
}

  #pragma mark HJManagedImageVDelegate

- (void)zoomWindow:(MTZoomWindow *)zoomWindow didZoomOutView:(UIView *)view {

}

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{    
    isSelect = YES;

    asynImageView.imageView.zoomedSize = [self displayRectForImage:attachmentPictureImageView isScreenScale:YES];
    asynImageView.defaultImage = asynImageView.imageView.image;

    asynImageView.imageView.wrapInScrollviewWhenZoomed = YES;
    [asynImageView clear];
    asynImageView.url = asynImageView.bmiddle_pic_url;
    asynImageView.showLoadingWheel = YES;
    [asynImageView loadImage];
    [asynImageView.imageView zoomIn];
}

- (void)dealloc
{
    self.avtarImageView = nil;
    self.pictureImageView = nil;
    self.contentLabel = nil;
    self.dateLabel = nil;
    self.name = nil;
    self.platformLabel = nil;
    self.fowardCountLabel = nil;
    self.commentCountLabel = nil;
    self.attachmentContentLabel = nil;
    self.attachmentPictureImageView = nil;
    self.attachmentImageView = nil;
    
    [super dealloc];
}

@end
