//
//  TopHintView.h
//  Weibo
//
//  Created by China on 09-12-1.
//  Copyright 2009 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ZSTTopHintView : UIView {
	UILabel *hintLabel;
	UIImageView *bgImage;
}

@property (nonatomic,retain) UILabel *hintLabel;
@property (nonatomic,retain) UIImageView *bgImage;
		   
-(void)showHint:(NSString*)msg count:(int)count;
-(void)hideHint;

@end
