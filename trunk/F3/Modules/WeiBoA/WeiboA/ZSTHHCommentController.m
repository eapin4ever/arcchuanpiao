//
//  HHCommentController.m
//  HHelloCat
//
//  Created by luo bin on 12-3-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTHHCommentController.h"
#import "DSActivityView.h"
#import "TKUIUtil.h"

#define kMaxCommentLength 140

@implementation ZSTHHCommentController

@synthesize textView;
@synthesize countLabel;
@synthesize backgroundView;
@synthesize delegate;
@synthesize ID;
@synthesize CID;
//@synthesize engine;
@synthesize isForward;
@synthesize isComment;
@synthesize isReply;
@synthesize forwardString;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
//    [HHUIUtil hiddenHUD];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [DSBezelActivityView removeViewAnimated:NO];
    self.ID = nil;
    self.backgroundView = nil;
    self.countLabel = nil;
    self.textView = nil;
//    self.engine = nil;
    [super dealloc];
}

#pragma mark －UITextViewDelegate

//- (void)textViewDidChangeSelection:(UITextView *)textView
//{
//    NSRange range;
//    range.location = 0;
//    range.length = 0;
//    self.textView.selectedRange = range;
//}

#pragma mark - View lifecycle



- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed: @"framework_top_bg.png"];

    self.view.backgroundColor = [UIColor whiteColor];
    
    if (isComment) {
        self.navigationItem.title = NSLocalizedString(@"发表评论", nil);
    }else if (isForward){
        self.navigationItem.title = NSLocalizedString(@"转发微博", nil);
    }else if (isReply){
        self.navigationItem.title = NSLocalizedString(@"回复微博", nil);
    }

    self.navigationItem.leftBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"返回", @"返回") target:self selector:@selector (dismiss)];
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"发送", @"发送") target:self selector:@selector (sendButtonAction:)];
    UIButton *sendButton = (UIButton *)self.navigationItem.rightBarButtonItem.customView;
    sendButton.enabled = NO;

    self.backgroundView = [[[UIImageView alloc] initWithFrame:CGRectMake(0.0, 148.0, 320.0, 40.0)] autorelease];//24
    self.backgroundView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.backgroundView.backgroundColor = [UIColor clearColor];
    self.backgroundView.image = ZSTModuleImage(@"bottom_tool_bar.png");
    CALayer *layer = self.backgroundView.layer;
    layer.shadowOffset = CGSizeMake(0, 3); 
    layer.shadowRadius = 5.0; 
    layer.shadowColor = [UIColor blackColor].CGColor; 
    layer.shadowOpacity = 0.8; 
    
    self.textView = [[[TKPlaceHolderTextView alloc] initWithFrame:CGRectMake(1, 15, 318.0, 170.0)] autorelease];//25
    self.textView.font = [UIFont systemFontOfSize:16];
    self.textView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.textView.clearsContextBeforeDrawing = YES;
    self.textView.clipsToBounds = YES;
    self.textView.contentMode = UIViewContentModeScaleToFill;
    self.textView.text = @"";
    self.textView.delegate = self;
    self.textView.textAlignment = NSTextAlignmentLeft;

    if ([self.forwardString isEqualToString:@""] || [self.forwardString length] == 0 || self.forwardString == nil) {
        [self.textView setPlaceholder:NSLocalizedString(@"说点什么", @"")];
    }else{
        self.textView.text = self.forwardString;
    }
    [self.textView becomeFirstResponder];
    self.textView.selectedRange = NSMakeRange(0, 0);
    
    countLabel = [[UILabel alloc] initWithFrame:CGRectMake(260.0, 10.0, 48.0, 21.0)];//26
    countLabel.adjustsFontSizeToFitWidth = YES;
    countLabel.autoresizesSubviews = YES;
    countLabel.backgroundColor = [UIColor clearColor];
    countLabel.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    countLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    countLabel.contentMode = UIViewContentModeLeft;
    countLabel.font = [UIFont systemFontOfSize:14];
    countLabel.minimumFontSize = 10.000;
    countLabel.shadowOffset = CGSizeMake(0.0, -1.0);
    countLabel.text = @"0/140";
    countLabel.textAlignment = NSTextAlignmentRight;
    
    [self.view addSubview:self.textView];
    [self.backgroundView addSubview:countLabel];
    [self.view addSubview:self.backgroundView];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardWillShowNotification object:nil]; 
    
}

- (void) keyboardWasShown:(NSNotification *) notif{ 
    NSDictionary *info = [notif userInfo]; 
    NSValue *value = [info objectForKey:UIKeyboardFrameEndUserInfoKey]; 
    CGSize keyboardSize = [value CGRectValue].size; 
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    self.backgroundView.frame = CGRectMake(0.0, 148.0 - (keyboardSize.height-216), 320.0, 40.0);//24
    self.textView.frame = CGRectMake(1, 15, 318.0, 170 - (keyboardSize.height-216));//25
    [UIView commitAnimations];
}


- (void)dismiss
{
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([self.delegate respondsToSelector:@selector(commentControllerDidCancel:)]) {
        [self.delegate commentControllerDidCancel:self];
    }
}

- (void)viewDidUnload
{
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

//////////////////////////////////////////////////////////////////////////////////////

#pragma mark - ButtonAction

- (void)sendButtonAction:(id)sender
{
    [DSBezelActivityView newActivityViewForView:self.view withLabel:NSLocalizedString(@"发送中", @"")];
    
    if (self.isComment) {
        [self.sinaWeiboEngine sendCommentComment:textView.text ID:self.ID];
    }else if (self.isForward){
        [self.sinaWeiboEngine sendReportMessage:textView.text ID:self.ID];
    }else if(self.isReply){
        [self.sinaWeiboEngine sendReplyMessage:textView.text ID:self.ID CID:self.CID];
    }
}

//////////////////////////////////////////////////////////////////////////////////////

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)theTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *comment = [theTextView.text stringByReplacingCharactersInRange:range withString:text];
    NSUInteger length = [comment length];
    
    UIButton *sendButton = (UIButton *)self.navigationItem.rightBarButtonItem.customView;
    sendButton.enabled = (length != 0);
    
    if (length <= kMaxCommentLength) {
        self.countLabel.text = [NSString stringWithFormat:@"%d/%d", length, kMaxCommentLength];
        return YES;
    } else {
        return NO;
    }
}

//////////////////////////////////////////////////////////////////////////////////////
#pragma mark - ZSTLegicyCommunicatorDelegate

- (void)sendMessageResponse
{
    [self performSelector:@selector(dosSendMessageResponse) withObject:nil afterDelay:1];
}

- (void)dosSendMessageResponse
{
    [DSBezelActivityView removeViewAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([self.delegate respondsToSelector:@selector(commentControllerDidFinish:)]) {
        [self.delegate commentControllerDidFinish:self];
    }
}

- (void)requestDidFail:(NSError *)error method:(NSString *)method
{
    [DSBezelActivityView removeViewAnimated:YES];
}

#pragma -WBEngineDelegate---
- (void)engine:(SinaWeibo *)engine requestDidSucceedWithResult:(id)result
{
    [self sendMessageResponse];
}
- (void)engine:(SinaWeibo *)engine requestDidFailWithError:(NSError *)error
{
     [self dismiss];
    if (self.isComment) {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"评论失败", nil) withImage:nil];    
    }else if (self.isForward){
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"转发失败", nil) withImage:nil];    
    }
}
@end
