//
//  ActionSheetHint.m
//  Weibo
//
//  Created by China on 10-1-25.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ZSTActionSheetHint.h"

@implementation ZSTActionSheetHint

@synthesize twitterEngine;
@synthesize canDelegate;

+ (id) hintWithTitle:(NSString *)hint delegate:(id <UIActionSheetDelegate>)delegate shouldShowCancel:(BOOL)shouldShowCancel engine:(SinaWeibo *)engine parentView:(UIView *)parentView {
	ZSTActionSheetHint *sheet;
	if (sheet = [[ZSTActionSheetHint alloc] initWithTitle:hint delegate:delegate cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil]) {
		
		UIActivityIndicatorView* progressInd = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(-18.5, -9.0, 40.0, 40.0)];
		progressInd.tag = 1001;
		progressInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
		[progressInd sizeToFit];
		progressInd.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin |
										UIViewAutoresizingFlexibleRightMargin |
										UIViewAutoresizingFlexibleTopMargin |
										UIViewAutoresizingFlexibleBottomMargin);
		[progressInd startAnimating];
		[sheet addSubview:progressInd];
		[progressInd autorelease];
		if (shouldShowCancel) {
			UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
			cancelButton.tag = 1002;
			cancelButton.titleLabel.font = [UIFont systemFontOfSize:13];
			[cancelButton setTitle:NSLocalizedString(@"取消",@"取消") forState:UIControlStateNormal];
			[cancelButton setBackgroundImage:ZSTModuleImage(@"module_weiboa_cancel_action.png") forState:UIControlStateNormal];
			cancelButton.frame = CGRectMake(20,10,51,28);
			[cancelButton addTarget:sheet action:@selector(onCancelSheet:) forControlEvents:UIControlEventTouchUpInside];
			[sheet addSubview:cancelButton];
		}
		
		[sheet showInView:parentView]; 
		sheet.twitterEngine = engine;
		
		[sheet autorelease];
	}
	return sheet;
}

- (void) onCancelSheet:(id)sender {
	if (self.twitterEngine)
		[self.twitterEngine closeAllConnections];
	if([self.delegate respondsToSelector:@selector(ActionSheetHintCancel)] ){
		[canDelegate ActionSheetHintCancel];
	}
	[self dismissWithClickedButtonIndex:0 animated:YES];
}

/*
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
}
*/


- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [super dealloc];
}


@end
