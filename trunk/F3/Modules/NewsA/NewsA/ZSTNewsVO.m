//
//  NewsVO.m
//  F3Engine
//
//  Created by admin on 12-7-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTNewsVO.h"

@implementation ZSTNewsVO

@synthesize title;
@synthesize summary;
@synthesize iconFileID;
@synthesize iconType;
@synthesize addTime;
@synthesize source;
@synthesize categoryID;
@synthesize msgID;
@synthesize msgType;
@synthesize orderNum;
@synthesize isRead;

+ (id)voWithDic:(NSDictionary *)dic
{
    return [[[self alloc] initWithDic: dic] autorelease];
}

- (id)initWithDic:(NSDictionary *)dic
{
    if( (self=[super init])) {
        self.title =  [dic safeObjectForKey:@"Title"];
        self.summary = [dic safeObjectForKey:@"Summary"] ? [dic safeObjectForKey:@"Summary"]: @"";
        self.iconFileID = [dic safeObjectForKey:@"IConFileID"];
        self.iconType = [dic safeObjectForKey:@"IConType"];
        self.addTime = [dic safeObjectForKey:@"AddTime"];
        self.source = [dic safeObjectForKey:@"Source"];
        self.categoryID = [dic safeObjectForKey:@"CategoryID"];
        self.msgID = [dic safeObjectForKey:@"MsgID"];
        self.msgType = [dic safeObjectForKey:@"MsgType"];
        self.orderNum = [dic safeObjectForKey:@"OrderNum"];
        self.isRead = [[dic safeObjectForKey:@"IsRead"] boolValue];
    }
    
    return self;
}

- (void)dealloc
{
    self.title = nil;
    self.summary = nil;
    self.iconFileID = nil;
    self.iconType = nil;
    self.addTime = nil;
    self.source = nil;
    self.categoryID = nil;
    self.msgID = nil;
    self.msgType = nil;
    self.orderNum = nil;

    [super dealloc];
}

@end
