//
//  NewsContentVO.m
//  News
//
//  Created by admin on 12-7-25.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTNewsContentVO.h"

@implementation ZSTNewsContentVO

@synthesize title;//主标题
@synthesize content;
@synthesize fileArr;
@synthesize fileId;
@synthesize fileType;
@synthesize addTime;//日期
@synthesize source;//来源
@synthesize notice;
@synthesize result;
@synthesize shareurl;


+ (id)voWithDic:(NSDictionary *)dic
{
    return [[[self alloc] initWithDic: dic] autorelease];
}

- (id)initWithDic:(NSDictionary *)dic
{
    if( (self=[super init])) {
        self.title =  [dic safeObjectForKey:@"Title"];
        self.content = [dic safeObjectForKey:@"Content"];
        self.fileArr = [dic safeObjectForKey:@"File"];
        self.notice = [dic safeObjectForKey:@"Notice"];
        self.addTime = [dic safeObjectForKey:@"AddTime"];
        self.source = [dic safeObjectForKey:@"Source"];
        self.result = [dic safeObjectForKey:@"Result"];
        self.shareurl = [dic safeObjectForKey:@"ShareUrl"];
    }
    
    return self;
}

-(NSNumber*)getFileId {
    return  [dict safeObjectForKey:@"FileID"];
} 

-(NSNumber*)getFileType {
    return  [dict safeObjectForKey:@"FileType"];
} 

@end
