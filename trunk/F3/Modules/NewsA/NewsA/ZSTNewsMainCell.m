
//
//  NewsMainCell.m
//  F3
//
//  Created by admin on 12-7-17.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTNewsMainCell.h"
#import "ZSTGlobal+NewsA.h"
#import "ZSTDao+NewsA.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ZSTNewsMainCell

@synthesize cellImageView;
@synthesize title;
@synthesize subTitle;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier 
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) 
    {
        self.frame = CGRectMake(0, 0, 320, 80);
//        cellImageView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(5, (self.frame.size.height*0.22)/2, 80, 60)];
        cellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, (self.frame.size.height*0.22)/2, 80, 60)];
//        cellImageView.adorn = [[UIImage imageNamed:@"Module.bundle/module_newsa_list_default_img.png"] stretchableImageWithLeftCapWidth:40 topCapHeight:30];
//        cellImageView.imageInset = UIEdgeInsetsMake(2.5, 2.5, 3, 2.5);
//        cellImageView.adjustsImageWhenHighlighted = NO;
        [self.contentView addSubview:cellImageView];
        
        
        title = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellImageView.frame)+10, (self.frame.size.height*0.20)/2, self.frame.size.width - CGRectGetMaxX(cellImageView.frame)-35, CGRectGetHeight(cellImageView.frame)*0.5)];
        title.backgroundColor = [UIColor clearColor];
        title.textColor = [UIColor blackColor];
        title.font = [UIFont systemFontOfSize:15];
        title.text =  @"";
        title.highlightedTextColor = [UIColor whiteColor];
        title.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:title];
        
        subTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellImageView.frame)+10, CGRectGetMaxY(title.frame), self.frame.size.width - CGRectGetMaxX(cellImageView.frame)-35, CGRectGetHeight(cellImageView.frame)*0.5)];
        subTitle.text = @"";
        subTitle.font = [UIFont systemFontOfSize:11];
        subTitle.highlightedTextColor = [UIColor whiteColor];
        subTitle.textAlignment = NSTextAlignmentLeft;
        subTitle.numberOfLines = 0;
        subTitle.textColor = [UIColor grayColor];
        subTitle.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:subTitle];
        
        //        self.contentView.transform = CGAffineTransformMakeRotation(M_PI / 2);// 顺时转90
    }
    return self;
}

- (void)configCell:(ZSTNewsVO*)nvo cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    title.text = nvo.title;

    title.textColor = [self.viewController.dao getNewsIsRead:[nvo.msgID integerValue]] ?[UIColor grayColor]:[UIColor blackColor];
    newsId = [nvo.msgID integerValue];
    subTitle.text = nvo.summary;
//    [cellImageView clear];
//    cellImageView.url = [NSURL URLWithString:[GetNewsFile stringByAppendingString:[NSString stringWithFormat:@"?FileID=%@",nvo.iconFileID]]];
//    [cellImageView loadImage];
    [cellImageView setImageWithURL:[NSURL URLWithString:[GetNewsFile stringByAppendingString:[NSString stringWithFormat:@"?FileID=%@",nvo.iconFileID]]]];
}

- (void)didMoveToSuperview
{
    [super didMoveToSuperview];

    title.textColor = [self.viewController.dao getNewsIsRead:newsId] ?[UIColor grayColor]:[UIColor blackColor];
}


- (void)dealloc
{
    self.cellImageView = nil;
    self.title = nil;
    self.subTitle = nil;
    [super dealloc];
}

@end



@implementation ZSTNewsCarouselViewCell

@synthesize carouselView;
@synthesize dataArry;
@synthesize carouselViewCellDelegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier 
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) 
    {
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //轮播图
        carouselView = [[ZSTNewsCarouselView alloc] initWithFrame:CGRectMake(0, 0, 320, 160)];
        carouselView.carouselDataSource = self;
        carouselView.carouselDelegate = self;
        [self.contentView addSubview:carouselView];
    }
    return self;
}
- (void)configCell:(NSArray*)array
{
    [dataArry release];
    self.dataArry = nil;
    self.dataArry = [array retain];
    
    if ([dataArry count] != 0) {
        [carouselView reloadData];
    }
}

#pragma mark - ZSTNewsCarouselViewDataSource

- (NSInteger)numberOfViewsInCarouselView:(ZSTNewsCarouselView *)newsCarouselView
{
    return [dataArry count];
}
- (NSDictionary *)carouselView:(ZSTNewsCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index
{
    if ([dataArry count] != 0) {
        return [dataArry objectAtIndex:index];
    }
    return nil;
}

#pragma mark - ZSTNewsCarouselViewDelegate

- (void)carouselView:(ZSTNewsCarouselView *)theCarouselView didSelectedViewAtIndex:(NSInteger)index
{
    if ([carouselViewCellDelegate respondsToSelector:@selector(carouselViewCell:didSelectedViewAtIndex:)]) {
        [carouselViewCellDelegate carouselViewCell:theCarouselView didSelectedViewAtIndex:index];
    }
}

- (void)dealloc
{
    [super dealloc];
}
@end
