//
//  NewsContentVO.h
//  News
//
//  Created by admin on 12-7-25.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTNewsContentVO : NSObject{
    NSDictionary *dict;
}

@property (nonatomic, retain) NSString *title;//主标题
@property (nonatomic, retain) NSString *content;
@property (nonatomic, readonly, getter = getFileId) NSNumber *fileId;
@property (nonatomic, readonly, getter = getFileType) NSNumber *fileType;
@property (nonatomic, retain) NSArray *fileArr;
@property (nonatomic, retain) NSString *addTime;//日期
@property (nonatomic, retain) NSString *source;//来源
@property (nonatomic, retain) NSString *notice;
@property (nonatomic, retain) NSNumber *result;
@property (nonatomic, retain) NSString *shareurl;


+ (id)voWithDic:(NSDictionary *)dic;
- (id)initWithDic:(NSDictionary *)dic;

@end
