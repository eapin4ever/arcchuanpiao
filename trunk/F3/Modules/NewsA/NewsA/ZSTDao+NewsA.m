//
//  ZSTDao.m
//  News
//  
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTDao+NewsA.h"
#import "ZSTSqlManager.h"
#import "ZSTUtils.h"
#import "TKUtil.h"
#import "ZSTGlobal+NewsA.h"

#define CREATE_TABLE_CMD(moduleType) [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [Z_News_%@] (\
                                                                    ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                    MsgID INTEGER DEFAULT 0 , \
                                                                    CategoryID INTEGER DEFAULT 0, \
                                                                    MsgType INTEGER DEFAULT 0,\
                                                                    Title VARCHAR DEFAULT '',\
                                                                    Summary VARCHAR DEFAULT '', \
                                                                    IConFileID VARCHAR DEFAULT '', \
                                                                    AddTime double DEFAULT (getdate()), \
                                                                    IConType INTEGER DEFAULT 0, \
                                                                    OrderNum INTEGER  DEFAULT 0, \
                                                                    Source VARCHAR  DEFAULT '',\
                                                                    IsRead INTEGER  DEFAULT 0, \
                                                                    UNIQUE(MsgID, CategoryID)\
                                                                    );\
                        CREATE TABLE IF NOT EXISTS [Z_NewsCategory_%@] (\
                                                                    ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                    CategoryID INTEGER DEFAULT 0 UNIQUE, \
                                                                    CategoryName VARCHAR DEFAULT '', \
                                                                    OrderNum INTEGER  DEFAULT 0, \
                                                                    DefaultFlag INTEGER  DEFAULT 0, \
                                                                    IndexFlag INTEGER  DEFAULT 0\
                                                                    );", @(moduleType), @(moduleType)]

TK_FIX_CATEGORY_BUG(NewsA)

@implementation ZSTDao(NewsA)

- (void)createTableIfNotExistForInfoaModule
{
    [ZSTSqlManager executeSqlWithSqlStrings:CREATE_TABLE_CMD(self.moduleType)];
}

- (BOOL)addCategory:(NSInteger)categoryID
       categoryName:(NSString *)categoryName
           orderNum:(NSInteger)orderNum
        defaultFlag:(BOOL)defaultFlag
{
    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO Z_NewsCategory_%ld (CategoryID, CategoryName, OrderNum, DefaultFlag) VALUES (?,?,?,?)", (long)self.moduleType], 
                    [NSNumber numberWithInteger:categoryID],
                    [TKUtil wrapNilObject:categoryName], 
                    [NSNumber numberWithInteger:orderNum], 
                    [NSNumber numberWithBool:defaultFlag]
                    ];
    return success;
}

- (BOOL)deleteAllCategories;
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM Z_NewsCategory_%ld", (long)self.moduleType];
    if (![ZSTSqlManager executeUpdate:deleteSQL]) {
        
        return NO;
    }
    return YES;
}

- (NSDictionary *)getDefaultCategory
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, CategoryID, CategoryName, OrderNum, DefaultFlag, IndexFlag FROM Z_NewsCategory_%ld where DefaultFlag = 1 ORDER BY OrderNum desc", (long)self.moduleType];
    NSArray *results = [ZSTSqlManager executeQuery:querySql];
    if ([results count]) {
        return [results lastObject];
    }
    return nil;
}

- (NSArray *)getCategories
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, CategoryID, CategoryName, OrderNum, DefaultFlag, IndexFlag FROM Z_NewsCategory_%ld where DefaultFlag = 0 ORDER BY OrderNum desc", (long)self.moduleType];
    NSArray *results = [ZSTSqlManager executeQuery:querySql];
    return results;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL)addNews:(NSInteger)msgID
     categoryID:(NSInteger)categoryID
        msgType:(int)msgType
          title:(NSString *)title
        summary:(NSString *)summary
     iconFileID:(NSString *)iconFileID
       iconType:(NSInteger)iconType
       orderNum:(NSInteger)orderNum
         source:(NSString *)source
        addTime:(NSDate *)addTime
{
    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO Z_News_%ld (MsgID, CategoryID, MsgType, Title, summary, IConFileID, IConType, OrderNum, Source, AddTime) VALUES (?,?,?,?,?,?,?,?, ?, ?)", (long)self.moduleType],
                  [NSNumber numberWithInteger:msgID],  
                  [NSNumber numberWithInteger:categoryID], 
                  [NSNumber numberWithInteger:msgType], 
                  [TKUtil wrapNilObject:title], 
                  [TKUtil wrapNilObject:summary], 
                  [TKUtil wrapNilObject:iconFileID], 
                  [NSNumber numberWithDouble:iconType], 
                  [NSNumber numberWithInteger:orderNum], 
                  [TKUtil wrapNilObject:source], 
                  [NSNumber numberWithDouble:[addTime timeIntervalSince1970]]
                  ];
    return success;
}

- (BOOL)setNews:(NSInteger)newsID isRead:(BOOL)isRead
{
    NSString *updateSQL = [NSString stringWithFormat:@"UPDATE Z_News_%ld set IsRead = ?  WHERE MsgID = ?", (long)self.moduleType];
    if (![ZSTSqlManager executeUpdate:updateSQL, [NSNumber numberWithInt:isRead? 1 : 0], [NSNumber numberWithDouble:newsID]]) {
        
        return NO;
    }
    return YES;
}

- (BOOL)getNewsIsRead:(NSInteger)newsID
{
    return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"SELECT IsRead FROM Z_News_%ld where msgID = ?", (long)self.moduleType], [NSNumber numberWithDouble:newsID]];
}

- (BOOL)deleteNews:(NSInteger)newsID
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM Z_News_%ld WHERE MsgID = ?", (long)self.moduleType];
    if (![ZSTSqlManager executeUpdate:deleteSQL, [NSNumber numberWithDouble:newsID]]) {
        
        return NO;
    }
    return YES;
}

- (BOOL)deleteAllNewsOfBroadcast
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM Z_News_%ld where IConType = ?", (long)self.moduleType];
    if (![ZSTSqlManager executeUpdate:deleteSQL, [NSNumber numberWithInt:ZSTIconType_Broadcast]]) {
        return NO;
    }
    return YES;
}

- (BOOL)deleteAllNewsOfCategory:(NSInteger)categoryID
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM Z_News_%ld where categoryID = ?", (long)self.moduleType];
    if (![ZSTSqlManager executeUpdate:deleteSQL, [NSNumber numberWithInteger:categoryID]]) {
        return NO;
    }
    return YES;
}

- (BOOL)newsExist:(NSInteger)newsID
{
    return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"SELECT COUNT(ID) as cnt FROM Z_News_%ld where msgID = ?", (long)self.moduleType], [NSNumber numberWithInteger:newsID]];
}


- (NSDictionary *)newsAtIndex:(int)index category:(NSInteger)categoryID
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, MsgID, CategoryID, MsgType, Title, Summary, IConFileID, IConType, OrderNum, Source, AddTime, IsRead FROM Z_News_%ld where categoryID = ? ORDER BY AddTime desc limit ?,?", (long)self.moduleType];
    NSArray *results = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:
                                                            [NSNumber numberWithInteger:categoryID],
                                                            [NSNumber numberWithInteger:index - 1], 
                                                            [NSNumber numberWithInteger:1], 
                                                            nil]];
    if ([results count]) {
        return [results objectAtIndex:0];
    }
    return nil;
}

- (NSArray *)getBroadcastNews
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, MsgID, CategoryID, MsgType, Title, Summary, IConFileID, IConType, OrderNum, Source, AddTime, IsRead FROM Z_News_%ld where IConType = ? ORDER BY AddTime", (long)self.moduleType];
    NSArray *results = [ZSTSqlManager executeQuery:querySql, [NSNumber numberWithInt:ZSTIconType_Broadcast]];
    return results;
}

- (NSArray *)newsAtPage:(int)pageNum pageCount:(int)pageCount category:(NSInteger)categoryID
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, MsgID, CategoryID, MsgType, Title, Summary, IConFileID, IConType, OrderNum, Source, AddTime, IsRead FROM Z_News_%ld where categoryID = ? ORDER BY AddTime desc limit ?,?", (long)self.moduleType];
    NSArray *results = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:
                                                            [NSNumber numberWithInteger:categoryID],
                                                            [NSNumber numberWithInt:(pageNum-1)*pageCount], 
                                                            [NSNumber numberWithInt:pageCount], 
                                                            nil]];
    return results;
}

- (NSUInteger)newsCountOfCategory:(NSInteger)categoryID
{
    return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"SELECT COUNT(ID) FROM Z_News_%ld where categoryID = ? ", (long)self.moduleType], [NSNumber numberWithInteger:categoryID]];
}

- (NSUInteger) getMaxNewsIdOfCategory:(NSInteger)categoryID
{
	//不能用max(mblogid)，因为mblogid ascii排序有问题
	return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"select MsgID from Z_News_%ld where categoryID = ? and IConType = 1 order by AddTime desc limit 1", (long)self.moduleType], [NSNumber numberWithInteger:categoryID]];
}

- (NSUInteger) getMinNewsIdOfCategory:(NSInteger)categoryID
{
	//不能用max(mblogid)，因为mblogid ascii排序有问题
	return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"select MsgID from Z_News_%ld where categoryID = ? and IConType = 1 order by AddTime asc limit 1", (long)self.moduleType], [NSNumber numberWithInteger:categoryID]];
}
@end
