//
//  NewsMainCell.h
//  F3
//
//  Created by admin on 12-7-17.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZSTNewsVO.h"
#import "ZSTNewsCarouselView.h"
#import "TKAsynImageView.h"

@interface ZSTNewsMainCell : UITableViewCell {
//    TKAsynImageView *cellImageView;
    UIImageView *cellImageView;
    UILabel *title;
    UILabel *subTitle;
    NSInteger newsId;
}

//@property (nonatomic,retain) TKAsynImageView *cellImageView;
@property (nonatomic,retain) UIImageView *cellImageView;
@property (nonatomic,retain) UILabel *title;
@property (nonatomic,retain) UILabel *subTitle;

- (void)configCell:(ZSTNewsVO*)nvo cellForRowAtIndexPath:(NSIndexPath *)indexPath;
@end


@protocol ZSTNewsCarouselViewCellDelegate <NSObject>

@optional
- (void)carouselViewCell:(ZSTNewsCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;


@end

@interface ZSTNewsCarouselViewCell : UITableViewCell<ZSTNewsCarouselViewDataSource,ZSTNewsCarouselViewDelegate>
{
    
    ZSTNewsCarouselView *carouselView;
    NSArray *dataArry;
    id<ZSTNewsCarouselViewCellDelegate>carouselViewCellDelegate;
}
@property (nonatomic, retain) ZSTNewsCarouselView *carouselView;
@property (nonatomic ,retain) NSArray *dataArry;
@property (nonatomic ,retain) id<ZSTNewsCarouselViewCellDelegate>carouselViewCellDelegate;
- (void)configCell:(NSArray*)array;

@end
