//
//  NewsContentViewController.m
//  F3
//
//  Created by admin on 12-7-19.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTNewsContentViewController.h"
#import "ZSTDao+NewsA.h"
#import "ZSTLocalSubstitutionCache.h"
#import "ZSTTKResourceURLProtocol.h"
#import "ZSTTKResourceURL.h"
#import "TKUIUtil.h"
#import "ZSTGlobal+F3.h"
#import "WXApi.h"
#import "ZSTHHSinaShareController.h"
#import "ZSTUtils.h"

@interface ZSTNewsContentViewController ()
{
    BOOL  isfirst;
}

@end

@implementation ZSTNewsContentViewController

@synthesize hTableView;
@synthesize newsArr;
@synthesize selectedImageView;
@synthesize contentVo;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [self.engine cancelAllRequest];
    self.hTableView = nil;
    self.selectedImageView = nil;
    self.newsArr = nil;
    [super dealloc];
}

- (void)refreshTap:(UIControl *)sender
{
    //刷新
    TKHorizontalTableViewCell *cell = (TKHorizontalTableViewCell *)sender.superview.superview;
    UIActivityIndicatorView *aiView = (UIActivityIndicatorView *)[cell viewWithTag:100];
    if (![aiView isAnimating]) {
        [aiView startAnimating];
    }
    UIWebView *webView = (UIWebView *)[cell viewWithTag:99];
    UILabel *refreshLabel = (UILabel *)[cell viewWithTag:102];
    UIControl *refreshCtr = (UIControl *)[webView viewWithTag:103];
    [refreshCtr removeFromSuperview];
    refreshLabel.hidden = YES;
    
    NSInteger index = cell.tag - 1000;
    ZSTNewsVO *currentnvo = [ZSTNewsVO voWithDic:[self.newsArr objectAtIndex:index]];
    [self.engine getNewsContent:[currentnvo.msgID integerValue] userInfo:[NSNumber numberWithInteger:index]];
}


#pragma mark- ZSTHHSinaShareControllerDelegate

- (void)shareDidFinish:(ZSTHHSinaShareController *)sinaShareController options:(NSString *)options
{
    [TKUIUtil alertInView:self.view withTitle:options withImage:nil];
}

- (void)shareDidFail:(ZSTHHSinaShareController *)sinaShareController options:(NSString *)options
{
    [TKUIUtil alertInView:self.view withTitle:options withImage:nil];
}
#pragma mark wxShare

- (void)wxShareSucceed
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"微信分享成功", nil) withImage:nil];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 0) {
        NSString* installUrl =  [WXApi getWXAppInstallUrl];
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:installUrl]];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // 直接检测服务器是否绑定成功
    if (result==MessageComposeResultSent) {
        [ZSTUtils showAlertTitle:nil message:NSLocalizedString(@"分享成功!" , nil)];
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    
    
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信好友" , nil)]
        || [[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信朋友圈" , nil)]){
        
        if (![WXApi isWXAppInstalled] ||! [WXApi isWXAppSupportApi]) {
            
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:NSLocalizedString(@"提示" , nil)
                                  message:NSLocalizedString(@"您未安装微信，现在安装？" , nil)
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"取消" , nil)
                                  otherButtonTitles:NSLocalizedString(@"安装" , nil), nil];
            alert.tag = 108;
            [alert show];
            return;
        }
        // 发送内容给微信
        
        WXMediaMessage *message = [WXMediaMessage message];
        [message setThumbImage:[UIImage imageNamed:@"icon.png"]];
        message.title = self.contentVo.title;
        if ([self.contentVo.content length] != 0) {
            if ([self.contentVo.content length] >50) {
                message.description = [self.contentVo.content substringToIndex:50];
            }else{
                message.description = self.contentVo.content;
            }
        }else{
            message.description = NSLocalizedString(@"点击查看详情" , nil);
        }
        
        WXImageObject *ext = [WXImageObject object];
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"icon" ofType:@"png"];
        ext.imageData = [NSData dataWithContentsOfFile:filePath] ;
        message.mediaObject = ext;
        
        WXWebpageObject *webpage = [WXWebpageObject object];
        webpage.webpageUrl = self.contentVo.shareurl;
        message.mediaObject = webpage;
        
        SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
        req.bText = NO;
        req.message = message;
        
        if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信好友" , nil)]) {
            req.scene = WXSceneSession;
        }
        else  if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信朋友圈"  , nil)]){
            req.scene = WXSceneTimeline;
        }
        
        [WXApi sendReq:req];
        
    }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"短信" , nil)]){
        
        Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));
        
        if (messageClass != nil && [MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
            
            picker.body = [NSString stringWithFormat:NSLocalizedString(@"我在“%@”手机客户端，看到了一篇文章《%@》,分享给你,%@", nil),appDisplayName,self.contentVo.title,self.contentVo.shareurl];
            
            picker.messageComposeDelegate = self;
            
            [self presentModalViewController: picker animated:YES];
            
            [picker release];
        } else {
            
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[NSString stringWithFormat:@"sms:"]]];
        }
    }else{
        
        ZSTHHSinaShareController *sinaShare = [[ZSTHHSinaShareController alloc] init];
        sinaShare.delegate = self;        
        sinaShare.shareString = [NSString stringWithFormat:NSLocalizedString(@"#分享新闻#:%@,%@", nil),self.contentVo.title,self.contentVo.shareurl];
        if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"新浪微博" , nil)]) {
            sinaShare.shareType = sinaWeibo_ShareType;
            sinaShare.navigationItem.title = NSLocalizedString(@"新浪微博", nil);
        }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"QQ空间" , nil)]){
            sinaShare.shareType = QQ_ShareType;
            sinaShare.navigationItem.title = NSLocalizedString(@"QQ空间", nil);
        }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"腾讯微博" , nil)]){
            sinaShare.shareType = TWeibo_ShareType;
            sinaShare.navigationItem.title = NSLocalizedString(@"腾讯微博", nil);
        }
        
        UINavigationController *n = [[UINavigationController alloc] initWithRootViewController:sinaShare];
        [self presentModalViewController:n animated:YES];
        [n release];
        [sinaShare release];
    }
}


- (void)shareAction
{
    UIActionSheet *shareActionSheet = nil;
    NSMutableArray *shareNames = [NSMutableArray array];
    ZSTF3Preferences *pre = [ZSTF3Preferences shared];
    if (pre.SinaWeiBo) {
        [shareNames addObject:NSLocalizedString(@"新浪微博",nil)];
    }
    if (pre.TWeiBo) {
        [shareNames addObject:NSLocalizedString(@"腾讯微博",nil)];
    }
    if (pre.QQ) {
        [shareNames addObject:NSLocalizedString(@"QQ空间",nil)];
    }
    if (pre.WeiXin) {
        [shareNames addObject:NSLocalizedString(@"微信好友",nil)];
        [shareNames addObject:NSLocalizedString(@"微信朋友圈",nil)];
    }
    
    [shareNames addObject:NSLocalizedString(@"短信",nil)];
    
    shareActionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"分享到", nil)
                                                   delegate:self
                                          cancelButtonTitle:nil
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:nil];
    
    for (NSString * title in shareNames) {
        [shareActionSheet addButtonWithTitle:title];
    }
    [shareActionSheet addButtonWithTitle:NSLocalizedString(@"取消",nil)];
    shareActionSheet.cancelButtonIndex = shareActionSheet.numberOfButtons-1;
    shareActionSheet.tag = 101;
    [shareActionSheet showInView:self.view.window];
    [shareActionSheet release];
}

#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 设置图片缓存
    //	LocalSubstitutionCache *cache = [[LocalSubstitutionCache alloc] init];
    //	[NSURLCache setSharedURLCache:cache];
    //    [cache release];
    
    isfirst = YES;
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"分享", @"") target:self selector:@selector (shareAction)];
    
    [ZSTTKResourceURLProtocol registerProtocol];
    
    self.view.backgroundColor = RGBCOLOR(239, 239, 239);
    hTableView = [[TKHorizontalTableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    hTableView.delegate = self;
    hTableView.dataSource = self;
    hTableView.backgroundColor = RGBCOLOR(239, 239, 239);
    hTableView.showsVerticalScrollIndicator = NO;
    hTableView.showsHorizontalScrollIndicator = NO;
    hTableView.pagingEnabled = YES;
    
    [self.view addSubview:hTableView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxShareSucceed) name:NotificationName_WXShareSucceed object:nil];

}

#pragma mark ---
#pragma UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    if ([request.URL.scheme isEqualToString:@"image"]) {
        
        NSString *url = [request.URL description];
        NSRange range1 = [url rangeOfString:@"///"];
        NSRange range2 = [url rangeOfString:@"///" options:NSCaseInsensitiveSearch range:NSMakeRange(range1.location + 3, url.length - range1.location - 3)];
        
        NSString *offsetY = [request.URL.description substringWithRange:NSMakeRange(range1.location+range1.length, range2.location - range1.location-range1.length)];
        
        NSString *imageUrl = [request.URL.description substringFromIndex:range2.location+range2.length];        
        if (imageUrl!=nil && [imageUrl length]>0) {
            ZSTTKResourceURL *resourceURL = [ZSTTKResourceURL URLWithString:imageUrl];
            NSString *cachePath = [ZSTTKResourceURLProtocol cachePathForResourceURL:resourceURL];
            UIImage *image = [UIImage imageWithContentsOfFile:cachePath];
            if (image) {
                
                self.selectedImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(217, [offsetY intValue], 90, 90)] autorelease];
                self.selectedImageView.imageView.contentMode = UIViewContentModeScaleAspectFill;
                self.selectedImageView.imageView.clipsToBounds = YES;
                self.selectedImageView.backgroundColor = RGBCOLOR(239, 239, 239);
                
                self.selectedImageView.defaultImage = image;
                [self.selectedImageView clear];
                self.selectedImageView.url = [NSURL URLWithString:resourceURL.originalUrl];
                [self.selectedImageView loadImage];
                
                [self.view addSubview:selectedImageView];
                
                
                selectedImageView.imageView.zoomedSize = [self displayRectForImage:CGSizeMake(self.selectedImageView.imageView.image.size.width, self.selectedImageView.imageView.image.size.height)];
                
                selectedImageView.imageView.wrapInScrollviewWhenZoomed = YES;
                selectedImageView.showLoadingWheel = YES;
                
                [selectedImageView.imageView zoomIn];
                selectedImageView.imageView.zoomDelegate = self;
                selectedImageView.callbackOnSetImage = self;
            }            
            return NO;
        }
    } else if((navigationType == UIWebViewNavigationTypeLinkClicked || navigationType == UIWebViewNavigationTypeFormSubmitted) && [request.URL.scheme caseInsensitiveCompare:@"http"] == NSOrderedSame) {
        
        [[UIApplication sharedApplication] openURL:request.URL];
        return NO;
    }
    return YES;
}
#pragma mark HJManagedImageVDelegate
-(void) managedImageSet:(HJManagedImageV*)mi
{
    mi.imageView.contentMode = UIViewContentModeScaleAspectFill;
    mi.imageView.size = [self displayRectForImage:CGSizeMake(self.selectedImageView.imageView.image.size.width, self.selectedImageView.imageView.image.size.height)];
    mi.imageView.center = self.view.superview.superview.center;
}

- (void)zoomWindow:(MTZoomWindow *)zoomWindow didZoomOutView:(UIView *)view {
    self.selectedImageView.alpha = 0;
    [self.selectedImageView removeFromSuperview];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    TKDERROR(@"error == %@", error);
}

- (void)webViewDidStartLoad:(UIWebView *)webView //网页加载时调用
{
    //    [aiView startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView //网页完成加载时调用
{
    if (isfirst) {
        hTableView.contentOffset = CGPointMake(320*self.selectedIndex, 0);
        isfirst = NO;
    }
}
- (CGSize)displayRectForImage:(CGSize)imageSize {
    CGRect screenRect = TKScreenBounds();
    
    if (imageSize.width > screenRect.size.width) {
        CGFloat height = imageSize.height * screenRect.size.width / imageSize.width;
        CGFloat width = screenRect.size.width;
        if (height > screenRect.size.height) {
            width = width * screenRect.size.height/height;
            height = screenRect.size.height;
        }
        return CGSizeMake(width, height);
    }else if (imageSize.height > screenRect.size.height) {
        CGFloat width = imageSize.width * screenRect.size.height / imageSize.height;
        CGFloat height = screenRect.size.height;
        
        return CGSizeMake(width, height);
    } else {
        return CGSizeMake(imageSize.width, imageSize.height); 
    }
}


- (void)viewDidUnload
{
    //    [webView removeFromSuperview];
    [super viewDidUnload];
    //    [aiView removeFromSuperview];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)templateEngine:(MGTemplateEngine *)engine blockStarted:(NSDictionary *)blockInfo
{
    //	NSLog(@"Started block %@", [blockInfo objectForKey:BLOCK_NAME_KEY]);
}

- (void)templateEngine:(MGTemplateEngine *)engine blockEnded:(NSDictionary *)blockInfo
{
    //	NSLog(@"Ended block %@", [blockInfo objectForKey:BLOCK_NAME_KEY]);
}

- (void)templateEngineFinishedProcessingTemplate:(MGTemplateEngine *)engine
{
    //	NSLog(@"Finished processing template.");
}

- (void)templateEngine:(MGTemplateEngine *)engine encounteredError:(NSError *)error isContinuing:(BOOL)continuing;
{
	NSLog(@"Template error: %@", error);    
}

#pragma mark --
#pragma TKHorizontalTableViewDataSource -------------

- (NSInteger)numberOfRowsInTableView:(TKHorizontalTableView *)tableView {
    return [self.newsArr count]; 
}

- (TKHorizontalTableViewCell *)tableView:(TKHorizontalTableView *)tableView cellAtIndex:(NSUInteger)index {
    
    TKHorizontalTableViewCell *hCell = [tableView dequeueReusableCell];
    
    if (hCell == nil) {
        hCell = [[[TKHorizontalTableViewCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.height, 320)] autorelease];
        hCell.backgroundColor = RGBCOLOR(239, 239, 239);
        NSInteger height = self.view.frame.size.height;
        UIWebView *webView = [[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, height)] autorelease];
        webView.tag = 99;
        webView.backgroundColor = RGBCOLOR(239, 239, 239);
        webView.dataDetectorTypes = UIDataDetectorTypeNone;
        webView.opaque = NO;
        webView.delegate = self; 
        //去掉阴影
        for(UIView* subView in [webView subviews])
        {
            if([subView isKindOfClass:[UIScrollView class]]){
                for(UIView* shadowView in [subView subviews])
                {
                    if([shadowView isKindOfClass:[UIImageView class]]){
                        [shadowView setHidden:YES];
                    }
                }
            }
        }
        [hCell addSubview:webView];
        
        UIActivityIndicatorView *aiView = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
        aiView.center = CGPointMake(160, 185);
        aiView.tag = 100;
        [hCell addSubview:aiView];
        
        UILabel *refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 170, 200, 30)];
        refreshLabel.font = [UIFont boldSystemFontOfSize:18];
        refreshLabel.text = NSLocalizedString(@"加载失败，点击重新加载", nil);
        refreshLabel.textAlignment = NSTextAlignmentCenter;
        refreshLabel.textColor = RGBACOLOR(200, 200, 200, 1);
        refreshLabel.backgroundColor = [UIColor clearColor];
        refreshLabel.tag = 102;
        [hCell addSubview:refreshLabel];
        [refreshLabel release];
    }
    
    hCell.tag = index+1000;
    
    //    480-44-37-20
    UIWebView *webView = (UIWebView *)[hCell viewWithTag:99];
   [webView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML='';"];
    UIActivityIndicatorView *aiView = (UIActivityIndicatorView *)[hCell viewWithTag:100];
    [aiView startAnimating];
    
    ZSTNewsVO *nvo = [ZSTNewsVO voWithDic:[self.newsArr objectAtIndex:index]];
    
    [self.engine getNewsContent:[nvo.msgID integerValue] userInfo:[NSNumber numberWithInteger:index]];
    
    UILabel *refreshLabel = (UILabel *)[hCell viewWithTag:102];
    refreshLabel.hidden = YES;
    
    return hCell;
}

- (void)getNewsContentDidSucceed:(NSDictionary *)newsContent userInfo:(id)userInfo{
    
    NSNumber *index = userInfo;        
    ZSTNewsVO *nvo = [ZSTNewsVO voWithDic:[self.newsArr objectAtIndex:[index unsignedIntegerValue]]];
    [self.dao setNews:[nvo.msgID integerValue] isRead:YES];//设置内容为已读
    ZSTNewsContentVO *contentVO = [ZSTNewsContentVO voWithDic:newsContent];
    self.contentVo = contentVO;
    
    //目前只做图片
    NSMutableString *urlStr = [NSMutableString string];
    if ([contentVO.fileArr isKindOfClass:[NSArray class]] && [contentVO.fileArr count]>0) {
        for (int i=0; i<[contentVO.fileArr count]; i++) {
            [urlStr appendString:@"<a href=\"javascript:void(0)\"><span class=\"photo\"><img class=\"photo_box\" src=\""];
            NSDictionary *fileDic = [contentVO.fileArr objectAtIndex:i];
            NSString *url = [fileDic safeObjectForKey:@"FileUrl"];
            NSString *origUrl = [fileDic safeObjectForKey:@"originalUrl"];
            ZSTTKResourceURL *resourceURL = [ZSTTKResourceURL resourceURLWithResource:url originalUrl:origUrl MIMEType:@"image/png"];
            [urlStr appendString:resourceURL.absoluteString];
            [urlStr appendString:@"\" onload=\"javascript:DrawImage(this)\" onclick=\"javascript:extend_image(this.src,this)\"/></span></a>"];
        }
 	}
    
    // Set up template engine with your chosen matcher.
    MGTemplateEngine *engine = [MGTemplateEngine templateEngine];
    [engine setDelegate:self];
    [engine setMatcher:[ICUTemplateMatcher matcherWithTemplateEngine:engine]];    
    
    NSString *templatePath = ZSTPathForModuleBundleResource(@"module_newsa_content_template.html");
    
    NSArray *contenArray = [contentVO.content componentsSeparatedByString:@"\n"];
    NSMutableString * contentStr = [NSMutableString string];
    for (int i = 0; i < [contenArray count]; i ++) {
        NSString *str = [contenArray objectAtIndex:i];
        
//        NSMutableCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
//        //去掉段落头尾的空格和中文空格
//        UniChar chars[] = {0x3000};
//        NSString *string = [[NSString alloc] initWithCharacters:chars length:sizeof(chars)/sizeof(UniChar)];
//        [set addCharactersInString:string];
//        [string release];caseInsensitiveCompare:@"http"
//        str = [str stringByTrimmingCharactersInSet:set];  
        
        [contentStr appendString:[NSString stringWithFormat:@"<p style=\"text-indent : 0em;\">%@</p>",str]];
    }
    
    NSString *content = [NSString stringWithFormat:@"%@%@",urlStr,contentStr];
    
    NSDictionary *variables = [NSDictionary dictionaryWithObjectsAndKeys:contentVO.title,@"title", contentVO.addTime,@"date" ,contentVO.source,@"resource",content ,@"body", @"", @"fontClass", @"", @"theme", nil];        
    
    NSString *result = [engine processTemplateInFileAtPath:templatePath withVariables:variables];
    
    //不用生成html文件，直接加载字符串
    NSURL *url = [NSURL fileURLWithPath:ZSTPathForModuleBundleResource(@"NewsA")];
    TKHorizontalTableViewCell *cell = [hTableView cellForIndex:[index intValue]];
    UIActivityIndicatorView *aiView = (UIActivityIndicatorView *)[cell viewWithTag:100];
    if ([aiView isAnimating]) {
        [aiView stopAnimating];
    }
    UIWebView *webView = (UIWebView *)[cell viewWithTag:99];
    [webView loadHTMLString:result baseURL:url];  
    UILabel *refreshLabel = (UILabel *)[cell viewWithTag:102];
    refreshLabel.hidden = YES;
}

- (void)getNewsContentDidFailed:(int)resultCode userInfo:(id)userInfo
{  
    NSInteger selectIndex = [[userInfo safeObjectForKey:@"userInfo"] integerValue];
    
    TKHorizontalTableViewCell *cell = [hTableView cellForIndex:selectIndex];
    UIActivityIndicatorView *aiView = (UIActivityIndicatorView *)[cell viewWithTag:100];
    UIWebView *webView = (UIWebView *)[cell viewWithTag:99];
    
    if ([aiView isAnimating]) {
        [aiView stopAnimating];
    }
    
    UILabel *refreshLabel = (UILabel *)[cell viewWithTag:102];
    refreshLabel.hidden = NO;
    
    UIControl *refreshCtr = [[UIControl alloc] initWithFrame:self.view.frame];
    [refreshCtr addTarget:self action:@selector(refreshTap:) forControlEvents:UIControlEventTouchUpInside];
    refreshCtr.tag = 103;
    [webView addSubview:refreshCtr];
    [refreshCtr release];
}

#pragma mark --
#pragma TKHorizontalTableViewDelegate -------------
- (CGFloat)tableView:(TKHorizontalTableView *)tableView widthForCellAtIndex:(NSUInteger)index {
    return self.view.frame.size.width;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (hTableView.contentOffset.x < -20) {
        [self.navigationController popViewControllerAnimated:YES];
    }    
}


@end
