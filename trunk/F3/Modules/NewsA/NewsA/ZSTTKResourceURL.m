//
//  TKResourceURL.h
//  News
//
//  Created by luobin on 7/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTTKResourceURL.h"

@implementation ZSTTKResourceURL

- (void)dealloc
{
    [super dealloc];
}

+ (ZSTTKResourceURL*) resourceURLWithURL:(NSURL *)url
{
    if ([self isResourceURL:url]) {
        return [ZSTTKResourceURL URLWithString:url.absoluteString];
    }
    return nil;
}

+ (ZSTTKResourceURL*) resourceURLWithResource:(NSString *)resource originalUrl:(NSString *)originalUrl MIMEType:(NSString *)MIMEType
{
    if (resource == nil) {
        resource = @"";
    }
    if (originalUrl == nil) {
        originalUrl = @"";
    }
    if (MIMEType == nil) {
       MIMEType = @"text/html";
    }
    return [ZSTTKResourceURL URLWithString:[NSString stringWithFormat:@"resource://?resource=%@&originalUrl=%@&MIMEType=%@", [resource urlEncodeValue],[originalUrl urlEncodeValue], MIMEType]];
}

+ (BOOL)isResourceURL:(NSURL *)url
{
    return [url.scheme caseInsensitiveCompare:@"resource"] == NSOrderedSame;
}

- (NSString *)resource
{
    return [[[self.query queryDictionaryUsingEncoding:NSUTF8StringEncoding] safeObjectForKey:@"resource"] urlDecodeValue];
}

- (NSString *)MIMEType
{
    NSString *MIMEType = [[self.query queryDictionaryUsingEncoding:NSUTF8StringEncoding] safeObjectForKey:@"MIMEType"];
    if (MIMEType == nil) {
        MIMEType = @"text/html";
    }
    return MIMEType;
}

- (NSString *)originalUrl
{
    return [[[self.query queryDictionaryUsingEncoding:NSUTF8StringEncoding] safeObjectForKey:@"originalUrl"] urlDecodeValue];
}
@end
