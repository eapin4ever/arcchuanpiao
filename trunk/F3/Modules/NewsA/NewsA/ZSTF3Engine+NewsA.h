//
//  ZSTF3Engine+News.h
//  News
//
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTF3Engine.h"
#import "ZSTGlobal+NewsA.h"

@protocol ZSTF3EngineNewsDelegate <ZSTF3EngineDelegate>

@optional

- (void)getBroadcastNewsDidSucceed:(NSArray *)newsList;
- (void)getBroadcastNewsDidFailed:(int)resultCode;

- (void)getNewsDidSucceed:(NSArray *)newsList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish;
- (void)getNewsDidFailed:(int)resultCode;

- (void)getCategoryDidSucceed:(NSDictionary *)defaultIndexCategory  categories:(NSArray *)categories;
- (void)getCategoryDidFailed:(int)resultCode;

- (void)getNewsContentDidSucceed:(NSDictionary *)newsContent userInfo:(id)userInfo;
- (void)getNewsContentDidFailed:(int)resultCode userInfo:(id)userInfo;

@end

@interface ZSTF3Engine (NewsA)

/**
 *	@brief	获取新闻类别
 *
 */
- (void)getCategory;

///////////////////////////////////////////////////////////////////////// Private ///////////////////////////////////////////////////////////////////////

/**
 *	@brief	获取新闻列表
 *
 *	@param 	categoryID 	分类ID
 *	@param 	sinceID 	起始新闻ID
 *	@param 	orderType 	当OrderType为Desc时（刷新），从服务器获取MsgID大于SinceID的至多Size条记录；当OrderType为Asc时（更多），从服务器获取MsgID小于SinceID的至多Size条记录
 *	@param 	fetchCount 	获取最大条数
 *	@param 	iconType 	ICon图片类别，1：列表ICon；2：轮播ICon
 */
- (void)getNews:(NSInteger)categoryID
        sinceID:(NSInteger)sinceID
      orderType:(NSString *)orderType
     fetchCount:(NSUInteger)fetchCount
       iconType:(ZSTIconType)iconType;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief	获取轮播新闻
 *
 */
- (void)getBroadcastNews;

/**
 *	@brief	获取指定圈子下pageNum页的新闻
 *
 *	@param 	pageNum 	页数
 *	@param 	categoryID 	分类
 */
- (void)getNewsAtPage:(int)pageNum category:(NSInteger)categoryID;

/**
 *	@brief	获取新闻正文
 *
 *	@param 	newsID 	新闻ID
 */
- (void)getNewsContent:(NSInteger)newsID userInfo:(id)userInfo;
    
@end
