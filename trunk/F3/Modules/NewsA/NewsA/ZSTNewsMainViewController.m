//
//  NewsMainViewController.m
//  F3
//
//  Created by fujinsheng on 12-7-17.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTNewsMainViewController.h"
#import "ZSTNewsCarouselView.h"
#import "ColumnBar.h"
#import "ZSTDao+NewsA.h"
#import "ZSTF3Engine+NewsA.h"
#import "ZSTUtils.h"
#import "ZSTShell.h"

#define maxItems 6
#define intervalTime 1   //小时

@implementation ZSTNewsMainViewController

@synthesize newsArr;

- (void)moduleApplication:(ZSTModuleApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [application.dao createTableIfNotExistForInfoaModule];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _isLoadingMore = NO;
    _isRefreshing = NO;
    _hasMore = NO;
    _isFirstLoading = YES;
    
    _pagenum = 1;
    self.view.backgroundColor = [UIColor whiteColor];
    self.view.autoresizesSubviews = YES;
    
    self.newsArr = [NSMutableArray array];
    indexCategoryArr = (NSMutableArray *)[[self.dao getCategories] retain];
    broadcastNewsArr = [[self.dao getBroadcastNews] retain];
    headline = [[self.dao getDefaultCategory] retain];
    
    if ([headline count] != 0) {
        [indexCategoryArr insertObject:headline atIndex:0];
    }
    
    // 顶部导航栏
    _columnBar = [[ColumnBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 38)];
    _columnBar.dataSource = self;
    _columnBar.delegate = self;
    _columnBar.moverImage = ZSTModuleImage(@"module_newsa_category_selected.png");
    _columnBar.image =  ZSTModuleImage(@"module_newsa_category_bg.png");
    [self.view addSubview:_columnBar];
    
    if ([indexCategoryArr count] != 0) {
        if ([indexCategoryArr count] == 1) {
            _columnBar.hidden = YES;
        }
        [_columnBar reloadData];
        [_columnBar selectTabAtIndex:0];
    }
    
    //表格
    newsTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 37, 320, 480+(iPhone5?88:0)-37-(IS_IOS_7?0:20)) style:UITableViewStylePlain];
    newsTable.delegate = self;
    newsTable.dataSource = self;
    newsTable.backgroundColor = [UIColor whiteColor];
    newsTable.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    newsTable.separatorStyle =  UITableViewCellSeparatorStyleNone;
    if (_columnBar.hidden) {
        newsTable.frame = CGRectMake(0, 0, 320, 480-(IS_IOS_7?0:20)+(iPhone5?88:0));
    }
    [self.view addSubview:newsTable];
    
    if (_refreshHeaderView == nil) {
        
        _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f-250.0, self.view.frame.size.width, 250.0)];
        _refreshHeaderView.layer.contents = (id) ZSTModuleImage(@"module_newsa_pulltorefresh_background.png").CGImage;

        _refreshHeaderView.delegate = self;
        [newsTable addSubview:_refreshHeaderView];
        
        [_refreshHeaderView refreshLastUpdatedDate];
        
    }
    
    UIImageView *shadow = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_newsa_pulltorefresh_shadow.png")];
    shadow.frame = CGRectMake(0, 0, 320, -4);
    [newsTable addSubview:shadow];
    [shadow release];
    
    if (_loadMoreView == nil) {
        _loadMoreView = [[TKLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
        _loadMoreView.backgroundColor = [UIColor clearColor];
        _loadMoreView.delegate = self;

    }
    
    UIImageView *columnBarShadow = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_newsa_category_shadow.png")];
    columnBarShadow.frame = CGRectMake(0, CGRectGetMaxY(_columnBar.frame)-1, 320, 4);
    [self.view addSubview:columnBarShadow];    
    columnBarShadow.tag = 88;
    [columnBarShadow release];
    
    if (_columnBar.hidden) {
        columnBarShadow.hidden = YES;
    }
    
    
    [self.view newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkIndicatorView) {
        [self.engine getCategory];
    }];
  
}

- (void)updateListData
{
//    NSInteger categoryID= [[[indexCategoryArr objectAtIndex:currentSelectItem] safeObjectForKey:@"CategoryID"] integerValue];
    //    self.newsArr = (NSMutableArray *)[self.dao newsAtPage:1 pageCount:NEWS_PAGE_SIZE category:categoryID];
    
    [self.engine getNewsAtPage:1 category:[[[indexCategoryArr objectAtIndex:currentSelectItem] safeObjectForKey:@"CategoryID"] integerValue]];
    
//    _pagenum = 2;
//    _hasMore = ([self.dao newsCountOfCategory:categoryID] >= NEWS_PAGE_SIZE);
    [newsTable reloadData];
}

- (BOOL)isSelectHeadLine
{
    if (currentSelectItem == 0) {
        return YES;
    }else {
        return NO;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [newsTable reloadData];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)autoRefresh
{
    [_refreshHeaderView autoRefreshOnScroll:newsTable animated:YES];
}

- (void)finishLoading
{
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:newsTable];
    [_loadMoreView loadMoreScrollViewDataSourceDidFinishedLoading:newsTable];
    
    _isLoadingMore = NO;
    _isRefreshing = NO;
}

- (void)refreshNewestData
{
    //此处从服务器获取最新数据
    if ([indexCategoryArr count] == 0 || indexCategoryArr == nil) {
        return;
    }
    _pagenum = 1;
    [self.engine getNewsAtPage:1 category:[[[indexCategoryArr objectAtIndex:currentSelectItem] safeObjectForKey:@"CategoryID"] integerValue]];
}

- (void)loadMoreData {
    
    [self.engine getNewsAtPage:_pagenum category:[[[indexCategoryArr objectAtIndex:currentSelectItem] safeObjectForKey:@"CategoryID"] integerValue]];
    
}

- (void)appendData:(NSArray*)dataArray
{
    [self.newsArr addObjectsFromArray:dataArray];
    _pagenum ++ ;
}

- (void)insertData:(NSArray*)dataArray
{
    self.newsArr = [NSMutableArray arrayWithArray:dataArray];
    _pagenum ++;
}

#pragma mark HHLoadMoreViewDelegate Methods
- (void)loadMoreDidTriggerRefresh:(TKLoadMoreView*)view
{
    if (_hasMore) {
        _isLoadingMore = YES;
        [self loadMoreData];
    }
}

- (BOOL)loadMoreDataSourceIsLoading:(TKLoadMoreView*)view
{
    return _isLoadingMore;
}


- (void)fetchDataSuccess:(NSArray *)theData isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore
{
    _hasMore = hasMore;
    
    if (isLoadMore) {
        [self appendData:theData];
    }else {
        [self insertData:theData];
    }
    
    [newsTable reloadData];
    
    [self finishLoading];
}

- (void)fetchDataFailed
{
    [self finishLoading];
}

#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    if (!_isLoadingMore) {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
    if (!_isRefreshing) {
        [_loadMoreView loadMoreScrollViewDidEndDragging:scrollView];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (!_isLoadingMore) {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
    if (!_isRefreshing) {
        [_loadMoreView performSelector:@selector(loadMoreScrollViewDidEndDragging:) withObject:scrollView afterDelay:0];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!_isLoadingMore) {
        [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
        
    }
    if (!_isRefreshing) {

    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    TKLoadMoreView *loadMoreView = (TKLoadMoreView *)[cell descendantOrSelfWithClass:[TKLoadMoreView class]];
    if (loadMoreView) {
        if (!_isRefreshing) {
            [_loadMoreView loadMoreTriggerRefresh];
        }
    }
}

#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view {
    _isRefreshing = YES;
    [self refreshNewestData];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView *)view {
    return _isRefreshing;
}

- (void)egoRefreshTableHeaderDataSourceLastTimeUpdated:(EGORefreshTableHeaderView *)view {
    if (indexCategoryArr != nil && [indexCategoryArr count] != 0) {
        NSString * categoryID = [[[indexCategoryArr objectAtIndex:currentSelectItem] safeObjectForKey:@"CategoryID"] description];
        NSDate *lastTime = [NSDate date];
        [[NSUserDefaults standardUserDefaults] setObject:lastTime forKey:categoryID];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (NSDate *)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView *)view {
    //这里是获取每个分类时间
    NSDate *lastTime =  nil;
    
    if ([indexCategoryArr count] != 0) {
        NSString * categoryID= [[[indexCategoryArr objectAtIndex:currentSelectItem] safeObjectForKey:@"CategoryID"] description];
        lastTime = (NSDate *)[[NSUserDefaults standardUserDefaults] objectForKey:categoryID];
    }
    if (lastTime == nil) {
        lastTime = [NSDate date];
    }
    return lastTime;
}

#pragma mark - ColumnBarDataSource

- (NSInteger)numberOfTabsInColumnBar:(ColumnBar *)columnBar
{
    return [indexCategoryArr count];
}

- (NSString *)columnBar:(ColumnBar *)columnBar titleForTabAtIndex:(int)index
{
    return [[indexCategoryArr objectAtIndex:index] safeObjectForKey:@"CategoryName"];
}

#pragma mark - ColumnBarDelegate

- (BOOL)compareTimerkey:(NSString *)key timeDic:(NSMutableDictionary *)timerDic
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:timerDic];
    NSDate *compareDate = [dic safeObjectForKey:key];
    if (compareDate == nil || [[compareDate description] length] == 0) {
        [dic setValue:[[NSDate date] dateByAddingHours:intervalTime] forKey:key];//第一次计时
        [_timeDic removeAllObjects];
        [_timeDic release];
        _timeDic = [[NSMutableDictionary dictionaryWithDictionary:dic] retain];
        return YES;
    }else {
        if ([compareDate isEqualToDate:[NSDate date]] || [compareDate isEarlierThanDate:[NSDate date]]) {
            [dic removeObjectForKey:key];
            [dic setValue:[[NSDate date] dateByAddingHours:intervalTime] forKey:key];//第n次计时
            [_timeDic removeAllObjects];
            [_timeDic release];
            _timeDic = [[NSMutableDictionary dictionaryWithDictionary:dic] retain];           
            return YES;
        }else {
            return NO;
        }
    }
}

- (void)columnBar:(ColumnBar *)columnBar didSelectedTabAtIndex:(NSInteger)index
{
    currentSelectItem = index; 
    [self finishLoading];
    
    //从类型得数组里面得到该分类得对象，得到分类得编号
    [self.newsArr removeAllObjects];
    [newsTable reloadData];
    newsTable.tableFooterView = nil;
    
    [self updateListData];
    
    if (!_isFirstLoading) {
        
        BOOL isAutoRefreshTime = NO;
        isAutoRefreshTime = [self compareTimerkey:[[indexCategoryArr objectAtIndex:currentSelectItem] safeObjectForKey:@"CategoryID"] timeDic:_timeDic];
        if (isAutoRefreshTime) {
            if ([self isSelectHeadLine]) {
                [self.engine getBroadcastNews];//获取轮播图
            }
            
            //此处检查当前网络连接，如果是wifi则自动更新，非wifi就不自动更新//还没有做
            [self performSelector:@selector(autoRefresh) 
                       withObject:nil 
                       afterDelay:0.3];
        }
    }
}

#pragma mark - ZSTF3EngineNewsDelegate

- (void)getCategoryDidSucceed:(NSDictionary *)defaultIndexCategory  categories:(NSArray *)categories
{
    [self.view removeNetworkIndicatorView];
    
    if (([defaultIndexCategory count] != 0) || [categories count] != 0) {
        
        [headline release];
        headline = nil;
        headline = [defaultIndexCategory retain];
        
        //获取分类信息
        [indexCategoryArr release];
        indexCategoryArr = nil;
        indexCategoryArr = [[NSMutableArray arrayWithArray:categories] retain];
        
        if ([headline count] != 0) {
            [indexCategoryArr insertObject:headline atIndex:0];
        }
        
        UIImageView *shadow = (UIImageView *)[self.view viewWithTag:88];
        
        CGRect newsTableFrame = newsTable.frame;
        
        if ([indexCategoryArr count] == 1) {
            
            newsTableFrame.size.height +=  _columnBar.hidden ? 0 : 37;
            shadow.hidden = YES;
            _columnBar.hidden = YES;
        }else {
            newsTableFrame.size.height -=  _columnBar.hidden ? 37 :0;
            shadow.hidden = NO;
            _columnBar.hidden = NO;
        }
        
        [newsTable removeFromSuperview];
        
        newsTableFrame.origin.y = _columnBar.hidden ? 0 : 37;
        
        [UIView animateWithDuration:0 animations:^{
            newsTable.frame = newsTableFrame;
        }];
        
        [self.view addSubview:newsTable];
    }
    
    
    _isFirstLoading = NO;
    [_columnBar reloadData];
    [_columnBar selectTabAtIndex:0];
}

- (void)getCategoryDidFailed:(int)resultCode
{
    if ([indexCategoryArr count] != 0) {
        [self.view removeNetworkIndicatorView];
    }else {
        [self.view refreshFailed];
    }
}


- (void)getBroadcastNewsDidSucceed:(NSArray *)newsList
{
    if ([newsList count] != 0) {
        BOOL needRefresh = YES;
        if (broadcastNewsArr !=nil && [broadcastNewsArr count]>0) {
            needRefresh = NO;
        }
        [broadcastNewsArr release];
        broadcastNewsArr = nil;
        broadcastNewsArr = [newsList retain];
        if (needRefresh) {
            [newsTable reloadData];
        } else {
            [newsTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:0 inSection:0], nil] withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

- (void)getBroadcastNewsDidFailed:(int)resultCode
{
    
}

- (void)getNewsDidSucceed:(NSArray *)newsList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish
{
    [self fetchDataSuccess:newsList isLoadMore:isLoadMore hasMore:hasMore];
}

- (void)getNewsDidFailed:(int)resultCode
{
    [self finishLoading];
}

- (void)engine:(SinaWeibo *)engine requestDidSucceedWithResult:(id)result
{
    if ([result  count] == 0) {
        [self fetchDataSuccess:nil isLoadMore:NO hasMore:NO];    
    }
    [self finishLoading];  
}

- (void)requestDidFail:(ZSTLegacyResponse *)response
{
    if ([indexCategoryArr count] != 0) {
        [self.view removeNetworkIndicatorView];
    }else {
        [self.view refreshFailed];
    }
    [self finishLoading];    
}

#pragma mark - ZSTNewsCarouselViewCellDelegate

- (void)carouselViewCell:(ZSTNewsCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index
{
    ZSTNewsContentViewController *content = [[ZSTNewsContentViewController alloc] init];
    content.newsArr = broadcastNewsArr;
    content.hidesBottomBarWhenPushed = YES;
     content.selectedIndex = index;
    [self.navigationController pushViewController:content animated:YES];
//    content.hTableView.contentOffset = CGPointMake(320 * index, 0);
    [content release];
}

#pragma mark ---点击某行触发的方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger row = [indexPath row];
    ZSTNewsContentViewController *content = [[ZSTNewsContentViewController alloc] init];
    content.newsArr = newsArr;
    content.hidesBottomBarWhenPushed = YES;
    content.selectedIndex = (([self isSelectHeadLine ]&& [broadcastNewsArr count] != 0)?(row-1):row);
    [self.navigationController pushViewController:content animated:YES];
//    content.hTableView.contentOffset = CGPointMake(([self isSelectHeadLine ]&& [broadcastNewsArr count] != 0)? 320*(row - 1):320 * row, 0);
    [content release];
}

#pragma mark -
#pragma mark ---Table Data Source Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if ([self isSelectHeadLine] && [broadcastNewsArr count] != 0) {
        return [newsArr count]+1+(_hasMore?1:0);
    }else {
        return [newsArr count]+(_hasMore?1:0);
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TKCellBackgroundView *custview = [[[TKCellBackgroundView alloc] init] autorelease];
    custview.fillColor = [UIColor colorWithWhite:0.98 alpha:1];
    custview.borderColor = [UIColor colorWithWhite:0.88 alpha:1];
    custview.shadowColor = [UIColor whiteColor];
    
    if(([newsTable numberOfRowsInSection:indexPath.section]-1) == 0){
        custview.position = CustomCellBackgroundViewPositionSingle;
    }
    else if(indexPath.row == 0){
        custview.position = CustomCellBackgroundViewPositionTop;
    }
    else if (indexPath.row == ([newsTable numberOfRowsInSection:indexPath.section]-1)){
        custview.position  = CustomCellBackgroundViewPositionBottom;
    }
    else{
        custview.position = CustomCellBackgroundViewPositionMiddle;
    }
    
    if (indexPath.section == 0 && indexPath.row == 0 && [self isSelectHeadLine] && [broadcastNewsArr count] != 0) {//轮播图cell
        static NSString *newsCellIdentifier = @"newsCarouselViewCellIdentifier";
        ZSTNewsCarouselViewCell *cell = [tableView dequeueReusableCellWithIdentifier:newsCellIdentifier];
        if (cell == nil) {
            cell = [[[ZSTNewsCarouselViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:newsCellIdentifier] autorelease];
            cell.carouselViewCellDelegate = self;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        [cell configCell:broadcastNewsArr];        
        return cell;
    }
    else {//信息cell
   
        if ((indexPath.row == [newsArr count] && ![self isSelectHeadLine]) ||
            (indexPath.row == [newsArr count]+1 && [broadcastNewsArr count] != 0 && [self isSelectHeadLine]) ||
            (indexPath.row == [newsArr count] && [broadcastNewsArr count] == 0 && [self isSelectHeadLine])) {
            static NSString *newsCellIdentifier = @"loadMoreView";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:newsCellIdentifier];
            
            if (cell==nil) {
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:newsCellIdentifier] autorelease];
                cell.accessoryType = UITableViewCellAccessoryNone;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.userInteractionEnabled = NO;
                [cell.contentView addSubview:_loadMoreView];
            }
            return cell;
            
        } else {
            static NSString *newsCellIdentifier = @"newsCellIdentifier";
            ZSTNewsMainCell *cell = [tableView dequeueReusableCellWithIdentifier:newsCellIdentifier];
            
            if (cell==nil) {
                cell = [[[ZSTNewsMainCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:newsCellIdentifier] autorelease];
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            }
            
            UIImageView *selectView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Module.bundle/module_newsa_list_item_bg_p.png"]];
            selectView.frame = CGRectMake(0, 0, 320, 80);
            cell.selectedBackgroundView = selectView;
            cell.backgroundView = custview;
            [selectView release];
            if ([newsArr count]) {
                NSDictionary *newsDic = (NSDictionary *)[newsArr objectAtIndex: ([broadcastNewsArr count] && [self isSelectHeadLine]) ? (indexPath.row - 1) : indexPath.row];
                ZSTNewsVO *nvo = [ZSTNewsVO voWithDic:newsDic];
                [cell configCell:nvo cellForRowAtIndexPath:indexPath];
            }

            return cell;
        }
    }
    return nil;
}

#pragma mark ---UITableViewDelegate 

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0 && [self isSelectHeadLine] && [broadcastNewsArr count] != 0) {
        return 160;
    } else if ((indexPath.row == [newsArr count] + 1 && [self isSelectHeadLine] && [broadcastNewsArr count] != 0) ||
               (indexPath.row == [newsArr count] && ![self isSelectHeadLine]) ||
               (indexPath.row == [newsArr count] && [broadcastNewsArr count] == 0 && [self isSelectHeadLine])) {
        return 50;
    }else{
        return 80;
    }
}

- (void)dealloc
{
    [newsArr release];
    [newsTable release];
    [indexCategoryArr release];
    [broadcastNewsArr release];
    [super dealloc];
}


@end
