//
//  NewsContentViewController.h
//  F3
//
//  Created by admin on 12-7-19.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTNewsVO.h"
#import "MGTemplateEngine.h"
#import "ICUTemplateMatcher.h"
#import "TKHorizontalTableView.h"
#import "ZSTF3Engine+NewsA.h"
#import "ZSTNewsContentVO.h"
#import "TKAsynImageView.h"
#import "MTZoomWindowDelegate.h"
#import <MessageUI/MessageUI.h>
#import "ZSTHHSinaShareController.h"


@interface ZSTNewsContentViewController : UIViewController <UIWebViewDelegate, MGTemplateEngineDelegate,
    TKHorizontalTableViewDelegate,TKHorizontalTableViewDataSource,MTZoomWindowDelegate,HJManagedImageVDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,ZSTHHSinaShareControllerDelegate,MFMessageComposeViewControllerDelegate> {
    
    TKAsynImageView *selectedImageView;
    
    NSArray *newsArr;

    TKHorizontalTableView *hTableView;
    
    NSString *requestUrl;
    
}
@property (nonatomic, retain) TKHorizontalTableView *hTableView;
@property (nonatomic,retain) NSArray *newsArr;
@property (nonatomic,retain) TKAsynImageView *selectedImageView;
@property (nonatomic ,retain) ZSTNewsContentVO *contentVo;

@property (nonatomic, assign) NSInteger selectedIndex;

- (CGSize)displayRectForImage:(CGSize)imageSize;


@end
