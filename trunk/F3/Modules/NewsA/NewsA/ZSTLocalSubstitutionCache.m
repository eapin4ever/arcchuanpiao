
//
//  LocalSubstitutionCache.m
//  News
//
//  Created by luobin on 7/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTLocalSubstitutionCache.h"

static NSString *cacheDirectory;
static NSSet *supportSchemes; 
NSString *cacheScheme;

@implementation ZSTLocalSubstitutionCache

+ (void)initialize {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    cacheDirectory = [[paths objectAtIndex:0] retain];
    supportSchemes = [[NSSet setWithObjects:@"http", @"https", @"ftp", nil] retain]; 
    cacheScheme = @"cache";
}

- (id)initWithMemoryCapacity:(NSUInteger)memoryCapacity diskCapacity:(NSUInteger)diskCapacity diskPath:(NSString *)path {
    if (self = [super initWithMemoryCapacity:memoryCapacity diskCapacity:diskCapacity diskPath:path]) {
        cachedResponses = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (id)init
{
    if (self = [super init]) {
        cachedResponses = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (NSString *)mimeTypeForPath:(NSString *)originalPath
{
	//
	// Current code only substitutes PNG images
	//
	return @"image/png";	
}

- (BOOL)shoudCache:(NSURL *)url
{
    return [[url.scheme lowercaseString] isEqualToString:cacheScheme];
}

- (NSCachedURLResponse *)cachedResponseForRequest:(NSURLRequest *)request
{
    
    if (![self shoudCache:request.URL]) {
        return [super cachedResponseForRequest:request];
    }
    
	//
	// Get the path for the request
	//
	NSString *pathString = [[request URL] absoluteString];
	
	//
	// If we've already created a cache entry for this path, then return it.
	//
	NSCachedURLResponse *cachedResponse = [cachedResponses safeObjectForKey:pathString];
	if (cachedResponse)
	{
		return cachedResponse;
	}
    
    NSDictionary *params = [request.URL.absoluteString queryDictionaryUsingEncoding:NSUTF8StringEncoding];
	NSString *actualURL = [[[params safeObjectForKey:@"actualURL"] stringValue] urlDecodeValue];
    NSString *MIMEType = [params safeObjectForKey:@"MIMEType"];
    NSString *filename = [actualURL md5];
	//
	// Get the path to the substitution file
	//
    NSString *path = [cacheDirectory stringByAppendingPathComponent:filename];  
    NSFileManager *fileManager = [[[NSFileManager alloc] init] autorelease];  
    if ([fileManager fileExistsAtPath:path]) {   
        
        NSData *data = [NSData dataWithContentsOfFile:path];  
        NSURLResponse *response = [[NSURLResponse alloc] initWithURL:request.URL MIMEType:MIMEType expectedContentLength:data.length textEncodingName:nil];  
        cachedResponse = [[NSCachedURLResponse alloc] initWithResponse:response data:data];  
        [response release];  
        
        [cachedResponses setObject:cachedResponse forKey:pathString];  
        [cachedResponse release];  
        NSLog(@"cached: %@", pathString);  
        return cachedResponse;  
    }  
    
    NSMutableURLRequest *newRequest = [NSMutableURLRequest requestWithURL:request.URL cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:request.timeoutInterval];  
    newRequest.allHTTPHeaderFields = request.allHTTPHeaderFields;  
    newRequest.HTTPShouldHandleCookies = request.HTTPShouldHandleCookies; 
    
    //part 4
    /**
     实际上NSMutableURLRequest还有一些其他的属性，不过并不太重要，所以我就只复制了这2个。 
     然后就可以用它来发起请求了。由于UIWebView就是在子线程调用cachedResponseForRequest:的，不用担心阻塞的问题，所以无需使用异步请求： 
     **/
    NSError *error = nil;  
    NSURLResponse *response = nil;  
    NSData *data = [NSURLConnection sendSynchronousRequest:newRequest returningResponse:&response error:&error];  
    if (error) {  
        NSLog(@"%@", error);  
        NSLog(@"not cached: %@", pathString);  
        return nil;  
    }
    
    [fileManager createFileAtPath:path contents:data attributes:nil];  
    
    /**
     接下来还得构造一个NSCachedURLResponse。 
     然而这里还有个陷阱，因为直接使用response对象会无效。我稍微研究了一下，发现它其实是个NSHTTPURLResponse对象，
     可能是它的allHeaderFields属性影响了缓存策略，导致不能重用。 
     **/
    NSURLResponse *newResponse = [[NSURLResponse alloc] initWithURL:response.URL MIMEType:response.MIMEType expectedContentLength:data.length textEncodingName:nil];   
    cachedResponse = [[NSCachedURLResponse alloc] initWithResponse:newResponse data:data];  
    [newResponse release];  
    [cachedResponses setObject:cachedResponse forKey:pathString];  
    [cachedResponse release];  
    return cachedResponse;  
}

- (void)removeCachedResponseForRequest:(NSURLRequest *)request
{
	//
	// Get the path for the request
	//
	NSString *pathString = [[request URL] path];
	if ([cachedResponses safeObjectForKey:pathString])
	{
		[cachedResponses removeObjectForKey:pathString];
	}
	else
	{
		[super removeCachedResponseForRequest:request];
	}
}

- (void)removeAllCachedResponses {
    NSLog(@"removeAllObjects");
    [cachedResponses removeAllObjects];
    [super removeAllCachedResponses];
}

- (void)dealloc
{
	[cachedResponses release];
	cachedResponses = nil;
	[super dealloc];
}

@end
