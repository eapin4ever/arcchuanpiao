//
//  ZSTGlobal+News.h
//  News
//
//  Created by luobin on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    ZSTCategoryType_DefaultIndex = 1,              //默认Category                 
    ZSTCategoryType_Index = 2,                     //展示在首页
    ZSTCategoryType_More = 4                       //展示在更多
} ZSTCategoryType;                                     //Category类型

typedef enum
{
    ZSTIconType_List = 1,                          //列表ICon                 
    ZSTIconType_Broadcast = 2                      //轮播ICon
} ZSTIconType;                                     //icon类型

typedef enum
{
    ZSTNewsTypen_Normal = 0,                       //普通                 
    ZSTNewsType_Photos = 1,                        //图集
    ZSTNewsType_Video = 2,                         //视频
    ZSTNewsType_Special = 3                        //专题
} ZSTNewsType;                                     //新闻信息种类

#define NEWS_PAGE_SIZE 10

#define SortType_Desc @"Desc"               //向新取
#define SortType_Asc @"Asc"                 //向旧取


//////////////////////////////////////////////////////////////////////////////////////////

//#define NewsBaseURL @"http://mod.pmit.cn/NewsAapi/Home"
#define NewsBaseURL @"http://mod.pmit.cn/NewsA"//最新接口


#define GetCatagory NewsBaseURL @"/GetCatagory"
#define GetNewsList NewsBaseURL @"/GetNewsList"
#define GetNewsContent NewsBaseURL @"/GetNewsContent"
#define GetNewsFile NewsBaseURL @"/GetFile"


