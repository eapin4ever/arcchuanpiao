//
//  NewsMainViewController.h
//  F3
//  iphone size:320*480
//  状态栏：20
//  蓝条：44
//  底部Tab：49
//  Created by fujinsheng on 12-7-17.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTNewsMainCell.h"
#import "ZSTF3Engine.h"
#import "ZSTNewsVO.h"
#import "ZSTNewsCarouselView.h"
#import "ZSTNewsContentViewController.h"
#import "ColumnBar.h"
#import "ZSTModuleBaseViewController.h"

@interface ZSTNewsMainViewController : ZSTModuleBaseViewController <UITableViewDelegate,UITableViewDataSource,ZSTNewsCarouselViewDataSource,ZSTNewsCarouselViewDelegate,ColumnBarDelegate,ColumnBarDataSource,EGORefreshTableHeaderDelegate,TKLoadMoreViewDelegate,UIScrollViewDelegate,ZSTNewsCarouselViewCellDelegate>
//ZSTNewsCarouselViewDataSource，ZSTNewsCarouselViewDelegate both unused...tableview is a subClass of scrollview.
{
    EGORefreshTableHeaderView *_refreshHeaderView;
    TKLoadMoreView *_loadMoreView;
    BOOL _isRefreshing;
    BOOL _isLoadingMore;
    BOOL _hasMore;
    BOOL _isFirstLoading;
    
    ColumnBar *_columnBar;
    NSInteger currentSelectItem;//当前选中得分类
    UITableView *newsTable;
    
    
    NSMutableArray *newsArr;//普通列表数组
    NSMutableArray *indexCategoryArr;//导航展示分类数组
    NSArray *broadcastNewsArr;//轮播图数组
    
    NSDictionary *headline;//头条新闻信息
    NSMutableDictionary *_timeDic;//计时字典

    int _pagenum;
}
@property (nonatomic,retain) NSMutableArray *newsArr;

@end
