//
//  ZSTF3Engine+News.m
//  News
//
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTF3Engine+NewsA.h"
#import "ZSTCommunicator.h"
#import "ZSTDao+NewsA.h"
#import "ZSTF3Preferences.h"
#import "ZSTLegacyResponse.h"
#import "ZSTSqlManager.h"
#import "JSON.h"

#define NewsContentCachePath [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"/News/content"]

@implementation ZSTF3Engine (NewsA)

////////////////////////////////////////////////////////////////// category ///////////////////////////////////////////////////////////////////////

- (void)getCategory
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetCatagory stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getCategoryResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:YES];

}

- (void)getCategoryResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id categories = [response.jsonResponse safeObjectForKey:@"Info"];
        if ([categories isKindOfClass:[NSString class]]) {
            categories = [NSArray array];
        }
        
        [self.dao deleteAllCategories];
        
        //入库
        [ZSTSqlManager beginTransaction];
        for (NSMutableDictionary *category in categories){
            NSInteger categoryID = [[category safeObjectForKey:@"CategoryID"] integerValue];
            
            [self.dao addCategory:categoryID
                     categoryName:[category safeObjectForKey:@"CategoryName"]
                         orderNum:[[category safeObjectForKey:@"OrderNum"] intValue]
                      defaultFlag:[[category safeObjectForKey:@"DefaultFlag"] boolValue]];
        }
        [ZSTSqlManager commit];
        
        if ([self isValidDelegateForSelector:@selector(getCategoryDidSucceed:categories:)]) {
            [self.delegate getCategoryDidSucceed:[self.dao getDefaultCategory]
                                      categories:[self.dao getCategories]];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getCategoryDidFailed:)]) {
            [self.delegate getCategoryDidFailed:response.resultCode];
        }
    }
}

////////////////////////////////////////////////////////////////// newsList ///////////////////////////////////////////////////////////////////////

- (void)getNews:(NSInteger)categoryID
        sinceID:(NSInteger)sinceID
      orderType:(NSString *)orderType
     fetchCount:(NSUInteger)fetchCount
       iconType:(ZSTIconType)iconType
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (categoryID) {
        [params setSafeObject:[NSNumber numberWithInteger:categoryID] forKey:@"CategoryID"];
    }
    if (sinceID) {
        [params setSafeObject:[NSNumber numberWithInteger:sinceID] forKey:@"SinceID"];
    }
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:orderType forKey:@"OrderType"];
    [params setSafeObject:@(fetchCount).stringValue forKey:@"Size"];
    [params setSafeObject:[NSNumber numberWithInt:iconType] forKey:@"IConType"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetNewsList stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getNewsResponse:userInfo:)
                                       userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:YES];

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getNewsResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSDictionary *params = [userInfo safeObjectForKey:@"Params"];
    NSString *sortType = [params safeObjectForKey:@"OrderType"];
    NSInteger categoryID = [[params safeObjectForKey:@"CategoryID"] integerValue];
    ZSTIconType iconType = [[params safeObjectForKey:@"IConType"] intValue];
    
    if (response.resultCode == ZSTResultCode_OK) {
        id newsList = [response.jsonResponse safeObjectForKey:@"Info"];
        if ([newsList isKindOfClass:[NSString class]]) {
            newsList = [NSArray array];
        }
        
        if (iconType == ZSTIconType_Broadcast) {
            //删除轮播新闻
            [self.dao deleteAllNewsOfBroadcast];
            if ([self isValidDelegateForSelector:@selector(getBroadcastNewsDidSucceed:)]) {
                [self.delegate getBroadcastNewsDidSucceed:newsList];
            }
            
        } else {
            //如果获取到TOPIC_PAGE_SIZE条(count)，则删除原数据(因为不连续)
            if ([newsList count] == NEWS_PAGE_SIZE && [sortType isEqualToString:SortType_Desc]) {
                [self.dao deleteAllNewsOfCategory:categoryID];
            }
            if ([self isValidDelegateForSelector:@selector(getNewsDidSucceed:isLoadMore:hasMore:isFinish:)]) {
                [self.delegate getNewsDidSucceed:newsList isLoadMore:[sortType isEqualToString:SortType_Asc] hasMore:[[response.jsonResponse safeObjectForKey:@"HasMore"] boolValue] isFinish:YES];
            }
        }
        
        //入库
        [ZSTSqlManager beginTransaction];
        for (NSMutableDictionary *post in newsList) {
            NSInteger msgID = [[post safeObjectForKey:@"MsgID"] integerValue];
            //        BOOL isExist = [[ZSTPetmiiDao shared] postExist:postID];
            
            //服务器返回时间格式 '/Date(1342768136510)/'
            NSString *date = [[[post safeObjectForKey:@"AddTime"] stringByReplacingOccurrencesOfString:@"/Date(" withString:@""] stringByReplacingOccurrencesOfString:@")/" withString:@""];
            
            if (![self.dao newsExist:msgID]) {
                [self.dao addNews:msgID
                       categoryID:[[post safeObjectForKey:@"CategoryID"] integerValue]
                          msgType:[[post safeObjectForKey:@"MsgType"] intValue]
                            title:[post safeObjectForKey:@"Title"]
                          summary:[post safeObjectForKey:@"Summary"]
                       iconFileID:[post safeObjectForKey:@"IConFileID"]
                         iconType:[[post safeObjectForKey:@"IConType"] intValue]
                         orderNum:[[post safeObjectForKey:@"OrderNum"] intValue]
                           source:[post safeObjectForKey:@"Source"]
                          addTime:[NSDate dateWithTimeIntervalSince1970:[date doubleValue]/1000]];
            }
        }
        [ZSTSqlManager commit];
    } else {
        if (iconType == ZSTIconType_Broadcast) {
            if ([self isValidDelegateForSelector:@selector(getBroadcastNewsDidFailed:)]) {
                [self.delegate getBroadcastNewsDidFailed:response.resultCode];
            }
            
        } else {
            if ([self isValidDelegateForSelector:@selector(getNewsDidFailed:)]) {
                [self.delegate getNewsDidFailed:response.resultCode];
            }
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)getBroadcastNews
{
    [self getNews:0
          sinceID:0
        orderType:SortType_Desc
       fetchCount:3
         iconType:ZSTIconType_Broadcast];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getNewsAtPage:(int)pageNum category:(NSInteger)categoryID
{
	//如果是loadMore (pageNum>1),则从本地数据获取
	if (pageNum>1){
        
        NSArray *tempLocalNews = [self.dao newsAtPage:pageNum pageCount:NEWS_PAGE_SIZE category:categoryID];
        if ([tempLocalNews count] == 0) {
            NSInteger newNewsMinId = [self.dao getMinNewsIdOfCategory:categoryID];
            //            NSInteger newNewsMinId = [[[[ZSTDao shared] newsAtIndex:(pageNum - 1) * NEWS_PAGE_SIZE category:categoryID] objectForKey:@"MsgID"] integerValue];
            //            if ([self isValidDelegateForSelector:@selector(getNewsResponse:isLoadMore:hasMore:isFinish:)]) {
            //                [self.delegate getNewsResponse:tempLocalNews isLoadMore:YES  hasMore:YES isFinish:NO];
            //            }
            [self getNews:categoryID
                  sinceID:newNewsMinId
                orderType:SortType_Asc
               fetchCount:NEWS_PAGE_SIZE
                 iconType:ZSTIconType_List];
        } else {
            if ([self isValidDelegateForSelector:@selector(getNewsDidSucceed:isLoadMore:hasMore:isFinish:)]) {
                BOOL hasmore = ([tempLocalNews count] >= NEWS_PAGE_SIZE);
                [self.delegate getNewsDidSucceed:tempLocalNews isLoadMore:YES  hasMore:hasmore isFinish:YES];
            }
        }
	} else {
        //        NSInteger dbMaxId = [[ZSTDao shared] getMaxNewsIdOfCategory:categoryID];
        [self getNews:categoryID
              sinceID:0
            orderType:SortType_Desc
           fetchCount:NEWS_PAGE_SIZE
             iconType:ZSTIconType_List];
	}
}

////////////////////////////////////////////////////////////////// News content /////////////////////////////////////////////////////////////////////

//新闻内容缓存到文件中
- (void)getNewsContent:(NSInteger)newsID userInfo:(id)anUserInfo
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSError *error;
    if (![fileManager fileExistsAtPath:NewsContentCachePath]) {
        if (![fileManager createDirectoryAtPath:NewsContentCachePath withIntermediateDirectories:YES attributes:nil error:&error]) {
            if ([self isValidDelegateForSelector:@selector(requestDidFail:)]) {
                ZSTLegacyResponse *response = [ZSTLegacyResponse responseWithJSONResponse:nil
                                                                           stringResponse:nil
                                                                                    error:error
                                                                                 userInfo:nil];
                [self.delegate performSelector:@selector(requestDidFail:) withObject:response afterDelay:0];
            }
            return;
        }
    }
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:newsID], @"newsID", anUserInfo, @"userInfo", nil];
    
    //检查缓存是否有数据,如果有直接从缓存获取
    NSString *contentPath = [NewsContentCachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.json", @(newsID)]];
    BOOL isDirectory;
    if ([fileManager fileExistsAtPath:contentPath isDirectory:&isDirectory]) {
        if (!isDirectory) {
            NSDictionary *attributes = [fileManager attributesOfItemAtPath:contentPath error:&error];
            if (attributes && [attributes fileSize]) {
                NSString *content = [NSString stringWithContentsOfFile:contentPath encoding:NSUTF8StringEncoding error:&error];
                if (content) {
                    id json = [content JSONValue];
                    if (json) {
                        [self performSelector:@selector(getNewsContentResponse:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:json, @"json", userInfo, @"userInfo", nil] afterDelay:0.5];
                        return;
                    }
                    
                } else {
                    TKDERROR(@"error == %@", error);
                }
            } else {
                TKDERROR(@"error == %@", error);
            }
        }
    }
    //从缓存获取失败，删除缓存，重新从网络获取
    [fileManager removeItemAtPath:contentPath error:&error];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[NSNumber numberWithInteger:newsID] forKey:@"MsgID"];
    [[ZSTCommunicator shared] openAPIPostToPath:[GetNewsContent stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getNewsContentResponse:userInfo:)
                                       userInfo:userInfo
                                      matchCase:YES];

}

- (void)getNewsContentResponse:(NSDictionary *)param
{
    id results = [param safeObjectForKey:@"json"];
    id userInfo = [param safeObjectForKey:@"userInfo"];
    if ([self isValidDelegateForSelector:@selector(getNewsContentDidSucceed:userInfo:)]) {
        [self.delegate getNewsContentDidSucceed:results userInfo:[userInfo safeObjectForKey:@"userInfo"]];
    }
}

- (void)getNewsContentResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        [self performSelector:@selector(getNewsContentResponse:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:response.jsonResponse, @"json", userInfo, @"userInfo", nil] afterDelay:0.5];
        NSNumber *newsID = [userInfo safeObjectForKey:@"newsID"];
        
        NSString *contentPath = [NewsContentCachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.json", newsID]];
        NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:response.jsonResponse, @"results", contentPath, @"contentPath", nil];
        [self performSelector:@selector(writeArticleBContentToCache:) withObject:param afterDelay:0];
    } else {
        if ([self isValidDelegateForSelector:@selector(getNewsContentDidFailed:userInfo:)]) {
            [self.delegate getNewsContentDidFailed:response.resultCode userInfo:userInfo];
        }
    }
}

- (void) writeArticleBContentToCache:(NSDictionary *)param
{
    NSError *error = nil;
    if (![[[param safeObjectForKey:@"results"] JSONRepresentation] writeToFile:[param safeObjectForKey:@"contentPath"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        TKDERROR(@"Write news content failed, error == %@", error);
    }
}

@end
