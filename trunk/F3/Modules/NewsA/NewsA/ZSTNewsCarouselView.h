//
//  ZSTNewsCarouselView.h
//  F3
//
//  Created by xuhuijun on 12-7-18.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TKAsynImageView.h"

@class ZSTNewsCarouselView;

@protocol ZSTNewsCarouselViewDataSource <NSObject>

@optional
- (NSInteger)numberOfViewsInCarouselView:(ZSTNewsCarouselView *)newsCarouselView;
- (NSDictionary *)carouselView:(ZSTNewsCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index;

@end

@protocol ZSTNewsCarouselViewDelegate <NSObject>

@optional
- (void)carouselView:(ZSTNewsCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;

@end

@interface ZSTNewsCarouselView : UIView <HJManagedImageVDelegate,UIScrollViewDelegate,TKAsynImageViewDelegate>
{
    UIScrollView *_scrollView;
    UILabel *_describeLabel;
    UIPageControl *_pageControl;
    NSTimer *_carouselTimer;
    
    NSInteger _totalPages;
    NSInteger _curPage;
    
    NSMutableArray *_curSource;
    
    id<ZSTNewsCarouselViewDataSource> _carouselDataSource;
    id<ZSTNewsCarouselViewDelegate>  _carouselDelegate;
}

@property(nonatomic, assign) id<ZSTNewsCarouselViewDataSource> carouselDataSource;
@property(nonatomic, assign) id<ZSTNewsCarouselViewDelegate> carouselDelegate;

- (void)reloadData;
//- (void)selectTabAtIndex:(int)index;
- (void)carouselViewStopAnimation;

@end
