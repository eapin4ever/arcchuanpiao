//
//  NewsVO.h
//  F3Engine
//
//  Created by admin on 12-7-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTNewsVO : NSObject

@property (nonatomic, retain) NSString *title;//主标题
@property (nonatomic, retain) NSString *summary;//副标题
@property (nonatomic, retain) NSNumber *iconFileID;//图片id
@property (nonatomic, retain) NSNumber *iconType;//图片类型
@property (nonatomic, retain) NSString *addTime;//日期
@property (nonatomic, retain) NSString *source;//来源
@property (nonatomic, retain) NSNumber *categoryID;//分类id
@property (nonatomic, retain) NSNumber *msgID;//消息id
@property (nonatomic, retain) NSNumber *msgType;//消息类型
@property (nonatomic, retain) NSString *orderNum;//排列顺序
@property (nonatomic, assign) BOOL isRead;//排列顺序

+ (id)voWithDic:(NSDictionary *)dic;
- (id)initWithDic:(NSDictionary *)dic;


@end
