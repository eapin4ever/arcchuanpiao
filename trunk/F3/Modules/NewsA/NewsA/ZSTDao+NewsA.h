//
//  ZSTDao.h
//  News
//  
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ZSTGlobal+NewsA.h"

@interface ZSTDao(NewsA) 

- (void)createTableIfNotExistForInfoaModule;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief 添加分类
 *
 *	@param 	categoryID      分类ID
 *	@param 	categoryName 	分类名称
 *	@param 	orderNum        排序字段
 *  @param  defaultFlag     是否是默认头条
 *	@return	操作是否成功
 */
- (BOOL)addCategory:(NSInteger)categoryID
       categoryName:(NSString *)categoryName
           orderNum:(NSInteger)orderNum
        defaultFlag:(BOOL)defaultFlag;

/**
 *	@brief 删除分类
 *
 *	@return	操作是否成功
 */
- (BOOL)deleteAllCategories;

/**
 *	@brief 获取默认缺省分类
 *
 *	@return	操作是否成功
 */
- (NSDictionary *)getDefaultCategory;

/**
 *	@brief  获取所有分类
 *	@return	操作是否成功
 */
- (NSArray *)getCategories;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief 添加新闻
 *  
 *  @param 	msgID           新闻ID
 *	@param 	categoryID      分类ID
 *	@param 	msgType         新闻信息种类，0：普通；1：图集；2：视频；3：专题
 *	@param 	title           新闻标题
 *	@param 	summary         新闻信息摘要
 *	@param 	iconFileID      ICon图片编号
 *	@param 	iconType        ICon图片类别，1：列表ICon；2：轮播ICon
 *	@param 	orderNum        新闻信息来源
 *  @param  addTime         添加时间
 *	@return	操作是否成功
 */
- (BOOL)addNews:(NSInteger)msgID
     categoryID:(NSInteger)categoryID
        msgType:(int)msgType
          title:(NSString *)title
        summary:(NSString *)summary
     iconFileID:(NSString *)iconFileID
       iconType:(NSInteger)iconType
       orderNum:(NSInteger)orderNum
         source:(NSString *)source
        addTime:(NSDate *)addTime;

/**
 *	@brief  设置新闻是否已读
 *
 *  @param  isRead         设置是否已读
 *	@return	操作是否成功
 */
- (BOOL)setNews:(NSInteger)newsID isRead:(BOOL)isRead;

/**
 *	@brief  获取新闻是否已读
 *
 *  @param  isRead         设置是否已读
 *	@return	新闻是否已读
 */
- (BOOL)getNewsIsRead:(NSInteger)newsID;

/**
 *	@brief 删除新闻
 *
 *	@return	操作是否成功
 */
- (BOOL)deleteNews:(NSInteger)newsID;

/**
 *	@brief 删除所有轮播新闻
 *
 *	@return	操作是否成功
 */
- (BOOL)deleteAllNewsOfBroadcast;

/**
 *	@brief 删除指定分类的新闻
 *
 *	@return	操作是否成功
 */
- (BOOL)deleteAllNewsOfCategory:(NSInteger)categoryID;

/**
 *	@brief 判断新闻是否已存在
 *
 *	@return	新闻是否存在
 */
- (BOOL)newsExist:(NSInteger)newsID;

/**
 *	@brief 获取分类下第index条新闻
 *
 *	@return	新闻
 */
- (NSDictionary *)newsAtIndex:(int)index category:(NSInteger)categoryID;

/**
 *	@brief 获取轮播新闻
 *  
 *	@return	轮播新闻
 */
- (NSArray *)getBroadcastNews;

/**
 *	@brief 获取新闻
 *  
 *  @param 	pageNum             页数
 *	@param 	pageCount           每页新闻条数
 *	@param 	categoryID          分类ID
 *	@return	新闻列表
 */
- (NSArray *)newsAtPage:(int)pageNum pageCount:(int)pageCount category:(NSInteger)categoryID;

/**
 *	@brief 返回分类下的新闻条数
 *  
 *	@return	新闻条数
 */
- (NSUInteger)newsCountOfCategory:(NSInteger)categoryID;

/**
 *	@brief 返回分类下的最新的新闻的ID
 *  
 *	@return	分类下的最新的新闻的ID
 */
- (NSUInteger) getMaxNewsIdOfCategory:(NSInteger)categoryID;

- (NSUInteger) getMinNewsIdOfCategory:(NSInteger)categoryID;

@end
