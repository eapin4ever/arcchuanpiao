//
//  ZSTShellECarouselView.h
//  ShellE
//
//  Created by zhangwanqiang on 14-3-27.
//  Copyright (c) 2014年 zhangwanqiang. All rights reserved.
//


//@interface ZSTShellECarouselView : UIView

//
//  ZSTNewsCarouselView.h
//  F3
//
//  Created by xuhuijun on 12-7-18.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TKAsynImageView.h"

@class ZSTShellECarouselView;

@protocol ZSTShellECarouselViewDataSource <NSObject>

@optional
- (NSInteger)numberOfViewsInCarouselView:(ZSTShellECarouselView *)newsCarouselView;
- (NSDictionary *)carouselView:(ZSTShellECarouselView *)carouselView infoForViewAtIndex:(NSInteger)index;

@end

@protocol ZSTShellECarouselViewDelegate <NSObject>

@optional
- (void)carouselView:(ZSTShellECarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;

@end

@interface ZSTShellECarouselView : UIView <HJManagedImageVDelegate,UIScrollViewDelegate,TKAsynImageViewDelegate>
{
    UIScrollView *_scrollView;
    UILabel *_describeLabel;
    TKPageControl *_pageControl;
    NSTimer *_carouselTimer;
    
    NSInteger _totalPages;
    NSInteger _curPage;
    
    NSMutableArray *_curSource;
    
    id<ZSTShellECarouselViewDataSource> _carouselDataSource;
    id<ZSTShellECarouselViewDelegate>  _carouselDelegate;
}

@property(nonatomic, assign) id<ZSTShellECarouselViewDataSource> carouselDataSource;
@property(nonatomic, assign) id<ZSTShellECarouselViewDelegate> carouselDelegate;

- (void)reloadData;
- (void)stopAnimation;
- (void)startAnimation;

@end
