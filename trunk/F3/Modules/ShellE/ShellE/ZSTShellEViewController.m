//
//  ZSTShellEViewController.m
//  ShellE
//
//  Created by zhangwanqiang on 14-3-27.
//  Copyright (c) 2014年 xuhuijun. All rights reserved.
//

#import "ZSTShellEViewController.h"


//
//  ZSTShellBViewController.m
//  shellB
//
//  Created by xuhuijun on 13-1-11.
//
//

#import "ZSTShellEViewController.h"
#import "ZSTShellESpeedBar.h"
#import "ZSTF3ClientAppDelegate.h"
#import "ZSTF3Engine+ShellE.h"
#import "ElementParser.h"
#import "ZSTModuleManager.h"
#import "ZSTUtils.h"
#import "ZSTWebViewController.h" 
#import "ZSTMineViewController.h"

@interface ZSTShellEViewController ()

{
    TKPageControl *_pageControl;
    int _currentPage;
    NSMutableArray * _speedBarDataArr;
    BOOL _isRolled;
    float roundWidth;//圆宽
    float roundSpace;//圆边距
    float stepPace;//一次移动步长
    float oncePages;//一次加载页数
    float origionalOffsetX;//初始偏移量
    float pointX;//记录偏移量
    float contextsizeWidth;//ScrollerView可滑动的范围
    float rightMaxContextOffset;//右移最大的偏移量 大于这个值翻右页
    float leftMaxContextOffset;//左边最大偏移量 小于这个值翻左页
}
@end

@implementation ZSTShellEViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void) initializeData
{
    _isRolled = NO;
    _currentPage = 0;
    _speedBarDataArr = [[NSMutableArray alloc]init];
    [self.engine getShellEAD];
    roundWidth = 170 + (iPhone5?40:0);
    roundSpace = 50 - (iPhone5?20:0);
    oncePages = 5;
    stepPace = roundWidth + roundSpace;
    origionalOffsetX = roundWidth - (160 - roundWidth/2 - roundSpace);
    contextsizeWidth = oncePages * roundWidth + (oncePages - 1) * roundSpace - origionalOffsetX * 2;
    rightMaxContextOffset = roundWidth*3 + 3*roundSpace + roundWidth - origionalOffsetX - 320 - roundWidth/2;
    leftMaxContextOffset = roundWidth* 3/2 + roundSpace  - origionalOffsetX;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initializeData];
    
    self.view.backgroundColor = [UIColor lightGrayColor];
    
    self.carouselView = [[ZSTShellECarouselView alloc] initWithFrame:CGRectMake(0, 0, 320, 160)];
    self.carouselView.carouselDataSource = self;
    self.carouselView.carouselDelegate = self;
    [self.view addSubview:self.carouselView];
    
    //    UIImageView *backgroundImg2 = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_shellb_firstBackground.png")];
    //    backgroundImg2.frame = CGRectMake(0, CGRectGetMaxY(self.carouselView.frame), 320, 207);
    //    backgroundImg2.contentMode = UIViewContentModeScaleToFill;
    //    [self.view addSubview:backgroundImg2];
    //    [backgroundImg2 release];
    
    UIImageView *backgroundImg3 = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_shelle_mainView_background.png")];
    backgroundImg3.frame = CGRectMake(0, CGRectGetMaxY(self.carouselView.frame)-1, 320, 14);
    backgroundImg3.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:backgroundImg3];
    [backgroundImg3 release];
    
    self.speedBarScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.carouselView.frame), 320, 480 - 20 - 44 - 49 - 160+(iPhone5?88:0))];
    self.speedBarScrollView.delegate = self;
    self.speedBarScrollView.showsHorizontalScrollIndicator = NO;
    self.speedBarScrollView.showsVerticalScrollIndicator = NO;
    self.speedBarScrollView.backgroundColor = [UIColor clearColor];
    self.speedBarScrollView.pagingEnabled = NO;
    [self.view addSubview:self.speedBarScrollView];
    
    _pageControl = [[TKPageControl alloc] initWithFrame:CGRectMake(0, CGRectGetMinY(self.speedBarScrollView.frame) + 8, 320, 10)];
    _pageControl.type = TKPageControlTypeOnImageOffImage;
    _pageControl.numberOfPages = 0;
    _pageControl.dotSize = 4;
    _pageControl.dotSpacing = 8;
    _pageControl.onImage = ZSTModuleImage(@"module_shelle_pageControlOn.png");
    _pageControl.offImage = ZSTModuleImage(@"module_shelle_pageControlOff.png");
    _pageControl.backgroundColor = [UIColor clearColor];
    _pageControl.userInteractionEnabled = NO;
//    [self.view addSubview:_pageControl];
    _pageControl.hidden = YES;
    
    [self imageButtonElementsForView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.rightBarButtonItem = [self initWithSubviews];
}

- (UIBarButtonItem *) initWithSubviews
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(mineAction:) forControlEvents:UIControlEventTouchUpInside];
    
    btn.layer.cornerRadius = btn.frame.size.width / 2.0;
    btn.layer.borderColor = [UIColor clearColor].CGColor;
    btn.layer.masksToBounds = YES;
    
    NSString *path = nil;
    UIImage *image = nil;
    NSDictionary *dic = [ZSTF3Preferences shared].avaterName;
    
    if (![ZSTF3Preferences shared].loginMsisdn || [ZSTF3Preferences shared].loginMsisdn.length == 0) {
        
        [ZSTF3Preferences shared].loginMsisdn = @"";
    }
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0) {
        
        image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
    } else {
        
        if (dic || [dic isKindOfClass:[NSDictionary class]]) {
            path = [dic safeObjectForKey:[ZSTF3Preferences shared].loginMsisdn];
        }
        
        if (path && path.length > 0) {
            
            NSString *homePath = NSHomeDirectory();
            NSString *imgTempPath = [path substringFromIndex:[path rangeOfString:@"tmp"].location];
            NSString *finalPath = [NSString stringWithFormat:@"%@/%@",homePath,imgTempPath];
            NSData *reader = [NSData dataWithContentsOfFile:finalPath];
            image = [UIImage imageWithData:reader];
            
            if (!image) {
                
                image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
            }
            
        } else {
            
            image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
        }
    }
    
    [btn setImage:image forState:UIControlStateNormal];
    
    //暂时解决btn图片不能显示的问题
    UIImageView *iv = [[UIImageView alloc] initWithFrame:btn.imageView.frame];
    iv.image = image;
    [btn insertSubview:iv aboveSubview:btn.imageView];
    
    return [[[UIBarButtonItem alloc] initWithCustomView:btn] autorelease];
}


- (void)mineAction:(id)sender
{
    ZSTMineViewController *controller = [[ZSTMineViewController alloc] initWithNibName:@"ZSTMineViewController" bundle:nil];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

- (void) showGuideView
{
    UIView *guideView = [[UIView alloc] initWithFrame:CGRectMake(0,0 , 320, 480+(iPhone5?88:0))];
    guideView.backgroundColor = [UIColor blackColor];
    guideView.tag = 9999999;
    guideView.alpha = 0.7f;
    
    UIImageView *guideTextImgView = [[UIImageView alloc] initWithFrame:CGRectMake(113,250+(iPhone5?88:0) , 93, 44)];
    guideTextImgView.image = ZSTModuleImage(@"module_shelle_BeginnersGuide.png");
    [guideView addSubview:guideTextImgView];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:FIRST_LAUNCH_ShellE];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(removeGuideView)];
    [guideView addGestureRecognizer:singleTap];
    
    [self.view addSubview:guideView];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}
- (void) removeGuideView
{
    [[[UIApplication sharedApplication].keyWindow viewWithTag:9999999] removeFromSuperview];
}

- (void)imageButtonElementsForView
{
    
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module_shelle_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSArray *itemElements = [root selectElements:@"ImageButton Item"];
    if (![[NSUserDefaults standardUserDefaults] valueForKey:FIRST_LAUNCH_ShellE]
        && [itemElements count] > 1) {
        [self showGuideView];
    }
    
    
    // 计算PageCount
    NSUInteger pageCount = [itemElements count];//[itemElements count] / (rowCount * columnCount);
    if ([itemElements count] > 1) {
        _pageControl.hidden = NO;
        _pageControl.numberOfPages = pageCount;
    }
    
    for (int i = 0; i < [itemElements count]; i++) {
        Element *itemElement = [[itemElements objectAtIndex:i] retain];
        
        NSString *btnName = [[itemElement selectElement:@"Title"] contentsText];
        NSNumber *moduleID = [[itemElement selectElement:@"ModuleId"] contentsNumber];
        NSNumber *moduleType = [[itemElement selectElement:@"ModuleType"] contentsNumber];
        NSString *myTitleColor = [[itemElement selectElement:@"TitleColor"] contentsText];
        if (myTitleColor == nil || [myTitleColor length] == 0 || [myTitleColor isEqualToString:@""]) {
            myTitleColor = @"#000000";
        }
        
        NSDictionary *paramDic = [NSDictionary dictionary];
        if (moduleID.intValue == 12 || moduleID.intValue == 24) {
            NSString *interfaceUrl = [[itemElement selectElement:@"InterfaceUrl"] contentsText];
            paramDic = [NSDictionary dictionaryWithObjectsAndKeys:btnName,@"Title",moduleType,@"type",interfaceUrl,@"InterfaceUrl",myTitleColor,@"TitleColor",moduleID,@"moduleID",nil];
        }else{
            paramDic = [NSDictionary dictionaryWithObjectsAndKeys:btnName,@"Title",moduleType,@"type",myTitleColor,@"TitleColor",moduleID,@"moduleID", nil];
        }
        [_speedBarDataArr addObject:paramDic];
        
    }
    [parser release];
    [self setImageButtons:0];
    //oncePages * roundWidth + (oncePages - 1) * roundSpace
    _speedBarScrollView.contentSize = CGSizeMake(contextsizeWidth, _speedBarScrollView.frame.size.height);
}
-(void)setImageButtons:(int)centerPage
{
    NSInteger totalPage = [_speedBarDataArr count];
    
    for (NSInteger i = 0; i < 5; i++) {
        NSInteger origional = (i - 2 + totalPage + _currentPage)%totalPage;
        NSLog(@"加载<%@>页",@(origional));
        NSDictionary * paramDic = [_speedBarDataArr objectAtIndex:origional];
        float x = (roundSpace + roundWidth) * i;
        float y = (_speedBarScrollView.frame.size.height - roundWidth)/2;
        
        ZSTShellESpeedBar *bar = [[[ZSTShellESpeedBar alloc] init] autorelease];
        bar.speedBtn.tag = [[paramDic objectForKey:@"moduleID"] intValue];
        bar.speedBtn.titleLabel.text = [paramDic objectForKey:@"Title"];
        
        bar.delegate = self;
        NSString *normalImagePath = [NSString stringWithFormat:@"module_shelle_icon%@_n.png", @(origional+1)];
        NSString *selectedImagePath = [NSString stringWithFormat:@"module_shelle_icon%@_p.png", @(origional+1)];
        
        [bar configSpeedBarNormalImage:ZSTModuleImage(normalImagePath)
                         selectedImage:ZSTModuleImage(selectedImagePath)
                                 param:paramDic];
        
        
        bar.frame = CGRectMake(x - origionalOffsetX, y, roundWidth, roundWidth);
        [self.speedBarScrollView addSubview:bar];

    }
    [_speedBarScrollView setContentOffset:CGPointMake(stepPace, 0) animated:YES];
}
-(void)cleanSpeedBarScrollView
{
    for (ZSTShellESpeedBar * speedBar in _speedBarScrollView.subviews) {
        [speedBar removeFromSuperview];
//        [speedBar release];
    }
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    _isRolled = decelerate;
    if (!decelerate) {
        if (scrollView.contentOffset.x > rightMaxContextOffset) {
            [self cleanSpeedBarScrollView];
            _speedBarScrollView.contentOffset = CGPointMake(scrollView.contentOffset.x - stepPace, 0);
            _currentPage ++;
            _currentPage %= [_speedBarDataArr count];
            [self setImageButtons:_currentPage];
            //        _currentPage ++;
            //        [self removeViewsFromSpeedBarDataArr];
            //        [self setImageButtons:_currentPage];
        }
        else if(scrollView.contentOffset.x < leftMaxContextOffset)
        {
            [self cleanSpeedBarScrollView];
            _speedBarScrollView.contentOffset = CGPointMake(scrollView.contentOffset.x + stepPace, 0);
            _currentPage --;
            _currentPage += [_speedBarDataArr count];
            _currentPage %= [_speedBarDataArr count];
            [self setImageButtons:_currentPage];
            //       _speedBarScrollView.contentOffset = CGPointMake(0, 0);
            //        _currentPage ++;
            //        [self removeViewsFromSpeedBarDataArr];
            //        [self setImageButtons:_currentPage];
        }
        else
        {
            [_speedBarScrollView setContentOffset:CGPointMake(stepPace, 0) animated:YES];
        }
    }
    else
    {
//        if (scrollView.contentOffset.x > rightMaxContextOffset) {
//            [self cleanSpeedBarScrollView];
//            _speedBarScrollView.contentOffset = CGPointMake(scrollView.contentOffset.x - stepPace, 0);
//            _currentPage ++;
//            _currentPage %= [_speedBarDataArr count];
//            [self setImageButtons:_currentPage];
//            //        _currentPage ++;
//            //        [self removeViewsFromSpeedBarDataArr];
//            //        [self setImageButtons:_currentPage];
//        }
//        else if(scrollView.contentOffset.x < leftMaxContextOffset)
//        {
//            [self cleanSpeedBarScrollView];
//            _speedBarScrollView.contentOffset = CGPointMake(scrollView.contentOffset.x + stepPace, 0);
//            _currentPage --;
//            _currentPage += [_speedBarDataArr count];
//            _currentPage %= [_speedBarDataArr count];
//            [self setImageButtons:_currentPage];
//            //       _speedBarScrollView.contentOffset = CGPointMake(0, 0);
//            //        _currentPage ++;
//            //        [self removeViewsFromSpeedBarDataArr];
//            //        [self setImageButtons:_currentPage];
//        }

    }
    if (!_pageControl.hidden) {
        _pageControl.currentPage = _currentPage;
    }

    _isRolled?NSLog(@"有加速度"):NSLog(@"无加速度");
}
-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    NSLog(@"滚动停止");
}
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if (_isRolled) {
        
    
    
        if (scrollView.contentOffset.x > rightMaxContextOffset) {
            [self cleanSpeedBarScrollView];
            _speedBarScrollView.contentOffset = CGPointMake(scrollView.contentOffset.x - stepPace, 0);
            _currentPage ++;
            _currentPage %= [_speedBarDataArr count];
            [self setImageButtons:_currentPage];
            //        _currentPage ++;
            //        [self removeViewsFromSpeedBarDataArr];
            //        [self setImageButtons:_currentPage];
        }
        else if(scrollView.contentOffset.x < leftMaxContextOffset)
        {
            [self cleanSpeedBarScrollView];
            _speedBarScrollView.contentOffset = CGPointMake(scrollView.contentOffset.x + stepPace, 0);
            _currentPage --;
            _currentPage += [_speedBarDataArr count];
            _currentPage %= [_speedBarDataArr count];
            [self setImageButtons:_currentPage];
            //       _speedBarScrollView.contentOffset = CGPointMake(0, 0);
            //        _currentPage ++;
            //        [self removeViewsFromSpeedBarDataArr];
            //        [self setImageButtons:_currentPage];
        }
        else
        {
            [_speedBarScrollView setContentOffset:CGPointMake(stepPace, 0) animated:YES];
        }
    }
    if (!_pageControl.hidden) {
        _pageControl.currentPage = _currentPage;
    }

    NSLog(@"第《%d》",_currentPage);
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    if (!_pageControl.hidden) {
//        _pageControl.currentPage = _currentPage;
//    }
    
}

#pragma mark ZSTShellESpeedBarDelegate

- (void)zstShellESpeedBar:(ZSTShellESpeedBar *)speedBar withParam:(NSDictionary *)param
{
    NSMutableDictionary *moduleParams = [NSMutableDictionary dictionaryWithDictionary:param];
    
    ZSTModule *module = [[ZSTModuleManager shared] findModule:speedBar.speedBtn.tag];
    if (module) {
        UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:speedBar.speedBtn.tag withOptions:moduleParams];
        if (controller) {
            controller.hidesBottomBarWhenPushed = YES;
            controller.navigationItem.titleView = [ZSTUtils titleViewWithTitle:speedBar.titleLabel.text];
            controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}


#pragma mark ZSTF3EngineShellEDelegate

- (void)getShellEADDidSucceed:(NSArray *)ADArray
{
    self.ADArray = [ADArray retain];
    [self.carouselView reloadData];
}
- (void)getShellEADDidFailed:(int)resultCode
{
    [TKUIUtil alertInView:self.view withTitle:@"获取广告失败！" withImage:nil];
}


#pragma mark - ZSTShellECarouselViewDataSource

- (NSInteger)numberOfViewsInCarouselView:(ZSTShellECarouselView *)newsCarouselView;
{
    return [self.ADArray count];
}

- (NSDictionary *)carouselView:(ZSTShellECarouselView *)carouselView infoForViewAtIndex:(NSInteger)index
{
    if ([self.ADArray  count] != 0) {
        return [self.ADArray  objectAtIndex:index];
    }
    return nil;
}

#pragma mark - ZSTShellECarouselViewDelegate

- (void)carouselView:(ZSTShellECarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;
{
    NSString *urlPath = [[self.ADArray objectAtIndex:index] safeObjectForKey:@"ad_linkurl"];
    if (urlPath == nil || [urlPath length] == 0 || [urlPath isEmptyOrWhitespace]) {
        return;
    }
    
    ZSTWebViewController *webViewController = [[ZSTWebViewController alloc] init];
    [webViewController setURL:urlPath];
    webViewController.type = self.moduleType;
    webViewController.hidesBottomBarWhenPushed = YES;
    webViewController.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
    [self.navigationController pushViewController:webViewController animated:NO];
    [webViewController release];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc
{
    [_pageControl release];
    [_carouselView release];
    [_speedBarScrollView release];
    [super dealloc];
}

@end
