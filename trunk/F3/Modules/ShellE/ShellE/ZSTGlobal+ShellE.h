//
//  ZSTGlobal+ShellE.h
//  ShellE
//
//  Created by zhangwanqiang on 14-3-27.
//  Copyright (c) 2014年 zhangwanqiang. All rights reserved.
//



#import <Foundation/Foundation.h>


#define ShellEBaseURL @"http://mod.pmit.cn/shelle"

#define GetShellEAD ShellEBaseURL @"/GetShellEInfo"

#define FIRST_LAUNCH_ShellE   @"FIRST_LAUNCH_ShellE"
