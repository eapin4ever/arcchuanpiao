//
//  ZSTF3Engine+ShellE.m
//  ShellE
//
//  Created by zhangwanqiang on 14-3-27.
//  Copyright (c) 2014年 zhangwanqiang. All rights reserved.
//

#import "ZSTF3Engine+ShellE.h"

#import "ZSTCommunicator.h"
#import "ZSTGlobal+ShellE.h"

@implementation ZSTF3Engine (ShellE)


- (void)getShellEAD
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    [params setSafeObject:@([ZSTF3Preferences shared].shellId).stringValue forKey:@"moduleid"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:GetShellEAD
                                         params:params
                                         target:self
                                       selector:@selector(getShellEADResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:YES];
}


- (void)getShellEADResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id shellEList = [response.jsonResponse safeObjectForKey:@"info"];
        if (![shellEList isKindOfClass:[NSArray class]]) {
            shellEList = [NSArray array];
        }
        if ([self isValidDelegateForSelector:@selector(getShellEADDidSucceed:)]) {
            [self.delegate getShellEADDidSucceed:shellEList];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getShellEADDidFailed:)]) {
            [self.delegate getShellEADDidFailed:response.resultCode];
        }
    }
    
}

@end
