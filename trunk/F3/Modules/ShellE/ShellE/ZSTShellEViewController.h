//
//  ZSTShellEViewController.h
//  ShellB
//
//  Created by zhangwanqiang on 14-3-27.
//  Copyright (c) 2014年 xuhuijun. All rights reserved.
//

//ZSTShellEViewController

#import "ZSTModuleBaseViewController.h"
#import "ZSTGlobal+ShellE.h"
#import "ZSTShellECarouselView.h"
#import "ZSTShellESpeedBar.h"

@interface ZSTShellEViewController : ZSTModuleBaseViewController<ZSTShellECarouselViewDataSource,ZSTShellECarouselViewDelegate,ZSTShellESpeedBarDelegate,UIScrollViewDelegate>

@property (strong)ZSTShellECarouselView *carouselView;
@property (strong)NSArray *ADArray;
@property (strong,nonatomic) UIScrollView *speedBarScrollView;
@end
