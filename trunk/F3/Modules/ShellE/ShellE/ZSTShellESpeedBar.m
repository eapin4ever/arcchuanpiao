//
//  ZSTShellESpeedBar.m
//  ShellE
//
//  Created by zhangwanqiang on 14-3-27.
//  Copyright (c) 2014年 zhangwanqiang. All rights reserved.
//


#import "ZSTShellESpeedBar.h"
#import "ZSTUtils.h"

@implementation ZSTShellESpeedBar

- (void)dealloc
{
    [_titleLabel release];
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius = 85;
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor yellowColor];
        self.layer.borderColor = [UIColor whiteColor].CGColor;
        self.layer.borderWidth = 3;
//        self.frame = CGRectMake(0, 0, 200 , 200);
        self.speedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//      self.speedBtn.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        [self.speedBtn addTarget:self action:@selector(selectSpeedBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.speedBtn];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-4, 59+8, 70, 21)];
//        self.titleLabel.backgroundColor = [UIColor clearColor];
//        self.titleLabel.textAlignment = NSTextAlignmentCenter;
//        self.titleLabel.font = [UIFont systemFontOfSize:11];
//        self.titleLabel.textColor = [UIColor colorWithRed:66/255 green:66/255 blue:66/255 alpha:1];
//        [self addSubview:self.titleLabel];
    }
    return self;
}
-(void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    self.speedBtn.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
    self.layer.cornerRadius = frame.size.width/2.0f;
}

- (void)configSpeedBarNormalImage:(UIImage *)normalImage
                    selectedImage:(UIImage *)selectedImage
                            param:(NSDictionary *)param
{
    [self.speedBtn setBackgroundImage:normalImage forState:UIControlStateNormal];
    [self.speedBtn setBackgroundImage:selectedImage forState:UIControlStateHighlighted];
    [self.titleLabel setText:[param safeObjectForKey:@"Title"]];
//    self.titleLabel.textColor = [ZSTUtils colorFromHexColor:[param safeObjectForKey:@"TitleColor"]];
    self.moduleParams = param;
}

- (void)selectSpeedBtn:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(zstShellESpeedBar:withParam:)]) {
        [self.delegate zstShellESpeedBar:self withParam:self.moduleParams];
    }
}


@end
