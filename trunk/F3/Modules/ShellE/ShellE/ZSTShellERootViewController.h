//
//  ZSTShellERootViewController.h
//  ShellE
//
//  Created by zhangwanqiang on 14-3-26.
//  Copyright (c) 2014年 zhangwanqiang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTModule.h"
#import "ZSTModuleDelegate.h"
#import "ZSTDao.h"


@interface ZSTShellERootViewController : UITabBarController<ZSTModuleDelegate,UINavigationControllerDelegate,UITabBarControllerDelegate>
@property (nonatomic, retain) UINavigationController *navigationController;
@end
