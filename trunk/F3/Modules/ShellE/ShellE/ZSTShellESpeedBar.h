//
//  ZSTShellESpeedBar.h
//  ShellE
//
//  Created by zhangwanqiang on 14-3-27.
//  Copyright (c) 2014年 zhangwanqiang. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZSTShellESpeedBar;

@protocol ZSTShellESpeedBarDelegate <NSObject>

- (void)zstShellESpeedBar:(ZSTShellESpeedBar *)speedBar withParam:(NSDictionary *)param;

@end


@interface ZSTShellESpeedBar : UIView

@property(strong)UILabel *titleLabel;
@property(strong)UIButton *speedBtn;
@property(strong,nonatomic)NSDictionary *moduleParams;


@property(strong)id<ZSTShellESpeedBarDelegate>delegate;

- (void)configSpeedBarNormalImage:(UIImage *)normalImage
                    selectedImage:(UIImage *)selectedImage
                            param:(NSDictionary *)param;


@end
