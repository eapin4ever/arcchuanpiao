//
//  ZSTF3Engine+ShellE.h
//  ShellE
//
//  Created by zhangwanqiang on 14-3-27.
//  Copyright (c) 2014年 zhangwanqiang. All rights reserved.
//

#import "ZSTF3Engine.h"


@protocol ZSTF3EngineShellEDelegate <ZSTF3EngineDelegate>

- (void)getShellEADDidSucceed:(NSArray *)ADArray;
- (void)getShellEADDidFailed:(int)resultCode;

@end

@interface ZSTF3Engine (ShellE)

/**
 *	@brief	获取shell广告
 *
 */
- (void)getShellEAD;

@end