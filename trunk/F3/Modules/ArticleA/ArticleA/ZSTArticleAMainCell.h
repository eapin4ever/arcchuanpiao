//
//  NewsMainCell.h
//  F3
//
//  Created by admin on 12-7-17.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZSTArticleAVO.h"
#import "ZSTArticleACarouselView.h"
#import "TKAsynImageView.h"

@interface ZSTArticleAMainCell : UITableViewCell {
//    TKAsynImageView *imageView;
    UIImageView *imageView;
    UILabel *title;
    UILabel *subTitle;
}

//@property (nonatomic,retain) TKAsynImageView *myImageView;
@property (nonatomic,retain) UIImageView *myImageView;
@property (nonatomic,retain) UILabel *title;
@property (nonatomic,retain) UILabel *subTitle;

- (void)configCell:(ZSTArticleAVO*)nvo cellForRowAtIndexPath:(NSIndexPath *)indexPath;
@end


@protocol ZSTNewsCarouselViewCellDelegate <NSObject>

@optional
- (void)carouselViewCell:(ZSTArticleACarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;


@end

@interface ZSTArticleACarouselViewCell : UITableViewCell<ZSTNewsCarouselViewDataSource,ZSTNewsCarouselViewDelegate>
{
    
    ZSTArticleACarouselView *carouselView;
    NSArray *dataArry;
    id<ZSTNewsCarouselViewCellDelegate>carouselViewCellDelegate;
}
@property (nonatomic, retain) ZSTArticleACarouselView *carouselView;
@property (nonatomic ,retain) NSArray *dataArry;
@property (nonatomic ,retain) id<ZSTNewsCarouselViewCellDelegate>carouselViewCellDelegate;
- (void)configCell:(NSArray*)array;

@end
