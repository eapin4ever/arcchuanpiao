//
//  ZSTDao.m
//  News
//  
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTDao+ArticleA.h"
#import "ZSTSqlManager.h"
#import "ZSTUtils.h"
#import "TKUtil.h"
#import "ZSTGlobal+ArticleA.h"

#define CREATE_TABLE_CMD(moduleType) [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [ArticleA_Article_%@] (\
                                                                    ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                    MsgID INTEGER DEFAULT 0 , \
                                                                    CategoryID INTEGER DEFAULT 0, \
                                                                    MsgType INTEGER DEFAULT 0,\
                                                                    Title VARCHAR DEFAULT '',\
                                                                    KeyWord VARCHAR DEFAULT '', \
                                                                    IconUrl VARCHAR  DEFAULT '', \
                                                                    AddTime double DEFAULT (getdate()), \
                                                                    OrderNum INTEGER  DEFAULT 0, \
                                                                    Source VARCHAR  DEFAULT '',\
                                                                    IsRead INTEGER  DEFAULT 0, \
                                                                    Version INTEGER DEFAULT 0, \
                                                                    UNIQUE(MsgID, CategoryID)\
                                                                    );\
                        CREATE TABLE IF NOT EXISTS [ArticleA_Category_%@] (\
                                                                    ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                    CategoryID INTEGER DEFAULT 0 UNIQUE, \
                                                                    CategoryName VARCHAR DEFAULT '', \
                                                                    OrderNum INTEGER  DEFAULT 0, \
                                                                    Version INTEGER DEFAULT 0, \
                                                                    DefaultFlag INTEGER  DEFAULT 0, \
                                                                    IndexFlag INTEGER  DEFAULT 0, \
                                                                    IconUrl VARCHAR  DEFAULT ''\
                                                                    );", [NSNumber numberWithInteger:moduleType], [NSNumber numberWithInteger:moduleType]]

TK_FIX_CATEGORY_BUG(ArticleA)

@implementation ZSTDao(ArticleA)

- (void)createTableIfNotExistForArticleAModule
{
    
    [ZSTSqlManager executeSqlWithSqlStrings:CREATE_TABLE_CMD(self.moduleType)];
//    [self addcolumnTableWithName:@"LinkUrl"];
    
    [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"alter table ArticleA_Category_%ld add column %@ VARCHAR  DEFAULT ''",(long)self.moduleType,@"LinkUrl"]];
}

- (BOOL) addcolumnTableWithName:(NSString *)name
{
    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"alter table ArticleA_Category_%ld add column %@ VARCHAR  DEFAULT ''",(long)self.moduleType,name]];
    return success;
}

- (BOOL)addArticleACategory:(NSInteger)categoryID
               categoryName:(NSString *)categoryName
                   orderNum:(NSInteger)orderNum
                    version:(NSInteger)version
                defaultFlag:(BOOL)defaultFlag
                  indexFlag:(BOOL)indexFlag
                    iconUrl:(NSString *)iconUrl
                    linkUrl:(NSString *)linkUrl
{
    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO ArticleA_Category_%ld (CategoryID, CategoryName, OrderNum, Version, DefaultFlag, IndexFlag, IconUrl,LinkUrl) VALUES (?,?,?,?,?,?,?,?)", (long)self.moduleType],
                    [NSNumber numberWithInteger:categoryID],
                    [TKUtil wrapNilObject:categoryName], 
                    [NSNumber numberWithInteger:orderNum],
                    [NSNumber numberWithInteger:version],
                    [NSNumber numberWithBool:defaultFlag],
                    [NSNumber numberWithBool:indexFlag],
                    [TKUtil wrapNilObject:iconUrl],
                    [TKUtil wrapNilObject:linkUrl]
                    ];
    return success;
}

- (BOOL)deleteAllArticleACategories
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM ArticleA_Category_%ld", (long)self.moduleType];
    if (![ZSTSqlManager executeUpdate:deleteSQL]) {
        
        return NO;
    }
    return YES;
}

- (BOOL)deleteCategoryOfId:(NSInteger)categoryId
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM ArticleA_Category_%ld where CategoryID=%ld", (long)self.moduleType, categoryId];
    if (![ZSTSqlManager executeUpdate:deleteSQL]) {
        
        return NO;
    }
    return YES;
}

- (NSDictionary *)getDefaultArticleACategory
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, CategoryID, CategoryName, OrderNum, Version, DefaultFlag, IndexFlag, IconUrl FROM ArticleA_Category_%ld where DefaultFlag = 1 ORDER BY OrderNum desc", (long)self.moduleType];
    NSArray *results = [ZSTSqlManager executeQuery:querySql];
    if ([results count]) {
        return [results lastObject];
    }
    return nil;
}

- (NSArray *)getArticleACategories
{
//    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, CategoryID, CategoryName, OrderNum, Version, DefaultFlag, IndexFlag, IconUrl FROM ArticleA_Category_%d where DefaultFlag = 0 ORDER BY OrderNum desc", self.moduleType];
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, CategoryID, CategoryName, OrderNum, Version, DefaultFlag, IndexFlag, IconUrl FROM ArticleA_Category_%ld ORDER BY OrderNum desc", (long)self.moduleType];
    NSArray *results = [ZSTSqlManager executeQuery:querySql];
    return results;
}

- (NSArray *)getArticleACategory:(NSInteger)categoryId
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, CategoryID, CategoryName, OrderNum, Version, DefaultFlag, IndexFlag, IconUrl FROM ArticleA_Category_%ld where DefaultFlag = 0 and CategoryID = %ld ORDER BY OrderNum desc", (long)self.moduleType, categoryId];
    NSArray *results = [ZSTSqlManager executeQuery:querySql];
    return results;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL)addArticleA:(NSInteger)msgID
         categoryID:(NSInteger)categoryID
            msgType:(int)msgType
              title:(NSString *)title
            keyword:(NSString *)keyWord
            iconUrl:(NSString *)iconUrl
            addTime:(NSDate *)addTime
           orderNum:(NSInteger)orderNum
             source:(NSString *)source
            version:(NSInteger)version
{
    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO ArticleA_Article_%ld (MsgID, CategoryID, MsgType, Title, KeyWord, IconUrl, AddTime, OrderNum, Source, Version) VALUES (?,?,?,?,?,?,?,?,?,?)", (long)self.moduleType],
                  [NSNumber numberWithInteger:msgID],  
                  [NSNumber numberWithInteger:categoryID], 
                  [NSNumber numberWithInteger:msgType], 
                  [TKUtil wrapNilObject:title], 
                  [TKUtil wrapNilObject:keyWord],
                  [TKUtil wrapNilObject:iconUrl],
                  [NSNumber numberWithDouble:[addTime timeIntervalSince1970]],
                  [NSNumber numberWithInteger:orderNum], 
                  [TKUtil wrapNilObject:source],
                  [NSNumber numberWithInteger:version]
                  ];
    return success;
}

- (BOOL)setArticleA:(NSInteger)infobID isRead:(BOOL)isRead
{
    NSString *updateSQL = [NSString stringWithFormat:@"UPDATE ArticleA_Article_%ld set IsRead = ?  WHERE MsgID = ?", (long)self.moduleType];
    if (![ZSTSqlManager executeUpdate:updateSQL, [NSNumber numberWithInt:isRead? 1 : 0], [NSNumber numberWithDouble:infobID]]) {
        
        return NO;
    }
    return YES;
}

- (BOOL)getArticleAIsRead:(NSInteger)infobID
{
    return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"SELECT IsRead FROM ArticleA_Article_%ld where msgID = ?", (long)self.moduleType], [NSNumber numberWithDouble:infobID]];
}

- (BOOL)deleteArticleA:(NSInteger)infobID
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM ArticleA_Article_%ld WHERE MsgID = ?", (long)self.moduleType];
    if (![ZSTSqlManager executeUpdate:deleteSQL, [NSNumber numberWithDouble:infobID]]) {
        
        return NO;
    }
    return YES;
}

- (BOOL)deleteAllArticleAOfBroadcast
{
//    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM Z_News_%d where IConType = ?", self.moduleType];
//    if (![ZSTSqlManager executeUpdate:deleteSQL, [NSNumber numberWithInt:ZSTIconType_Broadcast]]) {
//        return NO;
//    }
    return YES;
}

- (BOOL)deleteAllArticleAOfCategory:(NSInteger)categoryID
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM ArticleA_Article_%ld where categoryID = ?", (long)self.moduleType];
    if (![ZSTSqlManager executeUpdate:deleteSQL, [NSNumber numberWithInteger:categoryID]]) {
        return NO;
    }
    return YES;
}

- (BOOL)ArticleAExist:(NSInteger)infobID
{
    return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"SELECT COUNT(ID) as cnt FROM ArticleA_Article_%ld where MsgID = ?", (long)self.moduleType], [NSNumber numberWithInteger:infobID]];
}


- (NSDictionary *)ArticleAAtIndex:(int)index category:(NSInteger)categoryID
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, MsgID, CategoryID, MsgType, Title, KeyWord, IconUrl, AddTime, OrderNum, Source, IsRead, Version FROM ArticleA_Article_%ld where CategoryID = ? ORDER BY AddTime desc limit ?,?", (long)self.moduleType];
    NSArray *results = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:
                                                            [NSNumber numberWithInteger:categoryID],
                                                            [NSNumber numberWithInteger:index - 1], 
                                                            [NSNumber numberWithInteger:1], 
                                                            nil]];
    if ([results count]) {
        return [results objectAtIndex:0];
    }
    return nil;
}

- (NSArray *)getBroadcastArticleA
{
//    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, MsgID, CategoryID, MsgType, Title, Summary, IConFileID, IConType, OrderNum, Source, AddTime, IsRead FROM Z_News_%d where IConType = ? ORDER BY AddTime", self.moduleType];
//    NSArray *results = [ZSTSqlManager executeQuery:querySql, [NSNumber numberWithInt:ZSTIconType_Broadcast]];
//    return results;
    
    return nil;
}

- (NSArray *)ArticleAAtPage:(int)pageNum pageCount:(int)pageCount category:(NSInteger)categoryID
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, MsgID, CategoryID, MsgType, Title, KeyWord, IconUrl, AddTime, OrderNum, Source, IsRead, Version FROM ArticleA_Article_%ld where CategoryID = ? ORDER BY OrderNum desc limit ?,?", (long)self.moduleType];
    NSArray *results = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:
                                                            [NSNumber numberWithInteger:categoryID],
                                                            [NSNumber numberWithInt:(pageNum-1)*pageCount], 
                                                            [NSNumber numberWithInt:pageCount], 
                                                            nil]];
    return results;
}

- (NSUInteger)ArticleACountOfCategory:(NSInteger)categoryID
{
    return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"SELECT COUNT(ID) FROM ArticleA_Article_%ld where CategoryID = ? ", (long)self.moduleType], [NSNumber numberWithInteger:categoryID]];
}

- (NSUInteger) getMaxArticleAIdOfCategory:(NSInteger)categoryID
{
	//不能用max(mblogid)，因为mblogid ascii排序有问题
	return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"select MsgID from ArticleA_Article_%ld where CategoryID = ? order by AddTime desc limit 1", (long)self.moduleType], [NSNumber numberWithInteger:categoryID]];
}

- (NSUInteger) getMinArticleAIdOfCategory:(NSInteger)categoryID
{
	//不能用max(mblogid)，因为mblogid ascii排序有问题
	return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"select MsgID from ArticleA_Article_%ld where CategoryID = ? order by AddTime asc limit 1", (long)self.moduleType], [NSNumber numberWithInteger:categoryID]];
}

- (NSArray *)ArticleAIdOfCategory:(NSInteger)categoryId
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, MsgID FROM ArticleA_Article_%ld where CategoryID = %ld ORDER BY AddTime desc", (long)self.moduleType, categoryId];
    
    NSArray *results = [ZSTSqlManager executeQuery:querySql];
    return results;
}

@end
