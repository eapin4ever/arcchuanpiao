//
//  NewsMainViewController.m
//  F3
//
//  Created by fujinsheng on 12-7-17.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTArticleAViewController.h"
#import "ZSTArticleACarouselView.h"
#import "ColumnBar.h"
#import "ZSTDao+ArticleA.h"
#import "ZSTF3Engine+ArticleA.h"
#import "ZSTUtils.h"
#import "ZSTShell.h"
#import "ZSTArticleAMyFavoriteViewController.h"
#import "ZSTArticleASearchViewController.h"
#import "ZSTWebViewController.h"
#import "BaseNavgationController.h"

#define maxItems 6
#define intervalTime 1   //小时

@interface ZSTArticleAViewController ()
{
    NSInteger categoryID;
    UITapGestureRecognizer *singleTap;
}

@end


@implementation ZSTArticleAViewController

@synthesize searchBar;
@synthesize goSearchButton;
@synthesize filter;
@synthesize newsArr;


- (void)moduleApplication:(ZSTModuleApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [application.dao createTableIfNotExistForArticleAModule];
}

- (void)openMyFavorites
{
     if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0)
     {
         ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
         controller.isFromSetting = YES;
         controller.delegate = self;
         BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
         
         [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
         }];
         [navicontroller release];
         [controller release];
         
         return;
     }
    
    ZSTArticleAMyFavoriteViewController *myMarket = [[ZSTArticleAMyFavoriteViewController alloc] init];
    myMarket.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:myMarket animated:YES];
    [myMarket release];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"我的收藏", nil) target:self selector:@selector (openMyFavorites)];
    
    _isLoadingMore = NO;
    _isRefreshing = NO;
    _pagenum = 1;
    
//    UIImageView *bgImg = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_articlea_bg.png")];
//    bgImg.frame = CGRectMake(0, 0, 320, 480+(iPhone5?88:0));
//    bgImg.contentMode = UIViewContentModeScaleToFill;
//    [self.view addSubview:bgImg];
//    [bgImg release];
    
    
    self.newsArr = [NSMutableArray array];
    indexCategoryArr = (NSMutableArray *)[[self.dao getArticleACategories] retain];
    
    // 顶部导航栏
    _columnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 38)];
    UIImageView *columnbgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 38)];
    columnbgImgView.image = ZSTModuleImage(@"module_articlea_category_bg.png");
    [_columnView addSubview:columnbgImgView];
    [columnbgImgView release];
    [self.view addSubview:_columnView];
    
    _columnBar = [[ColumnBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width - 50, 38)];
    _columnBar.dataSource = self;
    _columnBar.delegate = self;
    _columnBar.topbarType = TopBarType_Slider;
    _columnBar.hidden = NO;
    _columnBar.moverImage = ZSTModuleImage(@"module_csa_top_btn_p.png");
    _columnBar.leftCapImage = ZSTModuleImage(@"module_articlea_leftCapImage.png");
    _columnBar.rightCapImage = ZSTModuleImage(@"module_articlea_rightCapImage.png");
//    _columnBar.image =  ZSTModuleImage(@"module_articlea_category_bg.png");
    [_columnView addSubview:_columnBar];
    
    self.searchBar = [[[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)] autorelease];
    self.searchBar.backgroundColor = [UIColor clearColor];
    self.searchBar.delegate = self;
    self.searchBar.tintColor = [UIColor whiteColor];
	self.searchBar.placeholder = NSLocalizedString(@"输入查找内容" , nil);
	self.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
	self.searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
	self.searchBar.keyboardType = UIKeyboardTypeDefault;
    self.searchBar.hidden = YES;
    [_columnView addSubview:self.searchBar];
    
    if ([indexCategoryArr count] != 0) {
        if ([indexCategoryArr count] == 1) {
            _columnBar.hidden = YES;
            self.searchBar.hidden = NO;
        }
        [_columnBar reloadData];
        [self initData];
    }
    
    //表格
//    UIImage *primitiveImage = ZSTModuleImage(@"module_articlea_article_table_bg.png");
//    CGFloat capWidth = primitiveImage.size.width / 2;
//    CGFloat capHeight = primitiveImage.size.height / 2;
//    UIImage* stretchableImage = [primitiveImage stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
//    imageView = [[UIImageView alloc] initWithImage:stretchableImage];
//    imageView.frame = CGRectMake(15, 52, 290, 420-24+(iPhone5?88:0));
//    imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//    [self.view addSubview:imageView];54                                    -24 
    
    newsTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 39, 320, 419+(iPhone5?88:0)+(IS_IOS_7?20:0)) style:UITableViewStylePlain];
//    newsTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 44, 320, 412+(iPhone5?88:0)) style:UITableViewStylePlain];
    newsTable.delegate = self;
    newsTable.dataSource = self;
    newsTable.backgroundColor = [UIColor whiteColor];
    newsTable.separatorStyle =  UITableViewCellSeparatorStyleNone;
    newsTable.clipsToBounds = YES;
//    newsTable.layer.cornerRadius = 8; 
//    newsTable.layer.masksToBounds = YES;
    
//    if (_columnBar.hidden) {
//        imageView.frame = CGRectMake(15, 21, 290, 420 +(iPhone5?88:0));
//        newsTable.frame = CGRectMake(15, 22, 290, 416 +(iPhone5?88:0));
//    }
    newsTable.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:newsTable];
    
    if (_loadMoreView == nil) {
        _loadMoreView = [[TKLoadMoreView alloc] initWithFrame:CGRectMake(-15, 0, 350, 50)];
        _loadMoreView.backgroundColor = [UIColor clearColor];
        _loadMoreView.delegate = self;
    }
    
//    UIImageView *columnBarShadow = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_articlea_category_shadow.png")];
//    columnBarShadow.frame = CGRectMake(0, CGRectGetMaxY(_columnBar.frame)-1, 320, 4);
//    [self.view addSubview:columnBarShadow];
//    columnBarShadow.tag = 88;
//    [columnBarShadow release];
//    
//    if (_columnBar.hidden) {
//        columnBarShadow.hidden = YES;
//    }
    
    [self.view newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkIndicatorView) {
        //获取分类
        [self.engine getArticleACategory];
    }];
    
//    self.goSearchButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.goSearchButton = [[PMRepairButton alloc] init];
    self.goSearchButton.frame = CGRectMake(CGRectGetMaxX(_columnBar.frame)+1 + (50 - 38)/2 , 0, 38, 38);
    [self.goSearchButton setImage:ZSTModuleImage(@"module_articlea_search_n@2x.png") forState:UIControlStateNormal];
    [self.goSearchButton setImage:ZSTModuleImage(@"module_articlea_search_p@2x.png") forState:UIControlStateHighlighted];
    self.goSearchButton.imageEdgeInsets = UIEdgeInsetsMake(10,10,10,10);
    [self.goSearchButton addTarget:self action:@selector(goSearchAction:) forControlEvents:UIControlEventTouchUpInside];
    self.goSearchButton.hidden = _columnBar.hidden;
    [_columnView addSubview:self.goSearchButton];
    
    UIImageView *verticalImgView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_columnBar.frame), 0, 1, 38)];
    verticalImgView.backgroundColor = [UIColor clearColor];
    verticalImgView.image = ZSTModuleImage(@"module_articlea_category_line.png");
    verticalImgView.hidden = self.goSearchButton.hidden;
    [_columnView addSubview:verticalImgView];
    [verticalImgView release];

    
    recoginzer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(doSomeThing:)];
    searchOriginPoint = self.goSearchButton.frame.origin;
    //[self.goSearchButton addGestureRecognizer:recoginzer];
    
    self.disableViewOverlay = [[UIView alloc] initWithFrame:CGRectMake(0.0f,42.0f,320.0f,416.0f)];
    self.disableViewOverlay.backgroundColor=[UIColor blackColor];
    self.disableViewOverlay.alpha = 0;
    
    singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardWillHide)];
    [self.disableViewOverlay addGestureRecognizer:singleTap];
}

- (void)initData
{
    currentSelectItem = 0;
    
    //从类型得数组里面得到该分类得对象，得到分类得编号
    [self.newsArr removeAllObjects];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [newsTable reloadData];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (void)doSomeThing:(UIPanGestureRecognizer*)recog
{
    switch (recoginzer.state) {
        case UIGestureRecognizerStateBegan:
            searchOriginPoint = self.goSearchButton.frame.origin;
            break;
        case UIGestureRecognizerStateChanged:
        {
            CGRect frame = self.goSearchButton.frame;
            CGPoint position = [recog translationInView:self.goSearchButton];
            frame.origin.x = searchOriginPoint.x + position.x;
            frame.origin.y = searchOriginPoint.y + position.y;
            self.goSearchButton.frame = frame;
            break;
        }
        case UIGestureRecognizerStateEnded:
        {
            CGRect frame = self.goSearchButton.frame;
            CGPoint volic = [recog velocityInView:self.view];
            //速度超过600，按照速度的方向放置按钮
            if (fabs(volic.x) > 600) {
                float aspect = volic.y/volic.x;
                if (volic.x > 0) {
                    float yC = aspect * (self.view.frame.size.width - frame.size.width - frame.origin.x);
                    frame.origin.y += yC;
                    frame.origin.x = self.view.frame.size.width - frame.size.width;
                }
                else {
                    float yC = aspect * frame.origin.x;
                    frame.origin.y -= yC;
                    frame.origin.x = 0;
                }
            }
            else {
                if (self.goSearchButton.center.x <= self.view.frame.size.width/2) {
                    frame.origin.x = 0;
                }
                else if (self.goSearchButton.center.x > self.view.frame.size.width/2) {
                    frame.origin.x = self.view.frame.size.width - frame.size.width;
                }
            }
            if (frame.origin.y < 44) {
                frame.origin.y = 44;
            }
            else if (frame.origin.y > self.view.frame.size.height - frame.size.height) {
                frame.origin.y = self.view.frame.size.height - frame.size.height;
            }
            float duration = 0.25f * fabs(self.goSearchButton.center.x - self.view.frame.size.width) / self.view.frame.size.width * 2;
            duration = 0.14f;
            [UIView animateWithDuration:duration animations:^{
                self.goSearchButton.frame = frame;
            } completion:^(BOOL finished) {
                searchOriginPoint = self.goSearchButton.frame.origin;
            }];
            break;
        }
        default:
            break;
    }
}

- (void)keyboardWillHide
{
    [self.searchBar resignFirstResponder];
    [self.disableViewOverlay removeFromSuperview];
    self.searchBar.text = @"";
//    [self searchBar:self.searchBar activate:NO];
     [self.searchBar setShowsCancelButton:NO animated:YES];
}

- (void)goSearchAction:(id)sender
{
    ZSTArticleASearchViewController *searchViewController = [[ZSTArticleASearchViewController alloc] init];
    searchViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:searchViewController animated:YES];
    [searchViewController release];
}


- (void)updateListData
{
    NSInteger categoryid = [[[indexCategoryArr objectAtIndex:currentSelectItem] safeObjectForKey:@"CategoryID"] integerValue];
    self.newsArr = (NSMutableArray *)[self.dao ArticleAAtPage:1 pageCount:NEWS_PAGE_SIZE category:categoryid];
    _pagenum = 1;
    _hasMore = NO;
    [newsTable reloadData];
}

- (void)finishLoading
{
    [_loadMoreView loadMoreScrollViewDataSourceDidFinishedLoading:newsTable];
    
    _isLoadingMore = NO;
    _isRefreshing = NO;
}


- (void)loadMoreData {
    
    [self.engine getArticleAAtPage:_pagenum category:[[[indexCategoryArr objectAtIndex:currentSelectItem] safeObjectForKey:@"CategoryID"] integerValue]];
}

- (void) loadDataWithKeyword:(NSString *)key
{
    NSLog(@"key = %@",key);
    
    _pagenum = 1;
    
    if (self.newsArr.count != 0) {
        
        [self.newsArr removeAllObjects];
        [newsTable reloadData];
    }
    
    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在搜索..." , nil)];
    [self.engine getArticleA:0 sinceID:_pagenum orderType:SortType_Desc Filter:key fetchCount:NEWS_PAGE_SIZE iconType:ZSTIconType_List];
}


- (void)appendData:(NSArray*)dataArray
{
    if (!dataArray || dataArray.count == 0) {
        return;
    }
    NSDictionary *dic = [dataArray objectAtIndex:0];
    NSInteger resCategoryID = [[dic safeObjectForKey:@"CategoryID"] integerValue];
    
    if (_pagenum == 1 || resCategoryID != categoryID) {
        
        [self.newsArr removeAllObjects];
    }
    [self.newsArr addObjectsFromArray:dataArray];
    _pagenum ++ ;
    [newsTable reloadData];
}

- (void)insertData:(NSArray*)dataArray
{
    self.newsArr = [NSMutableArray arrayWithArray:dataArray];
    _pagenum ++;
}

- (void)fetchDataSuccess:(NSArray *)theData isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore
{
    _hasMore = hasMore;
    
    [self appendData:theData];

    [newsTable reloadData];
    
    [self finishLoading];
}

- (void)fetchDataFailed
{
    [self finishLoading];
    [newsTable reloadData];
}

#pragma mark HHLoadMoreViewDelegate Methods
- (void)loadMoreDidTriggerRefresh:(TKLoadMoreView*)view
{
    if (_hasMore && !_isLoadingMore) {
        _isLoadingMore = YES;
        [self loadMoreData];
    }
}

- (BOOL)loadMoreDataSourceIsLoading:(TKLoadMoreView*)view
{
    return _isLoadingMore;
}

#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!_isRefreshing) {
        [_loadMoreView loadMoreScrollViewDidEndDragging:scrollView];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (!_isRefreshing) {
        [_loadMoreView performSelector:@selector(loadMoreScrollViewDidEndDragging:) withObject:scrollView afterDelay:0];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!_isRefreshing) {

    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    TKLoadMoreView *loadMoreView = (TKLoadMoreView *)[cell descendantOrSelfWithClass:[TKLoadMoreView class]];
    if (loadMoreView) {
        if (!_isRefreshing) {
            [_loadMoreView loadMoreTriggerRefresh];
        }
    }
}

- (BOOL)compareTimerkey:(NSString *)key timeDic:(NSMutableDictionary *)timerDic
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:timerDic];
    NSDate *compareDate = [dic safeObjectForKey:key];
    if (compareDate == nil || [[compareDate description] length] == 0) {
        [dic setValue:[[NSDate date] dateByAddingHours:intervalTime] forKey:key];//第一次计时
        [_timeDic removeAllObjects];
        [_timeDic release];
        _timeDic = [[NSMutableDictionary dictionaryWithDictionary:dic] retain];
        return YES;
    }else {
        if ([compareDate isEqualToDate:[NSDate date]] || [compareDate isEarlierThanDate:[NSDate date]]) {
            [dic removeObjectForKey:key];
            [dic setValue:[[NSDate date] dateByAddingHours:intervalTime] forKey:key];//第n次计时
            [_timeDic removeAllObjects];
            [_timeDic release];
            _timeDic = [[NSMutableDictionary dictionaryWithDictionary:dic] retain];
            return YES;
        }else {
            return NO;
        }
    }
}

- (void)engine:(SinaWeibo *)engine requestDidSucceedWithResult:(id)result
{
    if ([result  count] == 0) {
        [self fetchDataSuccess:nil isLoadMore:NO hasMore:NO];
    }
    [self finishLoading];
}

- (void)refreshLoadData
{
    [self.engine getArticleACategory];
    
    UIControl *refreshCtr = (UIControl *)[self.view viewWithTag:99];
    [refreshCtr removeTarget:self action:@selector(refreshLoadData)  forControlEvents:UIControlEventTouchUpInside];
    UIActivityIndicatorView *aiView = (UIActivityIndicatorView *)[refreshCtr viewWithTag:100];
    if (![aiView isAnimating]) {
        [aiView startAnimating];
    }
    UILabel *refreshLabel = (UILabel *)[refreshCtr viewWithTag:102];
    refreshLabel.hidden = YES;
}

#pragma mark - ColumnBarDataSource

- (NSInteger)numberOfTabsInColumnBar:(ColumnBar *)columnBar
{
    return [indexCategoryArr count];
}

- (NSString *)columnBar:(ColumnBar *)columnBar titleForTabAtIndex:(int)index
{
    return [[indexCategoryArr objectAtIndex:index] safeObjectForKey:@"CategoryName"];
}

#pragma mark - ColumnBarDelegate
- (void)columnBar:(ColumnBar *)columnBar didSelectedTabAtIndex:(NSInteger)index
{
    currentSelectItem = index;
    [self finishLoading];
    
    //从类型得数组里面得到该分类得对象，得到分类得编号
    [self.newsArr removeAllObjects];
    [newsTable reloadData];
    newsTable.tableFooterView = nil;
    
    [self updateListData];//如果有缓存，则先展示缓存数据
    
    categoryID = [[[indexCategoryArr objectAtIndex:currentSelectItem] safeObjectForKey:@"CategoryID"] integerValue];

    [newsTable newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkIndicatorView) {
        //获取分类
        [self.engine getArticleAAtPage:_pagenum category:categoryID];
    }];

}

#pragma mark - ZSTF3EngineNewsDelegate
- (void)getArticleACategoryDidSucceed:(NSDictionary *)defaultIndexCategory  categories:(NSArray *)categories
{
    [self.view removeNetworkIndicatorView];
    
    if ([categories count] != 0) {
        //获取分类信息
        [indexCategoryArr release];
        indexCategoryArr = nil;
        indexCategoryArr = [[NSMutableArray arrayWithArray:categories] retain];
        refreshCategoryArr = [[NSMutableArray arrayWithArray:categories] retain];
        
        CGRect newsTableFrame = newsTable.frame;
        CGRect imageFrame = imageView.frame;
        UIImageView *shadow = (UIImageView *)[self.view viewWithTag:88];
        
        if ([indexCategoryArr count] == 1) {
//            imageFrame.size.height += _columnBar.hidden ? 0 : 24;
//            newsTableFrame.size.height +=  _columnBar.hidden ? 0 : 24;
            shadow.hidden = YES;
            _columnBar.hidden = YES;
            self.searchBar.hidden = NO;
        }else {
//            imageFrame.size.height -=  _columnBar.hidden ? 24 :0;
//            newsTableFrame.size.height -=  _columnBar.hidden ? 24 :0;
            shadow.hidden = NO;
            _columnBar.hidden = NO;
            self.searchBar.hidden = YES;
        }
        
        [newsTable removeFromSuperview];
        [imageView removeFromSuperview];
        
//        imageFrame.origin.y = _columnBar.hidden ? 21 : 52;
//        newsTableFrame.origin.y = _columnBar.hidden ? 22 : 54;
        
        [UIView animateWithDuration:0 animations:^{
            newsTable.frame = newsTableFrame;
            imageView.frame = imageFrame;
        }];
        
        [self.view addSubview:imageView];
        [self.view addSubview:newsTable];
        [self.view bringSubviewToFront:self.goSearchButton];
    }
    [_columnBar reloadData];
    [_columnBar selectTabAtIndex:0];
}

- (void)getArticleACategoryDidFailed:(int)resultCode
{
    [self.view refreshFailed];
    
    [self finishLoading];
}

- (void)getArticleADidSucceed:(NSArray *)newsList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish
{
    [newsTable removeNetworkIndicatorView];
    [refreshCategoryArr removeObject:[indexCategoryArr objectAtIndex:currentSelectItem]];
    [self fetchDataSuccess:newsList isLoadMore:isLoadMore hasMore:hasMore];
}

- (void)getArticleADidFailed:(int)resultCode
{
    [newsTable refreshFailed];
    
    [self finishLoading];
}

#pragma mark ---点击某行触发的方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSInteger row = [indexPath row];
    ZSTArticleAVO *currentnvo = [ZSTArticleAVO voWithDic:[newsArr objectAtIndex:row]];
    if (currentnvo.linkUrl.length > 0) {
        
        ZSTWebViewController *webView = [[ZSTWebViewController alloc] init];
        [webView setURL:currentnvo.linkUrl];
        webView.hidesBottomBarWhenPushed = YES;
        webView.type = self.moduleType;
        webView.title = [[indexCategoryArr objectAtIndex:currentSelectItem] safeObjectForKey:@"CategoryName"];
        webView.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
        [self.navigationController pushViewController:webView animated:YES];
        [webView release];
        
    } else {
        ZSTArticleAContentViewController *content = [[ZSTArticleAContentViewController alloc] init];
        content.newsArr = newsArr;
        content.hidesBottomBarWhenPushed = YES;
        content.selectedIndex = row;
        [self.navigationController pushViewController:content animated:YES];
//        content.hTableView.contentOffset = CGPointMake(320 * row, 0);
        [content release];
    }
}

#pragma mark -
#pragma mark ---Table Data Source Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
        return [self.newsArr count] + (_hasMore ? 1:0);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == [newsArr count]) {
        static NSString *newsCellIdentifier = @"loadMoreView";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:newsCellIdentifier];
        
        if (cell==nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:newsCellIdentifier] autorelease];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.userInteractionEnabled = NO;
            [cell.contentView addSubview:_loadMoreView];
        }
        return cell;
    } else {
        static NSString *newsCellIdentifier = @"newsCellIdentifier";
        ZSTArticleAMainCell *cell = [tableView dequeueReusableCellWithIdentifier:newsCellIdentifier];
        
        if (cell==nil) {
            cell = [[[ZSTArticleAMainCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:newsCellIdentifier] autorelease];
            cell.accessoryType = UITableViewCellSelectionStyleNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }        
        NSDictionary *newsDic = (NSDictionary *)[self.newsArr objectAtIndex:indexPath.row];
        ZSTArticleAVO *nvo = [ZSTArticleAVO voWithDic:newsDic];
        [cell configCell:nvo cellForRowAtIndexPath:indexPath];
        return cell;
    }
}

#pragma mark ---UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 47;
}

#pragma mark -
#pragma mark UISearchBarDelegate Methods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText && ![searchText isEqualToString:@""]) {
        
        [self loadDataWithKeyword:self.searchBar.text];
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self searchBar:self.searchBar activate:YES];
    
    for(id cc in [self.searchBar subviews])
        
    {
        if([cc isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton *)cc;
            [btn setTitle:NSLocalizedString(@"取消",@"取消") forState:UIControlStateNormal];
            [btn setTintColor:[UIColor lightGrayColor]];
        }
    }
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.searchBar.text = @"";
    [self searchBar:self.searchBar activate:NO];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self searchBar:self.searchBar activate:NO];
    self.filter = self.searchBar.text;
    [self loadDataWithKeyword:self.filter];
}

- (void)searchBar:(UISearchBar *)searchBar activate:(BOOL) active
{
    newsTable.allowsSelection = !active;
    newsTable.scrollEnabled = !active;
    if (!active) {
        [self.disableViewOverlay removeFromSuperview];
        [self.searchBar resignFirstResponder];
        
        if (self.newsArr.count > 0) {
            
            [self.newsArr removeAllObjects];
        }
    
        [self updateListData];

    } else {
        self.disableViewOverlay.alpha = 0;
        [self.view addSubview:self.disableViewOverlay];
        
        [UIView beginAnimations:@"FadeIn" context:nil];
        [UIView setAnimationDuration:0.5];
        self.disableViewOverlay.alpha = 0.6;
        [UIView commitAnimations];
        
        NSIndexPath *selected = [newsTable indexPathForSelectedRow];
        if (selected) {
            [newsTable deselectRowAtIndexPath:selected animated:NO];
        }
    }
    
    [self.searchBar setShowsCancelButton:active animated:YES];
}

- (void)searchArticleAListDidSucceed:(NSArray *)resultList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish
{
    [TKUIUtil hiddenHUD];
    _hasMore = hasMore;
    [self appendData:resultList];
}

- (void)searchArticleAListDidFailed:(int)response
{
    [TKUIUtil hiddenHUD];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [TKUIUtil hiddenHUD];
}

- (void)dealloc
{
    [recoginzer release];
    [newsArr release];
    [newsTable release];
    [indexCategoryArr release];
    [imageView release];
    [_loadMoreView release];
    [_columnBar release];
    [_columnView release];
    
    [super dealloc];
}


@end
