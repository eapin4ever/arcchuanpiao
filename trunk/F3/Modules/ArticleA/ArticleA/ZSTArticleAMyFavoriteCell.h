//
//  ZSTZSTArticleAMyFavoriteCellMyFavoriteCellCell.h
//  infob
//
//  Created by luobin on 11/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTArticleAMyFavoriteCell : UITableViewCell
{

}

@property (nonatomic,retain) TKAsynImageView *articleImageView;
@property (nonatomic,retain) UILabel *title;

- (void)configCell:(NSDictionary *)article;

@end
