//
//  ZSTArticleAMyFavoriteViewController.h
//  infob
//
//  Created by luobin on 11/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTArticleAMyFavoriteViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,TKLoadMoreViewDelegate>
{
    UITableView *myFavoriteList;
    
    TKLoadMoreView *_loadMoreView;
    
    BOOL _isRefreshing;
    BOOL _isLoadingMore;
    BOOL _hasMore;
    
    int _pagenum;
}

@property (nonatomic ,retain) UITableView *myFavoriteList;
@property (nonatomic ,retain) NSMutableArray *myFavoriteArray;

@end
