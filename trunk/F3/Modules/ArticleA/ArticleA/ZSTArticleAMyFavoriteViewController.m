//
//  ZSTArticleAMyFavoriteViewController.m
//  infob
//
//  Created by luobin on 11/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTArticleAMyFavoriteViewController.h"
#import "ZSTUtils.h"
#import "ZSTF3Engine+ArticleA.h"
#import "ZSTArticleAMyFavoriteCell.h"
#import "ZSTArticleAEmptyTableCell.h"
#import "ZSTArticleAContentViewController.h"


@interface ZSTArticleAMyFavoriteViewController ()

@end

@implementation ZSTArticleAMyFavoriteViewController

@synthesize myFavoriteList;
@synthesize myFavoriteArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"我的收藏", nil)];
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
    UIImageView *bgImg = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_articlea_bg.png")];
    bgImg.frame = CGRectMake(0, 0, 320, 480+(iPhone5?88:0));
    bgImg.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:bgImg];
    [bgImg release];
    
    myFavoriteList = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 460+(iPhone5?88:0)) style:UITableViewStylePlain];
    //0 37 320 480-44-49-37-20
    myFavoriteList.delegate = self;
    myFavoriteList.dataSource = self;
    myFavoriteList.backgroundColor = [UIColor clearColor];
    myFavoriteList.separatorStyle =  UITableViewCellSeparatorStyleNone;
    myFavoriteList.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview: myFavoriteList];
    
    if (_loadMoreView == nil) {
        _loadMoreView = [[TKLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
        _loadMoreView.delegate = self;
        //        if (_hasMore) {
        //            newsTable.tableFooterView = _loadMoreView;
        //        }
    }
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.view newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkIndicatorView) {
        //获取分类
        [self refreshNewestData];
    }];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc
{
    [self.engine cancelAllRequest];

    TKRELEASE(myFavoriteList);
    TKRELEASE(myFavoriteArray);

    TKRELEASE(_loadMoreView);
    
    [super dealloc];
}

- (void)autoRefresh
{
//    [_refreshHeaderView autoRefreshOnScroll:myFavoriteList animated:YES];
}

- (void)loadMoreData 
{
    NSDictionary *lastFav = [myFavoriteArray lastObject];
    [self.engine getArticleAFavorites:[[lastFav safeObjectForKey:@"Id"] intValue] fetchCount:NEWS_PAGE_SIZE orderType:SortType_Asc];
}

- (void)finishLoading
{
    [_loadMoreView loadMoreScrollViewDataSourceDidFinishedLoading:myFavoriteList];
    
    _isLoadingMore = NO;
    _isRefreshing = NO;
}

- (void)refreshNewestData
{
    _pagenum = 1;
    [self.engine getArticleAFavorites:0 fetchCount:NEWS_PAGE_SIZE orderType:SortType_Desc];
}

- (void)appendData:(NSArray*)dataArray
{
    [myFavoriteArray addObjectsFromArray:dataArray];
    _pagenum ++ ;
}

- (void)insertData:(NSArray*)dataArray
{
    self.myFavoriteArray = [NSMutableArray arrayWithArray:dataArray];
    _pagenum ++;
}

- (void)fetchDataSuccess:(NSArray *)theData isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore
{
    if (isLoadMore) {
        _hasMore = hasMore;
        
        [self appendData:theData];
    }else {
        [self insertData:theData];
        
        if ([theData count] == NEWS_PAGE_SIZE) {
            _hasMore = YES;
        }
    }
    if (_hasMore) {
        myFavoriteList.tableFooterView = _loadMoreView;
    } else {
        myFavoriteList.tableFooterView = nil;
    }

    [myFavoriteList reloadData];
    
    [self finishLoading];
}

- (void)fetchDataFailed
{
    [self finishLoading];
}

#pragma mark HHLoadMoreViewDelegate Methods

- (void)loadMoreDidTriggerRefresh:(TKLoadMoreView*)view
{
    if (_hasMore) {
        _isLoadingMore = YES;
        [self loadMoreData];
    }
}

- (BOOL)loadMoreDataSourceIsLoading:(TKLoadMoreView*)view
{
    return _isLoadingMore;
}

#pragma mark - 

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate 
{
    if (!_isRefreshing) {
        [_loadMoreView performSelector:@selector(loadMoreScrollViewDidEndDragging:) withObject:scrollView afterDelay:0];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (!_isRefreshing) {
        [_loadMoreView performSelector:@selector(loadMoreScrollViewDidEndDragging:) withObject:scrollView afterDelay:0];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView 
{
    if (!_isRefreshing) {
        [_loadMoreView loadMoreScrollViewDidScroll:scrollView];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (myFavoriteArray == nil || ![myFavoriteArray count]) {
        return 1;
    } else {
        return [myFavoriteArray count] * 2;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([myFavoriteArray count] == 0) {
        
        static NSString *identifier = @"ZSTArticleAEmptyTableViewCell";
        ZSTArticleAEmptyTableCell *cell = [myFavoriteList dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[[ZSTArticleAEmptyTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
        }

        cell.introduceLabel.text = NSLocalizedString(@"你还没有收藏哦～" , nil);

        return cell;
    }else{
        if (indexPath.row % 2 == 0) {
            static NSString *CellIdentifier = @"myFavoriteCell";
        
            NSDictionary *articleInfo = nil;
        
            ZSTArticleAMyFavoriteCell *cell = [myFavoriteList dequeueReusableCellWithIdentifier:CellIdentifier];
        
            if (cell == nil) {
                cell = [[[ZSTArticleAMyFavoriteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
            }

            if ([myFavoriteArray count] != 0 ) {
                articleInfo = (NSDictionary *)[myFavoriteArray objectAtIndex:indexPath.row / 2];
            }
        
            if (cell != nil && [articleInfo count] != 0) {
                [cell configCell:articleInfo];
            }
        
            return cell;
        } else {
            static NSString *separatorCellIdentifier = @"separatorCellIdentifier";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:separatorCellIdentifier];
            if (cell==nil) {
                cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:separatorCellIdentifier] autorelease];
                cell.accessoryType = UITableViewCellSelectionStyleNone;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                UIImageView *selectView = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_articlea_table_separator.png")];
                selectView.frame = CGRectMake(0, 0, 320, 1);
                cell.backgroundView = selectView;
                //            cell.backgroundView = custview;
                [selectView release];
            }
            return cell;
        }
    }
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    NSInteger row = [indexPath row] / 2;
    ZSTArticleAContentViewController *content = [[ZSTArticleAContentViewController alloc] init];
    content.newsArr = myFavoriteArray;
    content.hidesBottomBarWhenPushed = YES;
    content.selectedIndex = row;
    [self.navigationController pushViewController:content animated:YES];
//    content.hTableView.contentOffset = CGPointMake(320 * row, 0);
    [content release];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{   
    
    return indexPath.row % 2 ? 1:47;
}

#pragma mark - ZSTF3EngineECADelegate
- (void)getArticleAFavoritesDidSucceed:(NSArray *)newsList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish
{
    [self.view removeNetworkIndicatorView];
    
    [self fetchDataSuccess:newsList isLoadMore:isLoadMore hasMore:hasMore];
}

- (void)getArticleAFavoritesDidFailed:(int)resultCode
{
    [self.view refreshFailed];
    
    [self finishLoading];
}

@end
