//
//  ZSTF3Engine+News.h
//  News
//
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTF3Engine.h"
#import "ZSTGlobal+ArticleA.h"

@protocol ZSTF3EngineArticleADelegate <ZSTF3EngineDelegate>

- (void)getArticleACategoryDidSucceed:(NSDictionary *)defaultIndexCategory  categories:(NSArray *)categories;
- (void)getArticleACategoryDidFailed:(int)resultCode;

- (void)getArticleADidSucceed:(NSArray *)newsList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish;
- (void)getArticleADidFailed:(int)resultCode;

- (void)getArticleAContentDidSucceed:(NSDictionary *)newsContent userInfo:(id)userInfo;
- (void)getArticleAContentDidFailed:(ZSTResponse *)response;

- (void)manageArticleAFavoritesDidSucceed:(id)userInfo;
- (void)manageArticleAFavoritesDidFailed:(int)resultCode userInfo:(id)userInfo;

- (void)getArticleAFavoritesDidSucceed:(NSArray *)newsList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish;
- (void)getArticleAFavoritesDidFailed:(int)resultCode;

- (void)getArticleACommentDidSucceed:(NSArray *)theCommentList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish;
- (void)getArticleACommentDidFailed:(int)resultCode;

- (void)addArticleACommentDidSucceed:(id)userInfo;
- (void)addArticleACommentDidFailed:(int)resultCode userInfo:(id)userInfo;

//查询伴奏
- (void)searchArticleAListDidSucceed:(NSArray *)resultList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish;
- (void)searchArticleAListDidFailed:(int)response;
@end


@interface ZSTF3Engine (ArticleA)

/**
 *	@brief	获取新闻类别
 *
 */
- (void)getArticleACategory;


///////////////////////////////////////////////////////////////////////// Private ///////////////////////////////////////////////////////////////////////

/**
 *	@brief	获取新闻列表
 *
 *	@param 	categoryID 	分类ID
 *	@param 	sinceID 	起始新闻ID
 *	@param 	orderType 	当OrderType为Desc时（刷新），从服务器获取MsgID大于SinceID的至多Size条记录；当OrderType为Asc时（更多），从服务器获取MsgID小于SinceID的至多Size条记录
 *	@param 	fetchCount 	获取最大条数
 *	@param 	iconType 	ICon图片类别，1：列表ICon；2：轮播ICon
 */
- (void)getArticleA:(NSInteger)categoryID
            sinceID:(NSInteger)sinceID
          orderType:(NSString *)orderType
         fetchCount:(NSUInteger)fetchCount
           iconType:(ZSTIconType)iconType;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 *	@brief	获取指定圈子下pageNum页的新闻
 *
 *	@param 	pageNum 	页数
 *	@param 	categoryID 	分类
 */
- (void)getArticleAAtPage:(int)pageNum category:(NSInteger)categoryID;

/**
 *	@brief	获取新闻正文
 *
 *	@param 	newsID 	新闻ID
 */
- (void)getArticleAContent:(NSInteger)newsID userInfo:(id)userInfo version:(NSInteger)version;

//收藏管理
- (void)manageArticleAFavorites:(NSInteger)articleId opType:(NSInteger)opType;

//获取收藏列表
- (void)getArticleAFavorites:(NSInteger)sinceId 
                  fetchCount:(NSInteger)fetchCount
                   orderType:(NSString *)orderType;

//获取评论列表
- (void)getArticleAComment:(NSInteger)msgId
                   sinceId:(NSInteger)sinceId
                fetchCount:(NSInteger)fetchCount
                 orderType:(NSString *)orderType;

//添加评论
- (void)addArticleAComment:(NSInteger)msgId
                replyTitle:(NSString *)replyTitle
                   content:(NSString *)content
                  parentId:(NSInteger)parentId;

- (void)modifyFavStateInCache:(BOOL)favState articleId:(int)articleId version:(int)version;

- (void)getArticleA:(NSInteger)categoryID
            sinceID:(NSInteger)sinceID
          orderType:(NSString *)orderType
             Filter:(NSString *)filter
         fetchCount:(NSUInteger)fetchCount
           iconType:(ZSTIconType)iconType;
    
@end
