//
//  ZSTCommentListViewController.m
//  infob
//
//  Created by xuhuijun on 12-11-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTCommentListViewController.h"
#import "ZSTGlobal+ArticleA.h"
#import "ZSTF3Engine+ArticleA.h"
#import "ZSTUtils.h"

#define kMaxCommentLength 140


@implementation ZSTCommentCell

@synthesize delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier 
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) 
    {
        avatarImageView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(10, 13, 25, 25)];
        avatarImageView.adorn = ZSTModuleImage(@"module_articlea_avatar.png");
        avatarImageView.adjustsImageWhenHighlighted = NO;
        [self.contentView addSubview:avatarImageView];
        
        userName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(avatarImageView.frame)+10, 8, 150, 22)];
        userName.font = [UIFont systemFontOfSize:14];
        userName.textColor = RGBCOLOR(169, 169, 169);
        userName.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:userName];
        
        replayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        replayBtn.frame = CGRectMake(265, 13, 45, 22);
        replayBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        
        //47 47 47
        [replayBtn setTitleColor:RGBCOLOR(169, 169, 169) forState:UIControlStateNormal];
        [replayBtn addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
        [replayBtn setBackgroundImage:ZSTModuleImage(@"module_articlea_commentBtn_n.png") forState:UIControlStateNormal];
        [replayBtn setBackgroundImage:ZSTModuleImage(@"module_articlea_commentBtn_p.png") forState:UIControlStateHighlighted];
        [self.contentView addSubview:replayBtn];
        
        commentContent = [[NIAttributedLabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(avatarImageView.frame)+10, 280, 80)];
        commentContent.font = [UIFont systemFontOfSize:13];
        commentContent.backgroundColor = [UIColor clearColor];
        commentContent.textColor = RGBCOLOR(100, 100, 100);
        commentContent.lineBreakMode = NSLineBreakByCharWrapping;
        [self.contentView addSubview:commentContent];
        
        time = [[UILabel alloc] initWithFrame:CGRectMake(300-85, CGRectGetMaxY(commentContent.frame)+10, 85, 11)];
        time.font = [UIFont systemFontOfSize:11];
        time.textColor = RGBCOLOR(199, 199, 199);
        time.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:time];
        
        timeView = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_articlea_comment_timeImage.png")];
        timeView.frame = CGRectMake(CGRectGetMinX(time.frame)-14, CGRectGetMaxY(commentContent.frame)+10, 10, 11);
        [self.contentView addSubview:timeView];

        cellInfo = [[NSDictionary dictionary] retain];
        
    }
    return self;
}

- (void)click:(UIButton *)sender
{
    if ([delegate respondsToSelector:@selector(commentCellDic:)]) {
        [delegate commentCellDic:cellInfo];
    }
}

- (void)configCell:(NSDictionary*)cellDic cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *iconUrl = [cellDic safeObjectForKey:@"UserIcon"];
    [avatarImageView clear];
    avatarImageView.url = nil;
    avatarImageView.url = [NSURL URLWithString:iconUrl];
    [avatarImageView loadImage];
    
    NSString *msisdn = [cellDic safeObjectForKey:@"Msisdn"];
    if ([msisdn length] == 11) {
        msisdn = [msisdn stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    }
    userName.text = msisdn;
    
    NSNumber *floor = [cellDic safeObjectForKey:@"Floor"]; 
    NSString *floorStr = [[floor description] stringByAppendingString:@"楼"];
    [replayBtn setTitle:floorStr forState:UIControlStateNormal];
    
    NSString *replyTitle = [cellDic safeObjectForKey:@"ReplyTitle"];
    NSString *content = [cellDic safeObjectForKey:@"Content"];
    commentContent.text = [replyTitle stringByAppendingString:content];
    NSRange range = [commentContent.text rangeOfString:replyTitle];
    [commentContent setTextColor:RGBCOLOR(47, 47, 47) range:range];
    
    CGSize size = [commentContent.text sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:CGSizeMake(280, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
    commentContent.frame = CGRectMake(10, CGRectGetMaxY(avatarImageView.frame)+10, 280, size.height);
    
    NSString *timeStr = [cellDic safeObjectForKey:@"AddTime"];
    NSRange range2 = [timeStr rangeOfString:@"/Date("];
    NSRange range3 = [timeStr rangeOfString:@")"];
    NSRange range4 =  NSMakeRange(range2.length, range3.location-range2.length);
    NSString *addTime = [timeStr substringWithRange:range4];
    NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:[addTime floatValue]/1000]; 
    NSDate *curDate = [self getCurrentTime];
    if ([ZSTUtils isInOneDate:timeDate date2:curDate]) {
        NSDateComponents * components1 = [ZSTUtils dateComponents:timeDate];
        NSDateComponents * components2 = [ZSTUtils dateComponents:curDate];
        if (([components2 hour] - [components1 hour]) > 0) {
            time.text = [NSString stringWithFormat:@"%ld小时前", ([components2 hour] - [components1 hour])];
        } else if (([components2 minute] - [components1 minute]) > 0) {
            time.text = [NSString stringWithFormat:@"%ld分钟前", ([components2 minute] - [components1 minute])];
        } else if (([components2 second] - [components1 second]) > 0) {
            time.text = [NSString stringWithFormat:@"%ld秒前", ([components2 second] - [components1 second])];
        } else {
            time.text = @"刚刚";
        }
    } else {
        time.text = [ZSTUtils formatDate:timeDate format:@"yyyy/MM/dd HH:mm"];
    }
    
    CGSize timeSize = [time.text sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(280, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
    time.frame = CGRectMake(self.size.width - timeSize.width, CGRectGetMaxY(commentContent.frame)+10, timeSize.width, 11);
    timeView.frame = CGRectMake(CGRectGetMinX(time.frame)-14, CGRectGetMaxY(commentContent.frame)+10, 10, 11);

    cellInfo = cellDic;
}

- (NSDate *)getCurrentTime
{
    NSDate *date = [NSDate date];
//    NSTimeZone *zone = [NSTimeZone systemTimeZone];
//    NSInteger interval = [zone secondsFromGMTForDate: date];
//    NSDate *localeDate = [date  dateByAddingTimeInterval: interval];  
//    
//    NSLog(@"%@", localeDate);
    
    return date;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    UIImage *image = ZSTModuleImage(@"module_articlea_listSeparate.png") ;
    [image drawInRect:CGRectMake(10, self.height-1, 300, 1)];   

}

- (void)dealloc
{
    [self.engine cancelAllRequest];
    avatarImageView = nil;
    userName = nil;
    commentContent = nil;
    time = nil;
    replayBtn = nil;
    [super dealloc];
}

@end

@implementation ZSTArticleAEmptyTableViewCell

@synthesize introduceLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        introduceLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 240, 270)];
        introduceLabel.numberOfLines = 1;
        introduceLabel.textAlignment = NSTextAlignmentCenter;
        introduceLabel.textColor = RGBCOLOR(150, 150, 150);
        introduceLabel.backgroundColor = [UIColor clearColor];
        introduceLabel.font = [UIFont systemFontOfSize:18];
        [self.contentView addSubview:introduceLabel];
    }
    return self;
}

- (void)setIntroduce:(NSString *)introduce
{
    self.introduceLabel.text = introduce;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    CGContextRef cr = UIGraphicsGetCurrentContext();
    [[UIColor redColor] set];
    [[UIColor clearColor] set];
    CGContextFillRect(cr, self.bounds);
    
}
- (void)dealloc
{
    TKRELEASE(introduceLabel);
    [super dealloc];
}

@end



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@interface ZSTCommentListViewController ()

@end

@implementation ZSTCommentListViewController


@synthesize textView;
@synthesize commentList;
@synthesize commentArray;
@synthesize infob;
@synthesize replayInfo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    UIControl *control = [[UIControl alloc] initWithFrame:self.view.frame];
    [control addTarget:self action:@selector(recoverView) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:control];
    [control release];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"发表" , nil) target:self selector:@selector(sendMessage)];
    //\self.view.layer.contents = (id) ZSTModuleImage(@"module_articlea_bg.png").CGImage;

    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"评论" , nil)];
    
    UIImageView *inputView = [[UIImageView alloc] initWithImage:[ZSTModuleImage(@"module_articlea_inputImage.png") stretchableImageWithLeftCapWidth:12 topCapHeight:12]];
    inputView.frame = CGRectMake(10, 5, 300, 132);
    inputView.userInteractionEnabled = YES;
    [self.view addSubview:inputView];
    
    self.textView = [[[TKPlaceHolderTextView alloc] initWithFrame:CGRectMake(10, 6, 300, 128+(iPhone5?88:0))] autorelease];
    self.textView.font = [UIFont systemFontOfSize:16];
    [self.textView setPlaceholder:NSLocalizedString(@"在此输入评论..." , nil)];
    self.textView.backgroundImage = nil;
    self.textView.delegate = self;
    self.textView.placeholderColor = RGBCOLOR(199, 199, 199);
    self.textView.textColor = RGBCOLOR(47, 47, 47);
    [self.view addSubview:self.textView];

    replayTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(150, 110, 150, 19)];
    replayTitleLabel.backgroundColor = [UIColor clearColor];
    replayTitleLabel.font = [UIFont systemFontOfSize:14];
    replayTitleLabel.textColor = RGBCOLOR(199, 199, 199);
    [self.view addSubview:replayTitleLabel];
    replayTitleLabel.hidden = YES;
    
    replayTitleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [replayTitleBtn setImage:ZSTModuleImage(@"module_articlea_delete_replayTitle_n.png") forState:UIControlStateNormal];
    [replayTitleBtn setImage:ZSTModuleImage(@"module_articlea_delete_replayTitle_p.png") forState:UIControlStateHighlighted];
    replayTitleBtn.frame = CGRectMake(CGRectGetMinX(replayTitleLabel.frame)-19, 110, 19, 19);
    [replayTitleBtn addTarget:self action:@selector(deleteReplayTitle) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:replayTitleBtn];
    replayTitleBtn.hidden = YES;
    
    self.commentList = [[[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(inputView.frame)+8, 320, 270+(iPhone5?88:0)) style:UITableViewStylePlain] autorelease];
    self.commentList.backgroundColor = [UIColor clearColor];
    self.commentList.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.commentList.delegate = self;
    self.commentList.dataSource = self;
    self.commentList.scrollEnabled = YES;
    [self.view addSubview:self.commentList];
    
    if (_refreshHeaderView == nil) {
        
        _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f-250.0, self.view.frame.size.width, 250.0)];
        _refreshHeaderView.delegate = self;
        [commentList addSubview:_refreshHeaderView];
        
        [_refreshHeaderView refreshLastUpdatedDate];
    }
    
    
    if (_loadMoreView == nil) {
        _loadMoreView = [[TKLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
        _loadMoreView.activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
        _loadMoreView.delegate = self;
    }
    
    
    commentArray = [[NSMutableArray array] retain];

    [self.view newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkIndicatorView) {
        [self.engine getArticleAComment:[self.infob.msgID intValue] sinceId:0 fetchCount:10 orderType:SortType_Desc];

    }];
    
    [inputView release];
    
}

- (void)dealloc
{
    TKRELEASE(textView);
    TKRELEASE(commentList);
    TKRELEASE(commentArray);
    [super dealloc];
}

- (void)autoRefresh
{
    [_refreshHeaderView autoRefreshOnScroll:commentList animated:YES];
}

- (void)finishLoading
{
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:commentList];
    [_loadMoreView loadMoreScrollViewDataSourceDidFinishedLoading:commentList];
    
    _isLoadingMore = NO;
    _isRefreshing = NO;
}

- (void)refreshNewestData
{    
    [self.engine getArticleAComment:[self.infob.msgID intValue] sinceId:0 fetchCount:10 orderType:SortType_Desc];

}

- (void)loadMoreData {
    
    [self.engine getArticleAComment:[self.infob.msgID intValue] sinceId:[[[commentArray lastObject] objectForKey:@"CommentID"] integerValue] fetchCount:10 orderType:SortType_Asc];

}

- (void)appendData:(NSArray*)dataArray
{
    [self.commentArray addObjectsFromArray:dataArray];
}

- (void)insertData:(NSArray*)dataArray
{
    self.commentArray = [NSMutableArray arrayWithArray:dataArray];
}

#pragma mark HHLoadMoreViewDelegate Methods

- (void)loadMoreDidTriggerRefresh:(TKLoadMoreView*)view
{
    _isLoadingMore = YES;
    [self loadMoreData];
}

- (BOOL)loadMoreDataSourceIsLoading:(TKLoadMoreView*)view
{
    return _isLoadingMore;
}


- (void)fetchDataSuccess:(NSArray *)theData isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore
{
    _hasMore = hasMore;
    
    if (isLoadMore) {
        [self appendData:theData];
    }else {
        [self insertData:theData];
    }
    
    if (_hasMore) {
        commentList.tableFooterView = _loadMoreView;
    }else {
        commentList.tableFooterView = nil;
    }
    
    [commentList reloadData];
    
    [self finishLoading];
    
}

- (void)fetchDataFailed
{
    [self finishLoading];
}

#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view {
    _isRefreshing = YES;
    [self refreshNewestData];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView *)view {
    return _isRefreshing;
}

- (NSDate *)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView *)view {
    return [NSDate date];
}


#pragma mark - 

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    if (!_isLoadingMore) {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
    if (!_isRefreshing) {
        [_loadMoreView performSelector:@selector(loadMoreScrollViewDidEndDragging:) withObject:scrollView afterDelay:0];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (!_isLoadingMore) {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
    if (!_isRefreshing) {
        [_loadMoreView performSelector:@selector(loadMoreScrollViewDidEndDragging:) withObject:scrollView afterDelay:0];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    
    if (!_isLoadingMore) {
        [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
        
    }
    if (!_isRefreshing) {
        [_loadMoreView loadMoreScrollViewDidScroll:scrollView];
    }
    
}

#pragma mark - ZSTF3EngineArticleADelegate

- (void)requestDidFail:(ZSTResponse *)response
{
    [self fetchDataFailed];
    [self.view refreshFailed];
}

- (void)getArticleACommentDidFailed:(int)resultCode
{
    [self fetchDataFailed];
    [self.view refreshFailed];
}

- (void)getArticleACommentDidSucceed:(NSArray *)theCommentList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish
{ 
    commentList.scrollEnabled = YES;
    [self.view removeNetworkIndicatorView];
    [self fetchDataSuccess:theCommentList isLoadMore:isLoadMore hasMore:hasMore];
}

- (void)addArticleACommentDidSucceed:(id)userInfo
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"评论成功" , nil) withImage:nil];
    commentList.scrollEnabled = YES;
    [self recoverView];
    [self autoRefresh];

}

- (void)addArticleACommentDidFailed:(int)resultCode userInfo:(id)userInfo
{
    [self fetchDataFailed];
    [self.view refreshFailed];
}


#pragma mark - Actions

- (void)recoverView
{
    [textView resignFirstResponder];
    textView.text = @"";
    replayTitleBtn.hidden = YES;
    replayTitleLabel.text = @"";
    replayTitleLabel.hidden = YES;
}

- (void)sendMessage
{
    if (textView.text == nil || [textView.text length] == 0|| [textView.text isEqualToString:@""]) {
        [TKUIUtil alertInWindow:NSLocalizedString(@"请输入评论内容" , nil) withImage:nil];
        return;
    }
    
    NSString *replyTitleStr = nil;
    NSInteger parentId = 0;
    if (!replayTitleBtn.hidden && !replayTitleLabel.hidden) {
        NSNumber *floor = [self.replayInfo safeObjectForKey:@"Floor"]; 
        NSInteger floorInt = [floor intValue];
        replyTitleStr = [NSString stringWithFormat:NSLocalizedString(@"回复给%d楼：" , nil),floorInt];
        parentId = [[self.replayInfo safeObjectForKey:@"ParentID"] intValue];
    }

    [self.engine addArticleAComment:[self.infob.msgID intValue]  replyTitle:replyTitleStr content:textView.text parentId:parentId];
}
- (void)deleteReplayTitle
{
    replayTitleBtn.hidden = YES;
    replayTitleLabel.text = @"";
    replayTitleLabel.hidden = YES;
}

#pragma mark - ZSTCommentCellDelegate

- (void)commentCellDic:(NSDictionary *)cellDic
{
    [self.textView becomeFirstResponder];
    self.replayInfo = cellDic;
    replayTitleBtn.hidden = NO;
    replayTitleLabel.hidden = NO;
    
    NSNumber *floor = [self.replayInfo safeObjectForKey:@"Floor"]; 
    int floorInt = [floor intValue];
    NSString *replyTitleStr = [NSString stringWithFormat:NSLocalizedString(@"回复给%d楼" , nil),floorInt];
    replayTitleLabel.text = replyTitleStr;
    
    CGSize size = [replayTitleLabel.text sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(280, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
    replayTitleLabel.frame = CGRectMake(CGRectGetMaxX(textView.frame)-size.width-10, 110, size.width, 19);
    replayTitleBtn.frame = CGRectMake(CGRectGetMinX(replayTitleLabel.frame)-19, 110, 19, 19);
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)theTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    return YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [commentArray count] ? [commentArray count] : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([commentArray count] == 0) {
        commentList.scrollEnabled = NO;
        static NSString *identifier = @"ZSTArticleAEmptyTableViewCell";
        ZSTArticleAEmptyTableViewCell *cell = [commentList dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[[ZSTArticleAEmptyTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.introduceLabel.text = NSLocalizedString(@"还没有评论哦～" , nil);
        return cell;
        
    }else{
        
        static NSString *identifier = @"cell";
        ZSTCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[[ZSTCommentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.delegate = self;
        
        }
        
        if ([commentArray count] != 0) {
            NSDictionary *dic = [commentArray objectAtIndex:indexPath.row];
            [cell configCell:dic cellForRowAtIndexPath:indexPath];
        }
        
        return cell;
    }
}

#pragma mark - UITableViewDelegate

- (float)heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *replyTitle = [[commentArray objectAtIndex:indexPath.row] safeObjectForKey:@"ReplyTitle"];
    NSString *content = [[commentArray objectAtIndex:indexPath.row] safeObjectForKey:@"Content"] ;
    CGSize size = [[replyTitle stringByAppendingString:content] sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:CGSizeMake(280, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
    
    return size.height+75;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([commentArray count]) {
        return [self heightForRowAtIndexPath:indexPath];
    }
    return 60;

}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)searchArticleAListDidSucceed:(NSArray *)resultList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish
{
    
}

- (void)getArticleADidFailed:(int)resultCode
{
    
}

- (void)manageArticleAFavoritesDidSucceed:(id)userInfo
{
    
}

- (void)getArticleACategoryDidSucceed:(NSDictionary *)defaultIndexCategory categories:(NSArray *)categories
{
    
}

- (void)searchArticleAListDidFailed:(int)response
{
    
}

- (void)getArticleAContentDidSucceed:(NSDictionary *)newsContent userInfo:(id)userInfo
{
    
}

- (void)getArticleACategoryDidFailed:(int)resultCode
{
    
}

- (void)getArticleAContentDidFailed:(ZSTResponse *)response
{
    
}

- (void)getArticleAFavoritesDidFailed:(int)resultCode
{
    
}

- (void)getArticleADidSucceed:(NSArray *)newsList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish
{
    
}

- (void)getArticleAFavoritesDidSucceed:(NSArray *)newsList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish
{
    
}

- (void)manageArticleAFavoritesDidFailed:(int)resultCode userInfo:(id)userInfo
{
    
}

@end
