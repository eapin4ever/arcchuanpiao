
//
//  NewsMainCell.m
//  F3
//
//  Created by admin on 12-7-17.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTArticleAMainCell.h"
#import "ZSTGlobal+ArticleA.h"
#import "ZSTDao+ArticleA.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ZSTArticleAMainCell

@synthesize myImageView;
@synthesize title;
@synthesize subTitle;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier 
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) 
    {
        self.frame = CGRectMake(0, 0, 290, 47);
//        imageView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(24, 12, 24, 24)];
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(24, 12, 24, 24)];
//        imageView.defaultImage = ZSTModuleImage(@"module_articlea_table_default_img.png");
//        imageView.adjustsImageWhenHighlighted = NO;
        [self.contentView addSubview:imageView];
        
        title = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame)+17, 12, self.frame.size.width - CGRectGetMaxX(imageView.frame)-20, 23)];
        title.backgroundColor = [UIColor clearColor];
        title.textColor = RGBCOLOR(43, 43, 43);
        title.font = [UIFont boldSystemFontOfSize:17];
        title.text =  @"";
        title.highlightedTextColor = RGBCOLOR(69, 71, 76);
        title.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:title];
        
        UIImageView *rightImgView = [[UIImageView alloc] initWithFrame:CGRectMake(285, (self.frame.size.height - 12)/2.0, 7, 12)];
        rightImgView.image = ZSTModuleImage(@"module_setting_icon_right.png");
        [self addSubview:rightImgView];

    }
    return self;
}

- (void)configCell:(ZSTArticleAVO*)nvo cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    title.text = nvo.title;
    
//    [imageView clear];
//    imageView.url = [NSURL URLWithString:nvo.iconUrl];
//    [imageView loadImage];
    [imageView setImageWithURL:[NSURL URLWithString:nvo.iconUrl] placeholderImage:ZSTModuleImage(@"module_articlea_table_default_img.png")];
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    UIImage *image = ZSTModuleImage(@"module_articlea_table_separator.png") ;
    [image drawInRect:CGRectMake(1, self.height-1, 320-2, 1)];
    
}


- (void)dealloc
{
    self.myImageView = nil;
    self.title = nil;
    self.subTitle = nil;
    [super dealloc];
}

@end



@implementation ZSTArticleACarouselViewCell

@synthesize carouselView;
@synthesize dataArry;
@synthesize carouselViewCellDelegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier 
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) 
    {
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //轮播图
        carouselView = [[ZSTArticleACarouselView alloc] initWithFrame:CGRectMake(0, 0, 320, 160)];
        carouselView.carouselDataSource = self;
        carouselView.carouselDelegate = self;
        [self.contentView addSubview:carouselView];
    }
    return self;
}
- (void)configCell:(NSArray*)array
{
    [dataArry release];
    self.dataArry = nil;
    self.dataArry = [array retain];
    
    if ([dataArry count] != 0) {
        [carouselView reloadData];
    }
}

#pragma mark - ZSTNewsCarouselViewDataSource

- (NSInteger)numberOfViewsInCarouselView:(ZSTArticleACarouselView *)newsCarouselView
{
    return [dataArry count];
}
- (NSDictionary *)carouselView:(ZSTArticleACarouselView *)carouselView infoForViewAtIndex:(NSInteger)index
{
    if ([dataArry count] != 0) {
        return [dataArry objectAtIndex:index];
    }
    return nil;
}

#pragma mark - ZSTNewsCarouselViewDelegate

- (void)carouselView:(ZSTArticleACarouselView *)theCarouselView didSelectedViewAtIndex:(NSInteger)index
{
    if ([carouselViewCellDelegate respondsToSelector:@selector(carouselViewCell:didSelectedViewAtIndex:)]) {
        [carouselViewCellDelegate carouselViewCell:theCarouselView didSelectedViewAtIndex:index];
    }
}

- (void)dealloc
{
    [super dealloc];
}
@end
