//
//  TKResourceURL.h
//  News
//
//  Created by luobin on 7/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

/**
 *	@brief	自定义资源缓存协议
 *  resource://?resource=%@&originalUrl=%@&MIMEType=image/png
 */

@interface ZSTARTICLEATKResourceURL : NSURL {
    
}

+ (ZSTARTICLEATKResourceURL*) resourceURLWithURL:(NSURL *)url;

+ (ZSTARTICLEATKResourceURL*) resourceURLWithResource:(NSString *)resource originalUrl:(NSString *)originalUrl MIMEType:(NSString *)MIMEType;

+ (BOOL)isResourceURL:(NSURL *)url;

@property (nonatomic, retain, readonly) NSString *resource;
@property (nonatomic, retain, readonly) NSString *MIMEType;
@property (nonatomic, retain, readonly) NSString *originalUrl;

@end
