//
//  NewsContentViewController.h
//  F3
//
//  Created by admin on 12-7-19.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTArticleAVO.h"
#import "MGTemplateEngine.h"
#import "ICUTemplateMatcher.h"
#import "TKHorizontalTableView.h"
#import "ZSTF3Engine+ArticleA.h"
#import "ZSTArticleAContentVO.h"
#import "TKAsynImageView.h"
#import "MTZoomWindowDelegate.h"
#import <MessageUI/MessageUI.h>
#import "ZSTHHSinaShareController.h"
#import "ZSTLoginViewController.h"
#import <PMRepairButton.h>

typedef enum
{
    FontSize_Small = 0,
    FontSize_Normal = 1,
    FontSize_Middle = 2,                        
    FontSize_Large = 3
} ZSTArticleAFontSize;

@interface ZSTArticleAContentViewController : UIViewController <UIWebViewDelegate, MGTemplateEngineDelegate,
    TKHorizontalTableViewDelegate,TKHorizontalTableViewDataSource,MTZoomWindowDelegate,HJManagedImageVDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,MFMessageComposeViewControllerDelegate,ZSTHHSinaShareControllerDelegate,LoginDelegate> {
    
    TKAsynImageView *selectedImageView;
    
    NSArray *newsArr;

    TKHorizontalTableView *hTableView;
    
    NSString *requestUrl;
        
    ZSTArticleAFontSize fontSize;
    NSMutableDictionary *favStateDic;
        
}
@property (nonatomic, retain) TKHorizontalTableView *hTableView;
@property (nonatomic,retain) NSArray *newsArr;
@property (nonatomic,retain) TKAsynImageView *selectedImageView;

//@property (nonatomic, retain) UIButton *favBtn;
@property (nonatomic,retain) PMRepairButton *favBtn;

@property (nonatomic, retain) ZSTArticleAVO *vo ;
@property (nonatomic ,retain) ZSTArticleAContentVO *contentVo;

@property (nonatomic, assign) NSInteger selectedIndex;

- (CGSize)displayRectForImage:(CGSize)imageSize;
@end
