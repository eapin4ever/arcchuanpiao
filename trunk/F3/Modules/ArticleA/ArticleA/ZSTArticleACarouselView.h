//
//  ZSTNewsCarouselView.h
//  F3
//
//  Created by xuhuijun on 12-7-18.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TKAsynImageView.h"

@class ZSTArticleACarouselView;

@protocol ZSTNewsCarouselViewDataSource <NSObject>

@optional
- (NSInteger)numberOfViewsInCarouselView:(ZSTArticleACarouselView *)newsCarouselView;
- (NSDictionary *)carouselView:(ZSTArticleACarouselView *)carouselView infoForViewAtIndex:(NSInteger)index;

@end

@protocol ZSTNewsCarouselViewDelegate <NSObject>

@optional
- (void)carouselView:(ZSTArticleACarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;

@end

@interface ZSTArticleACarouselView : UIView <HJManagedImageVDelegate,UIScrollViewDelegate,TKAsynImageViewDelegate>
{
    UIScrollView *_scrollView;
    UILabel *_describeLabel;
    UIPageControl *_pageControl;
    NSTimer *_carouselTimer;
    
    id<ZSTNewsCarouselViewDataSource> _carouselDataSource;
    id<ZSTNewsCarouselViewDelegate>  _carouselDelegate;
}

@property(nonatomic, assign) id<ZSTNewsCarouselViewDataSource> carouselDataSource;
@property(nonatomic, assign) id<ZSTNewsCarouselViewDelegate> carouselDelegate;

- (void)reloadData;
- (void)selectTabAtIndex:(int)index;
- (void)carouselViewStopAnimation;

@end
