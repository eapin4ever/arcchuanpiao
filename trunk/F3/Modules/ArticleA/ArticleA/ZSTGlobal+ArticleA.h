//
//  ZSTGlobal+News.h
//  News
//
//  Created by luobin on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    ZSTCategoryType_DefaultIndex = 1,              //默认Category                 
    ZSTCategoryType_Index = 2,                     //展示在首页
    ZSTCategoryType_More = 4                       //展示在更多
} ZSTCategoryType;                                     //Category类型

typedef enum
{
    ZSTIconType_List = 1,                          //列表ICon                 
    ZSTIconType_Broadcast = 2                      //轮播ICon
} ZSTIconType;                                     //icon类型

typedef enum
{
    ZSTNewsTypen_Normal = 0,                       //普通                 
    ZSTNewsType_Photos = 1,                        //图集
    ZSTNewsType_Video = 2,                         //视频
    ZSTNewsType_Special = 3                        //专题
} ZSTNewsType;                                     //新闻信息种类

#define NEWS_PAGE_SIZE 40

#define SortType_Desc @"Desc"               //降序 (刷新)
#define SortType_Asc @"Asc"                 //升序（更多）


//////////////////////////////////////////////////////////////////////////////////////////

#define ArticleABaseURL @"http://mod.pmit.cn/ArticleA"//最新接口
//#define ArticleABaseURL @"http://192.168.21.10:90/ArticleA"//最新接口

#define GetInfoCatagory ArticleABaseURL @"/getcatagory"
#define GetInfoListByPage  ArticleABaseURL @"/getinfolistbypage"
#define GetInfoContent ArticleABaseURL @"/GetInfoContent"
#define GetInfoFile ArticleABaseURL @"/GetFile"

#define InfoManageFavorites ArticleABaseURL @"/ManageFavorites"//用户收藏管理
#define GetInfoFavorites ArticleABaseURL @"/GetFavorites"//用户收藏获取
#define InfoAddComment ArticleABaseURL @"/AddComment"//文章评论添加
#define GetInfoComment ArticleABaseURL @"/GetComment"//文章评论获取

