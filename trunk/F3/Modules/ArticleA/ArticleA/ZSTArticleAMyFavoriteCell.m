//
//  ZSTArticleAMyFavoriteCellCell.m
//  infob
//
//  Created by luobin on 11/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTArticleAMyFavoriteCell.h"

@implementation ZSTArticleAMyFavoriteCell

@synthesize articleImageView;
@synthesize title;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.frame = CGRectMake(0, 0, 320, 47);
        articleImageView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(35, 12, 23, 23)];
        articleImageView.defaultImage = ZSTModuleImage(@"module_articlea_table_default_img.png");
        articleImageView.adjustsImageWhenHighlighted = NO;
        [self.contentView addSubview:articleImageView];
        
        
        title = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(articleImageView.frame)+15, 12, 250, 23)];
        title.backgroundColor = [UIColor clearColor];
        title.textColor = RGBCOLOR(69, 71, 76);
        title.font = [UIFont boldSystemFontOfSize:17];
        title.text =  @"";
        title.highlightedTextColor = RGBCOLOR(69, 71, 76);
        title.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:title];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCell:(NSDictionary *)article
{
    title.text = [article safeObjectForKey:@"Title"];

    [articleImageView clear];
    articleImageView.url = [NSURL URLWithString:[article safeObjectForKey:@"IconUrl"]];
    [articleImageView loadImage];
}

- (void)dealloc
{
    self.articleImageView = nil;
    self.title = nil;

    [super dealloc];
}

@end
