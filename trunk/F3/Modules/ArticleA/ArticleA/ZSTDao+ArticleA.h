//
//  ZSTDao.h
//  News
//  
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ZSTGlobal+ArticleA.h"

@interface ZSTDao(ArticleA) 

- (void)createTableIfNotExistForArticleAModule;

- (BOOL) addcolumnTableWithName:(NSString *)name;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief 添加分类
 *
 *	@param 	categoryID      分类ID
 *	@param 	categoryName 	分类名称
 *	@param 	orderNum        排序字段
 *  @param  defaultFlag     是否是默认头条
 *	@return	操作是否成功
 */
- (BOOL)addArticleACategory:(NSInteger)categoryID
       categoryName:(NSString *)categoryName
                orderNum:(NSInteger)orderNum
                 version:(NSInteger)version
             defaultFlag:(BOOL)defaultFlag
               indexFlag:(BOOL)indexFlag
                 iconUrl:(NSString *)iconUrl
                 linkUrl:(NSString *)linkUrl;

/**
 *	@brief 删除分类
 *
 *	@return	操作是否成功
 */
- (BOOL)deleteAllArticleACategories;

- (BOOL)deleteCategoryOfId:(NSInteger)categoryId;

/**
 *	@brief 获取默认缺省分类
 *
 *	@return	操作是否成功
 */
- (NSDictionary *)getDefaultArticleACategory;

/**
 *	@brief  获取所有分类
 *	@return	操作是否成功
 */
- (NSArray *)getArticleACategories;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief 添加新闻
 *  
 *  @param 	msgID           新闻ID
 *	@param 	categoryID      分类ID
 *	@param 	msgType         新闻信息种类，0：普通；1：图集；2：视频；3：专题
 *	@param 	title           新闻标题
 *	@param 	summary         新闻信息摘要
 *	@param 	iconFileID      ICon图片编号
 *	@param 	iconType        ICon图片类别，1：列表ICon；2：轮播ICon
 *	@param 	orderNum        新闻信息来源
 *  @param  addTime         添加时间
 *	@return	操作是否成功
 */
- (BOOL)addArticleA:(NSInteger)msgID
         categoryID:(NSInteger)categoryID
            msgType:(int)msgType
              title:(NSString *)title
            keyword:(NSString *)keyWord
            iconUrl:(NSString *)iconUrl
            addTime:(NSDate *)addTime
           orderNum:(NSInteger)orderNum
             source:(NSString *)source
            version:(NSInteger)version;

/**
 *	@brief  设置新闻是否已读
 *
 *  @param  isRead         设置是否已读
 *	@return	操作是否成功
 */
- (BOOL)setArticleA:(NSInteger)infobID isRead:(BOOL)isRead;

/**
 *	@brief  获取新闻是否已读
 *
 *  @param  isRead         设置是否已读
 *	@return	新闻是否已读
 */
- (BOOL)getArticleAIsRead:(NSInteger)InfobID;

/**
 *	@brief 删除新闻
 *
 *	@return	操作是否成功
 */
- (BOOL)deleteArticleA:(NSInteger)infobID;

/**
 *	@brief 删除所有轮播新闻
 *
 *	@return	操作是否成功
 */
//- (BOOL)deleteAllArticleAOfBroadcast;

/**
 *	@brief 删除指定分类的新闻
 *
 *	@return	操作是否成功
 */
- (BOOL)deleteAllArticleAOfCategory:(NSInteger)categoryID;

/**
 *	@brief 判断新闻是否已存在
 *
 *	@return	新闻是否存在
 */
- (BOOL)ArticleAExist:(NSInteger)infobID;

/**
 *	@brief 获取分类下第index条新闻
 *
 *	@return	新闻
 */
- (NSDictionary *)ArticleAAtIndex:(int)index category:(NSInteger)categoryID;

/**
 *	@brief 获取轮播新闻
 *  
 *	@return	轮播新闻
 */
- (NSArray *)getBroadcastArticleA;

/**
 *	@brief 获取新闻
 *  
 *  @param 	pageNum             页数
 *	@param 	pageCount           每页新闻条数
 *	@param 	categoryID          分类ID
 *	@return	新闻列表
 */
- (NSArray *)ArticleAAtPage:(int)pageNum pageCount:(int)pageCount category:(NSInteger)categoryID;

/**
 *	@brief 返回分类下的新闻条数
 *  
 *	@return	新闻条数
 */
- (NSUInteger)ArticleACountOfCategory:(NSInteger)categoryID;

/**
 *	@brief 返回分类下的最新的新闻的ID
 *  
 *	@return	分类下的最新的新闻的ID
 */
- (NSUInteger) getMaxArticleAIdOfCategory:(NSInteger)categoryID;

- (NSUInteger) getMinArticleAIdOfCategory:(NSInteger)categoryID;

- (NSArray *)ArticleAIdOfCategory:(NSInteger)categoryId;

- (NSArray *)getArticleACategory:(NSInteger)categoryId;


@end
