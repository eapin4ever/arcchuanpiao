//
//  NewsMainViewController.h
//  F3
//  iphone size:320*480
//  状态栏：20
//  蓝条：44
//  底部Tab：49
//  Created by fujinsheng on 12-7-17.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTArticleAMainCell.h"
#import "ZSTF3Engine.h"
#import "ZSTArticleAVO.h"
#import "ZSTArticleAContentVO.h"
#import "ZSTArticleAContentViewController.h"
#import "ZSTModuleBaseViewController.h"
#import "ColumnBar.h"
#import "ZSTLoginViewController.h"
#import <PMRepairButton.h>

@interface ZSTArticleAViewController : ZSTModuleBaseViewController <UITableViewDelegate,UITableViewDataSource,TKLoadMoreViewDelegate,UIScrollViewDelegate,UISearchBarDelegate,ColumnBarDelegate,ColumnBarDataSource,LoginDelegate>
//ZSTNewsCarouselViewDataSource，ZSTNewsCarouselViewDelegate both unused...tableview is a subClass of scrollview.
{
    TKLoadMoreView *_loadMoreView;
    BOOL _isRefreshing;
    BOOL _isLoadingMore;
    BOOL _hasMore;
    
    ColumnBar *_columnBar;
    NSInteger currentSelectItem;//当前选中得分类数字
    UITableView *newsTable;
    UIView *_columnView;
    
    
    NSMutableArray *newsArr;//普通列表数组
    NSMutableArray *indexCategoryArr;//导航展示分类数组
    NSMutableArray *refreshCategoryArr;//重启重置的分类数组，依据此数据判断分类是否需要更新
    
    NSMutableDictionary *_timeDic;//计时字典
    
    UIImageView* imageView;

    int _pagenum;
    
    UIPanGestureRecognizer* recoginzer;
    CGPoint searchOriginPoint;
//    UIButton* goSearchButton;
    PMRepairButton *goSearchButton;
    
    UISearchBar *searchBar;
     NSString *filter;
}
@property (nonatomic,retain) NSMutableArray *newsArr;
//@property (nonatomic,retain) UIButton* goSearchButton;
@property (nonatomic,retain) PMRepairButton *goSearchButton;
@property (nonatomic, retain) UISearchBar *searchBar;
@property (nonatomic, retain) UIView *disableViewOverlay;
@property (nonatomic, retain) NSString *filter;


@end
