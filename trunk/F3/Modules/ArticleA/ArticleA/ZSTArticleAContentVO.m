//
//  NewsContentVO.m
//  News
//
//  Created by admin on 12-7-25.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTArticleAContentVO.h"

@implementation ZSTArticleAContentVO

@synthesize title;//主标题
@synthesize content;
@synthesize fileArr;
@synthesize fileId;
@synthesize fileType;
@synthesize addTime;//日期
@synthesize source;//来源
@synthesize notice;
@synthesize result;
@synthesize contentArr;
@synthesize wapUrl;
@synthesize shareurl;


+ (id)voWithDic:(NSDictionary *)dic
{
    return [[[self alloc] initWithDic: dic] autorelease];
}

- (id)initWithDic:(NSDictionary *)dic
{
    if( (self=[super init])) {
        self.title =  [dic safeObjectForKey:@"Title"];
        self.content = [dic safeObjectForKey:@"content"];
        self.fileArr = [dic safeObjectForKey:@"File"];
        self.notice = [dic safeObjectForKey:@"Notice"];
        self.addTime = [dic safeObjectForKey:@"AddTime"];
        self.source = [dic safeObjectForKey:@"Source"];
        self.result = [dic safeObjectForKey:@"Result"];
        self.contentArr = [dic safeObjectForKey:@"Content"];
        self.wapUrl = [dic safeObjectForKey:@"WapUrl"];
        self.shareurl = [dic safeObjectForKey:@"ShareUrl"];

    }
    return self;
}

-(NSNumber*)getFileId {
    return  [dict safeObjectForKey:@"FileID"];
} 

-(NSNumber*)getFileType {
    return  [dict safeObjectForKey:@"FileType"];
} 

- (void)dealloc 
{
    self.title = nil;
    self.content = nil;
    self.fileArr = nil;
    self.addTime = nil;
    self.source = nil;
    self.notice = nil;
    self.result = nil;
    self.contentArr = nil;
    self.wapUrl = nil;
    
    [super dealloc];
}

@end
