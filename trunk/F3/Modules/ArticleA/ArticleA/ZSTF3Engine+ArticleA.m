//
//  ZSTF3Engine+News.m
//  News
//
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTF3Engine+ArticleA.h"
#import "ZSTCommunicator.h"
#import "ZSTDao+ArticleA.h"
#import "ZSTF3Preferences.h"
#import "ZSTLegacyResponse.h"
#import "ZSTSqlManager.h"
#import "JSON.h"

#define ArticleAContentCachePath [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"/ArticleA/content"]

@implementation ZSTF3Engine (ArticleA)

////////////////////////////////////////////////////////////////// category ///////////////////////////////////////////////////////////////////////

- (void)getArticleACategory
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetInfoCatagory stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                        params:params
                                          target:self                 
                                      selector:@selector(getArticleACategoryResponse:userInfo:) 
                                        userInfo:nil
                                        matchCase:YES];
}

- (void)getArticleACategoryResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode != ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(getArticleACategoryDidFailed:)]) {
            [self.delegate getArticleACategoryDidFailed:response.resultCode];
        }
        return;
    }
    
    id categories = [response.jsonResponse safeObjectForKey:@"Info"];
    if (![categories isKindOfClass:[NSArray class]]) {
        categories = [NSArray array];
    }
    
//    [self.dao deleteAllArticleACategories];
    NSArray *oldCategories = [self.dao getArticleACategories];
    
    //入库
    [ZSTSqlManager beginTransaction];
    for (NSMutableDictionary *category in categories){
        NSInteger categoryID = [[category safeObjectForKey:@"CategoryID"] integerValue];
        
        NSArray *categoryArr = [self.dao getArticleACategory:categoryID];
        if (categoryArr==nil || [categoryArr count]==0) {
            [self.dao addArticleACategory:categoryID
                             categoryName:[category safeObjectForKey:@"CategoryName"]
                                 orderNum:[[category safeObjectForKey:@"OrderNum"] intValue]
                                  version:[[category safeObjectForKey:@"Version"] intValue]
                              defaultFlag:[[category safeObjectForKey:@"DefaultFlag"] boolValue]
                                indexFlag:[[category safeObjectForKey:@"IndexFlag"] boolValue]
                                  iconUrl:[category safeObjectForKey:@"IconUrl"]
                                  linkUrl:[category safeObjectForKey:@"LinkUrl"]];
        } else {
            NSDictionary *oldCategory = [categoryArr objectAtIndex:0];
            NSInteger oldVersion = [[oldCategory safeObjectForKey:@"Version"] integerValue];
            NSInteger newVersion = [[category safeObjectForKey:@"Version"] integerValue];
            if (oldVersion != newVersion) {
                [self deleteAllArticleAOfCategory:categoryID];
                [self.dao deleteCategoryOfId:categoryID];
                
                [self.dao addArticleACategory:categoryID
                                 categoryName:[category safeObjectForKey:@"CategoryName"]
                                     orderNum:[[category safeObjectForKey:@"OrderNum"] intValue]
                                      version:[[category safeObjectForKey:@"Version"] intValue]
                                  defaultFlag:[[category safeObjectForKey:@"DefaultFlag"] boolValue]
                                    indexFlag:[[category safeObjectForKey:@"IndexFlag"] boolValue]
                                      iconUrl:[category safeObjectForKey:@"IconUrl"]
                                      linkUrl:[category safeObjectForKey:@"LinkUrl"]];
            }
        }

    }
    
    if (oldCategories!=nil && [oldCategories count]>0) {
        for (NSMutableDictionary *oldCategory in oldCategories) {
            NSInteger oldCategoryID = [[oldCategory safeObjectForKey:@"CategoryID"] integerValue];
            BOOL isExist = NO;
            for (NSMutableDictionary *newCategory in categories) {
                NSInteger newCategoryID = [[newCategory safeObjectForKey:@"CategoryID"] integerValue];
                if (oldCategoryID == newCategoryID) {
                    isExist = YES;
                    break;
                }
            }
            
            if (!isExist) {
                [self deleteAllArticleAOfCategory:oldCategoryID];
                [self.dao deleteCategoryOfId:oldCategoryID];
            }
        }
    }
    [ZSTSqlManager commit];
    
    if ([self isValidDelegateForSelector:@selector(getArticleACategoryDidSucceed:categories:)]) {
        [self.delegate getArticleACategoryDidSucceed:[self.dao getDefaultArticleACategory]
                         categories:[self.dao getArticleACategories]];
    }
}

- (void)deleteAllArticleAOfCategory:(NSInteger)categoryID
{
    NSArray *infobIds = [self.dao ArticleAIdOfCategory:categoryID];
    if (infobIds!=nil && [infobIds count]>0) {
        for (int i=0; i<[infobIds count]; i++) {
            NSDictionary *infobIdDic = [infobIds objectAtIndex:i];
            int infobId = [[infobIdDic safeObjectForKey:@"MsgID"] intValue];
            [self deleteArticleAContentById:infobId];
        }
        [self.dao deleteAllArticleAOfCategory:categoryID];
    }
}

////////////////////////////////////////////////////////////////// newsList ///////////////////////////////////////////////////////////////////////

- (void)getArticleA:(NSInteger)categoryID 
            sinceID:(NSInteger)sinceID 
          orderType:(NSString *)orderType 
         fetchCount:(NSUInteger)fetchCount 
           iconType:(ZSTIconType)iconType 
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (categoryID) {
//        [params setSafeObject:[NSNumber numberWithInt:156] forKey:@"CategoryID"];
        [params setSafeObject:[NSNumber numberWithInteger:categoryID] forKey:@"CategoryID"];
    }
    if (sinceID) {
        [params setSafeObject:[NSNumber numberWithInteger:sinceID] forKey:@"PageIndex"];
    }
//    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:orderType forKey:@"OrderType"];
    [params setSafeObject:[NSString stringWithFormat:@"%ld", fetchCount] forKey:@"Size"];
    
    TKDINFO(@"Will post param:%@", params);
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetInfoListByPage  stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                        params:params 
                                          target:self                                      
                                      selector:@selector(getArticleAResponse:userInfo:) 
                                          userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:YES];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getArticleAResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode != ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(getArticleADidFailed:)]) {
            [self.delegate getArticleADidFailed:response.resultCode];
        }
        return;
    }
    id newsList = [response.jsonResponse safeObjectForKey:@"Info"];
    if (![newsList isKindOfClass:[NSArray class]]) {
        newsList = [NSArray array];
    }
    
    NSDictionary *params = [userInfo safeObjectForKey:@"Params"];
    NSString *sortType = [params safeObjectForKey:@"OrderType"];
    NSInteger categoryID = [[params safeObjectForKey:@"CategoryID"] integerValue];
    
    NSInteger pageIndex = [[params safeObjectForKey:@"PageIndex"] integerValue];
        
    if ([self isValidDelegateForSelector:@selector(getArticleADidSucceed:isLoadMore:hasMore:isFinish:)]) {
        [self.delegate getArticleADidSucceed:newsList isLoadMore:[sortType isEqualToString:SortType_Asc] hasMore:[[response.jsonResponse safeObjectForKey:@"HasMore"] boolValue] isFinish:YES];
    }
    
    if (pageIndex == 1) {
        
        //如果获取到TOPIC_PAGE_SIZE条(count)，则删除原数据(因为不连续)
        if ([sortType isEqualToString:SortType_Desc]) {
            [self.dao deleteAllArticleAOfCategory:categoryID];
        }
    
        //入库
        [ZSTSqlManager beginTransaction];
        for (NSMutableDictionary *post in newsList) {
            NSInteger msgID = [[post safeObjectForKey:@"MsgID"] integerValue];
            //        BOOL isExist = [[ZSTPetmiiDao shared] postExist:postID];
        
            //服务器返回时间格式 '/Date(1342768136510)/'
            NSString *date = [[[post safeObjectForKey:@"AddTime"] stringByReplacingOccurrencesOfString:@"/Date(" withString:@""] stringByReplacingOccurrencesOfString:@")/" withString:@""];

            [self.dao addArticleA:msgID
                       categoryID:[[post safeObjectForKey:@"CategoryID"] integerValue]
                          msgType:[[post safeObjectForKey:@"MsgType"] intValue]
                            title:[post safeObjectForKey:@"Title"]
                          keyword:[post safeObjectForKey:@"Keyword"]
                          iconUrl:[post safeObjectForKey:@"IconUrl"]
                          addTime:[NSDate dateWithTimeIntervalSince1970:[date doubleValue]/1000]
                         orderNum:[[post safeObjectForKey:@"OrderNum"] intValue]
                           source:[post safeObjectForKey:@"Source"]
                          version:[[post safeObjectForKey:@"Version"] integerValue]];
        }
        [ZSTSqlManager commit];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getArticleAAtPage:(int)pageNum category:(NSInteger)categoryID
{
	//如果是loadMore (pageNum>1),则从本地数据获取
	if (pageNum>1){
        
        NSArray *tempLocalNews = [self.dao ArticleAAtPage:pageNum pageCount:NEWS_PAGE_SIZE category:categoryID];
        if ([tempLocalNews count] == 0) {
//            NSInteger newNewsMinId = [self.dao getMinArticleAIdOfCategory:categoryID];
            [self getArticleA:categoryID
                      sinceID:pageNum 
                    orderType:SortType_Desc 
                   fetchCount:NEWS_PAGE_SIZE
                     iconType:ZSTIconType_List];
        } else {
            if ([self isValidDelegateForSelector:@selector(getArticleADidSucceed:isLoadMore:hasMore:isFinish:)]) {
                BOOL hasmore = ([tempLocalNews count] >= NEWS_PAGE_SIZE);
                [self.delegate getArticleADidSucceed:tempLocalNews isLoadMore:YES  hasMore:hasmore isFinish:YES];
            }
        }
	} else {
        [self getArticleA:categoryID 
                  sinceID:1
                orderType:SortType_Desc 
               fetchCount:NEWS_PAGE_SIZE 
                 iconType:ZSTIconType_List];
	}
}

////////////////////////////////////////////////////////////////// News content /////////////////////////////////////////////////////////////////////

//新闻内容缓存到文件中
- (void)getArticleAContent:(NSInteger)newsID userInfo:(id)anUserInfo version:(NSInteger)version
{
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    NSError *error;
//    if (![fileManager fileExistsAtPath:ArticleAContentCachePath]) {
//        if (![fileManager createDirectoryAtPath:ArticleAContentCachePath withIntermediateDirectories:YES attributes:nil error:&error]) {
//            if ([self isValidDelegateForSelector:@selector(getArticleAContentDidFail:)]) {
//                ZSTResponse *response = [[ZSTResponse alloc] init];
//                response.request = nil;
//                response.stringResponse = nil;
//                response.userInfo = anUserInfo;
//                response.resultCode = ZSTResultCode_Other;
//                response.errorMsg = @"Directory create Failed.";
//                [self.delegate performSelector:@selector(getArticleAContentDidFail:) withObject:response afterDelay:0];
//            }
//            return;
//        }
//    }
//    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:newsID], @"infobID", anUserInfo, @"userInfo", [NSNumber numberWithInteger:version], @"Version", nil];
//    
//    //检查缓存是否有数据,如果有直接从缓存获取
//    NSString *contentPath = [ArticleAContentCachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%d_%d.json", newsID, version]];
//    BOOL isDirectory;
//    if ([fileManager fileExistsAtPath:contentPath isDirectory:&isDirectory]) {
//        if (!isDirectory) {
//            NSDictionary *attributes = [fileManager attributesOfItemAtPath:contentPath error:&error];
//            if (attributes && [attributes fileSize]) {
//                NSString *content = [NSString stringWithContentsOfFile:contentPath encoding:NSUTF8StringEncoding error:&error];
//                if (content) {
//                    id json = [content JSONValue];
//                    if (json) {
//                        [self performSelector:@selector(getArticleAContentResponse:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:json, @"json", userInfo, @"userInfo", nil] afterDelay:0.5];
//                        return;
//                    }
//                    
//                } else {
//                    TKDERROR(@"error == %@", error);
//                }
//            } else {
//                TKDERROR(@"error == %@", error);
//            }
//        }
//    }
//    //从缓存获取失败，删除缓存，重新从网络获取
//    [fileManager removeItemAtPath:contentPath error:&error];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[NSNumber numberWithInteger:newsID] forKey:@"MsgID"];

    [[ZSTCommunicator shared] openAPIPostToPath:[GetInfoContent stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                        params:params 
                                          target:self                                      
                                      selector:@selector(getArticleAContentResponse:userInfo:)  
                                          userInfo:userInfo
                                      matchCase:YES];
}

- (void)getArticleAContentResponse:(NSDictionary *)param
{
    id results = [param safeObjectForKey:@"json"];
    id userInfo = [param safeObjectForKey:@"userInfo"];
    if ([self isValidDelegateForSelector:@selector(getArticleAContentDidSucceed:userInfo:)]) {
        [self.delegate getArticleAContentDidSucceed:results userInfo:[userInfo safeObjectForKey:@"userInfo"]];
    }
}

- (void)getArticleAContentResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        [self performSelector:@selector(getArticleAContentResponse:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:response.jsonResponse, @"json", userInfo, @"userInfo", nil] afterDelay:0.5];
        NSNumber *newsID = [userInfo safeObjectForKey:@"infobID"];
        NSNumber *version = [userInfo safeObjectForKey:@"Version"];
    
        NSString *contentPath = [ArticleAContentCachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@_%@.json", newsID, version]];
        NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:response.jsonResponse, @"results", contentPath, @"contentPath", nil];
        [self performSelector:@selector(writeArticleAContentToCache:) withObject:param afterDelay:0];
    } else {
        if ([self isValidDelegateForSelector:@selector(getArticleAContentDidFailed:)]) {
            [self.delegate getArticleAContentDidFailed:response];
        }    
    }
}

- (void) writeArticleAContentToCache:(NSDictionary *)param
{
    NSError *error = nil;
    if (![[[param safeObjectForKey:@"results"] JSONRepresentation] writeToFile:[param safeObjectForKey:@"contentPath"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        TKDERROR(@"Write news content failed, error == %@", error);
    }
}

- (void)modifyFavStateInCache:(BOOL)favState articleId:(int)articleId version:(int)version
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *contentPath = [ArticleAContentCachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%d_%d.json", articleId, version]];
    BOOL isDirectory;
    NSError *error;
    id json = nil;
    if ([fileManager fileExistsAtPath:contentPath isDirectory:&isDirectory]) {
        if (!isDirectory) {
            NSDictionary *attributes = [fileManager attributesOfItemAtPath:contentPath error:&error];
            if (attributes && [attributes fileSize]) {
                NSString *content = [NSString stringWithContentsOfFile:contentPath encoding:NSUTF8StringEncoding error:&error];
                if (content) {
                    json = [content JSONValue];
                } else {
                    TKDERROR(@"error == %@", error);
                }
            } else {
                TKDERROR(@"error == %@", error);
            }
        }
    }
    if (json == nil) {
        return;
    }

    NSDictionary *article = json;
    if (favState) {
        [article setValue:@"true" forKey:@"IsFavorites"];
    } else {
        [article setValue:@"false" forKey:@"IsFavorites"];
    }

    NSString *articleStr = [article JSONRepresentation];
    if (![articleStr writeToFile:contentPath atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        TKDERROR(@"Write news content failed, error == %@", error);
    }
}

- (void)deleteArticleAContentById:(NSInteger)infobId
{
    NSString *contentPath = [ArticleAContentCachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.json", @(infobId)]];
    
    NSError *error = nil;
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager removeItemAtPath:contentPath error:&error]) {
        TKDERROR(@"delete news content failed, error == %@", error);
    }
}

//收藏管理
- (void)manageArticleAFavorites:(NSInteger)articleId opType:(NSInteger)opType
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];

    [params setSafeObject:[NSNumber numberWithInteger:articleId] forKey:@"MsgID"];
    [params setSafeObject:[NSNumber numberWithInteger:opType] forKey:@"OpType"];

    [[ZSTCommunicator shared] openAPIPostToPath:[InfoManageFavorites stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:                [NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                        params:params 
                                          target:self                                      
                                       selector:@selector(manageArticleAFavoritesResponse:userInfo:) 
                                          userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:YES];
}

- (void)manageArticleAFavoritesResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(manageArticleAFavoritesDidSucceed:)]) {
            [self.delegate manageArticleAFavoritesDidSucceed:userInfo];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(manageArticleAFavoritesDidFailed:userInfo:)]) {
            [self.delegate manageArticleAFavoritesDidFailed:response.resultCode userInfo:userInfo];
        }
    }
}

//获取收藏列表
- (void)getArticleAFavorites:(NSInteger)sinceId 
                  fetchCount:(NSInteger)fetchCount
                   orderType:(NSString *)orderType
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject: [NSString stringWithFormat:@"%@", @(fetchCount)] forKey:@"Size"];
    [params setSafeObject:[NSNumber numberWithInteger:sinceId] forKey:@"SinceID"];
    [params setSafeObject: orderType forKey:@"OrderType"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetInfoFavorites stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:                [NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]] 
                                        params:params 
                                          target:self                                      
                                        selector:@selector(getArticleAFavoritesResponse: userInfo:) 
                                          userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:YES];

}

- (void)getArticleAFavoritesResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    
    if (response.resultCode != ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(getArticleAFavoritesDidFailed:)]) {
            [self.delegate getArticleAFavoritesDidFailed:response.resultCode];
        }
        return;
    }
    
    id MyFavorites = [response.jsonResponse safeObjectForKey:@"Info"];
    if (![MyFavorites isKindOfClass:[NSArray class]]) {
        MyFavorites = [NSArray array];
    }
    
    NSDictionary *params = [userInfo safeObjectForKey:@"Params"];
    NSString *sortType = [params safeObjectForKey:@"OrderType"];
    
    if ([self isValidDelegateForSelector:@selector(getArticleAFavoritesDidSucceed:isLoadMore:hasMore:isFinish:)]) {
        [self.delegate getArticleAFavoritesDidSucceed:MyFavorites isLoadMore:[sortType isEqualToString:SortType_Asc] hasMore:[[response.jsonResponse safeObjectForKey:@"HasMore"] boolValue] isFinish:YES];
    }
}

//获取评论列表
- (void)getArticleAComment:(NSInteger)msgId
                   sinceId:(NSInteger)sinceId
                fetchCount:(NSInteger)fetchCount
                 orderType:(NSString *)orderType
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[NSNumber numberWithInteger:msgId] forKey:@"MsgID"];
    [params setSafeObject: [NSString stringWithFormat:@"%@", @(fetchCount)] forKey:@"Size"];
    [params setSafeObject:[NSNumber numberWithInteger:sinceId] forKey:@"SinceID"];
    [params setSafeObject: orderType forKey:@"OrderType"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetInfoComment stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:                [NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                        params:params 
                                          target:self                                      
                                   selector:@selector(getArticleACommentResponse: userInfo:) 
                                          userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:YES];

}

- (void)getArticleACommentResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode != ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(getArticleACommentDidFailed:)]) {
            [self.delegate getArticleACommentDidFailed:response.resultCode];
        }
        return;
    }
    
    id comments = [response.jsonResponse safeObjectForKey:@"Info"];
    if (![comments isKindOfClass:[NSArray class]]) {
        comments = [NSArray array];
    }
    
    NSDictionary *params = [userInfo safeObjectForKey:@"Params"];
    NSString *sortType = [params safeObjectForKey:@"OrderType"];
    
    if ([self isValidDelegateForSelector:@selector(getArticleACommentDidSucceed:isLoadMore:hasMore:isFinish:)]) {
        [self.delegate getArticleACommentDidSucceed:comments isLoadMore:[sortType isEqualToString:SortType_Asc] hasMore:[[response.jsonResponse safeObjectForKey:@"HasMore"] boolValue] isFinish:YES];
    }
}

//添加评论
- (void)addArticleAComment:(NSInteger)msgId
                replyTitle:(NSString *)replyTitle
                   content:(NSString *)content
                  parentId:(NSInteger)parentId
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[NSNumber numberWithInteger:msgId] forKey:@"MsgID"];
    [params setSafeObject: replyTitle forKey:@"ReplyTitle"];
    [params setSafeObject:content forKey:@"Content"];
    [params setSafeObject: [NSNumber numberWithInteger:parentId] forKey:@"ParentID"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[InfoAddComment stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:                [NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                        params:params 
                                          target:self                                      
                                   selector:@selector(addArticleACommentResponse: userInfo:) 
                                          userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:YES];

}

- (void)addArticleACommentResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(addArticleACommentDidSucceed:)]) {
            [self.delegate addArticleACommentDidSucceed:userInfo];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(addArticleACommentDidFailed:userInfo:)]) {
            [self.delegate addArticleACommentDidFailed:response.resultCode userInfo:userInfo];
        }
    }
}

- (void)getArticleA:(NSInteger)categoryID
            sinceID:(NSInteger)sinceID
          orderType:(NSString *)orderType
             Filter:(NSString *)filter
         fetchCount:(NSUInteger)fetchCount
           iconType:(ZSTIconType)iconType
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (categoryID) {
        [params setSafeObject:[NSNumber numberWithInteger:categoryID] forKey:@"CategoryID"];
    }
    if (sinceID) {
        [params setSafeObject:[NSNumber numberWithInteger:sinceID] forKey:@"PageIndex"];
    }
    [params setSafeObject:orderType forKey:@"OrderType"];
    [params setSafeObject:[NSString stringWithFormat:@"%ld", fetchCount] forKey:@"Size"];
    [params setSafeObject:filter forKey:@"Filter"];
    
    TKDINFO(@"Will post param:%@", params);
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetInfoListByPage  stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getArticleASearchResponse:userInfo:)
                                       userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:YES];
}

- (void)getArticleASearchResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode != ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(searchArticleAListDidFailed:)]) {
            [self.delegate searchArticleAListDidFailed:response.resultCode];
        }
        return;
    }
    id newsList = [response.jsonResponse safeObjectForKey:@"Info"];
    if (![newsList isKindOfClass:[NSArray class]]) {
        newsList = [NSArray array];
    }
    
    NSDictionary *params = [userInfo safeObjectForKey:@"Params"];
    NSString *sortType = [params safeObjectForKey:@"OrderType"];

    if ([self isValidDelegateForSelector:@selector(searchArticleAListDidSucceed:isLoadMore:hasMore:isFinish:)]) {
        [self.delegate searchArticleAListDidSucceed:newsList isLoadMore:[sortType isEqualToString:SortType_Asc] hasMore:[[response.jsonResponse safeObjectForKey:@"HasMore"] boolValue] isFinish:YES];
    }

}

@end
