//
//  ZSTArticleASearchViewController.m
//  ArticleA
//
//  Created by LiZhenQu on 13-6-4.
//
//

#import "ZSTArticleASearchViewController.h"
#import "ZSTUtils.h"
#import "ChineseToPinyin.h"
#import "ZSTF3Engine+ArticleA.h"
#import "ZSTF3Engine.h"
#import "ZSTArticleAVO.h"
#import "ZSTArticleAContentVO.h"
#import "ZSTArticleAContentViewController.h"
#import "ZSTArticleAMainCell.h"

@interface ZSTArticleASearchViewController ()

@end

@implementation ZSTArticleASearchViewController

@synthesize filter;
@synthesize resultTable;
@synthesize searchBar;
@synthesize resultArray;
@synthesize disableViewOverlay;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
     _pagenum = 1;
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.titleView = [ZSTUtils logoView];
    
//    UIImageView *bgImg = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_articlea_bg.png")];
//    bgImg.frame = CGRectMake(0, 0, 320, 480+(iPhone5?88:0));
//    bgImg.contentMode = UIViewContentModeScaleToFill;
//    [self.view addSubview:bgImg];
//    [bgImg release];
    
    self.searchBar = [[[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)] autorelease];
    self.searchBar.backgroundColor = [UIColor clearColor];
    self.searchBar.delegate = self;
    self.searchBar.tintColor = [UIColor whiteColor];
	self.searchBar.placeholder = NSLocalizedString(@"输入查找内容" , nil);
	self.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
	self.searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
	self.searchBar.keyboardType = UIKeyboardTypeDefault;
    [self.view addSubview:self.searchBar];
    
    self.resultTable = [[[UITableView alloc] initWithFrame:CGRectMake(0, 54, 320, 416-20-44+(iPhone5?88:0)) style:UITableViewStylePlain] autorelease];
    self.resultTable.delegate = self;
    self.resultTable.dataSource = self;
    self.resultTable.backgroundColor = [UIColor clearColor];
    self.resultTable.separatorStyle =  UITableViewCellSeparatorStyleNone;
    self.resultTable.clipsToBounds = YES;
    self.resultTable.layer.cornerRadius = 8;
    self.resultTable.layer.masksToBounds = YES;
    [self.view addSubview:self.resultTable];
    
    self.resultArray = [[NSMutableArray alloc] init];
}

- (void)displayStyle
{
    if (imageView == nil) {
        UIImage *primitiveImage = ZSTModuleImage(@"module_articlea_article_table_bg.png");
        CGFloat capWidth = primitiveImage.size.width / 2;
        CGFloat capHeight = primitiveImage.size.height / 2;
        UIImage* stretchableImage = [primitiveImage stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    
        imageView = [[[UIImageView alloc] initWithImage:stretchableImage] autorelease];
        imageView.frame = CGRectMake(15, 52, 290, 416-20-40+(iPhone5?88:0));
        imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:imageView];
    }
}

- (void)appendData:(NSArray*)dataArray
{
    [self.resultArray addObjectsFromArray:dataArray];
    [self.resultTable reloadData];
    _pagenum ++;
}

- (void) loadDataWithKeyword:(NSString *)key
{
    NSLog(@"key = %@",key);
    
    _pagenum = 1;
    
    if (self.resultArray.count != 0) {
        
        [self.resultArray removeAllObjects];
        [self.resultTable reloadData];
    }
    
     [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在搜索..." , nil)];
    [self.engine getArticleA:0 sinceID:_pagenum orderType:SortType_Desc Filter:key fetchCount:NEWS_PAGE_SIZE iconType:ZSTIconType_List];
}

- (void)loadMoreData
{
    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在加载..." , nil)];
    [self.engine getArticleA:0 sinceID:_pagenum orderType:SortType_Desc Filter:self.filter fetchCount:NEWS_PAGE_SIZE iconType:ZSTIconType_List];
}

#pragma mark -
#pragma mark ---Table Data Source Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.resultArray count] + (_hasMore ? 1:0);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == [resultArray count]) {
        static NSString *newsCellIdentifier = @"loadMoreView";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:newsCellIdentifier];
        
        if (cell==nil) {
            cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:newsCellIdentifier] autorelease];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.userInteractionEnabled = NO;
            [cell.contentView addSubview:_loadMoreView];
        }
        return cell;
    } else {
        static NSString *newsCellIdentifier = @"newsCellIdentifier";
        ZSTArticleAMainCell *cell = [tableView dequeueReusableCellWithIdentifier:newsCellIdentifier];
        
        if (cell==nil) {
            cell = [[[ZSTArticleAMainCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:newsCellIdentifier] autorelease];
            cell.accessoryType = UITableViewCellSelectionStyleNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        NSDictionary *newsDic = (NSDictionary *)[self.resultArray objectAtIndex:indexPath.row];
        ZSTArticleAVO *nvo = [ZSTArticleAVO voWithDic:newsDic];
        [cell configCell:nvo cellForRowAtIndexPath:indexPath];
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    TKLoadMoreView *loadMoreView = (TKLoadMoreView *)[cell descendantOrSelfWithClass:[TKLoadMoreView class]];
    if (loadMoreView) {
        if (!_isRefreshing) {
            [_loadMoreView loadMoreTriggerRefresh];
        }
    }
}


#pragma mark ---点击某行触发的方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        
    NSInteger row = [indexPath row];
    ZSTArticleAContentViewController *content = [[ZSTArticleAContentViewController alloc] init];
    content.newsArr = self.resultArray;
    content.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:content animated:YES];
        
    content.hTableView.contentOffset = CGPointMake(320 * row, 0);
    [content release];
}


#pragma mark ---UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 47;
}


#pragma mark -
#pragma mark UISearchBarDelegate Methods

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText && ![searchText isEqualToString:@""]) {
        
        [self loadDataWithKeyword:self.searchBar.text];
    }
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    [self searchBar:self.searchBar activate:YES];
    
    for(id cc in [self.searchBar subviews])
        
    {
        if([cc isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton *)cc;
            [btn setTitle:NSLocalizedString(@"取消",@"取消") forState:UIControlStateNormal];
            [btn setTintColor:[UIColor lightGrayColor]];
        }
    }
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.searchBar.text = @"";
    [self searchBar:self.searchBar activate:NO];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self searchBar:self.searchBar activate:NO];
    self.filter = self.searchBar.text;
    [self loadDataWithKeyword:self.filter];
}

- (void)searchBar:(UISearchBar *)searchBar activate:(BOOL) active
{
    self.resultTable.allowsSelection = !active;
    self.resultTable.scrollEnabled = !active;
    if (!active) {
        [self.disableViewOverlay removeFromSuperview];
        [self.searchBar resignFirstResponder];
    } else {
        self.disableViewOverlay.alpha = 0;
        [self.view addSubview:self.disableViewOverlay];
        
        [UIView beginAnimations:@"FadeIn" context:nil];
        [UIView setAnimationDuration:0.5];
        self.disableViewOverlay.backgroundColor = [UIColor blackColor];
        self.disableViewOverlay.alpha = 0.7;
        [UIView commitAnimations];
        
        NSIndexPath *selected = [self.resultTable indexPathForSelectedRow];
        if (selected) {
            [self.resultTable deselectRowAtIndexPath:selected animated:NO];
        }
    }
    
    [self.searchBar setShowsCancelButton:active animated:YES];
}

- (void)searchArticleAListDidSucceed:(NSArray *)resultList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish
{
    [TKUIUtil hiddenHUD];
    _hasMore = hasMore;
//    [self displayStyle];
    [self appendData:resultList];
}

- (void)searchArticleAListDidFailed:(int)response
{
    [TKUIUtil hiddenHUD];
}

#pragma mark HHLoadMoreViewDelegate Methods
- (void)loadMoreDidTriggerRefresh:(TKLoadMoreView*)view
{
    if (_hasMore && !_isLoadingMore) {
        _isLoadingMore = YES;
        [self loadMoreData];
    }
}

- (BOOL)loadMoreDataSourceIsLoading:(TKLoadMoreView*)view
{
    return _isLoadingMore;
}

#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!_isRefreshing) {
        [_loadMoreView loadMoreScrollViewDidEndDragging:scrollView];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (!_isRefreshing) {
        [_loadMoreView performSelector:@selector(loadMoreScrollViewDidEndDragging:) withObject:scrollView afterDelay:0];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (!_isRefreshing) {
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) dealloc
{
    [resultArray release];
    imageView = nil;
    self.searchBar = nil;
    self.resultTable = nil;
    
    [super dealloc];
}

@end
