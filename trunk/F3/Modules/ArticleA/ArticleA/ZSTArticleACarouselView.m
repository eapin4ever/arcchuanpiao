//
//  ZSTNewsCarouselView.m
//  F3
//
//  Created by xuhuijun on 12-7-18.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTArticleACarouselView.h"
#import "TKAsynImageView.h"
#import "ZSTGlobal+ArticleA.h"
#define maxItems 3

@implementation ZSTArticleACarouselView

@synthesize carouselDelegate = _carouselDelegate;
@synthesize carouselDataSource = _carouselDataSource;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor orangeColor];
        _scrollView = [[UIScrollView alloc] initWithFrame:frame];
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.backgroundColor = [UIColor clearColor];
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
        [self addSubview:_scrollView];
        
        UIImageView *titleView = [[[UIImageView alloc] initWithFrame:CGRectMake( 0,frame.size.height - 30, frame.size.width, 30)] autorelease];
        titleView.image = ZSTModuleImage(@"module_articlea_scroll_txt_bg.png");
        titleView.opaque = NO;
        [self addSubview:titleView];
        
        _describeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, titleView.frame.size.width-80, titleView.frame.size.height)];
        _describeLabel.text = @"";
        _describeLabel.backgroundColor = [UIColor clearColor];
        _describeLabel.font = [UIFont systemFontOfSize:15];
        _describeLabel.textColor = [UIColor whiteColor];
        [titleView addSubview:_describeLabel];
        
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_describeLabel.frame), 0, 80, titleView.frame.size.height)];
        _pageControl.numberOfPages = 0;
        _pageControl.backgroundColor = [UIColor clearColor];
        _pageControl.userInteractionEnabled = NO;
        [titleView addSubview:_pageControl];
            
        _carouselTimer = [[NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(pageTurn) userInfo:nil repeats:YES] retain];

    }
    return self;
}


- (void)reloadData
{
   NSInteger viewNum = [_carouselDataSource numberOfViewsInCarouselView:self];
    if (viewNum > maxItems) {
        viewNum = maxItems;
    }
    if (viewNum != 1) {
        _pageControl.numberOfPages = viewNum;
    }
    NSDictionary *dataDic = [self.carouselDataSource carouselView:self infoForViewAtIndex:0];
    _describeLabel.text = [dataDic safeObjectForKey:@"Title"];

    for (NSInteger i = 0; i < viewNum; i ++) {
        NSDictionary *dataDic = [self.carouselDataSource carouselView:self infoForViewAtIndex:i];

        TKAsynImageView *asynImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(i*self.frame.size.width, 0, self.frame.size.width, self.frame.size.height)] autorelease];
        asynImageView.defaultImage = [UIImage imageNamed:@"Module.bundle/module_articlea_scroll_default_img.png"];
        asynImageView.asynImageDelegate = self;
        asynImageView.adorn = ZSTModuleImage(@"module_articlea_scroll_default_img.png");
        [asynImageView clear];
        asynImageView.url = [NSURL URLWithString:[GetInfoFile stringByAppendingString:[NSString stringWithFormat:@"?FileID=%@",[dataDic safeObjectForKey:@"IConFileID"]]]];;
        [asynImageView loadImage];
        [_scrollView addSubview:asynImageView];
    }
    _scrollView.contentSize = CGSizeMake((self.frame.size.width) * viewNum, self.frame.size.height);
}

#pragma mark - TKAsynImageViewDelegate

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{
    //根据_pageControl.currentPage 打开响应的详细页
    
    if ([_carouselDelegate respondsToSelector:@selector(carouselView:infoForViewAtIndex:)]) {
        [_carouselDelegate carouselView:self didSelectedViewAtIndex:_pageControl.currentPage];
    }
}

#pragma mark - UIScrollViewDelegate


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    _pageControl.currentPage = scrollView.contentOffset.x / scrollView.bounds.size.width;//是页面控制器的按钮根据页数改变显示
    
    NSInteger whichPage = _pageControl.currentPage;
    NSDictionary *dataDic = [self.carouselDataSource carouselView:self infoForViewAtIndex: whichPage ? whichPage : 0];
    _describeLabel.text = [dataDic safeObjectForKey:@"Title"];
}

//当页数变化时，改变scrollView的内容大小
- (void) pageTurn
{
	NSInteger whichPage = _pageControl.currentPage;
    NSInteger viewNum = [_carouselDataSource numberOfViewsInCarouselView:self];
    if (viewNum > maxItems) {
        viewNum = maxItems;
    }
    
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:1.0f];
	[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    if (whichPage == (viewNum-1)) {
        _scrollView.contentOffset = CGPointMake((self.frame.size.width) * 0, 0.0f);
        _pageControl.currentPage = 0;
    }else{
        _scrollView.contentOffset = CGPointMake((self.frame.size.width) * (whichPage+1), 0.0f);
        _pageControl.currentPage = whichPage+1;
    }
	[UIView commitAnimations];
    
    NSDictionary *dataDic = [self.carouselDataSource carouselView:self infoForViewAtIndex:_pageControl.currentPage];
    _describeLabel.text = [dataDic safeObjectForKey:@"Title"];
}

- (void)carouselViewStopAnimation
{
    if ([_carouselTimer isValid]) {
        [_carouselTimer invalidate];
    }
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    UIImage *image = ZSTModuleImage(@"module_articlea_scoll_pic.png") ;
    [image drawInRect:rect];
}

- (void)dealloc
{
    [_carouselTimer invalidate];
    [_carouselTimer release];
    [_describeLabel release];
    [_scrollView release];
    [_pageControl release];
    [super dealloc];
}

- (void)selectTabAtIndex:(int)index
{
    
}

@end
