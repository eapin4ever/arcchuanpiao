//
//  ZSTArticleASearchViewController.h
//  ArticleA
//
//  Created by LiZhenQu on 13-6-4.
//
//

#import <UIKit/UIKit.h>
#import "ZSTModuleBaseViewController.h"

@interface ZSTArticleASearchViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,TKLoadMoreViewDelegate,UIScrollViewDelegate>
{
    UITableView *resultTable;
    UISearchBar *searchBar;
    UISearchDisplayController *searchDC;
    
    NSMutableArray *resultArray;
    
    TKLoadMoreView *_loadMoreView;
    BOOL _isRefreshing;
    BOOL _isLoadingMore;
    BOOL _hasMore;
    int _pagenum;
    
    UIImageView *imageView;
    
    NSString *filter;
}

@property (nonatomic, retain) UITableView *resultTable;
@property (nonatomic, retain) UISearchBar *searchBar;
@property (nonatomic, retain) NSMutableArray *resultArray;
@property (nonatomic, retain) UIView *disableViewOverlay;
@property (nonatomic, retain) NSString *filter;


@end
