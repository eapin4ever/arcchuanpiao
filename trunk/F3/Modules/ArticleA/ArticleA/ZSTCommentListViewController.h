//
//  ZSTCommentListViewController.h
//  infob
//
//  Created by xuhuijun on 12-11-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine+ArticleA.h"
#import "ZSTArticleAVO.h"

@protocol ZSTCommentCellDelegate <NSObject>

- (void)commentCellDic:(NSDictionary *)dic;

@end

@interface ZSTCommentCell : UITableViewCell

{
    TKAsynImageView *avatarImageView;
    UILabel *userName;
    NIAttributedLabel *commentContent;
    UIImageView *timeView;
    UILabel *time;
    UIButton *replayBtn;
    id<ZSTCommentCellDelegate> delegate;
    NSDictionary *cellInfo;
}

@property (nonatomic ,assign)id<ZSTCommentCellDelegate> delegate;

- (void)configCell:(NSDictionary*)cellDic cellForRowAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface ZSTArticleAEmptyTableViewCell : UITableViewCell
@property (nonatomic ,retain) UILabel *introduceLabel;

- (void)setIntroduce:(NSString *)introduce;
@end



@interface ZSTCommentListViewController : UIViewController <UITextViewDelegate,UITableViewDataSource,UITableViewDelegate,ZSTF3EngineArticleADelegate,TKLoadMoreViewDelegate,EGORefreshTableHeaderDelegate,UIScrollViewDelegate,ZSTCommentCellDelegate>

{
    EGORefreshTableHeaderView *_refreshHeaderView;
    TKLoadMoreView *_loadMoreView;
    
    BOOL _isRefreshing;
    BOOL _isLoadingMore;
    BOOL _hasMore;
    
    UIButton *replayTitleBtn;
    UILabel *replayTitleLabel;

}
@property (nonatomic, retain) TKPlaceHolderTextView *textView;
@property (nonatomic, retain) UITableView *commentList;
@property (nonatomic, retain) NSMutableArray *commentArray;
@property (nonatomic, retain) ZSTArticleAVO *infob;
@property (nonatomic, retain) NSDictionary *replayInfo;

@end



