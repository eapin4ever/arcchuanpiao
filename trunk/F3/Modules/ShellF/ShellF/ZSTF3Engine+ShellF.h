//
//  ZSTF3Engine+ShellD.h
//  ShellD
//
//  Created by xuhuijun on 13-6-19.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTF3Engine.h"


@protocol ZSTF3EngineShellDDelegate <ZSTF3EngineDelegate>

- (void)getShellFADDidSucceed:(NSArray *)ADArray;
- (void)getShellFADDidFailed:(int)resultCode;


- (void)getShellFModuleADDidSucceed:(NSArray *)ADArray;
- (void)getShellFModuleADDidFailed:(int)resultCode;

@end


@interface ZSTF3Engine (ShellF)

/**
 *	@brief	获取shell广告  type = 1 轮播图片
 *
 */
- (void)getShellFAD;

/**
 *	@brief	获取shelld 模块图片  type = 2 模块图片
 *
 */
- (void)getShellFModuleAD;

@end
