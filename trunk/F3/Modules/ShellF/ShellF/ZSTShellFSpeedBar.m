//
//  ZSTShellBSpeedBar.m
//  shellB
//
//  Created by xuhuijun on 13-1-10.
//
//

#import "ZSTShellFSpeedBar.h"
#import "ZSTUtils.h"

@implementation ZSTShellFSpeedBar

- (void)dealloc
{
    [self.speedBtn release];
    [self.titleLabel release];
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = CGRectMake(0, 0, 144, 72);
        self.speedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.speedBtn.backgroundColor = [UIColor clearColor];
        self.speedBtn.frame = CGRectMake(0, 0, 144, 72);
        [self.speedBtn addTarget:self action:@selector(selectSpeedBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.speedBtn];

        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-4, 59+8, 70, 21)];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:11];
        self.titleLabel.textColor = [UIColor colorWithRed:66/255 green:66/255 blue:66/255 alpha:1];
//        [self addSubview:self.titleLabel];
    }
    return self;
}


- (void)configSpeedBarNormalImage:(UIImage *)normalImage
                    selectedImage:(UIImage *)selectedImage
                            param:(NSDictionary *)param
{
    [self.speedBtn setBackgroundImage:normalImage forState:UIControlStateNormal];
    [self.speedBtn setBackgroundImage:selectedImage forState:UIControlStateHighlighted];
    [self.titleLabel setText:[param safeObjectForKey:@"Title"]];
    self.titleLabel.textColor = [ZSTUtils colorFromHexColor:[param safeObjectForKey:@"TitleColor"]];
    self.moduleParams = param;
}

- (void)speedBarInerSpace:(float)space
{
    CGRect titleLabelFrame = self.titleLabel.frame;
    titleLabelFrame.origin.y += space;
    self.titleLabel.frame = titleLabelFrame;
}

- (void)selectSpeedBtn:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(ZSTShellFSpeedBar:withParam:)]) {
        [self.delegate ZSTShellFSpeedBar:self withParam:self.moduleParams];
    }
}


@end
