//
//  ZSTShellBViewController.h
//  shellB
//
//  Created by xuhuijun on 13-1-11.
//
//

#import "ZSTModuleBaseViewController.h"

#import "ZSTShellBCarouselView.h"
#import "ZSTShellBSpeedBar.h"

@interface ZSTShellBViewController : ZSTModuleBaseViewController<ZSTShellBCarouselViewDataSource,ZSTShellBCarouselViewDelegate,ZSTShellBSpeedBarDelegate,UIScrollViewDelegate>

@property (strong)ZSTShellBCarouselView *carouselView;
@property (strong)NSArray *ADArray;
@property (strong,nonatomic) UIScrollView *speedBarScrollView;
@end
