//
//  ZSTShellBRootViewController.h
//  ShellB
//
//  Created by xuhuijun on 13-6-9.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTModule.h"
#import "ZSTModuleDelegate.h"
#import "ZSTDao.h"

@interface ZSTShellBRootViewController : UITabBarController <UINavigationControllerDelegate,UITabBarControllerDelegate,ZSTModuleDelegate>

@property (nonatomic, retain) UINavigationController *navigationController;

@end
