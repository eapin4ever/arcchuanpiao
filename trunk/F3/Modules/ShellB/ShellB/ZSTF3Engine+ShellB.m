//
//  ZSTF3Engine+ShellB.m
//  shellB
//
//  Created by xuhuijun on 13-1-11.
//
//

#import "ZSTF3Engine+ShellB.h"
#import "ZSTCommunicator.h"
#import "ZSTGlobal+ShellB.h"

@implementation ZSTF3Engine (ShellB)


- (void)getShellBAD
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:GetShellBAD 
                                         params:params
                                         target:self
                                       selector:@selector(getShellBADResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:YES];
}


- (void)getShellBADResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id shellBList = [response.jsonResponse objectForKey:@"info"];
        if (![shellBList isKindOfClass:[NSArray class]]) {
            shellBList = [NSArray array];
        }
        if ([self isValidDelegateForSelector:@selector(getShellBADDidSucceed:)]) {
            [self.delegate getShellBADDidSucceed:shellBList];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getShellBADDidFailed:)]) {
            [self.delegate getShellBADDidFailed:response.resultCode];
        }
    }
    
}

@end
