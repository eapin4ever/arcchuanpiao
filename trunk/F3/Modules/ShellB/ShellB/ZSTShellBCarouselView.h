//
//  ZSTNewsCarouselView.h
//  F3
//
//  Created by xuhuijun on 12-7-18.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TKAsynImageView.h"

@class ZSTShellBCarouselView;

@protocol ZSTShellBCarouselViewDataSource <NSObject>

@optional
- (NSInteger)numberOfViewsInCarouselView:(ZSTShellBCarouselView *)newsCarouselView;
- (NSDictionary *)carouselView:(ZSTShellBCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index;

@end

@protocol ZSTShellBCarouselViewDelegate <NSObject>

@optional
- (void)carouselView:(ZSTShellBCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;

@end

@interface ZSTShellBCarouselView : UIView <HJManagedImageVDelegate,UIScrollViewDelegate,TKAsynImageViewDelegate>
{
    UIScrollView *_scrollView;
    UILabel *_describeLabel;
    TKPageControl *_pageControl;
    NSTimer *_carouselTimer;
    
    NSInteger _totalPages;
    NSInteger _curPage;
    
    NSMutableArray *_curSource;
    
    id<ZSTShellBCarouselViewDataSource> _carouselDataSource;
    id<ZSTShellBCarouselViewDelegate>  _carouselDelegate;
}

@property(nonatomic, assign) id<ZSTShellBCarouselViewDataSource> carouselDataSource;
@property(nonatomic, assign) id<ZSTShellBCarouselViewDelegate> carouselDelegate;

- (void)reloadData;
- (void)stopAnimation;
- (void)startAnimation;

@end
