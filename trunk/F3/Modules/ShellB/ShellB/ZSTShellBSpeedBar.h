//
//  ZSTShellBSpeedBar.h
//  shellB
//
//  Created by xuhuijun on 13-1-10.
//
//

#import <UIKit/UIKit.h>

@class ZSTShellBSpeedBar;

@protocol ZSTShellBSpeedBarDelegate <NSObject>

- (void)zstShellBSpeedBar:(ZSTShellBSpeedBar *)speedBar withParam:(NSDictionary *)param;

@end


@interface ZSTShellBSpeedBar : UIView

@property(strong)UILabel *titleLabel;
@property(strong)UIButton *speedBtn;
@property(strong,nonatomic)NSDictionary *moduleParams;


@property(strong)id<ZSTShellBSpeedBarDelegate>delegate;

- (void)configSpeedBarNormalImage:(UIImage *)normalImage
                    selectedImage:(UIImage *)selectedImage
                            param:(NSDictionary *)param;

- (void)speedBarInerSpace:(float) space;

@end
