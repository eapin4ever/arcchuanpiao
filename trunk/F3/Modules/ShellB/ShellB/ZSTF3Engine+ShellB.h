//
//  ZSTF3Engine+ShellB.h
//  shellB
//
//  Created by xuhuijun on 13-1-11.
//
//

#import "ZSTF3Engine.h"

@protocol ZSTF3EngineShellBDelegate <ZSTF3EngineDelegate>

- (void)getShellBADDidSucceed:(NSArray *)ADArray;
- (void)getShellBADDidFailed:(int)resultCode;

@end

@interface ZSTF3Engine (ShellB)

/**
 *	@brief	获取shell广告
 *
 */
- (void)getShellBAD;

@end
