//
//  ZSTF3Engine+ArticleC.h
//  ArticleC
//
//  Created by LiZhenQu on 13-4-2.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTF3Engine.h"
#import "ZSTGlobal+ArticleC.h"

@protocol ZSTF3EngineArticleCDelegate <ZSTF3EngineDelegate>

@optional

//学员
- (void)getStudentListDidSucceed:(NSArray *)studentList hasMore:(BOOL)hasMore;
- (void)getStudentListDidFailed:(int)resultCode;

@end

@interface ZSTF3Engine (ArticleC)

/*
 *获取学员列表
 */
- (void)getStudentListWithDelegete:(id<ZSTF3EngineArticleCDelegate>)delegate
                          pageSize:(int)pageSize
                          pageIdex:(int)pageindex
                         orderType:(NSString *)orderType;

@end
