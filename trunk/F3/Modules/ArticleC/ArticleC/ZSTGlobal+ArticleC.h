//
//  ZSTGlobal+ArticleC.h
//  ArticleC
//
//  Created by LiZhenQu on 13-4-2.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import <Foundation/Foundation.h>


#define SortType_Desc @"Desc"               //向新取
#define SortType_Asc @"Asc"                 //向旧取


#define ArticleCBaseURL @"http://mod.pmit.cn/ArticleC"

#define GetMessageList ArticleCBaseURL @"/GetMessageList"
