//
//  ZSTArticleCViewController.h
//  VoiceChina
//
//  Created by LiZhenQu on 13-4-2.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTModuleBaseViewController.h"
#import "ZSTF3Engine+ArticleC.h"
#import "ZSTGlobal+ArticleC.h"

@interface ZSTArticleCViewController : ZSTModuleBaseViewController<UIScrollViewDelegate,ZSTF3EngineArticleCDelegate,TKAsynImageViewDelegate>
{
    UIScrollView *studentList;//头像滑动view
    
    UIImageView *headBgIma;//头像选中背景
    
    UITextView *studentText;//学员信息
    
    NSMutableArray *studentDataArr;
    NSMutableArray *studentImageArr;
    
    NSOperationQueue *imaQueue;
    
    BOOL nowLoading;
    UIActivityIndicatorView *waitAction;
    
    UIWebView *studentTextWeb;
    
    NSString *tempHTMLpath;
    
    UILabel *thisName;
    
    UIImageView *freshIma;
    UIImageView *moreIma;
    
    BOOL hasMore;
    
    UIButton *callbtn;
    
}

-(void)getFreshData;//获取 、刷新
-(void)getMoreData:(NSString *)_sendData;//加载更多

-(void)createStudentlist:(NSArray *)_newListArr;//学员列表（刷新、创建）
-(void)addsubStudentlist:(NSArray *)_newListArr;//学员列表(加载)

@end
