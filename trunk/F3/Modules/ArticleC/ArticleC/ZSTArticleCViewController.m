//
//  ZSTArticleCViewController.m
//  VoiceChina
//
//  Created by LiZhenQu on 13-4-2.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTArticleCViewController.h"
#import "TKAsynImageView.h"
#import "ZSTUtils.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ZSTArticleCViewController ()
{
    BOOL isRefresh;
    UIView *phoneView;
    
    UILabel *positionLabl;
    NSMutableArray *array;
}

@end

@implementation ZSTArticleCViewController

//#773d32

int pageNum;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    nowLoading = NO;
    pageNum = 1;
    
    imaQueue = [[NSOperationQueue alloc] init];
    [imaQueue setMaxConcurrentOperationCount:2];
    
    studentDataArr = [[NSMutableArray alloc] init];
    studentImageArr = [[NSMutableArray alloc] init];
    array = [[NSMutableArray alloc] init];
    tempHTMLpath = [[NSString alloc] initWithFormat:@"%@",[NSHomeDirectory()stringByAppendingPathComponent:@"Documents/tempHtml.html"]];
    
    UIImageView *listBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Module.bundle/module_articlec_leftbg.png"]];
    listBg.frame = CGRectMake(0, 0, 90,self.view.height);
    [self.view addSubview:listBg];
    [listBg release];
    
    UIImageView *textBg = [[UIImageView alloc] initWithFrame:CGRectMake(90, 0, 230, self.view.height)];
    textBg.backgroundColor = [ZSTUtils colorFromHexColor:@"#EFEFEF"];
    [self.view addSubview:textBg];
    [textBg release];
    
    studentList = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 90, self.view.height)];
    studentList.delegate = self;
    studentList.backgroundColor = [UIColor clearColor];
    studentList.showsHorizontalScrollIndicator = NO;
    studentList.showsVerticalScrollIndicator = NO;
    studentList.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:studentList];
    
    headBgIma = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Module.bundle/module_articlec_leftclick.png"]];
    headBgIma.frame = CGRectMake(0, 0, 90, 90);
    [studentList addSubview:headBgIma];
    
    thisName = [[UILabel alloc] initWithFrame:CGRectMake(102, 54-44, 210, 30)];
    //    #773d32
    thisName.font = [UIFont fontWithName:@"Helvetica-Bold" size:18.0f];
    [thisName setTextColor:[ZSTUtils colorFromHexColor:@"#486d89"]];
    thisName.backgroundColor = [UIColor clearColor];
    [self.view addSubview:thisName];
    
    
    studentTextWeb = [[UIWebView alloc] initWithFrame:CGRectMake(100, 38, 210, self.view.height-38)];
    studentTextWeb.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    studentTextWeb.opaque = NO;
    
    //滚动条和边界阴影
    for (UIView *aView in [studentTextWeb subviews])
    {
        if ([aView isKindOfClass:[UIScrollView class]])
        {
            [(UIScrollView *)aView setShowsVerticalScrollIndicator:NO];
            for (UIView *shadowView in aView.subviews)
            {
                if ([shadowView isKindOfClass:[UIImageView class]])
                {
                    shadowView.hidden = YES;
                }
            }
        }
    }
    studentTextWeb.backgroundColor = [UIColor clearColor];
    [self.view addSubview:studentTextWeb];

    freshIma = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    freshIma.image = [UIImage imageNamed:@"TKCommonLib.bundle/EGOTableViewPullRefresh/blueArrow.png"];
    freshIma.layer.transform = CATransform3DMakeRotation(3.14159265, 1.0f, 0.0f, 0.0f);
    
    
    moreIma = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 10, 20)];
    moreIma.image = [UIImage imageNamed:@"TKCommonLib.bundle/EGOTableViewPullRefresh/blueArrow.png"];
    
    phoneView = [[[UIView alloc] initWithFrame:CGRectMake(100,self.view.height-30, 225, 25)] autorelease];
    phoneView.backgroundColor = [UIColor clearColor];
    
    phoneView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:phoneView];
    
    UIImageView *phoneImg = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 20, 20)];
    phoneImg.image = [UIImage imageNamed:@"Module.bundle/module_articlec_phone.png"];
    [phoneView addSubview:phoneImg];
    [phoneImg release];
    
    UIButton *imgbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    imgbtn.frame = phoneView.frame;
    [imgbtn addTarget:self action:@selector(callAction:) forControlEvents:UIControlEventTouchUpInside];
    [phoneView addSubview:imgbtn];
    
    callbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    callbtn.frame = CGRectMake(30, phoneImg.frame.origin.y-2, 180, 25);
    [callbtn setTitleColor:[ZSTUtils colorFromHexColor:@"#666666"] forState:UIControlStateNormal];
    callbtn.titleLabel.font = [UIFont systemFontOfSize:15];
    callbtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [callbtn addTarget:self action:@selector(callAction:) forControlEvents:UIControlEventTouchUpInside];
    [phoneView addSubview:callbtn];
    
    [self getFreshData];//:SortType_Desc];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getFreshData
{
    
    pageNum = 1;
    isRefresh = YES;
    [self.engine getStudentListWithDelegete:self pageSize:10 pageIdex:pageNum orderType:SortType_Desc];
}

-(void)getMoreData:(NSString *)_sendData
{
    if (hasMore) {
        pageNum++;
        isRefresh = NO;
        [self.engine getStudentListWithDelegete:self pageSize:10 pageIdex:pageNum orderType:SortType_Desc];
    }
}

- (void)getStudentListDidSucceed:(NSArray *)studentlist hasMore:(BOOL)hasmore
{
    nowLoading = NO;
    waitAction.hidden = YES;
    hasMore = hasmore;
    
    moreIma.hidden = !hasmore;
    
    if (isRefresh) {
        [self createStudentlist:studentlist];
    }else{
        [self addsubStudentlist:studentlist];
        studentList.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    }
}

- (void)getStudentListDidFailed:(int)resultCode
{
    waitAction.hidden = YES;
    if (!isRefresh) {
        studentList.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    }
}

-(void)createStudentlist:(NSArray *)_newListArr
{
    
    if (studentList!=nil) {
        studentList.hidden = YES;
        [studentList removeFromSuperview];
        [studentList release];
        studentList = nil;
    }
    if (headBgIma!=nil) {
        headBgIma.hidden = YES;
        [headBgIma removeFromSuperview];
        [headBgIma release];
        headBgIma = nil;
    }
    
    [freshIma removeFromSuperview];
    [moreIma removeFromSuperview];
    
    studentList = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 90, self.view.height)];
    studentList.delegate = self;
    studentList.backgroundColor = [UIColor clearColor];
    studentList.showsHorizontalScrollIndicator = NO;
    studentList.showsVerticalScrollIndicator = NO;
    studentList.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:studentList];
    
    freshIma.center = CGPointMake(45, -20);
    [studentList addSubview:freshIma];
    [studentList addSubview:moreIma];
    freshIma.hidden = NO;
    
    headBgIma = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Module.bundle/module_articlec_leftclick.png"]];
    headBgIma.frame = CGRectMake(0, 0, 90, 90);
    [studentList addSubview:headBgIma];
    
    if ([studentDataArr count]>0||studentDataArr!=nil) {
        [studentDataArr removeAllObjects];
    }
    
    if ([array count]>0||array != nil) {
        [array removeAllObjects];
    }
    
    for (NSDictionary *_dic in _newListArr)
    {
        [studentDataArr addObject:_dic];
//        TKAsynImageView *_nowIma = [[TKAsynImageView alloc] initWithFrame:CGRectMake(10, ([studentDataArr count]-1)*105+10, 70, 70)];
        UIImageView *_nowIma = [[UIImageView alloc] initWithFrame:CGRectMake(10, ([studentDataArr count]-1)*105+10, 70, 70)];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(touchImageView:)];
        _nowIma.userInteractionEnabled = YES;
        [_nowIma addGestureRecognizer:tap];
//        _nowIma.asynImageDelegate = self;
        _nowIma.tag = [studentDataArr count];
        [_nowIma setImage:[UIImage imageNamed:@"student_head_loading"]];
        NSString *nowimaurl = [_dic safeObjectForKey:@"iconurl"];
        if (nowimaurl.length>0||nowimaurl!=nil)
        {
//            [_nowIma clear];
//            _nowIma.url = [NSURL URLWithString:[_dic safeObjectForKey:@"iconurl"]];
//            [_nowIma loadImage];
            [_nowIma setImageWithURL:[NSURL URLWithString:[_dic safeObjectForKey:@"iconurl"]]];
        }
        [studentList addSubview:_nowIma];
        [_nowIma release];
        
        positionLabl = [[[UILabel alloc] initWithFrame:CGRectMake(0, _nowIma.frame.origin.y + 70 + 10, 90, 18)] autorelease];
        positionLabl.backgroundColor = [UIColor clearColor];
        positionLabl.font = [UIFont systemFontOfSize:11];
        positionLabl.tag = [studentDataArr count];
        positionLabl.textColor = [ZSTUtils colorFromHexColor:@"#666666"];
        if ([studentDataArr count] == 1) {
            positionLabl.textColor = [ZSTUtils colorFromHexColor:@"#ffffff"];
        }
        positionLabl.textAlignment = NSTextAlignmentCenter;
        positionLabl.text = [_dic safeObjectForKey:@"position"];
        [studentList addSubview:positionLabl];
        [array addObject:positionLabl];
    }
    
    float height = 105*[studentDataArr count]>studentList.frame.size.height?105*[studentDataArr count]:(studentList.frame.size.height + 2);
    
    studentList.contentSize = CGSizeMake(90, height);
    moreIma.center = CGPointMake(45, studentList.contentSize.height+20);
    
    if (studentDataArr == nil || studentDataArr.count <= 0) {
        
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:nil
                              message:NSLocalizedString(@"暂无数据", nil)
                              delegate:self
                              cancelButtonTitle:NSLocalizedString(@"我知道了", nil)
                              otherButtonTitles:nil, nil];
        [alert show];
        return;
    }
    
    NSDictionary *_dic =  [studentDataArr objectAtIndex:headBgIma.frame.origin.y/90];
    NSMutableString *textmsg = [_dic safeObjectForKey:@"introductions"];
    NSString *nowlistname = [_dic safeObjectForKey:@"name"];
    thisName.text = nowlistname;
    NSString *phoneStr = [[_dic safeObjectForKey:@"phone"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ( phoneStr && phoneStr.length != 0) {
        
        CGRect rect = studentTextWeb.frame;
        rect.size.height = self.view.height - 40-38;
        studentTextWeb.frame = rect;
        
        phoneView.hidden = NO;
        [callbtn setTitle:[_dic safeObjectForKey:@"phone"] forState:UIControlStateNormal];
    } else {
        CGRect rect = studentTextWeb.frame;
        rect.size.height = self.view.height-38;
        studentTextWeb.frame = rect;
        
        phoneView.hidden = YES;
    }
    
    if (textmsg.length>0||textmsg!=nil)
    {
        [[self changehtmlstr:textmsg] writeToFile:tempHTMLpath atomically:YES encoding:NSUTF8StringEncoding error:nil];
        NSURL *url = [NSURL fileURLWithPath:tempHTMLpath];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [studentTextWeb loadRequest:request];

    }
    
}
-(void)addsubStudentlist:(NSArray *)_newListArr
{
    for (NSDictionary *_dic in _newListArr)
    {
        [studentDataArr addObject:_dic];
        
        TKAsynImageView *_nowIma = [[TKAsynImageView alloc] initWithFrame:CGRectMake(10, ([studentDataArr count]-1)*105+10, 70, 70)];
        _nowIma.asynImageDelegate = self;
        _nowIma.tag = [studentDataArr count];
        [_nowIma setImage:[UIImage imageNamed:@"student_head_loading"]];
        NSString *nowimaurl = [_dic safeObjectForKey:@"iconurl"];
        if (nowimaurl.length>0||nowimaurl!=nil)
        {
            [_nowIma clear];
            _nowIma.url = [NSURL URLWithString:[_dic safeObjectForKey:@"iconurl"]];
            [_nowIma loadImage];
        }
        [studentList addSubview:_nowIma];
        [_nowIma release];
        
        positionLabl = [[[UILabel alloc] initWithFrame:CGRectMake(0, _nowIma.frame.origin.y + 70 + 10, 90, 18)] autorelease];
        positionLabl.backgroundColor = [UIColor clearColor];
        positionLabl.font = [UIFont systemFontOfSize:11];
        positionLabl.textColor = [ZSTUtils colorFromHexColor:@"#666666"];
        positionLabl.tag = [studentDataArr count];
        positionLabl.textAlignment = NSTextAlignmentCenter;
        positionLabl.text = [_dic safeObjectForKey:@"position"];
        [studentList addSubview:positionLabl];
        [array addObject:positionLabl];
    }
    
    float height = 105*[studentDataArr count]>studentList.frame.size.height?105*[studentDataArr count]:(studentList.frame.size.height + 2);
    
    studentList.contentSize = CGSizeMake(90, height);
    
    moreIma.center = CGPointMake(45, studentList.contentSize.height+20);
}

- (void) callAction:(id)sender
{    
    if (callbtn.currentTitle==nil || [callbtn.currentTitle stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]==0) {
        
        TKAlertAppNameTitle(NSLocalizedString(@"电话号码不能空！" , nil));
        return;
    }
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", callbtn.currentTitle]]];
}

#pragma mark - TKAsynImageViewDelegate

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{
    positionLabl.textColor = [ZSTUtils colorFromHexColor:@"#666666"];
    
    positionLabl = [array objectAtIndex:0];
    positionLabl.textColor = [ZSTUtils colorFromHexColor:@"#666666"];
    NSDictionary *_dic =  [studentDataArr objectAtIndex:asynImageView.tag-1];
    NSMutableString *textmsg = [_dic safeObjectForKey:@"introductions"];
    NSString *nowlistname = [_dic safeObjectForKey:@"name"];
    thisName.text = nowlistname;
    positionLabl = [array objectAtIndex:asynImageView.tag-1];
    positionLabl.textColor = [ZSTUtils colorFromHexColor:@"#ffffff"];
    NSString *phoneStr = [[_dic safeObjectForKey:@"phone"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ( phoneStr && phoneStr.length != 0) {
        
        CGRect rect = studentTextWeb.frame;
        rect.size.height = self.view.height - 40-38;
        studentTextWeb.frame = rect;
        
        phoneView.hidden = NO;
        
        [callbtn setTitle:[_dic safeObjectForKey:@"phone"] forState:UIControlStateNormal];
    } else {
        
        CGRect rect = studentTextWeb.frame;
        rect.size.height = self.view.height-38;
        studentTextWeb.frame = rect;
        
        phoneView.hidden = YES;
    }
    if (textmsg.length>0||textmsg!=nil)
    {
        [[self changehtmlstr:textmsg] writeToFile:tempHTMLpath atomically:YES encoding:NSUTF8StringEncoding error:nil];
        NSURL *url = [NSURL fileURLWithPath:tempHTMLpath];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [studentTextWeb loadRequest:request];
    }else
    {
        studentText.text = @"";
    }
    
    [UIView beginAnimations:@"ddd" context:nil]; //设置动画
    headBgIma.center = asynImageView.center;
    [UIView commitAnimations];
}


-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    
    float nowEndY = scrollView.contentOffset.y;

    if (nowEndY<-40)
    {
        nowLoading = YES;
        waitAction.hidden = NO;
        freshIma.hidden = YES;
        
        scrollView.contentInset = UIEdgeInsetsMake(40.0f, 0.0f, 0.0f, 0.0f);
        [waitAction removeFromSuperview];
        waitAction.center = CGPointMake(45, -20);
        
        [scrollView addSubview:waitAction];
        
        [self getFreshData];
    }else if(nowEndY+scrollView.frame.size.height > 105*[studentDataArr count]+40)
    {
        nowLoading = YES;
        waitAction.hidden = NO;        
        scrollView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 40.0f, 0.0f);
        
        waitAction.center = CGPointMake(45, scrollView.frame.size.height+20);
        [scrollView addSubview:waitAction];
        
        [self getMoreData:nil];
    }
    
}

-(NSString *)changehtmlstr:(NSString *)nowData
{
    
    NSMutableString *nowstr00 = [[NSMutableString alloc] initWithFormat:@"%@",nowData];
    [nowstr00 replaceOccurrencesOfString:@"&amp;" withString:@"&" options:NSLiteralSearch  range:NSMakeRange(0,[nowData length])];
    
    NSMutableString *nowstr01 = [[NSMutableString alloc] initWithFormat:@"%@",nowstr00];
    [nowstr01 replaceOccurrencesOfString:@"&lt;" withString:@"<" options:NSLiteralSearch  range:NSMakeRange(0,[nowstr00 length])];
    [nowstr00 release];
    
    NSMutableString *nowstr02 = [[NSMutableString alloc] initWithFormat:@"%@",nowstr01];
    [nowstr02 replaceOccurrencesOfString:@"&gt;" withString:@">" options:NSLiteralSearch  range:NSMakeRange(0,[nowstr01 length])];
    [nowstr01 release];
    
    NSMutableString *nowstr03 = [[NSMutableString alloc] initWithFormat:@"%@",nowstr02];
    [nowstr03 replaceOccurrencesOfString:@"&quot;" withString:@"\"" options:NSLiteralSearch  range:NSMakeRange(0,[nowstr02 length])];
    [nowstr02 release];
    
    NSMutableString *nowstr04 = [[NSMutableString alloc] initWithFormat:@"%@",nowstr03];
    [nowstr04 replaceOccurrencesOfString:@"&nbsp;" withString:@" " options:NSLiteralSearch  range:NSMakeRange(0,[nowstr03 length])];
    [nowstr03 release];
    
    NSMutableString *nowstr05 = [[NSMutableString alloc] initWithFormat:@"%@",nowstr04];
    [nowstr05 replaceOccurrencesOfString:@"&copy;" withString:@"©" options:NSLiteralSearch  range:NSMakeRange(0,[nowstr04 length])];
    [nowstr04 release];
    
    NSMutableString *nowstr06 = [[[NSMutableString alloc] initWithFormat:@"%@",nowstr05] autorelease];
    [nowstr06 replaceOccurrencesOfString:@"&reg" withString:@"®" options:NSLiteralSearch  range:NSMakeRange(0,[nowstr05 length])];
    [nowstr05 release];
    
    return nowstr06;
    
    
}

- (void)touchImageView:(UITapGestureRecognizer *)tap
{
    UIImageView *asynImageView = (UIImageView *)tap.view;
    positionLabl.textColor = [ZSTUtils colorFromHexColor:@"#666666"];
    
    positionLabl = [array objectAtIndex:0];
    positionLabl.textColor = [ZSTUtils colorFromHexColor:@"#666666"];
    NSDictionary *_dic =  [studentDataArr objectAtIndex:asynImageView.tag-1];
    NSMutableString *textmsg = [_dic safeObjectForKey:@"introductions"];
    NSString *nowlistname = [_dic safeObjectForKey:@"name"];
    thisName.text = nowlistname;
    positionLabl = [array objectAtIndex:asynImageView.tag-1];
    positionLabl.textColor = [ZSTUtils colorFromHexColor:@"#ffffff"];
    NSString *phoneStr = [[_dic safeObjectForKey:@"phone"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ( phoneStr && phoneStr.length != 0) {
        
        CGRect rect = studentTextWeb.frame;
        rect.size.height = self.view.height - 40-38;
        studentTextWeb.frame = rect;
        
        phoneView.hidden = NO;
        
        [callbtn setTitle:[_dic safeObjectForKey:@"phone"] forState:UIControlStateNormal];
    } else {
        
        CGRect rect = studentTextWeb.frame;
        rect.size.height = self.view.height-38;
        studentTextWeb.frame = rect;
        
        phoneView.hidden = YES;
    }
    if (textmsg.length>0||textmsg!=nil)
    {
        [[self changehtmlstr:textmsg] writeToFile:tempHTMLpath atomically:YES encoding:NSUTF8StringEncoding error:nil];
        NSURL *url = [NSURL fileURLWithPath:tempHTMLpath];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [studentTextWeb loadRequest:request];
    }else
    {
        studentText.text = @"";
    }
    
    [UIView beginAnimations:@"ddd" context:nil]; //设置动画
    headBgIma.center = asynImageView.center;
    [UIView commitAnimations];
}

-(void)dealloc
{
    [array release];
    [studentList release];
    [headBgIma release];
    [studentDataArr release];
    [waitAction release];
    
    [studentTextWeb release];
    
    [tempHTMLpath release];
    
    [thisName release];
    
    [freshIma release];
    [moreIma release];
    
    [super dealloc];
}

@end
