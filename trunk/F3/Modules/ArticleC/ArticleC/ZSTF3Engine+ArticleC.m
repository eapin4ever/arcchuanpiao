//
//  ZSTF3Engine+ArticleC.m
//  ArticleC
//
//  Created by LiZhenQu on 13-4-2.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTF3Engine+ArticleC.h"
#import "ZSTCommunicator.h"
#import "ZSTF3Preferences.h"
#import "ZSTLegacyResponse.h"
#import "ZSTSqlManager.h"
#import "JSON.h"
//#import "ZSTVideoBVO.h"
//#import "ZSTDao+VideoB.h"


@implementation ZSTF3Engine (ArticleC)

- (void)getStudentListWithDelegete:(id<ZSTF3EngineArticleCDelegate>)delegate
                          pageSize:(int)pageSize
                          pageIdex:(int)pageindex
                         orderType:(NSString *)orderType
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[NSNumber numberWithInt:pageSize] forKey:@"size"];
    [params setObject:[NSNumber numberWithInt:pageindex] forKey:@"pageindex"];
    [params setObject:orderType forKey:@"ordertype"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetMessageList stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getArticleResponse:userInfo:)
                                       userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:NO];
}

- (void)getArticleResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        
        BOOL hasmore = [[response.jsonResponse safeObjectForKey:@"hasmore"] boolValue];
        bool result = [[response.jsonResponse safeObjectForKey:@"result"] boolValue];
        if (result){
            id articleList = [response.jsonResponse safeObjectForKey:@"info"];
            if (![articleList isKindOfClass:[NSArray class]]) {
                articleList = [NSArray array];
            }
            
            if ([self isValidDelegateForSelector:@selector(getStudentListDidSucceed: hasMore:)]) {
                
                [self.delegate getStudentListDidSucceed:articleList hasMore:hasmore];
            }
        }
        
    } else {
        if ([self isValidDelegateForSelector:@selector(getStudentListDidFailed:)]) {
            [self.delegate getStudentListDidFailed:response.resultCode];
        }
    }
}

@end
