//
//  ZSTShellPViewController.m
//  ShellP
//
//  Created by LiZhenQu on 14-10-20.
//  Copyright (c) 2014年 xuhuijun. All rights reserved.
//

#import "ZSTShellPViewController.h"
#import "ZSTF3ClientAppDelegate.h"
#import "ZSTF3Engine+ShellP.h"
#import "ElementParser.h"
#import "ZSTModuleManager.h"
#import "ZSTUtils.h"
#import "ZSTWebViewController.h"
#import "ZSTMineViewController.h"

@interface ZSTShellPViewController ()
{
    NSInteger random;
    NSTimer *timer;
    
    NSInteger num;
}

@end

@implementation ZSTShellPViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    num = 0;
    
    self.speedBarScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, (480 - 20 - 44 - 49)+(iPhone5?88:0))];
    self.speedBarScrollView.showsHorizontalScrollIndicator = NO;
    self.speedBarScrollView.showsVerticalScrollIndicator = NO;
//
    self.speedBarScrollView.backgroundColor = RGBCOLOR(240, 240, 243);
    [self.view addSubview:self.speedBarScrollView];
    
    self.carouselView = [[ZSTShellPCarouselView alloc] initWithFrame:CGRectMake(0, 0, 320, 160)];
    self.carouselView.carouselDataSource = self;
    self.carouselView.carouselDelegate = self;
    [self.speedBarScrollView addSubview:self.carouselView];
    
    UIImageView *backgroundImg3 = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_shellp_mainView_background.png")];
    backgroundImg3.frame = CGRectMake(0, CGRectGetMaxY(self.carouselView.frame)-1, 320, 14);
    backgroundImg3.contentMode = UIViewContentModeScaleToFill;
//    [self.speedBarScrollView addSubview:backgroundImg3];
    [backgroundImg3 release];
    
    [self imageButtonElementsForView];
    
    [self.engine getShellPAD];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
     self.navigationItem.rightBarButtonItem = [self initWithSubviews];
    
    timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(startAnimation) userInfo:nil repeats:YES];
    
    [timer fire];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if ([timer isValid]) {
        
        [timer invalidate];
    }
    
    [self stopAnimation];
}

- (UIBarButtonItem *) initWithSubviews
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(mineAction:) forControlEvents:UIControlEventTouchUpInside];
    
    btn.layer.cornerRadius = btn.frame.size.width / 2.0;
    btn.layer.borderColor = [UIColor clearColor].CGColor;
    btn.layer.masksToBounds = YES;
    
    NSString *path = nil;
    UIImage *image = nil;
    NSDictionary *dic = [ZSTF3Preferences shared].avaterName;
    
    if (![ZSTF3Preferences shared].loginMsisdn || [ZSTF3Preferences shared].loginMsisdn.length == 0) {
        
        [ZSTF3Preferences shared].loginMsisdn = @"";
    }
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0) {
        
        image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
    } else {
        
        if (dic || [dic isKindOfClass:[NSDictionary class]]) {
            path = [dic valueForKey:[ZSTF3Preferences shared].loginMsisdn];
        }
        
        if (path && path.length > 0) {
            
            NSString *homePath = NSHomeDirectory();
            NSString *imgTempPath = [path substringFromIndex:[path rangeOfString:@"tmp"].location];
            NSString *finalPath = [NSString stringWithFormat:@"%@/%@",homePath,imgTempPath];
            NSData *reader = [NSData dataWithContentsOfFile:finalPath];
            image = [UIImage imageWithData:reader];
            
            if (!image) {
                
                image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
            }
            
        } else {
            
            image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
        }
    }
    
    [btn setImage:image forState:UIControlStateNormal];
    
    //暂时解决btn图片不能显示的问题
    UIImageView *iv = [[UIImageView alloc] initWithFrame:btn.imageView.frame];
    iv.image = image;
    [btn insertSubview:iv aboveSubview:btn.imageView];
    
    return [[[UIBarButtonItem alloc] initWithCustomView:btn] autorelease];
}


- (void)mineAction:(id)sender
{
    ZSTMineViewController *controller = [[ZSTMineViewController alloc] initWithNibName:@"ZSTMineViewController" bundle:nil];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

- (void) startAnimation
{
     [self stopAnimation];
//    if (num >= 3) {
//        
//        num = 0;
//        
//        if ([timer isValid]) {
//            
//            [timer invalidate];
//        }
//        return;
//    }
   
    NSArray *itemElements = [self getImageBtnArray];
    random = [self getRandomNumber:0 to:(itemElements.count-1)];
    if (random == 8 || random == (itemElements.count - 1)) {
        
        random --;
    }
    
    if (random == 0) {
        
        random = 1;
    }
    
    UIView *view = [self.speedBarScrollView viewWithTag:random];
    
    if ([view isKindOfClass:[ZSTShellPSpeedBar class]]) {
        
        [(ZSTShellPSpeedBar *)view startAnimation];
    }
    
    if ([view isKindOfClass:[ZSTShellPImageBtn class]]) {
        
        [(ZSTShellPImageBtn *)view startAnimation];
    }
    
    num ++;
}

- (void) stopAnimation
{
    UIView *view = [self.speedBarScrollView viewWithTag:random];
    
    if ([view isKindOfClass:[ZSTShellPSpeedBar class]]) {
        
        [(ZSTShellPSpeedBar *)view stopAnimation];
    }
    
    if ([view isKindOfClass:[ZSTShellPImageBtn class]]) {
        
        [(ZSTShellPImageBtn *)view stopAnimation];
    }
}

-(NSInteger)getRandomNumber:(NSInteger)from to:(NSInteger)to
{
    return (NSInteger)(from + (arc4random() % (to - from + 1)));
}

- (NSArray *) getImageBtnArray
{
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module_shellp_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSArray *itemElements = [root selectElements:@"ImageButton Item"];
    
    return itemElements;
}

- (void)imageButtonElementsForView
{
    NSArray *itemElements = [self getImageBtnArray];
    
    NSUInteger count = itemElements.count;
    
    BOOL more = NO;
    
    if (itemElements.count >= 8) {
        
        count = 8;
        
        more = YES;
    }
    
    for (int i = 0; i < count; i++) {
       
        Element *itemElement = [[itemElements objectAtIndex:i] retain];
        
        NSString *btnName = [[itemElement selectElement:@"Title"] contentsText];
        int moduleID = [[[itemElement selectElement:@"ModuleId"] contentsNumber] intValue];
        NSNumber *moduleType = [[itemElement selectElement:@"ModuleType"] contentsNumber];
        NSString *TitleColor = [[itemElement selectElement:@"TitleColor"] contentsText];
        NSString *backColor = [[itemElement selectElement:@"BackgroundColor"] contentsText];
        if (TitleColor == nil || [TitleColor length] == 0 || [TitleColor isEqualToString:@""]) {
            TitleColor = @"#000000";
        }
        
        if (backColor == nil || [backColor length] == 0 || [backColor isEqualToString:@""]) {
            backColor = @"#FFFFFF";
        }
        
        NSDictionary *paramDic = [NSDictionary dictionary];
        if (moduleID == 12 || moduleID == 24) {
            NSString *interfaceUrl = [[itemElement selectElement:@"InterfaceUrl"] contentsText];
            paramDic = [NSDictionary dictionaryWithObjectsAndKeys:btnName,@"Title",moduleType,@"type",interfaceUrl,@"InterfaceUrl",TitleColor,@"TitleColor",backColor,@"BackgroundColor", nil];
        }else{
            paramDic = [NSDictionary dictionaryWithObjectsAndKeys:btnName,@"Title",moduleType,@"type",TitleColor,@"TitleColor", backColor,@"BackgroundColor",nil];
        }

        
        ZSTShellPSpeedBar *bar = [[ZSTShellPSpeedBar alloc] initWithFrame:CGRectMake(79*(i%4)+2*(i%4),79*(i/4)+2*(i/4)+2+160, 79, 79)];
        bar.tag = i;
        bar.speedBtn.tag = moduleID;
        bar.delegate = self;
        NSString *normalImagePath = [NSString stringWithFormat:@"module_shellp_icon%d_n.png", i+1];
        NSString *selectedImagePath = [NSString stringWithFormat:@"module_shellp_icon%d_p.png", i+1];
        
        [bar configSpeedBarNormalImage:ZSTModuleImage(normalImagePath)
                         selectedImage:ZSTModuleImage(selectedImagePath)
                                 param:paramDic];
        
        [self.speedBarScrollView addSubview:bar];
         self.speedBarScrollView.contentSize = CGSizeMake(self.view.frame.size.width, CGRectGetMaxY(bar.frame) + 8);
        [bar release];
    }
    
    if (more) {

        for (int i = 8; i < itemElements.count; i++) {
            Element *itemElement = [[itemElements objectAtIndex:i] retain];
            
            NSString *btnName = [[itemElement selectElement:@"Title"] contentsText];
            int moduleID = [[[itemElement selectElement:@"ModuleId"] contentsNumber] intValue];
            NSNumber *moduleType = [[itemElement selectElement:@"ModuleType"] contentsNumber];
            NSString *TitleColor = [[itemElement selectElement:@"TitleColor"] contentsText];
            NSString *backColor = [[itemElement selectElement:@"BackgroundColor"] contentsText];
            if (TitleColor == nil || [TitleColor length] == 0 || [TitleColor isEqualToString:@""]) {
                TitleColor = @"#000000";
            }
            
            if (backColor == nil || [backColor length] == 0 || [backColor isEqualToString:@""]) {
                backColor = @"#FFFFFF";
            }
            
            NSDictionary *paramDic = [NSDictionary dictionary];
            if (moduleID == 12 || moduleID == 24) {
                NSString *interfaceUrl = [[itemElement selectElement:@"InterfaceUrl"] contentsText];
                paramDic = [NSDictionary dictionaryWithObjectsAndKeys:btnName,@"Title",moduleType,@"type",interfaceUrl,@"InterfaceUrl",TitleColor,@"TitleColor",backColor,@"BackgroundColor", nil];
            }else{
                paramDic = [NSDictionary dictionaryWithObjectsAndKeys:btnName,@"Title",moduleType,@"type",TitleColor,@"TitleColor", backColor,@"BackgroundColor",nil];
            }
            
            ZSTShellPImageBtn *bar = [[ZSTShellPImageBtn alloc] initWithFrame:CGRectMake(150*((i-8)%2)+6*((i-8)%2)+8,168+75*((i-8)/2)+6*((i-8)/2)+160, 150, 75)];
            bar.tag = i;
            bar.speedBtn.tag = moduleID;
            
            bar.delegate = self;
            NSString *normalImagePath = [NSString stringWithFormat:@"module_shellp_icon%d_n.png", i+1];
            NSString *selectedImagePath = [NSString stringWithFormat:@"module_shellp_icon%d_p.png", i+1];
            [bar configImageBtnNormalImage:ZSTModuleImage(normalImagePath) selectedImage:ZSTModuleImage(selectedImagePath) param:paramDic];
            [self.speedBarScrollView addSubview:bar];
            self.speedBarScrollView.contentSize = CGSizeMake(self.view.frame.size.width, CGRectGetMaxY(bar.frame) + 8);
            [bar release];
        }
    }
}

#pragma mark ZSTShellBSpeedBarDelegate

- (void)zstShellPSpeedBar:(ZSTShellPSpeedBar *)speedBar withParam:(NSDictionary *)param
{
    NSMutableDictionary *moduleParams = [NSMutableDictionary dictionaryWithDictionary:param];
    
    ZSTModule *module = [[ZSTModuleManager shared] findModule:speedBar.speedBtn.tag];
    if (module) {
        UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:speedBar.speedBtn.tag withOptions:moduleParams];
        if (controller) {
            controller.hidesBottomBarWhenPushed = YES;
            controller.navigationItem.titleView = [ZSTUtils titleViewWithTitle:speedBar.titleLabel.text];
            controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

- (void)ZSTShellPImageBtn:(ZSTShellPImageBtn *)speedBar withParam:(NSDictionary *)param
{
    NSMutableDictionary *moduleParams = [NSMutableDictionary dictionaryWithDictionary:param];
    
    ZSTModule *module = [[ZSTModuleManager shared] findModule:speedBar.speedBtn.tag];
    if (module) {
        UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:speedBar.speedBtn.tag withOptions:moduleParams];
        if (controller) {
            controller.hidesBottomBarWhenPushed = YES;
            controller.navigationItem.titleView = [ZSTUtils titleViewWithTitle:speedBar.titleLabel.text];
            controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

#pragma mark ZSTF3EngineShellBDelegate

- (void)getShellPADDidSucceed:(NSArray *)ADArray
{
    self.ADArray = [ADArray retain];
    [self.carouselView reloadData];
}
- (void)getShellPADDidFailed:(int)resultCode
{
    [TKUIUtil alertInView:self.view withTitle:@"广告暂无数据！" withImage:nil];
}


#pragma mark - ZSTShellBCarouselViewDataSource

- (NSInteger)numberOfViewsInCarouselView:(ZSTShellPCarouselView *)newsCarouselView;
{
    return [self.ADArray count];
}

- (NSDictionary *)carouselView:(ZSTShellPCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index
{
    if ([self.ADArray  count] != 0) {
        return [self.ADArray  objectAtIndex:index];
    }
    return nil;
}

#pragma mark - ZSTShellBCarouselViewDelegate

- (void)carouselView:(ZSTShellPCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;
{
    NSString *urlPath = [[self.ADArray objectAtIndex:index] safeObjectForKey:@"ad_linkurl"];
    if (urlPath == nil || [urlPath length] == 0 || [urlPath isEmptyOrWhitespace]) {
        return;
    }
    
    ZSTWebViewController *webViewController = [[ZSTWebViewController alloc] init];
    [webViewController setURL:urlPath];
    webViewController.type = self.moduleType;
    webViewController.hidesBottomBarWhenPushed = YES;
    webViewController.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
    [self.navigationController pushViewController:webViewController animated:NO];
    [webViewController release];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [self.carouselView release];
    [self.speedBarScrollView release];
    [super dealloc];
}

@end
