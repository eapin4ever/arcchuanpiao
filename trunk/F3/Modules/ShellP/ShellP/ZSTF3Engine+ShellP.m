//
//  ZSTF3Engine+ShellP.m
//  ShellP
//
//  Created by LiZhenQu on 14-10-20.
//  Copyright (c) 2014年 xuhuijun. All rights reserved.
//

#import "ZSTF3Engine+ShellP.h"
#import "ZSTCommunicator.h"
#import "ZSTGlobal+ShellP.h"

@implementation ZSTF3Engine (ShellP)


- (void)getShellPAD
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    [params setSafeObject:[NSString stringWithFormat:@"%ld",(long)[ZSTF3Preferences shared].shellId] forKey:@"moduleid"];
    
    
    [[ZSTCommunicator shared] openAPIPostToPath:GetShellPAD
                                         params:params
                                         target:self
                                       selector:@selector(getShellPADResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:YES];
}


- (void)getShellPADResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id shellBList = [response.jsonResponse objectForKey:@"info"];
        if (![shellBList isKindOfClass:[NSArray class]]) {
            shellBList = [NSArray array];
        }
        if ([self isValidDelegateForSelector:@selector(getShellPADDidSucceed:)]) {
            [self.delegate getShellPADDidSucceed:shellBList];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getShellPADDidFailed:)]) {
            [self.delegate getShellPADDidFailed:response.resultCode];
        }
    }
    
}

@end

