//
//  ZSTShellBSpeedBar.h
//  shellB
//
//  Created by xuhuijun on 13-1-10.
//
//

#import <UIKit/UIKit.h>

@class ZSTShellPSpeedBar;

@protocol ZSTShellPSpeedBarDelegate <NSObject>

- (void)zstShellPSpeedBar:(ZSTShellPSpeedBar *)speedBar withParam:(NSDictionary *)param;

@end


@interface ZSTShellPSpeedBar : UIView

@property (retain, nonatomic) UILabel *titleLabel;
@property (retain, nonatomic) UIButton *speedBtn;
@property (retain, nonatomic) NSDictionary *moduleParams;
@property (retain, nonatomic) UIButton *imagebtn;

@property(strong)id<ZSTShellPSpeedBarDelegate>delegate;

- (void)configSpeedBarNormalImage:(UIImage *)normalImage
                    selectedImage:(UIImage *)selectedImage
                            param:(NSDictionary *)param;

- (void)speedBarInerSpace:(float) space;

- (void) startAnimation;

- (void) stopAnimation;

@end
