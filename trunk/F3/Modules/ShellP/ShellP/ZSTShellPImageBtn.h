//
//  ZSTShellPImageBtn.h
//  ShellP
//
//  Created by LiZhenQu on 14-10-21.
//  Copyright (c) 2014年 xuhuijun. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZSTShellPImageBtn;

@protocol ZSTShellPImageBtnDelegate <NSObject>

- (void)ZSTShellPImageBtn:(ZSTShellPImageBtn *)speedBar withParam:(NSDictionary *)param;

@end

@interface ZSTShellPImageBtn : UIView

@property (retain, nonatomic) NSDictionary *moduleParams;
@property (retain, nonatomic) UIButton *speedBtn;
@property (retain, nonatomic) UILabel *titleLabel;

@property(strong)id<ZSTShellPImageBtnDelegate>delegate;

- (void)configImageBtnNormalImage:(UIImage *)normalImage
                    selectedImage:(UIImage *)selectedImage
                            param:(NSDictionary *)param;

- (void) startAnimation;

- (void) stopAnimation;

@end
