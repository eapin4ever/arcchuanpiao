//
//  ZSTShellPImageBtn.m
//  ShellP
//
//  Created by LiZhenQu on 14-10-21.
//  Copyright (c) 2014年 xuhuijun. All rights reserved.
//

#import "ZSTShellPImageBtn.h"
#import "ZSTUtils.h"

@implementation ZSTShellPImageBtn

- (void)dealloc
{
    [self.speedBtn release];

    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor clearColor];;
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(4, 55, 70, 18)];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:11];
        self.titleLabel.textColor = [UIColor colorWithRed:66/255 green:66/255 blue:66/255 alpha:1];
        
        self.speedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.speedBtn.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        [self.speedBtn addTarget:self action:@selector(selectSpeedBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.speedBtn];
        
        }
    return self;
}

- (void) startAnimation
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    rotationAnimation.duration = 2;
    rotationAnimation.repeatCount = 999999;//你可以设置到最大的整数值
    rotationAnimation.cumulative = NO;
    rotationAnimation.removedOnCompletion = NO;
    rotationAnimation.fillMode = kCAFillModeForwards;
    [self.speedBtn.layer addAnimation:rotationAnimation forKey:@"Rotation"];
}

- (void) stopAnimation
{
    [self.speedBtn.layer removeAllAnimations];
}

- (void)configImageBtnNormalImage:(UIImage *)normalImage
                    selectedImage:(UIImage *)selectedImage
                            param:(NSDictionary *)param
{
    [self.speedBtn setBackgroundImage:normalImage forState:UIControlStateNormal];
    [self.speedBtn setBackgroundImage:selectedImage forState:UIControlStateHighlighted];
    [self.titleLabel setText:[param safeObjectForKey:@"Title"]];
    self.backgroundColor = [ZSTUtils colorFromHexColor:[param safeObjectForKey:@"BackgroundColor"]];
    self.moduleParams = param;
}

- (void)selectSpeedBtn:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(ZSTShellPImageBtn:withParam:)]) {
        [self.delegate ZSTShellPImageBtn:self withParam:self.moduleParams];
    }
}

@end
