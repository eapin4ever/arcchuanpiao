//
//  ZSTF3Engine+ShellP.h
//  ShellP
//
//  Created by LiZhenQu on 14-10-20.
//  Copyright (c) 2014年 xuhuijun. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ZSTF3EngineShellPDelegate <ZSTF3EngineDelegate>

- (void)getShellPADDidSucceed:(NSArray *)ADArray;
- (void)getShellPADDidFailed:(int)resultCode;

@end

@interface ZSTF3Engine (ShellP)

/**
 *	@brief	获取shell广告
 *
 */
- (void)getShellPAD;

@end

