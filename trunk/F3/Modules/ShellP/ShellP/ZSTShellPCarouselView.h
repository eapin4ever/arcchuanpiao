//
//  ZSTNewsCarouselView.h
//  F3
//
//  Created by xuhuijun on 12-7-18.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TKAsynImageView.h"

@class ZSTShellPCarouselView;

@protocol ZSTShellPCarouselViewDataSource <NSObject>

@optional
- (NSInteger)numberOfViewsInCarouselView:(ZSTShellPCarouselView *)newsCarouselView;
- (NSDictionary *)carouselView:(ZSTShellPCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index;

@end

@protocol ZSTShellPCarouselViewDelegate <NSObject>

@optional
- (void)carouselView:(ZSTShellPCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;

@end

@interface ZSTShellPCarouselView : UIView <HJManagedImageVDelegate,UIScrollViewDelegate,TKAsynImageViewDelegate>
{
    UIScrollView *_scrollView;
    UILabel *_describeLabel;
    TKPageControl *_pageControl;
    NSTimer *_carouselTimer;
    
    NSInteger _totalPages;
    NSInteger _curPage;
    
    NSMutableArray *_curSource;
    
    id<ZSTShellPCarouselViewDataSource> _carouselDataSource;
    id<ZSTShellPCarouselViewDelegate>  _carouselDelegate;
}

@property(nonatomic, assign) id<ZSTShellPCarouselViewDataSource> carouselDataSource;
@property(nonatomic, assign) id<ZSTShellPCarouselViewDelegate> carouselDelegate;

- (void)reloadData;
- (void)stopAnimation;
- (void)startAnimation;

@end
