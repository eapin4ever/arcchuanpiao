//
//  ZSTShellPViewController.h
//  ShellP
//
//  Created by LiZhenQu on 14-10-20.
//  Copyright (c) 2014年 xuhuijun. All rights reserved.
//

#import "ZSTModuleBaseViewController.h"
#import "ZSTShellPCarouselView.h"
#import "ZSTShellPSpeedBar.h"
#import "ZSTShellPImageBtn.h"

@interface ZSTShellPViewController : ZSTModuleBaseViewController<ZSTShellPCarouselViewDataSource,ZSTShellPCarouselViewDelegate,ZSTShellPSpeedBarDelegate,ZSTShellPImageBtnDelegate>

@property (strong)ZSTShellPCarouselView *carouselView;
@property (strong)NSArray *ADArray;
@property (strong,nonatomic) UIScrollView *speedBarScrollView;

@end
