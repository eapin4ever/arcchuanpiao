//
//  ZSTShellPRootViewController.h
//  ShellP
//
//  Created by LiZhenQu on 14-10-20.
//  Copyright (c) 2014年 xuhuijun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTModule.h"
#import "ZSTModuleDelegate.h"
#import "ZSTDao.h"

@interface ZSTShellPRootViewController : UITabBarController <UINavigationControllerDelegate,UITabBarControllerDelegate,ZSTModuleDelegate>

@property (nonatomic, retain) UINavigationController *navigationController;

@end
