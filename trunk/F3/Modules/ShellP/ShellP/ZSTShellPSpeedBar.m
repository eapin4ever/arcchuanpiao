//
//  ZSTShellBSpeedBar.m
//  shellB
//
//  Created by xuhuijun on 13-1-10.
//
//

#import "ZSTShellPSpeedBar.h"
#import "ZSTUtils.h"

@implementation ZSTShellPSpeedBar

- (void)dealloc
{
    [self.speedBtn release];
    [self.titleLabel release];
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.imagebtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.imagebtn.frame = CGRectMake(24, 16, 32, 32);
        [self addSubview:self.imagebtn];
        
        self.speedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.speedBtn.frame = CGRectMake(0, 0, 79, 79);
        [self.speedBtn addTarget:self action:@selector(selectSpeedBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.speedBtn];

        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(4, 53, 70, 18)];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:11];
        self.titleLabel.textColor = [UIColor colorWithRed:66/255 green:66/255 blue:66/255 alpha:1];
        [self addSubview:self.titleLabel];
    }
    return self;
}

- (void) startAnimation
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    rotationAnimation.duration = 2;
    rotationAnimation.repeatCount = 999999;//你可以设置到最大的整数值
    rotationAnimation.cumulative = NO;
    rotationAnimation.removedOnCompletion = NO;
    rotationAnimation.fillMode = kCAFillModeForwards;
    [self.imagebtn.layer addAnimation:rotationAnimation forKey:@"Rotation"];
}

- (void) stopAnimation
{
    [self.imagebtn.layer removeAllAnimations];
}

- (void)configSpeedBarNormalImage:(UIImage *)normalImage
                    selectedImage:(UIImage *)selectedImage
                            param:(NSDictionary *)param
{
    [self.imagebtn setBackgroundImage:normalImage forState:UIControlStateNormal];
    [self.imagebtn setBackgroundImage:selectedImage forState:UIControlStateHighlighted];
    [self.titleLabel setText:[param safeObjectForKey:@"Title"]];
    self.titleLabel.textColor = [ZSTUtils colorFromHexColor:[param safeObjectForKey:@"TitleColor"]];
    self.backgroundColor = [ZSTUtils colorFromHexColor:[param safeObjectForKey:@"BackgroundColor"]];
    self.moduleParams = param;
}

- (void)speedBarInerSpace:(float)space
{
    CGRect titleLabelFrame = self.titleLabel.frame;
    titleLabelFrame.origin.y += space;
    self.titleLabel.frame = titleLabelFrame;
}

- (void)selectSpeedBtn:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(zstShellPSpeedBar:withParam:)]) {
        [self.delegate zstShellPSpeedBar:self withParam:self.moduleParams];
    }
}


@end
