//
//  NewsContentVO.h
//  News
//
//  Created by admin on 12-7-25.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTArticleDContentVO : NSObject{
    NSDictionary *dict;
}

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *summary;
@property (nonatomic, retain) NSString *keyword;
@property (nonatomic, retain) NSString *source;
@property (nonatomic, retain) NSString *addtime;
@property (nonatomic, assign) BOOL isfavorites;
@property (nonatomic, assign) int detailid;
@property (nonatomic, retain) NSArray *letter;
@property (nonatomic, retain) NSString *imageurl;
@property (nonatomic, retain) NSString *imagedesc;
@property (nonatomic, assign) int ordernum;
@property (nonatomic, retain) NSString *officialurl;
@property (nonatomic, retain) NSString *onlineservice;
@property (nonatomic, retain) NSString *ServicePhone;



+ (id)voWithDic:(NSDictionary *)dic;
- (id)initWithDic:(NSDictionary *)dic;

@end
