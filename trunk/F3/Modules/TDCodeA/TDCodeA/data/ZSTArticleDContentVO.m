//
//  NewsContentVO.m
//  News
//
//  Created by admin on 12-7-25.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTArticleDContentVO.h"

@implementation ZSTArticleDContentVO

@synthesize title;
@synthesize summary;
@synthesize keyword;
@synthesize source;
@synthesize addtime;
@synthesize isfavorites;
@synthesize detailid;
@synthesize letter;
@synthesize imageurl;
@synthesize imagedesc;
@synthesize ordernum;
@synthesize officialurl;
@synthesize onlineservice;
@synthesize ServicePhone;


+ (id)voWithDic:(NSDictionary *)dic
{
    return [[[self alloc] initWithDic: dic] autorelease];
}

- (id)initWithDic:(NSDictionary *)dic
{
    if( (self=[super init])) {
        self.title =  [dic valueForKey:@"title"]; 
        self.summary =  [dic valueForKey:@"summary"];
        self.keyword =  [dic valueForKey:@"keyword"];
        self.source =  [dic valueForKey:@"source"];
        self.addtime =  [dic valueForKey:@"addtime"];
        self.isfavorites =  [[dic valueForKey:@"isfavorites"] boolValue];
        self.detailid =  [[dic valueForKey:@"detailid"] intValue];
        self.letter =  [dic valueForKey:@"content"];
        self.imageurl =  [dic valueForKey:@"imageurl"];
        self.imagedesc =  [dic valueForKey:@"imagedesc"];
        self.ordernum =  [[dic valueForKey:@"ordernum"] intValue];
        self.officialurl =  [dic valueForKey:@"officialurl"];
        self.onlineservice =  [dic valueForKey:@"onlineservice"];
        self.ServicePhone =  [dic valueForKey:@"ServicePhone"];


    }
    return self;
}


- (void)dealloc 
{
    self.title = nil;
    self.summary = nil;
    self.keyword = nil;
    self.source = nil;
    self.addtime = nil;
    self.imageurl = nil;
    self.imagedesc = nil;
    self.officialurl = nil;
    self.onlineservice = nil;
    self.ServicePhone = nil;
    
    [super dealloc];
}

@end
