//
//  ZSTF3Engine+ArticleB.m
//  ArticleB
//
//  Created by xuhuijun on 13-1-29.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTF3Engine+ArticleD.h"
#import "ZSTGlobal+ArticleD.h"
#import "ZSTCommunicator.h"
#import "ZSTF3Preferences.h"
#import "ZSTLegacyResponse.h"
#import "ZSTSqlManager.h"
#import "JSON.h"

#define ArticleDContentCachePath [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"/AticleD/content"]

@implementation ZSTF3Engine (ArticleD)


////////////////////////////////////////////////////////////////// News content /////////////////////////////////////////////////////////////////////

//新闻内容缓存到文件中
- (void)getArticleDContent:(NSInteger)newsID userInfo:(id)anUserInfo
{
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    
//    NSError *error;
//    if (![fileManager fileExistsAtPath:ArticleDContentCachePath]) {
//        if (![fileManager createDirectoryAtPath:ArticleDContentCachePath withIntermediateDirectories:YES attributes:nil error:&error]) {
//            if ([self isValidDelegateForSelector:@selector(requestDidFail:)]) {
//                ZSTLegacyResponse *response = [ZSTLegacyResponse responseWithJSONResponse:nil
//                                                                           stringResponse:nil
//                                                                                    error:error
//                                                                                 userInfo:nil];
//                [self.delegate performSelector:@selector(requestDidFail:) withObject:response afterDelay:0];
//            }
//            return;
//        }
//    }
    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:newsID], @"newsID", anUserInfo, @"userInfo", nil];
    
//    //检查缓存是否有数据,如果有直接从缓存获取
//    NSString *contentPath = [ArticleDContentCachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%d.json", newsID]];
//    BOOL isDirectory;
//    if ([fileManager fileExistsAtPath:contentPath isDirectory:&isDirectory]) {
//        if (!isDirectory) {
//            NSDictionary *attributes = [fileManager attributesOfItemAtPath:contentPath error:&error];
//            if (attributes && [attributes fileSize]) {
//                NSString *content = [NSString stringWithContentsOfFile:contentPath encoding:NSUTF8StringEncoding error:&error];
//                if (content) {
//                    id json = [content JSONValue];
//                    if (json) {
//                        [self performSelector:@selector(getArticleDContentResponse:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:json, @"json", userInfo, @"userInfo", nil] afterDelay:0.5];
////                        return;
//                    }
//                    
//                } else {
//                    TKDERROR(@"error == %@", error);
//                }
//            } else {
//                TKDERROR(@"error == %@", error);
//            }
//        }
//    }
//    //从缓存获取失败，删除缓存，重新从网络获取
//    [fileManager removeItemAtPath:contentPath error:&error];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setSafeObject:[NSNumber numberWithInt:newsID] forKey:@"MsgID"];
    [[ZSTCommunicator shared] openAPIPostToPath:[GetSingleMessage stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getArticleDContentResponse:userInfo:)
                                       userInfo:userInfo
                                      matchCase:NO];
    
}

- (void)getArticleDContentResponse:(NSDictionary *)param
{
    id results = [param objectForKey:@"json"];
    id userInfo = [param objectForKey:@"userInfo"];
    if ([self isValidDelegateForSelector:@selector(getArticleDContentDidSucceed:userInfo:)]) {
        [self.delegate getArticleDContentDidSucceed:results userInfo:[userInfo objectForKey:@"userInfo"]];
    }
}

- (void)getArticleDContentResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        [self performSelector:@selector(getArticleDContentResponse:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:response.jsonResponse, @"json", userInfo, @"userInfo", nil] afterDelay:0.5];
        NSNumber *newsID = [userInfo objectForKey:@"newsID"];
        
        NSString *contentPath = [ArticleDContentCachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@.json", newsID]];
        NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:response.jsonResponse, @"results", contentPath, @"contentPath", nil];
//        [self performSelector:@selector(writeNewsContentToCache:) withObject:param afterDelay:0];
    } else {
        if ([self isValidDelegateForSelector:@selector(getArticleDContentDidFailed:userInfo:)]) {
            [self.delegate getArticleDContentDidFailed:response.resultCode userInfo:userInfo];
        }
    }
}

- (void) writeNewsContentToCache:(NSDictionary *)param
{
    NSError *error = nil;
    if (![[[param objectForKey:@"results"] JSONRepresentation] writeToFile:[param objectForKey:@"contentPath"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        TKDERROR(@"Write news content failed, error == %@", error);
    }
}

@end

