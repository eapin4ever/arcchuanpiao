//
//  ZSTF3Engine+ArticleB.h
//  ArticleB
//
//  Created by xuhuijun on 13-1-29.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTF3Engine.h"
#import "ZSTGlobal+ArticleD.h"

@protocol ZSTF3EngineArticleBDelegate <ZSTF3EngineDelegate>

@optional

- (void)getArticleDContentDidSucceed:(NSDictionary *)newsContent userInfo:(id)userInfo;
- (void)getArticleDContentDidFailed:(int)resultCode userInfo:(id)userInfo;

@end

@interface ZSTF3Engine (ArticleD)

/**
 *	@brief	获取文章正文
 *
 *	@param 	newsID 	新闻ID
 */
- (void)getArticleDContent:(NSInteger)newsID userInfo:(id)userInfo;
@end
