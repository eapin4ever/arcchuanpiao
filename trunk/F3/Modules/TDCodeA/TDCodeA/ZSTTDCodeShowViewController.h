//
//  ZSTTDCodeShowViewController.h
//  TDCodeA
//
//  Created by xuhuijun on 13-6-20.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTModuleBaseViewController.h"
#import "TTTAttributedLabel.h"

@interface ZSTTDCodeShowViewController : ZSTModuleBaseViewController<TTTAttributedLabelDelegate>
{
    TTTAttributedLabel *contenLabel;
    UIImageView *cotentImageView;
}
@property (nonatomic,strong) NSString *content;

@end
