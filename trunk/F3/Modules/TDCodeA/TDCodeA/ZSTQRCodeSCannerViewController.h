//
//  ZSTQRCodeSCannerViewController.h
//  TDCodeA
//
//  Created by xuhuijun on 13-6-21.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZXingWidgetController.h"

@interface ZSTQRCodeSCannerViewController : ZXingWidgetController<ZXingDelegate>

@end
