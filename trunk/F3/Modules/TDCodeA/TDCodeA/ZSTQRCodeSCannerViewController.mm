//
//  ZSTQRCodeSCannerViewController.m
//  TDCodeA
//
//  Created by xuhuijun on 13-6-21.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTQRCodeSCannerViewController.h"
#import "ZSTUtils.h"
#import "QRCodeReader.h"
#import "ZSTTDCodeLinkWebViewController.h"
#import "ZSTTDCodeShowViewController.h"

@interface ZSTQRCodeSCannerViewController ()

@end

@implementation ZSTQRCodeSCannerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self initWithDelegate:self showCancel:NO OneDMode:NO showLicense:NO];
    QRCodeReader* qrcodeReader = [[QRCodeReader alloc] init];
    NSSet *readers1 = [[NSSet alloc ] initWithObjects:qrcodeReader,nil];
    [qrcodeReader release];
    self.readers = readers1;
    [readers1 release];
    NSBundle *mainBundle = [NSBundle mainBundle];
    self.soundToPlay = [NSURL fileURLWithPath:[mainBundle pathForResource:@"beep-beep" ofType:@"aiff"] isDirectory:NO];
    
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.view.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin |
//    UIViewAutoresizingFlexibleWidth |
//    UIViewAutoresizingFlexibleRightMargin |
//    UIViewAutoresizingFlexibleTopMargin |
//    UIViewAutoresizingFlexibleHeight |
//    UIViewAutoresizingFlexibleBottomMargin;
    
//    self.view.frame = CGRectMake(0, 0, 320, 480);    
}

#pragma mark - ZXingWidgetDelegate methods
- (void)zxingController:(ZXingWidgetController *)controller didScanResult:(NSString *)scanresult
{
    if ([scanresult length] != 0 && scanresult != nil) {
        ZSTTDCodeShowViewController *showVC = [[ZSTTDCodeShowViewController alloc] init];
        showVC.content = scanresult;
        [self.navigationController pushViewController:showVC animated:YES];
    }
    [self stopCapture];
}

- (void)zxingControllerDidCancel:(ZXingWidgetController *)controller
{
    NSLog(@"canceled");
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
