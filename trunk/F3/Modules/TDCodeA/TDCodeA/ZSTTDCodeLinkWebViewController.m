//
//  ZSTTDCodeLinkWebViewController.m
//  TDCodeA
//
//  Created by LiZhenQu on 13-5-22.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTTDCodeLinkWebViewController.h"

@interface ZSTTDCodeLinkWebViewController ()

@end

@implementation ZSTTDCodeLinkWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];

	// Do any additional setup after loading the view.
    [self setURL:self.baseUrl];
    self.type = self.moduleType;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [super dealloc];
}

@end
