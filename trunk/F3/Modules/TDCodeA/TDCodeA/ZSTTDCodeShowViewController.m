//
//  ZSTTDCodeShowViewController.m
//  TDCodeA
//
//  Created by xuhuijun on 13-6-20.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTTDCodeShowViewController.h"
#import "ZSTTDCodeLinkWebViewController.h"

@interface ZSTTDCodeShowViewController ()

@end

@implementation ZSTTDCodeShowViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];

    self.navigationItem.title = NSLocalizedString(@"扫描结果", nil);
    
    UIImageView *resultImage = [[UIImageView alloc] initWithFrame:CGRectMake(30, 25, 20, 20)];
    resultImage.image = ZSTModuleImage(@"module_tdcodea_resultImage.png");
    [self.view addSubview:resultImage];
    [resultImage release];
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(60, 25, 100, 20);
    label.backgroundColor = [UIColor clearColor];
    label.text = NSLocalizedString(@"扫描结果", nil);
    label.textColor = RGBCOLOR(72, 109, 137);
    label.font = [UIFont systemFontOfSize:16];
    [self.view addSubview:label];
    [label release];
    
    cotentImageView = [[UIImageView alloc] initWithFrame:CGRectMake(22, 60, 276, 10)];
    cotentImageView.userInteractionEnabled = YES;
    cotentImageView.image = [ZSTModuleImage(@"module_tdcodea_resultBg.png") stretchableImageWithLeftCapWidth:0 topCapHeight:2];
    [self.view addSubview:cotentImageView];
    
    contenLabel = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(10, 8, cotentImageView.size.width-20, cotentImageView.size.height-20)];
    contenLabel.delegate = self;
    contenLabel.numberOfLines = 0;
    contenLabel.text = self.content;
    contenLabel.textColor = RGBCOLOR(102, 102, 102);
    contenLabel.lineBreakMode = UILineBreakModeWordWrap;
    contenLabel.font = [UIFont systemFontOfSize:16];
    contenLabel.backgroundColor = [UIColor clearColor];
    [cotentImageView addSubview:contenLabel];
    
    CGSize size = [self.content sizeWithFont:[UIFont systemFontOfSize:16] constrainedToSize:CGSizeMake(276, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
    cotentImageView.size = CGSizeMake(276, size.height + 35+(IS_IOS_7?10:0));
    contenLabel.frame = CGRectMake(10, 5, cotentImageView.size.width-20, cotentImageView.size.height);
    
    NSString* regexString = @"(http[s]{0,1}|ftp)://[a-zA-Z0-9\\.\\-]+\\.([a-zA-Z]{2,4})(:\\d+)?(/[a-zA-Z0-9\\.\\-~!@#$%^&amp;*+?:_/=<>]*)?";
    NSRange range = [self.content rangeOfString:regexString options:NSRegularExpressionSearch];
    
    [contenLabel setText:self.content afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
        [mutableAttributedString addAttribute:(NSString *)(kCTForegroundColorAttributeName) value:(id)RGBCOLOR(253, 98, 33) range:range];
        return mutableAttributedString;
    }];
    
    [contenLabel addLinkToURL:[NSURL URLWithString:self.content] withRange:range];    
}

#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {

    ZSTTDCodeLinkWebViewController *viewController = [[[ZSTTDCodeLinkWebViewController alloc] init] autorelease];
    viewController.baseUrl = [url absoluteString];
    viewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:viewController animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dealloc
{
    [contenLabel release];
    [cotentImageView release];
    [super dealloc];
}

@end
