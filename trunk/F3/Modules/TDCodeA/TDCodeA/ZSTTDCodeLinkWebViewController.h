//
//  ZSTTDCodeLinkWebViewController.h
//  TDCodeA
//
//  Created by LiZhenQu on 13-5-22.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTWebViewController.h"

@interface ZSTTDCodeLinkWebViewController : ZSTWebViewController<UIWebViewDelegate>
@property (nonatomic,strong)NSString *baseUrl;


@end
