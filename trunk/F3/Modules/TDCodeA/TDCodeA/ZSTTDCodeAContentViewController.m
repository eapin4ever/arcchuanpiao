//
//  NewsContentViewController.m
//  F3
//
//  Created by admin on 12-7-19.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTTDCodeAContentViewController.h"
#import "ZSTTDCODELocalSubstitutionCache.h"
#import "ZSTTDCODETKResourceURLProtocol.h"
#import "ZSTTDCODETKResourceURL.h"
#import "TKUIUtil.h"
#import "ZSTUtils.h"
#import "ZSTModuleManager.h"
#import "ZSTModule.h"


#define ArticleFontSize   @"ArticleFontSize"
#define LargeFont   @"3"
#define MiddleFont  @"2"
#define NormalFont  @"1"
#define SmallFont   @"0"

@interface ZSTTDCodeAContentViewController ()

@end

@implementation ZSTTDCodeAContentViewController

@synthesize hTableView;
@synthesize newsArr;
@synthesize selectedImageView;

//@synthesize webView;

//@synthesize textView;

@synthesize contentVo;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [self.engine cancelAllRequest];
    self.hTableView = nil;
    self.selectedImageView = nil;
    self.newsArr = nil;
    [super dealloc];
}

- (void)refreshTap:(UIControl *)sender
{
    //刷新
    TKHorizontalTableViewCell *cell = (TKHorizontalTableViewCell *)sender.superview.superview;
    UIActivityIndicatorView *aiView = (UIActivityIndicatorView *)[cell viewWithTag:100];
    if (![aiView isAnimating]) {
        [aiView startAnimating];
    }
    UIWebView *webView = (UIWebView *)[cell viewWithTag:99];
    UILabel *refreshLabel = (UILabel *)[cell viewWithTag:102];
    UIControl *refreshCtr = (UIControl *)[webView viewWithTag:103];
    [refreshCtr removeFromSuperview];
    refreshLabel.hidden = YES;
    
    NSInteger index = cell.tag - 1000;
//    ZSTArticleBVO *currentnvo = [ZSTArticleBVO voWithDic:[self.newsArr objectAtIndex:index]];
//    [self.engine getArticleAContent:[currentnvo.MsgId integerValue] userInfo:[NSNumber numberWithInt:index] version:currentnvo.Version];
    [self.engine getArticleDContent:45 userInfo:[NSNumber numberWithInt:index]];
}


#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 设置图片缓存
    //	LocalSubstitutionCache *cache = [[LocalSubstitutionCache alloc] init];
    //	[NSURLCache setSharedURLCache:cache];
    //    [cache release];
    
    [ZSTARTICLEDTKResourceURLProtocol registerProtocol];
    
    self.view.backgroundColor = RGBCOLOR(239, 239, 239);
    hTableView = [[TKHorizontalTableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    hTableView.delegate = self;
    hTableView.dataSource = self;
    hTableView.backgroundColor = RGBCOLOR(239, 239, 239);
    hTableView.showsVerticalScrollIndicator = NO;
    hTableView.showsHorizontalScrollIndicator = NO;
    hTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    hTableView.pagingEnabled = YES;
    
    [self.view addSubview:hTableView];        
    [self initFontSize];
}

- (void)initFontSize
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *fontSizeStr = [ud objectForKey:ArticleFontSize];
    
    if (fontSizeStr == nil) {
        fontSize = FontSize_Normal;
    } else {
        fontSize = [fontSizeStr intValue];
        if (fontSize < 0 || fontSize > 3) {
            fontSize = FontSize_Normal;
        }
    }
    
    if (fontSize == FontSize_Small) {
        UIButton *btn = (UIButton *)[self.view viewWithTag:300];
        btn.enabled = YES;
        
        btn = (UIButton *)[self.view viewWithTag:303];
        btn.enabled = NO;
        [btn setImage:ZSTModuleImage(@"module_ArticleA_font_zoomOut_d.png") forState:UIControlStateNormal];
    }
    
    if (fontSize == FontSize_Large) {
        UIButton *btn = (UIButton *)[self.view viewWithTag:300];
        btn.enabled = NO;
        [btn setImage:ZSTModuleImage(@"module_ArticleA_font_zoomIn_d.png") forState:UIControlStateNormal];
        
        btn = (UIButton *)[self.view viewWithTag:303];
        btn.enabled = YES;
    }
}

- (void)fontZoomIn
{
    int size = fontSize + 1;
    fontSize = size>FontSize_Large ? FontSize_Large : size;
    
    if (fontSize == FontSize_Large) {
        UIButton *btn = (UIButton *)[self.view viewWithTag:300];
        btn.enabled = NO;
        [btn setImage:ZSTModuleImage(@"module_ArticleA_font_zoomIn_d.png") forState:UIControlStateNormal];
    }
    
    
    UIButton *btn = (UIButton *)[self.view viewWithTag:303];
    if (!btn.enabled) {
        btn.enabled = YES;
        [btn setImage:ZSTModuleImage(@"module_ArticleA_font_zoomOut.png") forState:UIControlStateNormal];
    }


    [self changeFontSize];
}

- (void)fontZoomOut
{
    int size = fontSize - 1;
    fontSize = size<0 ? FontSize_Small : size;
    
    if (fontSize == FontSize_Small) {
        UIButton *btn = (UIButton *)[self.view viewWithTag:303];
        btn.enabled = NO;
        [btn setImage:ZSTModuleImage(@"module_ArticleA_font_zoomOut_d.png") forState:UIControlStateNormal];
    }
    
    UIButton *btn = (UIButton *)[self.view viewWithTag:300];
    if (!btn.enabled) {
        btn.enabled = YES;
        [btn setImage:ZSTModuleImage(@"module_ArticleA_font_zoomIn.png") forState:UIControlStateNormal];
    }

    [self changeFontSize];
}

- (void)changeFontSize
{
    int index = hTableView.contentOffset.x / 320;
    TKHorizontalTableViewCell *cell = [hTableView cellForIndex:index];
    UIWebView *webView = (UIWebView *)[cell viewWithTag:99];
    
    NSString *fontSizeStr = @"1";
    if (fontSize == FontSize_Large) {
        [webView stringByEvaluatingJavaScriptFromString: @"changeFontSize('font_large');" ];
        fontSizeStr = LargeFont;
    } else if (fontSize == FontSize_Middle) {
        [webView stringByEvaluatingJavaScriptFromString: @"changeFontSize('font_middle');" ];
        fontSizeStr = MiddleFont;
    } else if (fontSize == FontSize_Normal) {
        [webView stringByEvaluatingJavaScriptFromString: @"changeFontSize('font_normal');" ];
        fontSizeStr = NormalFont;
    } else {
        [webView stringByEvaluatingJavaScriptFromString: @"changeFontSize('font_small');" ];
        fontSizeStr = SmallFont;
    }
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:fontSizeStr forKey:ArticleFontSize];
    [ud synchronize];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



#pragma mark ---
#pragma UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    if ([request.URL.scheme isEqualToString:@"image"]) {
        
        NSString *url = [request.URL description];
        NSRange range1 = [url rangeOfString:@"///"];
        NSRange range2 = [url rangeOfString:@"///" options:NSCaseInsensitiveSearch range:NSMakeRange(range1.location + 3, url.length - range1.location - 3)];
        
        NSString *offsetY = [request.URL.description substringWithRange:NSMakeRange(range1.location+range1.length, range2.location - range1.location-range1.length)];
        
        NSString *imageUrl = [request.URL.description substringFromIndex:range2.location+range2.length];        
        if (imageUrl!=nil && [imageUrl length]>0) {
            ZSTARTICLEDTKResourceURL *resourceURL = [ZSTARTICLEDTKResourceURL URLWithString:imageUrl];
            NSString *cachePath = [ZSTARTICLEDTKResourceURLProtocol cachePathForResourceURL:resourceURL];
            UIImage *image = [UIImage imageWithContentsOfFile:cachePath];
            if (image) {
                
                self.selectedImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(217, [offsetY intValue], 90, 90)] autorelease];
                self.selectedImageView.imageView.contentMode = UIViewContentModeScaleAspectFill;
                self.selectedImageView.imageView.clipsToBounds = YES;
                self.selectedImageView.backgroundColor = RGBCOLOR(239, 239, 239);
                
                self.selectedImageView.defaultImage = image;
                [self.selectedImageView clear];
                self.selectedImageView.url = [NSURL URLWithString:resourceURL.originalUrl];
                [self.selectedImageView loadImage];
                
                [self.view addSubview:selectedImageView];
                
                
                selectedImageView.imageView.zoomedSize = [self displayRectForImage:CGSizeMake(self.selectedImageView.imageView.image.size.width, self.selectedImageView.imageView.image.size.height)];
                
                selectedImageView.imageView.wrapInScrollviewWhenZoomed = YES;
                selectedImageView.showLoadingWheel = YES;
                
                [selectedImageView.imageView zoomIn];
                selectedImageView.imageView.zoomDelegate = self;
                selectedImageView.callbackOnSetImage = self;
            }            
            return NO;
        }
    } else if((navigationType == UIWebViewNavigationTypeLinkClicked || navigationType == UIWebViewNavigationTypeFormSubmitted) && [request.URL.scheme caseInsensitiveCompare:@"http"] == NSOrderedSame) {
        
        [[UIApplication sharedApplication] openURL:request.URL];
        return NO;
    }else if (navigationType == UIWebViewNavigationTypeLinkClicked && [request.URL.scheme caseInsensitiveCompare:@"native"] == NSOrderedSame) {
        
        NSDictionary *params = [request.URL.absoluteString queryDictionaryUsingEncoding:NSUTF8StringEncoding];
        NSString *module_id = [params objectForKey:@"native://?module_id"];
        NSString *module_type = [params objectForKey:@"module_type"];
        NSString *title = [params objectForKey:@"title"];
        
        NSMutableDictionary *moduleParams = [NSMutableDictionary dictionary];
        [moduleParams setSafeObject:module_type forKey:@"type"];
        
        ZSTModule *module = [[ZSTModuleManager shared] findModule:[module_id integerValue]];
        if (module) {
            UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:[module_id integerValue] withOptions:moduleParams];
            if (controller) {
                controller.hidesBottomBarWhenPushed = YES;
                controller.navigationItem.titleView = [ZSTUtils titleViewWithTitle:title];
                controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:@"返回" target:self selector:@selector (popViewController)];
                [self.navigationController pushViewController:controller animated:YES];      
            }
        }
        
    }
    return YES;
}
#pragma mark HJManagedImageVDelegate
-(void) managedImageSet:(HJManagedImageV*)mi
{
    mi.imageView.contentMode = UIViewContentModeScaleAspectFill;
    mi.imageView.size = [self displayRectForImage:CGSizeMake(self.selectedImageView.imageView.image.size.width, self.selectedImageView.imageView.image.size.height)];
    mi.imageView.center = self.view.superview.superview.center;
}

- (void)zoomWindow:(MTZoomWindow *)zoomWindow didZoomOutView:(UIView *)view {
    self.selectedImageView.alpha = 0;
    [self.selectedImageView removeFromSuperview];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    TKDERROR(@"error == %@", error);
}

- (void)webViewDidStartLoad:(UIWebView *)webView //网页加载时调用
{
    //    [aiView startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView //网页完成加载时调用
{
//    [webView stringByEvaluatingJavaScriptFromString: @"changeFontSize('font_large');" ];
}
- (CGSize)displayRectForImage:(CGSize)imageSize {
    CGRect screenRect = TKScreenBounds();
    
    if (imageSize.width > screenRect.size.width) {
        CGFloat height = imageSize.height * screenRect.size.width / imageSize.width;
        CGFloat width = screenRect.size.width;
        if (height > screenRect.size.height) {
            width = width * screenRect.size.height/height;
            height = screenRect.size.height;
        }
        return CGSizeMake(width, height);
    }else if (imageSize.height > screenRect.size.height) {
        CGFloat width = imageSize.width * screenRect.size.height / imageSize.height;
        CGFloat height = screenRect.size.height;
        
        return CGSizeMake(width, height);
    } else {
        return CGSizeMake(imageSize.width, imageSize.height); 
    }
}


- (void)viewDidUnload
{
    //    [webView removeFromSuperview];
    [super viewDidUnload];
    //    [aiView removeFromSuperview];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)templateEngine:(MGTemplateEngine *)engine blockStarted:(NSDictionary *)blockInfo
{
    //	NSLog(@"Started block %@", [blockInfo objectForKey:BLOCK_NAME_KEY]);
}

- (void)templateEngine:(MGTemplateEngine *)engine blockEnded:(NSDictionary *)blockInfo
{
    //	NSLog(@"Ended block %@", [blockInfo objectForKey:BLOCK_NAME_KEY]);
}

- (void)templateEngineFinishedProcessingTemplate:(MGTemplateEngine *)engine
{
    //	NSLog(@"Finished processing template.");
}

- (void)templateEngine:(MGTemplateEngine *)engine encounteredError:(NSError *)error isContinuing:(BOOL)continuing;
{
	NSLog(@"Template error: %@", error);    
}

#pragma mark --
#pragma TKHorizontalTableViewDataSource -------------

- (NSInteger)numberOfRowsInTableView:(TKHorizontalTableView *)tableView {
    return 1;
}

- (TKHorizontalTableViewCell *)tableView:(TKHorizontalTableView *)tableView cellAtIndex:(NSUInteger)index {
    
    TKHorizontalTableViewCell *hCell = [tableView dequeueReusableCell];
    
    if (hCell == nil) {
        hCell = [[[TKHorizontalTableViewCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.height, 320)] autorelease];
        hCell.backgroundColor = RGBCOLOR(239, 239, 239);
        NSInteger height = 480-44-20-43-40;
        UIWebView *webView = [[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, height)] autorelease];
        webView.tag = 99;
        webView.backgroundColor = RGBCOLOR(239, 239, 239);
        webView.dataDetectorTypes = UIDataDetectorTypeNone;
        webView.opaque = NO;
        webView.delegate = self;
        webView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        //去掉阴影
        for(UIView* subView in [webView subviews])
        {
            if([subView isKindOfClass:[UIScrollView class]]){
                for(UIView* shadowView in [subView subviews])
                {
                    if([shadowView isKindOfClass:[UIImageView class]]){
                        [shadowView setHidden:YES];
                    }
                }
            }
        }
        [hCell addSubview:webView];
        
        UIActivityIndicatorView *aiView = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
        aiView.center = CGPointMake(160, 185);
        aiView.tag = 100;
        [hCell addSubview:aiView];
        
        UILabel *refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 170, 200, 30)];
        refreshLabel.font = [UIFont boldSystemFontOfSize:18];
        refreshLabel.text = @"加载失败，点击重新加载";
        refreshLabel.textAlignment = UITextAlignmentCenter;
        refreshLabel.textColor = RGBACOLOR(200, 200, 200, 1);
        refreshLabel.backgroundColor = [UIColor clearColor];
        refreshLabel.tag = 102;
        [hCell addSubview:refreshLabel];
        [refreshLabel release];
    }
    
    hCell.tag = index+1000;
    
    //    480-44-37-20
    UIWebView *webView = (UIWebView *)[hCell viewWithTag:99];
    [webView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML='';"];
    UIActivityIndicatorView *aiView = (UIActivityIndicatorView *)[hCell viewWithTag:100];
    [aiView startAnimating];
    
//    ZSTArticleBVO *nvo = [ZSTArticleBVO voWithDic:[self.newsArr objectAtIndex:index]];
//    
    [self.engine getArticleDContent:45 userInfo:[NSNumber numberWithInt:index]];
    
    UILabel *refreshLabel = (UILabel *)[hCell viewWithTag:102];
    refreshLabel.hidden = YES;
    
    return hCell;
}

- (void)getArticleDContentDidSucceed:(NSDictionary *)newsContent userInfo:(id)userInfo{
    NSNumber *index = userInfo;        
//    [self.dao setArticleA:[nvo.msgid integerValue] isRead:YES];//设置内容为已读
    ZSTArticleDContentVO *contentVO = [ZSTArticleDContentVO voWithDic:newsContent];
    self.contentVo = contentVO;
    
    //目前只做图片
    NSMutableString *urlStr = [NSMutableString string];
    if ([contentVO.letter isKindOfClass:[NSArray class]] && [contentVO.letter count]>0) {
        for (int i=0; i<[contentVO.letter count]; i++) {
            NSDictionary *fileDic = [contentVO.letter objectAtIndex:i];
            NSString *url = [fileDic objectForKey:@"imageurl"];
             NSString *phoneStr = [fileDic objectForKey:@"phone"];
            if (url && [url length]>0) {
                
                NSString *origUrl = @"";
                ZSTARTICLEDTKResourceURL *resourceURL = [ZSTARTICLEDTKResourceURL resourceURLWithResource:url originalUrl:origUrl MIMEType:@"image/png"];
                [urlStr appendFormat:@"<span><span class=\"photo_box\"><img class=\"photo\" style=\"margin:0 0 0 0; padding:2px; algin:center\" src=\"%@\" onload=\"javascript:DrawImage(this)\"/></span>", resourceURL.absoluteString?resourceURL.absoluteString:@""];
                NSString *desc = [fileDic objectForKey:@"imagedesc"];
                if (desc) {
                    [urlStr appendFormat:@"<span class=\"photo_title\">%@</span></span>", desc];
                }
                
                if (phoneStr) {
                    [urlStr appendFormat:@"<img src=\"%@\" style=\"vertical-align:middle\"> <a href='tel:%@' style=\"color:#666666; text-decoration:none; line-height:30px; vertical-align :middle\">%@</a><br>",ZSTPathForModuleBundleResource(@"module_ArticleC_phone.png"),phoneStr,phoneStr];
                }
            }
            
            NSString *str = [[contentVO.letter objectAtIndex:i] objectForKey:@"letter"];
            NSString *formatContent = [self formatArticleContent:str];
            [urlStr appendString:[NSString stringWithFormat:@"<p style=\"color:#2F2F2F; font-size:12pt\">%@</p>",formatContent]];
        }
 	}
    
    // Set up template engine with your chosen matcher.
    MGTemplateEngine *engine = [MGTemplateEngine templateEngine];
    [engine setDelegate:self];
    [engine setMatcher:[ICUTemplateMatcher matcherWithTemplateEngine:engine]];    
    
    NSString *templatePath = ZSTPathForModuleBundleResource(@"module_ArticleD_content_template.html");
    
    NSDictionary *variables = [NSDictionary dictionaryWithObjectsAndKeys:contentVO.title,@"title", contentVO.addtime,@"date" ,urlStr ,@"body", [self getFontSize], @"fontClass", @"", @"theme", nil];
    
    NSString *result = [engine processTemplateInFileAtPath:templatePath withVariables:variables];
    
    //不用生成html文件，直接加载字符串
    NSURL *url = [NSURL fileURLWithPath:ZSTPathForModuleBundleResource(@"")];
    
    TKHorizontalTableViewCell *cell = [hTableView cellForIndex:[index intValue]];
    UIActivityIndicatorView *aiView = (UIActivityIndicatorView *)[cell viewWithTag:100];
    if ([aiView isAnimating]) {
        [aiView stopAnimating];
    }
    UIWebView *webView = (UIWebView *)[cell viewWithTag:99];
    [webView loadHTMLString:result baseURL:url];  
    UILabel *refreshLabel = (UILabel *)[cell viewWithTag:102];
    refreshLabel.hidden = YES;
    
}

- (NSString *)formatArticleContent:(NSString *)content
{
    if (content == nil) {
        return nil;
    }
    
    NSMutableString *contentStr = [NSMutableString string];
    [contentStr appendString:content];
    
    [contentStr replaceOccurrencesOfString:@"\r" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, contentStr.length)];
    [contentStr replaceOccurrencesOfString:@"\n" withString:@"<br />" options:NSCaseInsensitiveSearch range:NSMakeRange(0, contentStr.length)];
    return contentStr;
}

- (NSString *)getFontSize
{
    if (fontSize == FontSize_Normal) {
        return @"font_normal";
    } else if (fontSize == FontSize_Middle) {
        return @"font_middle";
    } else if (fontSize == FontSize_Large) {
        return @"font_large";
    } else {
        return @"font_small";
    }
}

- (void)getArticleDContentDidFailed:(ZSTResponse *)response
{  
    NSInteger selectIndex = [[response.userInfo objectForKey:@"userInfo"] integerValue];
    
    TKHorizontalTableViewCell *cell = [hTableView cellForIndex:selectIndex];
    UIActivityIndicatorView *aiView = (UIActivityIndicatorView *)[cell viewWithTag:100];
    UIWebView *webView = (UIWebView *)[cell viewWithTag:99];
    
    if ([aiView isAnimating]) {
        [aiView stopAnimating];
    }
    
    UILabel *refreshLabel = (UILabel *)[cell viewWithTag:102];
    refreshLabel.hidden = NO;
    
    UIControl *refreshCtr = [[UIControl alloc] initWithFrame:self.view.frame];
    [refreshCtr addTarget:self action:@selector(refreshTap:) forControlEvents:UIControlEventTouchUpInside];
    refreshCtr.tag = 103;
    [webView addSubview:refreshCtr];
    [refreshCtr release];
}

#pragma mark --
#pragma TKHorizontalTableViewDelegate -------------
- (CGFloat)tableView:(TKHorizontalTableView *)tableView widthForCellAtIndex:(NSUInteger)index {
    return self.view.frame.size.width;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (hTableView.contentOffset.x < -20) {
//        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //放大 分享 收藏 缩小 按钮的tag值 300 301 302 303
    for (int i=300; i<304; i++) {
        UIButton *btn = (UIButton *)[self.view viewWithTag:i];
        btn.enabled = NO;
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //放大 分享 收藏 缩小 按钮的tag值 300 301 302 303
    for (int i=300; i<304; i++) {
        UIButton *btn = (UIButton *)[self.view viewWithTag:i];
        btn.enabled = YES;
    }
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [super touchesBegan:touches withEvent:event];
//
//}

@end
