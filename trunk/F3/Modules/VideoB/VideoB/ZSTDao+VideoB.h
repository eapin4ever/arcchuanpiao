//
//  ZSTDao+VideoB.h
//  VideoB
//
//  Created by lizhenqu on 13-3-28.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZSTGlobal+VideoB.h"


@interface ZSTDao(VideoB)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)createTableIfNotExistForVIDEOBModule;

/**
 *	@brief 添加视频
 *
 *	@param 	videoId         视频ID
 *	@param 	videoName       视频名称
 *	@param 	videoMemo       
 *	@param 	videoType 
 *	@param 	fileUrl 
 *	@param 	duration 
 *	@param 	orderNum 
 *	@param 	favoriteCount 
 *	@param 	supportCount 
 *	@param 	trampleCount 
 *	@param 	addTime 
 *  @param  iconUrl 
 *  @param  description     
 *	@return	操作是否成功
 */
- (BOOL)addVideoID:(NSString*)videoid
         videoName:(NSString*)videoname
         videoMemo:(NSString*)videomemo
         videoType:(NSInteger)videotype
           iconUrl:(NSString*)iconurl
           fileUrl:(NSString*)fileurl
          duration:(NSInteger)duration
          orderNum:(NSInteger)ordernum
     favoriteCount:(NSInteger)favoritecount
      supportCount:(NSInteger)supportcount
      trampleCount:(NSInteger)tramplecount
       description:(NSString*)description
           addTime:(NSString*)addtime;

/**
 *	@brief 删除所有的视频
 *
 *	@return	操作是否成功
 */
- (BOOL)deleteAllVideos;

/**
 *	@brief  获取所有视频
 *	@return	操作是否成功
 */
- (NSArray *)getVideosList;

/**
 *	@brief 判断视频是否已存在
 *
 *	@return	视频是否存在
 */
- (BOOL)videoExist:(NSString*)videoID;

/**
 *	@brief 删除视频
 *
 *	@return	操作是否成功
 */
- (BOOL)deleteVideo:(NSString*)videoID;

/**
 *	@brief 返回视频条数
 *
 *	@return	视频条数
 */
- (NSUInteger)videoCount;

/**
 *	@brief 返回最新的视频的ID
 *
 *	@return	最新的视频的ID
 */
- (NSUInteger) getMaxVideoId;

- (NSUInteger) getMinVideoId;



//////////////////////////////////////////////////////////////////////////////////////////////////////////////

@end
