//
//  VideoPlayerController.h
//  VoiceChina
//
//  Created by lizhenqu on 13-2-22.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import "ZSTF3Engine.h"
#import "ZSTF3Engine+VideoB.h"
#import <PMRepairButton.h>


@interface ZSTVideoPlayerController : UIViewController<ZSTF3EngineVideoBDelegate>
{
    NSString *mediaBuy;
    
    NSDictionary *allNote;
    
    MPMoviePlayerController *moviePlayer ;

    UIView *bottomView;
    UIView *headView;
    
//  UIButton *playBtn;
    PMRepairButton *playBtn;
    
    UISlider *playSlider;
    UILabel *nowtimeLabel;
    UILabel *leavetimeLabel;
    
    UISlider *voiceSlider;
    
    NSTimer *moveTimer;
    
    UIActivityIndicatorView *waitAction;
    UIImageView *playIV;
    
}
@property (nonatomic,strong) NSString *playUrl;
@property (nonatomic, strong) NSString *videoId;

//@property (nonatomic ,retain) NSString *filePath;
//@property (nonatomic ,retain) NSDictionary *allNote;
//@property (nonatomic ,retain) NSString *mediaUrl;
//@property (nonatomic ,retain) NSString *mediaVid;
//@property (nonatomic ,retain) NSString *mediaPlayer;
//@property (nonatomic ,retain) NSString *mediaName;
//@property (nonatomic ,retain) NSString *mediaBuy;


@end
