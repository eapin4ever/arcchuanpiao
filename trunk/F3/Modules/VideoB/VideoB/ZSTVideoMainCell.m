
//
//  NewsMainCell.m
//  F3
//
//  Created by admin on 12-7-17.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTVideoMainCell.h"
#import "ZSTGlobal+VideoB.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ZSTVideoMainCell
@synthesize cellImageView;
@synthesize title;
@synthesize subTitle;
@synthesize addTime;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier 
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) 
    {

        self.backgroundColor = [UIColor whiteColor];
        
        self.frame = CGRectMake(0, 0, 320, 80);
        cellImageView = [[UIImageView alloc] initWithFrame:CGRectMake(16, 12, 70, 50)];
        cellImageView.layer.cornerRadius = 4;
        cellImageView.layer.masksToBounds = YES;
        cellImageView.layer.shadowColor = [UIColor blackColor].CGColor;
        cellImageView.layer.shadowOffset = CGSizeMake(2, 2);
        [self.contentView addSubview:cellImageView];
        
        
        title = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellImageView.frame)+10, (self.frame.size.height*0.20)/2, self.frame.size.width - CGRectGetMaxX(cellImageView.frame)-35, CGRectGetHeight(cellImageView.frame)*0.5)];
        title.backgroundColor = [UIColor clearColor];
        title.textColor = RGBCOLOR(0,0, 0);
        title.font = [UIFont boldSystemFontOfSize:14];
        title.text =  @"";
        title.highlightedTextColor = [UIColor whiteColor];
        title.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:title];
        
        subTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cellImageView.frame)+10, CGRectGetMaxY(title.frame), self.frame.size.width - CGRectGetMaxX(cellImageView.frame)-40, CGRectGetHeight(cellImageView.frame)*0.5)];
        subTitle.text = @"";
        subTitle.font = [UIFont systemFontOfSize:11];
        subTitle.highlightedTextColor = [UIColor whiteColor];
        subTitle.textAlignment = NSTextAlignmentLeft;
        subTitle.numberOfLines = 0;
        subTitle.textColor = RGBCOLOR(194,194, 194);
        subTitle.backgroundColor = [UIColor clearColor];

        
        addTime = [[UILabel alloc] initWithFrame:CGRectMake(197, 8, 108, 10)];
        addTime.backgroundColor = [UIColor clearColor];
        addTime.font = [UIFont systemFontOfSize:10];
        addTime.textAlignment = NSTextAlignmentRight;
        addTime.textColor = RGBCOLOR(194,194, 194);

        
        UIImageView *videoImage = [[UIImageView alloc] initWithFrame:CGRectMake(38, 28, 24, 24)];
        videoImage.image = [UIImage imageNamed:@"Module.bundle/module_videob_list_icon_decoration.png"];
        [self.contentView addSubview:videoImage];
        
        UIImageView *dividlineImgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 77, 300, 1)];
        dividlineImgView.image = ZSTModuleImage(@"module_videob_dividline.png");
        [self.contentView addSubview:dividlineImgView];
        [self bringSubviewToFront:dividlineImgView];

        
        UIImageView *rightIconImgView = [[UIImageView alloc] initWithFrame:CGRectMake(295, 33, 7, 12)];
        rightIconImgView.image = ZSTModuleImage(@"module_videob_icon_right.png");
        [self.contentView addSubview:rightIconImgView];

    }
    return self;
}

- (void)configCell:(ZSTVideoBVO*)nvo
{
    if (!nvo) {
        
        return;
    }
    title.text = nvo.videoname;
    subTitle.text = nvo.Description;
    addTime.text = nvo.addtime;

    [cellImageView setImageWithURL:[NSURL URLWithString:nvo.iconurl] placeholderImage:[UIImage imageNamed:@"Module.bundle/module_videob_list_default_img.png"]];
}

@end



