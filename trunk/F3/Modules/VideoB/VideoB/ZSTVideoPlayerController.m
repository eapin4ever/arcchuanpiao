//
//  VideoPlayerController.m
//  VoiceChina
//
//  Created by lizhenqu on 13-2-22.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTVideoPlayerController.h"

@interface ZSTVideoPlayerController ()
{
    BOOL isfirst;
}

@end

@implementation ZSTVideoPlayerController
@synthesize playUrl;
@synthesize videoId;

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

float lastnum;
bool iamrun;

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    
    iamrun = NO;
    isfirst = YES;
    
    lastnum = 0.0f;
    
    
    //    [[UIApplication sharedApplication] setStatusBarStyle: UIStatusBarStyleBlackOpaque];
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
    
    self.wantsFullScreenLayout = YES;
    
    

        
    moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:playUrl]];
        //        moviePlayer.shouldAutoplay=YES;
        
    
    
    moviePlayer.shouldAutoplay = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(moviePlaybackComplete:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:moviePlayer];
    
    
    [moviePlayer.view setFrame:CGRectMake(0, 0, 480+(iPhone5?88:0), 320)];
    [moviePlayer setControlStyle:MPMovieControlStyleNone];

    [moviePlayer setFullscreen:YES];
    [self.view addSubview:moviePlayer.view];//设置写在添加之后
    [moviePlayer play];
    
    
    bottomView  = [[UIView alloc] initWithFrame:CGRectMake(0, 280, 480+(iPhone5?88:0), 40)];
    
    UIImageView *bottombg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Module.bundle/module_videob_player_bottombg.png"]];
    bottombg.frame = CGRectMake(0, 0, 480+(iPhone5?88:0), 40);
    [bottomView addSubview:bottombg];
    
//    playIV = [[UIImageView alloc] initWithFrame:CGRectMake(220+(iPhone5?44:0), 0, 40, 40)];
//    playIV.contentMode = UIViewContentModeScaleAspectFit;
//    playIV.image = [UIImage imageNamed:@"Module.bundle/module_videob_btn_play.png"];
//    [bottomView addSubview:playIV];
    
    playBtn = [[PMRepairButton alloc] init];
    playBtn.tag = 0;
    playBtn.frame = CGRectMake(220+(iPhone5?44:0), 0, 40, 40);
    [playBtn setImage:[UIImage imageNamed:@"Module.bundle/module_videob_btn_play.png"] forState:UIControlStateNormal];
    [playBtn setImage:[UIImage imageNamed:@"Module.bundle/module_videob_btn_suspended.png"] forState:UIControlStateSelected];
    playBtn.selected = YES;
    [playBtn addTarget:self action:@selector(playController:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:playBtn];
    
    [self.view addSubview:bottomView];
    
    headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 480+(iPhone5?88:0), 40)];
    
    UIImageView *headbg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Module.bundle/module_videob_player_topbg.png"]];
    headbg.frame = CGRectMake(0, 0, 480+(iPhone5?88:0), 40);
    [headView addSubview:headbg];
    
//    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    PMRepairButton *backBtn = [[PMRepairButton alloc] init];
    backBtn.frame = CGRectMake(10, 0, 55, 40);
    backBtn.bounds = CGRectMake(5, 5, 45, 30);
    [backBtn addTarget:self action:@selector(backBtn:) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setImage:[UIImage imageNamed:@"Module.bundle/module_videob_btn_back.png"] forState:UIControlStateNormal];
    [headView addSubview:backBtn];
    //    headView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:1 alpha:0.7];
    
       
    nowtimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(98+(iPhone5?44:0), 10, 40, 20)];
    nowtimeLabel.text = @"0:00";
    nowtimeLabel.textAlignment = NSTextAlignmentRight;
    nowtimeLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0f];
    nowtimeLabel.textColor = [UIColor whiteColor];
    nowtimeLabel.backgroundColor = [UIColor clearColor];
    [headView addSubview:nowtimeLabel];
    
    leavetimeLabel = [[UILabel alloc] initWithFrame:CGRectMake(340+(iPhone5?44:0), 10, 40, 20)];
    leavetimeLabel.text = @"0:00";
    leavetimeLabel.textColor = [UIColor whiteColor];
    leavetimeLabel.backgroundColor = [UIColor clearColor];
    leavetimeLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0f];
    [headView addSubview:leavetimeLabel];
    
    playSlider = [[UISlider alloc] initWithFrame:CGRectMake(140+(iPhone5?44:0), 10, 200, 20)];
    [headView addSubview:playSlider];
    
    if ( [[[UIDevice currentDevice] systemVersion ]floatValue] > 4.3)
        
    {
        playSlider.minimumTrackTintColor = [UIColor redColor];
        playSlider.maximumTrackTintColor = [UIColor blackColor];
    }
    
    [playSlider addTarget:self action:@selector(playchange:) forControlEvents:UIControlEventValueChanged];
    [playSlider addTarget:self action:@selector(playstop:) forControlEvents:UIControlEventTouchDown];
    [playSlider setMinimumValue:0.0f];
    [self.view addSubview:headView];
    
        
    moveTimer =  [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeRun) userInfo:nil repeats:YES];
    
    waitAction = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    waitAction.frame = CGRectMake(0,0, 20.0f, 20.0f);
    [waitAction startAnimating];
    
    waitAction.center = CGPointMake(240+(iPhone5?44:0), 160);
    
    [self.view addSubview:waitAction];
    
//    bottomView.hidden = YES;
//    headView.hidden = YES;
    
}


-(void)viewDidDisappear:(BOOL)animated
{
    
//    if (moviePlayer!=nil)
//    {
        [moviePlayer pause];
        [moviePlayer stop];
//        moviePlayer = nil;
//    }
}



#pragma mark back
-(void)backBtn:(UIButton *)sender
{
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];

    [moviePlayer pause];
    [moviePlayer stop];
    [moviePlayer.view removeFromSuperview];
    moviePlayer = nil;

    if (moveTimer!=nil)
    {
        [moveTimer invalidate];
        moveTimer = nil;
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}




#pragma mark jin du tiao kong zhi
-(void)timeRun
{
    
    if (!waitAction.hidden) {
        if (moviePlayer.currentPlaybackTime>0.5f)
        {
            waitAction.hidden = YES;
        }
    }
    
    if (moviePlayer.currentPlaybackTime > 1.0f && isfirst) {
        
        [self.engine setVideoActionWithDelegete:self opType:1 VideoId:videoId];
        isfirst = NO;
    }

    
    int minutes = floor(moviePlayer.currentPlaybackTime/60);
    if (minutes<0) {
        return;
    }
	int seconds = floor(moviePlayer.currentPlaybackTime - minutes*60);
    if (seconds == 60) {
        minutes+=1;
        seconds = 0;
    }
	nowtimeLabel.text = [NSString stringWithFormat:@"%d:%d",minutes,seconds];

	
	float resTime = moviePlayer.duration;// - moviePlayer.currentPlaybackTime;
    if (resTime == 0) {
        return;
    }
    
	int resMinutes = floor(resTime/60);
	int resSeconds = round(resTime - resMinutes * 60);
    
    if (resSeconds == 60) {
        resMinutes+=1;
        resSeconds = 0;
    }
    
	leavetimeLabel.text = [NSString stringWithFormat:@"%d:%d",resMinutes,resSeconds];
    
    [playSlider setMaximumValue:moviePlayer.duration];
    
    
    [playSlider setValue:moviePlayer.currentPlaybackTime];
    
    ;
    float nowNum = moviePlayer.currentPlaybackTime;
    
    if (moviePlayer.playableDuration>moviePlayer.currentPlaybackTime)
    {
        
    }else
    {

    }
    
    if (lastnum == nowNum || playBtn.tag == 1)
    {
        
       
        
    }else
    {
        playBtn.tag = 0;
        lastnum = nowNum;
    }
    
    
    
}
#pragma mark pause
-(void)playstop:(UISlider *)sender
{
    [moviePlayer pause];
}


-(void)playchange:(UISlider *)sender
{
    
    [moviePlayer pause];
    moviePlayer.currentPlaybackTime = sender.value;
    //    [moviePlayer setInitialPlaybackTime:sender.value];
    [moviePlayer prepareToPlay];
    [moviePlayer play];
}

-(void)playController:(UIButton *)sender
{
    
    if (sender.isSelected)
    {
        if (moveTimer == nil)
        {
            moveTimer =  [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeRun) userInfo:nil repeats:YES];
        }
        [moviePlayer pause];
    }
    else
    {
        [moviePlayer play];
        
    }
    sender.selected = !sender.isSelected;
}


- (void)moviePlaybackComplete:(NSNotification *)notification

{
    [moveTimer invalidate];
    moveTimer = nil;
//    [playBtn setImage:[UIImage imageNamed:@"play_button.png"] forState:UIControlStateNormal];
//    [playIV setImage:[UIImage imageNamed:@"play_button.png"]];
    playBtn.selected = NO;
    playBtn.tag = 1;
    [moviePlayer pause];
    playSlider.value = 0;
    nowtimeLabel.text = @"0:0";
    leavetimeLabel.text = @"0:0";
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    [UIApplication sharedApplication].statusBarOrientation = UIInterfaceOrientationLandscapeRight;
    //    return YES;
    return (interfaceOrientation == UIDeviceOrientationLandscapeLeft);
}

-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (bottomView.hidden == YES) {
        bottomView.hidden = NO;
        headView.hidden = NO;
        
    }else
    {
        bottomView.hidden = YES;
        headView.hidden = YES;
    }

    [self reloadInputViews];
    
}


#pragma heng ping

-(BOOL)shouldAutorotate
{
    return  UIInterfaceOrientationLandscapeLeft;
}
-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscapeRight;
}

-(void)stopPlayer{
    
    moviePlayer.initialPlaybackTime = -1;
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200 //3.2以上的需要先暂停再停止
    [moviePlayer pause];
#endif
    
    [moviePlayer stop];
    
}

-(void)dealloc
{
    
    [self stopPlayer];
    
    moviePlayer = nil;

    if (headView!=nil)
    {

        headView=nil;
    }if (bottomView!=nil)
    {
        bottomView=nil;
    }

       if (playSlider!=nil) {
        playSlider=nil;
    }
    if (nowtimeLabel!=nil) {
        nowtimeLabel=nil;
    }if (leavetimeLabel!=nil) {
        leavetimeLabel=nil;
    }

}


@end
