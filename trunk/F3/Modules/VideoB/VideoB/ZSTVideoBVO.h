//
//  ZSTVideoBVO.h
//  VideoB
//
//  Created by lizhenqu on 13-3-27.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTVideoBVO : NSObject

@property (nonatomic, retain) NSString *notice;
@property (nonatomic, assign) BOOL hasmore;
@property (nonatomic, retain) NSString *videoid;
@property (nonatomic, assign) NSInteger videotype;
@property (nonatomic, retain) NSString *videoname;
@property (nonatomic, retain) NSString *videomemo;
@property (nonatomic, retain) NSString *iconurl;
@property (nonatomic, retain) NSString *fileurl;
@property (nonatomic, assign) NSInteger duration;
@property (nonatomic, assign) NSInteger ordernum;
@property (nonatomic, assign) NSInteger favoritecount;
@property (nonatomic, assign) NSInteger supportcount;
@property (nonatomic, assign) NSInteger tramplecount;
@property (nonatomic, retain) NSString *Description;
@property (nonatomic, retain) NSString *addtime;

+ (ZSTVideoBVO*)videoListInfoWithdic:(NSDictionary*)dic;
+ (NSMutableArray*)videoListWithdic:(NSDictionary*)dic;
+ (NSMutableArray*)videoListLocalWithdic:(NSArray*)array;

@end

