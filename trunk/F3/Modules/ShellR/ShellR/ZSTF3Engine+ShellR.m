//
//  ZSTF3Engine+ShellR.m
//  ShellR
//
//  Created by LiZhenQu on 14-6-20.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTF3Engine+ShellR.h"
#import "ZSTCommunicator.h"
#import "ZSTGlobal+ShellR.h"

@implementation ZSTF3Engine (ShellR)


- (void)getShellRAD
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    [params setSafeObject:@([ZSTF3Preferences shared].shellId).stringValue forKey:@"moduleid"];

    
    [[ZSTCommunicator shared] openAPIPostToPath:GetShellRAD
                                         params:params
                                         target:self
                                       selector:@selector(getShellRADResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:YES];
}


- (void)getShellRADResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id shellRList = [response.jsonResponse safeObjectForKey:@"info"];
        if (![shellRList isKindOfClass:[NSArray class]]) {
            shellRList = [NSArray array];
        }
        if ([self isValidDelegateForSelector:@selector(getShellRADDidSucceed:)]) {
            [self.delegate getShellRADDidSucceed:shellRList];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getShellBADDidFailed:)]) {
            [self.delegate getShellRADDidFailed:response.resultCode];
        }
    }
    
}

@end
