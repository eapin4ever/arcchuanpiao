//
//  ZSTF3Engine+ShellR.h
//  ShellR
//
//  Created by LiZhenQu on 14-6-20.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTF3Engine.h"

@protocol ZSTF3EngineShellRDelegate <ZSTF3EngineDelegate>

- (void)getShellRADDidSucceed:(NSArray *)ADArray;
- (void)getShellRADDidFailed:(int)resultCode;

@end

@interface ZSTF3Engine (ShellR)

/**
 *	@brief	获取shell广告
 *
 */
- (void)getShellRAD;

@end

