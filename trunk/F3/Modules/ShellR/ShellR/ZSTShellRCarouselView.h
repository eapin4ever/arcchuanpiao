//
//  ZSTNewsCarouselView.h
//  F3
//
//  Created by xuhuijun on 12-7-18.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TKAsynImageView.h"

@class ZSTShellRCarouselView;

@protocol ZSTShellRCarouselViewDataSource <NSObject>

@optional
- (NSInteger)numberOfViewsInCarouselView:(ZSTShellRCarouselView *)newsCarouselView;
- (NSDictionary *)carouselView:(ZSTShellRCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index;

@end

@protocol ZSTShellRCarouselViewDelegate <NSObject>

@optional
- (void)carouselView:(ZSTShellRCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;

@end

@interface ZSTShellRCarouselView : UIView <HJManagedImageVDelegate,UIScrollViewDelegate,TKAsynImageViewDelegate>
{
    UIScrollView *_scrollView;
    UILabel *_describeLabel;
    TKPageControl *_pageControl;
    NSTimer *_carouselTimer;
    
    NSInteger _totalPages;
    NSInteger _curPage;
    
    NSMutableArray *_curSource;
    
    id<ZSTShellRCarouselViewDataSource> _carouselDataSource;
    id<ZSTShellRCarouselViewDelegate>  _carouselDelegate;
}

@property(nonatomic, assign) id<ZSTShellRCarouselViewDataSource> carouselDataSource;
@property(nonatomic, assign) id<ZSTShellRCarouselViewDelegate> carouselDelegate;

- (void)reloadData;
- (void)stopAnimation;
- (void)startAnimation;

@end
