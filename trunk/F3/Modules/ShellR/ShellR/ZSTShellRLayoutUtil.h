//
//  ZSTShellRLayoutUtil.h
//  ShellR
//
//  Created by LiZhenQu on 14-6-23.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTShellRLayoutUtil : NSObject

@property (nonatomic,retain) NSMutableArray * frameArray;

@property (nonatomic,assign) float pageHight;

+(ZSTShellRLayoutUtil *) sharedShellRLayoutUtil;

@end

//frame数据结构
@interface ModuleFrame : NSObject
@property (nonatomic,assign) float pointX;
@property (nonatomic,assign) float pointY;
@property (nonatomic,assign) float width;
@property (nonatomic,assign) float height;

@end