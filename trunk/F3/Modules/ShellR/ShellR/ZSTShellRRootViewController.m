//
//  ZSTShellRRootViewController.m
//  ShellR
//
//  Created by LiZhenQu on 14-6-20.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTShellRRootViewController.h"
#import "ZSTLogUtil.h"
#import "ZSTUtils.h"
#import "ZSTShell.h"
#import "ZSTGlobal+F3.h"
#import "ZSTShellRSpeedBar.h"
#import "ZSTF3ClientAppDelegate.h"
#import "ZSTF3Engine+ShellR.h"
#import "ElementParser.h"
#import "ZSTModuleManager.h"
#import "ZSTUtils.h"
#import "ZSTWebViewController.h"
#import "ZSTShellRLayoutUtil.h"
#import "ZSTMineViewController.h"

@interface ZSTShellRRootViewController ()

@end

@implementation ZSTShellRRootViewController

@synthesize application;
@synthesize rootView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (IS_IOS_7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    UIImageView *bgImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]];
    bgImg.frame = CGRectMake(0, 0, 320, 480+(iPhone5?88:0));
    bgImg.userInteractionEnabled = YES;
    bgImg.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:bgImg];
    [bgImg release];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(pushViewController)
                                                 name: NotificationName_PushViewController
                                               object: nil];
    
    
    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed: @"framework_top_bg.png"];
    self.navigationItem.titleView = [ZSTUtils logoView];
    
    [self.engine getShellRAD];
    
    self.speedBarScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, (480 - 20 - 49)+(iPhone5?88:0))];
    self.speedBarScrollView.delegate = self;
    self.speedBarScrollView.showsHorizontalScrollIndicator = NO;
    self.speedBarScrollView.showsVerticalScrollIndicator = NO;
    self.speedBarScrollView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.speedBarScrollView];

    
    self.carouselView = [[ZSTShellRCarouselView alloc] initWithFrame:CGRectMake(0, 0, 320, 160)];
    self.carouselView.carouselDataSource = self;
    self.carouselView.carouselDelegate = self;
    [self.speedBarScrollView addSubview:self.carouselView];
    
    [self imageButtonElementsForView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.rightBarButtonItem = [self initWithSubviews];
}

- (UIBarButtonItem *) initWithSubviews
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(mineAction:) forControlEvents:UIControlEventTouchUpInside];
    
    btn.layer.cornerRadius = btn.frame.size.width / 2.0;
    btn.layer.borderColor = [UIColor clearColor].CGColor;
    btn.layer.masksToBounds = YES;
    
    NSString *path = nil;
    UIImage *image = nil;
    NSDictionary *dic = [ZSTF3Preferences shared].avaterName;
    
    if (![ZSTF3Preferences shared].loginMsisdn || [ZSTF3Preferences shared].loginMsisdn.length == 0) {
        
        [ZSTF3Preferences shared].loginMsisdn = @"";
    }
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0) {
        
        image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
    } else {
        
        if (dic || [dic isKindOfClass:[NSDictionary class]]) {
            path = [dic valueForKey:[ZSTF3Preferences shared].loginMsisdn];
        }
        
        if (path && path.length > 0) {
            
            NSString *homePath = NSHomeDirectory();
            NSString *imgTempPath = [path substringFromIndex:[path rangeOfString:@"tmp"].location];
            NSString *finalPath = [NSString stringWithFormat:@"%@/%@",homePath,imgTempPath];
            NSData *reader = [NSData dataWithContentsOfFile:finalPath];
            image = [UIImage imageWithData:reader];
            
            if (!image) {
                
                image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
            }
            
        } else {
            
            image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
        }
    }
    
    [btn setImage:image forState:UIControlStateNormal];
    
    //暂时解决btn图片不能显示的问题
    UIImageView *iv = [[UIImageView alloc] initWithFrame:btn.imageView.frame];
    iv.image = image;
    [btn insertSubview:iv aboveSubview:btn.imageView];
    
    return [[[UIBarButtonItem alloc] initWithCustomView:btn] autorelease];
}


- (void)imageButtonElementsForView
{
    ZSTShellRLayoutUtil *lay = [ZSTShellRLayoutUtil sharedShellRLayoutUtil];
    //获得布局信息
   NSArray *frames = [lay frameArray];
    CGFloat  pageHight = [lay pageHight];
    
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module_shellr_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSArray *itemElements = [root selectElements:@"ImageButton Item"];
    
     for (int i = 0; i < [itemElements count]; i++) {
        
         Element *itemElement = [[itemElements objectAtIndex:i] retain];
         NSString *btnName = [[itemElement selectElement:@"Title"] contentsText];
         int moduleID = [[[itemElement selectElement:@"ModuleId"] contentsNumber] intValue];
         NSNumber *moduleType = [[itemElement selectElement:@"ModuleType"] contentsNumber];
         NSString *titleColor = [[itemElement selectElement:@"TitleColor"] contentsText];
         if (titleColor == nil || [titleColor length] == 0 || [titleColor isEqualToString:@""]) {
             titleColor = @"#000000";
         }
       
         NSDictionary *paramDic = [NSDictionary dictionary];
         if (moduleID == 12 || moduleID == 24) {
             NSString *interfaceUrl = [[itemElement selectElement:@"InterfaceUrl"] contentsText];
             paramDic = [NSDictionary dictionaryWithObjectsAndKeys:btnName,@"Title",moduleType,@"type",interfaceUrl,@"InterfaceUrl",titleColor,@"TitleColor", nil];
         }else{
             paramDic = [NSDictionary dictionaryWithObjectsAndKeys:btnName,@"Title",moduleType,@"type",titleColor,@"TitleColor", nil];
         }
         
         ModuleFrame * frame = [frames objectAtIndex:i%10];
#ifdef DEBUG
         NSLog(@"--%f,--%f,--%f,--%f",frame.pointX,frame.pointY,frame.width,frame.height);
#endif
         
         ZSTShellRSpeedBar *bar = [[[ZSTShellRSpeedBar alloc] initWithFrame:CGRectMake(frame.pointX , 160 + frame.pointY + i/10 * pageHight , frame.width, frame.height)] autorelease];
         bar.backgroundColor = [UIColor clearColor];
         bar.speedBtn.tag = moduleID;
         
         bar.delegate = self;
         NSString *normalImagePath = [NSString stringWithFormat:@"module_shellr_icon%d_n.png", i+1];
         NSString *selectedImagePath = [NSString stringWithFormat:@"module_shellr_icon%d_p.png", i+1];
         
         [bar configSpeedBarNormalImage:ZSTModuleImage(normalImagePath)
                          selectedImage:ZSTModuleImage(selectedImagePath)
                                  param:paramDic];
         
         [self.speedBarScrollView addSubview:bar];
         
         self.speedBarScrollView.contentSize = CGSizeMake(320, CGRectGetMaxY(bar.frame) + 8);
     }
    [parser release];
}

- (void)mineAction:(id)sender
{
    ZSTMineViewController *controller = [[ZSTMineViewController alloc] initWithNibName:@"ZSTMineViewController" bundle:nil];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

- (void)pushViewController
{
    ZSTF3Preferences *per = [ZSTF3Preferences shared];
    UIViewController *controller = per.pushViewController;
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    per.isInPush = YES;
    
}

- (void)popViewController {
    
    ZSTF3Preferences *per = [ZSTF3Preferences shared];
    per.isInPush = NO;
     [self.navigationController popViewControllerAnimated:YES];
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return (1 << UIInterfaceOrientationPortrait);
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIViewController *)rootViewController;
{
    return self;
}

#pragma mark ZSTShellRSpeedBarDelegate

- (void)zstShellRSpeedBar:(ZSTShellRSpeedBar *)speedBar withParam:(NSDictionary *)param
{
    NSMutableDictionary *moduleParams = [NSMutableDictionary dictionaryWithDictionary:param];
    
    ZSTModule *module = [[ZSTModuleManager shared] findModule:speedBar.speedBtn.tag];
    if (module) {
        UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:speedBar.speedBtn.tag withOptions:moduleParams];
        if (controller) {
            controller.hidesBottomBarWhenPushed = YES;
            controller.navigationItem.titleView = [ZSTUtils titleViewWithTitle:speedBar.titleLabel.text];
            controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}


#pragma mark ZSTF3EngineShellBDelegate

- (void)getShellRADDidSucceed:(NSArray *)ADArray
{
    self.ADArray = [ADArray retain];
    [self.carouselView reloadData];
}
- (void)getShellRADDidFailed:(int)resultCode
{
    [TKUIUtil alertInView:self.view withTitle:@"广告暂无数据！" withImage:nil];
}


#pragma mark - ZSTShellRCarouselViewDataSource

- (NSInteger)numberOfViewsInCarouselView:(ZSTShellRCarouselView *)newsCarouselView;
{
    return [self.ADArray count];
}

- (NSDictionary *)carouselView:(ZSTShellRCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index
{
    if ([self.ADArray  count] != 0) {
        return [self.ADArray  objectAtIndex:index];
    }
    return nil;
}

#pragma mark - ZSTShellRCarouselViewDelegate

- (void)carouselView:(ZSTShellRCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;
{
    NSString *urlPath = [[self.ADArray objectAtIndex:index] safeObjectForKey:@"ad_linkurl"];
    if (urlPath == nil || [urlPath length] == 0 || [urlPath isEmptyOrWhitespace]) {
        return;
    }
    
    ZSTWebViewController *webViewController = [[ZSTWebViewController alloc] init];
    [webViewController setURL:urlPath];
     webViewController.type = self.moduleType;
    webViewController.hidesBottomBarWhenPushed = YES;
    webViewController.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
    [self.navigationController pushViewController:webViewController animated:NO];
    [webViewController release];
}

- (void)dealloc
{
	[super dealloc];
}

@end
