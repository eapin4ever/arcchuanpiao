//
//  ZSTShellRLayoutUtil.m
//  ShellR
//
//  Created by LiZhenQu on 14-6-23.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTShellRLayoutUtil.h"

#define FixedPitch         4
#define Initial_PointX     4
#define Item_Hight         98
#define Item_Large_Width   154
#define Item_Small_width   75

static ZSTShellRLayoutUtil* sharedShellRLayoutUtil;

@implementation ZSTShellRLayoutUtil

+(ZSTShellRLayoutUtil *) sharedShellRLayoutUtil
{
    @synchronized(self){
        if (!sharedShellRLayoutUtil) {
            sharedShellRLayoutUtil = [[ZSTShellRLayoutUtil alloc]init];
        }
    }
    return sharedShellRLayoutUtil;
}

-(id)init
{
    self = [super init];
    if (self) {
        _frameArray = [[NSMutableArray alloc]init];
        [self pagelayout];
    }
    return self;
}

- (void) pagelayout
{
    float currPointX = 4.0f;
    float currPointY = 4.0f;
    
    for (int i = 0; i < 10; i ++) {
        ModuleFrame *frame = [[ModuleFrame alloc] init];
        
        switch (i) {
            case 0:
            {
                frame.pointX = currPointX;
                frame.pointY = currPointY;
                frame.width = Item_Large_Width;
                frame.height = Item_Hight;
                currPointX = frame.pointX + frame.width + FixedPitch;
                currPointY = frame.pointY;
            }
                break;
            case 1:
            {
                frame.pointX = currPointX;
                frame.pointY = currPointY;
                frame.width = Item_Large_Width;
                frame.height = Item_Hight;
                currPointX = Initial_PointX;
                currPointY += frame.height + FixedPitch;
            }
                break;
            case 2:
            {
                frame.pointX = currPointX;
                frame.pointY = currPointY;
                frame.width = Item_Small_width;
                frame.height = Item_Hight;
                currPointX += frame.width + FixedPitch;
                currPointY = frame.pointY;
            }
                break;
            case 3:
            {
                frame.pointX = currPointX;
                frame.pointY = currPointY;
                frame.width = Item_Small_width;
                frame.height = Item_Hight;
                currPointX += frame.width + FixedPitch;
                currPointY = frame.pointY;
            }
                break;
            case 4:
            {
                frame.pointX = currPointX;
                frame.pointY = currPointY;
                frame.width = Item_Large_Width;
                frame.height = Item_Hight;
                currPointX = Initial_PointX;
                currPointY += frame.height + FixedPitch;
            }
                break;
            case 5:
            {
                frame.pointX = currPointX;
                frame.pointY = currPointY;
                frame.width = Item_Large_Width;
                frame.height = Item_Hight;
                currPointX += frame.width + FixedPitch;
                currPointY = frame.pointY;
            }
                break;
            case 6:
            {
                frame.pointX = currPointX;
                frame.pointY = currPointY;
                frame.width = Item_Large_Width;
                frame.height = Item_Hight;
                currPointX = Initial_PointX;
                currPointY += frame.height + FixedPitch;
            }
                break;
            case 7:
            {
                frame.pointX = currPointX;
                frame.pointY = currPointY;
                frame.width = Item_Large_Width;
                frame.height = Item_Hight;
                currPointX += frame.width + FixedPitch;
                currPointY = frame.pointY;
            }
                break;
            case 8:
            {
                frame.pointX = currPointX;
                frame.pointY = currPointY;
                frame.width = Item_Small_width;
                frame.height = Item_Hight;
                currPointX += frame.width + FixedPitch;
                currPointY = frame.pointY;
            }
                break;
            case 9:
            {
                frame.pointX = currPointX;
                frame.pointY = currPointY;
                frame.width = Item_Small_width;
                frame.height = Item_Hight;
                currPointX = Initial_PointX;
                currPointY += frame.height;
            }
                break;
                
            default:
                break;
        }
        _pageHight = currPointY;
        [_frameArray addObject:frame];
        [frame release];
    }
}

@end

//自定义对象类型的Frame
@implementation ModuleFrame
@end