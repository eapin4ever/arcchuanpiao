//
//  ZSTShellRRootViewController.h
//  ShellR
//
//  Created by LiZhenQu on 14-6-20.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTModule.h"
#import "ZSTModuleDelegate.h"
#import "ZSTDao.h"
#import "ZSTShellRCarouselView.h"
#import "ZSTShellRSpeedBar.h"

@interface ZSTShellRRootViewController : UIViewController<ZSTModuleDelegate,ZSTShellRCarouselViewDataSource,ZSTShellRCarouselViewDelegate,ZSTShellRSpeedBarDelegate,UIScrollViewDelegate>

@property (strong)ZSTShellRCarouselView *carouselView;
@property (strong)NSArray *ADArray;
@property (strong,nonatomic) UIScrollView *speedBarScrollView;

@end
