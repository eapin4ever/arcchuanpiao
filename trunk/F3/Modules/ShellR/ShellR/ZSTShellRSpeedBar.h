//
//  ZSTShellBSpeedBar.h
//  shellB
//
//  Created by xuhuijun on 13-1-10.
//
//

#import <UIKit/UIKit.h>

@class ZSTShellRSpeedBar;

@protocol ZSTShellRSpeedBarDelegate <NSObject>

- (void)zstShellRSpeedBar:(ZSTShellRSpeedBar *)speedBar withParam:(NSDictionary *)param;

@end


@interface ZSTShellRSpeedBar : UIView

@property(strong)UILabel *titleLabel;
@property(strong)UIButton *speedBtn;
@property(strong,nonatomic)NSDictionary *moduleParams;


@property(strong)id<ZSTShellRSpeedBarDelegate>delegate;

- (void)configSpeedBarNormalImage:(UIImage *)normalImage
                    selectedImage:(UIImage *)selectedImage
                            param:(NSDictionary *)param;


@end
