//
//  ZSTShellBSpeedBar.m
//  shellB
//
//  Created by xuhuijun on 13-1-10.
//
//

#import "ZSTShellRSpeedBar.h"
#import "ZSTUtils.h"

@implementation ZSTShellRSpeedBar

- (void)dealloc
{
    [_speedBtn release];
    [_titleLabel release];
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.speedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.speedBtn.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        [self.speedBtn addTarget:self action:@selector(selectSpeedBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.speedBtn];

        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(-4, 59+8, 70, 21)];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.titleLabel.font = [UIFont systemFontOfSize:11];
        self.titleLabel.textColor = [UIColor colorWithRed:66/255 green:66/255 blue:66/255 alpha:1];
//        [self addSubview:self.titleLabel];
    }
    return self;
}


- (void)configSpeedBarNormalImage:(UIImage *)normalImage
                    selectedImage:(UIImage *)selectedImage
                            param:(NSDictionary *)param
{
    [self.speedBtn setBackgroundImage:normalImage forState:UIControlStateNormal];
    [self.speedBtn setBackgroundImage:selectedImage forState:UIControlStateHighlighted];
    [self.titleLabel setText:[param safeObjectForKey:@"Title"]];
    self.titleLabel.textColor = [ZSTUtils colorFromHexColor:[param safeObjectForKey:@"TitleColor"]];
    self.moduleParams = param;
}


- (void)selectSpeedBtn:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(zstShellRSpeedBar:withParam:)]) {
        [self.delegate zstShellRSpeedBar:self withParam:self.moduleParams];
    }
}


@end
