//
//  ZSTShakeACollectViewController.m
//  ShakeA
//
//  Created by xuhuijun on 13-7-15.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTShakeACollectViewController.h"
#import "ZSTDao+ShakeA.h"
#import "ZSTShakeACell.h"
#import "ZSTShakeAContentViewController.h"
#import "ZSTF3Engine+ShakeA.h"
#import "ZSTUtils.h"

@interface ZSTShakeACollectViewController ()

@end

@implementation ZSTShakeACollectViewController
@synthesize dataArray,shakeAtableView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"我的", nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];

    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.frame.size.height) style:UITableViewStylePlain];
    self.shakeAtableView = tableView;
    self.shakeAtableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.shakeAtableView.dataSource = self;
    self.shakeAtableView.delegate = self;
    self.shakeAtableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.shakeAtableView];
    
    _pageIndex = 1;
    [self loadDataForPage:_pageIndex];
}

- (void)loadDataForPage:(int)pageIndex
{
    self.dataArray = (NSMutableArray *)[self.dao getShakeAList];
    [self.shakeAtableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dataArray.count ? [self.dataArray count] : 1;

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if ([self.dataArray count] == 0) {
        tableView.scrollEnabled = NO;
        static NSString *identifier = @"ZSTShakeAEmptyCell";
        ZSTShakeAEmptyCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[[ZSTShakeAEmptyCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.introduceLabel.text = NSLocalizedString(@"还没有中奖历史哦～" , nil);
        return cell;
    }
    
    TKCellBackgroundView *custview = [[[TKCellBackgroundView alloc] init] autorelease];
    custview.fillColor = [UIColor colorWithWhite:0.98 alpha:1];
    custview.borderColor = [UIColor colorWithWhite:0.88 alpha:1];
    custview.shadowColor = [UIColor whiteColor];
    
    if(([tableView numberOfRowsInSection:indexPath.section]-1) == 0){
        custview.position = CustomCellBackgroundViewPositionSingle;
    }
    else if(indexPath.row == 0){
        custview.position = CustomCellBackgroundViewPositionTop;
    }
    else if (indexPath.row == ([tableView numberOfRowsInSection:indexPath.section]-1)){
        custview.position  = CustomCellBackgroundViewPositionBottom;
    }
    else{
        custview.position = CustomCellBackgroundViewPositionMiddle;
    }
    
    static NSString *cellIdentifier = @"CollectionCell";
    ZSTShakeACell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        cell = [[[ZSTShakeACell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    if ([self.dataArray count]) {
        
        NSDictionary *dic = [self.dataArray objectAtIndex:indexPath.row];
        cell.backgroundView = custview;
        [cell configCell:dic index:indexPath.row];
        return cell;
    }

    return nil;
}


#pragma mark -------UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row >= self.dataArray.count) //更多按钮
    {
        return 44;
    }
    return 44;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.dataArray count]) {
        ZSTShakeAContentViewController *viewController = [[[ZSTShakeAContentViewController alloc] init] autorelease];
        viewController.hidesBottomBarWhenPushed = YES;
        viewController.newsArr = self.dataArray;
        [self.navigationController pushViewController:viewController animated:YES];
        [viewController.actionView.collectBtn setTitle:NSLocalizedString(@"已收藏", @"") forState:UIControlStateNormal];
        viewController.actionView.collectBtn.enabled = NO;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.dataArray) {
        ZSTShakeMessageInfo *currentShakeInfo = [ZSTShakeMessageInfo ShakeMessageInfoWithdic:[self.dataArray objectAtIndex:indexPath.row]];
        [self.dataArray removeObjectAtIndex:indexPath.row];
        if ([self.dataArray count] == 0) {
            [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"ShakeArecordid"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        if(editingStyle == UITableViewCellEditingStyleDelete && [self.dataArray count] != 0) {
        [tableView deleteRowsAtIndexPaths: [NSArray arrayWithObject:indexPath] withRowAnimation: UITableViewRowAnimationFade];
        }else{
            [tableView reloadData];
        }
        [self.dao deleteShakeA:currentShakeInfo.recordid];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
