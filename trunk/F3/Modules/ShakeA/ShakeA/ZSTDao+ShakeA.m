//
//  ZSTDao+ShakeA.m
//  ShakeA
//
//  Created by xuhuijun on 13-7-15.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTDao+ShakeA.h"
#import "ZSTSqlManager.h"
#import "ZSTUtils.h"
#import "TKUtil.h"

#define CREATE_TABLE_CMD(moduleType) [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [ShakeA_List_%@] (\
                                                                  ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                  recordid VARCHAR DEFAULT '', \
                                                                  shakeid  VARCHAR DEFAULT '', \
                                                                   wapurl VARCHAR DEFAULT '', \
                                                                  imageUrl VARCHAR DEFAULT '', \
                                                                  title VARCHAR DEFAULT '' \
                                                                   );", @(moduleType)]
@implementation ZSTDao (ShakeA)

- (void)createTableIfNotExistForShakeAModule
{
    [ZSTSqlManager executeSqlWithSqlStrings:CREATE_TABLE_CMD(self.moduleType)];

}

- (BOOL)addShakeAID:(NSString*)shakeid
         imageUrl:(NSString*)imageUrl
        wapurl:(NSString*)wapurl
         title:(NSString*)title
         recordid:(NSString*)recordid

{
    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO ShakeA_List_%ld (recordid ,shakeid, imageUrl,wapurl, title) VALUES (?,?,?,?,?)", (long)self.moduleType],
                    [TKUtil wrapNilObject:recordid],
                    [TKUtil wrapNilObject:shakeid],
                    [TKUtil wrapNilObject:imageUrl],
                    [TKUtil wrapNilObject:wapurl],
                    [TKUtil wrapNilObject:title]];
    return success;
}

- (NSArray *)getShakeAList
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, recordid, shakeid, imageUrl, wapurl,title FROM ShakeA_List_%ld", (long)self.moduleType];
    NSArray *results = [ZSTSqlManager executeQuery:querySql];
    return results;
}


- (BOOL)deleteShakeA:(NSString*)recordid
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM ShakeA_List_%ld WHERE recordid = ?", (long)self.moduleType];
    if (![ZSTSqlManager executeUpdate:deleteSQL, recordid]) {
        
        return NO;
    }
    return YES;
}

@end
