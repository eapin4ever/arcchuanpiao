//
//  ZSTShakeViewController.m
//  Shake
//
//  Created by xuhuijun on 13-6-28.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTShakeAViewController.h"
#import "SoundEffect.h"
#import "ZSTF3Engine+ShakeA.h"
#import "ZSTShakeAContentViewController.h"
#import "ZSTGlobal+ShakeA.h"
#import "ZSTShakeAFolder.h"
#import "ZSTShakeACollectViewController.h"
#import "ZSTDao+ShakeA.h"
#import "ZSTShakeALinkViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ZSTShakeAViewController ()
{
    UIButton       *bigShakeBtn;
    UIButton       *soundBtn;
//    TKAsynImageView *asynImageView;
    UIImageView *asynImageView;
    BOOL            loading;
    SoundEffect    *shakeSound;
    SoundEffect    *successSound;
    SoundEffect    *failedSound;
    UIView     *pointView;
    UIImageView *pointImageView;
    UIImageView *bigIV;
    UIImageView *soundIV;


}
@end

@implementation ZSTShakeAViewController
@synthesize currentShakeDic;

- (void)moduleApplication:(ZSTModuleApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [application.dao createTableIfNotExistForShakeAModule];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(BOOL)canBecomeFirstResponder
{
    return YES;
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self becomeFirstResponder];
}

- (void) viewWillAppear:(BOOL)animated
{
    [self resignFirstResponder];
    
    [super viewWillAppear:animated];
    asynImageView.hidden = YES;
    pointView.hidden = YES;
    bigShakeBtn.hidden = NO;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImageView *bgImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 460-44+(iPhone5?88:0))];
    bgImgView.backgroundColor = [UIColor clearColor];
    bgImgView.image = ZSTModuleImage(@"module_shakea_bg.png");
    [self.view addSubview:bgImgView];
    
    bigIV = [[UIImageView alloc] initWithFrame:CGRectMake(90, 104+(iPhone5?44:0), 127, 163)];
    bigIV.image = ZSTModuleImage(@"module_shakea_bigBtn.png");
    [self.view addSubview:bigIV];
    
    bigShakeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [bigShakeBtn setImage:ZSTModuleImage(@"module_shakea_bigBtn.png") forState:UIControlStateNormal];
    [bigShakeBtn setImage:ZSTModuleImage(@"module_shakea_bigBtn.png") forState:UIControlStateSelected];
    [bigShakeBtn setImage:ZSTModuleImage(@"module_shakea_bigBtn.png") forState:UIControlStateHighlighted];

    bigShakeBtn.frame = CGRectMake(90, 104+(iPhone5?44:0), 127, 163);
    [bigShakeBtn addTarget:self action:@selector(shakeAction:) forControlEvents:UIControlEventTouchUpInside];
    bigShakeBtn.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:bigShakeBtn];
    
    
    self.navigationItem.rightBarButtonItem =  [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"我的", @"") target:self selector:@selector (collectAction)];
    
    soundIV = [[UIImageView alloc] initWithFrame:CGRectMake(320-47, 40, 47, 40)];
    
    soundBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:ShakeAMusicStatus]) {
        soundIV.image = ZSTModuleImage(@"module_shakea_music_n.png");
//        [soundBtn setImage:ZSTModuleImage(@"module_shakea_music_n.png") forState:UIControlStateNormal];
    }else{
        soundIV.image = ZSTModuleImage(@"module_shakea_music_p.png");
//        [soundBtn setImage:ZSTModuleImage(@"module_shakea_music_p.png") forState:UIControlStateNormal];
    }
    
    soundBtn.frame = CGRectMake(320-47, 40, 47, 40);
    [soundBtn addTarget:self action:@selector(soundAction:) forControlEvents:UIControlEventTouchUpInside];
    soundBtn.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:soundBtn];
    
//    asynImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(91, 114+(iPhone5?44:0), 149, 149)] autorelease];
    asynImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(91, 114+(iPhone5?44:0), 149, 149)] autorelease];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
    [asynImageView addGestureRecognizer:tap];
//    asynImageView.adjustsImageWhenHighlighted = NO;
//
//    asynImageView.asynImageDelegate = self;
//    asynImageView.adorn = [ZSTModuleImage(@"module_shakea_cell_img_frame.png") stretchableImageWithLeftCapWidth:4 topCapHeight:6];
//    asynImageView.imageInset = UIEdgeInsetsMake(3, 4, 5, 4);
//    asynImageView.defaultImage = ZSTModuleImage(@"module_shelld_scoll_pic.png");
    [self.view addSubview:asynImageView];
    asynImageView.hidden = YES;
    
    shakeSound = [[SoundEffect alloc] initWithContentsOfFile:ZSTPathForModuleBundleResource(@"module_shakea_sound_male.mp3")];
    
    successSound = [[SoundEffect alloc] initWithContentsOfFile:ZSTPathForModuleBundleResource(@"module_shakea_match.mp3")];
    
    failedSound = [[SoundEffect alloc] initWithContentsOfFile:ZSTPathForModuleBundleResource(@"module_shakea_nomatch.mp3")];
    
    loading = NO;
    
    //结果View
    pointView = [[UIView alloc] initWithFrame:CGRectMake(47.5, 80+(iPhone5?44:0), 225, 280)];
    //胶带
    UIImageView *tapeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(76.5, 0, 72, 27)];
    tapeImageView.image = ZSTModuleImage(@"module_shakea_messaeTape.png");
    //啥也没摇到。。。。
    pointImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 12, 225, 253)];
    pointImageView.image = ZSTModuleImage(@"module_shakea_noMessage.png");
    [pointView addSubview:pointImageView];
    [pointView addSubview:tapeImageView];
    [self.view addSubview:pointView];
    pointView.hidden = YES;

    
}

- (void)soundAction:(UIButton *)sender
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:ShakeAMusicStatus]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:ShakeAMusicStatus];
        soundIV.image = ZSTModuleImage(@"module_shakea_music_p.png");
        //        [soundBtn setImage:ZSTModuleImage(@"module_shakea_music_p.png") forState:UIControlStateNormal];
    }else{
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:ShakeAMusicStatus];
        soundIV.image = ZSTModuleImage(@"module_shakea_music_n.png");
        //        [soundBtn setImage:ZSTModuleImage(@"module_shakea_music_n.png") forState:UIControlStateNormal];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void)motionBegan:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    
	if (motion == UIEventSubtypeMotionShake )
	{
        if(!loading)
        {
            [self shakeAction:nil];
        }
	}
}

- (void)motionCancelled:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    
}


- (void)shakeAnimation
{
    [ZSTShakeAFolder openFolderWithView:self.view position:CGPointMake(160, 170+(iPhone5?44:0)) openBlock:^(UIView *contentView, CFTimeInterval duration, CAMediaTimingFunction *timingFunction) {
         [self loadData];
    }];
}

- (void)collectAction
{
    ZSTShakeACollectViewController *list = [[ZSTShakeACollectViewController alloc] init];
    list.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:list animated:YES];
}

- (void)shakeAction:(UIButton *)sender
{
    pointView.hidden = YES;
    asynImageView.hidden = YES;
    [self shakeAnimation];
    bigShakeBtn.hidden = YES;
    if (![[NSUserDefaults standardUserDefaults] boolForKey:ShakeAMusicStatus]) {
        [shakeSound play];
    }

}

- (void)loadData
{
    loading = YES;
    [self.engine shakeAGetShakeMessage];
    
}

- (void)shakeSuccess
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:ShakeAMusicStatus]) {
        [successSound play];
    }
}

- (void)shakeFailure
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:ShakeAMusicStatus]) {
        [failedSound play];
    }
}

#pragma mark --f3engineDelegate--

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynimageview
{
    if ([[self.currentShakeDic objectForKey:@"wapurl"] length] != 0 && [self.currentShakeDic objectForKey:@"wapurl"] != nil) {
        ZSTShakeALinkViewController *linkurlVC = [[[ZSTShakeALinkViewController alloc] init] autorelease];
        linkurlVC.hidesBottomBarWhenPushed = YES;
        linkurlVC.linkurl = [self.currentShakeDic objectForKey:@"wapurl"];
        [self.navigationController pushViewController:linkurlVC animated:YES];

    }else{
        ZSTShakeAContentViewController *viewController = [[[ZSTShakeAContentViewController alloc] init] autorelease];
        viewController.hidesBottomBarWhenPushed = YES;
        NSArray *array = [NSArray arrayWithObject:self.currentShakeDic];
        viewController.newsArr = array;
        [self.navigationController pushViewController:viewController animated:YES];
    }
}


- (void)shakeAGetShakeMessageDidSucceed:(NSDictionary*)shakeInfoDic
{
    loading = NO;
    [ZSTShakeAFolder closeCurrentFolder];
    [self shakeSuccess];
    self.currentShakeDic = shakeInfoDic;
    asynImageView.hidden = NO;
//    [asynImageView clear];
//    asynImageView.url = [NSURL URLWithString:[shakeInfoDic objectForKey:@"imgurl"]];
//    [asynImageView loadImage];
    [asynImageView setImageWithURL:[NSURL URLWithString:[shakeInfoDic objectForKey:@"imgurl"]]];
}


- (void)shakeAGetShakeMessageDidFailed:(NSString *)notice
{
    loading = NO;
    [ZSTShakeAFolder closeCurrentFolder];

    [self shakeFailure];

    if ([notice isEqualToString:@"摇奖信息不存在"]) {
        pointImageView.image = ZSTModuleImage(@"module_shakea_noMessage.png");
    }else{
        pointImageView.image = ZSTModuleImage(@"module_shakea_networkerror.png");
    }
    pointView.hidden = NO;

}

- (void)goToDetail:(UITapGestureRecognizer *)tap
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
