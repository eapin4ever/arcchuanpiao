//
//  ZSTShakeACollectViewController.h
//  ShakeA
//
//  Created by xuhuijun on 13-7-15.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTShakeInfo.h"


@interface ZSTShakeACollectViewController : UIViewController<ZSTF3EngineDelegate,UITableViewDataSource, UITableViewDelegate>
{
    int _pageIndex; //当前加载的页数，页面索引
}
@property(nonatomic, retain) NSMutableArray *dataArray;         //数据数组
@property(nonatomic, retain) UITableView *shakeAtableView;   //表视图

@end
