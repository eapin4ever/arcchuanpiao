//
//  ZSTShakeAFolder.h
//  ShakeA
//
//  Created by xuhuijun on 13-7-12.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZSTUIView+Folder.h"
#import "ZSTShakeAContentView.h"

@interface ZSTShakeAFolder : NSObject

+ (void)openFolderWithView:(UIView *)openFolderView position:(CGPoint)position openBlock:(JWFoldersOpenBlock)openBlock ;
+ (void)closeCurrentFolder;

@end
