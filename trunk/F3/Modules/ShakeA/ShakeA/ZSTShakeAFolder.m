//
//  ZSTShakeAFolder.m
//  ShakeA
//
//  Created by xuhuijun on 13-7-12.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTShakeAFolder.h"

@implementation ZSTShakeAFolder

ZSTJWFolders *folders;

+ (void)openFolderWithView:(UIView *)openFolderView position:(CGPoint)position openBlock:(JWFoldersOpenBlock)openBlock 
{
    ZSTShakeAContentView *contentView = [[ZSTShakeAContentView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)];
    folders = [[ZSTJWFolders alloc] init];
    [folders openFolderWithContentView:contentView
                              position:position
                         containerView:openFolderView
                             openBlock:openBlock
     ];
    [folders release];
}

+ (void)closeCurrentFolder
{
    [folders closeCurrentFolder];
}

@end
