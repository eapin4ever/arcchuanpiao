//
//  ZSTShakeInfo.h
//  ShakeA
//
//  Created by xuhuijun on 13-7-8.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTShakeMessageInfo : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *wapurl;
@property (nonatomic, strong) NSString *imgurl;
@property (nonatomic, strong) NSString *recordid;
@property (nonatomic, assign) NSInteger shakeid;

+ (ZSTShakeMessageInfo*)ShakeMessageInfoWithdic:(NSDictionary*)dic;

@end


@interface ZSTShakeMessageContentInfo : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *imgurl;
@property (nonatomic, strong) NSString *linkurl;
@property (nonatomic, strong) NSString *shareurl;
@property (nonatomic, strong) NSString *addtime;
@property (nonatomic, strong) NSArray *content;

+ (ZSTShakeMessageContentInfo*)ShakeMessageContentInfoWithdic:(NSDictionary*)dic;

@end
