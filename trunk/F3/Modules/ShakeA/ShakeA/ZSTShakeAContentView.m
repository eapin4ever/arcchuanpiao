//
//  ZSTShakeAContentView.m
//  ShakeA
//
//  Created by xuhuijun on 13-7-12.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTShakeAContentView.h"

@implementation ZSTShakeAContentView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        bgImageView.image = [ZSTModuleImage(@"module_shakea_folder_bg.png") stretchableImageWithLeftCapWidth:17 topCapHeight:17];
        [self addSubview:bgImageView];
        [bgImageView release];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
