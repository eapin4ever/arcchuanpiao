//
//  ZSTShakeInfo.m
//  ShakeA
//
//  Created by xuhuijun on 13-7-8.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTShakeInfo.h"

@implementation ZSTShakeMessageInfo

- (id)initWithDic:(NSDictionary *)dic
{
    if (![dic isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    self = [super init];
    if (self) {
        self.recordid = [dic safeObjectForKey:@"recordid"];
        self.title = [dic safeObjectForKey:@"title"];
        self.imgurl = [dic safeObjectForKey:@"imgurl"];
        self.wapurl = [dic safeObjectForKey:@"wapurl"];
        self.shakeid = [[dic safeObjectForKey:@"shakeid"] integerValue];
    }
    return self;
}

+ (ZSTShakeMessageInfo*)ShakeMessageInfoWithdic:(NSDictionary*)dic
{
    if (!dic) {
        return nil;
    }
    ZSTShakeMessageInfo* info = [[[ZSTShakeMessageInfo alloc] initWithDic:dic] autorelease];
    return info;
}


- (void) dealloc
{
    self.title = nil;
    self.imgurl = nil;
    [super dealloc];
}

@end

@implementation ZSTShakeMessageContentInfo

- (id)initWithDic:(NSDictionary *)dic
{
    if (![dic isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    self = [super init];
    if (self) {
        self.title = [dic safeObjectForKey:@"title"];
        self.imgurl = [dic safeObjectForKey:@"imgurl"];
        self.linkurl = [dic safeObjectForKey:@"linkurl"];
        self.shareurl = [dic safeObjectForKey:@"shareurl"];
        self.addtime = [dic safeObjectForKey:@"addtime"];
        self.content = [dic safeObjectForKey:@"contents"];
    }
    return self;
}

+ (ZSTShakeMessageContentInfo*)ShakeMessageContentInfoWithdic:(NSDictionary*)dic
{
    if (!dic) {
        return nil;
    }
    ZSTShakeMessageContentInfo* info = [[[ZSTShakeMessageContentInfo alloc] initWithDic:dic] autorelease];
    return info;
}


- (void) dealloc
{
    self.title = nil;
    self.imgurl = nil;
    self.linkurl = nil;
    self.shareurl = nil;
    self.addtime = nil;
    self.content = nil;
    [super dealloc];
}

@end