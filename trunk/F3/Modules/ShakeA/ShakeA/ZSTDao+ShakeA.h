//
//  ZSTDao+ShakeA.h
//  ShakeA
//
//  Created by xuhuijun on 13-7-15.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTDao.h"

@interface ZSTDao (ShakeA)

- (void)createTableIfNotExistForShakeAModule;


- (BOOL)addShakeAID:(NSString*)shakeid
           imageUrl:(NSString*)imageUrl
             wapurl:(NSString*)wapurl
              title:(NSString*)title
         recordid:(NSString*)recordid;


- (NSArray *)getShakeAList;

- (BOOL)deleteShakeA:(NSString*)shakeAID;

@end
