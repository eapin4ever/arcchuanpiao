//
//  ZSTShakeViewController.h
//  Shake
//
//  Created by xuhuijun on 13-6-28.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTModuleBaseViewController.h"
#import "ZSTShakeInfo.h"

@interface ZSTShakeAViewController : ZSTModuleBaseViewController <TKAsynImageViewDelegate>

@property(nonatomic,retain) NSDictionary *currentShakeDic;

@end
