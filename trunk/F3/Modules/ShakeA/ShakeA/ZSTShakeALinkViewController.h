//
//  ZSTShakeALinkViewController.h
//  ShakeA
//
//  Created by xuhuijun on 13-7-16.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTWebViewController.h"

@interface ZSTShakeALinkViewController : ZSTWebViewController
@property (nonatomic, retain) NSString *linkurl;

@end
