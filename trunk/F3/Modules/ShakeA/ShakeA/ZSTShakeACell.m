//
//  ZSTShakeACell.m
//  ShakeA
//
//  Created by xuhuijun on 13-7-15.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTShakeACell.h"



@implementation ZSTShakeAEmptyCell

@synthesize introduceLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        introduceLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 240, 270)];
        introduceLabel.numberOfLines = 1;
        introduceLabel.textAlignment = NSTextAlignmentCenter;
        introduceLabel.textColor = RGBCOLOR(150, 150, 150);
        introduceLabel.backgroundColor = [UIColor clearColor];
        introduceLabel.font = [UIFont systemFontOfSize:18];
        [self.contentView addSubview:introduceLabel];
    }
    return self;
}

- (void)setIntroduce:(NSString *)introduce
{
    self.introduceLabel.text = introduce;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    CGContextRef cr = UIGraphicsGetCurrentContext();
    [[UIColor redColor] set];
    [[UIColor clearColor] set];
    CGContextFillRect(cr, self.bounds);
    
}
- (void)dealloc
{
    TKRELEASE(introduceLabel);
    [super dealloc];
}

@end

@implementation ZSTShakeACell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        imageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(10, 5, 44, 34)] autorelease];
        imageView.adjustsImageWhenHighlighted = NO;
        imageView.adorn = [ZSTModuleImage(@"module_shakea_cell_img_frame.png") stretchableImageWithLeftCapWidth:4 topCapHeight:6];
        imageView.imageInset = UIEdgeInsetsMake(3, 4, 5, 4);
        [self.contentView addSubview:imageView];
        
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 5, 220, 34)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textColor = [UIColor grayColor];
        titleLabel.font = [UIFont systemFontOfSize:16];
        [self.contentView addSubview:titleLabel];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCell:(NSDictionary *)dic index:(NSUInteger)index
{
    titleLabel.text = [dic safeObjectForKey:@"title"];
    [imageView clear];
    imageView.url = [NSURL URLWithString:[dic safeObjectForKey:@"imageUrl"]];
    [imageView loadImage];
}

@end
