//
//  ZSTShakeAFounctionView.m
//  ShakeA
//
//  Created by xuhuijun on 13-7-15.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTShakeAFounctionView.h"

@implementation ZSTShakeAFounctionView
@synthesize delegate;
@synthesize collectBtn;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        dismissBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        dismissBtn.frame = self.frame;
        dismissBtn.backgroundColor  = [UIColor clearColor];
        [dismissBtn addTarget:self action:@selector(dismissAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:dismissBtn];
    
        bgView = [[UIImageView alloc] initWithFrame:CGRectMake(213, 5, 103, 77)];
        bgView.userInteractionEnabled = YES;
        bgView.backgroundColor = [UIColor blackColor];
        bgView.alpha = 0.85;
        bgView.tag = 100;
        CALayer * layer = [bgView layer];
        [layer setMasksToBounds:YES];
        [layer setCornerRadius:8.0];
        [layer setBorderWidth:1.0];
        [layer setBorderColor:[[UIColor clearColor] CGColor]];
        [self addSubview:bgView];
                
        shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        shareBtn.frame = CGRectMake(0, 0, 103, 38);
        [shareBtn setTitle:NSLocalizedString(@"分  享",nil) forState:UIControlStateNormal];
        shareBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        shareBtn.backgroundColor = [UIColor clearColor];
        [shareBtn addTarget:self action:@selector(shareBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:shareBtn];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 38, 103, 1)];
        imageView.image = ZSTModuleImage(@"module_shakea_horizontalLine.png");
        [bgView addSubview:imageView];
        [imageView release];
        
        collectBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [collectBtn setTitle:NSLocalizedString(@"收  藏",nil) forState:UIControlStateNormal];
        collectBtn.frame = CGRectMake(0, 39, 103, 38);
        collectBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        collectBtn.backgroundColor = [UIColor clearColor];
        [collectBtn addTarget:self action:@selector(collectBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:collectBtn];
        
    }
    return self;
}

- (void)dismissAction:(UIButton *)sender
{
    self.hidden = YES;
    if ([self.delegate respondsToSelector:@selector(shakeAFounctionViewDidDismiss)]) {
        [self.delegate shakeAFounctionViewDidDismiss];
    }
}


- (void)shareBtnAction:(UIButton *)sender
{
    self.hidden = YES;

    if ([self.delegate respondsToSelector:@selector(shakeAFounctionViewShareBtnDidSelect:)]) {
        [self.delegate shakeAFounctionViewShareBtnDidSelect:sender];
    }
}

- (void)collectBtnAction:(UIButton *)sender
{
    self.hidden = YES;

    if ([self.delegate respondsToSelector:@selector(shakeAFounctionViewCollectBtnDidSelect:)]) {
        [self.delegate shakeAFounctionViewCollectBtnDidSelect:sender];
    }
}


- (void)dealloc
{
    [bgView release];
    [super dealloc];
}

@end
