//
//  ZSTF3Engine+ShakeA.m
//  ShakeA
//
//  Created by xuhuijun on 13-7-8.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTF3Engine+ShakeA.h"
#import "ZSTGlobal+ShakeA.h"
#import "ZSTCommunicator.h"
#import "ZSTShakeInfo.h"

@implementation ZSTF3Engine (ShakeA)


- (void)shakeAGetShakeMessage
{
    [[ZSTCommunicator shared] openAPIPostToPath:[ShakeAGetShakeMessage stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:nil
                                         target:self
                                       selector:@selector(shakeAGetShakeMessageResponse:userInfo:)
                                       userInfo:nil
                                 longConnection:NO];
}

- (void)shakeAGetShakeMessageResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        
        id dic = [NSDictionary dictionaryWithDictionary:response.jsonResponse];
        if (![dic isKindOfClass:[NSDictionary class]]) {
            dic = [NSDictionary dictionary];
        }

        if ([self isValidDelegateForSelector:@selector(shakeAGetShakeMessageDidSucceed:)]) {
            
            [self.delegate shakeAGetShakeMessageDidSucceed:dic];
        }
    } else {
        NSString *notice = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(shakeAGetShakeMessageDidFailed:)]) {
            [self.delegate shakeAGetShakeMessageDidFailed:notice];
        }
    }
}


- (void)shakeAGetMessageContentWithShakeid:(NSUInteger)shakeid UserInfo:(id)anUserInfo
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[NSNumber numberWithInteger:shakeid] forKey:@"shakeid"];
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys: anUserInfo, @"userInfo",  nil];

    [[ZSTCommunicator shared] openAPIPostToPath:[ShakeAGetMessageContent stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(shakeAGetMessageContentResponse:userInfo:)
                                       userInfo:userInfo
                                 longConnection:NO];
}

- (void)shakeAGetMessageContentResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        ZSTShakeMessageContentInfo * shakeContentInfo = [ZSTShakeMessageContentInfo ShakeMessageContentInfoWithdic:response.jsonResponse];

        if ([self isValidDelegateForSelector:@selector(shakeAGetMessageContentDidSucceed:UserInfo:)]) {
            
            [self.delegate shakeAGetMessageContentDidSucceed:shakeContentInfo UserInfo:[userInfo safeObjectForKey:@"userInfo"]];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(shakeAGetMessageContentDidFailed:UserInfo:)]) {
            [self.delegate shakeAGetMessageContentDidFailed:response.resultCode UserInfo:[userInfo safeObjectForKey:@"userInfo"]];
        }
    }
}

@end
