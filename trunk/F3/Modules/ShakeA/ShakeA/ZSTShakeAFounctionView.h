//
//  ZSTShakeAFounctionView.h
//  ShakeA
//
//  Created by xuhuijun on 13-7-15.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol ZSTShakeAFounctionViewDelegate <NSObject>

- (void)shakeAFounctionViewDidDismiss;
- (void)shakeAFounctionViewShareBtnDidSelect:(UIButton *)sender;
- (void)shakeAFounctionViewCollectBtnDidSelect:(UIButton *)sender;

@end

@interface ZSTShakeAFounctionView : UIView
{
    UIImageView *bgView;
    UIButton *dismissBtn;
    UIButton *shareBtn;
    UIButton *collectBtn;

}

@property (nonatomic,assign)id<ZSTShakeAFounctionViewDelegate>delegate;
@property (nonatomic,retain)UIButton *collectBtn;

@end
