//
//  ZSTShakeACell.h
//  ShakeA
//
//  Created by xuhuijun on 13-7-15.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKAsynImageView.h"


@interface ZSTShakeAEmptyCell : UITableViewCell

@property (nonatomic ,retain) UILabel *introduceLabel;

- (void)setIntroduce:(NSString *)introduce;

@end

@interface ZSTShakeACell : UITableViewCell 

{
    TKAsynImageView *imageView;
    UILabel *titleLabel;
}

- (void)configCell:(NSDictionary *)dic index:(NSUInteger)index;

@end
