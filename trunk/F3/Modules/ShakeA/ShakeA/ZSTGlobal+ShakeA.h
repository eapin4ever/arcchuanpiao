//
//  ZSTGlobal+ShakeA.h
//  ShakeA
//
//  Created by xuhuijun on 13-7-8.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <Foundation/Foundation.h>


#define  ShakeABaseURL @"http://mod.pmit.cn/shakea"

#define ShakeAGetShakeMessage ShakeABaseURL @"/GetShakeMessage"  //获取结果
#define ShakeAGetMessageContent ShakeABaseURL @"/GetMessageContent" //获取详情

#define ShakeAMusicStatus  @"ShakeAMusicStatus"  //摇一摇音乐状态
