//
//  ZSTShakeALinkViewController.m
//  ShakeA
//
//  Created by xuhuijun on 13-7-16.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTShakeALinkViewController.h"

@interface ZSTShakeALinkViewController ()

@end

@implementation ZSTShakeALinkViewController
@synthesize linkurl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    [self setURL:linkurl];
    self.type = self.moduleType;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
