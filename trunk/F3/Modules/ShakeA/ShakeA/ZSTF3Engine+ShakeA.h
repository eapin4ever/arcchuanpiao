//
//  ZSTF3Engine+ShakeA.h
//  ShakeA
//
//  Created by xuhuijun on 13-7-8.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTF3Engine.h"
#import "ZSTShakeInfo.h"


@protocol ZSTF3EngineShakeADelegate <ZSTF3EngineDelegate>

@optional

- (void)shakeAGetShakeMessageDidSucceed:(ZSTShakeMessageInfo*)shakeInfo;
- (void)shakeAGetShakeMessageDidFailed:(NSString *)notice;


- (void)shakeAGetMessageContentDidSucceed:(ZSTShakeMessageContentInfo*)shakeContentInfo UserInfo:(id)anUserInfo;
- (void)shakeAGetMessageContentDidFailed:(int)resultFailed UserInfo:(id)anUserInfo;

@end


@interface ZSTF3Engine (ShakeA)


/**
 *	@brief	摇一摇结果
 *
 */

- (void)shakeAGetShakeMessage;


/**
 *	@brief	点击结果获取详情
 *  shakeid 摇一摇结果id
 */

- (void)shakeAGetMessageContentWithShakeid:(NSUInteger)shakeid UserInfo:(id)anUserInfo;


@end
