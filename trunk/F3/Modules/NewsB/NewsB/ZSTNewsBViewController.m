//
//  ZSTNewsBViewController.m
//  NewsB
//
//  Created by xuhuijun on 13-7-23.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTNewsBViewController.h"
#import "ZSTF3Engine+NewsB.h"
#import "ZSTGlobal+NewsB.h"
#import "ZSTNewsBTableViewCell.h"
#import "ZSTDao+NewsB.h"
#import "ZSTNewsBContentViewController.h"
#import "ZSTWebViewController.h"
#import "TKUIUtil.h"

#define intervalTime 1   //小时

@interface ZSTNewsBViewController ()<ZSTNewsBCarouselViewCellDelegate>
{
    NSMutableDictionary *_timeDic;//计时字典
    BOOL _isFirstLoading;
    NSInteger categoryid;
    NSString *_newsTitle;
}
@property (nonatomic, retain) NSMutableArray *carouseles;

@end

@implementation ZSTNewsBViewController

@synthesize carouseles;

- (void)moduleApplication:(ZSTModuleApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [application.dao createTableIfNotExistForNewsBModule];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)loadDataForPage:(int)pageIndex
{
    [self.engine getNewsB:@(categoryid).stringValue size:NewsBPageSize pageindex:pageIndex];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    for (UIView *sv in self.navigationItem.titleView.subviews) {
        if ([sv isKindOfClass:[UILabel class]])
        {
            _newsTitle = ((UILabel *)sv).text;
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _pageIndex = 1;
    _isFirstLoading = YES;
    
    [self addObserver:self forKeyPath:@"categories" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    [self addObserver:self forKeyPath:@"carouseles" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    self.categories = [NSMutableArray arrayWithArray:[self.dao getNewsBCategories]];
    if ([self.categories count]) {
        self.dataArray = [NSMutableArray arrayWithArray:[self.dao
                                                          newsBAtPage:1
                                                          pageCount:10
                                                          category:[[[self.categories objectAtIndex:0] safeObjectForKey:@"categoryid"] integerValue]]];
    }
    
    [self.view newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkIndicatorView) {
        
        [self.engine getNewsBCategory];
    }];
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSIndexSet *indexes = [change objectForKey:NSKeyValueChangeIndexesKey];
    if ([keyPath isEqualToString:@"categories"]) {
        switch ( [[change objectForKey:NSKeyValueChangeKindKey] intValue]) {
            default:
                assert(NO);
            case NSKeyValueChangeSetting: {
                [self.tableView reloadData];
            } break;
            case NSKeyValueChangeInsertion: {
                [self.tableView insertRowsAtIndexPaths:[self indexPathsForSection:1 rowIndexSet:indexes] withRowAnimation:UITableViewRowAnimationNone];
                [self.tableView flashScrollIndicators];
            } break;
            case NSKeyValueChangeRemoval: {
                [self.tableView deleteRowsAtIndexPaths:[self indexPathsForSection:1 rowIndexSet:indexes] withRowAnimation:UITableViewRowAnimationNone];
                [self.tableView flashScrollIndicators];
            } break;
            case NSKeyValueChangeReplacement: {
                [self.tableView reloadRowsAtIndexPaths:[self indexPathsForSection:1 rowIndexSet:indexes] withRowAnimation:UITableViewRowAnimationNone];
            } break;
        }
    } else if ([keyPath isEqualToString:@"carouseles"]) {
        [self.tableView reloadData];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:@"categories" context:nil];
    [self removeObserver:self forKeyPath:@"carouseles" context:nil];
    
    [super dealloc];
}

#pragma mark - KVO

- (NSArray *)indexPathsForSection:(NSUInteger)section rowIndexSet:(NSIndexSet *)indexSet
{
    NSMutableArray *    indexPaths;
    NSUInteger          currentIndex;
    
    assert(indexSet != nil);
    
    indexPaths = [NSMutableArray array];
    assert(indexPaths != nil);
    currentIndex = [indexSet firstIndex]; 
    while (currentIndex != NSNotFound) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:currentIndex inSection:section]];
        currentIndex = [indexSet indexGreaterThanIndex:currentIndex];
    }
    return indexPaths;
}


- (void)updateListDataAtIndex:(NSInteger)index
{
    NSInteger categoryID= [[[self.categories objectAtIndex:index] safeObjectForKey:@"categoryid"] integerValue];
    categoryid = categoryID;
    _hasMore = ([self.dao newsBCountOfCategory:categoryID] >= 10);
    self.carouseles = [NSMutableArray arrayWithArray:[self.dao getBroadcastNewsB:categoryid]];
    self.dataArray = [NSMutableArray arrayWithArray:[self.dao newsBAtPage:1 pageCount:10 category:categoryID]];
}

#pragma mark - ColumnBarDataSource

- (NSInteger)numberOfTabsInColumnBar:(ColumnBar *)columnBar //tab的数量
{
    return [self.categories count];
}

- (NSString *)columnBar:(ColumnBar *)columnBar titleForTabAtIndex:(int)index // tab的名称
{
    return [[self.categories objectAtIndex:index] safeObjectForKey:@"categoryname"];

}

#pragma mark - ColumnBarDelegate

- (BOOL)compareTimerkey:(NSString *)key timeDic:(NSMutableDictionary *)timerDic
{
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:timerDic];
    NSDate *compareDate = [dic safeObjectForKey:key];
    if (compareDate == nil || [[compareDate description] length] == 0) {
        [dic setValue:[[NSDate date] dateByAddingHours:intervalTime] forKey:key];//第一次计时
        [_timeDic removeAllObjects];
        [_timeDic release];
        _timeDic = [[NSMutableDictionary dictionaryWithDictionary:dic] retain];
        return YES;
    }else {
        if ([compareDate isEqualToDate:[NSDate date]] || [compareDate isEarlierThanDate:[NSDate date]]) {
            [dic removeObjectForKey:key];
            [dic setValue:[[NSDate date] dateByAddingHours:intervalTime] forKey:key];//第n次计时
            [_timeDic removeAllObjects];
            [_timeDic release];
            _timeDic = [[NSMutableDictionary dictionaryWithDictionary:dic] retain];
            return YES;
        }else {
            return NO;
        }
    }
}

- (void)columnBar:(ColumnBar *)columnBar didSelectedTabAtIndex:(NSInteger)index
{
    self.dataArray = [NSMutableArray array];
    self.carouseles = [NSMutableArray array];


    [self updateListDataAtIndex:index];
    [self doneLoadingData];

    if (!_isFirstLoading) {
        
        BOOL isAutoRefreshTime = NO;
        isAutoRefreshTime = [self compareTimerkey:[[self.categories objectAtIndex:index] objectForKey:@"categoryid"] timeDic:_timeDic];
        if (isAutoRefreshTime) {
            
            [self.engine getNewsBBroadcast:[NSString stringWithFormat:@"%@",@(categoryid)]];

            //此处检查当前网络连接，如果是wifi则自动更新，非wifi就不自动更新//还没有做
            [self performSelector:@selector(aotuLoadData)
                       withObject:nil
                       afterDelay:0.3];
        }
    }
}



#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if ([self.categories count] && [self.carouseles count] && [self.dataArray count]) {
        return 2;
    }else if ([self.categories count])
    {
        return 1;
    }else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([self.carouseles count] && indexPath.section == 0) {
        return 160;
    }else if (indexPath.row >= self.dataArray.count && [self.dataArray count]){//更多按钮
        return 44;
    }
    return 74;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中后的反显颜色即刻消失

    NSInteger row = [indexPath row];
    ZSTNewsBVO *currentnvo = [ZSTNewsBVO voWithDic:[self.dataArray objectAtIndex:row]];
    if (currentnvo.linkUrl.length > 0) {
        
        ZSTWebViewController *webView = [[ZSTWebViewController alloc] init];
        webView.titleString = _newsTitle;
        [webView setURL:currentnvo.linkUrl];
        webView.hidesBottomBarWhenPushed = YES;
         webView.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
        [self.navigationController pushViewController:webView animated:YES];
        [webView release];
        
    } else {
    
        ZSTNewsBContentViewController *content = [[ZSTNewsBContentViewController alloc] init];
        content.newsArr = self.dataArray;
        content.hidesBottomBarWhenPushed = YES;
        content.selectedIndex = row;
        content.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
        [self.navigationController pushViewController:content animated:YES];
        
//        content.hTableView.contentOffset = CGPointMake(320 * row, 0);
        [content release];
    }
}

- (void)carouselViewCell:(CarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index
{
    NSUInteger row = index;
    
    NSString *urlPath = [[self.carouseles objectAtIndex:index] safeObjectForKey:@"linkurl"];
    if (urlPath.length > 0) {
        
        ZSTWebViewController *webView = [[ZSTWebViewController alloc] init];
        [webView setURL:urlPath];
        webView.hidesBottomBarWhenPushed = YES;
        webView.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
        [self.navigationController pushViewController:webView animated:YES];
        [webView release];
    } else {

        ZSTNewsBContentViewController *content = [[ZSTNewsBContentViewController alloc] init];
        content.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
        content.newsArr = self.carouseles;
        content.hidesBottomBarWhenPushed = YES;
        content.selectedIndex = row;
        [self.navigationController pushViewController:content animated:YES];
//    content.hTableView.contentOffset = CGPointMake(320 * row, 0);
        [content release];
    }
}


#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (_hasMore)
    {
        //有更多按钮
        if ([self.carouseles count]) {
            return section?[self.dataArray count]+1:1;
        }else{
            return self.dataArray.count + 1;  
        }
    }else {
        
        if ([self.carouseles count]) {
            return section?[self.dataArray count]:1;
        }else{
            return self.dataArray.count;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if ([self.carouseles count]) {
        
        if (indexPath.row == self.dataArray.count && indexPath.section == 1 && [self.dataArray count])
        {
            return super.moreCell;
        }
    }else{
        if (indexPath.row == self.dataArray.count && [self.dataArray count])
        {
            return super.moreCell;
        }
    }
    
    
    TKCellBackgroundView *custview = [[[TKCellBackgroundView alloc] init] autorelease];
    custview.fillColor = [UIColor colorWithWhite:0.98 alpha:1];
    custview.borderColor = [UIColor colorWithWhite:0.96 alpha:1];
    custview.shadowColor = [UIColor whiteColor];
    
    if(([tableView numberOfRowsInSection:indexPath.section]-1) == 0){
        custview.position = CustomCellBackgroundViewPositionSingle;
    }
    else if(indexPath.row == 0){
        custview.position = CustomCellBackgroundViewPositionTop;
    }
    else if (indexPath.row == ([tableView numberOfRowsInSection:indexPath.section]-1)){
        custview.position  = CustomCellBackgroundViewPositionBottom;
    }
    else{
        custview.position = CustomCellBackgroundViewPositionMiddle;
    }
    
    
    if (indexPath.section == 0 && [self.carouseles count]) {
        static NSString *newsCellIdentifier = @"ZSTECACarouselViewCell";
        ZSTNewsBCarouselViewCell *cell = [tableView dequeueReusableCellWithIdentifier:newsCellIdentifier];
        if (cell == nil) {
            cell = [[[ZSTNewsBCarouselViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:newsCellIdentifier] autorelease];
            cell.carouselViewCellDelegate = self;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if ([self.carouseles count]) {
            [cell configCell:self.carouseles];
            return cell;
        }
        
    } else{
        static NSString *cellIdentifier = @"cell";
        ZSTNewsBTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        if (!cell) {
            cell = [[[ZSTNewsBTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier] autorelease];
        }
        UIImageView *selectView = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_newsb_cell_p.png")];
        selectView.frame = CGRectMake(0, 0, 320, 80);
        cell.selectedBackgroundView = selectView;
//        cell.backgroundView = custview;

        [selectView release];
        
        if ([self.dataArray count]) {
            NSDictionary *dic = [self.dataArray objectAtIndex:indexPath.row];
            [cell configCell:dic index:indexPath.row];
        }
        return cell;

    }
    return nil;
}

#pragma mark F3EngineDelegate

- (void)getCategoryDidSucceed:(NSArray *)categories
{
    [self.view removeNetworkIndicatorView];
    self.categories = [NSMutableArray arrayWithArray:categories];
    if ([self.categories count] == 1) {
        self.topbar.hidden = YES;
    }
    _isFirstLoading = NO;
    [self.topbar reloadData];
    [self.topbar selectTabAtIndex:0];

    [self reloadView];
}

- (void)getCategoryDidFailed:(id)notice
{
     [self.view refreshFailed];
}

- (void)getBroadcastDidSucceed:(NSArray *)broadcast
{
    self.carouseles = [NSMutableArray arrayWithArray:broadcast];
}
- (void)getBroadcastDidFailed:(id)notice
{
    [TKUIUtil alertInView:self.view withTitle:@"获取广告失败" withImage:nil];
}

- (void)getNewsBListDidSucceed:(NSArray *)newsBList hasMore:(BOOL)hasmore isRefresh:(BOOL)isRefresh
{
    _hasMore = hasmore;
    
    if(isRefresh)
    {
        self.dataArray = [NSMutableArray arrayWithArray:newsBList];
    }
    else
    {
        _pageIndex ++;

        [self.dataArray addObjectsFromArray:newsBList];
    }
    

    [self doneLoadingData];
}

- (void)getNewsBListDidFailed:(id)notice
{
    [self doneLoadingData];
}



@end
