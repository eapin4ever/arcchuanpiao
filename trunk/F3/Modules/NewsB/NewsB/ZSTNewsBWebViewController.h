//
//  ZSTNewsBWebViewController.h
//  NewsB
//
//  Created by xuhuijun on 13-8-2.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTWebViewController.h"

@interface ZSTNewsBWebViewController : ZSTWebViewController
@property (nonatomic ,retain) NSURL *webUrl;
@end
