//
//  NewsVO.m
//  F3Engine
//
//  Created by admin on 12-7-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTNewsBVO.h"

@implementation ZSTNewsBVO

@synthesize msgid;
@synthesize categoryid;
@synthesize title;
@synthesize source;
@synthesize iconurl;
@synthesize addtime;
@synthesize ordernum;
@synthesize isread;


+ (id)voWithDic:(NSDictionary *)dic
{
    return [[[self alloc] initWithDic: dic] autorelease];
}

- (id)initWithDic:(NSDictionary *)dic
{
    if( (self=[super init])) {
        self.msgid =  [[dic safeObjectForKey:@"msgid"] intValue];
        self.categoryid = [[dic safeObjectForKey:@"categoryid"] intValue];
        self.title = [dic safeObjectForKey:@"title"];
        self.source = [dic safeObjectForKey:@"source"];
        self.iconurl = [dic safeObjectForKey:@"iconurl"];
        self.addtime = [dic safeObjectForKey:@"addtime"];
        self.ordernum = [[dic safeObjectForKey:@"ordernum"] intValue];
        self.isread = [[dic safeObjectForKey:@"isread"] intValue];
        self.linkUrl = [dic safeObjectForKey:@"linkurl"];

    }
    
    return self;
}

- (void)dealloc
{
    self.title = nil;
    self.source = nil;
    self.iconurl = nil;
    self.addtime = nil;
    self.linkUrl = nil;
    [super dealloc];
}

@end
