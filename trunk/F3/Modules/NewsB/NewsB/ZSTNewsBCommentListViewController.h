//
//  ZSTCommentListViewController.h
//  infob
//
//  Created by xuhuijun on 12-11-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine+NewsB.h"
#import "ZSTNewsBVO.h"

@protocol ZSTNewsBCommentCellDelegate <NSObject>

- (void)commentCellDic:(NSDictionary *)dic;

@end

@interface ZSTNewsBCommentCell : UITableViewCell

{
//    TKAsynImageView *avatarImageView;
    UIImageView *avatarImageView;
    UILabel *userName;
    NIAttributedLabel *commentContent;
    UIImageView *timeView;
    UILabel *time;
    UIButton *replayBtn;
    id<ZSTNewsBCommentCellDelegate> delegate;
    NSDictionary *cellInfo;
}

@property (nonatomic ,assign)id<ZSTNewsBCommentCellDelegate> delegate;

- (void)configCell:(NSDictionary*)cellDic cellForRowAtIndexPath:(NSIndexPath *)indexPath;

@end

@interface ZSTNewsBEmptyTableViewCell : UITableViewCell
@property (nonatomic ,retain) UILabel *introduceLabel;

- (void)setIntroduce:(NSString *)introduce;
@end



@interface ZSTNewsBCommentListViewController : UIViewController <UITextViewDelegate,UITableViewDataSource,UITableViewDelegate,ZSTF3EngineNewsBDelegate,TKLoadMoreViewDelegate,EGORefreshTableHeaderDelegate,UIScrollViewDelegate,ZSTNewsBCommentCellDelegate>

{
    EGORefreshTableHeaderView *_refreshHeaderView;
    TKLoadMoreView *_loadMoreView;
    
    BOOL _isRefreshing;
    BOOL _isLoadingMore;
    BOOL _hasMore;
    
    UIButton *replayTitleBtn;
    UILabel *replayTitleLabel;

}
@property (nonatomic, retain) TKPlaceHolderTextView *textView;
@property (nonatomic, retain) UITableView *commentList;
@property (nonatomic, retain) NSMutableArray *commentArray;
@property (nonatomic, retain) ZSTNewsBVO *infob;
@property (nonatomic, retain) NSDictionary *replayInfo;

@end



