//
//  ZSTInfoBEmptyTableCell.h
//  infob
//
//  Created by luobin on 11/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTNewsBEmptyTableCell : UITableViewCell

@property (nonatomic ,retain) UILabel *introduceLabel;

- (void)setIntroduce:(NSString *)introduce;

@end
