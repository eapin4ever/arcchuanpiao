//
//  NewsContentViewController.m
//  F3
//
//  Created by admin on 12-7-19.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTNewsBContentViewController.h"
#import "ZSTDao+NewsB.h"
#import "ZSTNEWSBLocalSubstitutionCache.h"
#import "ZSTNEWSBTKResourceURLProtocol.h"
#import "ZSTNEWSBTKResourceURL.h"
#import "TKUIUtil.h"
#import "ZSTUtils.h"
#import "ZSTModuleManager.h"
#import "ZSTModule.h"
#import "WXApi.h"
#import "ZSTGlobal+F3.h"
#import "ZSTNewsBWebViewController.h"
#import "ZSTNewsBCommentListViewController.h"
#import "BaseNavgationController.h"
#import <PMRepairButton.h>

#define ArticleFontSize   @"ArticleFontSize"
#define LargeFont   @"3"
#define MiddleFont  @"2"
#define NormalFont  @"1"
#define SmallFont   @"0"

@interface ZSTNewsBContentViewController ()
{
    BOOL isfirst;
}

@end

@implementation ZSTNewsBContentViewController

@synthesize hTableView;
@synthesize newsArr;
@synthesize selectedImageView;
@synthesize vo;
@synthesize contentVo;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)dealloc
{
    [self.engine cancelAllRequest];
    self.engine.delegate = nil;
    [self.actionView release];
    self.hTableView = nil;
    self.selectedImageView = nil;
    self.newsArr = nil;
    self.vo = nil;
    [super dealloc];
}

- (void)refreshTap:(UIControl *)sender
{
    //刷新
    TKHorizontalTableViewCell *cell = (TKHorizontalTableViewCell *)sender.superview.superview;
    UIActivityIndicatorView *aiView = (UIActivityIndicatorView *)[cell viewWithTag:100];
    if (![aiView isAnimating]) {
        [aiView startAnimating];
    }
    UIWebView *webView = (UIWebView *)[cell viewWithTag:99];
    UILabel *refreshLabel = (UILabel *)[cell viewWithTag:102];
    UIControl *refreshCtr = (UIControl *)[webView viewWithTag:103];
    [refreshCtr removeFromSuperview];
    refreshLabel.hidden = YES;
    
    NSInteger index = cell.tag - 1000;
    ZSTNewsBVO *currentnvo = [ZSTNewsBVO voWithDic:[self.newsArr objectAtIndex:index]];
    [self.engine getNewsBContent:currentnvo.msgid userInfo:@(index) version:0];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = YES;

}
#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // 设置图片缓存
    //	LocalSubstitutionCache *cache = [[LocalSubstitutionCache alloc] init];
    //	[NSURLCache setSharedURLCache:cache];
    //    [cache release];
    
//    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
    if (IS_IOS_7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    isfirst = YES;
    
     self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"评论" , nil) target:self selector:@selector(openCommentListAction)];
    
    [ZSTNEWSBTKResourceURLProtocol registerProtocol];
    
    self.view.backgroundColor = RGBCOLOR(239, 239, 239);
    hTableView = [[TKHorizontalTableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-43)];
    hTableView.delegate = self;
    hTableView.dataSource = self;
    hTableView.backgroundColor = RGBCOLOR(239, 239, 239);
    hTableView.showsVerticalScrollIndicator = NO;
    hTableView.showsHorizontalScrollIndicator = NO;
    hTableView.pagingEnabled = YES;
    
    [self.view addSubview:hTableView];        

    UIImage *toolbarBackground = [ZSTModuleImage(@"module_newsb_content_toolbar.png") stretchableImageWithLeftCapWidth:1 topCapHeight:40];
    UIImageView *toolbarImageView = [[[UIImageView alloc] initWithImage:toolbarBackground] autorelease];
    toolbarImageView.backgroundColor = [UIColor redColor];
    toolbarImageView.userInteractionEnabled = YES;
    toolbarImageView.frame = CGRectMake(0, self.view.height-43, 320, 43);
    
//    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    PMRepairButton *backBtn = [[PMRepairButton alloc] init];
    backBtn.tag = 300;
    backBtn.frame = CGRectMake(9, 2.5, 62, 38);
    backBtn.imageEdgeInsets = UIEdgeInsetsMake(7.5, 18, 7.5, 18);
    [backBtn addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
    [backBtn setImage:ZSTModuleImage(@"module_news_content_back.png") forState:UIControlStateNormal];
    [toolbarImageView addSubview:backBtn];
    
//    UIButton *fontBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    fontBtn.frame = CGRectMake(CGRectGetMaxX(backBtn.frame)+9*2 ,2.5, 62, 38);
//    fontBtn.imageEdgeInsets = UIEdgeInsetsMake(7.5, 18, 7.5, 18);
//    fontBtn.tag = 301;
//    [fontBtn addTarget:self action:@selector(fontAction) forControlEvents:UIControlEventTouchUpInside];
//    [fontBtn setImage:ZSTModuleImage(@"module_newsb_content_font.png") forState:UIControlStateNormal];
//    [toolbarImageView addSubview:fontBtn];
    
//    UIButton *refreshBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    PMRepairButton *refreshBtn = [[PMRepairButton alloc] init];
    refreshBtn.frame = CGRectMake(CGRectGetMaxX(backBtn.frame)+9*6, 2.5, 62, 38);
    refreshBtn.imageEdgeInsets = UIEdgeInsetsMake(7.5, 18, 7.5, 18);
    refreshBtn.tag = 302;
    [refreshBtn addTarget:self action:@selector(refreshAction:) forControlEvents:UIControlEventTouchUpInside];
    [refreshBtn setImage:ZSTModuleImage(@"module_newsb_content_refresh.png") forState:UIControlStateNormal];
    [toolbarImageView addSubview:refreshBtn];
    

//    UIButton *shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    PMRepairButton *shareBtn = [[PMRepairButton alloc] init];
    shareBtn.tag = 303;
    shareBtn.frame = CGRectMake(CGRectGetMaxX(refreshBtn.frame)+9*6, 2.5, 62, 38);
    shareBtn.imageEdgeInsets = UIEdgeInsetsMake(7.5, 18, 7.5, 18);
    [shareBtn addTarget:self action:@selector(shareAction) forControlEvents:UIControlEventTouchUpInside];
    [shareBtn setImage:ZSTModuleImage(@"module_newsb_content_zhuanfa.png") forState:UIControlStateNormal];
    [toolbarImageView addSubview:shareBtn];

    toolbarImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;

    [self.view addSubview:toolbarImageView];
    
    fontSize = FontSize_Normal;
    [self changeFontSize];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxShareSucceed) name:NotificationName_WXShareSucceed object:nil];

    
    self.actionView = [[ZSTNewsBFontView alloc] initWithFrame:CGRectMake(0 , 0, self.view.width, self.view.height)];
    self.actionView.delegate = self;
    [self.view addSubview:self.actionView];
    self.actionView.hidden = YES;
    
}

#pragma mark- ZSTHHSinaShareControllerDelegate


- (void)shareDidFinish:(ZSTHHSinaShareController *)sinaShareController options:(NSString *)options
{
    [TKUIUtil alertInView:self.view withTitle:options withImage:nil];
}

- (void)shareDidFail:(ZSTHHSinaShareController *)sinaShareController options:(NSString *)options
{
    [TKUIUtil alertInView:self.view withTitle:options withImage:nil];
}
#pragma mark wxShare

- (void)wxShareSucceed
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"微信分享成功", nil) withImage:nil];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 0) {
        NSString* installUrl =  [WXApi getWXAppInstallUrl];
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:installUrl]];
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];

    
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信好友" , nil)]
        || [[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信朋友圈" , nil)]){
        
        if (![WXApi isWXAppInstalled] ||! [WXApi isWXAppSupportApi]) {
            
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:NSLocalizedString(@"提示" , nil)
                                  message:NSLocalizedString(@"您未安装微信，现在安装？" , nil)
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"取消" , nil)
                                  otherButtonTitles:NSLocalizedString(@"安装" , nil), nil];
            alert.tag = 108;
            [alert show];
            return;
        }
        // 发送内容给微信
        WXMediaMessage *message = [WXMediaMessage message];
        [message setThumbImage:[UIImage imageNamed:@"icon.png"]];
        message.title = self.contentVo.title;
        int index = hTableView.contentOffset.x / 320;
        
        if ([[[self.contentVo.contentArr objectAtIndex:0] safeObjectForKey:@"letter"] length] != 0) {
            if ([[[self.contentVo.contentArr objectAtIndex:0] safeObjectForKey:@"letter"] length] >50) {
                message.description = [[[self.contentVo.contentArr objectAtIndex:0] safeObjectForKey:@"letter"] substringToIndex:50];
            }else{
                message.description = [[self.contentVo.contentArr objectAtIndex:0] safeObjectForKey:@"letter"];
            }
        }else{
            
            message.description = NSLocalizedString(@"点击查看详情" , nil);
        }
        
        WXImageObject *ext = [WXImageObject object];
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"icon" ofType:@"png"];
        ext.imageData = [NSData dataWithContentsOfFile:filePath] ;
        message.mediaObject = ext;
        
        WXWebpageObject *webpage = [WXWebpageObject object];
        webpage.webpageUrl = self.contentVo.shareurl;
        message.mediaObject = webpage;
        
        SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
        req.bText = NO;
        req.message = message;
        
        if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信好友" , nil)]) {
            req.scene = WXSceneSession;
        }
        else  if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信朋友圈"  , nil)]){
            req.scene = WXSceneTimeline;
        }
        
        [WXApi sendReq:req];
        
    }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"短信" , nil)]){
        
        Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));
        
        if (messageClass != nil && [MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
            
            picker.body = [NSString stringWithFormat:NSLocalizedString(@"我在“%@”手机客户端，看到了一篇文章《%@》,分享给你,%@", nil),appDisplayName,self.contentVo.title,self.contentVo.shareurl];
            
            picker.messageComposeDelegate = self;
            
            [self presentModalViewController: picker animated:YES];
            
            [picker release];
        } else {
            
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[NSString stringWithFormat:@"sms:"]]];
        }
    }else{
        
        ZSTHHSinaShareController *sinaShare = [[ZSTHHSinaShareController alloc] init];
        sinaShare.delegate = self;
        sinaShare.shareString = [NSString stringWithFormat:NSLocalizedString(@"#分享文章#:%@,%@", nil),self.contentVo.title,self.contentVo.shareurl];

        if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"新浪微博" , nil)]) {
            sinaShare.shareType = sinaWeibo_ShareType;
            sinaShare.navigationItem.title = NSLocalizedString(@"新浪微博", nil);
        }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"QQ空间" , nil)]){
            sinaShare.shareType = QQ_ShareType;
            sinaShare.navigationItem.title = NSLocalizedString(@"QQ空间", nil);
        }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"腾讯微博" , nil)]){
            sinaShare.shareType = TWeibo_ShareType;
            sinaShare.navigationItem.title = NSLocalizedString(@"腾讯微博", nil);
        }
        
        UINavigationController *n = [[UINavigationController alloc] initWithRootViewController:sinaShare];
        [self presentModalViewController:n animated:YES];
        [n release];
        [sinaShare release];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // 直接检测服务器是否绑定成功
    if (result==MessageComposeResultSent) {
        [ZSTUtils showAlertTitle:nil message:NSLocalizedString(@"分享成功!" , nil)];
    }
}

#pragma mark - Actions -------------

- (void)refreshAction:(UIButton *)sender
{
    int index = hTableView.contentOffset.x / 320;
    ZSTNewsBVO *currentnvo = [ZSTNewsBVO voWithDic:[self.newsArr objectAtIndex:index]];
    [self.engine getNewsBContent:currentnvo.msgid userInfo:[NSNumber numberWithInt:index] version:0];
}

- (void)shareAction
{
    UIActionSheet *shareActionSheet = nil;
    NSMutableArray *shareNames = [NSMutableArray array];
    ZSTF3Preferences *pre = [ZSTF3Preferences shared];
    if (pre.SinaWeiBo) {
        [shareNames addObject:NSLocalizedString(@"新浪微博",nil)];
    }
    if (pre.TWeiBo) {
        [shareNames addObject:NSLocalizedString(@"腾讯微博",nil)];
    }
    if (pre.QQ) {
        [shareNames addObject:NSLocalizedString(@"QQ空间",nil)];
    }
    if (pre.WeiXin) {
        [shareNames addObject:NSLocalizedString(@"微信好友",nil)];
        [shareNames addObject:NSLocalizedString(@"微信朋友圈",nil)];
    }
    
    [shareNames addObject:NSLocalizedString(@"短信",nil)];
    
    shareActionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"分享到", nil)
                                                   delegate:self
                                          cancelButtonTitle:nil
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:nil];
    
   for (NSString * title in shareNames) {
        [shareActionSheet addButtonWithTitle:title];
    }
    [shareActionSheet addButtonWithTitle:NSLocalizedString(@"取消",nil)];
    shareActionSheet.cancelButtonIndex = shareActionSheet.numberOfButtons-1;
    shareActionSheet.tag = 101;
    [shareActionSheet showInView:self.view.window];
    [shareActionSheet release];

}


- (void)newsbFontViewDidDismiss
{
    
}

- (void)newsbFontViewSmallBtnDidSelect:(UIButton *)sender
{
    fontSize = FontSize_Normal;
    [self changeFontSize];

}

- (void)newsbFontViewMiddleBtnDidSelect:(UIButton *)sender
{
    fontSize = FontSize_Middle;
    [self changeFontSize];
}

- (void)newsbFontViewLargeBtnDidSelect:(UIButton *)sender
{
    fontSize = FontSize_Large;
    [self changeFontSize];
}



- (void)fontAction
{
    self.actionView.hidden = NO;
    [self.view bringSubviewToFront:self.actionView];
    self.actionView.layer.anchorPoint = CGPointMake(0.3f, 0.9f);
    self.actionView.frame = CGRectMake(0, 0, self.actionView.frame.size.width, self.actionView.frame.size.height);
    self.actionView.transform = CGAffineTransformMakeScale(0.1f, 0.1f);
    [UIView animateWithDuration:0.15f animations:^{
        self.actionView.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.1f animations:^{
            self.actionView.transform = CGAffineTransformMakeScale(0.95f, 0.95f);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.1f animations:^{
                self.actionView.transform = CGAffineTransformMakeScale(1.0f, 1.0f);
            }];
        }];
    }];
}


- (void)changeFontSize
{
    int index = hTableView.contentOffset.x / 320;
    TKHorizontalTableViewCell *cell = [hTableView cellForIndex:index];
    UIWebView *webView = (UIWebView *)[cell viewWithTag:99];
    
    NSString *fontSizeStr = @"1";
    if (fontSize == FontSize_Large) {
        [webView stringByEvaluatingJavaScriptFromString: @"changeFontSize('font_large');" ];
        fontSizeStr = LargeFont;
    } else if (fontSize == FontSize_Middle) {
        [webView stringByEvaluatingJavaScriptFromString: @"changeFontSize('font_middle');" ];
        fontSizeStr = MiddleFont;
    } else if (fontSize == FontSize_Normal) {
        [webView stringByEvaluatingJavaScriptFromString: @"changeFontSize('font_normal');" ];
        fontSizeStr = NormalFont;
    } else {
        [webView stringByEvaluatingJavaScriptFromString: @"changeFontSize('font_small');" ];
        fontSizeStr = SmallFont;
    }
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:fontSizeStr forKey:ArticleFontSize];
    [ud synchronize];
}

- (void)openCommentListAction
{
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0)
    {
        ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
        controller.isFromSetting = YES;
        controller.delegate = self;
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
        }];
        [navicontroller release];
        [controller release];
        
        return;
    }
    ZSTNewsBCommentListViewController *commentList = [[ZSTNewsBCommentListViewController alloc] init];
    commentList.infob = self.vo;
    [self.navigationController pushViewController:commentList animated:YES];
    [commentList release];
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



#pragma mark ---
#pragma UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    if ([request.URL.scheme isEqualToString:@"image"]) {
        
        NSString *url = [request.URL description];
        NSRange range1 = [url rangeOfString:@"///"];
        NSRange range2 = [url rangeOfString:@"///" options:NSCaseInsensitiveSearch range:NSMakeRange(range1.location + 3, url.length - range1.location - 3)];
        
        NSString *offsetY = [request.URL.description substringWithRange:NSMakeRange(range1.location+range1.length, range2.location - range1.location-range1.length)];
        
        NSString *imageUrl = [request.URL.description substringFromIndex:range2.location+range2.length];        
        if (imageUrl!=nil && [imageUrl length]>0) {
            ZSTNEWSBTKResourceURL *resourceURL = [ZSTNEWSBTKResourceURL URLWithString:imageUrl];
            NSString *cachePath = [ZSTNEWSBTKResourceURLProtocol cachePathForResourceURL:resourceURL];
            UIImage *image = [UIImage imageWithContentsOfFile:cachePath];
            if (image) {
                
                self.selectedImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(217, [offsetY intValue], 90, 90)] autorelease];
                self.selectedImageView.imageView.contentMode = UIViewContentModeScaleAspectFill;
                self.selectedImageView.imageView.clipsToBounds = YES;
                self.selectedImageView.backgroundColor = RGBCOLOR(239, 239, 239);
                
                self.selectedImageView.defaultImage = image;
                [self.selectedImageView clear];
                self.selectedImageView.url = [NSURL URLWithString:resourceURL.originalUrl];
                [self.selectedImageView loadImage];
                
                [self.view addSubview:selectedImageView];
                
                selectedImageView.imageView.zoomedSize = [self displayRectForImage:CGSizeMake(self.selectedImageView.imageView.image.size.width, self.selectedImageView.imageView.image.size.height)];
                
                selectedImageView.imageView.wrapInScrollviewWhenZoomed = YES;
                selectedImageView.showLoadingWheel = YES;
                
                [selectedImageView.imageView zoomIn];
                selectedImageView.imageView.zoomDelegate = self;
                selectedImageView.callbackOnSetImage = self;
            }            
            return NO;
        }
    } else if((navigationType == UIWebViewNavigationTypeLinkClicked || navigationType == UIWebViewNavigationTypeFormSubmitted) && [request.URL.scheme caseInsensitiveCompare:@"http"] == NSOrderedSame) {
        ZSTNewsBWebViewController *newsbWeb = [[ZSTNewsBWebViewController alloc] init];
        self.navigationController.navigationBarHidden = NO;
        newsbWeb.webUrl = request.URL;
        newsbWeb.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:newsbWeb animated:YES];
        [newsbWeb release];
        
        return NO;
    }else if (navigationType == UIWebViewNavigationTypeLinkClicked && [request.URL.scheme caseInsensitiveCompare:@"native"] == NSOrderedSame) {
        
        NSDictionary *params = [request.URL.absoluteString queryDictionaryUsingEncoding:NSUTF8StringEncoding];
        NSString *module_id = [params safeObjectForKey:@"native://?module_id"];
        NSString *module_type = [params safeObjectForKey:@"module_type"];
        NSString *title = [params safeObjectForKey:@"title"];
        
        NSMutableDictionary *moduleParams = [NSMutableDictionary dictionary];
        [moduleParams setSafeObject:module_type forKey:@"type"];
        
        ZSTModule *module = [[ZSTModuleManager shared] findModule:[module_id integerValue]];
        if (module) {
            UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:[module_id integerValue] withOptions:moduleParams];
            if (controller) {
                controller.hidesBottomBarWhenPushed = YES;
                controller.navigationItem.titleView = [ZSTUtils titleViewWithTitle:title];
                controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
                [self.navigationController pushViewController:controller animated:YES];      
            }
        }
        
    }
    return YES;
}
#pragma mark HJManagedImageVDelegate
-(void) managedImageSet:(HJManagedImageV*)mi
{
    mi.imageView.contentMode = UIViewContentModeScaleAspectFill;
    mi.imageView.size = [self displayRectForImage:CGSizeMake(self.selectedImageView.imageView.image.size.width, self.selectedImageView.imageView.image.size.height)];
    mi.imageView.center = self.view.superview.superview.center;
}

- (void)zoomWindow:(MTZoomWindow *)zoomWindow didZoomOutView:(UIView *)view {
    self.selectedImageView.alpha = 0;
    [self.selectedImageView removeFromSuperview];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    TKDERROR(@"error == %@", error);
}

- (void)webViewDidStartLoad:(UIWebView *)webView //网页加载时调用
{
    //    [aiView startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView //网页完成加载时调用
{
//    [webView stringByEvaluatingJavaScriptFromString: @"changeFontSize('font_large');" ];
    
    if (isfirst) {
        hTableView.contentOffset = CGPointMake(320*self.selectedIndex, 0);
        isfirst = NO;
    }
}
- (CGSize)displayRectForImage:(CGSize)imageSize {
    CGRect screenRect = TKScreenBounds();
    
    if (imageSize.width > screenRect.size.width) {
        CGFloat height = imageSize.height * screenRect.size.width / imageSize.width;
        CGFloat width = screenRect.size.width;
        if (height > screenRect.size.height) {
            width = width * screenRect.size.height/height;
            height = screenRect.size.height;
        }
        return CGSizeMake(width, height);
    }else if (imageSize.height > screenRect.size.height) {
        CGFloat width = imageSize.width * screenRect.size.height / imageSize.height;
        CGFloat height = screenRect.size.height;
        
        return CGSizeMake(width, height);
    } else {
        return CGSizeMake(imageSize.width, imageSize.height); 
    }
}


- (void)viewDidUnload
{
    //    [webView removeFromSuperview];
    [super viewDidUnload];
    //    [aiView removeFromSuperview];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)templateEngine:(MGTemplateEngine *)engine blockStarted:(NSDictionary *)blockInfo
{
    //	NSLog(@"Started block %@", [blockInfo objectForKey:BLOCK_NAME_KEY]);
}

- (void)templateEngine:(MGTemplateEngine *)engine blockEnded:(NSDictionary *)blockInfo
{
    //	NSLog(@"Ended block %@", [blockInfo objectForKey:BLOCK_NAME_KEY]);
}

- (void)templateEngineFinishedProcessingTemplate:(MGTemplateEngine *)engine
{
    //	NSLog(@"Finished processing template.");
}

- (void)templateEngine:(MGTemplateEngine *)engine encounteredError:(NSError *)error isContinuing:(BOOL)continuing;
{
	NSLog(@"Template error: %@", error);    
}

#pragma mark --
#pragma TKHorizontalTableViewDataSource -------------

- (NSInteger)numberOfRowsInTableView:(TKHorizontalTableView *)tableView {
    return [self.newsArr count];
}

- (TKHorizontalTableViewCell *)tableView:(TKHorizontalTableView *)tableView cellAtIndex:(NSUInteger)index {
    
    TKHorizontalTableViewCell *hCell = [tableView dequeueReusableCell];
    
    if (hCell == nil) {
        hCell = [[[TKHorizontalTableViewCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.height, 320)] autorelease];
        hCell.backgroundColor = RGBCOLOR(239, 239, 239);
        NSInteger height = self.view.frame.size.height-43-(IS_IOS_7?20:0);
        UIWebView *webView = [[[UIWebView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, height)] autorelease];
        webView.tag = 99;
        webView.backgroundColor = RGBCOLOR(239, 239, 239);
        webView.dataDetectorTypes = UIDataDetectorTypeNone;
        webView.opaque = NO;
        webView.delegate = self; 
        //去掉阴影
        for(UIView* subView in [webView subviews])
        {
            if([subView isKindOfClass:[UIScrollView class]]){
                for(UIView* shadowView in [subView subviews])
                {
                    if([shadowView isKindOfClass:[UIImageView class]]){
                        [shadowView setHidden:YES];
                    }
                }
            }
        }
        [hCell addSubview:webView];
        
        UIActivityIndicatorView *aiView = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
        aiView.center = CGPointMake(160, 185);
        aiView.tag = 100;
        [hCell addSubview:aiView];
        
        UILabel *refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 170, 200, 30)];
        refreshLabel.font = [UIFont boldSystemFontOfSize:18];
        refreshLabel.text = NSLocalizedString(@"加载失败，点击重新加载", nil);
        refreshLabel.textAlignment = NSTextAlignmentCenter;
        refreshLabel.textColor = RGBACOLOR(200, 200, 200, 1);
        refreshLabel.backgroundColor = [UIColor clearColor];
        refreshLabel.tag = 102;
        [hCell addSubview:refreshLabel];
        [refreshLabel release];
    }
    
    hCell.tag = index+1000;
    
    //    480-44-37-20
    UIWebView *webView = (UIWebView *)[hCell viewWithTag:99];
    [webView stringByEvaluatingJavaScriptFromString:@"document.body.innerHTML='';"];
    UIActivityIndicatorView *aiView = (UIActivityIndicatorView *)[hCell viewWithTag:100];
    [aiView startAnimating];
    
    ZSTNewsBVO *nvo = [ZSTNewsBVO voWithDic:[self.newsArr objectAtIndex:index]];
    
    [self.engine getNewsBContent:nvo.msgid userInfo:@(index) version:0];
    
    UILabel *refreshLabel = (UILabel *)[hCell viewWithTag:102];
    refreshLabel.hidden = YES;
    
    return hCell;
}

- (void)getNewsBContentDidSucceed:(NSDictionary *)newsContent userInfo:(id)userInfo{
    NSNumber *index = userInfo;        
    ZSTNewsBVO *nvo = [ZSTNewsBVO voWithDic:[self.newsArr objectAtIndex:[index unsignedIntegerValue]]];
    self.vo = nvo;
    [self.dao setNewsB:nvo.msgid isRead:YES];//设置内容为已读
    ZSTNewsBContentVO *contentVO = [ZSTNewsBContentVO voWithDic:newsContent];
    self.contentVo = contentVO;
    
    
    //目前只做图片
    
    NSMutableString *urlStr = [NSMutableString string];
    
    if ([contentVO.contentArr isKindOfClass:[NSArray class]] && [contentVO.contentArr count]>0) {
        
        for (int i=0; i<[contentVO.contentArr count]; i++) {
            NSString *str = [[contentVO.contentArr objectAtIndex:i] safeObjectForKey:@"letter"];
            NSString *formatContent = [self formatArticleContent:str];  
            [urlStr appendString:[NSString stringWithFormat:@"<p>%@</p>",formatContent]];
            
            NSDictionary *fileDic = [contentVO.contentArr objectAtIndex:i];
            NSString *url = [fileDic safeObjectForKey:@"imageurl"];
//            if (i == 0) {
//                
//                if (contentVO.topfigureurl && contentVO.topfigureurl.length > 0) {
//                    
//                    url = contentVO.topfigureurl;
//                }
//            }
            
            if (url && [url length]>0) {
                
                NSString *origUrl = @"";
                ZSTNEWSBTKResourceURL *resourceURL = [ZSTNEWSBTKResourceURL resourceURLWithResource:url originalUrl:origUrl MIMEType:@"image/png"];
                [urlStr appendFormat:@"<span class=\"photo_block\"><span class=\"photo_box\"><img class=\"photo\" src=\"%@\" onload=\"javascript:DrawImage(this)\"/></span>", resourceURL.absoluteString?resourceURL.absoluteString:@""];
                NSString *desc = [fileDic objectForKey:@"imagedesc"];
                if (desc) {
                    [urlStr appendFormat:@"<span class=\"photo_title\">%@</span></span>", desc];
                }
            }
        }
 	}
    
    // Set up template engine with your chosen matcher.
    MGTemplateEngine *engine = [MGTemplateEngine templateEngine];
    [engine setDelegate:self];
    [engine setMatcher:[ICUTemplateMatcher matcherWithTemplateEngine:engine]];    
    
    NSString *templatePath = ZSTPathForModuleBundleResource(@"module_newsb_content_template.html");
    
    NSDictionary *variables = [NSDictionary dictionaryWithObjectsAndKeys:contentVO.title,@"title", contentVO.addtime,@"date" ,urlStr ,@"body", [self getFontSize], @"fontClass", @"", @"theme",contentVO.topfigureurl,@"topfigureurl", nil];
    if ([contentVO.topfigureurl length] == 0 || contentVO.topfigureurl == nil) {
        
        variables = [NSDictionary dictionaryWithObjectsAndKeys:contentVO.title,@"title", contentVO.addtime,@"date" ,urlStr ,@"body", [self getFontSize], @"fontClass", @"", @"theme", nil];
    }
    if ([contentVO.originalurl length] != 0) {
        
        variables = [NSDictionary dictionaryWithObjectsAndKeys:contentVO.title,@"title", contentVO.addtime,@"date" ,urlStr ,@"body", [self getFontSize], @"fontClass", @"", @"theme",contentVO.topfigureurl,@"topfigureurl",contentVO.originalurl,@"originalurl", nil];
    }
    
    NSString *result = [engine processTemplateInFileAtPath:templatePath withVariables:variables];
    
    //不用生成html文件，直接加载字符串
    NSURL *url = [NSURL fileURLWithPath:ZSTPathForModuleBundleResource(@"")];
    
    TKHorizontalTableViewCell *cell = [hTableView cellForIndex:[index intValue]];
    UIActivityIndicatorView *aiView = (UIActivityIndicatorView *)[cell viewWithTag:100];
    if ([aiView isAnimating]) {
        [aiView stopAnimating];
    }
    UIWebView *webView = (UIWebView *)[cell viewWithTag:99];
    [webView loadHTMLString:result baseURL:url];  
    UILabel *refreshLabel = (UILabel *)[cell viewWithTag:102];
    refreshLabel.hidden = YES;
}

- (NSString *)formatArticleContent:(NSString *)content
{
    if (content == nil) {
        return nil;
    }
    
    NSMutableString *contentStr = [NSMutableString string];
    [contentStr appendString:content];
    
    [contentStr replaceOccurrencesOfString:@"\r" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, contentStr.length)];
    [contentStr replaceOccurrencesOfString:@"\n" withString:@"<br />" options:NSCaseInsensitiveSearch range:NSMakeRange(0, contentStr.length)];
    return contentStr;
}

- (NSString *)getFontSize
{
    if (fontSize == FontSize_Normal) {
        return @"font_normal";
    } else if (fontSize == FontSize_Middle) {
        return @"font_middle";
    } else if (fontSize == FontSize_Large) {
        return @"font_large";
    } else {
        return @"font_small";
    }
}

- (void)getNewsBContentDidFailed:(ZSTResponse *)response userInfo:(id)userInfo
{  
    NSInteger selectIndex = [[response.userInfo safeObjectForKey:@"userInfo"] integerValue];
    
    TKHorizontalTableViewCell *cell = [hTableView cellForIndex:selectIndex];
    UIActivityIndicatorView *aiView = (UIActivityIndicatorView *)[cell viewWithTag:100];
    UIWebView *webView = (UIWebView *)[cell viewWithTag:99];
    
    if ([aiView isAnimating]) {
        [aiView stopAnimating];
    }
    
    UILabel *refreshLabel = (UILabel *)[cell viewWithTag:102];
    refreshLabel.hidden = NO;
    
    UIControl *refreshCtr = [[UIControl alloc] initWithFrame:self.view.frame];
    [refreshCtr addTarget:self action:@selector(refreshTap:) forControlEvents:UIControlEventTouchUpInside];
    refreshCtr.tag = 103;
    [webView addSubview:refreshCtr];
    [refreshCtr release];
}



#pragma mark --
#pragma TKHorizontalTableViewDelegate -------------
- (CGFloat)tableView:(TKHorizontalTableView *)tableView widthForCellAtIndex:(NSUInteger)index {
    return self.view.frame.size.width;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (hTableView.contentOffset.x < -20) {
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //放大 分享 收藏 缩小 按钮的tag值 300 301 302 303
    for (int i=300; i<304; i++) {
        UIButton *btn = (UIButton *)[self.view viewWithTag:i];
        btn.enabled = NO;
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //放大 分享 收藏 缩小 按钮的tag值 300 301 302 303
    for (int i=300; i<304; i++) {
        UIButton *btn = (UIButton *)[self.view viewWithTag:i];
        btn.enabled = YES;
    }
}


@end
