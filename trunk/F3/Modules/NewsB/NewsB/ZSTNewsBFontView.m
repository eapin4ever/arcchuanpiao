//
//  ZSTNewsBFontView.m
//  NewsB
//
//  Created by xuhuijun on 13-7-26.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTNewsBFontView.h"

@implementation ZSTNewsBFontView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        dismissBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        dismissBtn.frame = self.frame;
        dismissBtn.backgroundColor  = [UIColor clearColor];
        [dismissBtn addTarget:self action:@selector(dismissAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:dismissBtn];
        
        bgView = [[UIImageView alloc] initWithFrame:CGRectMake(70, 300, 103, 114)];
        bgView.userInteractionEnabled = YES;
        bgView.backgroundColor = [UIColor blackColor];
        bgView.alpha = 0.85;
        bgView.tag = 100;
        CALayer * layer = [bgView layer];
        [layer setMasksToBounds:YES];
        [layer setCornerRadius:8.0];
        [layer setBorderWidth:1.0];
        [layer setBorderColor:[[UIColor clearColor] CGColor]];
        [self addSubview:bgView];

        UIButton *smallBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        smallBtn.frame = CGRectMake(0, 0, 103, 38);
        [smallBtn setTitle:NSLocalizedString(@"大",nil) forState:UIControlStateNormal];
        smallBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        smallBtn.backgroundColor = [UIColor clearColor];
        [smallBtn addTarget:self action:@selector(largeBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:smallBtn];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 38, 103, 1)];
        imageView.image = ZSTModuleImage(@"module_shakea_horizontalLine.png");
        [bgView addSubview:imageView];
        [imageView release];
        
        UIButton *middleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [middleBtn setTitle:NSLocalizedString(@"中",nil) forState:UIControlStateNormal];
        middleBtn.frame = CGRectMake(0, 39, 103, 38);
        middleBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        middleBtn.backgroundColor = [UIColor clearColor];
        [middleBtn addTarget:self action:@selector(middleBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:middleBtn];
        
        UIImageView *imageView2 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 77, 103, 1)];
        imageView2.image = ZSTModuleImage(@"module_shakea_horizontalLine.png");
        [bgView addSubview:imageView2];
        [imageView2 release];
        
        
        UIButton *largeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [largeBtn setTitle:NSLocalizedString(@"小",nil) forState:UIControlStateNormal];
        largeBtn.frame = CGRectMake(0, 78, 103, 38);
        largeBtn.titleLabel.font = [UIFont systemFontOfSize:16];
        largeBtn.backgroundColor = [UIColor clearColor];
        [largeBtn addTarget:self action:@selector(smallBtnAction:) forControlEvents:UIControlEventTouchUpInside];
        [bgView addSubview:largeBtn];
        
    }
    return self;
}

- (void)dismissAction:(UIButton *)sender
{
    self.hidden = YES;
    if ([self.delegate respondsToSelector:@selector(newsbFontViewDidDismiss)]) {
        [self.delegate newsbFontViewDidDismiss];
    }
}


- (void)smallBtnAction:(UIButton *)sender
{
    self.hidden = YES;
    
    if ([self.delegate respondsToSelector:@selector(newsbFontViewSmallBtnDidSelect:)]) {
        [self.delegate newsbFontViewSmallBtnDidSelect:sender];
    }
}
- (void)middleBtnAction:(UIButton *)sender
{
    self.hidden = YES;
    
    if ([self.delegate respondsToSelector:@selector(newsbFontViewMiddleBtnDidSelect:)]) {
        [self.delegate newsbFontViewMiddleBtnDidSelect:sender];
    }
}

- (void)largeBtnAction:(UIButton *)sender
{
    self.hidden = YES;
    
    if ([self.delegate respondsToSelector:@selector(newsbFontViewLargeBtnDidSelect:)]) {
        [self.delegate newsbFontViewLargeBtnDidSelect:sender];
    }
}


- (void)dealloc
{
    [bgView release];
    [super dealloc];
}

@end