//
//  ZSTNewsBTableViewCell.m
//  NewsB
//
//  Created by xuhuijun on 13-7-23.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTNewsBTableViewCell.h"
#import "ZSTUtils.h"
#import "NSDate+NSDateEx.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ZSTNewsBTableViewCell

@synthesize titleLabel,sourceLabel,timeLabel,asynView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        CGRect frame = self.frame;//height = 73
        UILabel *labeltitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, frame.size.width-94, 20)];
        labeltitle.font = [UIFont systemFontOfSize:14];
        labeltitle.backgroundColor = [UIColor clearColor];
        labeltitle.highlightedTextColor = RGBCOLOR(128, 128, 128);
        labeltitle.textColor = RGBCOLOR(78, 78, 78);
        [self.contentView addSubview:labeltitle];
        titleLabel = labeltitle;
        [labeltitle release];
        
//        UILabel *labelDes = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(titleLabel.frame)+5, CGRectGetWidth(titleLabel.frame), 20)];
//        labelDes.font = [UIFont systemFontOfSize:12];
//        labelDes.backgroundColor = [UIColor clearColor];
//        labelDes.highlightedTextColor = RGBCOLOR(128, 128, 128);
//        labelDes.textColor = RGBCOLOR(78, 78, 78);
//        [self.contentView addSubview:labelDes];
//        sourceLabel = labelDes;
//        [labelDes release];
        
        UILabel *labelTime = [[UILabel alloc] initWithFrame:CGRectMake(10, 55+4, CGRectGetWidth(titleLabel.frame), 10)];
        labelTime.font = [UIFont systemFontOfSize:12];
        labelTime.backgroundColor = [UIColor clearColor];
        labelTime.highlightedTextColor = RGBCOLOR(148, 148, 148);
        labelTime.textColor = RGBCOLOR(194, 194, 194);
//        [self.contentView addSubview:labelTime];
        timeLabel= labelTime;
        [labelTime release];
        
//        TKAsynImageView *viewasyn = [[TKAsynImageView alloc] initWithFrame:CGRectMake(246, 5, 64, 64)];
//        viewasyn.adorn = [ZSTModuleImage(@"module_shakea_cell_img_frame.png") stretchableImageWithLeftCapWidth:4 topCapHeight:6];
//        viewasyn.imageInset = UIEdgeInsetsMake(2, 3, 4, 3);
//        viewasyn.adjustsImageWhenHighlighted = NO;
        UIImageView *viewasyn = [[UIImageView alloc] initWithFrame:CGRectMake(246, 5, 64, 64)];
        [self.contentView addSubview:viewasyn];
        asynView = viewasyn;
        [viewasyn release];
        
        UILabel *lineLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 73, 320, 1)];
        lineLabel.backgroundColor = RGBCOLOR(196, 196, 196);
        [self addSubview:lineLabel];
        [lineLabel release];
    }
    return self;
}

- (void)configCell:(NSDictionary *)dic index:(NSUInteger)index
{
    titleLabel.text = [dic safeObjectForKey:@"title"];
//    sourceLabel.text = [dic objectForKey:@"source"];

    NSString *addTime = [dic safeObjectForKey:@"addtime"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *date = [dateFormatter dateFromString:addTime];
//    timeLabel.text = [NSDate dateStringWithDate:date];
    
//    [asynView clear];
//    asynView.url = [NSURL URLWithString:[dic safeObjectForKey:@"iconurl"]];
//    [asynView loadImage];
    [asynView setImageWithURL:[NSURL URLWithString:[dic safeObjectForKey:@"iconurl"]]];
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
//    
//    UIImage *image = ZSTModuleImage(@"module_newsb_cell_n.png") ;
//    [image drawInRect:rect];
    
}
@end


@implementation ZSTNewsBCarouselViewCell

@synthesize carouselView;
@synthesize dataArry;
@synthesize carouselViewCellDelegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //轮播图
        carouselView = [[CarouselView alloc] initWithFrame:CGRectMake(0, 0, 320, 160)];
        carouselView.carouselDataSource = self;
        carouselView.carouselDelegate = self;
        [self.contentView addSubview:carouselView];
    }
    return self;
}
- (void)configCell:(NSArray*)array
{
    [dataArry release];
    self.dataArry = nil;
    self.dataArry = [array retain];
    
    if ([dataArry count] != 0) {
        [carouselView reloadData];
    }
}

#pragma mark - ZSTNewsCarouselViewDataSource

- (NSInteger)numberOfViewsInCarouselView:(CarouselView *)newsCarouselView
{
    return [dataArry count];
}
- (NSDictionary *)carouselView:(CarouselView *)carouselView infoForViewAtIndex:(NSInteger)index
{
    if ([dataArry count] != 0) {
        return [dataArry objectAtIndex:index];
    }
    return nil;
}

#pragma mark - ZSTNewsCarouselViewDelegate

- (void)carouselView:(CarouselView *)theCarouselView didSelectedViewAtIndex:(NSInteger)index
{
    if ([carouselViewCellDelegate respondsToSelector:@selector(carouselViewCell:didSelectedViewAtIndex:)]) {
        [carouselViewCellDelegate carouselViewCell:theCarouselView didSelectedViewAtIndex:index];
    }
}

- (void)dealloc
{
    [carouselView release];
    [dataArry release];
    [super dealloc];
}
@end

