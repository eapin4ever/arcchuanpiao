//
//  ZSTCSAChatViewController.m
//  Service
//
//  Created by xuhuijun  on 12-11-13.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTCSAChatViewController.h"
#import "TKUtil.h"
#import "ZSTF3Engine+CSA.h"
#import "ZSTDao+CSA.h"
#import "ZSTAsynBubbleView.h"
#import "NSDate+NSDateEx.h"
#import "ZSTLoginViewController.h"
#import "BaseNavgationController.h"

@interface ZSTCSAChatViewController ()<LoginDelegate>

@end

@implementation ZSTCSAChatViewController

@synthesize chatList;
@synthesize chatArray;

@synthesize csArray;

@synthesize csInfo;

@synthesize msgarray;


- (void)moduleApplication:(ZSTModuleApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [application.dao csaCreateTableIfNotExistForModule];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(id)init
{
	self = [super init];
	if(self){
		[[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(keyboardWillShow:) 
													 name:UIKeyboardWillShowNotification 
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(keyboardWillHide:)  
													 name:UIKeyboardWillHideNotification 
												   object:nil];	
        
        _showSendingCell = NO;
        
	}
	
	return self;
}

- (void)dealloc
{
//    [self.engine csaNotifyTrans:CSACmdLogout msgIdStr:@""];
    [self.engine cancelAllRequest];
    
    if ([_timer isValid]) {
        [_timer invalidate];
    }
    
    TKRELEASE(_timer);
    TKRELEASE(containerView);
    TKRELEASE(textView);
    TKRELEASE(sendBtn);
    TKRELEASE(columnBar);
    TKRELEASE(csArray);
    
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    currentSelectItem = 0;
    
    isConnected = NO;
    
//    self.view.layer.contents = (id) ZSTModuleImage(@"module_csa_bg.png").CGImage;    
    lastContentOffsetY = 0.f;
     
    self.csArray = [NSMutableArray array];
    self.csArray = [(NSMutableArray *)[self.dao csaGetAllCutomerService] retain];
    columnBar = [[ColumnBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 38)];
    columnBar.dataSource = self;
    columnBar.delegate = self;
    columnBar.topbarType = TopBarType_Slider;
//    columnBar.scrollView.scrollEnabled = NO;
    columnBar.image =  ZSTModuleImage(@"module_csa_top_toolBar_bg.png");
    columnBar.moverImage = ZSTModuleImage(@"module_csa_top_btn_p.png");
    [self.view addSubview:columnBar];

    self.chatArray = [NSMutableArray array];
    self.chatList = [[[UITableView alloc] initWithFrame:CGRectMake(0, 38, self.view.width, 372-38+(iPhone5?88:0)-(IS_IOS_7?0:20)) style:UITableViewStylePlain] autorelease];
    self.chatList.delegate = self;
    self.chatList.dataSource = self;
    self.chatList.backgroundColor = [UIColor clearColor];
    self.chatList.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.chatList.separatorColor = [UIColor clearColor];
    [self.view addSubview:self.chatList];
    
    if ([csArray count] != 0) {
        [columnBar reloadData];
        [columnBar selectTabAtIndex:0];
    }
    
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.height - 44, self.view.width, 44)];
    
	textView = [[TKGrowingTextView alloc] initWithFrame:CGRectMake(6, 5, 235, 36)];
    textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    textView.userInteractionEnabled = YES;
	textView.minNumberOfLines = 1;
	textView.maxNumberOfLines = 3;
	textView.font = [UIFont systemFontOfSize:15.0f];
	textView.delegate = self;
    [textView setbackgroudImage];
    [textView setPlaceholder:NSLocalizedString(@"在此输入您的问题…", nil)];
    textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    textView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:containerView];
    
    UIImage *rawEntryBackground = ZSTModuleImage(@"module_csa_MessageEntryInputField.png");
    UIImage *entryBackground = [rawEntryBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *entryImageView = [[[UIImageView alloc] initWithImage:entryBackground] autorelease];
    entryImageView.frame = CGRectMake(5, 5, 242, 34);
    entryImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    UIImage *rawBackground = ZSTModuleImage(@"module_csa_MessageEntryBackground.png");
    UIImage *background = [rawBackground stretchableImageWithLeftCapWidth:1 topCapHeight:22];
    UIImageView *imageView = [[[UIImageView alloc] initWithImage:background] autorelease];
    imageView.frame = CGRectMake(0, 0, containerView.frame.size.width, containerView.frame.size.height);
    imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    // view hierachy
    [containerView addSubview:imageView];
    [containerView addSubview:entryImageView];
    [containerView addSubview:textView];
   
    UIImage *sendBtnBackground = ZSTModuleImage(@"module_csa_MessageEntrySendButton.png");

    sendBtn = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
//    sendBtn.enabled = NO;
	sendBtn.frame = CGRectMake(containerView.frame.size.width - 70, 6, 68, 34);
    sendBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
	[sendBtn addTarget:self action:@selector(sendMessageAction) forControlEvents:UIControlEventTouchUpInside];
    [sendBtn setTitle:@"发 送" forState:UIControlStateNormal];
    [sendBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sendBtn setBackgroundImage:sendBtnBackground forState:UIControlStateNormal];
	[containerView addSubview:sendBtn];
    containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;

    [self.view newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkIndicatorView) {
        [self.engine csaGetCustomerService];
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0)
    {
        ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
        controller.isFromSetting = YES;
        controller.delegate = self;
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
            
        }];
        
        [navicontroller release];
        [controller release];
        
        return;
    }
}

- (void)loginDidCancel
{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        if (![controller isKindOfClass:[self.rootViewController class]]) {
            
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
    NSNumber *shellId = [NSNumber numberWithInteger:[ZSTF3Preferences shared].shellId];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLoginCancel object:shellId];
}

- (void)loginFinish
{
   
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if (_timer != nil) {
        [_timer invalidate]; 
        TKRELEASE(_timer);
    }
}

- (void)returnTouched
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [self popViewController];
}

- (void)loadCacheMessage
{
    if (csArray == nil || [csArray count] == 0) {
        return;
    }
    self.csInfo = [csArray objectAtIndex:currentSelectItem];
    
    [self startPush];
    [self startCheck];
    
       NSArray *messageList = [self.dao csaGetMessage:[[csInfo safeObjectForKey:@"csid"] intValue] pageNum:0 pageCount:0];
    
    if (messageList==nil || [messageList count] == 0) {
        return;
    }
    for (int i=0; i<[messageList count]; i++) {
        NSDictionary *message = [messageList objectAtIndex:i];
        int isSend = [[message safeObjectForKey:@"issend"] intValue];
        
        if (isSend) {
            
            ZSTAsynBubbleView *bubbleView = [[[ZSTAsynBubbleView alloc] init] autorelease];
            bubbleView.showSendingWheel = NO;
            bubbleView.bubbleModuleType = self.moduleType;
            [bubbleView bubbleView:(NSMutableDictionary *)message from:YES];
            
            [self.chatArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:message, @"message", @"self", @"speaker", bubbleView, @"view", nil]];
        } else {
            ZSTAsynBubbleView *bubbleView = [[[ZSTAsynBubbleView alloc] init] autorelease];
            bubbleView.showSendingWheel = NO;
            bubbleView.bubbleModuleType = self.moduleType;
            [bubbleView bubbleView:(NSMutableDictionary *)message from:NO];
            
            [self.chatArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:message, @"message", @"cs", @"speaker", bubbleView, @"view", nil]];
        }
    }
    
    [self.chatList reloadData];
    [self.chatList scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.chatArray count]-1 inSection:0] 
                         atScrollPosition: UITableViewScrollPositionBottom 
                                 animated:YES];
    



}

- (void)stopCheck
{
    if ([_timer isValid]) {
        [_timer invalidate];
        TKRELEASE(_timer);
    }
}

- (void)startCheck
{
    if (_timer == nil) {
        _timer = [[NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(check) userInfo:nil repeats:YES] retain];
    }
}

- (void)check
{
    if (!isConnected) {
        [self startPush];
    }
}

- (void)startPush
{
    isConnected = YES;
    NSInteger conId = [ZSTF3Preferences shared].conId;
    if (conId <= 0) {
        conId = 1;
    }
    [self.engine cancelAllRequest];
    [self.engine csaPushMessage:[[self.csInfo safeObjectForKey:@"csid"] intValue] conId:conId];
}

#pragma mark - ZSTF3EngineCSADelegate

- (void)csaGetCustomerServiceDidSucceed:(NSArray *)theList
{
    [self.view removeNetworkIndicatorView];
    
    self.csArray = [[NSMutableArray arrayWithArray:theList] retain];
    if ([self.csArray count]) {
        self.csInfo = [self.csArray objectAtIndex:0];
        [columnBar reloadData];
        [columnBar selectTabAtIndex:0];
        [self.engine csaGetMessage:0 csId:0];
    }
}

- (void)csaGetCustomerServiceDidFailed:(int)resultCode
{
    [self.view refreshFailed];
}

- (void)csaGetMessageDidSucceed:(NSArray *)messageList
{
//    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"获取离线消息成功", nil) withImage:nil];
    
    [self startPush];
    
    [self startCheck];
    
    if (messageList && [messageList count]>0) {
        int csid = [[csInfo safeObjectForKey:@"csid"] intValue];
        for (int i=0; i<[messageList count]; i++) {
            NSDictionary *message = [messageList objectAtIndex:i];
            int msgCSID = [[message safeObjectForKey:@"csid"] intValue];
            if (csid != msgCSID) {
                continue;
            }
            
            ZSTAsynBubbleView *bubbleView = [[[ZSTAsynBubbleView alloc] init] autorelease];
            bubbleView.showSendingWheel = NO;
            bubbleView.bubbleModuleType = self.moduleType;
            [bubbleView bubbleView:(NSMutableDictionary *)message from:NO];
            
            [self.chatArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:message, @"message", @"cs", @"speaker", bubbleView, @"view", nil]];
        }
        
        [self.chatList reloadData];
        [self.chatList scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.chatArray count]-1 inSection:0] 
                             atScrollPosition: UITableViewScrollPositionBottom 
                                     animated:YES];
    }
}

- (void)csaGetMessageDidFailed:(int)resultCode
{
    [self startPush];
    
    [self startCheck];
    
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"网络异常，请检查！", nil) withImage:nil];
}

- (void)csaPushMessageDidSucceed:(NSArray *)messageList
{
    NSLog(@"push succeed");
    if (messageList) {
        
        NSMutableString * idStr = [NSMutableString stringWithCapacity:10];
        int csid = [[csInfo safeObjectForKey:@"csid"] intValue];
        for (int i=0; i<[messageList count]; i++) {
            NSDictionary *message = [messageList objectAtIndex:i];
            int msgCSID = [[message safeObjectForKey:@"csid"] intValue];
            if (csid == msgCSID) {
                
                ZSTAsynBubbleView *bubbleView = [[ZSTAsynBubbleView alloc] init];
                bubbleView.showSendingWheel = NO;
                bubbleView.bubbleModuleType = self.moduleType;
                [bubbleView bubbleView:(NSMutableDictionary *)message from:NO];
                
                [self.chatArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:message, @"message", @"cs", @"speaker", bubbleView, @"view", nil]];
            }

            long long int msgid = [[message safeObjectForKey:@"msgid"] longLongValue];
            [idStr appendFormat:@"%lld", msgid];
            if (i < ([messageList count]-1)) {
                [idStr appendString:@","];
            }
        }
        
        [self.engine csaNotifyTrans:CSACmdConfirm msgIdStr:idStr];
        
        [self.chatList reloadData];
        [self.chatList scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.chatArray count]-1 inSection:0] 
                             atScrollPosition: UITableViewScrollPositionBottom 
                                     animated:YES];
    }
    
    [self startPush];
}

- (void)csaPushMessageDidFailed:(int)resultCode
{
    NSLog(@"push failed");
    isConnected = NO;
    if (resultCode == ZSTResultCode_RequestTimedOut) {
        [self startPush];
    }
}

- (void)csaNotifyTransDidSucceed
{
    NSLog(@"csaNotifyTransDidSucceed");
}

- (void)csaNotifyTransDidFailed:(int)resultFailed
{
    NSLog(@"csaNotifyTransDidFailed");
}

#pragma mark - sendMessage

- (void)sendMessageAction
{
    
    NSString *messageStr = textView.text;
    
    if (messageStr == nil)
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"发送内容不能为空", nil) withImage:nil ];
    }else
    {
        [self sendMassage:messageStr];
    }
	textView.text = @"";
    
}

- (void)sendMassage:(NSString *)message
{    
    NSDateFormatter * dateFormatter = [[[NSDateFormatter alloc] init]autorelease];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *date = [NSDate date];
    NSString * addTime = [dateFormatter stringFromDate:date];
    NSString *guid = [NSString stringWithFormat:@"%lld",[[NSNumber numberWithDouble: [date timeIntervalSince1970]] longLongValue]];
    
    NSMutableDictionary *msgContent = [NSMutableDictionary dictionaryWithCapacity:1];
    [msgContent setSafeObject:[self.csInfo safeObjectForKey:@"csid"] forKey:@"csid"];
    [msgContent setSafeObject:message forKey:@"message"];
    [msgContent setSafeObject:[NSString stringWithFormat:@"%@",@"0"] forKey:@"msgtype"];
    [msgContent setSafeObject:@"" forKey:@"iconurl"];
    [msgContent setSafeObject:addTime forKey:@"addtime"];
    [msgContent setSafeObject:guid forKey:@"guid"];
    
    [msgarray addObject:msgContent];
    
    
    ZSTAsynBubbleView *bubbleView = [[ZSTAsynBubbleView alloc] init];
    bubbleView.showSendingWheel = YES;
    bubbleView.bubbleModuleType = self.moduleType;
    
    NSString *csId = [msgContent safeObjectForKey:@"csid"];
    if ([csId isKindOfClass:[NSNull class]]) {
    
        [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"亲,客服系统暂未配置，请联系企业管理员!", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]];
        [TKUIUtil hiddenHUDAfterDelay:2];
        return;
    }

    
    [bubbleView bubbleView:msgContent from:YES];
    
    [self.chatArray addObject:[NSDictionary dictionaryWithObjectsAndKeys:msgContent, @"message", @"self", @"speaker", bubbleView, @"view", nil]];
    
    [self.chatList reloadData];
    [self.chatList scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.chatArray count]-1 inSection:0]
                         atScrollPosition: UITableViewScrollPositionBottom
                                 animated:YES];
    
}


#pragma mark - ZSTCSAColumnBarDelegate

- (void)columnBar:(ColumnBar *)columnBar didSelectedTabAtIndex:(NSInteger)index
{
    currentSelectItem = index; 
    
    [self.chatArray removeAllObjects];
    [self.chatList reloadData];
//    [self.engine cancelAllRequest];
    [self loadCacheMessage];
}

#pragma mark - ZSTCSAColumnBarDataSource

- (NSInteger)numberOfTabsInColumnBar:(ColumnBar *)columnBar
{
    if (self.csArray) {
        return [csArray count];
    } else {
        return 0;
    }
}
//- (int)columnWidthOfTabsInColumnBar:(ColumnBar *)columnBar
//{
//    if (self.csArray && [csArray count]>0) {
//        return 310.0f / [csArray count];
//    } else {
//        return 0;
//    }
//}

- (NSString *)columnBar:(ColumnBar *)columnBar titleForTabAtIndex:(NSInteger)index
{
    return [[csArray objectAtIndex:index] safeObjectForKey:@"duty"];
}

//- (UIImage *)columnBar:(ColumnBar *)columnBar selectImageForTabAtIndex:(int)index
//{
//    return  [ZSTModuleImage(@"module_csa_top_btn_p.png") stretchableImageWithLeftCapWidth:7 topCapHeight:0];
//}

#pragma mark -  keyboard

-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
	CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
	// get a rect for the textView frame
//    uinavigationController *navigationController
    NSArray *vcArray = self.navigationController.viewControllers;
    CGRect containerFrame = containerView.frame;
    if ([vcArray count]>0 && self==[vcArray objectAtIndex:0]) {
        containerFrame.origin.y = self.view.bounds.size.height + self.navigationController.toolbar.frame.size.height + 5 - (keyboardBounds.size.height + containerFrame.size.height);
    } else {
        containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
    }
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    [UIView setAnimationDelegate:self];
	
	// set views with new info
	containerView.frame = containerFrame;
    self.chatList.frame = CGRectMake(0, 40, 320, containerFrame.origin.y-40);
	
	// commit animations
	[UIView commitAnimations];
}

- (void)animationDidStop:(NSString *)animationID finished:(BOOL)finished
{
    [self.chatList scrollToBottom:YES];

}

-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
	
	// get a rect for the textView frame
	CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
	
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
	// set views with new info
	containerView.frame = containerFrame;
    self.chatList.frame = CGRectMake(0, 40, 320, containerFrame.origin.y-40);
	
	// commit animations
	[UIView commitAnimations];
}

#pragma mark - TKGrowingTextViewDelegate

- (void)growingTextView:(TKGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
    
	CGRect r = containerView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
	containerView.frame = r;
    self.chatList.frame = CGRectMake(0, 47, 320, r.origin.y-47);
}

- (void)growingTextViewDidChange:(TKGrowingTextView *)theGrowingTextView
{
    if (theGrowingTextView.text == nil || [theGrowingTextView.text length] == 0) {
        
        sendBtn.enabled = NO;
    } else {
        
        sendBtn.enabled = YES;
    }
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)theTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UIView *chatView = [[self.chatArray objectAtIndex:indexPath.row] safeObjectForKey:@"view"];
    return chatView.frame.size.height+10;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.tracking && lastContentOffsetY > scrollView.contentOffset.y) {
        
        [textView resignFirstResponder];
    }
    lastContentOffsetY = scrollView.contentOffset.y;
}

#pragma mark - UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.chatArray count];
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"ZSTCommentTableViewCell";
    UITableViewCell *cell = [theTableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if ([self.chatArray count]) {
        NSDictionary *chatInfo = [self.chatArray objectAtIndex:indexPath.row];
        [self configCell:cell cellInfo:chatInfo];
    }
    
    return cell;
}

- (void)configCell:(UITableViewCell *)cell cellInfo:(NSDictionary *)cellInfo
{
    if (cell==nil || cellInfo==nil) {
        return;
    }
    
    UIView *chatView = [cell.contentView viewWithTag:1000];
    if (chatView) {
        [chatView removeFromSuperview];
    }
    
    NSDictionary *message = [cellInfo safeObjectForKey:@"message"];
    
    NSString *addTime = [message safeObjectForKey:@"addtime"];
    chatView = [cellInfo safeObjectForKey:@"view"];
    chatView.tag = 1000;
    UILabel *timeLabel = (UILabel *)[chatView viewWithTag:1201];
//    timeLabel.text = addTime;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *date = [dateFormatter dateFromString:addTime];
    timeLabel.text = [NSDate dateStringWithDate:date];
    
    UILabel *fromLabel = (UILabel *)[chatView viewWithTag:1200];
    TKAsynImageView *headImageView = (TKAsynImageView *)[chatView viewWithTag:1202];
    NSString *speaker = [cellInfo safeObjectForKey:@"speaker"];
    if (self.csInfo != nil && ![speaker isEqualToString:@"self"] ) {
        NSString *csName = [self.csInfo safeObjectForKey:@"name"];
        fromLabel.text = (csName==nil ? NSLocalizedString(@"发送内容不能为空", nil) : csName);
        
        [headImageView clear];
        headImageView.url = [NSURL URLWithString:[self.csInfo safeObjectForKey:@"iconurl"]];
        [headImageView loadImage];
    }
    
    [cell.contentView addSubview:chatView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}




@end
