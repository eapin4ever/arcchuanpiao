//
//  ColumnBar.h
//  ColumnBarDemo
//
//  Created by chenfei on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

// 按钮之间的间距
#define kSpace 5
#define kColumnWidth 46.333333

@protocol ZSTCSAColumnBarDataSource;
@protocol ZSTCSAColumnBarDelegate;

@interface ZSTCSAColumnBar : UIImageView <UIScrollViewDelegate> {
    UIScrollView    *scrollView;
//    UIImageView     *leftCap;
//    UIImageView     *rightCap;
    UIImageView     *mover;
    
    NSInteger             selectedIndex;
    
    id              dataSource;
    id              delegate;
    
    UIImageView    *slideImageView;
}

@property(nonatomic, retain) UIScrollView               *scrollView;
//@property(nonatomic, retain) UIImage                    *leftCapImage;
//@property(nonatomic, retain) UIImage                    *rightCapImage;
@property(nonatomic, retain) UIImage                    *moverImage;

@property(nonatomic, assign) NSInteger                        selectedIndex;

@property(nonatomic, assign) id<ZSTCSAColumnBarDataSource>    dataSource;
@property(nonatomic, assign) id<ZSTCSAColumnBarDelegate>      delegate;

@property(nonatomic, retain) UIImageView    *slideImageView;

- (void)reloadData;
- (void)selectTabAtIndex:(int)index;

@end

@protocol ZSTCSAColumnBarDataSource <NSObject>

- (NSInteger)numberOfTabsInColumnBar:(ZSTCSAColumnBar *)columnBar;//tab的数量
- (NSString *)columnBar:(ZSTCSAColumnBar *)columnBar titleForTabAtIndex:(NSInteger)index;// tab的名称

@optional
- (UIImage *)columnBar:(ZSTCSAColumnBar *)columnBar selectImageForTabAtIndex:(NSInteger)index;// tab 的背景图片和选中图片(没有mover 的情况下)
- (int)columnWidthOfTabsInColumnBar:(ZSTCSAColumnBar *)columnBar;//tab的间距


@end

@protocol ZSTCSAColumnBarDelegate <NSObject>

@optional
- (void)columnBar:(ZSTCSAColumnBar *)columnBar didSelectedTabAtIndex:(NSInteger)index;

@end
