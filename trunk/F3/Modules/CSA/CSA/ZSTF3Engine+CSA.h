//
//  ZSTF3Engine+CSA.h
//  CSA
//
//  Created by wangguangzhao on 11/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTF3Engine.h"
#import "ZSTGlobal+CSA.h"

@protocol ZSTF3EngineCSADelegate <ZSTF3EngineDelegate>

@optional
/**
 *	@brief	获取客服列表
 */
- (void)csaGetCustomerServiceDidSucceed:(NSArray *)csList;
- (void)csaGetCustomerServiceDidFailed:(int)resultCode;

/**
 *	@brief	获取离线消息
 */
- (void)csaGetMessageDidSucceed:(NSArray *)messageList;
- (void)csaGetMessageDidFailed:(int)resultCode;

/**
 *	@brief	发送消息
 */
- (void)csaSendMessageDidSucceed:(int)conId guid:(NSString *)guid;
- (void)csaSendMessageDidFailed:(int)resultCode conId:(int)conId notice:(NSString *)notice;

/**
 *	@brief	获取客服人员回复信息
 */
- (void)csaPushMessageDidSucceed:(NSArray *)messageList;
- (void)csaPushMessageDidFailed:(int)resultCode;

/**
 *	@brief	消息收到回复确认 退出客户端通知
 */
- (void)csaNotifyTransDidSucceed;
- (void)csaNotifyTransDidFailed:(int)resultFailed;

/**
 *	@brief	文件上传
 */
- (void)csaUploadFileDidSucceed:(int)fileId;
- (void)csaUploadFileDidFailed:(int)resultCode;

@end


@interface ZSTF3Engine (CSA)
/**
 *	@brief	获取客服人员列表
 */
- (void)csaGetCustomerService;

/**
 *	@brief	获取指定圈子下pageNum页的新闻
 * 
 *	@param 	sinceId 	信息起始编号
 *	@param 	csId        客服编号
 */
- (void)csaGetMessage:(int)sinceId csId:(int)csId;

/**
 *	@brief	发送信息
 * 
 *	@param 	guId        发送消息得随机编号
 *	@param 	csid        客服编号
 *	@param 	addtime     发送时间
 *	@param 	conId       回话链接编号
 *  @param  message     消息内容
 *  @param  msgType     信息类别
 *  @param  desc        描述
 */

- (void)csaSendMessage:(int)csId
                  guId:(NSString *)guId
               addtime:(NSString *)addTime
                 conId:(int)conId
               message:(NSString *)msg
               msgType:(ZSTCSAMessageType)msgType
                  desc:(NSString *)desc
                dao:(ZSTDao *)dao;

/**
 *	@brief	获取客服人员回复信息
 * 
 *	@param 	csid        客服编号
 *	@param 	conId       回话链接编号
 */
- (void)csaPushMessage:(NSInteger)csId conId:(NSInteger)conId;

/**
 *	@brief	客户端通知客服引擎消息
 * 
 *	@param 	cmd         通知命令
 *	@param 	conId       已确认消息编号组成的字符串
 */
- (void)csaNotifyTrans:(NSString *)cmd msgIdStr:(NSString *)msgIdStr;

/**
 *	@brief	上传文件
 * 
 *	@param 	fileName     文件名称
 *	@param 	fileType     文件类别
 *  @param  fileData     文件内容
 *  @param  fileExt      文件扩展名
 */
- (void)csaUploadFile:(NSString *)fileName 
             fileType:(int)fileType
             fileData:(NSString *)fileData
              fileExt:(NSString *)fileExt;



@end
