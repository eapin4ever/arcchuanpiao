//
//  ZSTCSAChatViewController.h
//  Service
//
//  Created by xuhuijun on 12-11-13.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTCSAColumnBar.h"
#import "ZSTModuleBaseViewController.h"
#import "ColumnBar.h"

@interface ZSTCSAChatViewController : ZSTModuleBaseViewController <TKGrowingTextViewDelegate, UITableViewDataSource, UITableViewDelegate,ZSTCSAColumnBarDelegate,ZSTCSAColumnBarDataSource,ColumnBarDelegate,ColumnBarDataSource>
{
    UIView *containerView;
    TKGrowingTextView *textView;
    
    BOOL _showSendingCell;

    UIButton *sendBtn;
    
    CGFloat lastContentOffsetY;
//    ZSTCSAColumnBar *columnBar;
    NSTimer *_timer;
    
    NSInteger currentSelectItem;
    
    BOOL isConnected;
    NSMutableArray *msgarray;
    
    ColumnBar *columnBar;

}

@property (nonatomic ,retain) UITableView *chatList;
@property (nonatomic ,retain) NSMutableArray *chatArray;
@property (nonatomic ,retain) NSDictionary *csInfo;


@property (nonatomic, retain) NSMutableArray *csArray;

@property (nonatomic, retain) NSMutableArray *msgarray;

@end
