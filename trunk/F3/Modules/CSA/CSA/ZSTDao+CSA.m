//
//  ZSTDao+CSA.m
//  CSA
//
//  Created by wangguangzhao on 11/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTDao+CSA.h"
#import "ZSTSqlManager.h"
#import "ZSTUtils.h"
#import "TKUtil.h"
#import "ZSTGlobal+CSA.h"

#define CREATE_TABLE_CMD(moduleType) [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [CSA_CustomerService_%@] (\
                                                                    id INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                    csid INTEGER DEFAULT 0 , \
                                                                    ecid INTEGER DEFAULT 0, \
                                                                    eccid INTEGER DEFAULT 0,\
                                                                    duty VARCHAR DEFAULT '',\
                                                                    name VARCHAR DEFAULT '', \
                                                                    status INTEGER DEFAULT 0, \
                                                                    iconurl VARCHAR DEFAULT '', \
                                                                    ordernum INTEGER  DEFAULT 0 \
                                                                    );\
                        CREATE TABLE IF NOT EXISTS [CSA_Message_%@] (\
                                                                    id INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                    msgid INTEGER DEFAULT 0, \
                                                                    guid INTEGER DEFAULT 0, \
                                                                    csid INTEGER DEFAULT 0, \
                                                                    message VARCHAR DEFAULT '', \
                                                                    msgtype INTEGER  DEFAULT 0, \
                                                                    iconurl VARCHAR DEFAULT '', \
                                                                    issend INTEGER  DEFAULT 0, \
                                                                    addtime VARCHAR DEFAULT ''\
                                                                    );", [NSNumber numberWithInteger:moduleType], [NSNumber numberWithInteger:moduleType]]

TK_FIX_CATEGORY_BUG(CSA)

@implementation ZSTDao (CSA)

- (void)csaCreateTableIfNotExistForModule
{
    [ZSTSqlManager executeSqlWithSqlStrings:CREATE_TABLE_CMD(self.moduleType)];
}

- (BOOL)csaAddCustomerService:(NSInteger)csid
                         ecid:(NSInteger)ecid
                        eccid:(NSInteger)eccid
                         duty:(NSString*)duty
                         name:(NSString*)name
                       status:(NSInteger)status
                      iconUrl:(NSString*)iconUrl
                     orderNum:(NSInteger)orderNum
{
    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO CSA_CustomerService_%@ (csid, ecid, eccid, duty, name, status, iconurl, ordernum) VALUES (?,?,?,?,?,?,?,?)", [NSNumber numberWithInteger:self.moduleType]],
                    [NSNumber numberWithInteger:csid],
                    [NSNumber numberWithInteger:ecid],
                    [NSNumber numberWithInteger:eccid],
                    [TKUtil wrapNilObject:duty], 
                    [TKUtil wrapNilObject:name],
                    [NSNumber numberWithInteger:status], 
                    [TKUtil wrapNilObject:iconUrl],
                    [NSNumber numberWithInteger:orderNum]
                    ];
    
    return success;
}

- (BOOL)csaCustomerServiceExist:(NSInteger)csID
{
    return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"SELECT csid FROM CSA_CustomerService_%@ where csid = ?", [NSNumber numberWithInteger:self.moduleType]], [NSNumber numberWithInteger:csID]];
}

- (BOOL)csaDeleteAllCustomerService
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM CSA_CustomerService_%@", [NSNumber numberWithInteger:self.moduleType]];
    if (![ZSTSqlManager executeUpdate:deleteSQL]) {
        
        return NO;
    }
    return YES;
}

- (NSArray *)csaGetAllCutomerService
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT id, csid, ecid, eccid, duty, name, status, iconurl, ordernum FROM CSA_CustomerService_%@ ORDER BY ordernum desc", [NSNumber numberWithInteger:self.moduleType]];
    NSArray *results = [ZSTSqlManager executeQuery:querySql];

    return results;
}

- (NSArray *)getCustomerServiceInfo:(NSInteger)csid
{ 
    NSString *querySql = [NSString stringWithFormat:@"SELECT id, csid, ecid, eccid, duty, name, status, iconurl, ordernum FROM CSA_CustomerService_%@ where csid = ? ORDER BY ordernum desc", [NSNumber numberWithInteger:self.moduleType]];
    NSArray *results = [ZSTSqlManager executeQuery:querySql, [NSNumber numberWithInteger:csid]];
    
    return results;
}

- (BOOL)csaAddMessage:(long long int)msgId
                    csId:(NSInteger)csid
                    guid:(NSInteger)guid
                    message:(NSString*)message
                    msgType:(NSInteger)msgType
                    iconUrl:(NSString*)iconUrl
                    isSend:(BOOL)isSend
                    addTime:(NSString *)addTime
{
    NSString *sql = [NSString stringWithFormat:@"Insert or replace into CSA_Message_%@ (msgid, csid, guid, message, msgtype, iconurl, issend, addtime) values (?,?,?,?,?,?,?,?)", [NSNumber numberWithInteger:self.moduleType]];
    BOOL success = [ZSTSqlManager executeUpdate:sql, 
                    [NSNumber numberWithLongLong:msgId], 
                    [NSNumber numberWithInteger:csid],
                    [NSNumber numberWithInteger:guid],
                    [TKUtil wrapNilObject:message], 
                    [NSNumber numberWithInteger:msgType], 
                    [TKUtil wrapNilObject:iconUrl],
                    [NSNumber numberWithInteger:isSend? 1 : 0],
                    addTime
                    ];
    return success;
}


- (BOOL)csaDeleteAllMessage
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM CSA_Message_%@", [NSNumber numberWithInteger:self.moduleType]];
    if (![ZSTSqlManager executeUpdate:deleteSQL]) {
        
        return NO;
    }
    return YES;
}

- (BOOL)csaUpdateMessageId:(long long int)newMsgid guid:(NSInteger)guid
{
    return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"UPDATE CSA_Message_%@ set msgid = ? where guid = ?", [NSNumber numberWithInteger:self.moduleType]], [NSNumber numberWithLongLong:newMsgid],[NSNumber numberWithInteger:guid]];
}


- (BOOL)csaDeleteAllMessage:(int)csid
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM CSA_Message_%@ WHERE csid = ?", [NSNumber numberWithInteger:self.moduleType]];
    if (![ZSTSqlManager executeUpdate:deleteSQL, [NSNumber numberWithDouble:csid]]) {
        
        return NO;
    }
    return YES;
}

- (BOOL)csaMessageExist:(long long int)msgID
{
    return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"SELECT csid FROM CSA_Message_%@ where msgid = ?", [NSNumber numberWithInteger:self.moduleType]], [NSNumber numberWithLongLong:msgID]];
}

- (NSArray *)csaGetMessage:(int)csid pageNum:(int)pageNum pageCount:(int)pageCount
{
    NSArray *results = nil;
    if (pageNum == 0 && pageCount == 0) {
        NSString *querySql = [NSString stringWithFormat:@"SELECT id, msgid, csid, message, msgtype, iconurl, issend, addtime FROM CSA_Message_%@ where csid = ? ORDER BY id asc", [NSNumber numberWithInteger:self.moduleType]];
        results = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:
                                                                  [NSNumber numberWithInt:csid], 
                                                                  nil]];
    } else {
        NSString *querySql = [NSString stringWithFormat:@"SELECT id, msgid, csid, message, msgtype, iconurl, issend, addtime FROM CSA_Message_%@ where csid = ? ORDER BY id asc limit ?,?", [NSNumber numberWithInteger:self.moduleType]];
    
        results = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:
                                                                      [NSNumber numberWithInt:csid], 
                                                                      [NSNumber numberWithInt:(pageNum-1)*pageCount], 
                                                                      [NSNumber numberWithInt:pageCount], 
                                                                      nil]];
    }
    return results;
}

- (NSString *)getCustomerServiceName:(NSInteger)csid
{
    return @"";
}

@end
