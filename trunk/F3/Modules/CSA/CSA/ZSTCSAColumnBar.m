//
//  ColumnBar.m
//  ColumnBarDemo
//
//  Created by chenfei on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
// 特例，默认加入头条 


#import "ZSTCSAColumnBar.h"
#import <QuartzCore/QuartzCore.h>

@implementation ZSTCSAColumnBar

@synthesize scrollView, selectedIndex, dataSource, delegate , moverImage , slideImageView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(5, 0, frame.size.width-10, frame.size.height)];
        scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//        scrollView.contentInset = UIEdgeInsetsMake(0.0f, 15.0f, 0.0f, 15.0f);
        scrollView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
        scrollView.backgroundColor = [UIColor clearColor];
        scrollView.delegate = self;
        scrollView.showsVerticalScrollIndicator = NO;
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.scrollEnabled = YES;
//        scrollView.layer.cornerRadius = frame.size.height/2;
        scrollView.backgroundColor = [UIColor clearColor];
        
        [self addSubview:scrollView];
        
//        leftCap = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, scrollView.contentInset.left, self.frame.size.height)];
//        leftCap.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
//        [self addSubview:leftCap];
//        
//        rightCap = [[UIImageView alloc] initWithFrame:CGRectMake(self.frame.size.width-scrollView.contentInset.right, 0, scrollView.contentInset.right, self.frame.size.height)];
//        rightCap.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
//        [self addSubview:rightCap];
        
        mover = [[UIImageView alloc] init];
        mover.clipsToBounds = YES;
//        mover.layer.cornerRadius = 5;
        
        [scrollView addSubview:mover];
        
        slideImageView = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_csa_top_btn_p.png")];
    }
    return self;
}

- (void)dealloc
{
    [scrollView release];
//    [leftCap release];
//    [rightCap release];
    [mover release];
    [slideImageView release];
    
    [super dealloc];
}

//- (void)setLeftCapImage:(UIImage *)image
//{
//    leftCap.image = image;
//}
//
//- (void)setRightCapImage:(UIImage *)image
//{
//    rightCap.image = image;
//}

- (void)setMoverImage:(UIImage *)image
{
    mover.image = [image stretchableImageWithLeftCapWidth:25 topCapHeight:0];
}

- (void)setupCaps
{
//    if(scrollView.contentSize.width <= scrollView.frame.size.width - scrollView.contentInset.left - scrollView.contentInset.right) {
////		leftCap.hidden = YES;
////		rightCap.hidden = YES;
//	} else {
//		if(scrollView.contentOffset.x > (-scrollView.contentInset.left)+10.0f) {
////			leftCap.hidden = NO;
//		} else {
////			leftCap.hidden = YES;
//		}
//		
//		if((scrollView.frame.size.width+scrollView.contentOffset.x)+10.0f >= scrollView.contentSize.width) {
////			rightCap.hidden = YES;
//		} else {
////			rightCap.hidden = NO;
//		}
//	}
}

- (void)setupVisible:(UIButton *)sender
{
	CGRect rect = sender.frame;
	[scrollView scrollRectToVisible:rect animated:YES];
}

- (void)reloadData
{    
    if(scrollView.subviews && scrollView.subviews.count > 0)
		[scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
	
	if(!self.dataSource)
		return;
	
	NSInteger items = [self.dataSource numberOfTabsInColumnBar:self];


	float origin_x = 0;

	for(int x = 0; x < items; x++) {
        
        NSString *name = nil;

        name = [self.dataSource columnBar:self titleForTabAtIndex:x];
          
		UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.adjustsImageWhenHighlighted = NO;
		[button addTarget:self action:@selector(buttonClicked:) forControlEvents:UIControlEventTouchUpInside];
        CGSize size = [name sizeWithFont:button.titleLabel.font];
        
        NSString * maxStr = @"新闻咨询";
        CGSize maxSize = [maxStr sizeWithFont:button.titleLabel.font];
        if (size.width > maxSize.width) {
            size = maxSize;
        }

		button.frame = CGRectMake(origin_x, 0.0f, size.width + kSpace, scrollView.frame.size.height);
        
        //button.backgroundColor = (x%2 == 0)?[UIColor yellowColor]:[UIColor cyanColor];
        if (x == 0) {
            slideImageView.frame = CGRectMake(button.center.x, CGRectGetMaxY(self.frame)-3, button.frame.size.width, 3);
        }
        
        if ([self.dataSource respondsToSelector:@selector(columnWidthOfTabsInColumnBar:)]) {
            int columnWidth = [self.dataSource columnWidthOfTabsInColumnBar:self];
            if (columnWidth != 0 && columnWidth > (size.width+kSpace*2)) {
                button.frame = CGRectMake(origin_x, 0.0f, columnWidth, scrollView.frame.size.height);
                origin_x += columnWidth;
            } else {
                origin_x += size.width + kSpace*2;
            }
        } 
        else {
            origin_x += size.width + kSpace*2;
        }
//        origin_x += size.width + kSpace*2;

 
        button.titleLabel.font = [UIFont systemFontOfSize:15];
        
		[button setTitle:name forState:UIControlStateNormal];
        
        [button setTitleColor:RGBCOLOR(194,194, 194) forState:UIControlStateNormal];
//        [button setTitleShadowColor:[UIColor colorWithWhite:0.4 alpha:1] forState:UIControlStateNormal];
        
        [button setTitleColor:RGBCOLOR(0, 0, 0) forState:UIControlStateSelected];
//        [button setTitleShadowColor:[UIColor colorWithWhite:0.95 alpha:1] forState:UIControlStateSelected];
        
        if ([self.dataSource respondsToSelector:@selector(columnBar:selectImageForTabAtIndex:)]) {
            UIImage *selectImage = [self.dataSource columnBar:self selectImageForTabAtIndex:x];
            if (selectImage != nil) {
//                [button setBackgroundImage:selectImage forState:UIControlStateSelected];
                [button setTitleColor:RGBCOLOR(194,194, 194) forState:UIControlStateNormal];
//                [button setTitleShadowColor:[UIColor colorWithWhite:0.4 alpha:1] forState:UIControlStateNormal];
                [button setTitleColor:RGBCOLOR(0, 0, 0) forState:UIControlStateSelected];
//                [button setTitleShadowColor:[UIColor colorWithWhite:0.95 alpha:1] forState:UIControlStateSelected];
            }
        }
        [scrollView addSubview:button];
        [scrollView addSubview:slideImageView];
        if (x == 0) {
            button.selected = YES;
        }
	}
	
	scrollView.contentSize = CGSizeMake(origin_x, scrollView.frame.size.height);
	
	[self setupCaps];
}

- (void)moveToFrame:(CGRect)frame animated:(BOOL)animated
{
    NSTimeInterval duration;
    
    if (animated)
        duration = 0.3;
    else
        duration = 0;
        
    [UIView animateWithDuration:duration animations:^(void) {
        [mover removeFromSuperview];
        mover.frame = CGRectMake(frame.origin.x-1, 6.5, frame.size.width, 24);
        [scrollView addSubview:mover];
        
        [slideImageView removeFromSuperview];
        slideImageView.frame = CGRectMake(frame.origin.x-1, CGRectGetMaxY(self.frame)-3, frame.size.width, 3);
        slideImageView.center = CGPointMake(frame.origin.x-1, CGRectGetMaxY(self.frame)-3);
        [scrollView addSubview:slideImageView];
    }];

    [scrollView sendSubviewToBack:mover];
}

- (void)buttonClicked:(UIButton *)button
{
    [self moveToFrame:button.frame animated:NO];
    
    [self setupVisible:button];
    
    for (UIButton *btn in scrollView.subviews) {
        if ([btn isKindOfClass:[UIButton class]])
            btn.selected = NO;
    }
    
    button.selected = YES;
    
    selectedIndex = [scrollView.subviews indexOfObject:button] - 1;
    
    CGPoint center = self.slideImageView.center;
    center.x = button.center.x;
    [UIView animateWithDuration:0.25f animations:^{
        self.slideImageView.center = center;
    }];
    
    if(self.delegate && [self.delegate respondsToSelector:@selector(columnBar:didSelectedTabAtIndex:)])
		[self.delegate columnBar:self didSelectedTabAtIndex:selectedIndex];
}

- (void)selectTabAtIndex:(int)index
{
    if(!scrollView.subviews || scrollView.subviews.count < index+1)
        return;
	
    UIButton *button = (UIButton *)[scrollView.subviews objectAtIndex:index];
    [self setupVisible:button];

	[self setupCaps];
    
    [self moveToFrame:button.frame animated:NO];
    
    [self buttonClicked:button];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)inScrollView
{
	[self setupCaps];
}

@end
