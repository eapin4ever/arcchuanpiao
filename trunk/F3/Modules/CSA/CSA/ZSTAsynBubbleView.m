//
//  ZSTAsynBubbleView.m
//  CSA
//
//  Created by xuhuijun on 13-1-8.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import "ZSTAsynBubbleView.h"
#import "ZSTGlobal+CSA.h"
#import "ZSTF3Engine+CSA.h"

#define KFacialSizeWidth  18
#define KFacialSizeHeight 18
#define MAX_WIDTH 165

#define BEGIN_FLAG @"[/"
#define END_FLAG @"]"


@implementation ZSTAsynBubbleView

@synthesize showSendingWheel,bubbleModuleType;
@synthesize errorImage,bubbleIndicatorView,reSendParam;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.bubbleIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.bubbleIndicatorView.backgroundColor = [UIColor clearColor];
        self.errorImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TKCommonLib.bundle/BubbleView/bubble_error.png"]];

    }
    return self;
}


//图文混排
- (void)getImageRange:(NSString*)message : (NSMutableArray*)array {
    NSRange range=[message rangeOfString: BEGIN_FLAG];
    NSRange range1=[message rangeOfString: END_FLAG];
    //判断当前字符串是否还有表情的标志。
    if (range.length>0 && range1.length>0) {
        if (range.location > 0) {
            [array addObject:[message substringToIndex:range.location]];
            [array addObject:[message substringWithRange:NSMakeRange(range.location, range1.location+1-range.location)]];
            NSString *str=[message substringFromIndex:range1.location+1];
            [self getImageRange:str :array];
        }else {
            NSString *nextstr=[message substringWithRange:NSMakeRange(range.location, range1.location+1-range.location)];
            //排除文字是“”的
            if (![nextstr isEqualToString:@""]) {
                [array addObject:nextstr];
                NSString *str=[message substringFromIndex:range1.location+1];
                [self getImageRange:str :array];
            }else {
                return;
            }
        }
        
    } else if (message != nil) {
        [array addObject:message];
    }
}

- (UIView *)assembleMessageAtIndex : (NSString *) message from:(BOOL)fromself
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    [self getImageRange:message :array];
    UIView *returnView = [[UIView alloc] initWithFrame:CGRectZero];
    NSArray *data = array;
    UIFont *fon = [UIFont systemFontOfSize:14.0f];
    CGFloat upX = 0;
    CGFloat upY = 0;
    CGFloat X = 0;
    CGFloat Y = 0;
    if (data) {
        
        UILabel *fromlabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, 50, 20)];
        fromlabel.font = [UIFont systemFontOfSize:14.0f];
        fromlabel.backgroundColor = [UIColor clearColor];
        fromlabel.tag = 1200;
        
        UIImageView *timeImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TKCommonLib.bundle/BubbleView/bubble-time.png"]];
        timeImageView.frame = CGRectMake(CGRectGetMaxX(fromlabel.frame)+5, 4+2, 12, 12);
        
        UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(timeImageView.frame)+5, 2, 115, 20)];
        dateLabel.font = [UIFont systemFontOfSize:11];
        dateLabel.textColor = [UIColor colorWithRed:145/255.0 green:145/255.0 blue:145/255.0 alpha:1];
        dateLabel.backgroundColor = [UIColor clearColor];
        NSDateFormatter  *formatter = [[NSDateFormatter alloc] init];
		[formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
		NSString *timeString = [formatter stringFromDate:[NSDate date]];
		[formatter release];
        dateLabel.tag = 1201;
		[dateLabel setText:timeString];
        
        if (fromself) {
            fromlabel.textColor = [UIColor colorWithRed:199/255.0 green:156/255.0 blue:54/255.0 alpha:1];
            fromlabel.text = NSLocalizedString(@"我：", nil);
        }else {
            
            fromlabel.textColor = [UIColor colorWithRed:47/255.0 green:137/255.0 blue:198/255.0 alpha:1];
            fromlabel.text = NSLocalizedString(@"客服：", nil);
        }
        
        [returnView addSubview:fromlabel];
        [returnView addSubview:timeImageView];
        [returnView addSubview:dateLabel];
        [fromlabel release];
        [timeImageView release];
        [dateLabel release];
        
        for (int i=0;i < [data count];i++) {
            NSString *str=[data objectAtIndex:i];
//            NSLog(@"str--->%@",str);
            if ([str hasPrefix: BEGIN_FLAG] && [str hasSuffix: END_FLAG])
            {
                if (upX >= MAX_WIDTH)
                {
                    upY = upY + KFacialSizeHeight;
                    upX = 0;
                    X = 150;
                    Y = upY;
                }
//            NSLog(@"str(image)---->%@",str);
                NSString *imageName=[str substringWithRange:NSMakeRange(2, str.length - 3)];
                UIImageView *img=[[UIImageView alloc]initWithImage:[UIImage imageNamed:imageName]];
                img.frame = CGRectMake(upX, upY+30, KFacialSizeWidth, KFacialSizeHeight);
                [returnView addSubview:img];
                [img release];
                upX=KFacialSizeWidth+upX;
                if (X<150) X = upX;
                
                
            } else {
                for (int j = 0; j < [str length]; j++) {
                    NSString *temp = [str substringWithRange:NSMakeRange(j, 1)];
                    if (upX >= MAX_WIDTH)
                    {
                        upY = upY + KFacialSizeHeight;
                        upX = 0;
                        X = 150;
                        Y =upY;
                    }
                    CGSize size=[temp sizeWithFont:fon constrainedToSize:CGSizeMake(150, 40)];
                    UILabel *la = [[UILabel alloc] initWithFrame:CGRectMake(upX,upY+30,size.width,size.height)];
                    la.font = fon;
                    la.text = temp;
                    la.backgroundColor = [UIColor clearColor];
                    [returnView addSubview:la];
                    [la release];
                    upX=upX+size.width;
                    if (X<150) {
                        X = upX;
                    }
                }
            }
        }
    }
    returnView.frame = CGRectMake(15.0f,1.0f, MAX_WIDTH+10, Y+35); //@ 需要将该view的尺寸记下，方便以后使用
    //    NSLog(@"%.1f %.1f", X, Y);
    return returnView;
}


- (void)bubbleView:(NSMutableDictionary *)param from:(BOOL)fromSelf {
    
    self.reSendParam = param;
    
    // build single chat bubble cell with given text
    UIView *returnView =  [self assembleMessageAtIndex:[param safeObjectForKey:@"message"] from:fromSelf];
    returnView.backgroundColor = [UIColor clearColor];
//    UIView *cellView = [[UIView alloc] initWithFrame:CGRectZero];
//    cellView.backgroundColor = [UIColor clearColor];
    
    NSString *imgPath=[[NSBundle mainBundle] pathForResource:fromSelf?@"TKCommonLib.bundle/BubbleView/bubble-myself-bg":@"TKCommonLib.bundle/BubbleView/bubble-other-bg" ofType:@"png"];
    UIImage *bubble = [UIImage imageWithContentsOfFile:imgPath];
	UIImageView *bubbleImageView = [[UIImageView alloc] initWithImage:[bubble stretchableImageWithLeftCapWidth:20 topCapHeight:26*2]];
    
    TKAsynImageView *headImageView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(24, 12, 23, 23)];
    headImageView.tag = 1202;
    headImageView.adjustsImageWhenHighlighted = NO;
        
    if(fromSelf){
        
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reSendmessageAction)];
        [self addGestureRecognizer:tapGestureRecognizer];
        [tapGestureRecognizer release];
        
        headImageView.defaultImage = [UIImage imageNamed:@"TKCommonLib.bundle/BubbleView/face-myself.png"];
        returnView.frame= CGRectMake(29.0f, 12.0f, returnView.frame.size.width, returnView.frame.size.height);
        bubbleImageView.frame = CGRectMake(20.0f, 10.0f, returnView.frame.size.width+24.0f, returnView.frame.size.height+24.0f );
        self.frame = CGRectMake(260.0f-bubbleImageView.frame.size.width-20.0f, 0.0f,bubbleImageView.frame.size.width+60.0f+20.0f, bubbleImageView.frame.size.height+30.0f);
        headImageView.frame = CGRectMake(bubbleImageView.frame.size.width + 30.0f, 10.0f, 41.0f, 52.0f);
        
        if (showSendingWheel) {
            [self performSelector:@selector(sendMessage:) withObject:param];
            bubbleIndicatorView.frame = CGRectMake(4.0f, CGRectGetMidY(bubbleImageView.frame)-4.0f, 8, 8);
            errorImage.frame = CGRectMake(.0f, CGRectGetMidY(bubbleImageView.frame)-8.0f, 16, 16);
            [self addSubview:bubbleIndicatorView];
            [bubbleIndicatorView startAnimating];
            [self addSubview:errorImage];
            errorImage.hidden = YES;
         }
    }
	else{
        headImageView.defaultImage = [UIImage imageNamed:@"TKCommonLib.bundle/BubbleView/face-other.png"];
        returnView.frame= CGRectMake(75.0f, 12.0f, returnView.frame.size.width, returnView.frame.size.height);
        bubbleImageView.frame = CGRectMake(60.0f, 10.0f, returnView.frame.size.width+24.0f, returnView.frame.size.height+24.0f);
		self.frame = CGRectMake(0.0f, 0.0f, CGRectGetMaxX(bubbleImageView.frame),bubbleImageView.frame.size.height+30.0f);
        headImageView.frame = CGRectMake(10.0f, 10.0f, 41.0f, 41.0f);
    }
    
    
    [self addSubview:bubbleImageView];
    [self addSubview:headImageView];
    [self addSubview:returnView];
    [bubbleImageView release];
    [returnView release];
    [headImageView release];
//	return [cellView autorelease];
    
}

- (BOOL) checkNetwork
{
    BOOL isOnline = TRUE;
    
    if (![UIDevice networkAvailable]) {
        
        [TKUIUtil showHUDInView:self.window withText:NSLocalizedString(@"网络异常，请检查网络", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]];
        [TKUIUtil hiddenHUDAfterDelay:2];
        isOnline = FALSE;
    }
    return isOnline;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        self.errorImage.hidden = YES;
        [self.bubbleIndicatorView startAnimating];
        [self sendMessage:self.reSendParam];
        [self performSelectorOnMainThread:@selector(checkNetwork) withObject:nil waitUntilDone:NO];
    }
}

- (void)reSendmessageAction
{
    UIActionSheet *reSendActionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                                   delegate:self
                                                          cancelButtonTitle:NSLocalizedString(@"取消",@"取消")
                                                     destructiveButtonTitle:nil
                                                          otherButtonTitles:NSLocalizedString(@"重发", nil), nil];
    reSendActionSheet.tag = 102;
    [reSendActionSheet showInView:self.window];
    [reSendActionSheet release]; 
}

- (void)sendMessage:(id)object
{
    ZSTDao *dao = [[ZSTDao alloc] init];
    dao.moduleType = self.bubbleModuleType;

    [self.engine csaSendMessage:[[object safeObjectForKey:@"csid"] intValue]
                           guId:[object safeObjectForKey:@"guid"]
                        addtime:[object safeObjectForKey:@"addtime"]
                          conId:0
                        message:[object safeObjectForKey:@"message"]
                        msgType:ZSTCSAMessageType_Text
                           desc:@""
                            dao:dao];
    [dao release];
}

- (void)csaSendMessageDidSucceed:(int)conId guid:(NSString *)guid
{
    [self bubleViewIndicatorOK];

    [ZSTF3Preferences shared].conId = conId;
}

- (void)csaSendMessageDidFailed:(int)resultCode conId:(int)conId  notice:(NSString *)notice
{
    [ZSTF3Preferences shared].conId = conId;
    [self bubleViewIndicatorError];
}


- (void)bubleViewIndicatorOK
{
    [self.bubbleIndicatorView stopAnimating];
}

- (void)bubleViewIndicatorError
{
    [self.bubbleIndicatorView stopAnimating];
    self.errorImage.hidden = NO;
}

- (void)clear
{
    [self.bubbleIndicatorView stopAnimating];
    self.bubbleIndicatorView = nil;
    self.errorImage = nil;
}

- (void)dealloc
{
    [self clear];
    [super dealloc];
}

@end
