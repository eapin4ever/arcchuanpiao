//
//  ZSTF3Engine+CSA.m
//  CSA
//
//  Created by wangguangzhao on 11/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTF3Engine+CSA.h"
#import "ZSTCommunicator.h"
#import "ZSTDao+CSA.h"
#import "ZSTF3Preferences.h"
#import "ZSTLegacyResponse.h"
#import "ZSTSqlManager.h"
#import "JSON.h"


@implementation ZSTF3Engine (CSA)

long long int MsgId = 0;

- (void)csaGetCustomerService
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[CSAGetCustomerService stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(csaGetCustomerServiceResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:NO];


}

- (void)csaGetCustomerServiceResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id csArray = [response.jsonResponse safeObjectForKey:@"info"];

        if (![csArray isKindOfClass:[NSArray class]]) {
            csArray = [NSArray array];
        }

        [self.dao csaDeleteAllCustomerService];
        //入库
        [ZSTSqlManager beginTransaction];
        for (NSMutableDictionary *post in csArray) {
            NSInteger csId = [[post safeObjectForKey:@"csid"] integerValue];
            
            if (![self.dao csaCustomerServiceExist:csId]) {
                [self.dao csaAddCustomerService:csId
                                           ecid:[[post safeObjectForKey:@"ecid"] integerValue]
                                          eccid:[[post safeObjectForKey:@"eccid"] integerValue]
                                           duty:[post safeObjectForKey:@"duty"]
                                           name:[post safeObjectForKey:@"name"]
                                         status:[[post safeObjectForKey:@"status"] integerValue]
                                        iconUrl:[post safeObjectForKey:@"iconurl"]
                                       orderNum:[[post safeObjectForKey:@"ordernum"] integerValue]];
            }
        }
        [ZSTSqlManager commit];
        
        csArray = [self.dao csaGetAllCutomerService];
        if (![csArray isKindOfClass:[NSArray class]]) {
            csArray = [NSArray array];
        }
        if ([self isValidDelegateForSelector:@selector(csaGetCustomerServiceDidSucceed:)]) {
            [self.delegate csaGetCustomerServiceDidSucceed:csArray];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(csaGetCustomerServiceDidFailed:)]) {
            [self.delegate csaGetCustomerServiceDidFailed:response.resultCode];
        }
    }
}

- (void)csaGetMessage:(int)sinceId csId:(int)csId
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[NSNumber numberWithInt:csId] forKey:@"csid"];
    [params setSafeObject:[NSNumber numberWithInt:sinceId] forKey:@"sinceid"];

    [[ZSTCommunicator shared] openAPIPostToPath:[CSAGetMessage stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]] 
                                         params:params
                                         target:self
                                       selector:@selector(csaGetMessageResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:NO];

    
}

- (void)csaGetMessageResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id messages = [response.jsonResponse safeObjectForKey:@"info"];
        if (![messages isKindOfClass:[NSArray class]]) {
            messages = [NSArray array];
        }

        [ZSTSqlManager beginTransaction];
        for (NSMutableDictionary *post in messages) {
            long long int msgID = [[post safeObjectForKey:@"msgid"] longLongValue];
            
            //服务器返回时间格式 '/Date(1342768136510)/'
//            NSString *date = [[[post objectForKey:@"addtime"] stringByReplacingOccurrencesOfString:@"/Date(" withString:@""] stringByReplacingOccurrencesOfString:@")/" withString:@""];
//            NSString *date = [post objectForKey:@"addtime"];
            if (![self.dao csaMessageExist:msgID]) {
                [self.dao csaAddMessage:msgID
                                   csId:[[post safeObjectForKey:@"csid"] integerValue]
                                   guid:[[post safeObjectForKey:@"guid"] integerValue]
                                message:[post safeObjectForKey:@"message"]
                                msgType:[[post safeObjectForKey:@"msgtype"] intValue]
                                iconUrl:[post safeObjectForKey:@"iconurl"]
                                 isSend:NO
                                addTime:[post safeObjectForKey:@"addtime"]];
            }
        }
        [ZSTSqlManager commit];
        if ([self isValidDelegateForSelector:@selector(csaGetMessageDidSucceed:)]) {
            [self.delegate csaGetMessageDidSucceed:messages];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(csaGetMessageDidFailed:)]) {
            [self.delegate csaGetMessageDidFailed:response.resultCode];
        }
    }
}


- (void)csaSendMessage:(int)csId
                  guId:(NSString *)guId
               addtime:(NSString *)addTime
                 conId:(int)conId
               message:(NSString *)msg
               msgType:(ZSTCSAMessageType)msgType
                  desc:(NSString *)desc
               dao:(ZSTDao *)dao
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[NSNumber numberWithInt:csId] forKey:@"csid"];
    [params setSafeObject:[NSNumber numberWithInt:conId] forKey:@"conid"];
    [params setSafeObject:msg forKey:@"message"];
    [params setSafeObject:[NSString stringWithFormat:@"%d", msgType] forKey:@"msgtype"];
    [params setSafeObject:desc forKey:@"description"];
    [params setSafeObject:guId forKey:@"guid"];

    MsgId = [[NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]] longLongValue];
    [ZSTSqlManager beginTransaction];
    if (![dao csaMessageExist:MsgId] ) {
        [dao csaAddMessage:MsgId
                      csId:csId
                      guid:[guId intValue]
                   message:msg
                   msgType:msgType
                   iconUrl:@""
                    isSend:YES
                   addTime:addTime];    
    }
    [ZSTSqlManager commit];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[CSASendMessage stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:dao.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(csaSendMessageResponse:userInfo:)
                                       userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"params",[NSNumber numberWithInteger:dao.moduleType],@"modultype", nil]
                                      matchCase:NO];

}

- (void)csaSendMessageResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    int conId = [[response.jsonResponse safeObjectForKey:@"conid"] intValue];
    long long int msgid = [[response.jsonResponse safeObjectForKey:@"msgid"] longLongValue];
    NSString *guid = [response.jsonResponse safeObjectForKey:@"guid"];
    NSString *notice = [response.jsonResponse safeObjectForKey:@"notice"];
    
    if (response.resultCode == ZSTResultCode_OK) {
        int conId = [[response.jsonResponse safeObjectForKey:@"conid"] intValue];
        NSDictionary *params = [userInfo safeObjectForKey:@"params"];
        NSDateFormatter * dateFormatter = [[[NSDateFormatter alloc] init]autorelease];
        [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
        NSDate *date = [NSDate date];
        NSString * addTime = [dateFormatter stringFromDate:date];
        
        ZSTDao *dao = [[ZSTDao alloc] init];
        dao.moduleType = [(NSNumber *)[userInfo safeObjectForKey:@"modultype"] integerValue];
        [ZSTSqlManager beginTransaction];
        if (![dao csaMessageExist:msgid] ) {
            [dao csaUpdateMessageId:[[response.jsonResponse safeObjectForKey:@"msgid"] longLongValue] guid:[[params safeObjectForKey:@"guid"] integerValue]];
        }
        
        [ZSTSqlManager commit];
        
        NSMutableDictionary *message = [NSMutableDictionary dictionaryWithCapacity:1];
        [message setSafeObject:[response.jsonResponse safeObjectForKey:@"msgid"] forKey:@"msgid"];
        [message setSafeObject:[params safeObjectForKey:@"csid"] forKey:@"csid"];
        [message setSafeObject:[params safeObjectForKey:@"message"] forKey:@"message"];
        [message setSafeObject:[params safeObjectForKey:@"msgtype"] forKey:@"msgtype"];
        [message setSafeObject:@"" forKey:@"iconurl"];

        [message setSafeObject:addTime forKey:@"addtime"];
        
        if ([self isValidDelegateForSelector:@selector(csaSendMessageDidSucceed:guid:)]) {
            [self.delegate csaSendMessageDidSucceed:conId guid:guid];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(csaSendMessageDidFailed:conId:notice:)]) {
            [self.delegate csaSendMessageDidFailed:response.resultCode conId:conId notice:notice];
        }
    }
}

- (void)csaPushMessage:(NSInteger)csId conId:(NSInteger)conId
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[NSNumber numberWithInteger:csId] forKey:@"csid"];
    [params setSafeObject:[NSNumber numberWithInteger:conId] forKey:@"conid"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[CSAPushMessage stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(csaPushMessageResponse:userInfo:)
                                       userInfo:nil
                                 longConnection:NO];
}

- (void)csaPushMessageResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id messages = [response.jsonResponse safeObjectForKey:@"info"];
        if (![messages isKindOfClass:[NSArray class]]) {
            messages = [NSArray array];
        }
    
        [ZSTSqlManager beginTransaction];
        for (NSMutableDictionary *post in messages) {
            long long int msgID = [[post safeObjectForKey:@"msgid"] longLongValue];
            
            NSString *date = [post safeObjectForKey:@"addtime"];
            
            NSLog(@"self.dao = %@",self.dao);

            if (![self.dao csaMessageExist:msgID]) {
                [self.dao csaAddMessage:msgID
                                csId:[[post safeObjectForKey:@"csid"] integerValue]
                                guid:[[post safeObjectForKey:@"guid"] integerValue]
                                message:[post safeObjectForKey:@"message"]
                                msgType:[[post safeObjectForKey:@"msgtype"] intValue]
                                iconUrl:[post safeObjectForKey:@"iconurl"]
                                isSend:NO
                                addTime:date];
            }
        }
        [ZSTSqlManager commit];
        
        if ([self isValidDelegateForSelector:@selector(csaPushMessageDidSucceed:)]) {
            [self.delegate csaPushMessageDidSucceed:messages];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(csaPushMessageDidFailed:)]) {
            [self.delegate csaPushMessageDidFailed:response.resultCode];
        }
    }
}

- (void)csaNotifyTrans:(NSString *)cmd msgIdStr:(NSString *)msgIdStr
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:cmd forKey:@"cmd"];
    [params setSafeObject:msgIdStr forKey:@"message"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[CSANotifyTrans stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(csaNotifyTransResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:NO];

}

- (void)csaNotifyTransResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        
        if ([self isValidDelegateForSelector:@selector(csaNotifyTransDidSucceed)]) {
            [self.delegate csaNotifyTransDidSucceed];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(csaNotifyTransDidFailed:)]) {
            [self.delegate csaNotifyTransDidFailed:response.resultCode];
        }
    }
}

- (void)csaUploadFile:(NSString *)fileName 
             fileType:(int)fileType
             fileData:(NSString *)fileData
              fileExt:(NSString *)fileExt
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:fileName forKey:@"filename"];
    [params setSafeObject:[NSNumber numberWithInt:fileType] forKey:@"filetype"];
    [params setSafeObject:fileData forKey:@"filedata"];
    [params setSafeObject:fileExt forKey:@"fileextension"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[CSAUploadFile stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(csaUploadFileResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:NO];

}

- (void)csaUploadFileResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        int fileId = [[response.jsonResponse safeObjectForKey:@"fileid"] intValue];
        
        if ([self isValidDelegateForSelector:@selector(csaUploadFileDidSucceed:)]) {
            [self.delegate csaUploadFileDidSucceed:fileId];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(csaUploadFileDidFailed:)]) {
            [self.delegate csaUploadFileDidFailed:response.resultCode];
        }
    }
}

@end
