//
//  ZSTCSAAvatarViewController.m
//  CSA
//
//  Created by admin on 12-11-15.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTCSAAvatarViewController.h"

@interface ZSTCSAAvatarViewController ()

@end

@implementation ZSTCSAAvatarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector(popViewController)];
    self.view.layer.contents = (id) ZSTModuleImage(@"module_csa_bg.png").CGImage;    

    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, 320, 50)];
    label.backgroundColor = [UIColor clearColor];
    label.text = NSLocalizedString(@"头像", nil);
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor grayColor];
    label.font = [UIFont systemFontOfSize:18];
    label.center = CGPointMake(320/2, 50);
    [self.view addSubview:label];
    
    NSUInteger cout = 10;
    NSUInteger _columnCount = 3;
    NSUInteger _rowCount = 3;
    
    float imageWidth = 35;
    float imageHeght = 35;
    
    float horizontalSpacing ;
    float verticalSpacing;
    
    horizontalSpacing = (self.view.frame.size.width - imageWidth*_columnCount) / (_columnCount + 1);
    verticalSpacing = 20;
    
    for (int i = 0; i< cout; i ++ ) {
        
        int row = i / _columnCount;
        row = row % _rowCount;
        
        int col = i % _columnCount;
        float x;
        float y;
        
        x = (horizontalSpacing)+(imageWidth + horizontalSpacing)*col;
        row = i / _columnCount;
        y = 100 + (verticalSpacing + imageHeght) * row;
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(x, y, imageWidth, imageHeght);
        [btn setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        [btn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        NSString *imageTitle = [NSString stringWithFormat:@"module_csa_icon%d",i+1];
        [btn setBackgroundImage:[ZSTModuleImage(imageTitle) stretchableImageWithLeftCapWidth:7 topCapHeight:0] forState:UIControlStateNormal];
        btn.tag = i+1000;
        [self.view addSubview:btn];
    }

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
