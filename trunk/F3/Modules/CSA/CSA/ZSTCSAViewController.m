//
//  ZSTCSAViewController.m
//  Service
//
//  Created by xuhuijun on 12-11-13.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTCSAViewController.h"
#import "ZSTCSAChatViewController.h"
#import "ZSTCSAAvatarViewController.h"
#import "ZSTF3Engine+CSA.h"
#import "TKUIUtil.h"
#import "ZSTDao+CSA.h"

@implementation ZSTCSAListCell

@synthesize csAvatarImage;
@synthesize csName;
@synthesize csDuty;
@synthesize delegate;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier 
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) 
    {
        csAvatarImage = [[TKAsynImageView alloc] initWithFrame:CGRectMake(10, 5, 50, 50)];
        csAvatarImage.adorn = [[UIImage imageNamed:@"Module.bundle/module_NewA_list_default_img.png"] stretchableImageWithLeftCapWidth:40 topCapHeight:30];
        csAvatarImage.imageInset = UIEdgeInsetsMake(2.5, 2.5, 3, 2.5);
        csAvatarImage.adjustsImageWhenHighlighted = NO;
        [self.contentView addSubview:csAvatarImage];
        
        csName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(csAvatarImage.frame)+15, 5, self.frame.size.width - CGRectGetMaxX(csAvatarImage.frame)-35, CGRectGetHeight(csAvatarImage.frame)*0.5)];
        csName.backgroundColor = [UIColor clearColor];
        csName.textColor = [UIColor blackColor];
        csName.font = [UIFont systemFontOfSize:15];
        csName.text =  @"";
        csName.highlightedTextColor = [UIColor whiteColor];
        csName.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:csName];
        
        csDuty = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(csAvatarImage.frame)+15, CGRectGetMaxY(csName.frame), self.frame.size.width - CGRectGetMaxX(csAvatarImage.frame)-35, CGRectGetHeight(csAvatarImage.frame)*0.5)];
        csDuty.text = @"";
        csDuty.font = [UIFont systemFontOfSize:11];
        csDuty.highlightedTextColor = [UIColor whiteColor];
        csDuty.textAlignment = NSTextAlignmentLeft;
        csDuty.numberOfLines = 0;
        csDuty.textColor = [UIColor grayColor];
        csDuty.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:csDuty];
        
    }
    return self;
}

- (void)configCell:(NSDictionary *)infoDic
{
    
    NSString *imageUrl = [infoDic safeObjectForKey:@"iconurl"];
    if (imageUrl && [imageUrl isKindOfClass:[NSString class]] && [imageUrl length]>0) {  
        [csAvatarImage clear];
        csAvatarImage.url = [NSURL URLWithString:imageUrl];
        [csAvatarImage loadImage];
    }
    
    csName.text = [infoDic safeObjectForKey:@"name"];
    csDuty.text = [infoDic safeObjectForKey:@"duty"];

}

- (void)dealloc
{
    [csAvatarImage release];
    [csDuty release];
    [csName release];
    
    [super dealloc];
}


@end

@interface ZSTCSAViewController ()

@end

@implementation ZSTCSAViewController

@synthesize csArray;

- (void)moduleApplication:(ZSTModuleApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [application.dao csaCreateTableIfNotExistForModule];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:@"客服" target:self selector:@selector (openChat:)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil itemForNavigationWithTitle:@"头像" target:self selector:@selector (openAvatars)];
    csArray = [[NSArray array] mutableCopy];
    
    csList = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 460) style:UITableViewStylePlain];
    csList.delegate = self;
    csList.dataSource = self;
    csList.backgroundColor = [UIColor whiteColor];
    csList.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    csList.separatorStyle =  UITableViewCellSeparatorStyleNone;
    [self.view addSubview:csList];
    
    [self.view newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkIndicatorView) {
        [self.engine csaGetCustomerService];
    }];
}

- (void)dealloc
{
    [csList release];
    [csArray release];
    
    [super dealloc];
}

#pragma mark - Actions

- (void)openChat:(NSDictionary *)dic
{
    ZSTCSAChatViewController *serviceChat = [[ZSTCSAChatViewController alloc] init];
//    serviceChat.csInfo = [csArray objectAtIndex:0];
    serviceChat.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:serviceChat animated:YES];
    [serviceChat release];
}

- (void)openAvatars
{
    ZSTCSAAvatarViewController *avatarVC = [[ZSTCSAAvatarViewController alloc] init];
    avatarVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:avatarVC animated:YES];
    [avatarVC release];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [csArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TKCellBackgroundView *custview = [[[TKCellBackgroundView alloc] init] autorelease];
    custview.fillColor = [UIColor colorWithWhite:0.98 alpha:1];
    custview.borderColor = [UIColor colorWithWhite:0.88 alpha:1];
    custview.shadowColor = [UIColor whiteColor];
    
    if(([tableView numberOfRowsInSection:indexPath.section]-1) == 0){
        custview.position = CustomCellBackgroundViewPositionSingle;
    }
    else if(indexPath.row == 0){
        custview.position = CustomCellBackgroundViewPositionTop;
    }
    else if (indexPath.row == ([tableView numberOfRowsInSection:indexPath.section]-1)){
        custview.position  = CustomCellBackgroundViewPositionBottom;
    }
    else{
        custview.position = CustomCellBackgroundViewPositionMiddle;
    }
    
    static NSString *newsCellIdentifier = @"ZSTCSAListCell";
    ZSTCSAListCell *cell = [tableView dequeueReusableCellWithIdentifier:newsCellIdentifier];
    if (cell == nil) {
        cell = [[[ZSTCSAListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:newsCellIdentifier] autorelease];
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.backgroundView = custview;

    if ([csArray count]) {
        NSDictionary *dic = [csArray objectAtIndex:indexPath.row];
        [cell configCell:dic];  
    }
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *csInfo = [csArray objectAtIndex:indexPath.row];
    [self openChat:csInfo];
}


#pragma mark -UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}


#pragma mark - ZSTF3EngineCSADelegate

- (void)csaGetCustomerServiceDidSucceed:(NSArray *)theList
{
    [self.view removeNetworkIndicatorView];

    [csArray release];
    csArray = [theList mutableCopy];
    [csList reloadData];

}

- (void)csaGetCustomerServiceDidFailed:(int)resultCode
{
    [self.view refreshFailed];
}

- (void)csaGetMessageDidSucceed:(NSArray *)messageList
{
    NSLog(@"离线消息获取成功");
}

- (void)csaGetMessageDidFailed:(int)resultCode
{
    NSLog(@"离线消息获取失败");
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
