//
//  ZSTGlobal+CSA.h
//  CSA
//
//  Created by wangguangzhao on 11/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>    

typedef enum
{
    ZSTCSAMessageType_Text = 0,  //文本
    ZSTCSAMessageType_Icon = 1,  //图片
    ZSTCSAMessageType_Audio = 2  //声音
} ZSTCSAMessageType;             //信息类型

#define CSACmdConfirm  @"confirm"   //确认回复
#define CSACmdLogout   @"logout"    //退出通知


//////////////////////////////////////////////////////////////////////////////////////////

#define  CSABaseURL @"http://mod.pmit.cn/csa"   

#define CSAGetCustomerService CSABaseURL @"/GetCustomerService"  //客服人员列表
#define CSAGetMessage         CSABaseURL @"/GetMessage"          //获取离线消息
#define CSASendMessage        CSABaseURL @"/SendMessage"         //咨询信息发送
#define CSAPushMessage        CSABaseURL @"/PushMessage"         //信息实时接收
#define CSANotifyTrans        CSABaseURL @"/NotifyTrans"         //事务通知（eg. 确认回复，退出通知）
#define CSAUploadFile         CSABaseURL @"/UploadFile"          //文件上传