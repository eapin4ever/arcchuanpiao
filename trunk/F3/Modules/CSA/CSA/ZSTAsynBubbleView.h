//
//  ZSTAsynBubbleView.h
//  CSA
//
//  Created by xuhuijun on 13-1-8.
//  Copyright (c) 2013年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTAsynBubbleView : UIView <UIActionSheetDelegate>

{
    UIActivityIndicatorView *bubbleIndicatorView;
    UIImageView *errorImage;
}

@property (nonatomic, assign) BOOL showSendingWheel;
@property (nonatomic, retain) UIImageView *errorImage;
@property (nonatomic, retain) UIActivityIndicatorView *bubbleIndicatorView;
@property (nonatomic, retain) NSMutableDictionary *reSendParam;
@property (nonatomic, assign) NSInteger bubbleModuleType;

- (void)bubbleView:(NSMutableDictionary *)param from:(BOOL)fromSelf;

@end
