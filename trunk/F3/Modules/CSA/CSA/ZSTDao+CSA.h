//
//  ZSTDao+CSA.h
//  CSA
//
//  Created by wangguangzhao on 11/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <Foundation/Foundation.h>
#import "ZSTDao.h"

@interface ZSTDao (CSA)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)csaCreateTableIfNotExistForModule;

/**
 *	@brief 添加客服
 *
 *	@param 	csid            客服编号
 *	@param 	ecid            企业编号
 *	@param 	eccid           部门编号
 *	@param 	duty            职责
 *  @param  name            姓名
 *  @param  status          状态
 *  @param  iconUrl         分类图标
 *  @param  orderNum        排列顺序
 *	@return	操作是否成功
 */
- (BOOL)csaAddCustomerService:(NSInteger)csid
                         ecid:(NSInteger)ecid
                        eccid:(NSInteger)eccid
                         duty:(NSString*)duty
                         name:(NSString*)name
                       status:(NSInteger)status
                      iconUrl:(NSString*)iconUrl
                     orderNum:(NSInteger)orderNum;

/**
 *	@brief 添加客服
 *
 *	@param 	csid            客服编号
 *	@return	客服是否已存在
 */
- (BOOL)csaCustomerServiceExist:(NSInteger)csID;

/**
 *	@brief 删除所有客服
 *
 *	@return	操作是否成功
 */
- (BOOL)csaDeleteAllCustomerService;

/**
 *	@brief 获取客服列表
 *
 *	@return	客服列表
 */
- (NSArray *)csaGetAllCutomerService;

/**
 *	@brief 获取客服名称
 *
 *	@param 	csid            客服编号
 *	@return	客服名称
 */
- (NSString *)getCustomerServiceName:(NSInteger)csid;

/**
 *	@brief 添加接收的消息
 *
 *	@param 	csid            客服编号
 *	@param 	msgId           信息编号
 *	@param 	message         信息内容
 *  @param  msgType         信息类别
 *  @param  iconUrl         分类图标
 *  @param  isSend          是否为发送的消息
 *  @param  addTime         添加时间
 *	@return	操作是否成功
 */
- (BOOL)csaAddMessage:(long long int)msgId
                    csId:(NSInteger)csId
                    guid:(NSInteger)guid
                    message:(NSString*)message
                    msgType:(NSInteger)msgType
                    iconUrl:(NSString*)iconUrl
                    isSend:(BOOL)isSend
                    addTime:(NSString *)addTime;

/**
 *	@brief 消息是否已存在
 *
 *	@param 	msgId         消息id
 *	@return	客服是否已存在
 */
- (BOOL)csaMessageExist:(long long int)msgID;

/**
 *	@brief 删除所有接收到的信息
 *
 *	@return	操作是否成功
 */
- (BOOL)csaDeleteAllMessage;

/**
 *	@brief 删除接收的指定客服的信息
 *
 *	@param 	csid            客服编号
 *	@return	操作是否成功
 */
- (BOOL)csaDeleteAllMessage:(int)csid;

/**
 *	@brief 更新发送成功的消息id
 *
 *	@param 	newMsgid            新消息id
 *	@param 	guid                消息对应guid（唯一标识符）
 *	@return	操作是否成功
 */
- (BOOL)csaUpdateMessageId:(long long int)newMsgid guid:(NSInteger)guid;


/**
 *	@brief 获取信息
 *  
 *  @param 	pageNum             页数
 *	@param 	pageCount           每页信息条数
 *	@param 	csid                客服编号
 *	@return	新闻列表
 */
- (NSArray *)csaGetMessage:(int)csid pageNum:(int)pageNum pageCount:(int)pageCount;

@end
