//
//  ZSTCSAViewController.h
//  Service
//
//  Created by xuhuijun  on 12-11-13.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZSTModuleBaseViewController.h"


@class ZSTCSAListCell;

@protocol ZSTCSAListCellDelegate <NSObject>

@optional
- (void)csaListCell:(ZSTCSAListCell *)cell didSelectedViewAtIndex:(NSInteger)index;
@end

@interface ZSTCSAListCell : UITableViewCell<ZSTCSAListCellDelegate>

@property (nonatomic, retain) TKAsynImageView *csAvatarImage;
@property (nonatomic, retain) UILabel *csName;
@property (nonatomic, retain) UILabel *csDuty;
@property (nonatomic, retain) id<ZSTCSAListCellDelegate> delegate;

- (void)configCell:(NSDictionary *)infoDic;

@end


@interface ZSTCSAViewController : ZSTModuleBaseViewController<UITableViewDataSource, UITableViewDelegate,ZSTCSAListCellDelegate>

{
    UITableView *csList;
}

@property (nonatomic, retain)NSMutableArray *csArray;

@end
 