//
//  ZSTECASubmitOrderViewController.m
//  ECA
//
//  Created by admin on 12-9-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTECASubmitOrderViewController.h"
#import "ZSTECAMainCell.h"
#import "ZSTECAConfirmPaymentViewController.h"

@interface ZSTECASubmitOrderViewController ()

@end

@implementation ZSTECASubmitOrderViewController

@synthesize bgScrollView,orderText,goodsImageView,realPrice,originalPrice,crossOutLine,amountText,amountField,details;

@synthesize userText,userTableImage,userTableView,submitBtn,productInfo,productContentInfo,userInfo,currentCell;
 

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:@"返回" target:self selector:@selector (popViewController)];
    self.view.backgroundColor = [UIColor colorWithPatternImage:ZSTModuleImage(@"ECA/module_eca_bg.png")];
    
    lastContentOffsetY = 0;
    
    if (self.userInfo == nil) {
        self.userInfo = [ECAUserVO voWithDic:[NSDictionary dictionary]];
    }
    
    bgScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 460-44)];
    bgScrollView.backgroundColor = [UIColor clearColor];
    bgScrollView.userInteractionEnabled = YES;
    bgScrollView.delegate = self;
    [self.view addSubview:bgScrollView];
    
    self.orderText = [[[UILabel alloc] initWithFrame:CGRectMake(18, 10, 100, 30)] autorelease];
    self.orderText.text = @"订单信息"; 
    self.orderText.textColor = RGBCOLOR(84, 146, 192);
    self.orderText.textAlignment = UITextAlignmentLeft;
    self.orderText.font = [UIFont systemFontOfSize:18];
    self.orderText.backgroundColor = [UIColor clearColor];
    [bgScrollView addSubview:self.orderText];
    
    UIImageView *goodsBg = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(orderText.frame)+8, 320, 94)];
    goodsBg.image = ZSTModuleImage(@"ECA/module_eca_orderText_bg.png");
    [bgScrollView addSubview:goodsBg];
    [goodsBg release];
    
    NSDictionary *fileDic = [self.productContentInfo.fileArr objectAtIndex:0];
//    NSString *url = [fileDic objectForKey:@"FileUrl"];
    NSString *origUrl = [fileDic objectForKey:@"originalUrl"];
    
    self.goodsImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(18, CGRectGetMaxY(orderText.frame)+10, 90, 90)] autorelease];
    self.goodsImageView.adjustsImageWhenHighlighted = NO;
    self.goodsImageView.backgroundColor = RGBCOLOR(239, 239, 239);
    self.goodsImageView.adorn = [[UIImage imageNamed:@"activity-photo-frame.png"] stretchableImageWithLeftCapWidth:4 topCapHeight:6];
    self.goodsImageView.imageInset = UIEdgeInsetsMake(2, 2.5, 3, 2.5);
    [self.goodsImageView clear];
    self.goodsImageView.url = [NSURL URLWithString:origUrl];
    [self.goodsImageView loadImage];
    [bgScrollView addSubview:self.goodsImageView];

    self.realPrice = [[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.goodsImageView.frame) +18, CGRectGetMaxY(orderText.frame)+10, 180, 45)] autorelease];
    self.realPrice.backgroundColor = [UIColor clearColor];
    self.realPrice.text = [NSString stringWithFormat:@"促销价格:%@元",self.productContentInfo.price];
    self.realPrice.font = [UIFont systemFontOfSize:20];
    self.realPrice.textColor = [UIColor redColor];
    [bgScrollView addSubview:self.realPrice];
    
    self.originalPrice = [[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.goodsImageView.frame) +18, CGRectGetMaxY(realPrice.frame), 180, 23)] autorelease];
    self.originalPrice.backgroundColor = [UIColor clearColor];
    self.originalPrice.text = [NSString stringWithFormat:@"价格:%@元",self.productContentInfo.primePrice];
    self.originalPrice.font = [UIFont systemFontOfSize:15];
    self.originalPrice.textColor = [UIColor darkGrayColor];
    [bgScrollView addSubview:self.originalPrice];
    
    self.crossOutLine = [[[UILabel alloc] initWithFrame:CGRectMake(0, 11, 90, 1)] autorelease];
    self.crossOutLine.backgroundColor = [UIColor darkGrayColor];
    [originalPrice addSubview:self.crossOutLine];
    
    self.amountText = [[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.goodsImageView.frame) +18, CGRectGetMaxY(originalPrice.frame), 70, 22)] autorelease];
    self.amountText.backgroundColor = [UIColor clearColor];
    self.amountText.text = @"购买数量:";
    self.amountText.font = [UIFont systemFontOfSize:15];
    self.amountText.textColor = [UIColor darkGrayColor];
    [bgScrollView addSubview:self.amountText];
    
    self.amountField = [[[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(amountText.frame)+10, CGRectGetMaxY(originalPrice.frame), 50, 22)] autorelease];
    self.amountField.borderStyle = UITextBorderStyleNone;
    self.amountField.background = ZSTModuleImage(@"ECA/module_eca_amountField.png");
    self.amountField.text = @"1";
    self.amountField.textColor = [UIColor darkGrayColor];
    self.amountField.textAlignment = UITextAlignmentCenter;
    self.amountField.keyboardType  = UIKeyboardTypeNumberPad;
    self.amountField.backgroundColor = [UIColor clearColor];
    [bgScrollView addSubview:self.amountField];
    
    self.details = [[[UILabel alloc] initWithFrame:CGRectMake(18, CGRectGetMaxY(goodsImageView.frame)+10, 284, 100)] autorelease];
    self.details.backgroundColor = [UIColor clearColor];
    self.details.text = self.productContentInfo.content;
    self.details.font = [UIFont systemFontOfSize:16];
    self.details.textColor = [UIColor darkGrayColor];
    self.details.numberOfLines = 0;
    [bgScrollView addSubview:self.details];
    
    UIImageView *seprateLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(details.frame)+10, 320, 1)];
    seprateLine.backgroundColor  = [UIColor colorWithPatternImage:ZSTModuleImage(@"ECA/module_eca_seprateLine.png")];
    [bgScrollView addSubview:seprateLine];
    
    userText = [[UILabel alloc] initWithFrame:CGRectMake(18, CGRectGetMaxY(details.frame)+20, 200, 30)];
    userText.text = @"收货人信息"; 
    userText.textColor = RGBCOLOR(84, 146, 192);
    userText.textAlignment = UITextAlignmentLeft;
    userText.font = [UIFont systemFontOfSize:16];
    userText.backgroundColor = [UIColor clearColor];
    [bgScrollView addSubview:userText];
    
    userTableImage = [[UIImageView alloc] initWithImage:[ZSTModuleImage(@"ECA/inputField.png") stretchableImageWithLeftCapWidth:9 topCapHeight:8]];
    userTableImage.userInteractionEnabled = YES;
    [bgScrollView addSubview:userTableImage];
    
    userTableView = [[UITableView alloc] initWithFrame:CGRectMake(18, CGRectGetMaxY(userText.frame)+3, 280, 400) style:UITableViewStylePlain];
    userTableView.delegate = self;
    userTableView.dataSource = self;
    userTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    userTableView.backgroundColor = [UIColor clearColor];
    userTableView.userInteractionEnabled = YES; 
    [bgScrollView addSubview:userTableView];
    
    submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [submitBtn setBackgroundImage:ZSTModuleImage(@"ECA/btn_submitOrder_n.png") forState:UIControlStateNormal];
    [submitBtn setBackgroundImage:ZSTModuleImage(@"ECA/btn_submitOrder_p.png") forState:UIControlStateHighlighted];
    [submitBtn addTarget:self action:@selector(submit:) forControlEvents:UIControlEventTouchUpInside];
    [submitBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [submitBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [bgScrollView addSubview:submitBtn];
    
    CGSize size1 = [originalPrice.text sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(180, CGFLOAT_MAX) lineBreakMode:UILineBreakModeCharacterWrap];
    crossOutLine.frame = CGRectMake(0, 11, size1.width, 1);
    
    CGSize size2 = [details.text sizeWithFont:[UIFont systemFontOfSize:16] constrainedToSize:CGSizeMake(284, CGFLOAT_MAX) lineBreakMode:UILineBreakModeCharacterWrap];
    self.details.frame = CGRectMake(18, CGRectGetMaxY(goodsImageView.frame)+10, 284, size2.height+20); 
    seprateLine.frame = CGRectMake(0, CGRectGetMaxY(details.frame)+10, 320, 1);
    userText.frame = CGRectMake(18, CGRectGetMaxY(details.frame)+20, 200, 30);

    CGRect rect2 = [userTableView rectForSection:0];
    
    userTableImage.frame = CGRectMake(10, CGRectGetMaxY(userText.frame), 320-20, rect2.size.height+10);
    userTableView.frame = CGRectMake(18, CGRectGetMaxY(userText.frame)+3, 280, rect2.size.height+10);
    
    submitBtn.frame = CGRectMake(10, CGRectGetMaxY(userTableImage.frame)+30, 300, 31);

    bgScrollView.contentSize = CGSizeMake(320, CGRectGetMaxY(submitBtn.frame)+20);
    
    [seprateLine release];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(keyboardWillShow:) 
                                                 name:UIKeyboardWillShowNotification 
                                               object:nil];
 
}

-(void) keyboardWillShow:(NSNotification *)note{
    
    NSDictionary *info = [note userInfo]; 
    NSValue *value = [info objectForKey:UIKeyboardFrameEndUserInfoKey]; 
    CGSize keyboardSize = [value CGRectValue].size; 
    
    [UIView beginAnimations:@"animation" context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    bgScrollView.frame = CGRectMake(0, 0, 320, 460-44-keyboardSize.height);
    [UIView commitAnimations];
    
    CGRect rect = [userTableView convertRect:currentCell.frame toView:bgScrollView];
    [bgScrollView scrollRectToVisible:rect animated:YES];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (bgScrollView.tracking && lastContentOffsetY > bgScrollView.contentOffset.y) {
        
        for (UIView *subview in [bgScrollView subviews]) {
            if ([subview isKindOfClass:[UITextField class]]) {
                [subview resignFirstResponder];
            }
        }
        
        [currentCell.userdetailText resignFirstResponder];
        [UIView beginAnimations:@"animation" context:nil];
        [UIView setAnimationDuration:0.3];
        bgScrollView.frame = CGRectMake(0, 0, 320, 460-44);
        
        [UIView commitAnimations];
    }
    lastContentOffsetY = bgScrollView.contentOffset.y;
}

#pragma mark- ZSTECAUserCellDelegate

- (void)ZSTECAUserCellShouldBeginEditing:(ZSTECAUserCell *)cell
{
    currentCell = (ZSTECAUserCell *)cell;
}

- (void)ZSTECAUserCellShouldChange:(ZSTECAUserCell *)cell 
{
    if (cell.userdetailText.tag == 0) {
        self.userInfo.name = cell.userdetailText.text;
    }else if (cell.userdetailText.tag == 1){
        self.userInfo.address = cell.userdetailText.text;
    }else if (cell.userdetailText.tag == 2){
        self.userInfo.zipCode = cell.userdetailText.text;
    }else if (cell.userdetailText.tag == 3){
        self.userInfo.msisdn = cell.userdetailText.text;
    }else if (cell.userdetailText.tag == 4){
        self.userInfo.memo = cell.userdetailText.text;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HHCellBackgroundView *custview = [[[HHCellBackgroundView alloc] init] autorelease];
    custview.backgroundColor = [UIColor clearColor];
    custview.fillColor = [UIColor clearColor];
    custview.borderColor = [UIColor colorWithWhite:0.88 alpha:1];
    custview.shadowColor = [UIColor whiteColor];
    
    
    if(([tableView numberOfRowsInSection:indexPath.section]-1) == 0){
        custview.position = CustomCellBackgroundViewPositionNone;
    }
    else if(indexPath.row == 0){
        custview.position = CustomCellBackgroundViewPositionTopNone;
    }
    else if (indexPath.row == ([tableView numberOfRowsInSection:indexPath.section]-1)){
        custview.position  = CustomCellBackgroundViewPositionBottomNone;
    }
    else{
        custview.position = CustomCellBackgroundViewPositionMiddle;
    }
    
    
    static NSString *identifier = @"cell";
    ZSTECAUserCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[[ZSTECAUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.backgroundView = custview;
    cell.delegate = self;
    
    if (tableView == userTableView){
        
        if (indexPath.row == 0) {
            cell.userTextLabel.text = @"姓      名 :";  
            cell.userdetailText.placeholder = @"请输入姓名";
        }else if(indexPath.row == 1){
            cell.userTextLabel.text = @"地      址 :"; 
            cell.userdetailText.placeholder = @"请输入地址";
        }else if(indexPath.row == 2){
            cell.userTextLabel.text = @"邮政编码:";  
            cell.userdetailText.placeholder = @"请输入邮编";
        }else if(indexPath.row == 3){
            cell.userTextLabel.text = @"手机号码:";  
            cell.userdetailText.placeholder = @"请输入手机号码";
        }
        cell.userdetailText.tag = indexPath.row; 

    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    return 40;
}

- (void)submit:(UIButton *)sender
{
    UIView *window = self.view;
    if ([[UIApplication sharedApplication] keyboardView] != nil) {
        window = [[UIApplication sharedApplication] keyboardView].window;
    }
    
    //判断收货人信息是否完整
    if ([self.userInfo.name length] == 0 || self.userInfo.name == nil) {
        [TKUIUtil alertInView:window withTitle:@"请输入姓名" withImage:[UIImage imageNamed:@"icon_warning.png"]];
        return;
    }
    if ([self.userInfo.address length] == 0 || self.userInfo.address == nil) {
        [TKUIUtil alertInView:window withTitle:@"请输入地址" withImage:[UIImage imageNamed:@"icon_warning.png"]];
        return;
    } 
    if ([self.userInfo.zipCode length] == 0 || self.userInfo.zipCode == nil) {
        [TKUIUtil alertInView:window withTitle:@"请输入邮编" withImage:[UIImage imageNamed:@"icon_warning.png"]];
        return;
    }   
    if ([self.userInfo.msisdn length] == 0 || self.userInfo.msisdn == nil) {
        [TKUIUtil alertInView:window withTitle:@"请输入手机号" withImage:[UIImage imageNamed:@"icon_warning.png"]];
        return;
    }else if ([self.userInfo.msisdn length] != 11)
    {
        [TKUIUtil alertInView:window withTitle:@"请输入正确手机号" withImage:[UIImage imageNamed:@"icon_warning.png"]];
        return;
    }
    
    [self.engine getProductsUserInfo:self.userInfo.name
                          address:self.userInfo.address
                          zipCode:self.userInfo.zipCode
                          msisdn:self.userInfo.msisdn
                          memo:self.userInfo.memo
                          count:self.amountField.text
                          productsInfo:self.productInfo
                          productsContenInfo:self.productContentInfo];
    
    
    [TKUIUtil showHUD:window withText:@"正在提交订单"];

}

#pragma mark - ZSTF3EngineECADelegate

- (void)getProductsOrderResponse:(NSDictionary *)orderDic userInfo:(id)userInfo
{
    [TKUIUtil hiddenHUD];
    
    NSString * orderID = [orderDic objectForKey:@"OrderID"];
    
    ZSTECAConfirmPaymentViewController *confirmPay = [[ZSTECAConfirmPaymentViewController alloc] init];
    confirmPay.buyCount = self.amountField.text;
    confirmPay.orderNum = orderID;
    confirmPay.orderProductInfo = self.productContentInfo;
    confirmPay.orderUserInfo = self.userInfo;
    
    confirmPay.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:confirmPay animated:YES];
    [confirmPay release];
}

- (void)requestDidFail:(ZSTResponse *)response
{
    UIView *window = self.view;
    if ([[UIApplication sharedApplication] keyboardView] != nil) {
        window = [[UIApplication sharedApplication] keyboardView].window;
    }
    
    [TKUIUtil showHUD:window withText:@"订单提交失败！"];
    [TKUIUtil hiddenHUDAfterDelay:2];    
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];	
    
    TKRELEASE(bgScrollView);
    TKRELEASE(orderText);
    TKRELEASE(goodsImageView);
    TKRELEASE(realPrice);
    TKRELEASE(originalPrice);
    TKRELEASE(crossOutLine);
    TKRELEASE(amountText);
    TKRELEASE(amountField);
    TKRELEASE(details);
    TKRELEASE(userText);
    TKRELEASE(userTableImage);
    TKRELEASE(userTableView);
    TKRELEASE(submitBtn);
    TKRELEASE(productInfo);
    TKRELEASE(productContentInfo);
    TKRELEASE(userInfo);
    TKRELEASE(currentCell);

    
    [super dealloc];
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
