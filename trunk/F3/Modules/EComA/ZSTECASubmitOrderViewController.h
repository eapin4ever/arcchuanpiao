//
//  ZSTECASubmitOrderViewController.h
//  ECA
//
//  Created by admin on 12-9-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKAsynImageView.h"
#import "ECAUserVO.h"
#import "ZSTECAProduct.h"
#import "ZSTECAMainCell.h"

@interface ZSTECASubmitOrderViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,ZSTECAUserCellDelegate,UIScrollViewDelegate>

{
    CGFloat lastContentOffsetY;
}

@property (nonatomic ,retain) UIScrollView *bgScrollView;

@property (nonatomic ,retain) UILabel *orderText;
@property (nonatomic ,retain) TKAsynImageView *goodsImageView;
@property (nonatomic ,retain) UILabel *realPrice;
@property (nonatomic ,retain) UILabel *originalPrice;
@property (nonatomic ,retain) UILabel *crossOutLine;
@property (nonatomic ,retain) UILabel *amountText;
@property (nonatomic ,retain) UITextField *amountField;
@property (nonatomic ,retain) UILabel *details;

@property (nonatomic ,retain) UILabel *userText;
@property (nonatomic ,retain) UIImageView *userTableImage;
@property (nonatomic ,retain) UITableView *userTableView;

@property (nonatomic ,retain) UIButton *submitBtn;

@property (nonatomic, retain) ZSTECAProduct *productInfo; 
@property (nonatomic, retain) ECAUserVO *userInfo; 

@property (nonatomic ,retain) ZSTECAUserCell *currentCell;


@end
