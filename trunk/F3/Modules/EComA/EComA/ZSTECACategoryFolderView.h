//
//  ZSTECACategoryFolderView.h
//  ECA
//
//  Created by luobin on 12-9-29.
//
//

#import <UIKit/UIKit.h>

@interface ZSTECACategoryFolderView : UIView<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) NSArray *categories;

@end
