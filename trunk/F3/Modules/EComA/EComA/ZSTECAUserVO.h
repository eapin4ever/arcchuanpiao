//
//  ECAUserVO.h
//  ECA
//
//  Created by admin on 12-9-4.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZSTGlobal+EComA.h"

@interface ZSTECAUserVO : NSObject

@property (nonatomic, strong) NSString *name;//姓名
@property (nonatomic, strong) NSString *address;//收货地址
@property (nonatomic, strong) NSString *zipCode;//邮编
@property (nonatomic, strong) NSString *mobile;//电话号码
@property (nonatomic, strong) NSString *memo;//备注
@property (nonatomic, strong) NSString *count;//商品数量
@property (nonatomic, assign) ZSTECAPayType payType;//支付类型
@property (nonatomic, strong) NSString *payStatus;//支付结果


+ (id)voWithDic:(NSDictionary *)dic;
- (id)initWithDic:(NSDictionary *)dic;

@end
