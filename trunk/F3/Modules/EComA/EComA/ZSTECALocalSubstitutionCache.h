
//
//  LocalSubstitutionCache.h
//  News
//
//  Created by luobin on 7/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

//extern NSString *cacheScheme;

/**
 *	@brief	实现图片的缓存
 */
@interface ZSTECALocalSubstitutionCache : NSURLCache
{
	NSMutableDictionary *cachedResponses;
}

@end
