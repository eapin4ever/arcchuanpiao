//
//  ZSTNewsCarouselView.h
//  F3
//
//  Created by xuhuijun on 12-7-18.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TKAsynImageView.h"

@class ZSTECACarouselView;

@protocol ZSTECACarouselViewDataSource <NSObject>

@optional
- (NSUInteger)numberOfViewsInCarouselView:(ZSTECACarouselView *)newsCarouselView;
- (NSDictionary *)carouselView:(ZSTECACarouselView *)carouselView infoForViewAtIndex:(NSInteger)index;

@end

@protocol ZSTECACarouselViewDelegate <NSObject>

@optional
- (void)carouselView:(ZSTECACarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;

@end

@interface ZSTECACarouselView : UIView <HJManagedImageVDelegate,UIScrollViewDelegate,TKAsynImageViewDelegate>
{
    UIScrollView *_scrollView;
    UILabel *_describeLabel;
    TKPageControl *_pageControl;
    NSTimer *_carouselTimer;
    
    NSInteger _totalPages;
    NSInteger _curPage;
    
    NSMutableArray *_curSource;
    
}

@property(nonatomic, assign) id<ZSTECACarouselViewDataSource> carouselDataSource;
@property(nonatomic, assign) id<ZSTECACarouselViewDelegate> carouselDelegate;

- (void)reloadData;
- (void)stopAnimation;
- (void)startAnimation;

@end
