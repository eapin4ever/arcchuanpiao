//
//  ZSTECAConfirmPaymentViewController.h
//  ECA
//
//  Created by admin on 12-8-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine.h"
#import "ZSTECAUserVO.h"

@interface ZSTECAConfirmPaymentViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UIGestureRecognizerDelegate>

@property (nonatomic ,strong) UIScrollView *bgScrollView;

@property (nonatomic ,strong) UITableView *orderTableView;
@property (nonatomic ,strong) UITableView *userTableView;
@property (nonatomic ,strong) UITableView *payTableView;

@property (nonatomic ,strong) UIButton *confirmPayBtn;

@property (nonatomic,strong) NSString *orderNum;
@property (nonatomic,strong) NSString *buyCount;
@property (nonatomic, strong) ZSTECAProduct *orderProductInfo;
@property (nonatomic, strong) ZSTECAUserVO *orderUserInfo;

@end


