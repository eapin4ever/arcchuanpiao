//
//  NewsContentViewController.m
//  F3
//
//  Created by admin on 12-7-19.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTECAProductDetailViewController.h"
#import "ZSTUtils.h"
#import "ZSTECASubmitOrderViewController.h"

@interface ZSTECAProductDetailViewController ()

@end

@implementation ZSTECAProductDetailViewController

@synthesize hTableView;
@synthesize products;
@synthesize callbtn;
@synthesize buybtn;
@synthesize orderNum;
@synthesize currentProductIndex;

- (void)dealloc
{
    [self.engine cancelAllRequest];
}

#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (IS_IOS_7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];

    self.view.layer.contents = (id) ZSTModuleImage(@"module_ecoma_submitOrder_bg.png").CGImage;    
    
    hTableView = [[TKHorizontalTableView alloc] initWithFrame:CGRectMake(0, 0, 320, 377+(iPhone5?88:0))];//416
    hTableView.autoresizingMask = UIViewAutoresizingNone;
    hTableView.delegate = self;
    hTableView.dataSource = self;
    //hTableView.backgroundColor = RGBCOLOR(239, 239, 239);
    hTableView.backgroundColor = [UIColor clearColor];
    hTableView.showsVerticalScrollIndicator = NO;
    hTableView.showsHorizontalScrollIndicator = NO;
    hTableView.pagingEnabled = YES;
    self.hTableView.contentOffset = CGPointMake(320 * currentProductIndex, 0);
    [self.view addSubview:hTableView];
    
    buyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    buyBtn.frame = CGRectMake(0, 377+(iPhone5?88:0), 320, 40);
    [buyBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_bottom_buy_buttn_n.png") forState:UIControlStateNormal];
    [buyBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_bottom_buy_buttn_p.png") forState:UIControlStateHighlighted];
    [buyBtn addTarget:self action:@selector(buyAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:buyBtn];
}

- (void)productDetailCell:(ZSTECAProductDetailCell *)cell didSelectAtIndex:(int)index
{    
    NSMutableArray *photos = [[NSMutableArray alloc] initWithCapacity:10];
    for (int i=0; i<cell.productPhotoView.photoURLs.count; i++) {
         NSDictionary *imageInfo = [cell.productPhotoView.photoURLs objectAtIndex:i];
        if (imageInfo&& [imageInfo safeObjectForKey:@"originalUrl"]) {
            EGOPhoto *photo = [[EGOPhoto alloc] initWithImageURL:[NSURL URLWithString:[imageInfo safeObjectForKey:@"originalUrl"]]];
            [photos addObject:photo];
        }
    }
    
    EGOPhotoSource *source = [[EGOPhotoSource alloc] initWithPhotos:photos];
    EGOPhotoViewController *photoController = [[EGOPhotoViewController alloc] initWithPhotoSource:source];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:photoController];
    [self presentViewController:navController animated:YES completion:nil];


    photoController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:navController action:@selector(dismissModalViewControllerAnimated:)];
}

- (void) buyAction
{
    int page = hTableView.contentOffset.x/320;
    ZSTECAProductDetailCell  *cell = (ZSTECAProductDetailCell *)[hTableView cellForIndex:page];
    if (cell.favoriteBtn.enabled) {
        ZSTECASubmitOrderViewController *commit = [[ZSTECASubmitOrderViewController alloc] init];
        commit.productInfo = cell.productInfo;
        commit.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:commit animated:YES];
    }
}

#pragma mark --
#pragma TKHorizontalTableViewDataSource -------------

- (NSInteger)numberOfRowsInTableView:(TKHorizontalTableView *)tableView {
    return [self.products count]; 
}

- (TKHorizontalTableViewCell *)tableView:(TKHorizontalTableView *)tableView cellAtIndex:(NSUInteger)index {
    
    ZSTECAProductDetailCell *hCell = (ZSTECAProductDetailCell *)[tableView dequeueReusableCell];
    if (hCell == nil) {
        hCell = [[ZSTECAProductDetailCell alloc] init];
        hCell.moduleType = self.moduleType;
    }
    hCell.delegate = self;
    hCell.tag = index+1000;
    
    ZSTECAProduct *nvo = [ZSTECAProduct voWithDic:[self.products objectAtIndex:index]];
    [hCell configCell:nvo forRowAtIndex:index];
    return hCell;
}

#pragma mark --
#pragma TKHorizontalTableViewDelegate -------------
- (CGFloat)tableView:(TKHorizontalTableView *)tableView widthForCellAtIndex:(NSUInteger)index {
    return self.view.frame.size.width;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (hTableView.contentOffset.x < -20) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{

}

@end
