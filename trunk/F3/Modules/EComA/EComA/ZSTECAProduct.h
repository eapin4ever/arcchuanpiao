//
//  NewsVO.h
//  F3Engine
//
//  Created by admin on 12-7-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTECAProduct : NSObject

@property (nonatomic, strong) NSString *productName;//商品名称

@property (nonatomic, strong) NSNumber *salesPrice;//现价
@property (nonatomic, strong) NSNumber *primePrice;//原价

@property (nonatomic, strong) NSString *primeLabel;//原价标签
@property (nonatomic, strong) NSString *salesLabel;//现价标签

@property (nonatomic, strong) NSString *price;//成交价（唯一价格）

@property (nonatomic, strong) NSString *summary;//副标题

@property (nonatomic, strong) NSString *iconUrl;//图标图片地址
@property (nonatomic, strong) NSString *carouselUrl;//轮播图地址
@property (nonatomic, strong) NSString *addTime;//上架日期
@property (nonatomic, strong) NSNumber *categoryID;//分类id
@property (nonatomic, strong) NSNumber *productID;//商品id
@property (nonatomic, strong) NSNumber *orderNum;//排列顺序
@property (nonatomic, strong) NSString *orderID;//订单id

+ (id)voWithDic:(NSDictionary *)dic;
- (id)initWithDic:(NSDictionary *)dic;


@end
