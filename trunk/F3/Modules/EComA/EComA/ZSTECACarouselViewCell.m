//
//  ZSTECACarouselViewCell.m
//  ECA
//
//  Created by luobin on 12-10-9.
//
//

#import "ZSTECACarouselViewCell.h"

@implementation ZSTECACarouselViewCell

@synthesize carouselView;
@synthesize dataArry;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        [self.contentView setBackgroundColor:[UIColor clearColor]];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        //轮播图
        carouselView = [[ZSTECACarouselView alloc] initWithFrame:CGRectMake(0, 0, 320, 106)];
        carouselView.carouselDataSource = self;
        carouselView.carouselDelegate = self;
        [self.contentView addSubview:carouselView];
    }
    return self;
}
- (void)configCell:(NSArray*)array
{
    self.dataArry = nil;
    self.dataArry = array;
    if ([dataArry count] != 0) {
        [carouselView reloadData];
    }
}

#pragma mark - ZSTECACarouselViewDataSource

- (NSUInteger)numberOfViewsInCarouselView:(ZSTECACarouselView *)newsCarouselView
{
    return [dataArry count];
}
- (NSDictionary *)carouselView:(ZSTECACarouselView *)carouselView infoForViewAtIndex:(NSInteger)index
{
    if ([dataArry count] != 0) {
        return [dataArry objectAtIndex:index];
    }
    return nil;
}

#pragma mark - ZSTECACarouselViewDelegate

- (void)carouselView:(ZSTECACarouselView *)theCarouselView didSelectedViewAtIndex:(NSInteger)index
{
    if ([self.carouselViewCellDelegate respondsToSelector:@selector(carouselViewCell:didSelectedViewAtIndex:)]) {
        [self.carouselViewCellDelegate carouselViewCell:theCarouselView didSelectedViewAtIndex:index];
    }
}

- (void)dealloc
{
    carouselView.carouselDelegate = nil;
    carouselView.carouselDataSource = nil;
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    [ZSTModuleImage(@"module_ecoma_carousel_bg.png") drawInRect:CGRectMake(0, 0, 320, 113)];
}

@end

