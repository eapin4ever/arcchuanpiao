//
//  Suggested_questions.m
//  Net_Information
//
//  Created by huqinghe on 11-6-8.
//  Copyright 2011 Shinetechchina.com. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "ZSTECAReportViewController.h"
#import "TKUIUtil.h"
#import "ZSTUtils.h"
#import "ZSTF3Engine+EComA.h"

@implementation ZSTECAReportViewController

@synthesize productInfo;


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    if ([TKUIUtil isActivity:self.view])
    {
        [TKUIUtil hiddenHUD];
    }
    
}

- (void)dealloc
{
    
}

- (void)addComplaintsDidFailed:(int)resultCode
{
    [TKUIUtil alertInWindow:NSLocalizedString(@"举报失败", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]  withCenter:CGPointMake(160, 100)];
}

- (void)addComplaintsDidSucceed
{
    [TKUIUtil alertInWindow:NSLocalizedString(@"信息已提交，感谢您的支持！", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]  withCenter:CGPointMake(160, 100)];
    
    [self performSelector:@selector(doSubmitReportResponse) withObject:nil afterDelay:2];
}

- (void)doSubmitReportResponse
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) click
{
    if (textview.text.length == 0) {
        [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"请输入举报内容", nil) withImage:[UIImage imageNamed:@"icon_warning.png"] withCenter:CGPointMake(160, 100)];
        [TKUIUtil hiddenHUDAfterDelay:2];
        return;
    }

    [textview resignFirstResponder];
    
	NSString *suggested = textview.text;
    NSNumber *productID = productInfo.productID;
    NSString *loginMsisdn = [ZSTF3Preferences shared].loginMsisdn;
    [self.engine addComplaints:suggested productId:[productID stringValue] msisdn:loginMsisdn];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (dismissModalViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"虚假商品信息举报", nil)];
    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed: @"framework_top_bg.png"];
	UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_ecoma_bg.png")];
	backgroundImage.frame = CGRectMake(0, 0, 320, 480);
	[self.view addSubview:backgroundImage];

	
	textview = [[TKPlaceHolderTextView alloc] initWithFrame:CGRectMake(4, 4, 312, 152)];
    textview.font = [UIFont systemFontOfSize:18];
	[self.view addSubview:textview];
    [textview becomeFirstResponder];

	
    button = [ZSTUtils customButtonTitle:NSLocalizedString(@"举 报" , nil)
                                  nImage:[UIImage imageNamed:@"btn_commonbg_n.png"] 
                                  pImage:[UIImage imageNamed:@"btn_commonbg_p.png"] 
                           titleFontSize:13 
                             conerRsdius:5.0];
    button.frame = CGRectMake(130, 163, 60, 30);
	[button addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];

	[self.view addSubview:button];
    
    NSString *minimumSystemVersion = @"5.0";
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    if ([systemVersion compare:minimumSystemVersion options:NSNumericSearch] != NSOrderedAscending)
    {
        CGRect textViewFrame = textview.frame;
        textViewFrame.size = CGSizeMake(312, 152-36);
        textview.frame = textViewFrame;
        
        button.frame = CGRectMake(130, 163-36, 60, 30);
    }
}

@end
