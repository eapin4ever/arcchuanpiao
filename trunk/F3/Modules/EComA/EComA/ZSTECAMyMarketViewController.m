//
//  ZSTECAMyMarketViewController.m
//  ECA
//
//  Created by xuhuijun on 12-9-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTECAMyMarketViewController.h"
#import "ZSTECAProductCell.h"
#import "ZSTF3Engine+EComA.h"
#import "TKDataCache.h"
#import "ZSTECAOrderDetailViewController.h"
#import "ZSTECAProductDetailViewController.h"
#import "ZSTUtils.h"
#import "ZSTECAMainCell.h"

@interface ZSTECAMyMarketViewController ()

@end

@implementation ZSTECAMyMarketViewController

@synthesize userInfo, myFavoriteList, pocketShoppingList,userImageView,userName,shopingTimes,registrationDate,myFavoriteBtn,pocketShoppingBtn,myFavoriteArray,pocketShoppingArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     _isRefreshing = NO;
     _isLoadingMore = NO;
     _favoriteHasMore = NO;
     _shoppingHasMore = NO; 

    
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"我的", nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.view.layer.contents = (id) ZSTModuleImage(@"module_ecoma_category.png").CGImage;    
    
    pocketShoppingList = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 460+(iPhone5?88:0)+(IS_IOS_7?20:0)) style:UITableViewStylePlain];
    pocketShoppingList.delegate = self;
    pocketShoppingList.dataSource = self;
    pocketShoppingList.separatorStyle = UITableViewCellSeparatorStyleNone;
    pocketShoppingList.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    pocketShoppingList.backgroundColor = [UIColor whiteColor];
    pocketShoppingList.userInteractionEnabled = YES;
    pocketShoppingList.hidden = NO;
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    tableHeaderView.backgroundColor = [UIColor clearColor];
    pocketShoppingList.tableHeaderView = tableHeaderView;
    [self.view addSubview:pocketShoppingList];

    self.headerView  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    self.headerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.headerView];
    
    UIView *headerViewBG = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    headerViewBG.layer.contents = (id) ZSTModuleImage(@"module_ecoma_category.png").CGImage;
    [self.headerView addSubview:headerViewBG];

    
    myFavoriteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    myFavoriteBtn.frame = CGRectMake(0, 0, 160, 44);
    [myFavoriteBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_myFavorite_n.png") forState:UIControlStateNormal];
    [myFavoriteBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_myFavorite_p.png") forState:UIControlStateHighlighted];
    [myFavoriteBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_myFavorite_p.png") forState:UIControlStateSelected];
    [myFavoriteBtn addTarget:self action:@selector(favoriteAction) forControlEvents:UIControlEventTouchUpInside];
    myFavoriteBtn.selected = YES;
    myFavoriteBtn.userInteractionEnabled = NO;
    [self.headerView addSubview:myFavoriteBtn];
    
    UIImageView *seprateLine = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_ecoma_market_seprateLine.png")];
    seprateLine.frame = CGRectMake(CGRectGetMaxX(myFavoriteBtn.frame)-1, 0, 2, 44);
    [self.headerView addSubview:seprateLine];
    
    pocketShoppingBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    pocketShoppingBtn.frame = CGRectMake(CGRectGetMaxX(myFavoriteBtn.frame)+1, 0, 160, 44);
    [pocketShoppingBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_pocketShopping_n.png") forState:UIControlStateNormal];
    [pocketShoppingBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_pocketShopping_p.png") forState:UIControlStateHighlighted];
    [pocketShoppingBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_pocketShopping_p.png") forState:UIControlStateSelected];
    [pocketShoppingBtn addTarget:self action:@selector(pocketShoppingAction) forControlEvents:UIControlEventTouchUpInside];
    [self.headerView addSubview:pocketShoppingBtn];

    
    self.headerView.frame = CGRectMake(0, 0, 320, CGRectGetMaxY(pocketShoppingBtn.frame));
    
    self.pocketShoppingArray = [NSMutableArray array];
    self.myFavoriteArray = [NSMutableArray array];

    
    if (_refreshHeaderView == nil) {
        
        _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f-250.0+CGRectGetMaxY(myFavoriteBtn.frame), self.view.frame.size.width, 250.0)];
        _refreshHeaderView.layer.contents = (id) ZSTModuleImage(@"module_ecoma_category.png").CGImage;    

        _refreshHeaderView.delegate = self;
        [pocketShoppingList addSubview:_refreshHeaderView];
        
        [_refreshHeaderView refreshLastUpdatedDate];
        [self performSelector:@selector(autoRefresh)
                   withObject:nil
                   afterDelay:0.5];
    }
    
    if (_loadMoreView == nil) {
        _loadMoreView = [[TKLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
        _loadMoreView.activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
        _loadMoreView.delegate = self;
        if ((myFavoriteBtn.selected && _favoriteHasMore) || (pocketShoppingBtn.selected && _shoppingHasMore)) {
            pocketShoppingList.tableFooterView = _loadMoreView;
        }
    }
    
    _fullScreenDelegate = [[YIFullScreenScroll alloc] initWithViewController:self];
    _fullScreenDelegate.shouldShowUIBarsOnScrollUp = YES;
    
    UIImageView *shadow = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_ecoma_top_column_shadow.png")];
    shadow.frame = CGRectMake(0, CGRectGetMaxY(myFavoriteBtn.frame), 320, -4);
    [pocketShoppingList addSubview:shadow];
}

#pragma mark - Actions

- (void)favoriteAction
{
    [self.engine cancelAllRequest];
    [self finishLoading];
    pocketShoppingBtn.selected = NO;
    myFavoriteBtn.selected = YES;
    myFavoriteBtn.userInteractionEnabled = NO;
    pocketShoppingBtn.userInteractionEnabled = YES;    
    [pocketShoppingList reloadData];
    if (_favoriteHasMore) {
        pocketShoppingList.tableFooterView = _loadMoreView;
    }else {
        pocketShoppingList.tableFooterView = nil;
    }
    [self performSelector:@selector(autoRefresh)
               withObject:nil
               afterDelay:0.0];

}

- (void)pocketShoppingAction
{
    [self.engine cancelAllRequest];
    [self finishLoading];
    pocketShoppingBtn.selected = YES;
    myFavoriteBtn.selected = NO;
    myFavoriteBtn.userInteractionEnabled = YES;
    pocketShoppingBtn.userInteractionEnabled = NO;
    [pocketShoppingList reloadData];
    if ( _shoppingHasMore) {
        pocketShoppingList.tableFooterView = _loadMoreView;
    }else {
        pocketShoppingList.tableFooterView = nil;
        
    }
    [self performSelector:@selector(autoRefresh)
               withObject:nil
               afterDelay:0.0];
}


- (void)autoRefresh
{
    [_refreshHeaderView autoRefreshOnScroll:pocketShoppingList animated:YES];
}

- (void)finishLoading
{
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:pocketShoppingList];
    [_loadMoreView loadMoreScrollViewDataSourceDidFinishedLoading:pocketShoppingList];
    
    _isLoadingMore = NO;
    _isRefreshing = NO;
}

- (void)refreshNewestData
{
    _pagenum = 1;
    
    if (pocketShoppingBtn.selected) {
        [self.engine getPocketShoppingAtPage:1 msisdn:self.userInfo.mobile sinceId:0];
    }else {
        [self.engine getMyFavoritesAtPage:1 msisdn:self.userInfo.mobile sinceId:0];
    }
}

- (void)loadMoreData {
    
    if (pocketShoppingBtn.selected) {
        [self.engine getPocketShoppingAtPage:_pagenum msisdn:self.userInfo.mobile sinceId:[[[pocketShoppingArray lastObject] safeObjectForKey:@"ID"] integerValue]];
    }else {
        [self.engine getMyFavoritesAtPage:_pagenum msisdn:self.userInfo.mobile sinceId:[[[myFavoriteArray lastObject] safeObjectForKey:@"ID"] integerValue]];    
    }
}

- (void)appendData:(NSArray*)dataArray
{
    if (pocketShoppingBtn.selected) {
        [self.pocketShoppingArray addObjectsFromArray:dataArray];
    }else {
        [self.myFavoriteArray addObjectsFromArray:dataArray];
    }
    _pagenum ++ ;
}

- (void)insertData:(NSArray*)dataArray
{
    if (pocketShoppingBtn.selected) {
        self.pocketShoppingArray = [NSMutableArray arrayWithArray:dataArray];
    }else {
        self.myFavoriteArray = [NSMutableArray arrayWithArray:dataArray];
    }
    _pagenum ++;
}

#pragma mark HHLoadMoreViewDelegate Methods

- (void)loadMoreDidTriggerRefresh:(TKLoadMoreView*)view
{
    if (myFavoriteBtn.selected && _favoriteHasMore) {
        _isLoadingMore = YES;
        [self loadMoreData];   
    }else if(pocketShoppingBtn.selected && _shoppingHasMore){
            _isLoadingMore = YES;
        [self loadMoreData];
    } 
}

- (BOOL)loadMoreDataSourceIsLoading:(TKLoadMoreView*)view
{
    return _isLoadingMore;
}


- (void)fetchDataSuccess:(NSArray *)theData isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore
{
    if (isLoadMore) {
        if (myFavoriteBtn.selected) {
            _favoriteHasMore = hasMore;
        }else {
            _shoppingHasMore = hasMore;
        }
        
        [self appendData:theData];
    }else {
        [self insertData:theData];
        
        if ([theData count] == PRODUCTS_PAGE_SIZE) {
            
            if (myFavoriteBtn.selected) {
                _favoriteHasMore = YES;
            }else {
                _shoppingHasMore = YES;
            }       
        }
    }
    if ((myFavoriteBtn.selected && _favoriteHasMore) || (pocketShoppingBtn.selected && _shoppingHasMore)) {
        pocketShoppingList.tableFooterView = _loadMoreView;
    }else {
        pocketShoppingList.tableFooterView = nil;

    }

    [pocketShoppingList reloadData];
    
    [self finishLoading];

}

- (void)fetchDataFailed
{
    [self finishLoading];
}

#pragma mark EGORefreshTableHeaderDelegate Methods

- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view {
    _isRefreshing = YES;
    [self refreshNewestData];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView *)view {
    return _isRefreshing;
}

- (NSDate *)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView *)view {
    return [NSDate date];
}


#pragma mark - 

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    if (!_isLoadingMore) {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
    if (!_isRefreshing) {
        [_loadMoreView performSelector:@selector(loadMoreScrollViewDidEndDragging:) withObject:scrollView afterDelay:0];
    }
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    if (!_isLoadingMore) {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
    if (!_isRefreshing) {
        [_loadMoreView performSelector:@selector(loadMoreScrollViewDidEndDragging:) withObject:scrollView afterDelay:0];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [_fullScreenDelegate scrollViewWillBeginDragging:scrollView];
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    return [_fullScreenDelegate scrollViewShouldScrollToTop:scrollView];;
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView
{
    [_fullScreenDelegate scrollViewDidScrollToTop:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [_fullScreenDelegate scrollViewDidScroll:scrollView];
    
    if (!_isLoadingMore) {
        [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
        
    }
    if (!_isRefreshing) {
        [_loadMoreView loadMoreScrollViewDidScroll:scrollView];
    }

}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (pocketShoppingBtn.selected) {
        
        return (pocketShoppingArray == nil || ![pocketShoppingArray count])? 1 : [self.pocketShoppingArray count];

    }
    return (myFavoriteArray == nil || ![myFavoriteArray count])? 1 : [self.myFavoriteArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TKCellBackgroundView *custview = [[TKCellBackgroundView alloc] init];
    custview.fillColor = [UIColor clearColor];
    custview.borderColor = [UIColor colorWithWhite:0.78 alpha:1];
    custview.shadowColor = [UIColor whiteColor];
    
    if(([tableView numberOfRowsInSection:indexPath.section]-1) == 0){
        custview.position = CustomCellBackgroundViewPositionSingle;
    }
    else if(indexPath.row == 0){
        custview.position = CustomCellBackgroundViewPositionTop;
    }
    else if (indexPath.row == ([tableView numberOfRowsInSection:indexPath.section]-1)){
        custview.position  = CustomCellBackgroundViewPositionBottom;
    }
    else{
        custview.position = CustomCellBackgroundViewPositionMiddle;
    }
    
    if ((pocketShoppingBtn.selected && [pocketShoppingArray count] == 0) || (myFavoriteBtn.selected && [myFavoriteArray count] == 0)) {
        
        static NSString *identifier = @"ZSTECAEmptyTableViewCell";
        ZSTECAEmptyTableViewCell *cell = [pocketShoppingList dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[ZSTECAEmptyTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        if (pocketShoppingBtn.selected) {
            cell.introduceLabel.text = NSLocalizedString(@"空空如也～" , nil);
        }else {
            cell.introduceLabel.text = NSLocalizedString(@"你还没有收藏哦～" , nil);
        }
        return cell;
        
    }else{
        static NSString *CellIdentifier = @"pocketShoppingCell";
        
        NSDictionary *productInfo = nil;
        
        ZSTECAProductCell *cell = [pocketShoppingList dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[ZSTECAProductCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.backgroundView = custview;
        
        if (pocketShoppingBtn.selected) {
            if ([pocketShoppingArray count] != 0 ) {
                productInfo = (NSDictionary *)[pocketShoppingArray objectAtIndex:indexPath.row];
                cell.paySuccess = YES;
            }
        }else {
            if ([myFavoriteArray count] != 0 ) {
                productInfo = (NSDictionary *)[myFavoriteArray objectAtIndex:indexPath.row];
                cell.paySuccess = NO;
            }
        }
        
        if (cell != nil && [productInfo count] != 0) {
            [cell configCell:productInfo];
        }
        
        return cell;
    }
}

#pragma mark - UITableViewDelegate

//- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    TKLoadMoreView *loadMoreView = (TKLoadMoreView *)[cell descendantOrSelfWithClass:[TKLoadMoreView class]];
//    if (loadMoreView) {
//        if (!_isRefreshing) {
//            [_loadMoreView loadMoreTriggerRefresh];
//        }
//    }
//}//不需要

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    if (pocketShoppingBtn.selected) {
        ZSTECAOrderDetailViewController *orderDetail = [[ZSTECAOrderDetailViewController alloc] init];
        orderDetail.orderNum = [[pocketShoppingArray objectAtIndex:indexPath.row] safeObjectForKey:@"OrderID"];
        [self.navigationController pushViewController:orderDetail animated:YES];
    }else {

        ZSTECAProductDetailViewController *productDetailViewController = [[ZSTECAProductDetailViewController alloc] init];
        productDetailViewController.products = myFavoriteArray;
        productDetailViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:productDetailViewController animated:YES];
        productDetailViewController.hTableView.contentOffset = CGPointMake(320 * indexPath.row, 0);
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{    
     if (pocketShoppingBtn.selected) {
         return 105;
     }
    return 95;
}

#pragma mark - ZSTF3EngineECADelegate

- (void)getPocketShoppingDidFailed:(int)resultCode
{
    [self fetchDataFailed];
}

- (void)getPocketShoppingDidSucceed:(NSArray *)list isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish;
{
    [self fetchDataSuccess:list isLoadMore:isLoadMore hasMore:hasMore];
}


- (void)getMyFavoritesDidSucceed:(NSArray *)list isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish
{

    [self fetchDataSuccess:list isLoadMore:isLoadMore hasMore:hasMore];
}

- (void)getMyFavoritesDidFailed:(int)resultCode
{
    [self fetchDataFailed];
}

- (void)dealloc
{
    [self.engine cancelAllRequest];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
