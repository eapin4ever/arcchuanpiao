//
//  ZSTF3Engine+ECA.m
//  News
//
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTF3Engine+EComA.h"
#import "ZSTCommunicator.h"
#import "ZSTF3Preferences.h"
#import "ZSTLegacyResponse.h"
#import "ZSTSqlManager.h"
#import "JSON.h"
#import "ZSTECAProduct.h"

NSString* const showImgKey = @"showImg";

TK_FIX_CATEGORY_BUG(EComA)
@implementation ZSTF3Engine (EComA)

////////////////////////////////////////////////////////////////// category ///////////////////////////////////////////////////////////////////////

- (void)getProductCategory
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetCatagory stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getProductCategoryResponse:userInfo:)];
}

- (void)getProductCategoryResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        BOOL showImg = ([[response.jsonResponse safeObjectForKey:@"ShowImg"] caseInsensitiveCompare:@"true"] == NSOrderedSame);
        [[NSUserDefaults standardUserDefaults] setBool:showImg forKey:showImgKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        id categories = [response.jsonResponse safeObjectForKey:@"Info"];
        if (![categories isKindOfClass:[NSArray class]]) {
            categories = [NSArray array];
        }
        [ZSTSqlManager beginTransaction];
        [self.dao deleteAllProductCategories];
        for (NSDictionary *category in categories) {
            [self.dao addCategory:[[category safeObjectForKey:@"CategoryID"] integerValue]
                     categoryName:[category safeObjectForKey:@"CategoryName"]
                         parentID:[[category safeObjectForKey:@"ParentID"] integerValue]
                         orderNum:[[category safeObjectForKey:@"OrderNum"] integerValue]
                      description:[category safeObjectForKey:@"Description"]
                          iconUrl:[category safeObjectForKey:@"IconUrl"]];
        }
        [ZSTSqlManager commit];
        
        if ([self isValidDelegateForSelector:@selector(getProductCategoryDidSucceed)]) {
            [self.delegate getProductCategoryDidSucceed];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getProductCategoryDidFailed:)]) {
            [self.delegate getProductCategoryDidFailed:response.resultCode];
        }
    }
    
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)getCarousel
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    [[ZSTCommunicator shared] openAPIPostToPath:[GetCarousel stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getCarouselResponse:userInfo:)];
}

- (void)getCarouselResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id carouseles = [response.jsonResponse safeObjectForKey:@"Info"];
        if (![carouseles isKindOfClass:[NSArray class]]) {
            carouseles = [NSArray array];
        }
        
        if ([self isValidDelegateForSelector:@selector(getCarouselDidSucceed:)]) {
            [self.delegate getCarouselDidSucceed:carouseles];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getCarouselDidFailed:)]) {
            [self.delegate getCarouselDidFailed:response.resultCode];
        }
    }
}

////////////////////////////////////////////////////////////////// newsList ///////////////////////////////////////////////////////////////////////

- (void)getProduct:(NSInteger)categoryID
       productName:(NSString *)productName
           sinceID:(NSInteger)sinceID
         orderType:(NSString *)orderType
        fetchCount:(NSUInteger)fetchCount
          iconType:(ZSTECAIconType)iconType
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (categoryID) {
        [params setSafeObject:[NSNumber numberWithInteger:categoryID] forKey:@"CategoryID"];
    }
    if (sinceID) {
        [params setSafeObject:[NSNumber numberWithInteger:sinceID] forKey:@"SinceID"];
    }
    
    [params setSafeObject:orderType forKey:@"OrderType"];
    [params setSafeObject:[NSString stringWithFormat:@"%@",[NSNumber numberWithUnsignedInteger:fetchCount]] forKey:@"Size"];
    //    [params setSafeObject:[NSNumber numberWithInt:iconType] forKey:@"IConType"];
    
    [params setSafeObject:productName forKey:@"ProductName"];
    [params setSafeObject:@"AddTime" forKey:@"OrderField"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetProductList stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getProductResponse:userInfo:)
                                       userInfo:params
                                      matchCase:YES];

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getProductResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id newsList = [response.jsonResponse safeObjectForKey:@"Info"];
        if (![newsList isKindOfClass:[NSArray class]]) {
            newsList = [NSArray array];
        }
        NSString *sortType = [userInfo safeObjectForKey:@"OrderType"];
        if ([self isValidDelegateForSelector:@selector(getProductDidSucceed:isLoadMore:hasMore:isFinish:)]) {
            [self.delegate getProductDidSucceed:newsList isLoadMore:[sortType isEqualToString:SortType_Asc] hasMore:[[response.jsonResponse safeObjectForKey:@"HasMore"] boolValue] isFinish:YES];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getProductDidFailed:)]) {
            [self.delegate getProductDidFailed:response.resultCode];
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getProductAtPage:(int)pageNum category:(NSInteger)categoryID productName:(NSString *)productName sinceId:(NSInteger)sinceId
{
	//如果是loadMore (pageNum>1),则从本地数据获取
	if (pageNum>1){
        [self getProduct:categoryID
             productName:productName
                 sinceID:sinceId
               orderType:SortType_Asc
              fetchCount:PRODUCTS_PAGE_SIZE
                iconType:ZSTIconType_List];
        
	} else {
        //        NSInteger dbMaxId = [[ZSTNewsDao shared] getMaxNewsIdOfCategory:categoryID];
        [self getProduct:categoryID
             productName:productName
                 sinceID:0
               orderType:SortType_Desc
              fetchCount:PRODUCTS_PAGE_SIZE
                iconType:ZSTIconType_List];
	}
}

///////////////////////////////////////////////////////////////// Product content /////////////////////////////////////////////////////////////////////

- (void)getProductsContent:(NSInteger)newsID
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[NSNumber numberWithInteger:newsID] forKey:@"ProductID"];
    [[ZSTCommunicator shared] openAPIPostToPath:[GetProductDetail stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getProductsContentResponse:userInfo:)];
}

- (void)getProductsContentResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(getProductsContentDidSucceed:)]) {
            [self.delegate getProductsContentDidSucceed:response.jsonResponse];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getProductsContentDidFailed:)]) {
            [self.delegate getProductsContentDidFailed:response.resultCode];
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getProductsUserInfo:(NSString *)userName
                    address:(NSString *)address
                    zipCode:(NSString *)zipCode
                     mobile:(NSString *)mobile
                       memo:(NSString *)memo
                      count:(NSString *)count
               productsInfo:(ZSTECAProduct *)productsInfo
//         productsContenInfo:(ZSTECAProductDetail *)productsContentInfo
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject: mobile forKey:@"Mobile"];
    [params setSafeObject: userName forKey:@"UserName"];
    [params setSafeObject: address forKey:@"Address"];
    [params setSafeObject: zipCode forKey:@"ZipCode"];
    [params setSafeObject: @"1" forKey:@"PayType"];
    [params setSafeObject: @"请尽快发货" forKey:@"Descripiton"];
    
    NSMutableDictionary *info = [NSMutableDictionary dictionary];
    
    [info setSafeObject:[NSString stringWithFormat:@"%@",productsInfo.productID] forKey:@"ProductID"];
    [info setSafeObject: count forKey:@"Count"];
    [info setSafeObject:[NSString stringWithFormat:@"%@",productsInfo.price] forKey:@"SalesPrice"];
    
    NSMutableArray *infoArray = [NSMutableArray array];
    [infoArray addObject:info];
    
    [params setSafeObject:infoArray forKey:@"Info"];
    [[ZSTCommunicator shared] openAPIPostToPath:[GetProductsOrder stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getProductsOrderResponse:userInfo:)];
    
}

- (void)getProductsOrderResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(getProductsOrderDidSucceed:)]) {
            [self.delegate getProductsOrderDidSucceed:response.jsonResponse];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getProductsOrderDidFailed:)]) {
            [self.delegate getProductsOrderDidFailed:response.resultCode];
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getPocketShopping:(NSString *)msisdn
                  sinceID:(NSInteger)sinceID
                orderType:(NSString *)orderType
               fetchCount:(NSUInteger)fetchCount
               orderField:(NSString *)orderField
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    [params setSafeObject: [NSString stringWithFormat:@"%ld", fetchCount] forKey:@"Size"];
    if (sinceID) {
        [params setSafeObject:[NSNumber numberWithInteger:sinceID] forKey:@"SinceID"];
    }
    [params setSafeObject: orderType forKey:@"OrderType"];
    [params setSafeObject: orderField forKey:@"OrderField"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetPocketShoppingList stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getPocketShoppingResponse: userInfo:)
                                       userInfo:params
                                      matchCase:YES];

}

- (void)getPocketShoppingResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id pocketShoppingList = [response.jsonResponse objectForKey:@"Info"];
        if (![pocketShoppingList isKindOfClass:[NSArray class]]) {
            pocketShoppingList = [NSArray array];
        }
        NSString *sortType = [userInfo safeObjectForKey:@"OrderType"];
        if ([self isValidDelegateForSelector:@selector(getPocketShoppingDidSucceed:isLoadMore:hasMore:isFinish:)]) {
            [self.delegate getPocketShoppingDidSucceed:pocketShoppingList isLoadMore:[sortType isEqualToString:SortType_Asc] hasMore:[[response.jsonResponse safeObjectForKey:@"HasMore"] boolValue] isFinish:YES];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getPocketShoppingDidFailed:)]) {
            [self.delegate getPocketShoppingDidFailed:response.resultCode];
        }
    }
    
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getPocketShoppingAtPage:(NSInteger)pageNum msisdn:(NSString *)msisdn sinceId:(NSInteger)sinceId
{
    //如果是loadMore (pageNum>1),则从本地数据获取
    if (pageNum>1){
        [self getPocketShopping:msisdn
                        sinceID:sinceId
                      orderType:SortType_Asc
                     fetchCount:PRODUCTS_PAGE_SIZE
                     orderField:@"AddTime"];
        
    } else {
        [self  getPocketShopping:msisdn
                         sinceID:0
                       orderType:SortType_Desc
                      fetchCount:PRODUCTS_PAGE_SIZE
                      orderField:@"AddTime"];
    }
}


- (void)getMyFavorites:(NSString *)msisdn
               sinceID:(NSInteger)sinceID
             orderType:(NSString *)orderType
            fetchCount:(NSUInteger)fetchCount
            orderField:(NSString *)orderField
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    [params setSafeObject: [NSString stringWithFormat:@"%ld", fetchCount] forKey:@"Size"];
    if (sinceID) {
        [params setSafeObject:[NSNumber numberWithInteger:sinceID] forKey:@"SinceID"];
    }
    [params setSafeObject: orderType forKey:@"OrderType"];
    [params setSafeObject: orderField forKey:@"OrderField"];
    
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetMyFavorites stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getMyFavoritesResponse:userInfo:)
                                       userInfo:params
                                      matchCase:YES];

}

- (void)getMyFavoritesResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id MyFavorites = [response.jsonResponse safeObjectForKey:@"Info"];
        if (![MyFavorites isKindOfClass:[NSArray class]]) {
            MyFavorites = [NSArray array];
        }
        
        NSString *sortType = [userInfo safeObjectForKey:@"OrderType"];
        
        if ([self isValidDelegateForSelector:@selector(getMyFavoritesDidSucceed:isLoadMore:hasMore:isFinish:)]) {
            [self.delegate getMyFavoritesDidSucceed:MyFavorites isLoadMore:[sortType isEqualToString:SortType_Asc] hasMore:[[response.jsonResponse safeObjectForKey:@"HasMore"] boolValue] isFinish:YES];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getMyFavoritesDidFailed:)]) {
            [self.delegate getMyFavoritesDidFailed:response.resultCode];
        }
    }
}

- (void)getMyFavoritesAtPage:(NSInteger)pageNum msisdn:(NSString *)msisdn sinceId:(NSInteger)sinceId
{
    //如果是loadMore (pageNum>1),则从本地数据获取
    if (pageNum>1){
        [self getMyFavorites:msisdn
                     sinceID:sinceId
                   orderType:SortType_Asc
                  fetchCount:PRODUCTS_PAGE_SIZE
                  orderField:@"AddTime"];
        
    } else {
        [self  getMyFavorites:msisdn
                      sinceID:0
                    orderType:SortType_Desc
                   fetchCount:PRODUCTS_PAGE_SIZE
                   orderField:@"AddTime"];
    }
}

- (void)getOrderDetail:(NSString *)orderId
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:orderId forKey:@"OrderID"];
    [[ZSTCommunicator shared] openAPIPostToPath:[GetOrderDetail stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getOrderDetailResponse: userInfo:)];
}

- (void)getOrderDetailResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(getOrderDetailDidSucceed:)]) {
            [self.delegate getOrderDetailDidSucceed:response.jsonResponse];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getOrderDetailDidFailed:)]) {
            [self.delegate getOrderDetailDidFailed:response.resultCode];
        }
    }
}

- (void)addComplaints:(NSString *)content
            productId:(NSString *)productId
               msisdn:(NSString *)msisdn
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:productId forKey:@"ProductID"];
    [params setSafeObject:content  forKey:@"Content"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[AddComplaints stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(addComplaintsResponse:userInfo:) ];
}

- (void)addComplaintsResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(addComplaintsDidSucceed)]) {
            [self.delegate addComplaintsDidSucceed];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(addComplaintsDidFailed:)]) {
            [self.delegate addComplaintsDidFailed:response.resultCode];
        }
    }
}

- (void)manageFavorites:(NSString *)productID msisdn:(NSString *)msisdn opType:(NSString *)opType
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:productID forKey:@"ProductID"];
    [params setSafeObject:opType forKey:@"OpType"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[ManageFavorites stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(manageFavoritesResponse: userInfo:)
                                       userInfo:opType
                                      matchCase:YES];

}

- (void)manageFavoritesResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(manageFavoritesDidSucceed:)]) {
            [self.delegate manageFavoritesDidSucceed:userInfo];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(manageFavoritesDidFailed:)]) {
            [self.delegate manageFavoritesDidFailed:response.resultCode];
        }
    }
}

@end
