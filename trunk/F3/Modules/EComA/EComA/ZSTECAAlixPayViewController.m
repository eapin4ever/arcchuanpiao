//
//  ZSTECAAlixPayViewController.m
//  ECA
//
//  Created by admin on 12-9-5.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTECAAlixPayViewController.h"
#import "ZSTUtils.h"

@interface ZSTECAAlixPayViewController ()<UIWebViewDelegate>
{
    UIWebView *_webView;
}

@property (nonatomic, retain)  NSString *url;

@end

@implementation ZSTECAAlixPayViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];

    [super viewDidLoad];
    
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 480+(iPhone5?88:0))];
    _webView.backgroundColor = [UIColor clearColor];
    [_webView setDelegate:self];
    _webView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:_webView];
    
    if (_url) {
        [self setURL:_url];
    }
}

- (void)setURL:(NSString *)url
{
    url = [self normalizeURL:url];
    _url = url;
    
    if (_webView) {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
        [_webView loadRequest:request];
    }
}

- (NSString *)normalizeURL:(NSString *)url
{
    if ([url length] != 0 && [url rangeOfString:@"://"].location == NSNotFound) {
        url = [NSString stringWithFormat:@"http://%@", url];
    }
    return url;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [TKUIUtil showHUD:_webView];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *title = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    self.navigationItem.titleView = [self titleViewWithTitle:title];
    
    [TKUIUtil hiddenHUD];
}

- (UIView *)titleViewWithTitle:(NSString *)title
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(62, 0, 200, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = title;
    label.font = [UIFont boldSystemFontOfSize:20];
    label.lineBreakMode = UILineBreakModeMiddleTruncation;
    label.textColor = [ZSTUtils getNavigationTextColor];
    return label;
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
//    [super webView:webView shouldStartLoadWithRequest:request navigationType:navigationType];

    NSString *url = [[request URL] absoluteString ];
    if ([url hasSuffix:@"clientback=1"]) {

        [self.navigationController popToRootViewControllerAnimated:YES];
        return NO;
    }
    
    return YES;
}

- (void) dealloc
{
    [_webView setDelegate:nil];
}

@end
