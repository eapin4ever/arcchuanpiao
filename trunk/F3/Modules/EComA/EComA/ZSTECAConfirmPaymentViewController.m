//
//  ZSTECAConfirmPaymentViewController.m
//  ECA
//
//  Created by admin on 12-8-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTECAConfirmPaymentViewController.h"
#import "ZSTECAMainCell.h"
#import "ZSTUtils.h"
#import "ZSTECAAlixPayViewController.h"
#import "TKUIUtil.h"

@interface ZSTECAConfirmPaymentViewController ()

@end

@implementation ZSTECAConfirmPaymentViewController

@synthesize bgScrollView ,orderTableView ,confirmPayBtn,orderNum,orderProductInfo,orderUserInfo;
@synthesize userTableView,payTableView,buyCount;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.view.layer.contents = (id) ZSTModuleImage(@"module_ecoma_submitOrder_bg.png").CGImage;    

    
    UILabel *titlelal = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320-110, 22)];
    titlelal.backgroundColor = [UIColor clearColor];
    titlelal.textAlignment = NSTextAlignmentCenter;
    titlelal.font = [UIFont boldSystemFontOfSize:18];
    titlelal.textColor = [ZSTUtils getNavigationTextColor];
    titlelal.text = NSLocalizedString(@"确认支付" , nil);
    self.navigationItem.titleView = titlelal;
        
    bgScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 460-44-40+(iPhone5?88:0))];
    bgScrollView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:bgScrollView];
    
    confirmPayBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmPayBtn.frame = CGRectMake(0, 376+(iPhone5?88:0), 320, 40);
    [confirmPayBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_btn_confirmPay_n.png") forState:UIControlStateNormal];
    [confirmPayBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_btn_confirmPay_p.png") forState:UIControlStateHighlighted];
    [confirmPayBtn addTarget:self action:@selector(confirmPay:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:confirmPayBtn];
    
    orderTableView = [[UITableView alloc] initWithFrame:CGRectMake(3, 10, 313, 400) style:UITableViewStylePlain];
    orderTableView.delegate = self;
    orderTableView.dataSource = self;
    orderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    orderTableView.backgroundColor = [UIColor clearColor];
    orderTableView.userInteractionEnabled = NO;
    [bgScrollView addSubview:orderTableView];
    
    userTableView = [[UITableView alloc] initWithFrame:CGRectMake(3, CGRectGetMaxY(orderTableView.frame)+5, 313, 400) style:UITableViewStylePlain];
    userTableView.delegate = self;
    userTableView.dataSource = self;
    userTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    userTableView.backgroundColor = [UIColor clearColor];
    userTableView.userInteractionEnabled = YES;
    [bgScrollView addSubview:userTableView];
    
    
    payTableView = [[UITableView alloc] initWithFrame:CGRectMake(3, CGRectGetMaxY(userTableView.frame)+5, 313, 400) style:UITableViewStylePlain];
    payTableView.delegate = self;
    payTableView.dataSource = self;
    payTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    payTableView.backgroundColor = [UIColor clearColor];
    payTableView.userInteractionEnabled = YES;
    [bgScrollView addSubview:payTableView];
    

    CGRect rect1 = [orderTableView rectForSection:0];
    orderTableView.frame = CGRectMake(5, 10, 310, rect1.size.height);
    
    CGRect rect2 = [userTableView rectForSection:0];
    userTableView.frame = CGRectMake(5, CGRectGetMaxY(orderTableView.frame)+5, 310, rect2.size.height);
    
    CGRect rect3 = [payTableView rectForSection:0];
    payTableView.frame = CGRectMake(5, CGRectGetMaxY(userTableView.frame)+5, 310, rect3.size.height);
    
    bgScrollView.contentSize = CGSizeMake(320, CGRectGetMaxY(payTableView.frame)+10);
    

}

#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isKindOfClass:[UIButton class]] || [touch.view isKindOfClass: [UIControl class]] ) 
    {    
        return NO;
    }
    return YES;
}


#pragma mark- UITextViewDelegate


- (void)textViewDidBeginEditing:(UITextView *)textView
{
    bgScrollView.scrollEnabled = NO;
}



- (BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    return YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == orderTableView) {
        return 5;
    }else if(tableView == userTableView){
        return 5;
    }else {
        return 2;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    ZSTECAOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[ZSTECAOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if (tableView == orderTableView) {

        if (indexPath.row == 0) {
            cell.orderTextLabel.text = NSLocalizedString(@"确认商品订单", nil);
        }else if (indexPath.row == 1) {
            cell.orderTextLabel.text = NSLocalizedString(@"订  单  号:", nil);        
            cell.orderdetailTextLabel.text = self.orderNum;
        }else if (indexPath.row == 2) {
            cell.orderTextLabel.text = NSLocalizedString(@"商品名称:", nil);        
            cell.orderdetailTextLabel.text = self.orderProductInfo.productName;
        }else if(indexPath.row == 3){
            cell.orderTextLabel.text = NSLocalizedString(@"单      价 :", nil);     
            cell.orderdetailTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@元", nil),self.orderProductInfo.salesPrice];
        }else if(indexPath.row == 4){
            cell.orderTextLabel.text = NSLocalizedString(@"购买数量:", nil);        
            cell.orderdetailTextLabel.text = self.buyCount;
        }
    }else if (tableView == userTableView){

        if (indexPath.row == 0) {
            cell.orderTextLabel.text = NSLocalizedString(@"确认联系方式及收货地址", nil);   
        }else if (indexPath.row == 1) {
            cell.orderTextLabel.text = NSLocalizedString(@"姓      名 :", nil);    
            cell.orderdetailTextLabel.text = self.orderUserInfo.name;
        }else if(indexPath.row == 2){
            cell.orderTextLabel.text = NSLocalizedString(@"地      址 :", nil);   
            cell.orderdetailTextLabel.text = self.orderUserInfo.address;

        }else if(indexPath.row == 3){
            cell.orderTextLabel.text = NSLocalizedString(@"邮政编码:", nil);  
            cell.orderdetailTextLabel.text = self.orderUserInfo.zipCode;
        }else if(indexPath.row == 4){
            cell.orderTextLabel.text = NSLocalizedString(@"手机号码:", nil);   
            cell.orderdetailTextLabel.text = self.orderUserInfo.mobile;
        }
    }else {
        if (indexPath.row == 0) {
            cell.orderTextLabel.text = NSLocalizedString(@"支付方式", nil);   
        }else if (indexPath.row == 1) {
            cell.orderTextLabel.text = NSLocalizedString(@"支付宝WAP支付", nil);
        }
    }
    
    if(([tableView numberOfRowsInSection:indexPath.section]-1) == 0){
        cell.position = CustomCellPositionSingle;
    }
    else if(indexPath.row == 0){
        cell.position = CustomCellPositionTop;
    }
    else if (indexPath.row == ([tableView numberOfRowsInSection:indexPath.section]-1)){
        cell.position  = CustomCellPositionBottom;
    }
    else{
        cell.position = CustomCellPositionMiddle;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{ 
    NSString *str = @"";
    float height = 0;
    if (tableView == orderTableView) {
        if (indexPath.row == 0) {
            str = NSLocalizedString(@"确认商品订单", nil);
        }else if (indexPath.row == 1) {
            str = self.orderNum;
        }else if(indexPath.row == 2){
            str = self.orderProductInfo.productName;
        }else if(indexPath.row == 3){
            str = [NSString stringWithFormat:NSLocalizedString(@"%@元", nil),self.orderProductInfo.salesPrice];
        }else if(indexPath.row == 4){
            str = self.buyCount;
        }
    }else if (tableView == userTableView) {
        if(indexPath.row == 0){
            str = NSLocalizedString(@"确认联系方式及收货地址", nil);
        }else if(indexPath.row == 1){
            str = self.orderUserInfo.name;
        }else if(indexPath.row == 2){
            str = self.orderUserInfo.address;
        }else if(indexPath.row == 3){
            str = self.orderUserInfo.zipCode;
        }else if(indexPath.row == 4){
            str = self.orderUserInfo.mobile;
        }
    }else {
        str = NSLocalizedString(@"支付宝wap支付", nil);
    }
    
    CGSize size = [str sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(190, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
    
    height = size.height;
    
    if (height < 36) {//两行字刚好是36
        height = 45;
    }else {
        height += 20;
    }
    
    return height;

}

- (void)confirmPay:(UIButton *)sender
{
    ZSTECAAlixPayViewController *alixPay = [[ZSTECAAlixPayViewController alloc] init];
    [alixPay setURL:[ECommerceAlixPayUrl stringByAppendingString:[NSString stringWithFormat:@"%@",self.orderNum]]];
    
    
    alixPay.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:alixPay animated:YES];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
