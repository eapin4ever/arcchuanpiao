//
//  ZSTECACategoryFolderCell.h
//  ECA
//
//  Created by luobin on 12-9-29.
//
//

#import <UIKit/UIKit.h>

@interface ZSTECACategoryFolderCell : UITableViewCell

@property (nonatomic, strong) NSMutableArray *iconViewes;

- (void)configCell:(NSArray *)categories forRowAtIndex:(NSIndexPath *)indexPath;

@end
