//
//  ECAUserVO.m
//  ECA
//
//  Created by admin on 12-9-4.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTECAUserVO.h"

@implementation ZSTECAUserVO

@synthesize name;
@synthesize address;
@synthesize zipCode;
@synthesize mobile;
@synthesize memo;
@synthesize count;
@synthesize payType;
@synthesize payStatus;

+ (id)voWithDic:(NSDictionary *)dic
{
    return [[self alloc] initWithDic: dic];
}

- (id)initWithDic:(NSDictionary *)dic
{
    if( (self=[super init])) {
        self.name =  [dic safeObjectForKey:@"name"];
        self.address = [dic safeObjectForKey:@"address"];
        self.zipCode = [dic safeObjectForKey:@"zipCode"] ? [dic safeObjectForKey:@"zipCode"]: @"";
        self.mobile = [dic safeObjectForKey:@"mobile"];
        self.memo = [dic safeObjectForKey:@"memo"];
        self.count = [dic safeObjectForKey:@"count"];
        self.payType = (ZSTECAPayType)[dic safeObjectForKey:@"payType"];
        self.payStatus = [dic safeObjectForKey:@"payStatus"];
    }
    return self;
}

@end
