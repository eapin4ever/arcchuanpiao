//
//  NewsContentViewController.h
//  F3
//
//  Created by admin on 12-7-19.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKHorizontalTableView.h"
#import "ZSTF3Engine+EComA.h"
#import "ZSTECAProductDetailCell.h"
#import "EGOPhotoGlobal.h"
#import "ZSTLoginViewController.h"

@interface ZSTECAProductDetailViewController : UIViewController <TKHorizontalTableViewDelegate,TKHorizontalTableViewDataSource,ZSTECAProductDetailDelegate,LoginDelegate> {
    UIButton *buyBtn;    
}
@property (nonatomic, strong) TKHorizontalTableView *hTableView;
@property (nonatomic, strong) NSArray *products;
@property (nonatomic, strong) UIButton *callbtn;
@property (nonatomic, strong) UIButton *buybtn;
@property (nonatomic, strong) NSString *orderNum;
@property (nonatomic, assign) NSInteger currentProductIndex;


@end
