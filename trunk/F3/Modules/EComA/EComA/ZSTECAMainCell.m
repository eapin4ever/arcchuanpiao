
//
//  NewsMainCell.m
//  F3
//
//  Created by admin on 12-7-17.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTECAMainCell.h"
#import "ZSTGlobal+EComA.h"

@implementation ZSTECAEmptyTableViewCell

@synthesize introduceLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        introduceLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 240, 416)];
        introduceLabel.numberOfLines = 1;
        introduceLabel.textAlignment = NSTextAlignmentCenter;
        introduceLabel.textColor = RGBCOLOR(150, 150, 150);
        introduceLabel.backgroundColor = [UIColor clearColor];
        introduceLabel.font = [UIFont systemFontOfSize:18];
        [self.contentView addSubview:introduceLabel];
    }
    return self;
}

- (void)setIntroduce:(NSString *)introduce
{
    self.introduceLabel.text = introduce;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code

    CGContextRef cr = UIGraphicsGetCurrentContext();
    [RGBCOLOR(241, 241, 241) set];
    [[UIColor clearColor] set];
    CGContextFillRect(cr, self.bounds);

}

@end


@implementation ZSTECAOrderCell

@synthesize orderTextLabel,orderdetailTextLabel,position;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier 
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) 
    {
        orderTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 3, 70, 40)];
        orderTextLabel.backgroundColor = [UIColor clearColor];
        orderTextLabel.textColor = RGBCOLOR(95, 95, 95);
//        orderTextLabel.textAlignment = NSTextAlignmentCenter;
        orderTextLabel.textAlignment = NSTextAlignmentLeft;
        orderTextLabel.font = [UIFont systemFontOfSize:14];
        [self.contentView addSubview:orderTextLabel];
        
        orderdetailTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(orderTextLabel.frame)+10, 0, 190, self.frame.size.height)];
        orderdetailTextLabel.textAlignment = NSTextAlignmentLeft;
        orderdetailTextLabel.font = [UIFont systemFontOfSize:14];
        orderdetailTextLabel.textColor = RGBCOLOR(158,158,158);
        orderdetailTextLabel.backgroundColor = [UIColor clearColor];
        orderdetailTextLabel.numberOfLines = 0;
        [self.contentView addSubview:orderdetailTextLabel];
        
    }
    return self;
}

- (void)layoutSubviews
{
    CGSize size = [orderdetailTextLabel.text sizeWithFont:self.orderdetailTextLabel.font constrainedToSize:CGSizeMake(190, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
    
    float height = size.height;
    if (height < self.frame.size.height) {
        height = self.frame.size.height;
    }else {
        height += 25;
    }
    CGSize size1 = [orderTextLabel.text sizeWithFont:self.orderTextLabel.font constrainedToSize:CGSizeMake(150, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];

    orderTextLabel.frame = CGRectMake(15, 3, size1.width, 40);
    orderdetailTextLabel.frame = CGRectMake(CGRectGetMaxX(orderTextLabel.frame)+10, 1, 190, height);
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef c = UIGraphicsGetCurrentContext();
    
    [[UIColor clearColor] set];
    CGContextFillRect(c, self.bounds);
    
    CGFloat height = 1.f;
    if (TKIsRetina()) {
        height = 0.5f;
    }
    
    if (position == CustomCellPositionTop) {
        
        self.orderTextLabel.textColor = [UIColor blackColor];
        self.orderTextLabel.font = [UIFont systemFontOfSize:16];
        self.orderTextLabel.width = 200;
        
        [ZSTModuleImage(@"module_ecoma_list_top_bg.png") drawInRect:CGRectMake(0, 0, self.width, self.height)];
        [ZSTModuleImage(@"module_ecoma_orderSeparator.png") drawInRect:CGRectMake(1, self.height - height, 307, height)];
        
        return;
    } else if (position == CustomCellPositionBottom) {
        
        [ZSTModuleImage(@"module_ecoma_list_bottom_bg.png") drawInRect:CGRectMake(0, 0, self.width, self.height)];
        return;
    }
    else if (position == CustomCellPositionSingle || position == CustomCellPositionMiddle) {
        
        [ZSTModuleImage(@"module_ecoma_list_middle_bg.png") drawInRect:CGRectMake(0, 0, self.width, self.height)];
        
        [ZSTModuleImage(@"module_ecoma_orderSeparator.png") drawInRect:CGRectMake(1, self.height - height, 307, height)];

        return;
    } else {
        [ZSTModuleImage(@"module_ecoma_list_middle_bg.png") drawInRect:CGRectMake(0, 0, self.width, self.height )];
    }
}

@end

@implementation ZSTECAUserCell

@synthesize userTextLabel,userdetailText,delegate,position;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier 
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) 
    {        
        userTextLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 3, 70, 38)];

        userTextLabel.backgroundColor = [UIColor clearColor];
        userTextLabel.textColor = RGBCOLOR(95, 95, 95);
//        userTextLabel.textAlignment = NSTextAlignmentCenter;
        userTextLabel.textAlignment = NSTextAlignmentLeft;
        userTextLabel.font = [UIFont systemFontOfSize:15];
        [self.contentView addSubview:userTextLabel];
        
        userdetailText = [[UITextField alloc] initWithFrame:CGRectMake(85, 3, 190, 38)];
        userdetailText.textAlignment = NSTextAlignmentLeft;
        userdetailText.clearButtonMode = UITextFieldViewModeWhileEditing;
        userdetailText.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        userdetailText.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        userdetailText.font = [UIFont systemFontOfSize:14];
        userdetailText.textColor = RGBCOLOR(158,158,158);
        userdetailText.backgroundColor = [UIColor clearColor];
        userdetailText.delegate = self;
        [userdetailText addTarget:self action:@selector(updateLabelUsingContentsOfTextField:) forControlEvents:UIControlEventEditingChanged];

        [self.contentView addSubview:userdetailText];
        
    }
    return self;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([delegate respondsToSelector:@selector(ZSTECAUserCellShouldBeginEditing:)]) {
        [delegate ZSTECAUserCellShouldBeginEditing:self];
    }
    return YES;
}

- (void)updateLabelUsingContentsOfTextField:(UITextField *)sender
{
    
    if ([delegate respondsToSelector:@selector(ZSTECAUserCellShouldChange:)]) {
        [delegate ZSTECAUserCellShouldChange:self];
    }

}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
     return YES;
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef c = UIGraphicsGetCurrentContext();
    
    [[UIColor clearColor] set];
    CGContextFillRect(c, self.bounds);
    
    CGFloat height = 1.f;
    if (TKIsRetina()) {
        height = 0.5f;
    }
    
    if (position == CustomCellPositionTop) {
        
        self.userTextLabel.textColor = [UIColor blackColor];
        self.userTextLabel.font = [UIFont systemFontOfSize:16];
        self.userTextLabel.width = 200;
        self.userdetailText.userInteractionEnabled = NO;
        
        [ZSTModuleImage(@"module_ecoma_list_top_bg.png") drawInRect:CGRectMake(0, 0, self.width, self.height)];
        [ZSTModuleImage(@"module_ecoma_orderSeparator.png") drawInRect:CGRectMake(1, self.height - height, 307, height)];
        
        return;
    } else if (position == CustomCellPositionBottom) {
        
        [ZSTModuleImage(@"module_ecoma_list_middle_bg.png") drawInRect:CGRectMake(0, 0, self.width, self.height-5)];
        [ZSTModuleImage(@"module_ecoma_product_detail_end_image.png") drawInRect:CGRectMake(1, self.height-5, 309, 5)];        
        return;
    }
    else if (position == CustomCellPositionSingle || position == CustomCellPositionMiddle) {
        
        [ZSTModuleImage(@"module_ecoma_list_middle_bg.png") drawInRect:CGRectMake(0, 0, self.width, self.height)];
        
        [ZSTModuleImage(@"module_ecoma_orderSeparator.png") drawInRect:CGRectMake(1, self.height - height, 307, height)];
        
        return;
    } else {
        [ZSTModuleImage(@"module_ecoma_list_middle_bg.png") drawInRect:CGRectMake(0, 0, self.width, self.height )];
    }
}

@end
