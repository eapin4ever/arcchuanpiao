//
//  ZSTNewsDao.h
//  News
//  
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ZSTGlobal+EComA.h"

@interface ZSTDao(EComA)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)createTableIfNotExistForECAModule;


/**
 *	@brief 添加分类
 *
 *	@param 	categoryID      分类ID
 *	@param 	categoryName 	分类名称
 *	@param 	parentID        父分类ID
 *	@param 	orderNum        排序字段
 *  @param  description     描述
 *  @param  iconUrl         分类图标
 *	@return	操作是否成功
 */
- (BOOL)addCategory:(NSInteger)categoryID
       categoryName:(NSString *)categoryName
           parentID:(NSInteger)parentID
           orderNum:(NSInteger)orderNum
        description:(NSString *)description
            iconUrl:(NSString *)iconUrl;

/**
 *	@brief 删除分类
 *
 *	@return	操作是否成功
 */
- (BOOL)deleteAllProductCategories;

/**
 *	@param 	parentID      父分类ID，为0时取一级分类
 *	@brief  获取子分类
 *	@return	子分类
 */
- (NSArray *)getChildrenOfCategory:(NSInteger)parentID;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

@end
