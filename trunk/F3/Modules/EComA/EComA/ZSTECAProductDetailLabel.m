//
//  ZSTECAProductDetailLabel.m
//  ECA
//
//  Created by luobin on 12-10-11.
//
//

#import "ZSTECAProductDetailLabel.h"

@implementation ZSTECAProductDetailLabel

@synthesize isExpanded;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (CGFloat)suggestExpandHeight
{
    CGFloat height = [self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(self.width - 30, 1000)].height + 7;
    return MAX(ceil(height), 40);
}

- (void)collapse
{
    if (isExpanded) {
        isExpanded = NO;
        self.height = 40;
    }
}

- (void)expand
{
    if (!isExpanded) {
        isExpanded = YES;
        CGFloat detailLabelHeight = [self suggestExpandHeight];
        self.height = detailLabelHeight;
    }
}

- (CGRect)textRectForBounds:(CGRect)bounds limitedToNumberOfLines:(NSInteger)numberOfLines
{
    return UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(0, 7, isExpanded?0:7, 15));
}

- (void)drawTextInRect:(CGRect)rect
{
    [super drawTextInRect: [self textRectForBounds:rect limitedToNumberOfLines:self.numberOfLines]];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
//- (void)drawRect:(CGRect)rect
//{
//    [[ZSTModuleImage(@"ECA/product_detail_border.png") stretchableImageWithLeftCapWidth:4 topCapHeight:4] drawInRect:CGRectMake(10, 0, self.width - 20, self.height - (isExpanded?0:7))];
//    if (!isExpanded) {
//        [ZSTModuleImage(@"ECA/product_detail_bottomArrow.png") drawInRect:CGRectMake(self.width/2 - 7, self.height - 7, 14, 7)];
//    }
//    
//    // Drawing code
//    [super drawRect:rect];
//}

@end
