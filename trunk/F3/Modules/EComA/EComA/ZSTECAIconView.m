//
//  ZSTECAIconView.m
//
//  Created by luo bin on 11-6-29.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#define RADIANS(degrees) ((degrees * M_PI) / 180.0)
#import "ZSTECAIconView.h"
#import "ZSTECAProductTableViewController.h"
#import <QuartzCore/QuartzCore.h>

#define kIphoneBlacklabel 99
#define kIphoneWhiteLabel 98

@implementation ZSTECAIconView
@synthesize titleLabel;
@synthesize iconView;
@synthesize category;

- (void)initUI
{
    self.backgroundColor = [UIColor redColor];
    
    self.iconView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(5, 0, 50, 50)];
    self.iconView.adorn = ZSTModuleImage(@"module_ecoma_iconImageBg.png");
    self.iconView.imageInset = UIEdgeInsetsMake(2.5, 2.5, 2.5, 2.5);
    self.iconView.callbackOnSetImage = self;
    self.iconView.asynImageDelegate = self;
    self.iconView.userInteractionEnabled = NO;
    self.iconView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconView];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 55, 60, 10)];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = RGBCOLOR(193, 193, 193);
    self.titleLabel.highlightedTextColor = RGBCOLOR(120, 120, 120);
    self.titleLabel.font = [UIFont systemFontOfSize:10];
    self.titleLabel.shadowColor = [UIColor blackColor];
    self.titleLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
    [self addSubview:self.titleLabel];
    
    [self addTarget:self action:@selector(asynImageViewDidClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    self.category = [[NSNumber alloc] initWithInteger:0];
}

- (id)init
{
    self = [super init];
    if (self) {
        [self initUI];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:CGRectMake(frame.origin.x, frame.origin.y, 60, [[NSUserDefaults standardUserDefaults] boolForKey:showImgKey]?62:30)];
}

- (CGRect)frame
{
    return CGRectMake(super.frame.origin.x, super.frame.origin.y, 60, [[NSUserDefaults standardUserDefaults] boolForKey:showImgKey]?62:30);
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:showImgKey]) {
        self.showsTouchWhenHighlighted = NO;
        self.iconView.hidden = NO;
        self.iconView.frame = CGRectMake(5, 0, 50, 50);
        self.titleLabel.frame = CGRectMake(0, 55, 60, 10);
        self.titleLabel.font = [UIFont systemFontOfSize:10];
    } else {
        self.showsTouchWhenHighlighted = YES;
        self.iconView.hidden = YES;
        self.titleLabel.frame = CGRectMake(0, 10, 60, 12);
        self.titleLabel.font = [UIFont systemFontOfSize:12];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    self.iconView.highlighted = highlighted;
    self.titleLabel.highlighted = highlighted;
    [self setNeedsDisplay];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    self.iconView.selected = selected;
    [self setNeedsDisplay];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];
    self.iconView.enabled = enabled;
    [self setNeedsDisplay];
}

-(void)setIcon:(NSString *)imageUrl
{
    [self.iconView clear];
    self.iconView.url = [NSURL URLWithString:imageUrl];
    [self.iconView loadImage];
}

- (void)setTitle:(NSString *)title;
{
    titleLabel.text = title;
}

- (void)setCategoryID:(NSNumber *)number
{
    self.category = number;
}

#pragma mark HJManagedImageVDelegate

-(void) managedImageSet:(HJManagedImageV*)mi
{
//    if (mi.image != nil && _isHorizontal && mi.image.size.width != 0) {
//        _iconView.imageView.image = [self getImageFromNewRect:mi.image];
//    }else if(mi.image != nil && !_isHorizontal){
//        self.adorn = nil;
//    }
}

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{
    ZSTECAProductTableViewController *productTableViewController = [[ZSTECAProductTableViewController alloc] init];
    productTableViewController.categoryID = [self.category integerValue];
    productTableViewController.hidesBottomBarWhenPushed = YES;
    [self.viewController.navigationController pushViewController:productTableViewController animated:YES];
}

@end
