//
//  ZSTECAProductDetailLabel.h
//  ECA
//
//  Created by luobin on 12-10-11.
//
//

#import <UIKit/UIKit.h>

@interface ZSTECAProductDetailLabel : UILabel

@property (nonatomic, assign) BOOL isExpanded;

- (void)collapse;
- (void)expand;
    
@end
