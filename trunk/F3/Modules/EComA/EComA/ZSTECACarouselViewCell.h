//
//  ZSTECACarouselViewCell.h
//  ECA
//
//  Created by luobin on 12-10-9.
//
//

#import <UIKit/UIKit.h>
#import "ZSTECACarouselView.h"

@protocol ZSTECACarouselViewCellDelegate <NSObject>
@optional
- (void)carouselViewCell:(ZSTECACarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;
@end

@interface ZSTECACarouselViewCell : UITableViewCell<ZSTECACarouselViewDataSource,ZSTECACarouselViewDelegate>
{
    ZSTECACarouselView *carouselView;
    NSArray *dataArry;
}
@property (nonatomic, strong) ZSTECACarouselView *carouselView;
@property (nonatomic ,strong) NSArray *dataArry;
@property (nonatomic ,assign) id<ZSTECACarouselViewCellDelegate> carouselViewCellDelegate;
- (void)configCell:(NSArray*)array;

@end