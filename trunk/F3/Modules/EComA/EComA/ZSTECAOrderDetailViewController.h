//
//  ZSTECASubmitOrderViewController.h
//  ECA
//
//  Created by admin on 12-8-31.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine.h"
#import "ZSTECAMainCell.h"
#import "ZSTECAUserVO.h"
#import "ZSTECAProduct.h"

@interface ZSTECAOrderDetailViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UITextViewDelegate,UIGestureRecognizerDelegate,ZSTECAUserCellDelegate,UIScrollViewDelegate>
{
     CGFloat lastContentOffsetY;
}

@property (nonatomic ,retain) UIScrollView *bgScrollView;

@property (nonatomic ,retain) UITableView *orderTableView;
@property (nonatomic ,retain) UITableView *userTableView;
@property (nonatomic ,retain) UITableView *payTableView;

@property (nonatomic,retain) NSString *orderNum;
@property (nonatomic, retain) ZSTECAProduct *productInfo; 
@property (nonatomic, retain) ZSTECAUserVO *userInfo; 

@property (nonatomic ,retain) UIButton *submitBtn;


@end
