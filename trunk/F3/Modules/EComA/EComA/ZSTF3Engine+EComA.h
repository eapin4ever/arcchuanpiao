//
//  ZSTF3Engine+ECA.h
//  News
//
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTF3Engine.h"
#import "ZSTGlobal+EComA.h"
#import "ZSTECAProduct.h"

extern NSString* const showImgKey;

@protocol ZSTF3EngineECADelegate <ZSTF3EngineDelegate>

@optional

/**
 *	@brief	获取产品分类
 */
- (void)getProductCategoryDidSucceed;
- (void)getProductCategoryDidFailed:(int)resultCode;

/**
 *	@brief	获取轮播列表
 */
- (void)getCarouselDidSucceed:(NSArray *)carouseles;
- (void)getCarouselDidFailed:(int)resultCode;

/**
 *	@brief	获取产品列表
 */
- (void)getProductDidSucceed:(NSArray *)newsList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish;
- (void)getProductDidFailed:(int)resultCode;

/**
 *	@brief	获取产品内容
 */
- (void)getProductsContentDidSucceed:(NSDictionary *)newsContent;
- (void)getProductsContentDidFailed:(int)resultCode;

/**
 *	@brief	获取产品订单
 */
- (void)getProductsOrderDidSucceed:(NSDictionary *)orderDic;
- (void)getProductsOrderDidFailed:(int)resultCode;

/**
 *	@brief	获取订单列表
 */
- (void)getPocketShoppingDidSucceed:(NSArray *)list isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish;
- (void)getPocketShoppingDidFailed:(int)resultCode;

/**
 *	@brief	获取订单列表
 */
- (void)getMyFavoritesDidSucceed:(NSArray *)list isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish;
- (void)getMyFavoritesDidFailed:(int)resultCode;

/**
 *	@brief	获取订单详情
 */
- (void)getOrderDetailDidSucceed:(NSDictionary *)orderDetail;
- (void)getOrderDetailDidFailed:(int)resultCode;

/**
 *	@brief	举报
 */
- (void)submitReportDidSucceed;
- (void)submitReportDidFailed:(int)resultCode;

/**
 *	@brief	举报
 */
- (void)manageFavoritesDidSucceed:(NSString *)opType;
- (void)manageFavoritesDidFailed:(int)resultCode;

/**
 *	@brief	投诉
 */
- (void)addComplaintsDidSucceed;
- (void)addComplaintsDidFailed:(int)resultCode;

@end

@interface ZSTF3Engine (EComA)

/**
 *	@brief	获取新闻类别
 *
 */
- (void)getProductCategory;


///////////////////////////////////////////////////////////////////////// Private ///////////////////////////////////////////////////////////////////////

/**
 *	@brief	获取新闻列表
 *
 *	@param 	categoryID 	分类ID
 *	@param 	sinceID 	起始新闻ID
 *	@param 	orderType 	当OrderType为Desc时（刷新），从服务器获取MsgID大于SinceID的至多Size条记录；当OrderType为Asc时（更多），从服务器获取MsgID小于SinceID的至多Size条记录
 *	@param 	fetchCount 	获取最大条数
 *	@param 	iconType 	ICon图片类别，1：列表ICon；2：轮播ICon
 */
//- (void)getProduct:(NSInteger)categoryID
//        sinceID:(NSInteger)sinceID
//      orderType:(NSString *)orderType
//     fetchCount:(NSUInteger)fetchCount
//       iconType:(ZSTECAIconType)iconType;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief	获取轮播
 *
 */
- (void)getCarousel;

/**
 *	@brief	获取指定圈子下pageNum页的新闻
 * 
 *	@param 	pageNum 	页数
 *	@param 	productName 产品名称
 *	@param 	categoryID 	分类
 */
- (void)getProductAtPage:(int)pageNum category:(NSInteger)categoryID productName:(NSString *)productName sinceId:(NSInteger)sinceId;

/**
 *	@brief	获取新闻正文
 *
 *	@param 	newsID 	新闻ID
 */
- (void)getProductsContent:(NSInteger)newsID;
    
/**
 *	@brief	获取订单信息
 *
 *	@param 	newsID 	新闻ID
 */

- (void)getProductsUserInfo:(NSString *)userName
           address:(NSString *)address
           zipCode:(NSString *)zipCode
           mobile:(NSString *)mobile
           memo:(NSString *)memo
           count:(NSString *)count
               productsInfo:(ZSTECAProduct *)productsInfo;
//           productsContenInfo:(ZSTECAProductDetail *)productsContentInfo;


/**
 *	
 *  @brief	获取购物清单
 *	
 *	@param 	msisdn 	    手机号
 *	@param 	sinceID 	起始商品编号
 *	@param 	orderType 	当OrderType为Desc时（刷新），从服务器获取MsgID大于SinceID的至多Size条记录；当OrderType为Asc时（更多），从服务器获取MsgID小于SinceID的至多Size条记录
 *	@param 	fetchCount 	获取最大条数
 *	@param 	orderField 	排序字段如：添加时间
 */

- (void)getPocketShopping:(NSString *)msisdn
                  sinceID:(NSInteger)sinceID
                orderType:(NSString *)orderType
               fetchCount:(NSUInteger)fetchCount
               orderField:(NSString *)orderField;


/**
 *	@brief	获取该用户购物清单
 *
 *	@param 	pageNum 	页数
 *	@param 	msisdn 	    手机号码
 *	@param 	sinceId 	
 */
- (void)getPocketShoppingAtPage:(NSInteger)pageNum msisdn:(NSString *)msisdn sinceId:(NSInteger)sinceId;


/**
 *	
 *  @brief	获取用户收藏列表
 *	
 *	@param 	msisdn 	    手机号
 *	@param 	sinceID 	起始商品编号
 *	@param 	orderType 	当OrderType为Desc时（刷新），从服务器获取MsgID大于SinceID的至多Size条记录；当OrderType为Asc时（更多），从服务器获取MsgID小于SinceID的至多Size条记录
 *	@param 	fetchCount 	获取最大条数
 *	@param 	orderField 	排序字段如：添加时间
 */

- (void)getMyFavorites:(NSString *)msisdn
                  sinceID:(NSInteger)sinceID
                orderType:(NSString *)orderType
               fetchCount:(NSUInteger)fetchCount
               orderField:(NSString *)orderField;


/**
 *	@brief	获取用户收藏列表
 *
 *	@param 	pageNum 	页数
 *	@param 	msisdn 	    手机号码
 *	@param 	sinceId 	
 */
- (void)getMyFavoritesAtPage:(NSInteger)pageNum msisdn:(NSString *)msisdn sinceId:(NSInteger)sinceId;


/**
 *	@brief	获取订单详情
 *
 *	@param 	orderId 	订单号
 */

- (void)getOrderDetail:(NSString *)orderId;


/**
 *	@brief 投诉
 *
 *	@param 	content 	投诉内容
 *	@param 	productId 	产品id
 *	@param 	msisdn 	    手机号
 */
- (void)addComplaints:(NSString *)content 
            productId:(NSString *)productId
               msisdn:(NSString *)msisdn;

- (void)manageFavorites:(NSString *)productID msisdn:(NSString *)msisdn opType:(NSString *)opType;

@end
