//
//  ResourceURLProtocol.h
//  News
//
//  Created by luobin on 7/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

/**
 *	@brief	实现资源缓存
 *  
 */
#import "ZSTECATKResourceURL.h"

@interface ZSTECATKResourceURLProtocol : NSURLProtocol<NSURLConnectionDelegate> {
    
}

//@property (nonatomic, retain) NSURLConnection *connection;
//@property (nonatomic, retain) NSMutableData *data;

+ (NSString *)cacheDirectory;

+ (NSString *)cachePathForResourceURL:(ZSTECATKResourceURL *)url;

+ (void) registerProtocol;

@end
