//
//  NewsVO.m
//  F3Engine
//
//  Created by admin on 12-7-18.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTECAProduct.h"

@implementation ZSTECAProduct


@synthesize productName;//商品名称
@synthesize salesPrice;//现价
@synthesize primePrice;//原价
@synthesize price;//成交价

@synthesize summary;//副标题

@synthesize iconUrl;//图标图片地址
@synthesize carouselUrl;//轮播图地址
@synthesize addTime;//上架日期
@synthesize categoryID;//分类id
@synthesize productID;//商品id
@synthesize orderNum;//排列顺序
@synthesize orderID;//订单id

+ (id)voWithDic:(NSDictionary *)dic
{
    return [[self alloc] initWithDic: dic];
}

- (id)initWithDic:(NSDictionary *)dic
{
    if( (self=[super init])) {
        self.productName =  [dic safeObjectForKey:@"ProductName"];
        self.salesPrice = [dic safeObjectForKey:@"SalesPrice"];
        self.primePrice = [dic safeObjectForKey:@"PrimePrice"];
        self.price = [dic safeObjectForKey:@"Price"];
        
        self.primeLabel = [dic safeObjectForKey:@"PrimeLabel"];
        self.salesLabel = [dic safeObjectForKey:@"SalesLabel"];

        self.summary = [dic safeObjectForKey:@"Details"] ? [dic safeObjectForKey:@"Details"]: @"";
        self.iconUrl = [dic safeObjectForKey:@"IconUrl"];
        self.carouselUrl = [dic safeObjectForKey:@"CarouselUrl"];
        self.addTime = [dic safeObjectForKey:@"AddTime"];
        self.categoryID = [dic safeObjectForKey:@"CategoryID"];
        self.productID = [dic safeObjectForKey:@"ProductID"];
        self.orderNum = [dic safeObjectForKey:@"OrderNum"];
        self.orderID = [dic safeObjectForKey:@"OrderID"];
    }
    
    return self;
}


@end
