//
//  ZSTECAOrderDetailViewController.m
//  ECA
//
//  Created by admin on 12-8-31.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTECAOrderDetailViewController.h"
#import "ZSTUtils.h"
#import "ZSTECAMainCell.h"
#import "TKUIUtil.h"
#import "ZSTF3Engine+EComA.h"
#import "TKNetworkIndicatorView.h"

@interface ZSTECAOrderDetailViewController ()

@end

@implementation ZSTECAOrderDetailViewController

@synthesize submitBtn,bgScrollView,orderTableView,userTableView,productInfo,userInfo,payTableView,orderNum;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.view.layer.contents = (id) ZSTModuleImage(@"module_ecoma_submitOrder_bg.png").CGImage;    

    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:@"订单详情"];
    
    self.productInfo = [ZSTECAProduct voWithDic:nil];
    self.userInfo = [ZSTECAUserVO voWithDic:nil];
    
     lastContentOffsetY = 0.f;

    bgScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 460-44)];
    bgScrollView.backgroundColor = [UIColor clearColor];
    bgScrollView.userInteractionEnabled = YES;
    bgScrollView.delegate = self;
    [self.view addSubview:bgScrollView];
    
    orderTableView = [[UITableView alloc] initWithFrame:CGRectMake(5, 10, 310, 400) style:UITableViewStylePlain];
    orderTableView.delegate = self;
    orderTableView.dataSource = self;
    orderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    orderTableView.backgroundColor = [UIColor clearColor];
    orderTableView.userInteractionEnabled = NO;
    [bgScrollView addSubview:orderTableView];

    
    userTableView = [[UITableView alloc] initWithFrame:CGRectMake(18, CGRectGetMaxY(orderTableView.frame)+5, 310, 400) style:UITableViewStylePlain];
    userTableView.delegate = self;
    userTableView.dataSource = self;
    userTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    userTableView.backgroundColor = [UIColor clearColor];
    userTableView.userInteractionEnabled = YES;
    [bgScrollView addSubview:userTableView];

    
    payTableView = [[UITableView alloc] initWithFrame:CGRectMake(18, CGRectGetMaxY(userTableView.frame)+5, 310, 400) style:UITableViewStylePlain];
    payTableView.delegate = self;
    payTableView.dataSource = self;
    payTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    payTableView.backgroundColor = [UIColor clearColor];
    payTableView.userInteractionEnabled = YES;
    [bgScrollView addSubview:payTableView];
    
    [self.view newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkStatusView) {
        [self.engine getOrderDetail:self.orderNum];

    }];
}


#pragma mark - UIScrollViewDelegate


#pragma mark - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([touch.view isKindOfClass:[UIButton class]] || [touch.view isKindOfClass: [UIControl class]] ) 
    {    
        return NO;
    }
    return YES;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == orderTableView) {
        return 5;
    }else if(tableView == userTableView){
        return 5;
    }else {
        return 2;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier = @"cell";
    ZSTECAOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[ZSTECAOrderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if (tableView == orderTableView) {

        if (indexPath.row == 0) {
            cell.orderTextLabel.text = NSLocalizedString(@"商品订单", nil);        
        }else if (indexPath.row == 1) {
            cell.orderTextLabel.text = NSLocalizedString(@"订  单  号:", nil);        
            cell.orderdetailTextLabel.text = self.productInfo.orderID;
        }else if (indexPath.row == 2) {
            cell.orderTextLabel.text = NSLocalizedString(@"商品名称:", nil);        
            cell.orderdetailTextLabel.text = self.productInfo.productName;
        }else if(indexPath.row == 3){
            cell.orderTextLabel.text = NSLocalizedString(@"单      价 :", nil);     
            cell.orderdetailTextLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%@元", nil),self.productInfo.price];
        }else if(indexPath.row == 4){
            cell.orderTextLabel.text = NSLocalizedString(@"购买数量:", nil);        
            cell.orderdetailTextLabel.text = self.userInfo.count;
        }
    }else if (tableView == userTableView){
    
        if (indexPath.row == 0) {
            cell.orderTextLabel.text = NSLocalizedString(@"联系方式及收货地址", nil);  
        }else if (indexPath.row == 1) {
            cell.orderTextLabel.text = NSLocalizedString(@"姓      名 :", nil);  
            cell.orderdetailTextLabel.text = self.userInfo.name;
        }else if(indexPath.row == 2){
            cell.orderTextLabel.text = NSLocalizedString(@"地      址 :", nil); 
            cell.orderdetailTextLabel.text = self.userInfo.address;
        }else if(indexPath.row == 3){
            cell.orderTextLabel.text = NSLocalizedString(@"邮政编码:", nil);  
            cell.orderdetailTextLabel.text = self.userInfo.zipCode;
        }else if(indexPath.row == 4){
            cell.orderTextLabel.text = NSLocalizedString(@"手机号码:", nil);    
            cell.orderdetailTextLabel.text = self.userInfo.mobile;
        }
        
    }else {

        if (indexPath.row == 0) {
            cell.orderTextLabel.text = NSLocalizedString(@"支付结果", nil);  

        }else if(indexPath.row == 1){
            cell.orderTextLabel.text = self.userInfo.payStatus;  
        }
    }
    
    if(([tableView numberOfRowsInSection:indexPath.section]-1) == 0){
        cell.position = CustomCellPositionSingle;
    }
    else if(indexPath.row == 0){
        cell.position = CustomCellPositionTop;
    }
    else if (indexPath.row == ([tableView numberOfRowsInSection:indexPath.section]-1)){
        cell.position  = CustomCellPositionBottom;
    }
    else{
        cell.position = CustomCellPositionMiddle;
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{       
    float height = 0;
    NSString *str = @"";

    if (tableView == orderTableView) {
        
        if (indexPath.row == 0) {
            str = NSLocalizedString(@"商品订单", nil);
        }else if (indexPath.row == 1) {
            str = self.productInfo.orderID;
        }else if (indexPath.row == 2) {
            str = self.productInfo.productName;
        }else if(indexPath.row == 3){
            str = [NSString stringWithFormat:NSLocalizedString(@"%@元", nil),self.productInfo.price];
        }else if(indexPath.row == 4){
            str = self.userInfo.count;
        }
    }else if (tableView == userTableView) {
        
        if (indexPath.row == 0) {
            str = NSLocalizedString(@"联系方式及收货地址", nil);
        }else if (indexPath.row == 1) {
            str = self.userInfo.name;
        }else if (indexPath.row == 2) {
            str = self.userInfo.address;
        }else if(indexPath.row == 3){
            str = self.userInfo.zipCode;
        }else if(indexPath.row == 4){
            str = self.userInfo.mobile;
        }   
    }else{
        height = 40;
    }
    
    CGSize size = [str sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(190, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
    
    height = size.height;
    if (height < 36) {//两行字刚好是36
        height = 45;
    }else if(height != 40) {
        height += 20;
    }
    return height;
}

#pragma mark - ZSTF3EngineECADelegate

- (void)resetFrame
{    
    [orderTableView reloadData];
    [userTableView reloadData];
    [payTableView reloadData];
    
    
    CGRect rect1 = [orderTableView rectForSection:0];
    orderTableView.frame = CGRectMake(5, 10, 310, rect1.size.height);
    
    CGRect rect2 = [userTableView rectForSection:0];
    userTableView.frame = CGRectMake(5, CGRectGetMaxY(orderTableView.frame)+5, 310, rect2.size.height);
    
    CGRect rect3 = [payTableView rectForSection:0];
    payTableView.frame = CGRectMake(5, CGRectGetMaxY(userTableView.frame)+5, 310, rect3.size.height);
    
    bgScrollView.contentSize = CGSizeMake(320, CGRectGetMaxY(payTableView.frame)+10);
}

- (void)getOrderDetailDidSucceed:(NSDictionary *)orderDetail
{
    if ([orderDetail count]) {
        [self.view removeNetworkIndicatorView];
    }
    self.productInfo.productName = [orderDetail safeObjectForKey:@"ProductName"];
    self.productInfo.salesPrice = [orderDetail safeObjectForKey:@"SalesPrice"];
    self.productInfo.price = [orderDetail safeObjectForKey:@"Price"];

    self.productInfo.orderID = [orderDetail safeObjectForKey:@"OrderID"];
    self.userInfo.count = [orderDetail safeObjectForKey:@"Count"];
    self.userInfo.mobile = [orderDetail safeObjectForKey:@"Mobile"];
    self.userInfo.name = [orderDetail safeObjectForKey:@"userName"];
    self.userInfo.address = [orderDetail safeObjectForKey:@"Address"];
    self.userInfo.zipCode = [orderDetail safeObjectForKey:@"ZipCode"];
    NSString *payTypeStr = [orderDetail safeObjectForKey:@"PayType"];
    if ([payTypeStr isEqualToString:@"1"]) {
        self.userInfo.payType = ZSTECAPayType_alixPay;
    }else if([payTypeStr isEqualToString:@"2"]){
        self.userInfo.payType = ZSTECAPayType_IVR;
    }else if([payTypeStr isEqualToString:@"3"]){
        self.userInfo.payType = ZSTECAPayType_unionPay;
    }
    self.userInfo.payStatus = [orderDetail safeObjectForKey:@"Status"];
    
    [self resetFrame];
}

- (void)getOrderDetailDidFailed:(int)resultCode
{
    [self.view refreshFailed];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc
{
    [self.engine cancelAllRequest];
}

@end
