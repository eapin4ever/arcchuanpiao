//
//  ZSTECAProductTableViewController.m
//  ECA
//
//  Created by luobin on 9/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTECAProductTableViewController.h"
#import "ZSTUtils.h"
#import "ZSTECAProductCell.h"
#import "ChineseToPinyin.h"
#import "ZSTF3Engine+EComA.h"
#import "ZSTECAProductDetailViewController.h"
#import "ZSTSearchResultsViewController.h"
#import "TKNetworkIndicatorView.h"

@interface ZSTECAProductTableViewController ()

@end

@implementation ZSTECAProductTableViewController

@synthesize searchDC = _searchDC;
@synthesize searchBar = _searchBar;
@synthesize fliterKey;
@synthesize fliterListArr;
@synthesize categoryID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _isLoadingMore = NO;
    _hasMore = NO;

    _pagenum = 1;
    
    productArray = [NSMutableArray array];
    
    self.fliterKey = [NSMutableArray array];
    NSArray *array1 = [[NSUserDefaults standardUserDefaults] objectForKey:@"ECASearchKey"];
    [self.fliterKey addObjectsFromArray:array1];
    if (self.fliterKey == nil) {
        self.fliterKey = [NSMutableArray array];
    }

    self.fliterListArr = [NSMutableArray array];
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
    //colorWithPatternImage方法有bug，导致程序内存占用过大
    self.view.layer.contents = (id) ZSTModuleImage(@"module_ecoma_category.png").CGImage;
    
    productTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 460+(iPhone5?88:0)) style:UITableViewStylePlain];
    productTable.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    productTable.backgroundColor = [UIColor clearColor];
    productTable.delegate = self;
    productTable.dataSource = self;
    productTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    productTable.separatorColor = RGBCOLOR(197, 197, 197);
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    tableHeaderView.backgroundColor = [UIColor clearColor];
    productTable.tableHeaderView = tableHeaderView;
    [self.view addSubview:productTable];
    
    self.headerView  = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 45)];
    self.headerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.headerView];
	
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 44.0f)];
	self.searchBar.delegate = self;
	self.searchBar.placeholder = NSLocalizedString(@"输入查找内容" , nil);
	self.searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
	self.searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
	self.searchBar.keyboardType = UIKeyboardTypeDefault;
    [self.headerView addSubview:self.searchBar];
    
    // Create the search display controller
	self.searchDC = [[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self];
	self.searchDC.searchResultsDataSource = self;
	self.searchDC.searchResultsDelegate = self;	
    self.searchDC.delegate = self;

    if (_loadMoreView == nil) {
        _loadMoreView = [[TKLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
        _loadMoreView.backgroundColor = [UIColor clearColor];
        _loadMoreView.delegate = self;
        if (_hasMore) {
            productTable.tableFooterView = _loadMoreView;
        }
    }

    _fullScreenDelegate = [[YIFullScreenScroll alloc] initWithViewController:self];
    _fullScreenDelegate.shouldShowUIBarsOnScrollUp = YES;
    
    [self.view newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkStatusView) {
        [self.engine getProductAtPage:1 category:self.categoryID productName:nil sinceId:0];
    }];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc
{
    
}

- (void)fetchDataSuccess:(NSArray *)theData hasMore:(BOOL)hasMore
{
    _hasMore = hasMore;
    
    [self appendData:theData];
    
    if (_hasMore) {
        productTable.tableFooterView = _loadMoreView;
    } else {
        productTable.tableFooterView = nil;
    }
    
    if ([theData count] != 0) {
        [productTable reloadData];
    }
    
    [self finishLoading];

}

- (void)fetchDataFailed
{
    [self finishLoading];
}

- (void)appendData:(NSArray*)dataArray 
{    
    [productArray addObjectsFromArray:dataArray];

    _pagenum ++ ;

}

- (void)finishLoading
{
    [_loadMoreView loadMoreScrollViewDataSourceDidFinishedLoading:productTable];
    
    _isLoadingMore = NO;
}

- (void)loadMoreData{
    
    
    NSDictionary *dic = nil;
    
    dic = [productArray lastObject];
    
    //获取更多商品
    [self.engine getProductAtPage:_pagenum category:self.categoryID productName:nil sinceId:[[dic safeObjectForKey:@"ProductID"] integerValue]];
    
}


- (void)getProductDidSucceed:(NSArray *)newsList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish
{
    [self.view removeNetworkIndicatorView];
    [self fetchDataSuccess:newsList hasMore:hasMore];
}

- (void)getProductDidFailed:(int)resultCode;
{
    [self fetchDataFailed];
    [self.view refreshFailed];
}

#pragma mark - UISearchBarDelegate

- (BOOL)searchResult:(NSString *)contactName searchText:(NSString *)searchT{
	NSComparisonResult result = [contactName compare:searchT options:NSCaseInsensitiveSearch
											   range:NSMakeRange(0, searchT.length)];
	if (result == NSOrderedSame)
		return YES;
	else
		return NO;
}

-(NSMutableArray*)searchByCondition:(NSString*)condition//搜索功能实现
{	
    if (self.fliterListArr) {
        [self.fliterListArr removeAllObjects];
    }
    if (self.fliterKey) {
        [self.fliterKey removeAllObjects];
    }
    
    BOOL isAlpha = [condition isAlpha];
    
    BOOL isNumber = [condition isNumber];
    
    NSArray *array = [[NSUserDefaults standardUserDefaults] objectForKey:@"searchKey"];
    [self.fliterKey addObjectsFromArray:array];
    
    if (self.fliterKey == nil || ([self.fliterKey count] == 0)) {
        self.fliterKey = [NSMutableArray array];
    }
    for (NSString *key in self.fliterKey) 
    { 
        if(key !=nil && [self searchResult:key searchText:condition]){
            
            [self.fliterListArr addObject:key];
            
        }else if(isNumber && [self searchResult:key searchText:condition]){
            [self.fliterListArr addObject:key];
            
        }else if (isAlpha && key != nil) 
        {
            NSString *pinYin = [ChineseToPinyin pinyinFromChiniseString:key];
            if (pinYin !=nil && [self searchResult:pinYin searchText:[condition uppercaseString]]) {
                [self.fliterListArr addObject:key]; 
            }
            
        }
    }
    
    return self.fliterListArr;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    self.fliterListArr = [self searchByCondition:searchText];
    
    [_searchDC.searchResultsTableView reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)theSearchBar
{
    
    if ([(NSMutableArray *)fliterKey count] == 0) {
        [(NSMutableArray *)fliterKey addObject:theSearchBar.text];
    }else {
        BOOL isKeyExist = NO;
        for (int i=0; i<[fliterKey count]; i++) {
            NSString *key = [fliterKey objectAtIndex:i];
            if ([key isEqualToString:theSearchBar.text]) {
                isKeyExist = YES;
                break;
            }
        }
        
        if (!isKeyExist) {
            [fliterKey addObject:[NSString stringWithString:theSearchBar.text]];
        }
    }
    
    if ([fliterKey count] > 20) {
        for (int i = 0; i< ([fliterKey count]-20); i ++ ) {
            [fliterKey removeObjectAtIndex:i];
        }
    }
    
    NSArray *array = [fliterKey copy];    
    [[NSUserDefaults standardUserDefaults] setObject:array forKey:@"searchKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSString *filter = theSearchBar.text;
    if (filter == nil) {
        filter = @"";
    }

    ZSTSearchResultsViewController *searchResultsViewController = [[ZSTSearchResultsViewController alloc] init];
    searchResultsViewController.categoryID = self.categoryID;
    searchResultsViewController.searchText = filter;
    searchResultsViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:searchResultsViewController animated:YES];
}

#pragma mark - UISearchDisplayDelegate

-(void)searchDisplayControllerWillEndSearch:(UISearchDisplayController *)controller{
        
    _searchBar.showsCancelButton = NO;
    _searchBar.text = @"";
    
    if (IS_IOS_7) {
        
        CGRect frame = _searchBar.frame;
        frame.origin.y = 0;
        _searchBar.frame = frame;
    }
}

- (void) searchDisplayControllerWillBeginSearch:(UISearchDisplayController *)controller;
{
    _searchBar.showsCancelButton = YES;
    
    if (IS_IOS_7) {
        
        CGRect frame = _searchBar.frame;
        frame.origin.y = 20;
        _searchBar.frame = frame;
    }
}

#pragma mark - UITableViewDelegate


- (CGFloat)tableView:(UITableView *)theTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{   
    if (theTableView == productTable) {
        return 95;
    } else {
        return 40;
    }
}

- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (theTableView == _searchDC.searchResultsTableView) {
        
        UITableViewCell *cell = [theTableView cellForRowAtIndexPath:indexPath];
        self.searchBar.text = cell.textLabel.text;
        [self searchBarSearchButtonClicked:self.searchBar];
        [self.searchBar resignFirstResponder];
    } else {
        ZSTECAProductDetailViewController *productDetailViewController = [[ZSTECAProductDetailViewController alloc] init];
        productDetailViewController.products = productArray;
        productDetailViewController.hidesBottomBarWhenPushed = YES;
        productDetailViewController.currentProductIndex = indexPath.row;
        [self.navigationController pushViewController:productDetailViewController animated:YES];
    }
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    if (theTableView == productTable) {
        return (productArray == nil || ![productArray count])? 0 : [productArray count];
    }
    return (self.fliterListArr == nil || ![self.fliterListArr count])? 0 : [self.fliterListArr count];
    
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TKCellBackgroundView *custview = [[TKCellBackgroundView alloc] init];
    custview.fillColor = [UIColor clearColor];
    custview.borderColor = [UIColor colorWithWhite:0.78 alpha:1];
    custview.shadowColor = [UIColor whiteColor];
    
    if(([theTableView numberOfRowsInSection:indexPath.section]-1) == 0){
        custview.position = CustomCellBackgroundViewPositionSingle;
    }
    else if(indexPath.row == 0){
        custview.position = CustomCellBackgroundViewPositionTop;
    }
    else if (indexPath.row == ([theTableView numberOfRowsInSection:indexPath.section]-1)){
        custview.position  = CustomCellBackgroundViewPositionBottom;
    }
    else{
        custview.position = CustomCellBackgroundViewPositionMiddle;
    }

    
    if (theTableView == _searchDC.searchResultsTableView) {
        
        static NSString *identifier = @"filterCell";
        UITableViewCell *cell = [_searchDC.searchResultsTableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        _searchDC.searchResultsTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        
        
        if ([fliterListArr count] != 0) {
            cell.textLabel.text = [fliterListArr objectAtIndex:indexPath.row];
        }
        return cell;
        
    } else {
        static NSString *CellIdentifier = @"productCell";
        
        NSDictionary *productInfo = nil;
        ZSTECAProductCell *cell = [theTableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[ZSTECAProductCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.moduleType = self.moduleType;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;

        }
        cell.backgroundView = custview;
        
        if ([productArray count] != 0 && theTableView == productTable) {
            productInfo = (NSDictionary *)[productArray objectAtIndex:indexPath.row];
        } else if ([fliterListArr count] != 0 && theTableView == _searchDC.searchResultsTableView){
            productInfo = (NSDictionary *)[fliterListArr objectAtIndex:indexPath.row];
            _searchDC.searchResultsTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        }
        
        if (cell != nil) {
            cell.paySuccess = NO;
            [cell configCell:productInfo];
        }
        
        return cell;
    }
}

#pragma mark HHLoadMoreViewDelegate Methods
- (void)loadMoreDidTriggerRefresh:(TKLoadMoreView*)view
{
    if (_hasMore) {
        _isLoadingMore = YES;
        [self loadMoreData];
    }
    
}

- (BOOL)loadMoreDataSourceIsLoading:(TKLoadMoreView*)view
{
    return _isLoadingMore;
}

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [_fullScreenDelegate scrollViewWillBeginDragging:scrollView];
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    return [_fullScreenDelegate scrollViewShouldScrollToTop:scrollView];;
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView
{
    [_fullScreenDelegate scrollViewDidScrollToTop:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    [_loadMoreView loadMoreScrollViewDidEndDragging:scrollView];

}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [_loadMoreView loadMoreScrollViewDidEndDragging:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [_fullScreenDelegate scrollViewDidScroll:scrollView];
    
    [_loadMoreView loadMoreScrollViewDidScroll:scrollView];
}

@end
