//
//  ZSTECAProductPhotoView.h
//  ECA
//
//  Created by luobin on 12-10-10.
//
//

#import "TKHorizontalTableView.h"

@class ZSTECAProductPhotoView;
@protocol ZSTECAProductPhotoViewDelegate<NSObject>
@required
- (void)productPhotoView:(ZSTECAProductPhotoView *)cell didSelectAtIndex:(int)index;
@end


@interface ZSTECAProductPhotoView : UIView<UIScrollViewDelegate, TKAsynImageViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIButton *leftArrow;
@property (nonatomic, strong) UIButton *rightArrow;
@property (nonatomic, strong) TKPageControl *pageControl;
@property (nonatomic, strong) NSArray *photoURLs;
@property (nonatomic, assign) id<ZSTECAProductPhotoViewDelegate> delegate;

@end
