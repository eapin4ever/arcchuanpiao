//
//  ZSTECAProductTableViewController.h
//  ECA
//
//  Created by luobin on 9/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECAProductTableViewController : UIViewController<TKLoadMoreViewDelegate,UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate>
{
    UISearchBar *_searchBar;
    UISearchDisplayController *_searchDC;
    
    UITableView *productTable;
    
    TKLoadMoreView *_loadMoreView;
    
    BOOL _isLoadingMore;
    BOOL _hasMore;

    
    NSMutableArray *productArray;
    
    int _pagenum;
    
    YIFullScreenScroll* _fullScreenDelegate;

}
@property (nonatomic, assign) NSInteger categoryID;

@property (nonatomic, retain) UISearchBar *searchBar;
@property (nonatomic, retain) UISearchDisplayController *searchDC;

@property (nonatomic , retain) NSMutableArray *fliterListArr;
@property (nonatomic , retain) NSMutableArray *fliterKey;




@end
