//
//  ZSTECACategoryCell.h
//  ECA
//
//  Created by luobin on 12-9-29.
//
//

#import <UIKit/UIKit.h>

typedef enum {
    CustomCellPositionTop,
    CustomCellPositionMiddle,
    CustomCellPositionBottom,
    CustomCellPositionSingle,
    CustomCellPositionNone
} CustomCellPosition;

@interface ZSTECACategoryCell : UITableViewCell

@property(nonatomic) CustomCellPosition position;

@property (nonatomic, strong) TKAsynImageView *leftImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *detailTitleLabel;

- (void)configCell:(NSDictionary *)category forRowAtIndex:(NSIndexPath *)indexPath;

@end
