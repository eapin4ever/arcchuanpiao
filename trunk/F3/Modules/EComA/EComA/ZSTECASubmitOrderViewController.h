//
//  ZSTECASubmitOrderViewController.h
//  ECA
//
//  Created by admin on 12-9-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKAsynImageView.h"
#import "ZSTECAUserVO.h"
#import "ZSTECAProduct.h"
#import "ZSTECAMainCell.h"

@interface ZSTECASubmitOrderViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,ZSTECAUserCellDelegate,UIScrollViewDelegate>
{
    CGFloat lastContentOffsetY;
}

@property (nonatomic ,retain) UIScrollView *bgScrollView;
@property (nonatomic ,retain) TKAsynImageView *productImageView;
@property (nonatomic ,retain) UILabel *realPrice;
@property (nonatomic ,retain) UILabel *logolal;
@property (nonatomic ,retain) UILabel *floatlal;
@property (nonatomic ,retain) UILabel *originalPrice;
@property (nonatomic ,retain) UILabel *crossOutLine;
@property (nonatomic ,retain) UILabel *amountText;
@property (nonatomic ,retain) UITextField *amountField;
@property (nonatomic ,retain) UILabel *titleLabel;

@property (nonatomic ,retain) UITableView *userTableView;

@property (nonatomic ,retain) UIButton *submitBtn;

@property (nonatomic, retain) ZSTECAProduct *productInfo; 
@property (nonatomic, retain) ZSTECAUserVO *userInfo; 

@property (nonatomic ,retain) ZSTECAUserCell *currentCell;


@end
