//
//  ZSTECAMyMarketViewController.h
//  ECA
//
//  Created by xuhuijun on 12-9-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKAsynImageView.h"
#import "ZSTECAUserVO.h"

@interface ZSTECAMyMarketViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,EGORefreshTableHeaderDelegate,TKLoadMoreViewDelegate>
{
    UITableView *myFavoriteList;
    UITableView *pocketShoppingList;  
    
    YIFullScreenScroll* _fullScreenDelegate;
    
    EGORefreshTableHeaderView *_refreshHeaderView;
    TKLoadMoreView *_loadMoreView;
    
    BOOL _isRefreshing;
    BOOL _isLoadingMore;
    BOOL _favoriteHasMore;
    BOOL _shoppingHasMore;
    
    int _pagenum;
}

@property (nonatomic, strong) ZSTECAUserVO *userInfo;

@property (nonatomic ,strong) UITableView *myFavoriteList;
@property (nonatomic ,strong) UITableView *pocketShoppingList;

@property (nonatomic ,strong) NSMutableArray *myFavoriteArray;
@property (nonatomic ,strong) NSMutableArray *pocketShoppingArray;

@property (nonatomic ,strong) TKAsynImageView *userImageView;
@property (nonatomic ,strong) UILabel *userName;
@property (nonatomic ,strong) UILabel *shopingTimes;
@property (nonatomic ,strong) UILabel *registrationDate;

@property (nonatomic ,strong) UIButton *myFavoriteBtn;
@property (nonatomic ,strong) UIButton *pocketShoppingBtn;




@end
