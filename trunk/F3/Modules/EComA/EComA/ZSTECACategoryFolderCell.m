//
//  ZSTECACategoryFolderCell.m
//  ECA
//
//  Created by luobin on 12-9-29.
//
//

#import "ZSTECACategoryFolderCell.h"
#import "ZSTECAIconView.h"

@implementation ZSTECACategoryFolderCell

@synthesize iconViewes;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        self.iconViewes = [NSMutableArray arrayWithCapacity:4];
        
        // Initialization code
        for (int i = 0; i < 4; i++) {
            ZSTECAIconView *iconView = [[ZSTECAIconView alloc] initWithFrame:CGRectMake(16*(i + 1) + 60*i, 5, 60, [[NSUserDefaults standardUserDefaults] boolForKey:showImgKey]?62:30)];
            iconView.backgroundColor = [UIColor clearColor];
            [self.contentView addSubview:iconView];
            [self.iconViewes addObject:iconView];
            
            self.backgroundColor = [UIColor clearColor];
        }
    }
    return self;
}


- (void)configCell:(NSArray *)categories forRowAtIndex:(NSIndexPath *)indexPath
{
    for (int i = 0; i < 4; i++) {
        ZSTECAIconView *iconView = [self.iconViewes objectAtIndex:i];
        if (i < [categories count]) {
            NSDictionary *category = [categories objectAtIndex:i];
            iconView.hidden = NO;
            [iconView setIcon:[category safeObjectForKey:@"IconUrl"]];
            [iconView setTitle:[category safeObjectForKey:@"CategoryName"]];
            [iconView setCategoryID:[category safeObjectForKey:@"CategoryID"]];
        } else {
            iconView.hidden = YES;
        }
    }
}
@end
