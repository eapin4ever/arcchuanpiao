//
//  ZSTSearchResultsViewController.m
//  ECA
//
//  Created by admin on 12-10-11.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTSearchResultsViewController.h"
#import "ZSTUtils.h"
#import "ZSTECAProductCell.h"
#import "ZSTECAProductDetailViewController.h"
#import "TKNetworkIndicatorView.h"
#import "ZSTF3Engine+EComA.h"
#import "ZSTECAMainCell.h"

@interface ZSTSearchResultsViewController ()

@end

@implementation ZSTSearchResultsViewController

@synthesize searchResultsList;
@synthesize searchResultsArray;

@synthesize searchText;
@synthesize categoryID;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _isLoadingMore = NO;
    _hasMore = NO;
    
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:self.searchText];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.view.layer.contents = (id) ZSTModuleImage(@"module_ecoma_category.png").CGImage;    
    searchResultsList = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 460+(iPhone5?88:0)) style:UITableViewStylePlain];
    searchResultsList.delegate = self;
    searchResultsList.dataSource = self;
    searchResultsList.separatorStyle = UITableViewCellSeparatorStyleNone;
    searchResultsList.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    searchResultsList.backgroundColor = [UIColor clearColor];
    searchResultsList.userInteractionEnabled = YES;
    searchResultsList.hidden = NO;

    [self.view addSubview:searchResultsList];
    
    self.searchResultsArray = [NSMutableArray array];
    
    if (_loadMoreView == nil) {
        _loadMoreView = [[TKLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
        _loadMoreView.activityView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
        _loadMoreView.delegate = self;
        if (_hasMore) {
            searchResultsList.tableFooterView = _loadMoreView;
        }
    }
    
    [self.view newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkStatusView) {
        [self.engine getProductAtPage:1 category:self.categoryID productName:self.searchText sinceId:0];
        
    }];
}


- (void)finishLoading
{
    [_loadMoreView loadMoreScrollViewDataSourceDidFinishedLoading:searchResultsList];
    
    _isLoadingMore = NO;
}


- (void)loadMoreData {
    
    NSDictionary *dic = nil;
    dic = [searchResultsArray lastObject];
    
    //获取更多商品
    [self.engine getProductAtPage:_pagenum category:self.categoryID productName:self.searchText sinceId:[[dic safeObjectForKey:@"ProductID"] integerValue]];
    
}

- (void)appendData:(NSArray*)dataArray
{
    [self.searchResultsArray addObjectsFromArray:dataArray];

    _pagenum ++ ;
}


#pragma mark HHLoadMoreViewDelegate Methods
- (void)loadMoreDidTriggerRefresh:(TKLoadMoreView*)view
{
    if (_hasMore) {
        _isLoadingMore = YES;
        [self loadMoreData];
    }
}

- (BOOL)loadMoreDataSourceIsLoading:(TKLoadMoreView*)view
{
    return _isLoadingMore;
}


- (void)fetchDataSuccess:(NSArray *)theData isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore
{
    _hasMore = hasMore;

    [self appendData:theData];

    if (_hasMore) {
        searchResultsList.tableFooterView = _loadMoreView;
    } else {
        searchResultsList.tableFooterView = nil;
    }
    
    if ([theData count] != 0) {
        [searchResultsList reloadData];
    }
    
    [self finishLoading];
    
}

- (void)fetchDataFailed
{
    [self finishLoading];
}



#pragma mark - 

#pragma mark - UIScrollViewDelegate Methods

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    
    [_loadMoreView performSelector:@selector(loadMoreScrollViewDidEndDragging:) withObject:scrollView afterDelay:0];

}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [_loadMoreView performSelector:@selector(loadMoreScrollViewDidEndDragging:) withObject:scrollView afterDelay:0];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [_fullScreenDelegate scrollViewWillBeginDragging:scrollView];
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView
{
    return [_fullScreenDelegate scrollViewShouldScrollToTop:scrollView];;
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView
{
    [_fullScreenDelegate scrollViewDidScrollToTop:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    [_fullScreenDelegate scrollViewDidScroll:scrollView];
    
    [_loadMoreView performSelector:@selector(loadMoreScrollViewDidEndDragging:) withObject:scrollView afterDelay:0];    
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.searchResultsArray count] ? [self.searchResultsArray count] : 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TKCellBackgroundView *custview = [[TKCellBackgroundView alloc] init];
    custview.fillColor = [UIColor clearColor];
    custview.borderColor = [UIColor colorWithWhite:0.78 alpha:1];
    custview.shadowColor = [UIColor whiteColor];
    
    if(([tableView numberOfRowsInSection:indexPath.section]-1) == 0){
        custview.position = CustomCellBackgroundViewPositionSingle;
    }
    else if(indexPath.row == 0){
        custview.position = CustomCellBackgroundViewPositionTop;
    }
    else if (indexPath.row == ([tableView numberOfRowsInSection:indexPath.section]-1)){
        custview.position  = CustomCellBackgroundViewPositionBottom;
    }
    else{
        custview.position = CustomCellBackgroundViewPositionMiddle;
    }
    
    if ([searchResultsArray count] == 0) {
        static NSString *identifier = @"ZSTECAEmptyTableViewCell";
        ZSTECAEmptyTableViewCell *cell = [searchResultsList dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[ZSTECAEmptyTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        }
        cell.introduceLabel.text = NSLocalizedString(@"没有符合条件的商品" , nil);
        return cell;
    }else {
        static NSString *CellIdentifier = @"pocketShoppingCell";
        
        NSDictionary *productInfo = nil;
        
        ZSTECAProductCell *cell = [searchResultsList dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[ZSTECAProductCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cell.backgroundView = custview;
        
        if ([searchResultsArray count] != 0 ) {
            productInfo = (NSDictionary *)[searchResultsArray objectAtIndex:indexPath.row];
        }
        
        if (cell != nil && [productInfo count] != 0) {
            [cell configCell:productInfo];
        }
        
        return cell;
    }
}
#pragma mark - UITableViewDelegate


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    ZSTECAProductDetailViewController *productDetailViewController = [[ZSTECAProductDetailViewController alloc] init];
    productDetailViewController.products = searchResultsArray;
    productDetailViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:productDetailViewController animated:YES];
    productDetailViewController.hTableView.contentOffset = CGPointMake(320 * indexPath.row, 0);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    return 95;
}

#pragma mark - ZSTF3EngineECADelegate

- (void)getProductDidFailed:(int)resultCode;
{
    [self fetchDataFailed];
    
    [self.view refreshFailed];

}

- (void)getProductDidSucceed:(NSArray *)newsList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish
{
    [self.view removeNetworkIndicatorView];
    [self fetchDataSuccess:newsList isLoadMore:isLoadMore hasMore:hasMore];
}

- (void)dealloc
{
    [self.engine cancelAllRequest];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end
