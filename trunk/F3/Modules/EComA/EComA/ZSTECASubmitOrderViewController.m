//
//  ZSTECASubmitOrderViewController.m
//  ECA
//
//  Created by admin on 12-9-28.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTECASubmitOrderViewController.h"
#import "ZSTECAMainCell.h"
#import "ZSTECAConfirmPaymentViewController.h"
#import "ZSTUtils.h"

@interface ZSTECASubmitOrderViewController ()

@end

@implementation ZSTECASubmitOrderViewController

@synthesize bgScrollView,productImageView,realPrice,originalPrice,crossOutLine,amountText,amountField,titleLabel,logolal,floatlal;
@synthesize userTableView,submitBtn,productInfo,userInfo,currentCell;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.view.layer.contents = (id) ZSTModuleImage(@"module_ecoma_submitOrder_bg.png").CGImage;

    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"订单信息", nil)];
    
    lastContentOffsetY = 0;
    
    if (self.userInfo == nil) {
        self.userInfo = [ZSTECAUserVO voWithDic:[NSDictionary dictionary]];
    }
    
    bgScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 460-44-40 +(iPhone5?88:0))];
    bgScrollView.backgroundColor = [UIColor clearColor];
    bgScrollView.userInteractionEnabled = YES;
    bgScrollView.delegate = self;
    bgScrollView.alwaysBounceVertical = YES;
    [self.view addSubview:bgScrollView];
    
    UIImageView *productInfoBg = [[UIImageView alloc] initWithFrame:CGRectMake(6, 5, 308, 210)];
    productInfoBg.userInteractionEnabled = YES;
    productInfoBg.image = [ZSTModuleImage(@"module_ecoma_submitOrder_ProductBg.png") stretchableImageWithLeftCapWidth:6 topCapHeight:5];
    [bgScrollView addSubview:productInfoBg];
    
    self.productImageView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(15, 10, 80, 80)];
    self.productImageView.adjustsImageWhenHighlighted = NO;
    self.productImageView.backgroundColor = RGBCOLOR(239, 239, 239);
    self.productImageView.defaultImage = ZSTModuleImage(@"module_ecoma_loadingImg_80_80.png");
    self.productImageView.adorn = [[UIImage imageNamed:@"activity-photo-frame.png"] stretchableImageWithLeftCapWidth:4 topCapHeight:6];
    self.productImageView.imageInset = UIEdgeInsetsMake(2, 2.5, 3, 2.5);
    [self.productImageView clear];
    self.productImageView.url = [NSURL URLWithString:self.productInfo.iconUrl];
    [self.productImageView loadImage];
    [productInfoBg addSubview:self.productImageView];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(108, 15, 190, 32)];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.text = self.productInfo.productName;
    self.titleLabel.font = [UIFont systemFontOfSize:14];
    self.titleLabel.textColor = [UIColor blackColor];
    self.titleLabel.numberOfLines = 2;
    [productInfoBg addSubview:self.titleLabel];
    
    NSString *priceStr = [NSString stringWithFormat:@"%.2f",[self.productInfo.salesPrice floatValue]];
    NSArray *priceArray = [priceStr componentsSeparatedByString: @"."];
    
    self.logolal = [[UILabel alloc] initWithFrame:CGRectMake(108, self.titleLabel.bottom + 17, 10, 20)];
    self.logolal.backgroundColor = [UIColor clearColor];
    self.logolal.font = [UIFont systemFontOfSize:14];
    self.logolal.text = @"¥";
    self.logolal.textColor = RGBCOLOR(198, 30, 32);
    [productInfoBg addSubview:self.logolal];
    
    self.realPrice = [[UILabel alloc] initWithFrame:CGRectMake(120, self.titleLabel.bottom + 15, 50, 20)];
    self.realPrice.backgroundColor = [UIColor clearColor];
    self.realPrice.text = [NSString stringWithFormat:@"%@.",[priceArray objectAtIndex:0]];
    self.realPrice.font = [UIFont systemFontOfSize:20];
    self.realPrice.textColor = RGBCOLOR(198, 30, 32);
    [productInfoBg addSubview:self.realPrice];
    CGSize size = [self.realPrice.text sizeWithFont:self.realPrice.font];
    self.realPrice.width = size.width;
    
    self.floatlal = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.realPrice.frame), self.titleLabel.bottom + 13, 50, 20)];
    self.floatlal.backgroundColor = [UIColor clearColor];
    self.floatlal.textAlignment = NSTextAlignmentLeft;
    self.floatlal.text = [priceArray objectAtIndex:1];
    self.floatlal.font = [UIFont systemFontOfSize:14];
    self.floatlal.textColor = RGBCOLOR(198, 30, 32);
    [productInfoBg addSubview:self.floatlal];
    
    
    self.amountText = [[UILabel alloc] initWithFrame:CGRectMake(210, CGRectGetMaxY(titleLabel.frame)+15, 40, 22)];
    self.amountText.backgroundColor = [UIColor clearColor];
    self.amountText.text = NSLocalizedString(@"数量:", nil);
    self.amountText.font = [UIFont systemFontOfSize:14];
    self.amountText.textColor = [UIColor blackColor];
    [productInfoBg addSubview:self.amountText];
    
    self.amountField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(amountText.frame), CGRectGetMaxY(titleLabel.frame)+15, 40, 22)];
    self.amountField.borderStyle = UITextBorderStyleNone;
    self.amountField.background = ZSTModuleImage(@"module_ecoma_amountField.png");
    self.amountField.text = @"1";
    self.amountField.font = [UIFont systemFontOfSize:15];
    self.amountField.textColor = [UIColor darkGrayColor];
    self.amountField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.amountField.textAlignment = NSTextAlignmentCenter;
    self.amountField.keyboardType  = UIKeyboardTypeNumberPad;
    self.amountField.backgroundColor = [UIColor clearColor];
    [productInfoBg addSubview:self.amountField];
    
    userTableView = [[UITableView alloc] initWithFrame:CGRectMake(18, CGRectGetMaxY(titleLabel.frame), 280, 400) style:UITableViewStylePlain];
    userTableView.delegate = self;
    userTableView.dataSource = self;
    userTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    userTableView.backgroundColor = [UIColor clearColor];
    userTableView.userInteractionEnabled = YES;
    userTableView.scrollEnabled = NO;
    [bgScrollView addSubview:userTableView];
    
    submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [submitBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_btn_submitOrder_n.png") forState:UIControlStateNormal];
    [submitBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_btn_submitOrder_p.png") forState:UIControlStateHighlighted];
    [submitBtn addTarget:self action:@selector(submit:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:submitBtn];
    
    CGSize size1 = [originalPrice.text sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:CGSizeMake(180, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
    crossOutLine.frame = CGRectMake(CGRectGetMaxX(self.productImageView.frame) +18, CGRectGetMaxY(realPrice.frame)+16-8, size1.width+4, 1);
    
    productInfoBg.height = self.productImageView.top + self.productImageView.height + 10;
    
    
    CGRect rect2 = [userTableView rectForSection:0];
    
    userTableView.frame = CGRectMake(5, CGRectGetMaxY(productInfoBg.frame) + 5, 310, rect2.size.height+10);
    
    submitBtn.frame = CGRectMake(0, 376+(iPhone5?88:0), 320, 40);
    
    bgScrollView.contentSize = CGSizeMake(320, CGRectGetMaxY(userTableView.frame)-5);
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
}


-(void) keyboardWillShow:(NSNotification *)note{
    
    NSDictionary *info = [note userInfo];
    NSValue *value = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGSize keyboardSize = [value CGRectValue].size;
    
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    bgScrollView.frame = CGRectMake(0, 0, 320, 460-44-keyboardSize.height-44+(iPhone5?88:0));
    submitBtn.frame = CGRectMake(0, 378+(iPhone5?88:0)-keyboardSize.height, 320, 40);
    [UIView commitAnimations];
    
    CGRect rect = [userTableView convertRect:currentCell.frame toView:bgScrollView];
    [bgScrollView scrollRectToVisible:rect animated:YES];
}

- (void) keyboardWillHide:(NSNotification *)note{
    
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
    bgScrollView.frame = CGRectMake(0, 0, 320, 460-44-44+(iPhone5?88:0));
    submitBtn.frame = CGRectMake(0, 378+(iPhone5?88:0), 320, 40);
    // commit animations
	[UIView commitAnimations];
    
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.tracking && lastContentOffsetY > scrollView.contentOffset.y) {
        UIView *firstResponder = [bgScrollView findFirstResponder];
        [firstResponder resignFirstResponder];
    }
    lastContentOffsetY = scrollView.contentOffset.y;
}

#pragma mark- ZSTECAUserCellDelegate

- (void)ZSTECAUserCellShouldBeginEditing:(ZSTECAUserCell *)cell
{
    currentCell = (ZSTECAUserCell *)cell;
}

- (void)ZSTECAUserCellShouldChange:(ZSTECAUserCell *)cell
{
    if (cell.userdetailText.tag == 1) {
        self.userInfo.name = cell.userdetailText.text;
    }else if (cell.userdetailText.tag == 2){
        self.userInfo.address = cell.userdetailText.text;
    }else if (cell.userdetailText.tag == 3){
        self.userInfo.zipCode = cell.userdetailText.text;
    }else if (cell.userdetailText.tag == 4){
        self.userInfo.mobile = cell.userdetailText.text;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"cell";
    ZSTECAUserCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[ZSTECAUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.textLabel.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.delegate = self;
    
    if (tableView == userTableView){
        
        if (indexPath.row == 0) {
            cell.userTextLabel.text = NSLocalizedString(@"收货人信息", nil);
        }else if (indexPath.row == 1) {
            cell.userTextLabel.text = NSLocalizedString(@"姓      名 :", nil);
            cell.userdetailText.placeholder = NSLocalizedString(@"请输入姓名", nil);
        }else if(indexPath.row == 2){
            cell.userTextLabel.text = NSLocalizedString(@"地      址 :", nil);
            cell.userdetailText.placeholder = NSLocalizedString(@"请输入地址", nil);
        }else if(indexPath.row == 3){
            cell.userTextLabel.text = NSLocalizedString(@"邮政编码:", nil);
            cell.userdetailText.placeholder = NSLocalizedString(@"请输入邮编", nil);
            cell.userdetailText.keyboardType = UIKeyboardTypeNumberPad;
        }else if(indexPath.row == 4){
            cell.userTextLabel.text = NSLocalizedString(@"手机号码:", nil);
            cell.userdetailText.placeholder = NSLocalizedString(@"请输入手机号码", nil);
            cell.userdetailText.keyboardType = UIKeyboardTypeNumberPad;
        }
        cell.userdetailText.tag = indexPath.row;
        
    }
    
    if(([tableView numberOfRowsInSection:indexPath.section]-1) == 0){
        cell.position = CustomCellPositionSingle;
    }
    else if(indexPath.row == 0){
        cell.position = CustomCellPositionTop;
    }
    else if (indexPath.row == ([tableView numberOfRowsInSection:indexPath.section]-1)){
        cell.position  = CustomCellPositionBottom;
    }
    else{
        cell.position = CustomCellPositionMiddle;
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;
{
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 4) {
        return 50.0f;
    }
    return 45.0f;
}

- (void)submit:(UIButton *)sender
{
    [self scrollViewDidScroll:bgScrollView];
    
    UIView *window = self.view;
    if ([[UIApplication sharedApplication] keyboardView] != nil) {
        window = [[UIApplication sharedApplication] keyboardView].window;
    }
    
    //判断收货人信息是否完整
    if ([self.amountField.text length] == 0 || self.amountField.text == nil) {
        [TKUIUtil alertInView:window withTitle:NSLocalizedString(@"请输入购买数量", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]];
        return;
    }
    
    if ([self.userInfo.name length] == 0 || self.userInfo.name == nil) {
        [TKUIUtil alertInView:window withTitle:NSLocalizedString(@"请输入姓名", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]];
        return;
    }
    if ([self.userInfo.address length] == 0 || self.userInfo.address == nil) {
        [TKUIUtil alertInView:window withTitle:NSLocalizedString(@"请输入地址", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]];
        return;
    }
    if ([self.userInfo.zipCode length] == 0 || self.userInfo.zipCode == nil) {
        [TKUIUtil alertInView:window withTitle:NSLocalizedString(@"请输入邮编", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]];
        return;
    }
    if ([self.userInfo.mobile length] == 0 || self.userInfo.mobile == nil) {
        [TKUIUtil alertInView:window withTitle:NSLocalizedString(@"请输入手机号码", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]];
        return;
    }else if ([self.userInfo.mobile length] != 11)
    {
        [TKUIUtil alertInView:window withTitle:NSLocalizedString(@"请输入正确手机号", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]];
        return;
    }
    
    [self.engine getProductsUserInfo:self.userInfo.name
                             address:self.userInfo.address
                             zipCode:self.userInfo.zipCode
                              mobile:self.userInfo.mobile
                                memo:self.userInfo.memo
                               count:self.amountField.text
                        productsInfo:self.productInfo];
    
    
    [TKUIUtil showHUD:window withText:NSLocalizedString(@"正在提交订单", nil)];
    
}

#pragma mark - ZSTF3EngineECADelegate

- (void)getProductsOrderDidSucceed:(NSDictionary *)orderDic
{
    [TKUIUtil hiddenHUD];
    
    NSString * orderID = [orderDic safeObjectForKey:@"OrderID"];
    
    if ([orderID length] != 0 && orderID != nil) {
        
        ZSTECAConfirmPaymentViewController *confirmPay = [[ZSTECAConfirmPaymentViewController alloc] init];
        confirmPay.buyCount = self.amountField.text;
        confirmPay.orderNum = orderID;
        confirmPay.orderProductInfo = self.productInfo;
        confirmPay.orderUserInfo = self.userInfo;
        
        confirmPay.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:confirmPay animated:YES];
    }
}

- (void)getProductsOrderDidFailed:(int)resultCode
{
    UIView *window = self.view;
    if ([[UIApplication sharedApplication] keyboardView] != nil) {
        window = [[UIApplication sharedApplication] keyboardView].window;
    }
    
    [TKUIUtil showHUD:window withText:NSLocalizedString(@"订单提交失败！", nil)];
    [TKUIUtil hiddenHUDAfterDelay:2];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self.engine cancelAllRequest];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
