//
//  ZSTECAProductCell.m
//  ECA
//
//  Created by luobin on 9/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTECAProductCell.h"

@implementation ZSTECAProductCell

@synthesize productImageView;
@synthesize primePriceLabel;
@synthesize salesPriceLabel;
@synthesize descriptionLabel;
@synthesize crossOutLine;
@synthesize timeLabel;
@synthesize paySuccess;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.productImageView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(10, 7, 80, 80)];//70 52.5
        self.productImageView.userInteractionEnabled = NO;
        self.productImageView.backgroundColor  = [UIColor clearColor];
        self.productImageView.defaultImage = ZSTModuleImage(@"module_ecoma_loadingImg_80_80.png");
        productImageView.adorn = [ZSTModuleImage(@"module_ecoma_productAbornImage.png") stretchableImageWithLeftCapWidth:40 topCapHeight:30];
        productImageView.imageInset = UIEdgeInsetsMake(2, 2, 2, 2);
        [self.contentView addSubview:productImageView];
        
        descriptionLabel = [[ZSTVerticallyAlignedLabel alloc] initWithFrame:CGRectMake(98, 12, 213, 38)];
        descriptionLabel.numberOfLines = 2;
        descriptionLabel.backgroundColor = [UIColor clearColor];
//        descriptionLabel.backgroundColor = [UIColor whiteColor];
        descriptionLabel.text = @"";
        descriptionLabel.font = [UIFont systemFontOfSize:16];
        descriptionLabel.textColor = RGBCOLOR(95, 95, 95);
        [self.contentView addSubview:descriptionLabel];
        
        salesPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(98, 60, 110, 20)];
        salesPriceLabel.backgroundColor = [UIColor clearColor];
        salesPriceLabel.text = @"";
        salesPriceLabel.font = [UIFont systemFontOfSize:13];
        salesPriceLabel.textColor = RGBCOLOR(255, 0, 2);
        [self.contentView addSubview:salesPriceLabel];
        
        primePriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(salesPriceLabel.frame)+10, 60, 110, 20)];
        primePriceLabel.backgroundColor = [UIColor clearColor];
        primePriceLabel.text = @"";
        primePriceLabel.font = [UIFont systemFontOfSize:12];
        primePriceLabel.textColor = [UIColor grayColor];
        [self.contentView addSubview:primePriceLabel];
        
        self.crossOutLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 11, 110, 1)];
        self.crossOutLine.backgroundColor = [UIColor grayColor];
        [primePriceLabel addSubview:self.crossOutLine];
        
        timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(93, CGRectGetMaxY(salesPriceLabel.frame), 220, 20)];
        timeLabel.backgroundColor = [UIColor clearColor];
        timeLabel.text = @"";
        timeLabel.font = [UIFont systemFontOfSize:12];
        timeLabel.textColor = [UIColor grayColor];
        timeLabel.hidden = YES;
        [self.contentView addSubview:timeLabel];
    
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCell:(NSDictionary *)cellInfo
{
    CGFloat primePrice = [[cellInfo safeObjectForKey:@"PrimePrice"] floatValue];
    CGFloat salesPrice = [[cellInfo safeObjectForKey:@"SalesPrice"] floatValue];
    CGFloat price = [[cellInfo safeObjectForKey:@"Price"] floatValue];
    
    if (!paySuccess) {
        primePriceLabel.hidden = NO;
        salesPriceLabel.text = [NSString stringWithFormat:@"¥%.2f", salesPrice];
        primePriceLabel.text = [NSString stringWithFormat:@"¥%.2f", primePrice];
    }else {
        primePriceLabel.hidden = YES;
        salesPriceLabel.text = [NSString stringWithFormat:@"¥%.2f", price];
    }
   
    NSString *payTime = [cellInfo safeObjectForKey:@"OrderTime"];
    timeLabel.hidden = YES;
    if (payTime != nil && ![payTime isEmptyOrWhitespace]) {
        timeLabel.hidden = NO;
        timeLabel.text = [NSString stringWithFormat:NSLocalizedString(@"成交时间:%@" , nil), payTime];
    }
    
    descriptionLabel.text = [cellInfo safeObjectForKey:@"ProductName"];
    [productImageView clear];
    productImageView.url = [NSURL URLWithString:[cellInfo safeObjectForKey:@"IconUrl"]];
    [productImageView loadImage];
    
    CGSize size1 = [salesPriceLabel.text sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:CGSizeMake(110, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
    if (!paySuccess) {
        salesPriceLabel.frame = CGRectMake(100, 65, size1.width, 20);
    } else {
        salesPriceLabel.frame = CGRectMake(100, 52, size1.width, 14);
    }
    
    CGSize size2 = [primePriceLabel.text sizeWithFont:[UIFont systemFontOfSize:12] constrainedToSize:CGSizeMake(110, CGFLOAT_MAX) lineBreakMode:NSLineBreakByCharWrapping];
    primePriceLabel.frame = CGRectMake(308 - size2.width, 65, size2.width, 20);
    crossOutLine.frame = CGRectMake(0, 11, size2.width, 1);
    
    timeLabel.frame = CGRectMake(100, CGRectGetMaxY(salesPriceLabel.frame)+2, 220, 14);
}

- (void)dealloc
{
    
}

@end
