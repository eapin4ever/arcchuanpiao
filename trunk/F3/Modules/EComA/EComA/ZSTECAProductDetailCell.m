//
//  ZSTECAProductDetailCell.m
//  ECA
//
//  Created by luobin on 12-10-10.
//
//

#import "ZSTECAProductDetailCell.h"
#import "ZSTECASubmitOrderViewController.h"
#import "ZSTECAProductDetailLabel.h"
#import "TKNetworkIndicatorView.h"
#import "ZSTECAReportViewController.h"
#import "ZSTECAProductDetailViewController.h"

@implementation ZSTECAProductDetailCell

@synthesize productInfo;

@synthesize callBtn;
@synthesize favoriteBtn;
@synthesize buyBtn;
@synthesize callPhoneWebView;
@synthesize scrollView;
@synthesize productPhotoView;
@synthesize descriptionLabel;
@synthesize detailLabel;
@synthesize salesPricePrefixLabel;
@synthesize salesPriceLabel;
@synthesize salesPriceFloatLabel;
@synthesize primePricePrefixLabel;
@synthesize primePriceLabel;
@synthesize crossOutLine;
@synthesize complaintBtn;

@synthesize staffLabel;
@synthesize phoneLabel;

@synthesize isFavorite;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        isFavorite = NO;
        isManage = NO;
        
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 377+(iPhone5?88:0))];
        scrollView.backgroundColor = [UIColor clearColor];
        //        scrollView.delegate = self;
        scrollView.pagingEnabled = NO;
        [self addSubview:scrollView];
        
        //轮播图
        productPhotoView = [[ZSTECAProductPhotoView alloc] initWithFrame:CGRectMake(4, 5, 171, 131)];
        productPhotoView.layer.contents = (id) ZSTModuleImage(@"module_ecoma_product_bg.png").CGImage;
        productPhotoView.delegate = self;
        [scrollView addSubview:productPhotoView];
        
        
        //价格
        priceBg = [[UIImageView alloc] initWithFrame:CGRectMake(4, 220, 310, 57)];
        priceBg.image = ZSTModuleImage(@"module_ecoma_product_price_bg.png");
        [scrollView addSubview:priceBg];
        
        descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 4, 300, 50)];
        descriptionLabel.numberOfLines = 0;
        descriptionLabel.backgroundColor = [UIColor clearColor];
        descriptionLabel.font = [UIFont systemFontOfSize:14];
        descriptionLabel.textColor = RGBCOLOR(95, 95, 95);
        [scrollView addSubview:descriptionLabel];
        
        salesPricePrefixLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 58, 24)];
//        salesPricePrefixLabel.text = @"促销：￥";
        salesPricePrefixLabel.backgroundColor = [UIColor clearColor];
        salesPricePrefixLabel.font = [UIFont systemFontOfSize:14];
        salesPricePrefixLabel.textColor = RGBCOLOR(191, 0, 2);
        [scrollView addSubview:salesPricePrefixLabel];
        
        salesPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(54, 0, 120, 40)];
        salesPriceLabel.backgroundColor = [UIColor clearColor];
        salesPriceLabel.font = [UIFont systemFontOfSize:27];
        salesPriceLabel.textColor = RGBCOLOR(191, 0, 2);
        [scrollView addSubview:salesPriceLabel];
        
        salesPriceFloatLabel = [[UILabel alloc] initWithFrame:CGRectMake(94, 0, 60, 24)];
        salesPriceFloatLabel.backgroundColor = [UIColor clearColor];
        salesPriceFloatLabel.font = [UIFont systemFontOfSize:14];
        salesPriceFloatLabel.textColor = RGBCOLOR(191, 0, 2);
        salesPriceFloatLabel.textAlignment = NSTextAlignmentLeft;
        [scrollView addSubview:salesPriceFloatLabel];
        
        primePricePrefixLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 52, 24)];
//        primePricePrefixLabel.text = @"原价：￥";
        primePricePrefixLabel.backgroundColor = [UIColor clearColor];
        primePricePrefixLabel.font = [UIFont systemFontOfSize:14];
        primePricePrefixLabel.textColor = RGBCOLOR(158, 158, 158);
        [scrollView addSubview:primePricePrefixLabel];
        
        primePriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 320, 24)];
        primePriceLabel.backgroundColor = [UIColor clearColor];
        primePriceLabel.font = [UIFont systemFontOfSize:14];
        primePriceLabel.textColor = RGBCOLOR(158, 158, 158);
        [scrollView addSubview:primePriceLabel];
        
        self.crossOutLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 1)];
        self.crossOutLine.backgroundColor = [UIColor colorWithWhite:0.7 alpha:1];
        [scrollView addSubview:self.crossOutLine];
        
        self.favoriteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        favoriteBtn.frame = CGRectMake(205, 284, 40, 40);
        [favoriteBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_bottom_favorite_buttn_n.png") forState:UIControlStateNormal];
        [favoriteBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_bottom_favorite_buttn_p.png") forState:UIControlStateHighlighted];
        favoriteBtn.enabled = NO;
        [favoriteBtn addTarget:self action:@selector(manageAction) forControlEvents:UIControlEventTouchUpInside];
        [scrollView addSubview:favoriteBtn];
        
        self.complaintBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        complaintBtn.frame = CGRectMake(255, 284, 40, 40);
        [complaintBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_bottom_complaint_btn_n.png") forState:UIControlStateNormal];
        [complaintBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_bottom_complaint_btn_p.png") forState:UIControlStateHighlighted];
        complaintBtn.enabled = NO;
        [complaintBtn addTarget:self action:@selector(addComplaintsAction) forControlEvents:UIControlEventTouchUpInside];
        [scrollView addSubview:complaintBtn];
        
        //详情标题
        detailTitleBg = [[UIImageView alloc] initWithFrame:CGRectMake(4, 343, 312, 38)];
        detailTitleBg.image = ZSTModuleImage(@"module_ecoma_product_title_bg.png");
        [scrollView addSubview:detailTitleBg];
        
        detailTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 352, 40, 20)];
        detailTitleLabel.text = NSLocalizedString(@"详情" , nil);
        detailTitleLabel.textColor = [UIColor blackColor];
        detailTitleLabel.backgroundColor = [UIColor clearColor];
        detailTitleLabel.font = [UIFont systemFontOfSize:16];
        [scrollView addSubview:detailTitleLabel];
        
        //商品详情
        detailBg = [[UIImageView alloc] initWithFrame:CGRectMake(4, 381, 312, 50)];
        detailBg.image = ZSTModuleImage(@"module_ecoma_product_common_bg.png");
        [scrollView addSubview:detailBg];
        detailBg.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(expandDetail)];
        [detailBg addGestureRecognizer:tapGestureRecognizer];
        
        detailLabel = [[ZSTECAProductDetailLabel alloc] initWithFrame:CGRectMake(4, 0, 270, 40)];
        detailLabel.lineBreakMode = UILineBreakModeWordWrap;
        detailLabel.numberOfLines = 0;
        detailLabel.backgroundColor = [UIColor clearColor];
        detailLabel.font = [UIFont systemFontOfSize:14];
        detailLabel.textColor = RGBCOLOR(158, 158, 158);
        [scrollView addSubview:detailLabel];
        
        arrowImage = [[UIImageView alloc] initWithFrame:CGRectMake(295, 0, 9, 9)];
        arrowImage.image = ZSTModuleImage(@"module_ecoma_product_detail_down_arrow.png");
        [scrollView addSubview:arrowImage];
        
        //联系方式
        contactBg = [[UIImageView alloc] initWithFrame:CGRectMake(4, 427, 312, 50)];
        contactBg.image = ZSTModuleImage(@"module_ecoma_product_common_bg.png");
        [scrollView addSubview:contactBg];
        
        contactImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 18, 18)];
        contactImage.image = ZSTModuleImage(@"module_ecoma_product_contact.png");
        [scrollView addSubview:contactImage];
        
        staffLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 0, 160, 16)];
        staffLabel.backgroundColor = [UIColor clearColor];
        staffLabel.font = [UIFont systemFontOfSize:14];
        staffLabel.textColor = RGBCOLOR(95, 95, 95);
        [scrollView addSubview:staffLabel];
        
        serviceCallImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 18, 18)];
        serviceCallImage.image = ZSTModuleImage(@"module_ecoma_product_service_call.png");
        [scrollView addSubview:serviceCallImage];
        
        phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(35, 0, 160, 16)];
        phoneLabel.textAlignment = NSTextAlignmentLeft;
        phoneLabel.backgroundColor = [UIColor clearColor];
        phoneLabel.font = [UIFont systemFontOfSize:14];
        phoneLabel.textColor = RGBCOLOR(95, 95, 95);
        [scrollView addSubview:phoneLabel];
        
        self.callBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        callBtn.frame = CGRectMake(120, 0, 40, 40);
        [callBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_bottom_call_buttn_n.png") forState:UIControlStateNormal];
        [callBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_bottom_call_buttn_p.png") forState:UIControlStateHighlighted];
        [callBtn addTarget:self action:@selector(callAction) forControlEvents:UIControlEventTouchUpInside];
        callBtn.enabled = NO;
        [scrollView addSubview:callBtn];
        
        endImage = [[UIImageView alloc] initWithFrame:CGRectMake(4, 0, 312, 4)];
        endImage.image = ZSTModuleImage(@"module_ecoma_product_detail_end_image.png");
        [scrollView addSubview:endImage];
        
        //分割线
        separator1 = [[UIImageView alloc] initWithFrame:CGRectMake(6, 381, 308, 1)];
        separator1.image = ZSTModuleImage(@"module_ecoma_product_detail_dash.png");
        [scrollView addSubview:separator1];
        
        //分割线
        separator2 = [[UIImageView alloc] initWithFrame:CGRectMake(6, 376, 308, 1)];
        separator2.image = ZSTModuleImage(@"module_ecoma_product_detail_dash.png");
        [scrollView addSubview:separator2];
    }
    return self;
}

- (void)productPhotoView:(ZSTECAProductPhotoView *)cell didSelectAtIndex:(int)index;
{
    if(self.delegate&&[self.delegate respondsToSelector:@selector(productDetailCell:didSelectAtIndex:)])
    {
        [self.delegate productDetailCell:self didSelectAtIndex:0];
    }
}


- (void)expandDetail
{
    if (!detailLabel.isExpanded) {
        [UIView beginAnimations:@"animation" context:nil];
        [UIView setAnimationDuration:0.3];
        [UIView setAnimationBeginsFromCurrentState:YES];
        [detailLabel expand];
        
        arrowImage.image = ZSTModuleImage(@"module_ecoma_product_detail_up_arrow.png");
        
        //详情
        detailBg.frame = CGRectMake(4, detailTitleBg.bottom, 312, detailLabel.bottom-detailLabel.top+10);
        detailLabel.top = detailBg.top + 5;
        detailLabel.bottom = detailBg.bottom - 5;
        
        arrowImage.top = detailBg.top + 15;
        
        separator2.top = detailBg.bottom;
        
        //联系人
        contactBg.top = detailBg.bottom;
        staffLabel.top = contactBg.top + 5;
        contactImage.top = contactBg.top + 5;
        phoneLabel.top = staffLabel.bottom + 5;
        serviceCallImage.top = staffLabel.bottom + 5;
        
        self.callBtn.top = contactBg.top + 5;
        self.callBtn.right = 296;
        
        endImage.top = contactBg.bottom;
        [UIView commitAnimations];
        
        self.scrollView.contentSize = CGSizeMake(320, contactBg.bottom + 10);
    } else {
        [UIView beginAnimations:@"animation" context:nil];
        [UIView setAnimationDuration:0.3];
        [detailLabel collapse];
        
        arrowImage.image = ZSTModuleImage(@"module_ecoma_product_detail_down_arrow.png");
        //详情
        detailBg.frame = CGRectMake(4, detailTitleBg.bottom, 312, detailLabel.bottom-detailLabel.top+10);
        detailLabel.top = detailBg.top + 5;
        detailLabel.bottom = detailBg.bottom - 5;
        
        arrowImage.top = detailBg.top + 15;
        
        separator2.top = detailBg.bottom;
        
        //联系人
        contactBg.top = detailBg.bottom;
        staffLabel.top = contactBg.top + 5;
        contactImage.top = contactBg.top + 5;
        phoneLabel.top = staffLabel.bottom + 5;
        serviceCallImage.top = staffLabel.bottom + 5;
        
        self.callBtn.top = contactBg.top + 5;
        self.callBtn.right = 296;
        
        endImage.top = contactBg.bottom;
        [UIView commitAnimations];
        
        self.scrollView.contentSize = CGSizeMake(320, contactBg.bottom + 10);
    }
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    
    descriptionLabel.height = [descriptionLabel.text sizeWithFont:descriptionLabel.font constrainedToSize:CGSizeMake(descriptionLabel.width, 100)].height;
    int line = descriptionLabel.height / 16;
    if (line > 2) {
        line = 2;
    }
    descriptionLabel.height = line * 16;
    descriptionLabel.frame = CGRectMake(10, priceBg.top+5, 300, line*20);
    
    favoriteBtn.top = descriptionLabel.bottom + 10;
    complaintBtn.top = descriptionLabel.bottom + 10;
    
    salesPricePrefixLabel.top = descriptionLabel.bottom + 14;
    salesPriceLabel.top = descriptionLabel.bottom+8;
    
    CGFloat salesPriceLabelWidth = [salesPriceLabel.text sizeWithFont:salesPriceLabel.font constrainedToSize:CGSizeMake(200, 40)].width;
    salesPriceLabel.frame = CGRectMake(salesPricePrefixLabel.right, salesPriceLabel.top, salesPriceLabelWidth, 30);
    
    salesPriceFloatLabel.left = salesPriceLabel.right;
    salesPriceFloatLabel.top = salesPriceLabel.top;
    
    CGFloat primePrefixLabelWidth = [primePricePrefixLabel.text sizeWithFont:primePricePrefixLabel.font constrainedToSize:CGSizeMake(100, 15)].width;
    primePricePrefixLabel.frame = CGRectMake(10, descriptionLabel.bottom+35, primePrefixLabelWidth, 20);
    
    CGFloat primePriceLabelWidth = [primePriceLabel.text sizeWithFont:primePriceLabel.font constrainedToSize:CGSizeMake(100, 15)].width;
    primePriceLabel.frame = CGRectMake(primePricePrefixLabel.right, descriptionLabel.bottom+35, primePriceLabelWidth, 20);
    
    crossOutLine.frame = CGRectMake(primePricePrefixLabel.right + 2, primePriceLabel.top + 10, 1 + primePriceLabelWidth, 1);
    
    priceBg.frame = CGRectMake(priceBg.left, priceBg.top, 310.5, primePriceLabel.bottom-priceBg.top+5);
    
    detailTitleBg.top = priceBg.bottom + 5;
    detailTitleLabel.top = detailTitleBg.top + 9;
    
    separator1.top = detailTitleBg.bottom;
    
    //详情
    detailBg.frame = CGRectMake(4, detailTitleBg.bottom, 312, detailLabel.bottom-detailLabel.top+10);
    detailLabel.top = detailBg.top + 5;
    detailLabel.bottom = detailBg.bottom - 5;
    
    arrowImage.top = detailBg.top + 15;
    
    separator2.top = detailBg.bottom;
    
    //联系人
    contactBg.top = detailBg.bottom;
    staffLabel.top = contactBg.top + 5;
    contactImage.top = contactBg.top + 5;
    phoneLabel.top = staffLabel.bottom + 5;
    serviceCallImage.top = staffLabel.bottom + 5;
    
    self.callBtn.top = contactBg.top + 5;
    self.callBtn.right = 296;
    
    endImage.top = contactBg.bottom;
    
    self.scrollView.contentSize = CGSizeMake(320, contactBg.bottom + 10);
}

- (void) callAction
{
    if (callPhoneWebView == nil) {
        self.callPhoneWebView = [[UIWebView alloc] init];
    }
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", [phoneLabel.text substringFromIndex:5]]]];
    [callPhoneWebView loadRequest:request];
}

- (void) buyAction
{
    ZSTECASubmitOrderViewController *commit = [[ZSTECASubmitOrderViewController alloc] init];
    commit.productInfo = self.productInfo;
    commit.hidesBottomBarWhenPushed = YES;
    [self.viewController.navigationController pushViewController:commit animated:YES];
}

- (void)manageAction
{
    isManage = YES;
    NSString *loginMsisdn = [ZSTF3Preferences shared].loginMsisdn;
    if (isFavorite) {
        [self.engine manageFavorites:[self.productInfo.productID stringValue] msisdn:loginMsisdn opType:@"2"];
    } else {
        [self.engine manageFavorites:[self.productInfo.productID stringValue] msisdn:loginMsisdn opType:@"1"];
    }
}

- (void)addComplaintsAction
{
    ZSTECAReportViewController *vc = [[ZSTECAReportViewController alloc] init];
    vc.productInfo = self.productInfo;
    vc.hidesBottomBarWhenPushed = YES;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:vc];
    [self.viewController presentViewController:navController animated:YES completion:nil];

}

- (void)configCell:(ZSTECAProduct *)nvo forRowAtIndex:(NSUInteger)index
{
    self.productInfo = nvo;
    
    [detailLabel collapse];
    self.scrollView.contentOffset = CGPointMake(0, 0);
    
    [self.engine cancelAllRequest];
    [self newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkIndicatorView) {
        [self.engine getProductsContent:[nvo.productID integerValue]];
    }];
}

- (void)getProductsContentDidSucceed:(NSDictionary *)theProductDetail
{
    self.productInfo.iconUrl = [theProductDetail safeObjectForKey:@"IconUrl"];
    self.productInfo.summary = [theProductDetail safeObjectForKey:@"Description"];
    self.productInfo.salesPrice = [theProductDetail safeObjectForKey:@"SalesPrice"];
    self.productInfo.primePrice = [theProductDetail safeObjectForKey:@"PrimePrice"];
    self.productInfo.productName = [theProductDetail safeObjectForKey:@"ProductName"];
    self.productInfo.primeLabel = [theProductDetail safeObjectForKey:@"PrimeLabel"];
    self.productInfo.salesLabel = [theProductDetail safeObjectForKey:@"SalesLabel"];
    [productPhotoView setPhotoURLs:[theProductDetail safeObjectForKey:@"File"]];
    
    NSString *priceStr = [NSString stringWithFormat:@"%.2f",[self.productInfo.salesPrice floatValue]];
    NSArray *priceArray = [priceStr componentsSeparatedByString: @"."];
    
    descriptionLabel.text = [theProductDetail safeObjectForKey:@"ProductName"];//Description
    salesPriceLabel.text = [NSString stringWithFormat:@"%@.",[priceArray objectAtIndex:0]];
    salesPricePrefixLabel.text = [theProductDetail safeObjectForKey:@"SalesLabel"];
    primePricePrefixLabel.text = [theProductDetail safeObjectForKey:@"PrimeLabel"];
    salesPriceFloatLabel.text = [priceArray objectAtIndex:1];
    primePriceLabel.text = [NSString stringWithFormat:@"%.2f", [[theProductDetail safeObjectForKey:@"PrimePrice"] floatValue]];
    detailLabel.text = [theProductDetail safeObjectForKey:@"Details"];
    
    staffLabel.text = [NSString stringWithFormat:NSLocalizedString(@"联 系 人：%@" , nil), [theProductDetail safeObjectForKey:@"CsStaff"]];
    phoneLabel.text = [NSString stringWithFormat:NSLocalizedString(@"客服电话：%@" , nil), [theProductDetail safeObjectForKey:@"CsPhone"]];
    
    NSString *result = [theProductDetail safeObjectForKey:@"FavoriteFlag"];
    if (result!=nil && [result isEqualToString:@"false"]) {
        isFavorite = NO;
        [favoriteBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_bottom_favorite_buttn_p.png") forState:UIControlStateNormal];
        [favoriteBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_bottom_favorite_buttn_p.png") forState:UIControlStateHighlighted];
    } else {
        isFavorite = YES;
        [favoriteBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_bottom_favorite_buttn_n.png") forState:UIControlStateNormal];
        [favoriteBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_bottom_favorite_buttn_n.png") forState:UIControlStateHighlighted];
    }
    
    self.favoriteBtn.enabled = YES;
    self.callBtn.enabled = YES;
    self.complaintBtn.enabled = YES;
    
    [self setNeedsLayout];
    
    [self removeNetworkIndicatorView];
}

- (void)getProductsContentDidFailed:(int)resultCode
{
    
    self.favoriteBtn.enabled = NO;
    self.callBtn.enabled = NO;
    
    [self refreshFailed];
    
}

- (void)manageFavorites:(NSNumber *)productId opType:(NSString *)opType
{
    NSString *loginMsisdn = [ZSTF3Preferences shared].loginMsisdn;
    
    [self.engine manageFavorites:[productId stringValue] msisdn:loginMsisdn opType:opType];
    
}

- (void)manageFavoritesDidSucceed:(NSString *)opType
{
    isManage = NO;
    if ([opType isEqualToString:@"1"]) {
        isFavorite = YES;
        [TKUIUtil alertInWindow:NSLocalizedString(@"收藏成功" , nil) withImage:[UIImage imageNamed:@"icon_warning.png"]  withCenter:CGPointMake(160, 100)];
        
        [favoriteBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_bottom_favorite_buttn_n.png") forState:UIControlStateNormal];
        [favoriteBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_bottom_favorite_buttn_n.png") forState:UIControlStateHighlighted];
    } else {
        isFavorite = NO;
        [TKUIUtil alertInWindow:NSLocalizedString(@"收藏已取消" , nil) withImage:[UIImage imageNamed:@"icon_warning.png"]  withCenter:CGPointMake(160, 100)];
        
        [favoriteBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_bottom_favorite_buttn_p.png") forState:UIControlStateNormal];
        [favoriteBtn setBackgroundImage:ZSTModuleImage(@"module_ecoma_bottom_favorite_buttn_p.png") forState:UIControlStateHighlighted];
    }
}

- (void)manageFavoritesDidFailed:(int)resultCode
{
    isManage = NO;
    [TKUIUtil alertInWindow:NSLocalizedString(@"操作失败", @"") withImage:[UIImage imageNamed:@"icon_warning.png"]  withCenter:CGPointMake(160, 100)];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
