//
//  ZSTECAProductDetailCell.h
//  ECA
//
//  Created by luobin on 12-10-10.
//
//

#import "TKHorizontalTableView.h"
#import "ZSTECAProductPhotoView.h"

@class ZSTECAProductDetailCell;
@protocol ZSTECAProductDetailDelegate<NSObject>
@required
- (void)productDetailCell:(ZSTECAProductDetailCell *)cell didSelectAtIndex:(int)index;
@end

@class ZSTECAProductDetailLabel;
@class ZSTECAProductPhotoView;

@interface ZSTECAProductDetailCell : TKHorizontalTableViewCell<ZSTECAProductPhotoViewDelegate>
{
    BOOL isManage;
    
    UIImageView *priceBg;
    
    UIImageView *descriptionBg;
    
    UIImageView *detailTitleBg;
    UILabel *detailTitleLabel;
    
    UIImageView *detailBg;
    
    UIImageView *arrowImage;
    
    UIImageView *separator1;
    
    UIImageView *separator2;
    
    UIImageView *contactBg;
    
    UIImageView *endImage;
    
    UIImageView *serviceCallImage;
    UIImageView *contactImage;
}

@property (nonatomic, strong) ZSTECAProduct *productInfo;

@property (nonatomic, strong) UIButton *callBtn;
@property (nonatomic, strong) UIButton *favoriteBtn;
@property (nonatomic, strong) UIButton *buyBtn;
@property (nonatomic, strong) UIWebView *callPhoneWebView;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) ZSTECAProductPhotoView *productPhotoView;

@property (nonatomic, strong) UILabel *descriptionLabel;
@property (nonatomic, strong) ZSTECAProductDetailLabel *detailLabel;
@property (nonatomic, strong) UILabel *salesPricePrefixLabel;
@property (nonatomic, strong) UILabel *salesPriceLabel;
@property (nonatomic, strong) UILabel *salesPriceFloatLabel;
@property (nonatomic, strong) UILabel *primePricePrefixLabel;
@property (nonatomic, strong) UILabel *primePriceLabel;
@property (nonatomic, strong) UILabel *crossOutLine;
@property (nonatomic, strong) UILabel *staffLabel;
@property (nonatomic, strong) UILabel *phoneLabel;
@property (nonatomic, strong) UIButton *complaintBtn;

@property (nonatomic, assign) BOOL isFavorite;
@property (nonatomic, assign) id<ZSTECAProductDetailDelegate>delegate;

- (void)configCell:(ZSTECAProduct *)product forRowAtIndex:(NSUInteger)index;

@end
