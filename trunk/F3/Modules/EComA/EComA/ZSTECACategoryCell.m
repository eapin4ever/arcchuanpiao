//
//  ZSTECACategoryCell.m
//  ECA
//
//  Created by luobin on 12-9-29.
//
//

#import "ZSTECACategoryCell.h"

@implementation ZSTECACategoryCell

@synthesize leftImageView;
@synthesize titleLabel;
@synthesize detailTitleLabel;
@synthesize position;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        
        // Initialization code
        self.leftImageView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(23.5, 12, 55, 55)];
        self.leftImageView.backgroundColor = [UIColor clearColor];
        self.leftImageView.userInteractionEnabled = NO;
        self.leftImageView.adjustsImageWhenHighlighted = NO;
        [self.contentView addSubview:self.leftImageView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(90, 14, 268, 30)];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.shadowColor = [UIColor whiteColor];
		self.titleLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
        self.titleLabel.textColor = RGBCOLOR(26, 26, 26);
        self.titleLabel.text =  @"";
        self.titleLabel.font = [UIFont boldSystemFontOfSize:15];
        self.titleLabel.highlightedTextColor = [UIColor whiteColor];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.titleLabel];
        
        self.detailTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(90, 44, 268, 20)];
        self.detailTitleLabel.text = @"";
        self.detailTitleLabel.font = [UIFont systemFontOfSize:12];
        self.detailTitleLabel.textAlignment = NSTextAlignmentLeft;
        self.detailTitleLabel.numberOfLines = 0;
        self.detailTitleLabel.textColor = RGBCOLOR(158, 158, 158);
        self.detailTitleLabel.highlightedTextColor = [UIColor whiteColor];
        self.detailTitleLabel.shadowColor = [UIColor whiteColor];
		self.detailTitleLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
        self.detailTitleLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.detailTitleLabel];
    }
    return self;
}

- (void)configCell:(NSDictionary *)category forRowAtIndex:(NSIndexPath *)indexPath
{
    [self.leftImageView clear];
    self.leftImageView.url = [NSURL URLWithString:[category safeObjectForKey:@"IconUrl"]];
    [self.leftImageView loadImage];
    self.titleLabel.text = [category safeObjectForKey:@"CategoryName"];
    self.detailTitleLabel.text = [category safeObjectForKey:@"Description"];
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    
    [[UIColor whiteColor] set];
    CGContextFillRect(c, self.bounds);
    
    CGFloat height = 1.f;
    if (TKIsRetina()) {
        height = 0.5f;
    }
    
    if (position == CustomCellPositionTop) {
        
        [ZSTModuleImage(@"module_ecoma_category.png") drawAsPatternInRect:CGRectMake(0, 0, self.width, self.height - height)];
        
        [[UIColor colorWithWhite:0.8 alpha:1] set];
        CGContextFillRect(c, CGRectMake(0, self.height - height, self.width, height));
        
        return;
    } else if (position == CustomCellPositionBottom) {
        
        [ZSTModuleImage(@"module_ecoma_category.png") drawAsPatternInRect:CGRectMake(0, height, self.width, self.height - height*2)];
        
        [[UIColor colorWithWhite:0.98 alpha:1] set];
        CGContextFillRect(c, CGRectMake(0, 0, self.width, height));
        
        [[UIColor colorWithWhite:0.8 alpha:1] set];
        CGContextFillRect(c, CGRectMake(0, self.height - height, self.width, height));
        
        return;
    }
    else if (position == CustomCellPositionSingle || position == CustomCellPositionMiddle) {
        
        [ZSTModuleImage(@"module_ecoma_category.png") drawAsPatternInRect:CGRectMake(0, height, self.width, self.height - 2*height)];
        
        [[UIColor colorWithWhite:0.98 alpha:1] set];
        CGContextFillRect(c, CGRectMake(0, 0, self.width, height));
        
        [[UIColor colorWithWhite:0.8 alpha:1] set];
        CGContextFillRect(c, CGRectMake(0, self.height - height, self.width, height));
        
        return;
    } else {
        [ZSTModuleImage(@"module_ecoma_category.png") drawAsPatternInRect:CGRectMake(0, 0, self.width, self.height - height)];
        
        [[UIColor colorWithWhite:0.8 alpha:1] set];
        CGContextFillRect(c, CGRectMake(0, self.height - height, self.width, height));
    }
}

@end
