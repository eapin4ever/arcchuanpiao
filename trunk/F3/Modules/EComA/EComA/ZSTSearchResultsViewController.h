//
//  ZSTSearchResultsViewController.h
//  ECA
//
//  Created by admin on 12-10-11.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTSearchResultsViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,TKLoadMoreViewDelegate>

{
    UITableView *searchResultsList;
    
    TKLoadMoreView *_loadMoreView;
    
    BOOL _isLoadingMore;
    BOOL _hasMore;
    
    int _pagenum;
    
    
    YIFullScreenScroll* _fullScreenDelegate;//以后可能会用吧

}


@property (nonatomic ,strong) UITableView *searchResultsList;
@property (nonatomic ,strong) NSMutableArray *searchResultsArray;


@property (nonatomic, assign) NSInteger categoryID;
@property(nonatomic ,strong) NSString *searchText;


@end
