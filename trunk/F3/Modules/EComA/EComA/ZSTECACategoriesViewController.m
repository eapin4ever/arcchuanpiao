//
//  ZSTECACategoriesViewController.m
//  ECA
//
//  Created by luobin on 12-9-29.
//
//

#import "ZSTECACategoriesViewController.h"
#import "ZSTECACarouselView.h"
#import "ZSTECACategoryCell.h"
#import "ZSTECACarouselViewCell.h"
#import "ZSTUIView+Folder.h"
#import "ZSTECACategoryFolderView.h"
#import "ZSTECAMyMarketViewController.h"
#import "TKNetworkIndicatorView.h"
#import "ZSTECAProductTableViewController.h"
#import "ZSTECAProductDetailViewController.h"
#import "ZSTECAConfirmPaymentViewController.h"
#import "ZSTLoginViewController.h"
#import "BaseNavgationController.h"

@interface ZSTECACategoriesViewController ()<UITableViewDataSource, UITableViewDelegate, ZSTECACarouselViewCellDelegate,LoginDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSArray *categories;
@property (nonatomic, strong) NSArray *carouseles;

- (NSArray *)indexPathsForSection:(NSUInteger)section rowIndexSet:(NSIndexSet *)indexSet;

- (void)openFolderForCategories:(NSArray *)categories;

@end

@implementation ZSTECACategoriesViewController

@synthesize tableView;
@synthesize categories;
@synthesize carouseles;

- (void)moduleApplication:(ZSTModuleApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [application.dao createTableIfNotExistForECAModule];
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:@"categories" context:nil];
    [self removeObserver:self forKeyPath:@"carouseles" context:nil];
}

- (void)openMyMarket
{
    ZSTECAMyMarketViewController *myMarket = [[ZSTECAMyMarketViewController alloc] init];
    myMarket.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:myMarket animated:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"我的", nil) target:self selector:@selector (openMyMarket)];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.frame.size.height) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.tableView.separatorStyle =  UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];
    
    [self addObserver:self forKeyPath:@"categories" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    [self addObserver:self forKeyPath:@"carouseles" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0)
    {
        ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
        controller.isFromSetting = YES;
        controller.delegate = self;
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
        }];
        
        return;
    }
    
    [self.view newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkIndicatorView) {
        //获取分类
        if (self.categories == nil) {
            [self.engine getProductCategory];
        }
        if (self.carouseles == nil) {
            [self.engine getCarousel];
        }
    }];
}

- (void)loginDidCancel
{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        if (![controller isKindOfClass:[self.rootViewController class]]) {
            
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
    NSNumber *shellId = [NSNumber numberWithInteger:[ZSTF3Preferences shared].shellId];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLoginCancel object:shellId];
}

- (void)loginFinish
{
    
}

- (NSArray *)indexPathsForSection:(NSUInteger)section rowIndexSet:(NSIndexSet *)indexSet
{
    NSMutableArray *    indexPaths;
    NSUInteger          currentIndex;
    
    assert(indexSet != nil);
    
    indexPaths = [NSMutableArray array];
    assert(indexPaths != nil);
    currentIndex = [indexSet firstIndex];
    while (currentIndex != NSNotFound) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:currentIndex inSection:section]];
        currentIndex = [indexSet indexGreaterThanIndex:currentIndex];
    }
    return indexPaths;
}

- (void)openFolderForCategories:(NSArray *)children {
    
    CGFloat height;
    NSInteger maxRowCount;
    CGFloat rowHeight;
    
    NSInteger rowCount = [children count]/4 + (([children count]%4)? 1 : 0);
    if ([[NSUserDefaults standardUserDefaults] boolForKey:showImgKey]) {
        maxRowCount = 4;
        rowHeight = 62;
    } else {
        maxRowCount = 6;
        rowHeight = 30;
    }
    
    if (rowCount > maxRowCount) {
        rowCount = maxRowCount;
    }
    
    if (rowCount <= 1) {
        height = 10*2 + rowHeight;
    } else if (rowCount <= 2) {
        height = 10*3 + rowHeight*2;
    } else {
        height = 10*4 + rowHeight*3;
    }
    
    ZSTECACarouselViewCell *carouselViewCell = (ZSTECACarouselViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    ZSTECACategoryFolderView *categoryFolderView = [[ZSTECACategoryFolderView alloc] initWithFrame:CGRectMake(0, 0, 320, height)];
    categoryFolderView.categories = children;
    [self.tableView openFolderWithContentView:categoryFolderView
                                    openBlock:^(UIView *contentView, CFTimeInterval duration, CAMediaTimingFunction *timingFunction) {
                                        if ([self.carouseles count]) {
                                            [carouselViewCell.carouselView stopAnimation];
                                        }                                    }
                                   closeBlock:^(UIView *contentView, CFTimeInterval duration, CAMediaTimingFunction *timingFunction) {
                                       
                                   }
                              completionBlock:^ {
                                  if ([self.carouseles count]) {
                                      [carouselViewCell.carouselView startAnimation];
                                  }
                              }
     ];
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSIndexSet *indexes = [change objectForKey:NSKeyValueChangeIndexesKey];
    if ([keyPath isEqualToString:@"categories"]) {
        switch ( [[change objectForKey:NSKeyValueChangeKindKey] intValue]) {
            default:
                assert(NO);
            case NSKeyValueChangeSetting: {
                [self.tableView reloadData];
            } break;
            case NSKeyValueChangeInsertion: {
                [self.tableView insertRowsAtIndexPaths:[self indexPathsForSection:1 rowIndexSet:indexes] withRowAnimation:UITableViewRowAnimationNone];
                [self.tableView flashScrollIndicators];
            } break;
            case NSKeyValueChangeRemoval: {
                [self.tableView deleteRowsAtIndexPaths:[self indexPathsForSection:1 rowIndexSet:indexes] withRowAnimation:UITableViewRowAnimationNone];
                [self.tableView flashScrollIndicators];
            } break;
            case NSKeyValueChangeReplacement: {
                [self.tableView reloadRowsAtIndexPaths:[self indexPathsForSection:1 rowIndexSet:indexes] withRowAnimation:UITableViewRowAnimationNone];
            } break;
        }
    } else if ([keyPath isEqualToString:@"carouseles"]) {
        [self.tableView reloadData];
    }
}

#pragma mark - ZSTF3EngineECADelegate
- (void)getProductCategoryDidSucceed
{
    self.categories = [self.dao getChildrenOfCategory:0];
    [self.view removeNetworkIndicatorView];
    if ([self.categories count] == 0 && [self.carouseles count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"提示", @"")
                              message:NSLocalizedString(@"暂无数据", nil)
                              delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"确定", @"")
                              otherButtonTitles:nil,nil];
        [alert show];
    }
}

- (void)getProductCategoryDidFailed:(int)resultCode
{
    [self.view refreshFailed];
}

- (void)getCarouselDidSucceed:(NSArray *)theCarouseles
{
    if ([theCarouseles count] != 0) {
        self.carouseles = theCarouseles;
    }
    [self.view removeNetworkIndicatorView];
}

- (void)getCarouselDidFailed:(int)resultCode;
{
    [self.view refreshFailed];
}

#pragma mark ---点击某行触发的方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 1 || ([self.carouseles count] == 0 && [self.categories count])) {
        NSDictionary *category = [self.categories objectAtIndex:indexPath.row];
        NSArray *children = [self.dao getChildrenOfCategory:[[category safeObjectForKey:@"CategoryID"] integerValue]];
        if ([children count] > 0) {
            [self openFolderForCategories:children];
        } else {
            ZSTECAProductTableViewController *productTableViewController = [[ZSTECAProductTableViewController alloc] init];
            productTableViewController.categoryID = [[category safeObjectForKey:@"CategoryID"] integerValue];
            productTableViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:productTableViewController animated:YES];
        }
    } else {
        
    }
}

- (void)carouselViewCell:(ZSTECACarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index
{
    ZSTECAProductDetailViewController *productDetailViewController = [[ZSTECAProductDetailViewController alloc] init];
    productDetailViewController.products = self.carouseles;
    productDetailViewController.hidesBottomBarWhenPushed = YES;
    productDetailViewController.currentProductIndex = index;
    [self.navigationController pushViewController:productDetailViewController animated:YES];
}

#pragma mark -
#pragma mark ---Table Data Source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if (self.categories && self.carouseles) {
        return 2;
    }else if (self.categories)
    {
        return 1;
    }else {
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (self.carouseles) {
        return section?[self.categories count]:1;
    }
    return [self.categories count];
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0 && [self.carouseles count]) {
        static NSString *newsCellIdentifier = @"ZSTECACarouselViewCell";
        ZSTECACarouselViewCell *cell = [tableView dequeueReusableCellWithIdentifier:newsCellIdentifier];
        if (cell == nil) {
            cell = [[ZSTECACarouselViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:newsCellIdentifier];
            cell.carouselViewCellDelegate = self;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if ([carouseles count]) {
            [cell configCell:carouseles];
        }
        return cell;
        
    } else {
        static NSString *reuseIdentifier  = @"ZSTECACategoryCell";
        ZSTECACategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
        if (cell == nil) {
            cell = [[ZSTECACategoryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
        }
        if(([theTableView numberOfRowsInSection:indexPath.section]-1) == 0){
            cell.position = CustomCellPositionSingle;
        }
        else if(indexPath.row == 0){
            cell.position = CustomCellPositionNone;
        }
        else if (indexPath.row == ([theTableView numberOfRowsInSection:indexPath.section]-1)){
            cell.position  = CustomCellPositionBottom;
        }
        else{
            cell.position = CustomCellPositionMiddle;
        }
        if ([self.categories count]) {
            NSDictionary *category = [self.categories objectAtIndex:indexPath.row];
            [cell configCell:category forRowAtIndex:indexPath];
        }
        return cell;

    }
}

#pragma mark ---UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.carouseles) {
        return indexPath.section?87:108;
    }
    return 87;
}

@end
