//
//  ZSTECACategoryFolderView.m
//  ECA
//
//  Created by luobin on 12-9-29.
//
//

#import "ZSTECACategoryFolderView.h"
#import "ZSTECACategoryFolderCell.h"

@interface ZSTECACategoryFolderView()

@property (nonatomic, retain) UITableView *tableView;

@end

@implementation ZSTECACategoryFolderView

@synthesize categories;
@synthesize tableView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        bgImageView.image = [ZSTModuleImage(@"module_ecoma_folder_bg.png") stretchableImageWithLeftCapWidth:17 topCapHeight:17];
        [self addSubview:bgImageView];
        
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height) style:UITableViewStylePlain];
        self.tableView.contentInset = UIEdgeInsetsMake(5, 0, 5, 0);
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self addSubview:self.tableView];
    }
    return self;
}

- (void)dealloc
{
    self.tableView = nil;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.tableView.frame = CGRectMake(0, 0, self.width, self.height);
}

- (void)setCategories:(NSArray *)theCategories
{
    if (categories != theCategories) {
        categories = theCategories;
        [self.tableView reloadData];
    }
}

#pragma mark -
#pragma mark ---Table Data Source Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.categories count]/4 + (([self.categories count]%4)? 1 : 0);
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *reuseIdentifier = @"ZSTECACategoryFolderCell";
    ZSTECACategoryFolderCell *cell = [self.tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    if (cell == nil) {
        cell = [[ZSTECACategoryFolderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSUInteger len = 4;
    if ((indexPath.row + 1) * 4 >= self.categories.count) {
        len = self.categories.count - indexPath.row * 4;
    }
    NSArray *categoriesForCell = [self.categories subarrayWithRange:NSMakeRange(indexPath.row * 4, len)];
    [cell configCell:categoriesForCell forRowAtIndex:indexPath];
    return cell;
}

#pragma mark ---UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [[NSUserDefaults standardUserDefaults] boolForKey:showImgKey]?72:40;
}

@end
