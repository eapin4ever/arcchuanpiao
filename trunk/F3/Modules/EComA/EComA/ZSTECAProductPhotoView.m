//
//  ZSTECAProductPhotoView.m
//  ECA
//
//  Created by luobin on 12-10-10.
//
//

#import "ZSTECAProductPhotoView.h"

@implementation ZSTECAProductPhotoView

@synthesize scrollView;
@synthesize leftArrow;
@synthesize rightArrow;
@synthesize pageControl;
@synthesize photoURLs;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, 312, 210)];
    if (self) {
        // Initialization code
        scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(45, 15, 215, 165)];
        scrollView.delegate = self;
        scrollView.pagingEnabled = YES;
        scrollView.scrollEnabled = NO;
        scrollView.backgroundColor = [UIColor clearColor];
        scrollView.showsHorizontalScrollIndicator = NO;
        scrollView.showsVerticalScrollIndicator = NO;
        [self addSubview:scrollView];
        
        self.leftArrow = [UIButton buttonWithType:UIButtonTypeCustom];
        leftArrow.frame = CGRectMake(0, 0, 50, 215);
        [leftArrow setImage:ZSTModuleImage(@"module_ecoma_product_detail_leftArrow.png") forState:UIControlStateNormal];
        [leftArrow addTarget:self action:@selector(previousPage) forControlEvents:UIControlEventTouchUpInside];
        leftArrow.enabled = NO;
        [self addSubview:leftArrow];
        
        self.rightArrow = [UIButton buttonWithType:UIButtonTypeCustom];
        rightArrow.frame = CGRectMake(245, 0, 85, 215);
        [rightArrow setImage:ZSTModuleImage(@"module_ecoma_product_detail_rightArrow.png") forState:UIControlStateNormal];
        [rightArrow addTarget:self action:@selector(nextPage) forControlEvents:UIControlEventTouchUpInside];
        rightArrow.enabled = NO;
        [self addSubview:rightArrow];
        
        pageControl = [[TKPageControl alloc] initWithFrame:CGRectMake(0, 190, 80, 11)];
        [pageControl addTarget:self action:@selector(pageControlValueChange) forControlEvents:UIControlEventValueChanged];
        pageControl.type = TKPageControlTypeOnImageOffImage;
        pageControl.dotSize = 7;
        pageControl.dotSpacing = 8;
        pageControl.onImage = ZSTModuleImage(@"module_ecoma_pageControlOn.png");
        pageControl.offImage = ZSTModuleImage(@"module_ecoma_pageControlOff.png");
        pageControl.backgroundColor = [UIColor clearColor];
        pageControl.userInteractionEnabled = NO;
        [self addSubview:pageControl];
    }
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:CGRectMake(frame.origin.x, frame.origin.y, 312, 210)];
}

- (void)pageControlValueChange
{
    self.leftArrow.enabled = (pageControl.currentPage != 0);
    self.rightArrow.enabled = (pageControl.currentPage != [photoURLs count] - 1);
    [self.scrollView setContentOffset:CGPointMake(215 * pageControl.currentPage, 0)  animated:YES];
}

- (void)nextPage
{
    pageControl.currentPage = pageControl.currentPage + 1;
}

- (void)previousPage
{
    pageControl.currentPage = pageControl.currentPage - 1;
}

- (void)clickAction:(id)sender
{
    if(self.delegate&&[self.delegate respondsToSelector:@selector(productPhotoView:didSelectAtIndex:)])
    {
        [self.delegate productPhotoView:self didSelectAtIndex:0];
    }
}

#pragma mark - TKAsynImageViewDelegate

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)theScrollView
{
//    pageControl.currentPage = scrollView.contentOffset.x / scrollView.bounds.size.width;//是页面控制器的按钮根据页数改变显示
}

- (void)setPhotoURLs:(NSArray *)theImageURLs
{
    if (photoURLs != theImageURLs) {
        photoURLs = theImageURLs;
        [scrollView removeAllSubviews];
        for (int i = 0; i < [photoURLs count]; i ++) {
            NSDictionary *imageInfo = [photoURLs objectAtIndex:i];
            TKAsynImageView *asynImageView = [[TKAsynImageView alloc] initWithFrame:CGRectMake( i*215, 0, 215, 165)];
            asynImageView.asynImageDelegate = self;
            asynImageView.userInteractionEnabled = NO;
            asynImageView.imageInset = UIEdgeInsetsMake(1, 1, 1, 1);
            asynImageView.defaultImage = ZSTModuleImage(@"module_ecoma_loadingImg_215_165.png");
            asynImageView.adorn = [ZSTModuleImage(@"module_ecoma_product_detail_photo_frame.png") stretchableImageWithLeftCapWidth:45 topCapHeight:45];
            asynImageView.url = [NSURL URLWithString:[imageInfo safeObjectForKey:@"originalUrl"]];
            [asynImageView loadImage];
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            btn.frame = asynImageView.frame;
            btn.tag = i;
            [btn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
            [scrollView addSubview:asynImageView];
            [scrollView addSubview:btn];
        }
        pageControl.numberOfPages = [photoURLs count];
        pageControl.currentPage = 0;
        self.leftArrow.enabled = NO;
        self.rightArrow.enabled = ([photoURLs count] != 1);
    }
}



@end
