//
//  ELButtonView.h
//  CustomButton
//
//  Created by luobin on 12-9-29.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECAIconView : UIButton <HJManagedImageVDelegate, TKAsynImageViewDelegate>

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) TKAsynImageView *iconView;
@property (nonatomic, strong) NSNumber *category;

//设置图标
-(void)setIcon:(NSString *)imageUrl;

//设置信息简介
- (void)setTitle:(NSString *)title;

//设置分类id
- (void)setCategoryID:(NSNumber *)number;

@end

