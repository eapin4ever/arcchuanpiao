//
//  NewsMainCell.h
//  F3
//
//  Created by admin on 12-7-17.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZSTECAProduct.h"
#import "ZSTECACarouselView.h"


typedef enum {
    CustomCellPositionTop,
    CustomCellPositionMiddle,
    CustomCellPositionBottom,
    CustomCellPositionSingle,
    CustomCellPositionNone
} CustomCellPosition;


@interface ZSTECAEmptyTableViewCell : UITableViewCell
@property (nonatomic ,retain) UILabel *introduceLabel;

- (void)setIntroduce:(NSString *)introduce;
@end

@interface ZSTECAOrderCell : UITableViewCell

@property(nonatomic) CustomCellPosition position;

@property (nonatomic ,strong) UILabel *orderTextLabel;
@property (nonatomic ,strong) UILabel *orderdetailTextLabel;

@end


@class ZSTECAUserCell;

@protocol ZSTECAUserCellDelegate <NSObject>

@optional

- (void)ZSTECAUserCellShouldBeginEditing:(ZSTECAUserCell *)cell ;

- (void)ZSTECAUserCellShouldChange:(ZSTECAUserCell *)cell ;


@end

@interface ZSTECAUserCell : UITableViewCell <UITextFieldDelegate>

@property(nonatomic) CustomCellPosition position;
@property (nonatomic ,strong) UILabel *userTextLabel;
@property (nonatomic ,strong) UITextField *userdetailText;
@property (nonatomic ,assign) id<ZSTECAUserCellDelegate> delegate;


@end


