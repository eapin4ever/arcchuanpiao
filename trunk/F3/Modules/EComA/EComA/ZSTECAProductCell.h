//
//  ZSTECAProductCell.h
//  ECA
//
//  Created by luobin on 9/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZSTVerticallyAlignedLabel.h"

@interface ZSTECAProductCell : UITableViewCell

@property (nonatomic, retain) TKAsynImageView *productImageView;
@property (nonatomic, retain) UILabel *primePriceLabel;
@property (nonatomic, retain) UILabel *salesPriceLabel;
@property (nonatomic ,retain) UILabel *crossOutLine;
@property (nonatomic, retain) ZSTVerticallyAlignedLabel *descriptionLabel;
@property (nonatomic ,retain) UILabel *timeLabel;
@property (nonatomic ,assign) BOOL paySuccess;


- (void)configCell:(NSDictionary *)cellInfo;
@end
