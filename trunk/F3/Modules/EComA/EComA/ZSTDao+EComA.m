//
//  ZSTNewsDao.m
//  News
//  
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTDao+EComA.h"
#import "ZSTSqlManager.h"
#import "ZSTUtils.h"
#import "ZSTGlobal+EComA.h"
#import "TKUtil.h"

#define CREATE_TABLE_CMD(moduleType) [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [EComA_Category_%ld] (\
                                                                    ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                    CategoryID INTEGER DEFAULT 0 UNIQUE, \
                                                                    CategoryName VARCHAR DEFAULT '', \
                                                                    ParentID INTEGER DEFAULT 0, \
                                                                    OrderNum INTEGER  DEFAULT 0, \
                                                                    Description VARCHAR  DEFAULT '', \
                                                                    IconUrl VARCHAR  DEFAULT ''\
                                                                );", moduleType]

@implementation ZSTDao(EComA)

- (void)createTableIfNotExistForECAModule
{
    [ZSTSqlManager executeSqlWithSqlStrings:CREATE_TABLE_CMD(self.moduleType)];
}

- (BOOL)addCategory:(NSInteger)categoryID
       categoryName:(NSString *)categoryName
           parentID:(NSInteger)parentID
           orderNum:(NSInteger)orderNum
        description:(NSString *)description
            iconUrl:(NSString *)iconUrl
{
    BOOL success = [ZSTSqlManager executeUpdate:
                    [NSString stringWithFormat:@"INSERT OR REPLACE INTO EComA_Category_%ld (CategoryID, CategoryName, ParentID, OrderNum, Description, IconUrl) VALUES (?,?,?,?,?,?)", (long)self.moduleType],
                    [NSNumber numberWithInteger:categoryID],
                    [TKUtil wrapNilObject:categoryName], 
                    [NSNumber numberWithInteger:parentID], 
                    [NSNumber numberWithInteger:orderNum],
                    [TKUtil wrapNilObject:description],
                    [TKUtil wrapNilObject:iconUrl]
                    ];
    return success;
}

- (BOOL)deleteAllProductCategories;
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM EComA_Category_%ld", (long)self.moduleType];
    if (![ZSTSqlManager executeUpdate:deleteSQL]) {
        
        return NO;
    }
    return YES;
}

- (NSArray *)getChildrenOfCategory:(NSInteger)parentID
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, CategoryID, CategoryName, ParentID, OrderNum, Description, IconUrl FROM EComA_Category_%ld where parentID = ? ORDER BY OrderNum desc, CategoryID desc", (long)self.moduleType];
    NSArray *results = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:[NSNumber numberWithInteger:parentID], nil]];
    return results;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@end
