

/*
 * from https://github.com/jwilling/JWFolders
 * 根据JWFolders修改而来，在原有功能上做了改进
 *   1.自动调整内容视图的位置，使内容视图永远显示在容器视图之内
 *   2.添加凹凸槽
 *   3.添加对tableView的支持
 *   4.支持同时展示多个JWFolders
 *
 *   Updated by luobin on 12-9-27.
 */

#import <Foundation/Foundation.h>
@class JWFolderSplitView, CAMediaTimingFunction;

typedef void (^JWFoldersCompletionBlock)(void);
typedef void (^JWFoldersCloseBlock)(UIView *contentView, CFTimeInterval duration, CAMediaTimingFunction *timingFunction);
typedef void (^JWFoldersOpenBlock)(UIView *contentView, CFTimeInterval duration, CAMediaTimingFunction *timingFunction);

@interface ZSTJWFolders : NSObject

/*
 Description:       The following are convenience methods that will create
 a JWFolders object which will handle the folder animation
 and removal for you.  All you are responsible for, as the
 sender, is to pass in (at minimum) the content view, position,
 and container view.  The rest are optional.
 
 ****Required Parameters****
 Content View:      This is the view that you wish to be embedded in between
 two folder-style panels.
 
 Container View:    This is the view in which you wish the folders to be added
 as a subview of. Behaviour when the container view is
 smaller than the content view is undefined.
 
 Position:          The position is used to determine where the folders should
 be opened.  In later updates the x-coordinate will be used
 to create a "notch", similar to the iOS Springboard. The
 position should be relative to the container view.
 
 ****Optional Parameters****
 
 Open Block:        The open block will be run when the animation of opening
 the folder is about to be performed.  Use this opportunity
 to perform animations and other custom behaviour on the
 content view. Use the passed-in reference to the content view.
 
 Close Block:       The close block will be run when the animation of closing
 the folder is about to be performed.  Use this opportunity
 to perform animations and other custom behaviour on the
 content view. Use the passed-in reference to the content veiw.
 
 Completion Block:  The completion block is called when the folder has been
 closed, and all views have been removed from the container
 view.  Use this opportunity to perform updates in your UI
 if needed.
 
 */

- (void)openFolderWithContentView:(UIView *)contentView
                         position:(CGPoint)position
                    containerView:(UIView *)containerView
                        openBlock:(JWFoldersOpenBlock)openBlock
                       closeBlock:(JWFoldersCloseBlock)closeBlock
                  completionBlock:(JWFoldersCompletionBlock)completionBlock;

- (void)openFolderWithContentView:(UIView *)contentView
                         position:(CGPoint)position
                    containerView:(UIView *)containerView
                        openBlock:(JWFoldersOpenBlock)openBlock;

- (void)openFolderWithContentViewController:(UIViewController *)viewController
                                   position:(CGPoint)position
                              containerView:(UIView *)containerView;

/* This attempts to close the folder that is currently displaying. */
- (void)closeCurrentFolder;

@end


/* For light highlight on folder buttons */
@interface JWFolderSplitView : UIControl
@property(nonatomic)BOOL isTop;
@property(nonatomic)CGPoint position;
@end
