//
//  ZSTGlobal+ECA.h
//  News
//
//  Created by luobin on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    ZSTCategoryType_DefaultIndex = 1,              //默认Category                 
    ZSTCategoryType_Index = 2,                     //展示在首页
    ZSTCategoryType_More = 4                       //展示在更多
} ZSTECACategoryType;                                     //Category类型

typedef enum
{
    ZSTIconType_List = 1,                          //列表ICon                 
    ZSTIconType_Broadcast = 2                      //轮播ICon
} ZSTECAIconType;                                     //icon类型

typedef enum
{
    ZSTNewsTypen_Normal = 0,                       //普通                 
    ZSTNewsType_Photos = 1,                        //图集
    ZSTNewsType_Video = 2,                         //视频
    ZSTNewsType_Special = 3                        //专题
} ZSTECAProductsType;                                     //新闻信息种类


typedef enum
{
    ZSTECAPayType_alixPay = 1,              //支付宝               
    ZSTECAPayType_IVR = 2,                  //IVR
    ZSTECAPayType_unionPay = 3              //银联
    
} ZSTECAPayType;                            //支付类型

#define PRODUCTS_PAGE_SIZE 10

#define SortType_Desc @"Desc"               //向新取
#define SortType_Asc @"Asc"                 //向旧取

//////////////////////////////////////////////////////////////////////////////////////////

#define ECommerceBaseURL @"http://mod.pmit.cn/ecoma"//最新接口


#define GetCatagory ECommerceBaseURL @"/GetCategory"
#define GetCarousel ECommerceBaseURL @"/GetCarousel"
#define GetProductList ECommerceBaseURL @"/GetProductList"
#define GetProductDetail ECommerceBaseURL @"/GetProductDetail"
#define GetProductsFile ECommerceBaseURL @"/GetFile"
#define GetMyFavorites ECommerceBaseURL @"/GetFavorites"//获取用户收藏
#define GetPocketShoppingList ECommerceBaseURL @"/GetOrderList"//获取购物清单 
#define GetOrderDetail ECommerceBaseURL @"/GetOrderDetail"//订单详情
#define GetProductsOrder ECommerceBaseURL @"/AddOrder"//添加商品订单
#define AddComplaints ECommerceBaseURL @"/AddComplaints"
#define ManageFavorites  ECommerceBaseURL @"/ManageFavorites"

#define ECommerceAlixPayUrl @"http://mod.pmit.cn/ecoma/alipay/Trade?orderid=" //支付地址

#define Broadcast @"+Broadcast"



