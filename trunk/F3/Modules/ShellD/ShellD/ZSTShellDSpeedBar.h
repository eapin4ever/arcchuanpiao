//
//  ZSTShellDSpeedBar.h
//  ShellD
//
//  Created by xuhuijun on 13-6-18.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <UIKit/UIKit.h>


@class ZSTShellDSpeedBar;

@protocol ZSTShellDSpeedBarDelegate <NSObject>

- (void)ZSTShellDSpeedBar:(ZSTShellDSpeedBar *)speedBar withParam:(NSDictionary *)param;

@end

@interface ZSTShellDSpeedBar : UIView

@property(nonatomic,strong)UIButton *speedBtn;
@property(nonatomic,strong)UIImageView *speedImage;
@property(strong,nonatomic)NSDictionary *moduleParams;
@property(strong)id<ZSTShellDSpeedBarDelegate>delegate;


- (void)configSpeedBarNormalImage:(UIImage *)normalImage
                    selectedImage:(UIImage *)selectedImage
                            param:(NSDictionary *)param;

@end
