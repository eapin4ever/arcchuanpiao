//
//  ZSTF3Engine+ShellD.m
//  ShellD
//
//  Created by xuhuijun on 13-6-19.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTF3Engine+ShellD.h"
#import "ZSTGlobal+ShellD.h"

@implementation ZSTF3Engine (ShellD)

- (void)getShellDAD
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];

    [params setSafeObject:[NSNumber numberWithInt:1] forKey:@"type"];
    [[ZSTCommunicator shared] openAPIPostToPath:GetShellBAD
                                         params:params
                                         target:self
                                       selector:@selector(getShellDADResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:NO];
}


- (void)getShellDADResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id shellBList = [response.jsonResponse objectForKey:@"info"];
        if (![shellBList isKindOfClass:[NSArray class]]) {
            shellBList = [NSArray array];
        }
        if ([self isValidDelegateForSelector:@selector(getShellDADDidSucceed:)]) {
            [self.delegate getShellDADDidSucceed:shellBList];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getShellDADDidFailed:)]) {
            [self.delegate getShellDADDidFailed:response.resultCode];
        }
    }
    
}


- (void)getShellDModuleAD
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[NSNumber numberWithInt:2] forKey:@"type"];
    [[ZSTCommunicator shared] openAPIPostToPath:GetShellBAD
                                         params:params
                                         target:self
                                       selector:@selector(getShellDModuleADResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:NO];
}


- (void)getShellDModuleADResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id shellBList = [response.jsonResponse objectForKey:@"info"];
        if (![shellBList isKindOfClass:[NSArray class]]) {
            shellBList = [NSArray array];
        }
        if ([self isValidDelegateForSelector:@selector(getShellDModuleADDidSucceed:)]) {
            [self.delegate getShellDModuleADDidSucceed:shellBList];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getShellDModuleADDidFailed:)]) {
            [self.delegate getShellDModuleADDidFailed:response.resultCode];
        }
    }
    
}


@end
