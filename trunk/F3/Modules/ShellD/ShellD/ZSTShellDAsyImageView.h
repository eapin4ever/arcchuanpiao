//
//  ZSTShellDAsyImageView.h
//  ShellD
//
//  Created by xuhuijun on 13-6-19.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "TKAsynImageView.h"

@interface ZSTShellDAsyImageView : TKAsynImageView <TKAsynImageViewDelegate>

@property(strong,nonatomic)NSDictionary *moduleParams;

@end
