//
//  ZSTShellDCarouselView.h
//  ShellD
//
//  Created by xuhuijun on 13-6-18.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKAsynImageView.h"

@class ZSTShellDCarouselView;

@protocol ZSTShellDCarouselViewDataSource <NSObject>

@optional
- (NSInteger)numberOfViewsInCarouselView:(ZSTShellDCarouselView *)newsCarouselView;
- (NSDictionary *)carouselView:(ZSTShellDCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index;

@end

@protocol ZSTShellDCarouselViewDelegate <NSObject>

@optional
- (void)carouselView:(ZSTShellDCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;

@end
@interface ZSTShellDCarouselView : UIView <HJManagedImageVDelegate,UIScrollViewDelegate,TKAsynImageViewDelegate>

{
    UIScrollView *_scrollView;
    UILabel *_describeLabel;
    TKPageControl *_pageControl;
    NSTimer *_carouselTimer;
    
    NSInteger _totalPages;
    NSInteger _curPage;
    
    NSMutableArray *_curSource;
    
    id<ZSTShellDCarouselViewDataSource> _carouselDataSource;
    id<ZSTShellDCarouselViewDelegate>  _carouselDelegate;
}


@property(nonatomic, assign) id<ZSTShellDCarouselViewDataSource> carouselDataSource;
@property(nonatomic, assign) id<ZSTShellDCarouselViewDelegate> carouselDelegate;

- (void)reloadData;
- (void)stopAnimation;
- (void)startAnimation;

@end
