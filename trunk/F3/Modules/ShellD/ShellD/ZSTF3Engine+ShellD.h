//
//  ZSTF3Engine+ShellD.h
//  ShellD
//
//  Created by xuhuijun on 13-6-19.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTF3Engine.h"


@protocol ZSTF3EngineShellDDelegate <ZSTF3EngineDelegate>

- (void)getShellDADDidSucceed:(NSArray *)ADArray;
- (void)getShellDADDidFailed:(int)resultCode;


- (void)getShellDModuleADDidSucceed:(NSArray *)ADArray;
- (void)getShellDModuleADDidFailed:(int)resultCode;

@end


@interface ZSTF3Engine (ShellD)

/**
 *	@brief	获取shell广告  type = 1 轮播图片
 *
 */
- (void)getShellDAD;

/**
 *	@brief	获取shelld 模块图片  type = 2 模块图片
 *
 */
- (void)getShellDModuleAD;

@end
