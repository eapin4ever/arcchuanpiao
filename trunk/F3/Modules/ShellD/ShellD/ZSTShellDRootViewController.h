//
//  ZSTShellDViewController.h
//  ShellD
//
//  Created by xuhuijun on 13-6-8.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTModule.h"
#import "ZSTModuleDelegate.h"
#import "ZSTDao.h"
#import "ZSTShellDCarouselView.h"
#import "ZSTShellDAsyImageView.h"
#import "ZSTShellDSpeedBar.h"
#import "ZSTF3Engine+ShellD.h"

@interface ZSTShellDRootViewController : UIViewController <ZSTModuleDelegate,ZSTShellDSpeedBarDelegate,ZSTShellDCarouselViewDataSource,ZSTShellDCarouselViewDelegate,UIScrollViewDelegate>

@property (nonatomic,strong)IBOutlet UIImageView *bgImageView;
@property (nonatomic,strong)NSArray *shellDADArray;
@property (nonatomic,strong)NSArray *shellDModuleADArray;


@property (nonatomic,strong)IBOutlet ZSTShellDCarouselView *shelldCarouselView;
@property (nonatomic,strong)IBOutlet UIScrollView *bgScrollView;
@property (nonatomic,strong)IBOutlet UIView *bgView;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view1;//ZSTShellDAsyImageView *view9;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view2;//ZSTShellDAsyImageView *view9;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view3;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view4;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view5;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view6;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view7;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view8;//ZSTShellDAsyImageView *view9;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view9;//ZSTShellDAsyImageView *view9;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view10;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view11;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view12;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view13;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view14;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view15;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view16;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view17;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view18;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view19;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view20;
@property (nonatomic,strong)IBOutlet ZSTShellDSpeedBar *view21;

@end
