//
//  ZSTShellDAsyImageView.m
//  ShellD
//
//  Created by xuhuijun on 13-6-19.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTShellDAsyImageView.h"

@implementation ZSTShellDAsyImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

//具体实现什么，暂时不明确，只为消除警告
- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{
    //这个类貌似不会调用到这个方法
}

@end
