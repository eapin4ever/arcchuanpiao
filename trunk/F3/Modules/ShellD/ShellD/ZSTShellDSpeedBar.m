//
//  ZSTShellDSpeedBar.m
//  ShellD
//
//  Created by xuhuijun on 13-6-18.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTShellDSpeedBar.h"
#import "ZSTUtils.h"

@implementation ZSTShellDSpeedBar


- (void)dealloc
{
    [self.speedBtn release];
    [self.speedImage release];
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    
    }
    return self;
}


- (void)configSpeedBarNormalImage:(UIImage *)normalImage
                    selectedImage:(UIImage *)selectedImage
                            param:(NSDictionary *)param
{
    self.backgroundColor = [ZSTUtils colorFromHexColor:[param safeObjectForKey:@"BackgroundColor"]];
    self.speedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.speedBtn.backgroundColor = [UIColor clearColor];
    self.speedBtn.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    [self.speedBtn setBackgroundImage:ZSTModuleImage(@"module_shelld_clearBtn_n.png") forState:UIControlStateNormal];
    [self.speedBtn setBackgroundImage:ZSTModuleImage(@"module_shelld_clearBtn_p.png") forState:UIControlStateSelected];
    [self.speedBtn setBackgroundImage:ZSTModuleImage(@"module_shelld_clearBtn_p.png") forState:UIControlStateHighlighted];

    [self.speedBtn addTarget:self action:@selector(selectSpeedBtn:) forControlEvents:UIControlEventTouchUpInside];
    self.speedBtn.autoresizingMask = self.autoresizingMask;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 74, 50)];
    view.backgroundColor = [UIColor clearColor];
    [self addSubview:view];
    view.autoresizingMask = UIViewAutoresizingFlexibleTopMargin |
                            UIViewAutoresizingFlexibleBottomMargin |
                            UIViewAutoresizingFlexibleLeftMargin |
                            UIViewAutoresizingFlexibleRightMargin;
    view.center = self.speedBtn.center;

    self.speedImage = [[UIImageView alloc] initWithFrame:CGRectMake((74-32)/2, 0, 32, 32)];
    self.speedImage.backgroundColor = [UIColor clearColor];
    self.speedImage.image = normalImage;
    [view addSubview:self.speedImage];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 35, 74, 15)];
    titleLabel.text = [param safeObjectForKey:@"Title"];
    titleLabel.font = [UIFont systemFontOfSize:12];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [ZSTUtils colorFromHexColor:[param safeObjectForKey:@"TitleColor"]];
    titleLabel.backgroundColor = [UIColor clearColor];
    
    [view addSubview:titleLabel];
    [titleLabel release];
    
    [view release];
    [self addSubview:self.speedBtn];

    self.moduleParams = param;
}


- (void)selectSpeedBtn:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(ZSTShellDSpeedBar:withParam:)]) {
        [self.delegate ZSTShellDSpeedBar:self withParam:self.moduleParams];
    }
}


@end
