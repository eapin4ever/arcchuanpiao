//
//  ZSTShellDViewController.m
//  ShellD
//
//  Created by xuhuijun on 13-6-8.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTShellDRootViewController.h"
#import "ElementParser.h"
#import "ZSTModuleManager.h"
#import "ZSTUtils.h"
#import "ZSTWebViewController.h"
#import "ZSTGlobal+ShellD.h"
#import "ZSTShell.h"
#import "ZSTGlobal+F3.h"
#import "ZSTMineViewController.h"

@interface ZSTShellDRootViewController ()
{
    TKPageControl *_pageControl;
}
@end

@implementation ZSTShellDRootViewController

@synthesize application;
@synthesize rootView;

- (id)init
{
    NSBundle *bundle = [NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"ShellDResources" withExtension:@"bundle"]];
    if ((self = [super initWithNibName:@"ZSTShellDRootViewController" bundle:bundle])) {
    }
    return self;

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (IS_IOS_7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(pushViewController)
                                                 name: NotificationName_PushViewController
                                               object: nil];
    
    
    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed: @"framework_top_bg.png"];
    self.navigationItem.titleView = [ZSTUtils logoView];
    [self.engine getShellDAD];
    [self.engine getShellDModuleAD];
    self.shelldCarouselView.frame = self.shelldCarouselView.frame;
    self.shelldCarouselView.carouselDataSource = self;
    self.shelldCarouselView.carouselDelegate = self;
    self.bgImageView.image = [UIImage imageNamed:@"bg.png"];
    [self imageButtonElementsForView];
    
    [self initFrameAuto];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationItem.rightBarButtonItem = [self initWithSubviews];
}

- (UIBarButtonItem *) initWithSubviews
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(mineAction:) forControlEvents:UIControlEventTouchUpInside];
    
    btn.layer.cornerRadius = btn.frame.size.width / 2.0;
    btn.layer.borderColor = [UIColor clearColor].CGColor;
    btn.layer.masksToBounds = YES;
    
    NSString *path = nil;
    UIImage *image = nil;
    NSDictionary *dic = [ZSTF3Preferences shared].avaterName;
    
    if (![ZSTF3Preferences shared].loginMsisdn || [ZSTF3Preferences shared].loginMsisdn.length == 0) {
        
        [ZSTF3Preferences shared].loginMsisdn = @"";
    }
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0) {
        
        image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
    } else {
        
        if (dic || [dic isKindOfClass:[NSDictionary class]]) {
            path = [dic safeObjectForKey:[ZSTF3Preferences shared].loginMsisdn];
        }
        
        if (path && path.length > 0) {
            
            NSString *homePath = NSHomeDirectory();
            NSString *imgTempPath = [path substringFromIndex:[path rangeOfString:@"tmp"].location];
            NSString *finalPath = [NSString stringWithFormat:@"%@/%@",homePath,imgTempPath];
            NSData *reader = [NSData dataWithContentsOfFile:finalPath];
            image = [UIImage imageWithData:reader];
            
            if (!image) {
                
                image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
            }
            
        } else {
            
            image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
        }
    }
    
    [btn setImage:image forState:UIControlStateNormal];
    
    //暂时解决btn图片不能显示的问题
    UIImageView *iv = [[UIImageView alloc] initWithFrame:btn.imageView.frame];
    iv.image = image;
    [btn insertSubview:iv aboveSubview:btn.imageView];
    
    return [[[UIBarButtonItem alloc] initWithCustomView:btn] autorelease];
}


- (void)mineAction:(id)sender
{
    ZSTMineViewController *controller = [[ZSTMineViewController alloc] initWithNibName:@"ZSTMineViewController" bundle:nil];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}


-(void)initFrameAuto
{
    float space_y = 3.0f;//_bgScrollView 上下之间小模块的间隔
    float space_x = 4.0f;//_bgScrollView 左右之间小模块的间隔

    
    _shelldCarouselView.frame = CGRectMake(5, 5, 310, 155);
        
    _bgScrollView.frame = CGRectMake(0, CGRectGetMaxY(_shelldCarouselView.frame) + 5, 320, 460 + (iPhone5?88:0) - CGRectGetMaxY(_shelldCarouselView.frame) - 5 - 6 - 44);
    
    CGRect frame = _bgScrollView.frame;
    
    float model_h = (frame.size.height - space_y * 2) / 3.0f;
    float model_w = (310 - space_x) / 2.0f;
    
    _view1.frame = CGRectMake(5, 0, model_w, model_h);
    _view2.frame = CGRectMake(5 + model_w + space_x , 0, model_w, model_h);            
    _view8.frame = CGRectMake(_view1.frame.origin.x
                              + 320, _view1.frame.origin.y, _view1.frame.size.width, model_h);
    _view9.frame = CGRectMake(_view2.frame.origin.x
                              + 320, _view2.frame.origin.y, _view2.frame.size.width, model_h);

    _bgView.frame = CGRectMake(0, _view1.frame.origin.y + _view1.frame.size.height + space_y, 320 * 3 , _bgScrollView.frame.size.height - _view1.frame.size.height - space_y);
    
    _view3.frame = CGRectMake(5, 0, (_view1.frame.size.width - 4)/2, model_h);
    _view4.frame = CGRectMake(5 + _view3.frame.size.width + 4, 0, _view3.frame.size.width,model_h);
    _view5.frame = CGRectMake(5 + model_w + space_x, 0, _view1.frame.size.width,model_h);
    
    _view6.frame = CGRectMake(5, _view2.origin.y + _view2.size.height + space_y, _view1.frame.size.width, model_h);
    _view7.frame = CGRectMake(5 + model_w + space_x, _view6.origin.y, _view6.frame.size.width, model_h);
    
    _view10.frame = CGRectMake(5 + 320, 0, model_w, model_h);
    _view11.frame = CGRectMake(CGRectGetMaxX(_view10.frame) + space_x, 0, model_w, model_h);    
    _view12.frame = CGRectMake(5 + 320, CGRectGetMaxY(_view10.frame) + space_y, model_w, model_h);
    _view13.frame = CGRectMake(CGRectGetMaxX(_view12.frame) + space_x, CGRectGetMaxY(_view11.frame) + space_y, (model_w - 4)/2, model_h);
    _view14.frame = CGRectMake(CGRectGetMaxX(_view13.frame) + 4, _view13.frame.origin.y, _view13.frame.size.width, _view13.frame.size.height);
    
    _view15.frame = CGRectMake(5 + 640, 0, model_w, model_h);
    _view16.frame = CGRectMake(5 + 640 + model_w + space_x , 0, model_w, model_h);
    _view17.frame = CGRectMake(5 + 640, 0, (_view1.frame.size.width - 4)/2, model_h);
    _view18.frame = CGRectMake(5 + 640 + _view3.frame.size.width + 4, 0, _view3.frame.size.width,model_h);
    _view19.frame = CGRectMake(5 + 640 + model_w + space_x, 0, _view1.frame.size.width,model_h);
    
    _view20.frame = CGRectMake(5 + 640, _view2.origin.y + _view2.size.height + space_y, _view1.frame.size.width, model_h);
    _view21.frame = CGRectMake(5 + 640 + model_w + space_x, _view6.origin.y, _view6.frame.size.width, model_h);

}
- (void) showGuideView
{
    UIView *guideView = [[UIView alloc] initWithFrame:CGRectMake(0,0 , 320, 480+(iPhone5?88:0))];
    guideView.backgroundColor = [UIColor blackColor];
    guideView.tag = 1111;
    guideView.alpha = 0.7f;
    
    UIImageView *guideTextImgView = [[UIImageView alloc] initWithFrame:CGRectMake(113,270+(iPhone5?88:0) , 93, 44)];
    guideTextImgView.image = ZSTModuleImage(@"module_shelld_BeginnersGuide.png");
    [guideView addSubview:guideTextImgView];
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:FIRST_LAUNCH_ShellD];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(removeGuideView)];
    [guideView addGestureRecognizer:singleTap];
    
    [self.view addSubview:guideView];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

- (void) removeGuideView
{
    [[[UIApplication sharedApplication].keyWindow viewWithTag:1111] removeFromSuperview];
}

- (void)imageButtonElementsForView
{
    
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module_shelld_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    [ZSTShell getPushOpentype:content];
    
    NSArray *itemElements = [root selectElements:@"ImageButton Item"];
    
    NSLog(@"---- %@ ----",@(itemElements.count));

    if (![[NSUserDefaults standardUserDefaults] valueForKey:FIRST_LAUNCH_ShellD]
        && ([itemElements count] > 8)) {
        [self showGuideView];
    }
    
    
    // 计算PageCount
    NSUInteger pageCount = [itemElements count] / 7;
    
    // 如果不是边界，则增加1
    if ([itemElements count]  % 7)
    {
        pageCount++;
    }

    self.bgScrollView.contentSize = CGSizeMake(320*pageCount, 209);
    for (int i = 0; i < [itemElements count]; i++) {
 
        Element *itemElement = [[itemElements objectAtIndex:i] retain];
        
        NSString *btnName = [[itemElement selectElement:@"Title"] contentsText];
        int moduleID = [[[itemElement selectElement:@"ModuleId"] contentsNumber] intValue];
        NSNumber *moduleType = [[itemElement selectElement:@"ModuleType"] contentsNumber];
        NSString *titleColor = [[itemElement selectElement:@"TitleColor"] contentsText];
        NSString *bgColor = [[itemElement selectElement:@"BackgroundColor"] contentsText];

        NSDictionary *paramDic = [NSDictionary dictionary];
        if (moduleID == 12 || moduleID == 24) {
            NSString *interfaceUrl = [[itemElement selectElement:@"InterfaceUrl"] contentsText];
            paramDic = [NSDictionary dictionaryWithObjectsAndKeys:
                        [NSNumber numberWithInt:moduleID],@"ModuleId",
                        btnName,@"Title",
                        moduleType,@"type",
                        interfaceUrl,@"InterfaceUrl",
                        bgColor,@"BackgroundColor",
                        titleColor,@"TitleColor", nil];
        }else{
            paramDic = [NSDictionary dictionaryWithObjectsAndKeys:
                        [NSNumber numberWithInt:moduleID],@"ModuleId",
                        btnName,@"Title",
                        moduleType,@"type",
                        bgColor,@"BackgroundColor",
                        titleColor,@"TitleColor", nil];
        }
        if (i == 0 || i == 1 || i == 7 || i == 8 || i == 14 || i == 15){
//            ZSTShellDAsyImageView *view = (ZSTShellDAsyImageView *)[self.bgScrollView viewWithTag:i+1];
//            view.moduleParams = paramDic;
//            [view addTarget:self action:@selector(asynImageViewDidClicked:) forControlEvents:UIControlEventTouchUpInside];
//            NSString *imageName = [NSString stringWithFormat:@"module_shelld_module_pic%d_n.png",i+1];
//            view.defaultImage = ZSTModuleImage(imageName);
//            view.hidden = NO;

            
            ZSTShellDSpeedBar *view = (ZSTShellDSpeedBar *)[self.bgScrollView viewWithTag:i+1];
            view.delegate = self;
            view.hidden = NO;
            NSString *imageName = [NSString stringWithFormat:@"module_shelld_icon%d_n.png",i+1];
            [view configSpeedBarNormalImage:ZSTModuleImage(imageName) selectedImage:nil param:paramDic];
            
        }else{
            ZSTShellDSpeedBar *view = (ZSTShellDSpeedBar *)[self.bgView viewWithTag:i+1];
            view.delegate = self;
            view.hidden = NO;
            NSString *imageName = [NSString stringWithFormat:@"module_shelld_icon%d_n.png",i+1];
            [view configSpeedBarNormalImage:ZSTModuleImage(imageName) selectedImage:nil param:paramDic];
        }
    }
    [parser release];
    
    
    if ([itemElements count] > 8) {
        _pageControl = [[TKPageControl alloc] initWithFrame:CGRectMake(0, 409+(iPhone5?88:0), 320, 10)];
        _pageControl.type = TKPageControlTypeOnImageOffImage;
        _pageControl.dotSize = 4;
        _pageControl.dotSpacing = 8;
        _pageControl.onImage = ZSTModuleImage(@"module_shelld_pageControlOn.png");
        _pageControl.offImage = ZSTModuleImage(@"module_shelld_pageControlOff.png");
        _pageControl.backgroundColor = [UIColor clearColor];
        _pageControl.userInteractionEnabled = NO;
        _pageControl.hidden = NO;
        _pageControl.numberOfPages = pageCount;
        [self.view addSubview:_pageControl];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView
{
    if (!_pageControl.hidden) {
        _pageControl.currentPage = aScrollView.contentOffset.x / aScrollView.bounds.size.width;
    }
}

- (void)pushViewController
{
    ZSTF3Preferences *per = [ZSTF3Preferences shared];
    UIViewController *controller = per.pushViewController;
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    per.isInPush = YES;
    
}

- (void)popViewController {
    
    ZSTF3Preferences *per = [ZSTF3Preferences shared];
    per.isInPush = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)pushViewControllerWithParam:(NSDictionary *)param
{
    NSMutableDictionary *moduleParams = [NSMutableDictionary dictionaryWithDictionary:param];
    
    ZSTModule *module = [[ZSTModuleManager shared] findModule:[[param safeObjectForKey:@"ModuleId"] integerValue]];
    if (module) {
        UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:[[param safeObjectForKey:@"ModuleId"] integerValue] withOptions:moduleParams];
        if (controller) {
            controller.hidesBottomBarWhenPushed = YES;
            controller.navigationItem.titleView = [ZSTUtils titleViewWithTitle:[param safeObjectForKey:@"Title"]];
            controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
            
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}
- (void)asynImageViewDidClicked:(ZSTShellDAsyImageView *)asynImageView
{
    [self pushViewControllerWithParam:asynImageView.moduleParams];
}

#pragma mark ZSTShellDSpeedBarDelegate

- (void)ZSTShellDSpeedBar:(ZSTShellDSpeedBar *)speedBar withParam:(NSDictionary *)param
{
    [self pushViewControllerWithParam:param];
}

#pragma mark ZSTF3EngineShellDDelegate

- (void)getShellDADDidSucceed:(NSArray *)ADArray
{
    self.shellDADArray = [ADArray retain];
    [self.shelldCarouselView reloadData];
}

- (void)getShellDADDidFailed:(int)resultCode
{
    [TKUIUtil alertInView:self.view withTitle:@"获取广告失败" withImage:nil];
}

- (void)getShellDModuleADDidSucceed:(NSArray *)ModuleADArray
{
#warning 原代码中，有效代码已被注释
//    self.shellDModuleADArray = [ModuleADArray retain];
//    for (NSDictionary *moduleDic in self.shellDModuleADArray) {
//        int location = [[moduleDic objectForKey:@"location"] integerValue];
//        //ZSTShellDAsyImageView *view = (ZSTShellDAsyImageView *)[self.bgScrollView viewWithTag:location];
//        //[view clear];
//        // view.url = [NSURL URLWithString:[moduleDic objectForKey:@"fileurl"]];
//        // [view loadImage];
//    
//    }
    
}

- (void)getShellDModuleADDidFailed:(int)resultCode
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"获取功能图片失败", nil) withImage:nil];
}

#pragma mark ZSTShellDCarouselViewDataSource

- (NSInteger)numberOfViewsInCarouselView:(ZSTShellDCarouselView *)newsCarouselView
{
    return [self.shellDADArray count];
}

- (NSDictionary *)carouselView:(ZSTShellDCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index
{
    if ([self.shellDADArray  count] != 0) {
        return [self.shellDADArray  objectAtIndex:index];
    }
    return nil;
}

#pragma mark ZSTShellDCarouselViewDelegate

- (void)carouselView:(ZSTShellDCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;
{
    NSString *urlPath = [[self.shellDADArray objectAtIndex:index] safeObjectForKey:@"linkurl"];
    if (urlPath == nil || [urlPath length] == 0 || [urlPath isEmptyOrWhitespace]) {
        return;
    }
    
    ZSTWebViewController *webViewController = [[ZSTWebViewController alloc] init];
    
    [webViewController setURL:urlPath];
    webViewController.type = self.moduleType;
    webViewController.hidesBottomBarWhenPushed = YES;
    webViewController.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
    [self.navigationController pushViewController:webViewController animated:NO];
    [webViewController release];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return (1 << UIInterfaceOrientationPortrait);
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIViewController *)rootViewController;
{
    return self;
}

- (void)dealloc
{
    if (_pageControl != nil) {
        [_pageControl release];
    }
	[super dealloc];
}

@end
