//
//  CustomView.h
//  DrawTest
//
//  Created by LiZhenQu on 14/11/11.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CustomView;
typedef  void (^touchBlocked)(CustomView *cell,NSDictionary *dic);

@interface CustomView : UIView
{
    touchBlocked _clickblocked;
    
    NSArray *dataArray;
}

@property (nonatomic, retain) NSArray *colorArray;

- (void)initWithData:(NSArray *)array;

- (void) setBlock:(touchBlocked)block;

@end
