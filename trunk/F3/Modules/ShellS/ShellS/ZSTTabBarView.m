//
//  ZSTTabarView.m
//  ShellS
//
//  Created by LiZhenQu on 14/11/14.
//  Copyright (c) 2014年 xuhuijun. All rights reserved.
//

#import "ZSTTabBarView.h"
#import "ZSTTabBar.h"

@implementation ZSTTabBarView
@synthesize tabBar, contentView;

- (void)setTabBar:(ZSTTabBar *)aTabBar {
    if (tabBar != aTabBar) {
        [tabBar removeFromSuperview];
        tabBar = aTabBar;
        [self addSubview:tabBar];
    }
}

- (void)setContentView:(UIView *)aContentView {
    [contentView removeFromSuperview];
    contentView = aContentView;
    contentView.frame = CGRectMake(0, 0, self.bounds.size.width, self.tabBar.frame.origin.y);
    
    [self addSubview:contentView];
    [self sendSubviewToBack:contentView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect f = contentView.frame;
    
    if (self.tabBar.isInvisible) {
        f.size.height = self.bounds.size.height;
    } else {
        f.size.height = self.tabBar.frame.origin.y;
    }
    contentView.frame = f;
    //	[contentView layoutSubviews];
    [contentView setNeedsLayout];
}

- (void)drawRect:(CGRect)rect {
    CGContextRef c = UIGraphicsGetCurrentContext();
    [[UIColor colorWithRed:230/255.0 green:230/255.0 blue:230/255.0 alpha:1] set];
    CGContextFillRect(c, self.bounds);
}

@end
