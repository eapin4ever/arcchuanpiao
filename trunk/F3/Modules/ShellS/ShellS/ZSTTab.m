//
//  ZSTTab.m
//  ShellS
//
//  Created by LiZhenQu on 14/11/14.
//  Copyright (c) 2014年 xuhuijun. All rights reserved.
//

#import "ZSTTab.h"
#import "ZSTUtils.h"

#define kDefaultTitleColor      [UIColor colorWithRed:211.0f/255.0f green:204.0f/255.0f blue:196.0f/255.0f alpha:1.0f]
#define kHighlitedTitleColor    [UIColor whiteColor]
#define kTitleLabelHeight       12

@implementation UIButton (UIButtonTitleWithImage)

- (void) setImage1:(UIImage *)image withTitle:(NSString *)title forState:(UIControlState)stateType {
    
    //UIEdgeInsetsMake(CGFloat top, CGFloat left, CGFloat bottom, CGFloat right)
    
    // get the size of the elements here for readability
    CGSize imageSize = image.size;
    CGSize titleSize = [title sizeWithFont:[UIFont boldSystemFontOfSize:10.f]];
    
    // get the height they will take up as a unit
    CGFloat totalHeight = (imageSize.height + titleSize.height );
    
    // raise the image and push it right to center it
    
    // lower the text and push it left to center it
    UIImageView *newsIV = [[UIImageView alloc] init];
    newsIV.frame = CGRectMake(16, self.imageView.frame.origin.y, imageSize.width, imageSize.height);
    UIImageView *newsHighIV = [[UIImageView alloc] init];
    newsHighIV.frame = CGRectMake(16, self.imageView.frame.origin.y, imageSize.width, imageSize.height);
    
    //    [self.imageView setContentMode:UIViewContentModeCenter];
    
    [self setImageEdgeInsets:UIEdgeInsetsMake(- floor(totalHeight - imageSize.height), 0.0f , -2, 0.0)];
    newsHighIV.hidden = YES;
    [newsIV setContentMode:UIViewContentModeScaleAspectFit];
    [newsHighIV setContentMode:UIViewContentModeScaleAspectFit];
    [self addSubview:newsIV];
    [self addSubview:newsHighIV];
    
    //    [self setImage:image forState:stateType];
    if (stateType == UIControlStateNormal)
    {
        [newsIV setImage:image];
        newsIV.hidden = NO;
        newsHighIV.hidden = YES;
    }
    else
    {
        [newsHighIV setImage:image];
        newsHighIV.hidden = NO;
        newsIV.hidden = YES;
    }
    
    [self.titleLabel setContentMode:UIViewContentModeCenter];
    
    [self.titleLabel setBackgroundColor:[UIColor clearColor]];
    
    [self.titleLabel setFont:[UIFont boldSystemFontOfSize:10.f]];
    
    [self.titleLabel setTextColor:kDefaultTitleColor];
    
    [self setTitleEdgeInsets:UIEdgeInsetsMake(0.0, 0.0, - floor(totalHeight - titleSize.height) - 2, 0.0)];
    
    [self setTitle:title forState:stateType];
    
}

@end

@interface ZSTTab ()
@property (nonatomic, retain) UIImage *rightBorder;
@property (nonatomic, retain) UIImage *background;
@end

@implementation ZSTTab
@synthesize rightBorder, background;
@synthesize badgeView,tabTitleLabel;


-(void)setBadgeNumber:(NSInteger)count
{
    if (0 == count)
    {
        self.badgeView.alpha = 0;
        self.badgeView.text = @"0";
    }
    else
    {
        self.badgeView.alpha = 1;
        self.badgeView.text = @(count).stringValue;
    }
}

- (id)initWithIconImageName:(NSString *)imageName title:(NSString*)title
{
    if (self = [super init]) {
        self.adjustsImageWhenHighlighted = NO;
        self.background = [UIImage imageNamed:@"BCTabBarController.bundle/tab-background.png"];
        self.rightBorder = [UIImage imageNamed:@"BCTabBarController.bundle/tab-right-border.png"];
        self.backgroundColor = [UIColor clearColor];
        
        NSString *selectedStr = [[imageName stringByDeletingPathExtension] stringByReplacingOccurrencesOfString:@"_n" withString:@"_p"];
        NSString *selectedName = [NSString stringWithFormat:@"%@.%@",
                                  selectedStr,
                                  [imageName pathExtension]];
        
        if (title) {
            [self setImage1:[UIImage imageNamed:imageName] withTitle:title forState:UIControlStateNormal];
            [self setImage1:[UIImage imageNamed:selectedName] withTitle:title forState:UIControlStateSelected];
//            [self setTitleColor:kHighlitedTitleColor forState:UIControlStateSelected];
        } else {
            
            [self setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
            [self setBackgroundImage:[UIImage imageNamed:selectedName]  forState:UIControlStateSelected];
            
        }
        
        self.badgeView =  [[ZSTSignaView alloc] initWithImage:ZSTModuleImage(@"module_shells_left_messageCount.png")];
        self.badgeView.backgroundColor = [UIColor clearColor];
        self.badgeView.frame = CGRectMake(38, 5, 23, 20);
        [self setBadgeNumber:0];
        [self addSubview:self.badgeView];
        
    }
    return self;
}

- (id)initWithIconImageName:(NSString *)imageName title:(NSString*)title titleColor:(NSString *)color
{
    self = [self initWithIconImageName:imageName title:title];
    
    UIColor *co = [ZSTUtils colorFromHexColor:color];
    if (!co) {
        
        co = kDefaultTitleColor;
    }
    [self setTitleColor:co forState:UIControlStateNormal];
    return self;
}

- (void)setHighlighted:(BOOL)aBool {
    // no highlight state
}

- (void)setFrame:(CGRect)aFrame {
    [super setFrame:aFrame];
    [self setNeedsDisplay];
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
}


@end
