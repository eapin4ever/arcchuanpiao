//
//  ZSTTabar.h
//  ShellC
//
//  Created by LiZhenQu on 14/11/14.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

@class ZSTTab;

@protocol ZSTTabBarDelegate;

@interface ZSTTabBar: UIView

- (id)initWithFrame:(CGRect)aFrame;
- (void)setSelectedTab:(ZSTTab *)aTab animated:(BOOL)animated;

@property (nonatomic, retain) NSArray *tabs;
@property (nonatomic, retain) ZSTTab *selectedTab;
@property (nonatomic, assign) id <ZSTTabBarDelegate> delegate;
@property (nonatomic, retain) UIImageView *arrow;
@property (nonatomic, assign) BOOL isInvisible;
@end

@protocol ZSTTabBarDelegate

- (void)tabBar:(ZSTTabBar *)aTabBar didSelectTabAtIndex:(NSInteger)index;
@end

