//
//  ZSTShellSRootViewController.h
//  ShellS
//
//  Created by LiZhenQu on 14/11/14.
//  Copyright (c) 2014年 xuhuijun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTShellSTabarViewController.h"
#import "ZSTModuleBaseViewController.h"

@interface ZSTShellSRootViewController : UITabBarController<UINavigationControllerDelegate,ZSTModuleDelegate,ZSTTabBarControllerDelegate>

@property (nonatomic, retain) UINavigationController *navigationController;
@property (nonatomic, retain) ZSTShellSTabarViewController *tabbarController;

@end
