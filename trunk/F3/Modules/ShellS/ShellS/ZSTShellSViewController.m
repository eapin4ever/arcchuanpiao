                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        //
//  ZSTShellSViewController.m
//  ShellS
//
//  Created by LiZhenQu on 14/11/14.
//  Copyright (c) 2014年 xuhuijun. All rights reserved.
//

#import "ZSTShellSViewController.h"
#import "CustomView.h"
#import "ElementParser.h"
#import "ZSTModuleManager.h"
#import "ZSTUtils.h"
#import "ZSTMineViewController.h"

@interface ZSTShellSViewController ()
{
    CustomView *customView;
}

@end

@implementation ZSTShellSViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    if (IS_IOS_7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    customView = [[CustomView alloc]initWithFrame:CGRectMake(0, 0, 320, self.view.frame.size.height-44-49-20-33)];
    customView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:customView];
    
    [customView setBlock:^(CustomView *cell,NSDictionary *dic) {
        
        NSMutableDictionary *moduleParams = [NSMutableDictionary dictionaryWithDictionary:dic];
        
        int moduleId = [[dic safeObjectForKey:@"ModuleId"] intValue];
        NSString *title = [dic safeObjectForKey:@"Title"];
        
        ZSTModule *module = [[ZSTModuleManager shared] findModule:moduleId];
        if (module) {
            UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:moduleId withOptions:moduleParams];
            if (controller) {
                controller.hidesBottomBarWhenPushed = YES;
                controller.navigationItem.titleView = [ZSTUtils titleViewWithTitle:title];
                controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
                [self.navigationController pushViewController:controller animated:YES];
            }
        }

    }];
    
    [self imageButtonElementsForView];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
     self.navigationItem.rightBarButtonItem = [self initWithSubviews];
}

- (UIBarButtonItem *) initWithSubviews
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(mineAction:) forControlEvents:UIControlEventTouchUpInside];
    
    btn.layer.cornerRadius = btn.frame.size.width / 2.0;
    btn.layer.borderColor = [UIColor clearColor].CGColor;
    btn.layer.masksToBounds = YES;
    
    NSString *path = nil;
    UIImage *image = nil;
    NSDictionary *dic = [ZSTF3Preferences shared].avaterName;
    
    if (![ZSTF3Preferences shared].loginMsisdn || [ZSTF3Preferences shared].loginMsisdn.length == 0) {
        
        [ZSTF3Preferences shared].loginMsisdn = @"";
    }
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0) {
        
        image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
    } else {
        
        if (dic || [dic isKindOfClass:[NSDictionary class]]) {
            path = [dic valueForKey:[ZSTF3Preferences shared].loginMsisdn];
        }
        
        if (path && path.length > 0) {
            
            NSString *homePath = NSHomeDirectory();
            NSString *imgTempPath = [path substringFromIndex:[path rangeOfString:@"tmp"].location];
            NSString *finalPath = [NSString stringWithFormat:@"%@/%@",homePath,imgTempPath];
            NSData *reader = [NSData dataWithContentsOfFile:finalPath];
            image = [UIImage imageWithData:reader];
            
            if (!image) {
                
                image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
            }
            
        } else {
            
            image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
        }
    }
    
    [btn setImage:image forState:UIControlStateNormal];
    
    //暂时解决btn图片不能显示的问题
    UIImageView *iv = [[UIImageView alloc] initWithFrame:btn.imageView.frame];
    iv.image = image;
    [btn insertSubview:iv aboveSubview:btn.imageView];
    
    return [[[UIBarButtonItem alloc] initWithCustomView:btn] autorelease];
}


- (void)mineAction:(id)sender
{
    ZSTMineViewController *controller = [[ZSTMineViewController alloc] initWithNibName:@"ZSTMineViewController" bundle:nil];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

- (void)imageButtonElementsForView
{
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module_shells_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSArray *itemElements = [root selectElements:@"ImageButton Item"];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSMutableArray *clorArray = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < [itemElements count]; i++) {
        Element *itemElement = [[itemElements objectAtIndex:i] retain];
        
        NSString *btnName = [[itemElement selectElement:@"Title"] contentsText];
        NSNumber *moduleID = [[itemElement selectElement:@"ModuleId"] contentsNumber];
        NSNumber *moduleType = [[itemElement selectElement:@"ModuleType"] contentsNumber];
        NSString *TitleColor = [[itemElement selectElement:@"TitleColor"] contentsText];
        NSString *backgroundColor = [[itemElement selectElement:@"BackgroundColor"] contentsText];
        if (TitleColor == nil || [TitleColor length] == 0 || [TitleColor isEqualToString:@""]) {
            TitleColor = @"#000000";
        }
        
        NSDictionary *paramDic = [NSDictionary dictionary];
        if ([moduleID intValue] == 12 || [moduleID intValue] == 24) {
            NSString *interfaceUrl = [[itemElement selectElement:@"InterfaceUrl"] contentsText];
            paramDic = [NSDictionary dictionaryWithObjectsAndKeys:btnName,@"Title",moduleType,@"type",interfaceUrl,@"InterfaceUrl",TitleColor,@"TitleColor",moduleID,@"ModuleId",nil];
        }else{
            paramDic = [NSDictionary dictionaryWithObjectsAndKeys:btnName,@"Title",moduleType,@"type",TitleColor,@"TitleColor",moduleID,@"ModuleId", nil];
        }
        
        [array addObject:paramDic];
        [clorArray addObject:backgroundColor];
    }
    customView.colorArray = clorArray;
    [customView initWithData:array];
    [array release];
    [clorArray release];
    [parser release];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [customView release];
    [super dealloc];
}

@end
