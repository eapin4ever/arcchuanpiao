//
//  CustomView.m
//  DrawTest
//
//  Created by LiZhenQu on 14/11/11.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "CustomView.h"
#import "ZSTUtils.h"

#define PI 3.14159265358979323846 
#define radius 144

#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]

@implementation CustomView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _colorArray = [[NSArray alloc] init];
        dataArray = [[NSArray alloc] init];
    }
    return self;
}

static inline float radians(double degrees) {
    return degrees * PI / 180;
}

static inline void drawArc(CGContextRef ctx, CGPoint point, float angle_start, float angle_end, UIColor* color) {
    CGContextMoveToPoint(ctx, point.x, point.y);
//    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 255, 255, 255, 1);
//     CGContextSetLineWidth(ctx, 1.0);//线的宽度
    CGContextSetFillColor(ctx, CGColorGetComponents([color CGColor]));
//     CGContextSetFillColorWithColor(ctx, color.CGColor);//填充颜色
    CGContextAddArc(ctx, point.x, point.y, radius,  angle_start, angle_end, 0);
    //CGContextClosePath(ctx);
    CGContextFillPath(ctx);
//    CGContextDrawPath(ctx, kCGPathFillStroke);
}

- (void)drawRect:(CGRect)rect
{
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextClearRect(ctx, rect);
    
    float angle_start = radians(3.0);
    float angle_end = radians(60);
    drawArc(ctx, self.center, angle_start, angle_end, [self colorFromHexColor:[self.colorArray objectAtIndex:3]]);
    
    angle_start = radians(63);
    angle_end = radians(120.0);
    drawArc(ctx, self.center, angle_start, angle_end, [self colorFromHexColor:[self.colorArray objectAtIndex:4]]);
    
    angle_start = radians(123);
    angle_end = radians(180);
    drawArc(ctx, self.center, angle_start, angle_end, [self colorFromHexColor:[self.colorArray objectAtIndex:5]]);
    
    
    angle_start = radians(183);
    angle_end = radians(240);
    drawArc(ctx, self.center, angle_start, angle_end, [self colorFromHexColor:[self.colorArray objectAtIndex:6]]);
    
    angle_start = radians(243);
    angle_end = radians(300);
    drawArc(ctx, self.center, angle_start, angle_end, [self colorFromHexColor:[self.colorArray objectAtIndex:1]]);
    
    angle_start = radians(303);
    angle_end = radians(360);
    drawArc(ctx, self.center, angle_start, angle_end, [self colorFromHexColor:[self.colorArray objectAtIndex:2]]);
    
     angle_start = radians(0.0);
     angle_end = radians(360.0);
    
    UIColor*aColor = [self colorFromHexColor:[self.colorArray objectAtIndex:0]];
    CGContextSetFillColorWithColor(ctx, aColor.CGColor);//填充颜色
    CGContextSetLineWidth(ctx, 0.0);//线的宽度
    CGContextAddArc(ctx, self.center.x, self.center.y, 62, 0, 2*PI, 0); //添加一个圆
    CGContextDrawPath(ctx, kCGPathFillStroke);
}

- (UIColor *)colorFromHexColor:(NSString *)hexColor
{
    if (![hexColor hasPrefix:@"#"]) {
        return [UIColor whiteColor];
    }
    
    if ([hexColor length] < 6) {
        return [UIColor whiteColor];
    }
    hexColor = [hexColor substringFromIndex:[hexColor length] - 6];
    
    unsigned int red,green,blue;
    NSRange range;
    range.length = 2;
    
    range.location = 0;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&red];
    
    range.location = 2;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&green];
    
    range.location = 4;
    [[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&blue];
    
    return [UIColor colorWithRed:(float)(red/255.0f) green:(float)(green / 255.0f) blue:(float)(blue / 255.0f) alpha:1.0f];
}

- (void)initWithData:(NSArray *)array
{
    if (!array || ![array isKindOfClass:[NSArray class]] || array.count < 7) {
        
        return;
    }
    
    dataArray = [array retain];
    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake((self.frame.size.width - 117 )/2, (self.frame.size.height - 117 )/2, 117, 117)];
    image.image = ZSTModuleImage(@"module_shells_icon1_n.png");
    [self addSubview:image];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = image.frame;
    [button addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    button.tag = 0;
    [self addSubview:button];
    [image release];
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width - 80)/2, 35+(iPhone5?40:0)-(IS_IOS_7?0:15), 80, 21)];
    label1.backgroundColor = [UIColor clearColor];
    label1.textAlignment = NSTextAlignmentCenter;
    label1.font = [UIFont systemFontOfSize:13];
    label1.text = [[array objectAtIndex:1] safeObjectForKey:@"Title"];
    label1.textColor = [self colorFromHexColor:[[array objectAtIndex:1] safeObjectForKey:@"TitleColor"]];
    [self addSubview:label1];
    [label1 release];
    
    UIImageView *image1 = [[UIImageView alloc] initWithFrame:CGRectMake(145, 60+(iPhone5?40:0)-(IS_IOS_7?0:15), 36, 36)];
    image1.image = ZSTModuleImage(@"module_shells_icon2_n.png");
    [self addSubview:image1];
    [image1 release];
    
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    button1.backgroundColor = [UIColor clearColor];
    button1.frame = CGRectMake(120, 22+(iPhone5?40:0)-(IS_IOS_7?0:15), 80, 70);
    [button1 addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    button1.tag = 1;
    [self addSubview:button1];
    
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(225, 143+(iPhone5?40:0)-(IS_IOS_7?0:15), 70, 21)];
    label2.backgroundColor = [UIColor clearColor];
    label2.textAlignment = NSTextAlignmentCenter;
    label2.font = [UIFont systemFontOfSize:13];
    label2.text = [[array objectAtIndex:2] safeObjectForKey:@"Title"];
    label2.textColor = [self colorFromHexColor:[[array objectAtIndex:2] safeObjectForKey:@"TitleColor"]];
    [self addSubview:label2];
    [label2 release];
    
    UIImageView *image2 = [[UIImageView alloc] initWithFrame:CGRectMake(235, 97+(iPhone5?35:0)-(IS_IOS_7?0:15), 36, 36)];
    image2.image = ZSTModuleImage(@"module_shells_icon3_n.png");
    [self addSubview:image2];
    [image2 release];
    
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    button2.backgroundColor = [UIColor clearColor];
    button2.frame = CGRectMake(222, 76+(iPhone5?44:0)-(IS_IOS_7?0:15), 60, 90);
    [button2 addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    button2.tag = 2;
    [self addSubview:button2];
    
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(222, 180+(iPhone5?40:0)-(IS_IOS_7?0:15), 70, 21)];
    label3.backgroundColor = [UIColor clearColor];
    label3.textAlignment = NSTextAlignmentCenter;
    label3.font = [UIFont systemFontOfSize:13];
    label3.text = [[array objectAtIndex:3] safeObjectForKey:@"Title"];
    label3.textColor = [self colorFromHexColor:[[array objectAtIndex:3] safeObjectForKey:@"TitleColor"]];
    [self addSubview:label3];
    [label3 release];
    
    UIImageView *image3 = [[UIImageView alloc] initWithFrame:CGRectMake(235, 205+(iPhone5?40:0)-(IS_IOS_7?0:15), 36, 36)];
    image3.image = ZSTModuleImage(@"module_shells_icon4_n.png");
    [self addSubview:image3];
    [image3 release];
    
    UIButton *button3 = [UIButton buttonWithType:UIButtonTypeCustom];
    button3.backgroundColor = [UIColor clearColor];
    button3.frame = CGRectMake(222, 170+(iPhone5?50:0)-(IS_IOS_7?0:15), 60, 70);
    [button3 addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    button3.tag = 3;
    [self addSubview:button3];

    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake((self.frame.size.width - 80)/2, 283+(iPhone5?40:0)-(IS_IOS_7?0:15), 80, 21)];
    label4.backgroundColor = [UIColor clearColor];
    label4.textAlignment = NSTextAlignmentCenter;
    label4.font = [UIFont systemFontOfSize:13];
    label4.text = [[array objectAtIndex:4] safeObjectForKey:@"Title"];
    label4.textColor = [self colorFromHexColor:[[array objectAtIndex:4] safeObjectForKey:@"TitleColor"]];
    [self addSubview:label4];
    [label4 release];
    
    UIImageView *image4 = [[UIImageView alloc] initWithFrame:CGRectMake(142, 245+(iPhone5?40:0)-(IS_IOS_7?0:15), 36, 36)];
    image4.image = ZSTModuleImage(@"module_shells_icon5_n.png");
    [self addSubview:image4];
    [image4 release];
    
    UIButton *button4 = [UIButton buttonWithType:UIButtonTypeCustom];
    button4.backgroundColor = [UIColor clearColor];
    button4.frame = CGRectMake(125, 226+(iPhone5?44:0)-(IS_IOS_7?0:15), 60, 80);
    [button4 addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    button4.tag = 4;
    [self addSubview:button4];
    
    UILabel *label5 = [[UILabel alloc] initWithFrame:CGRectMake(30, 177+(iPhone5?40:0)-(IS_IOS_7?0:15), 80, 21)];
    label5.backgroundColor = [UIColor clearColor];
    label5.textAlignment = NSTextAlignmentCenter;
    label5.font = [UIFont systemFontOfSize:13];
    label5.text = [[array objectAtIndex:5] safeObjectForKey:@"Title"];
    label5.textColor = [self colorFromHexColor:[[array objectAtIndex:5] safeObjectForKey:@"TitleColor"]];
    [self addSubview:label5];
    [label5 release];
    
    UIImageView *image5 = [[UIImageView alloc] initWithFrame:CGRectMake(55, 205+(iPhone5?40:0)-(IS_IOS_7?0:15), 36, 36)];
    image5.image = ZSTModuleImage(@"module_shells_icon6_n.png");
    [self addSubview:image5];
    [image5 release];
    
    UIButton *button5 = [UIButton buttonWithType:UIButtonTypeCustom];
    button5.backgroundColor = [UIColor clearColor];
    button5.frame = CGRectMake(40, 176+(iPhone5?44:0)-(IS_IOS_7?0:15), 60, 70);
    [button5 addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    button5.tag = 5;
    [self addSubview:button5];
    
    UILabel *label6 = [[UILabel alloc] initWithFrame:CGRectMake(30, 140+(iPhone5?40:0)-(IS_IOS_7?0:15), 80, 21)];
    label6.backgroundColor = [UIColor clearColor];
    label6.textAlignment = NSTextAlignmentCenter;
    label6.font = [UIFont systemFontOfSize:13];
    label6.text = [[array objectAtIndex:6] safeObjectForKey:@"Title"];
    label6.textColor = [self colorFromHexColor:[[array objectAtIndex:6] safeObjectForKey:@"TitleColor"]];
    [self addSubview:label6];
    [label6 release];
    
    UIImageView *image6 = [[UIImageView alloc] initWithFrame:CGRectMake(55, 100+(iPhone5?35:0)-(IS_IOS_7?0:15), 36, 36)];
    image6.image = ZSTModuleImage(@"module_shells_icon7_n.png");
    [self addSubview:image6];
    [image6 release];
    
    UIButton *button6 = [UIButton buttonWithType:UIButtonTypeCustom];
    button6.backgroundColor = [UIColor clearColor];
    button6.frame = CGRectMake(40, 76+(iPhone5?44:0)-(IS_IOS_7?0:15), 60, 80);
    [button6 addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    button6.tag = 6;
    [self addSubview:button6];
}

- (void) setBlock:(touchBlocked)block
{
    _clickblocked = [block copy];
}

- (void)clickAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    NSDictionary *dic = [dataArray objectAtIndex:button.tag];
    
    _clickblocked(self,dic);
}

- (void) dealloc
{
    [dataArray release];
    [_colorArray release];
    [super dealloc];
}

@end
