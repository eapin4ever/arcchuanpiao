//
//  ZSTTab.h
//  ShellS
//
//  Created by LiZhenQu on 14/11/14.
//  Copyright (c) 2014年 xuhuijun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTSignaView.h"

@interface UIButton (UIButtonTitleWithImage)

- (void) setImage1:(UIImage *)image withTitle:(NSString *)title forState:(UIControlState)stateType;

@end

@interface ZSTTab : UIButton
{
    UIImage *background;
    UIImage *rightBorder;
}

@property (retain, nonatomic) UILabel* tabTitleLabel;
@property (retain, nonatomic) ZSTSignaView *badgeView;

@property (retain, nonatomic) NSString *string;

//- (id)initWithIconImageName:(NSString *)imageName;
- (id)initWithIconImageName:(NSString *)imageName title:(NSString*)title;
- (id)initWithIconImageName:(NSString *)imageName title:(NSString*)title titleColor:(NSString *)color;
-(void)setBadgeNumber:(NSInteger)count;

@end