//
//  ZSTShellSTabarViewController.h
//  ShellS
//
//  Created by LiZhenQu on 14/11/14.
//  Copyright (c) 2014年 xuhuijun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTTabBar.h"

#define kBCTabBarControllerKey      @"kBCTabBarControllerKey"


@class ZSTTabBarView;
@class ZSTTabBarController;

@protocol ZSTTabBarControllerDelegate <NSObject>

- (void)shellstabbarController:(ZSTTabBarController*)controller didSelectTabAtIndex:(NSInteger)index;

@end


@interface ZSTShellSTabarViewController : UIViewController<ZSTTabBarDelegate,UINavigationControllerDelegate>

@property (nonatomic, retain) NSArray *viewControllers;
@property (nonatomic, retain) ZSTTabBar *tabBar;
@property (nonatomic, retain) UIViewController *selectedViewController;
@property (nonatomic, retain) ZSTTabBarView *tabBarView;
@property (nonatomic) NSUInteger selectedIndex;
@property (nonatomic, readonly) BOOL visible;
@property (assign, nonatomic) id<ZSTTabBarControllerDelegate> mDelegate;

- (void)replaceControllerAtIndex:(int)index withController:(id)controller;

@end
