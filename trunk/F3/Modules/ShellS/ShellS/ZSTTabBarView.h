//
//  ZSTTabarView.h
//  ShellS
//
//  Created by LiZhenQu on 14/11/14.
//  Copyright (c) 2014年 xuhuijun. All rights reserved.
//

@class ZSTTabBar;

@interface ZSTTabBarView : UIView

@property (nonatomic, assign) UIView *contentView;
@property (nonatomic, assign) ZSTTabBar *tabBar;


@end