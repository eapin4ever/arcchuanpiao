//
//  ZSTShellSRootViewController.m
//  ShellS
//
//  Created by LiZhenQu on 14/11/14.
//  Copyright (c) 2014年 xuhuijun. All rights reserved.
//

#import "ZSTShellSRootViewController.h"
#import "ZSTShell.h"
#import "ZSTGlobal+F3.h"
#import "ZSTModuleAddition.h"

@interface ZSTShellSRootViewController ()

@end

@implementation ZSTShellSRootViewController

@synthesize application;
@synthesize navigationController;
@synthesize rootView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (IS_IOS_7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    NSArray *array = [ZSTShell viewControllerForShellSTabBarController];
    [self initTabbarViewContorller:array];
}

- (void)initTabbarViewContorller:(NSArray *)controllers
{
    if (controllers) {
        
        if (self.tabbarController) {
            self.tabbarController = nil;
        }
        
        self.tabbarController = [[[ZSTShellSTabarViewController alloc] init] autorelease];
        self.tabbarController.viewControllers = [NSArray arrayWithArray:controllers];
        self.tabbarController.mDelegate = self;
    }
}

#pragma mark - ZSTTabBarControllerDelegate

- (void)shellstabbarController:(ZSTTabBarController*)controller didSelectTabAtIndex:(NSInteger)index
{
    
}


//- (void)reloadFirstControllerModulID:(NSInteger)modulID interfaceUrl:(NSString *)interfaceUrl type:(NSInteger)type
//{
//    ZSTF3Preferences *per = [ZSTF3Preferences shared];
//    if (modulID == 22 || modulID == -1) {
//        per.isInPush = YES;
//    }else{
//        per.isInPush = NO;
//    }
//    ZSTModuleBaseViewController *controller = (ZSTModuleBaseViewController *)[ZSTShell reloadShellCFirstControllerModulID:modulID interfaceUrl:interfaceUrl type:type];
//    if (controller) {
//        self.navigationController = [[[UINavigationController alloc] initWithRootViewController: controller] autorelease];
//        self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed:@"Module.bundle/module_shellc_top_bg.png"];
//        controller.navigationItem.titleView = [ZSTUtils shellClogoView];
//        
//        controller.iconImageName = @"Module.bundle/module_shellc_tabbar_mainicon3_n.png";
//    }
//    [self.tabbarController replaceControllerAtIndex:2 withController:self.navigationController];
//}


- (UIViewController *)rootViewController;
{
    return self.tabbarController;
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return (1 << UIInterfaceOrientationPortrait);
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
