//
//  ZSTTabar.m
//  ShellC
//
//  Created by LiZhenQu on 14/11/14.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTTabBar.h"
#import "ZSTTab.h"
#define kTabMargin 0.0

@interface ZSTTabBar ()
@property (nonatomic, retain) UIImage *backgroundImage;

- (void)positionArrowAnimated:(BOOL)animated;
@end

@implementation ZSTTabBar
@synthesize tabs, selectedTab, backgroundImage, arrow, delegate,isInvisible;

- (id)initWithFrame:(CGRect)aFrame {
    
    if (self = [super initWithFrame:aFrame]) {
        
        self.backgroundColor = [UIColor clearColor];
        
        UIImageView *backgroudView = [[UIImageView alloc] initWithFrame:CGRectMake(0, -30, self.bounds.size.width, self.bounds.size.height+35)];
        self.backgroundImage = [UIImage imageNamed:@"framework_bottom_bg.png"];
        backgroudView.image = self.backgroundImage;
        backgroudView.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:backgroudView];
        
        self.userInteractionEnabled = YES;
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight |
        UIViewAutoresizingFlexibleTopMargin;
        
    }
    
    return self;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    CGContextRef context = UIGraphicsGetCurrentContext();
    [self.backgroundImage drawAtPoint:CGPointMake(0, 0)];
    //	[[UIColor blackColor] set];
    CGContextFillRect(context, CGRectMake(0, self.bounds.size.height / 2, self.bounds.size.width, self.bounds.size.height / 2));
}

- (void)setTabs:(NSArray *)array {
    if (tabs != array) {
        for (ZSTTab *tab in tabs) {
            [tab removeFromSuperview];
        }
        
        tabs = [array retain];
        
        for (ZSTTab *tab in tabs) {
            tab.userInteractionEnabled = YES;
            [tab addTarget:self action:@selector(tabSelected:) forControlEvents:UIControlEventTouchDown];
        }
        [self setNeedsLayout];
        
    }
}

- (void)setSelectedTab:(ZSTTab *)aTab animated:(BOOL)animated {
    if (aTab != selectedTab) {
        selectedTab = aTab;
        selectedTab.selected = YES;
        
        for (ZSTTab *tab in tabs) {
            if (tab == aTab) continue;
            
            tab.selected = NO;
        }
    }
    
    [self positionArrowAnimated:animated];
}

- (void)setSelectedTab:(ZSTTab *)aTab {
    [self setSelectedTab:aTab animated:YES];
}

- (void)tabSelected:(ZSTTab *)sender {
   	[self.delegate tabBar:self didSelectTabAtIndex:[self.tabs indexOfObject:sender]];
}

- (void)positionArrowAnimated:(BOOL)animated {
    if (animated) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.2];
    }
    CGRect f = self.arrow.frame;
    f.origin.x = self.selectedTab.frame.origin.x + ((self.selectedTab.frame.size.width / 2) - (f.size.width / 2));
    self.arrow.frame = f;
    
    if (animated) {
        [UIView commitAnimations];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGRect f = self.bounds;
    f.size.width /= self.tabs.count;
    f.size.width -= (kTabMargin * (self.tabs.count + 1)) / self.tabs.count;
    
    ZSTTab *tab0 = [self.tabs objectAtIndex:0];
    tab0.frame = CGRectMake(35, -12, 60, 43);
    
    
    ZSTTab *tab1 = [self.tabs objectAtIndex:1];
    tab1.frame = CGRectMake(130, -25, 60, 55);
    
    
    ZSTTab *tab2 = [self.tabs objectAtIndex:2];
    tab2.frame = CGRectMake(225, -12, 60, 43);
    
    [self addSubview:tab0];
    [self addSubview:tab1];
    [self addSubview:tab2];
}

- (void)setFrame:(CGRect)aFrame {
    [super setFrame:aFrame];
    [self setNeedsDisplay];
}


@end
