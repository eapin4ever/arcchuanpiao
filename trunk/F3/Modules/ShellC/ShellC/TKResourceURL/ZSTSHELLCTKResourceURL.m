//
//  TKResourceURL.h
//  News
//
//  Created by luobin on 7/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTSHELLCTKResourceURL.h"

@implementation ZSTSHELLCTKResourceURL

- (void)dealloc
{
    [super dealloc];
}

+ (ZSTSHELLCTKResourceURL*) resourceURLWithURL:(NSURL *)url
{
    if ([self isResourceURL:url]) {
        return [ZSTSHELLCTKResourceURL URLWithString:url.absoluteString];
    }
    return nil;
}

+ (ZSTSHELLCTKResourceURL*) resourceURLWithResource:(NSString *)resource originalUrl:(NSString *)originalUrl MIMEType:(NSString *)MIMEType
{
    if (resource == nil) {
        resource = @"";
    }
    if (originalUrl == nil) {
        originalUrl = @"";
    }
    if (MIMEType == nil) {
       MIMEType = @"text/html";
    }
    return [ZSTSHELLCTKResourceURL URLWithString:[NSString stringWithFormat:@"resource://?resource=%@&originalUrl=%@&MIMEType=%@", [resource urlEncodeValue],[originalUrl urlEncodeValue], MIMEType]];
}

+ (BOOL)isResourceURL:(NSURL *)url
{
    return [url.scheme caseInsensitiveCompare:@"resource"] == NSOrderedSame;
}

- (NSString *)resource
{
    return [[[self.query queryDictionaryUsingEncoding:NSUTF8StringEncoding] objectForKey:@"resource"] urlDecodeValue];
}

- (NSString *)MIMEType
{
    NSString *MIMEType = [[self.query queryDictionaryUsingEncoding:NSUTF8StringEncoding] objectForKey:@"MIMEType"];
    if (MIMEType == nil) {
        MIMEType = @"text/html";
    }
    return MIMEType;
}

- (NSString *)originalUrl
{
    return [[[self.query queryDictionaryUsingEncoding:NSUTF8StringEncoding] objectForKey:@"originalUrl"] urlDecodeValue];
}
@end
