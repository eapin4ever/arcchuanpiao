//
//  ResourceURLProtocol.m
//  News
//
//  Created by luobin on 7/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTSHELLCTKResourceURLProtocol.h"

@implementation ZSTSHELLCTKResourceURLProtocol

static NSString *cacheDirectory;

+ (void)initialize
{
    cacheDirectory = [[[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"Resource"] retain];
    NSFileManager *fileManager = [[NSFileManager alloc] init];
    if (![fileManager fileExistsAtPath:cacheDirectory]) {
        [fileManager createDirectoryAtPath:cacheDirectory withIntermediateDirectories:YES attributes:nil error:nil];
    }
    [fileManager release];
}

+ (NSString *)cacheDirectory
{
    return cacheDirectory;
}

+ (NSString *)cachePathForResourceURL:(ZSTSHELLCTKResourceURL *)url
{
    NSString *filename = [url.resource md5];
   return [cacheDirectory stringByAppendingPathComponent:filename];  
}


+ (void) registerProtocol {
	static BOOL inited = NO;
	if ( ! inited ) {
		[NSURLProtocol registerClass:[ZSTSHELLCTKResourceURLProtocol class]];
		inited = YES;
	}
}

+ (BOOL)canInitWithRequest:(NSURLRequest *)request
{
    return [ZSTSHELLCTKResourceURL isResourceURL:request.URL];
}

+ (NSURLRequest *)canonicalRequestForRequest:(NSURLRequest *)request
{
    return request;
}

-(void)startLoading
{   
    ZSTSHELLCTKResourceURL *url = [ZSTSHELLCTKResourceURL resourceURLWithURL:self.request.URL];
	NSString *actualURL = [url resource];
    NSString *MIMEType = [url MIMEType];
    NSString *path = [ZSTSHELLCTKResourceURLProtocol cachePathForResourceURL:url];
    NSFileManager *fileManager = [[[NSFileManager alloc] init] autorelease];  
    if ([fileManager fileExistsAtPath:path]) {   
        
        NSData *data = [NSData dataWithContentsOfFile:path];  

        NSURLResponse *response = [[[NSURLResponse alloc] initWithURL:[[self request] URL]
                                                            MIMEType:MIMEType
                                               expectedContentLength:[data length]
                                                    textEncodingName:nil] autorelease];
        
        [[self client] URLProtocol:self didReceiveResponse:response cacheStoragePolicy:NSURLCacheStorageAllowed];
        [[self client] URLProtocol:self didLoadData:data];
        [[self client] URLProtocolDidFinishLoading:self];
        
        return;  
    }
    
    NSMutableURLRequest *newRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:actualURL ] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:self.request.timeoutInterval];  
    newRequest.allHTTPHeaderFields = self.request.allHTTPHeaderFields;  
    newRequest.HTTPShouldHandleCookies = self.request.HTTPShouldHandleCookies; 
    
    /**
        由于UIWebView就是在子线程调用startLoading:的，不用担心阻塞的问题，所以无需使用异步请求： 
     **/
    NSError *error = nil;  
    NSURLResponse *response = nil;  
    NSData *data = [NSURLConnection sendSynchronousRequest:newRequest returningResponse:&response error:&error];  
    if (error) {  
        NSLog(@"%@", error);  
        [[self client] URLProtocol:self didFailWithError:error]; 
        return;
    }
    
    BOOL createFileSuccess = [fileManager createFileAtPath:path contents:data attributes:nil];  
    if (!createFileSuccess) {  
        NSError *error = [NSError errorWithDomain:NSCocoaErrorDomain code:NSFileWriteUnknownError userInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"Cache resource %@ failed because we were to create a file at %@",actualURL,path],NSLocalizedDescriptionKey,nil]];
        [[self client] URLProtocol:self didFailWithError:error]; 
        return;
    }
    
    NSURLResponse *newResponse = [[[NSURLResponse alloc] initWithURL:self.request.URL MIMEType:response.MIMEType expectedContentLength:data.length textEncodingName:nil] autorelease];   
    [[self client] URLProtocol:self didReceiveResponse:newResponse cacheStoragePolicy:NSURLCacheStorageAllowed];
    [[self client] URLProtocol:self didLoadData:data];
    [[self client] URLProtocolDidFinishLoading:self];
}

-(void)stopLoading
{
    return;
}

@end
