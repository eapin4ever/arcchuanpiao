//
//  ZSTF3Engine+ShellC.m
//  ShellC
//
//  Created by LiZhenQu on 13-4-26.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTF3Engine+ShellC.h"
#import "ZSTF3Global+ShellC.h"
#import "ZSTShellCContentVO.h"
#import "JSON.h"

#define ShellCContentCachePath [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"/ShellC/content"]


@implementation ZSTF3Engine (ShellC)

- (void)getHomeCoverWithDelegate:(id<ZSTF3EngineDelegate>)delegate
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [[ZSTCommunicator shared] openAPIPostToPath:GetCoverList 
                                         params:params
                                         target:self
                                       selector:@selector(getHomeCoverResponse:userInfo:)
                                       userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:NO];

}

- (void)getHomeCoverResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        NSArray* array = [HomeCoverInfo coverListWithDic:response.jsonResponse];
               
        if ([self isValidDelegateForSelector:@selector(getHomeCoverSucceed:)]) {
            
            [self.delegate getHomeCoverSucceed:array];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getHomeCoverFailed:)]) {
            [self.delegate getHomeCoverFailed:response.resultCode];
        }
    }
}

//新闻内容缓存到文件中
- (void)getShellCContentUserInfo:(id)anUserInfo
{
    
    [self performSelector:@selector(getShellCContentResponse:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:anUserInfo, @"userInfo", nil] afterDelay:0.5];
}

- (void)getShellCContentResponse:(NSDictionary *)param
{
    id userInfo = [param safeObjectForKey:@"userInfo"];
    if ([self isValidDelegateForSelector:@selector(getShellCContentDidSucceed:)]) {
        [self.delegate getShellCContentDidSucceed:userInfo];
    }
}


@end
