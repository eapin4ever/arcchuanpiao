//
//  ZSTShellCContentVO.h
//  ShellC
//
//  Created by LiZhenQu on 13-4-26.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsCatagoryInfo : NSObject

@property (nonatomic, strong) NSString *notice;
@property (nonatomic, assign) NSInteger categoryid;
@property (nonatomic, strong) NSString *categoryname;
@property (nonatomic, assign) NSInteger ordernum;
@property (nonatomic, assign) NSInteger version;
@property (nonatomic, assign) BOOL defaultflag;
@property (nonatomic, strong) NSString *iconurl;

+ (NewsCatagoryInfo *)newsCatagoryInfoWithdic:(NSDictionary*)dic;
+ (NSMutableArray*)newsCatagoryListWithdic:(NSDictionary*)dic;

@end


@interface HomeCoverInfo : NSObject

@property (nonatomic) int coverId;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* picURL;
@property (nonatomic, strong) NSString* linkURL;
@property (nonatomic, strong) NSString* summary;
@property (nonatomic, strong) NSString* redirectURL;
@property (nonatomic, strong) NSString* Description;
@property (nonatomic, strong) NSString* addTime;
@property (nonatomic) int orderNum;
@property (nonatomic, strong) NSArray *content;

- (id)initWithDic:(NSDictionary*)dic;
+ (NSArray*)coverListWithDic:(NSDictionary*)dic;

@end


@interface ZSTShellCContentVO : NSObject{
    NSDictionary *dict;
}

@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *summary;
@property (nonatomic, retain) NSString *keyword;
@property (nonatomic, retain) NSString *source;
@property (nonatomic, retain) NSString *addtime;
@property (nonatomic, assign) BOOL isfavorites;
@property (nonatomic, assign) int detailid;
@property (nonatomic, retain) NSString *letter;
@property (nonatomic, retain) NSString *imageurl;
@property (nonatomic, retain) NSString *imagedesc;
@property (nonatomic, assign) int ordernum;
@property (nonatomic, retain) NSString *officialurl;
@property (nonatomic, retain) NSString *onlineservice;
@property (nonatomic, retain) NSString *ServicePhone;



+ (id)voWithDic:(NSDictionary *)dic;
- (id)initWithDic:(NSDictionary *)dic;

@end
