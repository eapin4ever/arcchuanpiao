//
//  AppContext.m
//  Ancient
//
//  Created by Admin on 2/29/12.
//  Copyright (c) 2012 zhangshangtong. All rights reserved.
//

#import "AppContext.h"

@implementation NSMutableArray (QueueAdditions)

@end

@implementation AppContext

@synthesize accountid;
@synthesize userDic;
@synthesize myWorks;//这里的myWorks只是临时用于存储个人主页里的作品列表，不一定是我的作品列表。以下参数皆同。
@synthesize otherUserDic;
@synthesize commentNumber;
static AppContext *_sharedAppContext = nil;

+ (AppContext *)sharedContext;
{
    @synchronized(self)
    {
        if (_sharedAppContext  ==  nil)
        {
            _sharedAppContext = [[self alloc] init];
        }
    }
    
    return _sharedAppContext;
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if (_sharedAppContext == nil)
        {
            _sharedAppContext = [super allocWithZone:zone];
            return  _sharedAppContext;
        }
    }
    
    return  nil;
}

- (id)init
{
    if ((self=[super init])) {
        

    }
    return self;
}

- (NSString *)accountid
{
}

-(int) getUpdateFrontTime
{
    return [NSDate timeIntervalSinceReferenceDate];
}

- (void)eventUpdateMyWorks:(NSMutableArray *)arr
{
    self.myWorks = [NSMutableArray arrayWithArray:arr];
}

- (void)eventUpdateUserDic:(NSMutableDictionary *)dic
{
    if (userDic != nil) {
        [self updateDictionary:self.userDic with:dic];
    } else {
        self.userDic = dic;
    }
}
- (void)eventUpdateOtherUserDic:(NSMutableDictionary *)dic{
    if (otherUserDic != nil) {
        [self updateDictionary:self.otherUserDic with:dic];
    } else {
        self.otherUserDic = dic;
    }
}
- (void)updateDictionary:(NSMutableDictionary *)dic with:(NSMutableDictionary *)newDic{
//    CCLOG(@"----------------old and young :%@ -----------------------and %@ ",dic,newDic);
    for (id key in newDic) {
        id val = [newDic objectForKey:key];
        if ([val isKindOfClass:[NSMutableDictionary class]])
        {
            NSMutableDictionary *dicx = [dic objectForKey:key];
            if (dicx != nil) {
                [self updateDictionary:dicx with:val];
            } else 
            {
                if (![dic isKindOfClass:[NSNull class]]) {
                    [dic setValue:val forKey:key];
                }
            }
        }
        else
        {
            if (![dic isKindOfClass:[NSNull class]]) {
                [dic setValue:val forKey:key];
            }
        }
    }
}

-(void)removeData {
    self.userDic = nil;
    self.accountid = nil;
    self.otherUserDic = nil;
}

@end
