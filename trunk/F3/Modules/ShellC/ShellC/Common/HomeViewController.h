//
//  HomeViewController.h
//  VoiceChina
//
//  Created by huijun xu on 13-2-17.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTModuleBaseViewController.h"
#import "ZSTLeftViewController.h"


@interface HomeViewController : ZSTModuleBaseViewController

@property (strong, nonatomic) IBOutlet UIButton *left;
@property (strong, nonatomic) IBOutlet UIButton *right;
@property (strong, nonatomic) IBOutlet UIImageView *naviBackImageView;

@property (strong, nonatomic) NSMutableArray *carouselArray;


@end
