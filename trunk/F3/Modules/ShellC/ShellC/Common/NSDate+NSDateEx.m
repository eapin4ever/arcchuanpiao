//
//  NSDate+NSDateEx.m
//  star_letv
//
//  Created by Yunfei Bai on 11-12-26.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import "NSDate+NSDateEx.h"

@implementation NSDate (NSDateEx)
#pragma mark ------------------　时　间　-----------------
+(int) minutesSinceMidnight: (NSDate *)date
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned unitFlags =  NSHourCalendarUnit | NSMinuteCalendarUnit;
    NSDateComponents *components = [gregorian components:unitFlags fromDate:date];
    
    return 60 * [components hour] + [components minute];    
}


+(int) secondsSinceMidnight: (NSDate *)date
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    unsigned unitFlags =  NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *components = [gregorian components:unitFlags fromDate:date];
    int hour = [components hour];
    int minute = [components minute];
    int second = [components second];
    return 60 * 60 * hour + 60 * minute + second;    
}

+ (NSString*)dateStringWithDateString:(NSString*)dateString
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate* date = [formatter dateFromString:dateString];
    return [[self class] dateStringWithDate:date];
}

+(NSString *) dateStringWithDate:(NSDate*) date
{
    NSTimeInterval interval = [date timeIntervalSince1970];
    return [NSDate dateStringWithTimeInterval:interval];
}

+(NSString *) dateStringWithTimeInterval:(NSTimeInterval) timeInterval
{
    double lastTimeStamp = [[NSDate date] timeIntervalSince1970];
    
    NSString *resultString = nil;
    
    NSTimeInterval currentTimeInterval = lastTimeStamp;

    //获得今天凌晨到现在已经过去的时间
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* component = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    NSDate* date = [calendar dateFromComponents:component];
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSTimeInterval midnightTimeInterval = [date timeIntervalSince1970];
    
//    NSTimeInterval midnightTimeInterval = currentTimeInterval - 86400;
    
    NSTimeInterval offsetSinceMidnight = timeInterval - midnightTimeInterval;
    
    if (currentTimeInterval - timeInterval <= 60*60) {
        NSTimeInterval timeOffset = currentTimeInterval - timeInterval;
        
        if (timeOffset < 60)
        {
            resultString = [NSString stringWithFormat:@"刚刚"];
        }
        else if(timeOffset < 60 * 60)
        {
            resultString = [NSString stringWithFormat: @"%d分钟前", (int)timeOffset / 60];
        }
        else
        {
            resultString = [NSString stringWithFormat: @"%d小时前", (int)timeOffset / 60 / 60];
        }
    }
    else {
        if (offsetSinceMidnight > 0) {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat: @"HH:mm"];
            
            NSDate *targetDate = [NSDate dateWithTimeIntervalSince1970: timeInterval];
            resultString = [formatter stringFromDate: targetDate];
        }
        else {
            component.day --;
            NSDate* yesterday = [calendar dateFromComponents:component];
            NSTimeInterval yesterdayInterval = [yesterday timeIntervalSince1970];
            if (timeInterval > yesterdayInterval) {
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat: @"昨天HH:mm"];
                NSDate *targetDate = [NSDate dateWithTimeIntervalSince1970: timeInterval];
                resultString = [formatter stringFromDate: targetDate];
            }
            else
            {
                component.year --;
                NSDate* lastYear = [calendar dateFromComponents:component];
                NSTimeInterval lastYesrInterval = [lastYear timeIntervalSince1970];
                if (timeInterval > lastYesrInterval) {
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat: @"MM-dd HH:mm"];
                    NSDate *targetDate = [NSDate dateWithTimeIntervalSince1970: timeInterval];
                    resultString = [formatter stringFromDate: targetDate];
                }
                else {
                    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                    [formatter setDateFormat: @"yyyy-MM-dd HH:mm"];
                    NSDate *targetDate = [NSDate dateWithTimeIntervalSince1970: timeInterval];
                    resultString = [formatter stringFromDate: targetDate];
                }
            }
        }
    }    
    return resultString;
}


+ (NSString *)stringFromDate:(NSDate *)date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; 
    
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息 +0000。
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    
    NSString *destDateString = [dateFormatter stringFromDate:date];
    
    return destDateString;
    
}


+ (NSDate *)dateFromString:(NSString *)dateString{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat: @"yyyy-MM-dd"]; 
    
    
    NSDate *destDate= [dateFormatter dateFromString:dateString];
    
    return destDate;
}
@end
