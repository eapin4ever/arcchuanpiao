//
//
//  Created by admin on 2/29/12.
//  Copyright (c) 2012 zhangshangtong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (QueueAdditions)

@end

@interface AppContext : NSObject {

}
@property (nonatomic,retain) NSMutableArray *myWorks;
@property (nonatomic,retain) NSMutableDictionary *userDic;

@property (nonatomic,retain) NSMutableDictionary *otherUserDic;
@property (nonatomic,retain) NSString *accountid;
@property (nonatomic) NSInteger commentNumber;
+ (AppContext *)sharedContext;
- (void)eventUpdateUserDic:(NSMutableDictionary *)dic;
- (void)eventUpdateOtherUserDic:(NSMutableDictionary *)dic;

- (void)eventUpdateMyWorks:(NSMutableArray *)arr;
-(void)removeData;
@end
