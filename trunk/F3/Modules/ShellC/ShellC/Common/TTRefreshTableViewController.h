//
//  TTRefreshTableViewController.h
//  TravelGuide
//
//  Created by Ma Jianglin on 12/27/12.
//  Copyright (c) 2012 Ma Jianglin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTModuleBaseViewController.h"
#import "EGORefreshTableHeaderView.h"
#import "MoreButton.h"

@interface TTRefreshTableViewController : ZSTModuleBaseViewController<UITableViewDataSource, UITableViewDelegate, EGORefreshTableHeaderDelegate>
{
	BOOL _loading;  //是否正在加载中
    
    int _pageIndex; //当前加载的页数，页面索引
    int _pageSize;  //每页数据条数
    
    BOOL _hasMore;  //是否有更多数据，是否长更多按钮
}


@property(nonatomic, strong) IBOutlet UITableView *tableView;   //表视图
@property(nonatomic, strong) NSMutableArray *dataArray;         //数据数组
@property(nonatomic, strong) UITableViewCell *moreCell;         //更多按钮所在的行

- (void)doneLoadingData;

@end;