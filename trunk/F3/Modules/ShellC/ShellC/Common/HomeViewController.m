//
//  HomeViewController.m
//  VoiceChina
//
//  Created by huijun xu on 13-2-17.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "HomeViewController.h"
#import "ZSTLeftViewController.h"
#import "ZSTGlobal+F3.h"
#import "ElementParser.h"
#import <PMRepairButton.h>

@interface HomeViewController ()

@end

@implementation HomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSString *)iconImageName
{
	return iconImageName;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *array = [self leftButtonElementsForView];
    
#ifdef DEBUG
    NSLog(@"------ %lu ------",(unsigned long)array.count);
#endif
    
    if (array && array.count > 0) {
        
        PMRepairButton *naviButton = [[PMRepairButton alloc] init];
        naviButton.frame = CGRectMake(10, 7, 40, 30);
        naviButton.imageEdgeInsets = UIEdgeInsetsMake(5, 10, 5, 10);
        [naviButton setImage:ZSTModuleImage(@"module_shellc_top_menu_n.png") forState:UIControlStateNormal];
        
        [naviButton setBackgroundImage:[[UIImage imageNamed:@"TKCommonLib.bundle/NaivagationBar/btn_navigationBaritemAction_normal.png"] stretchableImageWithLeftCapWidth:25 topCapHeight:0] forState:UIControlStateNormal];
        [naviButton setBackgroundImage:[[UIImage imageNamed:@"TKCommonLib.bundle/NaivagationBar/btn_navigationBaritemAction_pressed.png"] stretchableImageWithLeftCapWidth:25 topCapHeight:0] forState:UIControlStateHighlighted];
        
        [naviButton addTarget:self action:@selector(showLeft:) forControlEvents:UIControlEventTouchUpInside];
        
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:naviButton];
    }
}

- (IBAction)showLeft:(UIButton *)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}

- (IBAction)showRight:(UIButton *)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionRight animated:YES];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    LeftViewController *left = [[LeftViewController alloc] init];
    
    UINavigationController *leftNav = [[UINavigationController alloc] initWithRootViewController:left];
    leftNav.navigationBarHidden = YES;
    [self.revealSideViewController preloadViewController:leftNav forSide:PPRevealSideDirectionLeft];
    [[NSNotificationCenter defaultCenter] addObserver: left
                                             selector: @selector(pushViewController)
                                                 name: NotificationName_PushViewController
                                               object: nil];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"LaunchRemoteNotificationKey"]) {
        
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"LaunchRemoteNotificationKey"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_PushViewController object:nil];
    }
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(mineAction:) forControlEvents:UIControlEventTouchUpInside];
    
    btn.layer.cornerRadius = btn.frame.size.width / 2.0;
    btn.layer.borderColor = [UIColor clearColor].CGColor;
    btn.layer.masksToBounds = YES;
    
    NSString *path = nil;
    UIImage *image = nil;
    NSDictionary *dic = [ZSTF3Preferences shared].avaterName;
    
    if (![ZSTF3Preferences shared].loginMsisdn || [ZSTF3Preferences shared].loginMsisdn.length == 0) {
        
        [ZSTF3Preferences shared].loginMsisdn = @"";
    }
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0) {
        
        image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
    } else {
        
        if (dic || [dic isKindOfClass:[NSDictionary class]]) {
            path = [dic valueForKey:[ZSTF3Preferences shared].loginMsisdn];
        }
        
        if (path && path.length > 0) {
            
            NSData *reader = [NSData dataWithContentsOfFile:path];
            image = [UIImage imageWithData:reader];
            
            if (!image) {
                
                image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
            }
            
        } else {
            
            image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
        }
    }
    
    [btn setImage:image forState:UIControlStateNormal];
    
    //暂时解决btn图片不能显示的问题
    UIImageView *iv = [[UIImageView alloc] initWithFrame:btn.imageView.frame];
    iv.image = image;
    [btn insertSubview:iv aboveSubview:btn.imageView];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btn];
}


- (NSArray *)leftButtonElementsForView
{
    NSMutableArray *elements = [NSMutableArray array];
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module_shellc_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSArray *itemElements = [root selectElements:@"ImageButton Item"];
    for (NSInteger i = 0; i < [itemElements count]; i++) {
        Element *itemElement = [itemElements objectAtIndex:i];
        [elements addObject:itemElement];
    }
    return elements;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
