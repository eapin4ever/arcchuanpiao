//
//  MoreButton.h
//  QingTing2
//
//  Created by Eric on 11-12-9.
//  Copyright (c) 2011年 TOTEM.ME All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreButton : UIButton
{
    UIActivityIndicatorView *_indicator;
}

- (void)displayIndicator;
- (void)hideIndicator;

+ (id)button;

@end
