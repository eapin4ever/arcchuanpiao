//
//  ZSTShellCRootViewController.m
//  F3
//
//  Created by xuhuijun on 13-5-31.
//  Copyright (c) 2013年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTShellCRootViewController.h"
#import "ZSTModuleAddition.h"
#import "ZSTShell.h"
#import "ZSTUtils.h"
#import "ZSTModuleBaseViewController.h"
#import "ZSTGlobal+F3.h"
#import "ZSTMineViewController.h"
#import <PMRepairButton.h>

@interface ZSTShellCRootViewController ()

@end

@implementation ZSTShellCRootViewController


@synthesize tabbarController;
@synthesize selectedIndex;
@synthesize navigationController;
@synthesize rootView;

@synthesize application;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (IS_IOS_7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
    }
        
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(pushBMessageAction:)
                                                 name: NotificationName_PushBMessage
                                               object: nil];
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginDidCancel:) name:kNotificationLoginCancel object:nil];

       
    NSArray *array = [ZSTShell shellCViewControllerForTabBarController];
    [self initTabbarViewContorller:array];
}

- (void)loginDidCancel:(NSNotification*)notify
{
    [self reloadFirstControllerModulID:shellcHomeID interfaceUrl:nil type:self.moduleType];
}

- (void)initTabbarViewContorller:(NSArray *)controllers
{
    if (controllers) {
        
        if (self.tabbarController) {
            self.tabbarController = nil;
        }
        
        self.tabbarController = [[[BCTabBarController alloc] init] autorelease];
        self.tabbarController.viewControllers = [NSArray arrayWithArray:controllers];
        self.tabbarController.mDelegate = self;
        
        
        [self initWithRootViewController:self.tabbarController];
        self.tapInteractionsWhenOpened = PPRevealSideInteractionNone;
        [self setDirectionsToShowBounce:PPRevealSideDirectionNone];
        [self setPanInteractionsWhenClosed:PPRevealSideInteractionNone];
        [self setPanInteractionsWhenOpened:PPRevealSideInteractionNone];
    }
}

- (UIBarButtonItem *) initWithSubviews
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(mineAction:) forControlEvents:UIControlEventTouchUpInside];
    
    btn.layer.cornerRadius = btn.frame.size.width / 2.0;
    btn.layer.borderColor = [UIColor clearColor].CGColor;
    btn.layer.masksToBounds = YES;
    
    NSString *path = nil;
    UIImage *image = nil;
    NSDictionary *dic = [ZSTF3Preferences shared].avaterName;
    
    if (![ZSTF3Preferences shared].loginMsisdn || [ZSTF3Preferences shared].loginMsisdn.length == 0) {
        
        [ZSTF3Preferences shared].loginMsisdn = @"";
    }
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0) {
        
        image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
    } else {
        
        if (dic || [dic isKindOfClass:[NSDictionary class]]) {
            path = [dic safeObjectForKey:[ZSTF3Preferences shared].loginMsisdn];
        }
        
        if (path && path.length > 0) {
            
            NSString *homePath = NSHomeDirectory();
            NSString *imgTempPath = [path substringFromIndex:[path rangeOfString:@"tmp"].location];
            NSString *finalPath = [NSString stringWithFormat:@"%@/%@",homePath,imgTempPath];
            NSData *reader = [NSData dataWithContentsOfFile:finalPath];
            image = [UIImage imageWithData:reader];
            
            if (!image) {
                
                image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
            }
            
        } else {
            
            image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
        }
    }
    
    [btn setImage:image forState:UIControlStateNormal];
    
    //暂时解决btn图片不能显示的问题
    UIImageView *iv = [[UIImageView alloc] initWithFrame:btn.imageView.frame];
    iv.image = image;
    [btn insertSubview:iv aboveSubview:btn.imageView];
    
    return [[[UIBarButtonItem alloc] initWithCustomView:btn] autorelease];
}

- (void)mineAction:(id)sender
{
    ZSTMineViewController *controller = [[ZSTMineViewController alloc] initWithNibName:@"ZSTMineViewController" bundle:nil];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}


- (UIViewController *)rootViewController;
{
    return self;
}


- (void)pushBMessageAction:(NSNotification *)noti
{
    NSDictionary *dic = [noti object];
    self.tabbarController.selectedIndex = [[dic objectForKey:@"selectIndex"] intValue];
}


#pragma mark - BCTabBarControllerDelegate

- (void)tabbarController:(BCTabBarController*)controller didSelectTabAtIndex:(NSInteger)index
{
    
}

- (IBAction)showLeft:(UIButton *)sender
{
    [self.revealSideViewController pushOldViewControllerOnDirection:PPRevealSideDirectionLeft animated:YES];
}

- (void)reloadFirstControllerModulID:(NSInteger)modulID interfaceUrl:(NSString *)interfaceUrl type:(NSInteger)type
{
    ZSTF3Preferences *per = [ZSTF3Preferences shared];
    if (modulID == 22 || modulID == -1) {
         per.isInPush = YES;
    }else{
        per.isInPush = NO;
    }
    ZSTModuleBaseViewController *controller = (ZSTModuleBaseViewController *)[ZSTShell reloadShellCFirstControllerModulID:modulID interfaceUrl:interfaceUrl type:type];
    if (controller) {
        self.navigationController = [[[UINavigationController alloc] initWithRootViewController: controller] autorelease];
        self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed:@"Module.bundle/module_shellc_top_bg.png"];
        controller.navigationItem.titleView = [ZSTUtils shellClogoView];
        
//        UIButton *naviButton = [UIButton buttonWithType:UIButtonTypeCustom];
        PMRepairButton *naviButton = [[PMRepairButton alloc] init];
        naviButton.frame = CGRectMake(10, 7, 40, 30);
        naviButton.imageEdgeInsets = UIEdgeInsetsMake(5, 10, 5, 10);
        [naviButton setImage:ZSTModuleImage(@"module_shellc_top_menu_n.png") forState:UIControlStateNormal];
        [naviButton setBackgroundImage:[[UIImage imageNamed:@"TKCommonLib.bundle/NaivagationBar/btn_navigationBaritemAction_normal.png"] stretchableImageWithLeftCapWidth:25 topCapHeight:0] forState:UIControlStateNormal];
        [naviButton setBackgroundImage:[[UIImage imageNamed:@"TKCommonLib.bundle/NaivagationBar/btn_navigationBaritemAction_pressed.png"] stretchableImageWithLeftCapWidth:25 topCapHeight:0] forState:UIControlStateHighlighted];
        [naviButton addTarget:self action:@selector(showLeft:) forControlEvents:UIControlEventTouchUpInside];
        controller.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:naviButton] autorelease];
        controller.navigationItem.rightBarButtonItem = [self initWithSubviews];
        
        controller.iconImageName = @"Module.bundle/module_shellc_tabbar_mainicon3_n.png";
    }
    [self.tabbarController replaceControllerAtIndex:2 withController:self.navigationController];
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return (1 << UIInterfaceOrientationPortrait);
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    self.navigationController = nil;
    self.tabbarController = nil;
    [super dealloc];
}

@end
