//
//  ZSTShellCRootViewController.h
//  F3
//
//  Created by xuhuijun on 13-5-31.
//  Copyright (c) 2013年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "PPRevealSideViewController.h"
#import "ZSTModule.h"
#import "ZSTModuleDelegate.h"
#import "ZSTDao.h"

@interface ZSTShellCRootViewController : PPRevealSideViewController <ZSTModuleDelegate,BCTabBarControllerDelegate>

@property (nonatomic, retain) BCTabBarController *tabbarController;
@property (nonatomic, retain) UINavigationController *navigationController;
@property (nonatomic) NSInteger selectedIndex;

- (void)initTabbarViewContorller:(NSArray *)controllers;

- (void)reloadFirstControllerModulID:(NSInteger)modulID interfaceUrl:(NSString *)interfaceUrl type:(NSInteger)type;

@end
