//
//  ZSTF3Global+ShellC.h
//  ShellC
//
//  Created by LiZhenQu on 13-4-26.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

//////////////////////////////////////////////////////////////////////////////////////////

#define ShellCBaseURL @"http://mod.pmit.cn"

#define GetCoverList ShellCBaseURL  @"/ShellC/GetShellAD"

#define ArticleDBaseURL @"http://mod.pmit.cn/ArticleD"

#define GetSingleMessage ArticleDBaseURL @"/GetSingleMessage"
