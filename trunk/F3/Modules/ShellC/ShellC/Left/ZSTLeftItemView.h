//
//  LeftItemView.h
//  VoiceChina
//
//  Created by LiZhenQu on 13-2-20.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PMRepairButton.h>

@interface ZSTLeftItemView : UIView

//@property (strong ,nonatomic) IBOutlet UIButton *btn;
@property (strong,nonatomic) PMRepairButton *btn;
//@property (nonatomic, strong) IBOutlet UIButton* actionButton;
@property (strong,nonatomic) PMRepairButton *actionButton;
@property (strong ,nonatomic) IBOutlet UIImageView *countImage;
@property (strong ,nonatomic) IBOutlet UILabel *countLabel;
@property (strong, nonatomic) UILabel *titleLabel;

- (void) initWithControls;

@end
