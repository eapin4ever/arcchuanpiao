//
//  LeftItemView.m
//  VoiceChina
//
//  Created by LiZhenQu on 13-2-20.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTLeftItemView.h"

@implementation ZSTLeftItemView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setTag:(NSInteger)tag
{
    [super setTag:tag];
    self.actionButton.tag = tag;
    self.btn.tag = tag;
}

- (void) initWithControls
{
//    self.btn = [[UIImageView alloc] initWithFrame:CGRectMake(12, 2, 52, 50)];
    
//    self.btn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btn = [[PMRepairButton alloc] init];
    self.btn.frame = CGRectMake(12, 2, 52, 50);
//    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton = [[PMRepairButton alloc] init];
    self.actionButton.frame = CGRectMake(0, 2, 76, 56);
    [self.actionButton setImage:ZSTModuleImage(@"module_shellc_left_h.png")forState:UIControlStateSelected];
    [self addSubview:self.actionButton];
    [self addSubview:self.btn];
    
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 57, 75, 20)];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:self.titleLabel];
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

@end
