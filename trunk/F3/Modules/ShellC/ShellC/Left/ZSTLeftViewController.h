//
//  LeftViewController.h
//  VoiceChina
//
//  Created by LiZhenQu on 13-2-17.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTModuleBaseViewController.h"

@class ZSTLeftItemView;

@interface LeftViewController : UIViewController
{
     NSArray *leftArray;
}

@property (nonatomic, strong) IBOutlet ZSTLeftItemView* leftItemHome;
@property (nonatomic, strong) IBOutlet ZSTLeftItemView *leftItemNews;
@property (nonatomic, strong) IBOutlet ZSTLeftItemView *leftItemVideo;
@property (nonatomic, strong) IBOutlet ZSTLeftItemView *leftItemMusic;
@property (nonatomic, strong) IBOutlet ZSTLeftItemView *leftItemStudent;
@property (nonatomic, strong) IBOutlet ZSTLeftItemView* leftItemSetting;
@property (nonatomic) NSInteger selectedIndex;

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) ZSTLeftItemView *leftItemView;

@property (nonatomic, retain) NSArray *leftArray;

@end
