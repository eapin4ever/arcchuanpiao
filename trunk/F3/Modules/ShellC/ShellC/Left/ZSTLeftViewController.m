//
//  LeftViewController.m
//  VoiceChina
//
//  Created by LiZhenQu on 13-2-17.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTLeftViewController.h"
#import "ZSTLeftItemView.h"
#import "ZSTShellCRootViewController.h"
#import "ElementParser.h"
#import "ZSTF3ClientAppDelegate.h"
#import "ZSTUtils.h"
#import "ZSTShellCRootViewController.h"


#define lItecmSelected @"lItecmSelected"

#define lLeftItemNewsTag     0
#define lLeftItemVideoTag    1
#define lLeftItemMusicTag    2
#define lLeftItemStudentTag  3

#define SharedAppDelegate ((ZSTF3ClientAppDelegate*)[[UIApplication sharedApplication] delegate])

@interface LeftViewController ()


@end

@implementation LeftViewController
@synthesize leftArray;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (IS_IOS_7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    UIImageView *bgImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0+(IS_IOS_7?20:0), 320, 480+(iPhone5?88:0))];
    bgImg.image = [UIImage imageNamed:@"Module.bundle/module_shellc_left_bg.png"];
    [self.view addSubview:bgImg];
    
    leftArray = [[NSArray alloc] init];
    leftArray = [self leftButtonElementsForView];
        
    [self initWithSubviews];
}

- (void) initWithSubviews
{
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0+(IS_IOS_7?20:0), 100, 480+(iPhone5?88:0)-(IS_IOS_7?20:0))];
    self.scrollView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.scrollView];
    
    float marginVertical = 80;
    float width = 80;
    
    for (int i = 0;i < leftArray.count + 1; i ++) {
        
        int moduleID = 0;
        NSString *title = nil;
        NSString *titleColor = nil;
        NSString *imgpath = @"module_shellc_left_icon0.png";
        NSString *imgpath_h = @"module_shellc_left_icon0_p.png";
        if (i == 0) {
        
            moduleID = -4;
            title = NSLocalizedString(@"首页", nil);
            
            if ([leftArray count]) {
                titleColor = [[[leftArray objectAtIndex:0] selectElement:@"TitleColor"] contentsText];
            }

        } else {
        
            moduleID = [[[[leftArray objectAtIndex:i-1] selectElement:@"ModuleId"] contentsNumber] intValue];
            title = [[[leftArray objectAtIndex:i-1] selectElement:@"Title"] contentsText];
            imgpath = [NSString stringWithFormat:@"module_shellc_left_icon%d_n.png", i];
            imgpath_h = [NSString stringWithFormat:@"module_shellc_left_icon%d_p.png",i];            
            titleColor = [[[leftArray objectAtIndex:i-1] selectElement:@"TitleColor"] contentsText];
        }
        
        if (titleColor == nil || [titleColor length] == 0 || [titleColor isEqualToString:@""]) {
            titleColor = @"#FFFAFA";
        }
        self.leftItemView = [[ZSTLeftItemView alloc] init];
        [self.leftItemView initWithControls];
        self.leftItemView.frame = CGRectMake(0, marginVertical * i, width, width);
        self.leftItemView.countLabel.hidden = YES;
        self.leftItemView.countImage.hidden = YES;
        self.leftItemView.tag = i;
        self.leftItemView.titleLabel.text = title;
        self.leftItemView.titleLabel.textColor = [ZSTUtils colorFromHexColor:titleColor];

        
        [self.leftItemView.btn setImage:ZSTModuleImage(imgpath) forState:UIControlStateNormal];
        [self.leftItemView.btn setImage:ZSTModuleImage(imgpath_h) forState:UIControlStateHighlighted];
        [self.leftItemView.actionButton addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.leftItemView.btn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.scrollView addSubview:self.leftItemView];
    }
    
    self.scrollView.contentSize = CGSizeMake(100, 84*(leftArray.count+1));
    self.selectedIndex = 0;
}

- (void)setSelectedIndex:(NSInteger)selectedIndex {
    _selectedIndex = selectedIndex;
    for (UIView* subView in self.scrollView.subviews) {
        if ([subView isKindOfClass:[ZSTLeftItemView class]]) {
            ZSTLeftItemView* view = (ZSTLeftItemView*)subView;
            view.actionButton.selected = (view.tag == selectedIndex);
        }
    }
}

- (void)selectAction:(UIButton*)sender
{
    self.selectedIndex = sender.tag;
    ((ZSTShellCRootViewController *)(SharedAppDelegate.rootController)).selectedIndex = sender.tag;
}

- (NSArray *)leftButtonElementsForView
{
    NSMutableArray *elements = [NSMutableArray array];
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module_shellc_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSArray *itemElements = [root selectElements:@"ImageButton Item"];
    for (NSInteger i = 0; i < [itemElements count]; i++) {
        Element *itemElement = [itemElements objectAtIndex:i];
        [elements addObject:itemElement];
    }
    return elements;
}


- (void)pushViewController
{
    NSArray *array = [self leftButtonElementsForView];
    for (int i = 0; i < [array count];i ++) {
        Element *element = [array objectAtIndex:i];
        int moduleId = [[[element selectElement:@"ModuleId"] contentsNumber] intValue];

        if (moduleId == -1 || moduleId == 22) {
            
          int type = [[[element selectElement:@"ModuleType"] contentsNumber] intValue];
            
            [((ZSTShellCRootViewController *)(SharedAppDelegate.rootController)) reloadFirstControllerModulID:moduleId interfaceUrl:nil type:type];
            self.selectedIndex = i+1;
        }
    }
}

- (void) clickAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    [self selectAction:sender];
    [self.revealSideViewController popViewControllerAnimated:YES completion:^{
        NSInteger moduleId;
        Element *element;
        NSInteger type = 0;
        NSString *interfaceUrl = nil;
        if (button.tag == 0) {
            moduleId = -4;
        }else{
            
            NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module_shellc_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
            ElementParser *parser = [[ElementParser alloc] init];
            DocumentRoot *root = [parser parseXML:content];
            
            NSArray *itemElements = [root selectElements:@"ImageButton Item"];
            element = [itemElements objectAtIndex:button.tag - 1];
            
            moduleId = [[[element selectElement:@"ModuleId"] contentsNumber] integerValue];
            type = [[[element selectElement:@"ModuleType"] contentsNumber] integerValue];
            
            if (moduleId == 12 || moduleId == 24) {
               interfaceUrl = [[element selectElement:@"InterfaceUrl"] contentsText];
            }
        }
        [((ZSTShellCRootViewController *)(SharedAppDelegate.rootController)) reloadFirstControllerModulID:moduleId interfaceUrl:interfaceUrl type:type];
    }];
}

#pragma mark - Actions

- (void)performPop
{
    [self performSelector:@selector(popController) withObject:nil afterDelay:.1f];
}

- (void)popController
{
    [self.revealSideViewController popViewControllerAnimated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
