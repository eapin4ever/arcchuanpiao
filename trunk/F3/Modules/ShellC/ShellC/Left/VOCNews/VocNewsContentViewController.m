//
//  NewsContentViewController.m
//  F3
//
//  Created by admin on 12-7-19.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "VocNewsContentViewController.h"
#import "ZSTARTICLEALocalSubstitutionCache.h"
#import "ZSTARTICLEATKResourceURLProtocol.h"
#import "ZSTARTICLEATKResourceURL.h"
#import "ZSTCommentListViewController.h"
#import "NSStringAdditions.h"
#import "GTMBase64.h"
#import "ZoneViewController.h"



#define ArticleFontSize   @"ArticleFontSize"
#define LargeFont   @"3"
#define MiddleFont  @"2"
#define NormalFont  @"1"
#define SmallFont   @"0"
#define RGBCOLOR(r,g,b) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:1]

@interface VocNewsContentViewController ()

@end

@implementation VocNewsContentViewController

@synthesize selectedImageView;

//@synthesize webView;

//@synthesize textView;

@synthesize contentVo;


- (void)dealloc
{
    self.selectedImageView = nil;
    [super dealloc];
}

- (void)refreshTap:(UIControl *)sender
{
    UIWebView *webView = (UIWebView *)[self.view viewWithTag:99];
    UILabel *refreshLabel = (UILabel *)[webView viewWithTag:102];
    UIControl *refreshCtr = (UIControl *)[webView viewWithTag:103];
    refreshCtr.hidden = YES;
    refreshLabel.hidden = YES;
    
    [self showLoadingViewWithMessage:@"加载中..."];
    [self.client getNewsDetailWithDelegete:self
                            AccountId:@"0"
                                msgId:self.msgId
                             userInfo:nil
                               verson:0];
}


#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [ZSTARTICLEATKResourceURLProtocol registerProtocol];
    
    self.view.backgroundColor = RGBCOLOR(239, 239, 239);
    
    UIWebView *webView = [[[UIWebView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.height-44)] autorelease];
    webView.tag = 99;
    webView.backgroundColor = RGBCOLOR(239, 239, 239);
    webView.dataDetectorTypes = UIDataDetectorTypeNone;
    webView.opaque = NO;
    webView.delegate = self;
    //去掉阴影
    for(UIView* subView in [webView subviews])
    {
        if([subView isKindOfClass:[UIScrollView class]]){
            for(UIView* shadowView in [subView subviews])
            {
                if([shadowView isKindOfClass:[UIImageView class]]){
                    [shadowView setHidden:YES];
                }
            }
        }
    }
    [self.view addSubview:webView];
    UILabel *refreshLabel = [[UILabel alloc] initWithFrame:CGRectMake(60, 170, 200, 30)];
    refreshLabel.font = [UIFont boldSystemFontOfSize:18];
    refreshLabel.text = @"加载失败，点击重新加载";
    refreshLabel.textAlignment = UITextAlignmentCenter;
    refreshLabel.textColor = RGBCOLOR(200, 200, 200);
    refreshLabel.backgroundColor = [UIColor clearColor];
    refreshLabel.tag = 102;
    refreshLabel.hidden = YES;
    [webView addSubview:refreshLabel];
    [refreshLabel release];
    
    UIControl *refreshCtr = [[UIControl alloc] initWithFrame:self.view.frame];
    [refreshCtr addTarget:self action:@selector(refreshTap:) forControlEvents:UIControlEventTouchUpInside];
    refreshCtr.tag = 103;
    refreshCtr.hidden = YES;
    [webView addSubview:refreshCtr];
    [refreshCtr release];
 
    [self initFontSize];
    
    [self initNavgationView];
    
    [self showLoadingViewWithMessage:@"加载中..."];
    [self.client getNewsDetailWithDelegete:self
                                 AccountId:@"0"
                                     msgId:self.msgId
                                  userInfo:nil
                                    verson:0];
}

- (void)initNavgationView
{
    UIView *navView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    navView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:navView];
    UIImageView *navIamge = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    navIamge.image = [UIImage imageNamed:@"navi_bg.png"];
    [navView addSubview:navIamge];
    
    UIButton *backBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    backBtn.frame = CGRectMake(10, 7, 46, 30);
    [backBtn setImage:[UIImage imageNamed:@"back_arrow.png"] forState:UIControlStateNormal];
    [backBtn setImage:[UIImage imageNamed:@"back_arrow.png"] forState:UIControlStateHighlighted];
    [backBtn setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateNormal];
    [backBtn setBackgroundImage:[UIImage imageNamed:@"back_btn.png"] forState:UIControlStateHighlighted];
    [backBtn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [navView addSubview:backBtn];
}

- (void)initFontSize
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *fontSizeStr = [ud objectForKey:ArticleFontSize];
    
    if (fontSizeStr == nil) {
        fontSize = VocNewsFontSize_Normal;
    } else {
        fontSize = [fontSizeStr intValue];
        if (fontSize < VocNewsFontSize_Normal || fontSize > VocNewsFontSize_Large) {
            fontSize = VocNewsFontSize_Normal;
        }
    }
    
    if (fontSize == VocNewsFontSize_Small) {
        UIButton *btn = (UIButton *)[self.view viewWithTag:300];
        btn.enabled = YES;
        
        btn = (UIButton *)[self.view viewWithTag:303];
        btn.enabled = NO;
        [btn setImage:[UIImage imageNamed:@"module_ArticleA_font_zoomOut_d.png"] forState:UIControlStateNormal];
    }
    
    if (fontSize == VocNewsFontSize_Large) {
        UIButton *btn = (UIButton *)[self.view viewWithTag:300];
        btn.enabled = NO;
        [btn setImage:[UIImage imageNamed:@"module_ArticleA_font_zoomIn_d.png"] forState:UIControlStateNormal];
        
        btn = (UIButton *)[self.view viewWithTag:303];
        btn.enabled = YES;
    }
}

- (void)fontZoomIn
{
    int size = fontSize + 1;
    fontSize = size>VocNewsFontSize_Large ? VocNewsFontSize_Large : size;
    
    if (fontSize == VocNewsFontSize_Large) {
        UIButton *btn = (UIButton *)[self.view viewWithTag:300];
        btn.enabled = NO;
        [btn setImage:[UIImage imageNamed:@"module_ArticleA_font_zoomIn_d.png"] forState:UIControlStateNormal];
    }
    
    
    UIButton *btn = (UIButton *)[self.view viewWithTag:303];
    if (!btn.enabled) {
        btn.enabled = YES;
        [btn setImage:[UIImage imageNamed:@"module_ArticleA_font_zoomOut.png"] forState:UIControlStateNormal];
    }


    [self changeFontSize];
}

- (void)fontZoomOut
{
    int size = fontSize - 1;
    fontSize = size<0 ? VocNewsFontSize_Small : size;
    
    if (fontSize == VocNewsFontSize_Small) {
        UIButton *btn = (UIButton *)[self.view viewWithTag:303];
        btn.enabled = NO;
        [btn setImage:[UIImage imageNamed:@"module_ArticleA_font_zoomOut_d.png"] forState:UIControlStateNormal];
    }
    
    UIButton *btn = (UIButton *)[self.view viewWithTag:300];
    if (!btn.enabled) {
        btn.enabled = YES;
        [btn setImage:[UIImage imageNamed:@"module_ArticleA_font_zoomIn.png"] forState:UIControlStateNormal];
    }

    [self changeFontSize];
}

- (void)changeFontSize
{
    UIWebView *webView = (UIWebView *)[self.view viewWithTag:99];
    
    NSString *fontSizeStr = @"1";
    if (fontSize == VocNewsFontSize_Large) {
        [webView stringByEvaluatingJavaScriptFromString: @"changeFontSize('font_large');" ];
        fontSizeStr = LargeFont;
    } else if (fontSize == VocNewsFontSize_Middle) {
        [webView stringByEvaluatingJavaScriptFromString: @"changeFontSize('font_middle');" ];
        fontSizeStr = MiddleFont;
    } else if (fontSize == VocNewsFontSize_Normal) {
        [webView stringByEvaluatingJavaScriptFromString: @"changeFontSize('font_normal');" ];
        fontSizeStr = NormalFont;
    } else {
        [webView stringByEvaluatingJavaScriptFromString: @"changeFontSize('font_small');" ];
        fontSizeStr = SmallFont;
    }
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setObject:fontSizeStr forKey:ArticleFontSize];
    [ud synchronize];
}

- (void)openCommentListAction
{

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



#pragma mark ---
#pragma UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    if ([request.URL.scheme isEqualToString:@"image"]) {
        
        NSString *url = [request.URL description];
        NSRange range1 = [url rangeOfString:@"///"];
        NSRange range2 = [url rangeOfString:@"///" options:NSCaseInsensitiveSearch range:NSMakeRange(range1.location + 3, url.length - range1.location - 3)];
        
        NSString *offsetY = [request.URL.description substringWithRange:NSMakeRange(range1.location+range1.length, range2.location - range1.location-range1.length)];
        
        NSString *imageUrl = [request.URL.description substringFromIndex:range2.location+range2.length];        
        if (imageUrl!=nil && [imageUrl length]>0) {
            ZSTARTICLEATKResourceURL *resourceURL = [ZSTARTICLEATKResourceURL URLWithString:imageUrl];
            NSString *cachePath = [ZSTARTICLEATKResourceURLProtocol cachePathForResourceURL:resourceURL];
            UIImage *image = [UIImage imageWithContentsOfFile:cachePath];
            if (image) {
                
                self.selectedImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(217, [offsetY intValue], 90, 90)] autorelease];
                self.selectedImageView.contentMode = UIViewContentModeScaleAspectFill;
                self.selectedImageView.clipsToBounds = YES;
                self.selectedImageView.backgroundColor = RGBCOLOR(239, 239, 239);
                
//                self.selectedImageView.defaultImage = image;
//                [self.selectedImageView clear];
//                self.selectedImageView.url = [NSURL URLWithString:resourceURL.originalUrl];
//                [self.selectedImageView loadImage];
                [self.selectedImageView setImageWithURL:[NSURL URLWithString:resourceURL.originalUrl]];
                [self.view addSubview:selectedImageView];
                
                
//                selectedImageView.zoomedSize = [self displayRectForImage:CGSizeMake(self.selectedImageView.image.size.width, self.selectedImageView.image.size.height)];
//
//                selectedImageView.imageView.wrapInScrollviewWhenZoomed = YES;
//                selectedImageView.showLoadingWheel = YES;
//                
//                [selectedImageView.imageView zoomIn];
//                selectedImageView.imageView.zoomDelegate = self;
//                selectedImageView.callbackOnSetImage = self;
            }
            return NO;
        }
    } else if((navigationType == UIWebViewNavigationTypeLinkClicked || navigationType == UIWebViewNavigationTypeFormSubmitted) && [request.URL.scheme caseInsensitiveCompare:@"http"] == NSOrderedSame) {
        
        [[UIApplication sharedApplication] openURL:request.URL];
        return NO;
    } else if (navigationType == UIWebViewNavigationTypeLinkClicked && [request.URL.scheme caseInsensitiveCompare:@"native"] == NSOrderedSame) {
        
        NSDictionary *params = [request.URL.absoluteString queryDictionaryUsingEncoding:NSUTF8StringEncoding];
        NSString *module_id = [params objectForKey:@"native://?module_id"];
        NSString *module_type = [params objectForKey:@"module_type"];
        NSString *title = [params objectForKey:@"title"];
        
        NSMutableDictionary *moduleParams = [NSMutableDictionary dictionary];
        [moduleParams setSafeObject:module_type forKey:@"type"];
            
    } else if ([[request.URL host] isEqualToString:@"contestworks"]) {
    }
    return YES;
}

- (void)zoomWindow:(MTZoomWindow *)zoomWindow didZoomOutView:(UIView *)view {
    self.selectedImageView.alpha = 0;
    [self.selectedImageView removeFromSuperview];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    TKDERROR(@"error == %@", error);
}

- (void)webViewDidStartLoad:(UIWebView *)webView //网页加载时调用
{
    //    [aiView startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView //网页完成加载时调用
{
//    [webView stringByEvaluatingJavaScriptFromString: @"changeFontSize('font_large');" ];
}
- (CGSize)displayRectForImage:(CGSize)imageSize {
    CGRect screenRect = PPScreenBounds();
    
    if (imageSize.width > screenRect.size.width) {
        CGFloat height = imageSize.height * screenRect.size.width / imageSize.width;
        CGFloat width = screenRect.size.width;
        if (height > screenRect.size.height) {
            width = width * screenRect.size.height/height;
            height = screenRect.size.height;
        }
        return CGSizeMake(width, height);
    }else if (imageSize.height > screenRect.size.height) {
        CGFloat width = imageSize.width * screenRect.size.height / imageSize.height;
        CGFloat height = screenRect.size.height;
        
        return CGSizeMake(width, height);
    } else {
        return CGSizeMake(imageSize.width, imageSize.height); 
    }
}


- (void)viewDidUnload
{
    //    [webView removeFromSuperview];
    [super viewDidUnload];
    //    [aiView removeFromSuperview];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)templateEngine:(MGTemplateEngine *)engine blockStarted:(NSDictionary *)blockInfo
{
    //	TTLog(@"Started block %@", [blockInfo objectForKey:BLOCK_NAME_KEY]);
}

- (void)templateEngine:(MGTemplateEngine *)engine blockEnded:(NSDictionary *)blockInfo
{
    //	TTLog(@"Ended block %@", [blockInfo objectForKey:BLOCK_NAME_KEY]);
}

- (void)templateEngineFinishedProcessingTemplate:(MGTemplateEngine *)engine
{
    //	TTLog(@"Finished processing template.");
}

- (void)templateEngine:(MGTemplateEngine *)engine encounteredError:(NSError *)error isContinuing:(BOOL)continuing;
{
	TTLog(@"Template error: %@", error);    
}

- (void)getNewsDetailDidSucceed:(NewsDetailInfo *)detail userInfo:(id)userInfo
{
    [self hideLoadingView];
    UIWebView *webView = (UIWebView *)[self.view viewWithTag:99];
    UILabel *refreshLabel = (UILabel *)[webView viewWithTag:102];
    refreshLabel.hidden = YES;
    
    UIControl *refreshCtr = (UIControl *)[webView viewWithTag:103];
    refreshCtr.hidden = YES;
    
    NewsDetailInfo *contentVO = detail;
    self.contentVo = contentVO;
    
    //目前只做图片
    NSMutableString *urlStr = [NSMutableString string];
    if ([contentVO.contents isKindOfClass:[NSArray class]] && [contentVO.contents count]>0) {
        for (int i=0; i<[contentVO.contents count]; i++) {
            NSString *str = [[contentVO.contents objectAtIndex:i] objectForKey:@"letter"];
            NSString *formatContent = [self formatArticleContent:str];  
            [urlStr appendString:[NSString stringWithFormat:@"<p>%@</p>",formatContent]];
            
            NSDictionary *fileDic = [contentVO.contents objectAtIndex:i];
            NSString *url = [fileDic objectForKey:@"imageurl"];
            if (url && [url length]>0) {
                
                NSString *origUrl = @"";
                ZSTARTICLEATKResourceURL *resourceURL = [ZSTARTICLEATKResourceURL resourceURLWithResource:url originalUrl:origUrl MIMEType:@"image/png"];            
                [urlStr appendFormat:@"<span class=\"photo_block\"><span class=\"photo_box\"><img class=\"photo\" src=\"%@\" onload=\"javascript:DrawImage(this)\"/></span>", resourceURL.absoluteString?resourceURL.absoluteString:@""];
                NSString *desc = [fileDic objectForKey:@"imagedesc"];
                if (desc) {
                    [urlStr appendFormat:@"<span class=\"photo_title\">%@</span></span>", desc];
                }
            }
        }
 	}
    
    // Set up template engine with your chosen matcher.
    MGTemplateEngine *engine = [MGTemplateEngine templateEngine];
    [engine setDelegate:self];
    [engine setMatcher:[ICUTemplateMatcher matcherWithTemplateEngine:engine]];    
    
     NSString *templatePath = [[NSBundle mainBundle] pathForResource:@"module_ArticleA_content_template" ofType:@"html"];
    NSDictionary *variables = [NSDictionary dictionaryWithObjectsAndKeys:contentVO.title,@"title", contentVO.addtime,@"date" ,urlStr ,@"body", [self getFontSize], @"fontClass", @"", @"theme", nil];
    
    
    NSString *result = [engine processTemplateInFileAtPath:templatePath withVariables:variables];
    
    //不用生成html文件，直接加载字符串
    NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]];
    [webView loadHTMLString:result baseURL:url];  
}

- (NSString *)formatArticleContent:(NSString *)content
{
    if (content == nil) {
        return nil;
    }
    
    NSMutableString *contentStr = [NSMutableString string];
    [contentStr appendString:content];
    
    [contentStr replaceOccurrencesOfString:@"\r" withString:@"" options:NSCaseInsensitiveSearch range:NSMakeRange(0, contentStr.length)];
    [contentStr replaceOccurrencesOfString:@"\n" withString:@"<br />" options:NSCaseInsensitiveSearch range:NSMakeRange(0, contentStr.length)];
    return contentStr;
}

- (NSString *)getFontSize
{
    if (fontSize == VocNewsFontSize_Normal) {
        return @"font_normal";
    } else if (fontSize == VocNewsFontSize_Middle) {
        return @"font_middle";
    } else if (fontSize == VocNewsFontSize_Large) {
        return @"font_large";
    } else {
        return @"font_small";
    }
}

- (void)getNewsDetailDidFailed:(NSDictionary *)dic userInfo:(id)userInfo
{
    [self hideLoadingView];
    
    UIWebView *webView = (UIWebView *)[self.view viewWithTag:99];
    UILabel *refreshLabel = (UILabel *)[webView viewWithTag:102];
    refreshLabel.hidden = NO;
    
    UIControl *refreshCtr = (UIControl *)[webView viewWithTag:103];
    refreshCtr.hidden = NO;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    //放大 分享 收藏 缩小 按钮的tag值 300 301 302 303
    for (int i=300; i<304; i++) {
        UIButton *btn = (UIButton *)[self.view viewWithTag:i];
        btn.enabled = NO;
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    //放大 分享 收藏 缩小 按钮的tag值 300 301 302 303
    for (int i=300; i<304; i++) {
        UIButton *btn = (UIButton *)[self.view viewWithTag:i];
        btn.enabled = YES;
    }
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [super touchesBegan:touches withEvent:event];
//
//}

@end
