//
//  VOCNewsViewController.m
//  VoiceChina
//
//  Created by ZhangChuntao on 3/26/13.
//  Copyright (c) 2013 zhangshangtong. All rights reserved.
//

#import "VOCNewsViewController.h"
#import "DataModel.h"
#import "ZSTArticleAContentViewController.h"
#import "CacheUtil.h"
#import "BaiduMobStat.h"

#import "NSStringAdditions.h"
#import "GTMBase64.h"
#import "PlayViewController.h"
#import "VideoPlayerController.h"
#import "ZSTCommentListViewController.h"
#import "UserHomePageViewController.h"
#import "VocNewsContentViewController.h"
#import "BrowserViewController.h"

@interface VOCNewsViewController ()

@end

@implementation VOCNewsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - NewsCarouselViewDataSource

- (int)numberOfViewsInCarouselView:(NewsCarouselView *)newsCarouselView
{
    return [self.carouselArray count];
}

- (NSString*)carouseView:(NewsCarouselView*)carouselView titleForViewAtIndex:(int)index
{
    if ([self.carouselArray count] != 0) {
        HomeCoverInfo* info = [self.carouselArray objectAtIndex:index];
        return info.description;
    }
    return nil;
}

- (NSString*)carouseView:(NewsCarouselView *)carouselView picURLForViewAtIndex:(int)index
{
    if ([self.carouselArray count] != 0) {
        HomeCoverInfo* info = [self.carouselArray objectAtIndex:index];
        return info.picURL;
    }
    return nil;
}

- (NSString*)carouseView:(NewsCarouselView *)carouselView headerForViewAtIndex:(int)index
{
    if ([self.carouselArray count] != 0) {
        HomeCoverInfo* info = [self.carouselArray objectAtIndex:index];
        return info.title;
    }
    return nil;
}

#pragma mark - NewsCarouselViewDelegate

- (void)carouselView:(NewsCarouselView *)carouselView didSelectedViewAtIndex:(int)index
{

    HomeCoverInfo *info = [self.carouselArray objectAtIndex:index];
    NSURL* url = [NSURL URLWithString:info.linkURL];
    NSString* host = url.host;
    TTLog(@"host:%@", host);
    if ([host isEqualToString:@"online_page"]) {
        NSDictionary* params = [url.query queryDictionaryUsingEncoding:NSUTF8StringEncoding];
        NSMutableString* baseUrl = [params valueForKey:@"vocurl"];
        NSString* content = [params valueForKey:@"content"];
        NSData* contentData = [GTMBase64 decodeString:content];
        NSString* temp = [[NSString alloc] initWithData:contentData encoding:NSUTF8StringEncoding];
        [baseUrl appendFormat:@"?%@", temp];
//        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:baseUrl]];
        BrowserViewController *viewController = [[BrowserViewController alloc] init];
        viewController.targetUrl = baseUrl;
        viewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:viewController animated:YES];
        return;

    }
    else {
        NSData* data = [GTMBase64 decodeString:url.query];
        NSString* content = [[NSString  alloc] initWithData:data encoding:NSUTF8StringEncoding];
        NSDictionary* param = [content queryDictionaryUsingEncoding:NSUTF8StringEncoding];
        NSString* obj = [param valueForKey:@"obj"];
        if ([host isEqualToString:@"worksplay"]) {
            NSString* uid = [param valueForKey:@"uid"];
            PlayViewController *viewController = [[PlayViewController alloc] initWithNibName:@"PlayViewController" bundle:nil];
            viewController.worksId = obj;
            viewController.worksAccountId = uid;
            viewController.isSelf = NO;
            viewController.accountId = [Preferences shared].accountId;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if ([host isEqualToString:@"comment"]) {
            ZSTCommentListViewController *comment = [[ZSTCommentListViewController alloc] init];
            comment.hidesBottomBarWhenPushed = YES;
            comment.worksid = obj;
            [self.navigationController pushViewController:comment animated:YES];
        }
        else if ([host isEqualToString:@"newsdetail"]) {
            VocNewsContentViewController *viewController = [[VocNewsContentViewController alloc] init];
            viewController.hidesBottomBarWhenPushed = YES;
            viewController.msgId = [obj integerValue];
            [self.navigationController pushViewController:viewController animated:YES];
        }
        else if ([host isEqualToString:@"videoplay"]) {
            VideoPlayerController *videoView = [[VideoPlayerController alloc] init];
            //fix me
            videoView.playUrl = nil;
            [self presentModalViewController:videoView animated:YES];
        }
        else if ([host isEqualToString:@"contestworks"]) {
//            [SharedAppDelegate application:[UIApplication sharedApplication] openURL:url sourceApplication:nil annotation:nil];
            BrowserViewController *viewController = [[BrowserViewController alloc] init];
            viewController.targetUrl = info.linkURL;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
            return;
        }
        else if ([host isEqualToString:@"userinfo"]) {
            UserHomePageViewController* controller = [[UserHomePageViewController alloc] init];
            controller.accountid = obj;
            controller.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.carouselView.carouselDataSource = self;
    self.carouselView.carouselDelegate = self;
    [self readCache];
    [self showLoadingViewWithMessage:@"加载中..."];
    [self.client getHomeCoverWithDelegate:self type:2];
    [[BaiduMobStat defaultStat] pageviewStartWithName:@"VOCNewsViewController"];
    // Do any additional setup after loading the view from its nib.
}

- (void) showGuideView
{
    self.guideView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
    self.guideView.backgroundColor = [UIColor blackColor];
    self.guideView.alpha = 0.7f;
    
    UIImageView *guideImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 131)];
    guideImgView.image = [UIImage imageNamed:@"home_guide.png"];
    [self.guideView addSubview:guideImgView];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(removeGuideView)];
    [self.guideView addGestureRecognizer:singleTap];
    
    [[UIApplication sharedApplication].keyWindow addSubview:self.guideView];
    
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:FIRST_LAUNCH_APP_HOME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)readCache
{
    id carouselInfo = [CacheUtil readCacheOfType:CACHE_HOME_NEWS_INFO];
    id carousels = [HomeCoverInfo coverListWithDic:carouselInfo];
    if (![carousels isKindOfClass:[NSArray class]]) {
        carousels = [NSArray array];
    }
    if ([(NSArray *)carousels count] > 0) {
        self.carouselArray = [NSMutableArray arrayWithArray:carousels];
    }
    [self.carouselView reloadData];
}

- (void)getHomeCoverSucceed:(id)array
{
    [self hideLoadingView];
    if (array) {
        self.carouselArray = [NSMutableArray arrayWithArray:array];
        [self.carouselView reloadData];
    }
}

- (void)getHomeCoverFailed:(NSString *)response
{
    [self hideLoadingView];
    if (!response) {
        response = @"获取广告失败！";
    }
    [self showToast:response];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
