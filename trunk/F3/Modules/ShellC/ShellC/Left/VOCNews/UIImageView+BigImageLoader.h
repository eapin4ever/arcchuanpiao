//
//  UIImageView+BigImageLoader.h
//  VoiceChina
//
//  Created by wangguangzhao on 13-4-15.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SDWebImageCompat.h"
#import "SDWebImageManagerDelegate.h"
#import "SDWebImageManager.h"
#import "SDImageCache.h"

@interface UIImageView (BigImageLoader) <SDWebImageManagerDelegate>

/**
 * Set the imageView `image` with an `url` and a placeholder.
 *
 * The downloand is asynchronous and cached.
 *
 * @param url The url for the image.
 * @param placeholder The image to be set initially, until the image request finishes.
 * @see setImageWithURL:placeholderImage:options:
 */
- (void)setBigImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder;

/**
 * Set the imageView `image` with an `url`, placeholder and custom options.
 *
 * The downloand is asynchronous and cached.
 *
 * @param url The url for the image.
 * @param placeholder The image to be set initially, until the image request finishes.
 * @param options The options to use when downloading the image. @see SDWebImageOptions for the possible values.
 */
- (void)setBigImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder options:(SDWebImageOptions)options;

/**
 * Cancel the current download
 */
- (void)cancelCurrentBigImageLoad;

@end
