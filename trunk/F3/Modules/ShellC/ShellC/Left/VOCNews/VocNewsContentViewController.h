//
//  NewsContentViewController.h
//  F3
//
//  Created by admin on 12-7-19.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataModel.h"
#import "MGTemplateEngine.h"
#import "ICUTemplateMatcher.h"
#import "TKHorizontalTableView.h"
#import "MTZoomWindowDelegate.h"
#import <MessageUI/MessageUI.h>
#import "BaseViewController.h"
#import "UIImageView+WebCache.h"

typedef enum
{
    VocNewsFontSize_Small = 0,
    VocNewsFontSize_Normal = 1,
    VocNewsFontSize_Middle = 2,                        
    VocNewsFontSize_Large = 3
} VocNewsFontSize;

@interface VocNewsContentViewController : BaseViewController <UIWebViewDelegate, MGTemplateEngineDelegate,
    TKHorizontalTableViewDelegate,TKHorizontalTableViewDataSource,MTZoomWindowDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,MFMessageComposeViewControllerDelegate> {
    
    UIImageView *selectedImageView;
    
    NSString *requestUrl;
        
    VocNewsFontSize fontSize;
        
}

@property (nonatomic,retain) UIImageView *selectedImageView;

@property (nonatomic ,retain) NewsDetailInfo *contentVo;

@property (nonatomic, assign) int msgId;

- (CGSize)displayRectForImage:(CGSize)imageSize;
@end
