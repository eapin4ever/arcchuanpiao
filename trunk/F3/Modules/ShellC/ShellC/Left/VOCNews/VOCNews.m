//
//  VOCNews.m
//  VoiceChina
//
//  Created by ZhangChuntao on 3/28/13.
//  Copyright (c) 2013 zhangshangtong. All rights reserved.
//

#import "VOCNews.h"
#import "UIImageView+BigImageLoader.h"

@implementation VOCNews

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)doAdditionalInit
{
    self.backgroundColor = [UIColor clearColor];
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    _scrollView.delegate = self;
    _scrollView.pagingEnabled = YES;
    _scrollView.backgroundColor = [UIColor clearColor];
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    [self addSubview:_scrollView];
    
    _titleBgImageView = [[[UIImageView alloc] initWithFrame:CGRectMake( 0,self.frame.size.height - 72, self.frame.size.width, 72)] autorelease];
    _titleBgImageView.image = [UIImage imageNamed:@"news_view_bg.png"];
    _titleBgImageView.opaque = NO;
    [self addSubview:_titleBgImageView];
    
    _describeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, _titleBgImageView.frame.size.width-20, _titleBgImageView.frame.size.height - 22)];
    _describeLabel.text = @"";
    _describeLabel.backgroundColor = [UIColor clearColor];
    _describeLabel.font = [UIFont systemFontOfSize:12];
    _describeLabel.textColor = [UIColor whiteColor];
    [_titleBgImageView addSubview:_describeLabel];
    
    _pageControl = [[TKPageControl alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 22, 80, 22)];
    _pageControl.type = TKPageControlTypeOnImageOffImage;
    _pageControl.dotSize = 8;
    _pageControl.onImage = [UIImage imageNamed:@"pagecontrol_dot_h.png"];
    _pageControl.offImage = [UIImage imageNamed:@"pagecontrol_dot.png"];
    _pageControl.backgroundColor = [UIColor clearColor];
    _pageControl.userInteractionEnabled = NO;
    [self addSubview:_pageControl];
    _curPage = 0;//轮播图得第一张

    
    _carouselTimer = [[NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(pageTurn) userInfo:nil repeats:YES] retain];
    self.tagBgImageView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tag_bg.png"]] autorelease];
    self.tagBgImageView.frame = CGRectMake(0, self.frame.size.height - 82, self.tagBgImageView.frame.size.width, self.tagBgImageView.frame.size.height);
    [self addSubview:self.tagBgImageView];
    self.tagLabel = [[[UILabel alloc] initWithFrame:CGRectMake(5, self.tagBgImageView.frame.origin.y + 5, self.tagBgImageView.frame.size.width - 10, self.tagBgImageView.frame.size.height - 10)] autorelease];
    self.tagLabel.font = [UIFont systemFontOfSize:11.0f];
    self.tagLabel.backgroundColor = [UIColor clearColor];
    self.tagLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.tagLabel];
    _scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    _pageControl.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    _describeLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    _titleBgImageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    self.tagLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    self.tagBgImageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
}

- (void)loadData
{
    _pageControl.currentPage = _curPage;
    
    if(_scrollView.subviews && _scrollView.subviews.count > 0) {
		[_scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    [self getDisplayImagesWithCurpage:_curPage];
    
    for (int i = 0; i < 3; i ++) {
        int index = [[_curSource objectAtIndex:i] intValue];
        UIButton *imageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        imageBtn.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, _scrollView.frame.size.height);
        imageBtn.frame = CGRectOffset(imageBtn.frame, imageBtn.frame.size.width * i, 0);
//        UIImageView* imageView = [[UIImageView alloc] initWithFrame:imageBtn.frame];
        UIImageView* imageView = [[[UIImageView alloc] initWithFrame:CGRectMake(imageBtn.frame.origin.x, imageBtn.frame.origin.y, 320, 480)] autorelease];
        [imageView setBigImageWithURL:[NSURL URLWithString:[_carouselDataSource carouseView:self picURLForViewAtIndex:index]] placeholderImage:[UIImage imageNamed:@"home_default.png"]];
        imageView.contentMode = UIViewContentModeScaleToFill;
        imageView.clipsToBounds = YES;
        [imageBtn addTarget:self action:@selector(carouselViewDidSelect:) forControlEvents:UIControlEventTouchUpInside];
        [_scrollView addSubview:imageBtn];
        [_scrollView addSubview:imageView];
    }
    self.tagLabel.text = [_carouselDataSource carouseView:self headerForViewAtIndex:_pageControl.currentPage];
    _describeLabel.text = [_carouselDataSource carouseView:self titleForViewAtIndex:_pageControl.currentPage];
    
    [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width, 0)];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
