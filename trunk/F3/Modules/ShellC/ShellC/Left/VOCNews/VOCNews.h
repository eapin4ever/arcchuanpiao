//
//  VOCNews.h
//  VoiceChina
//
//  Created by ZhangChuntao on 3/28/13.
//  Copyright (c) 2013 zhangshangtong. All rights reserved.
//

#import "NewsCarouselView.h"

@interface VOCNews : NewsCarouselView

@property (nonatomic, strong) UIImageView* tagBgImageView;
@property (nonatomic, strong) UILabel* tagLabel;

@end
