//
//  VOCNewsViewController.h
//  VoiceChina
//
//  Created by ZhangChuntao on 3/26/13.
//  Copyright (c) 2013 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "NewsCarouselView.h"

@interface VOCNewsViewController : HomeViewController <NewsCarouselViewDataSource,NewsCarouselViewDelegate>
{
    NewsCatagoryInfo *_catagoryInfo;
}

@property (strong, nonatomic) IBOutlet NewsCarouselView *carouselView;
@property (strong, nonatomic) NSArray *dataArray;
@property (nonatomic, strong) UIView *guideView;

@end
