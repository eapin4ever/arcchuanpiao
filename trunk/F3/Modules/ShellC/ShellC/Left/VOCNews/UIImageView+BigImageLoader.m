//
//  UIImageView+BigImageLoader.m
//  VoiceChina
//
//  Created by wangguangzhao on 13-4-15.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "UIImageView+BigImageLoader.h"
#import "UIImageView+WebCache.h"
#import <CommonCrypto/CommonDigest.h>

@interface BigImageCache : NSCache
- (UIImage *)cachedImageForKey:(NSString *)key;
- (void)cacheImage:(UIImage *)image
        forKey:(NSString *)key;
@end

@implementation UIImageView (BigImageLoader)

+ (BigImageCache *)big_sharedImageCache {
    static BigImageCache *_big_imageCache = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _big_imageCache = [[BigImageCache alloc] init];
        [_big_imageCache setCountLimit:4];
    });
    
    return _big_imageCache;
}

- (void)setBigImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder
{
    [self setBigImageWithURL:url placeholderImage:placeholder options:0];
}

- (void)setBigImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder options:(SDWebImageOptions)options
{
    NSString *urlString = [url absoluteString];
    NSString *path = [self pathForKey:urlString];
    if ([[[self class] big_sharedImageCache] objectForKey:urlString]) {
        self.image = [[[self class] big_sharedImageCache] objectForKey:urlString];
        return;
    } else {
        UIImage *tmpImage = [[UIImage alloc] initWithContentsOfFile:path];
        if (tmpImage != nil) {
            self.image = tmpImage;
            [[[self class] big_sharedImageCache] setObject:tmpImage forKey:urlString];
            return;
        }
    }
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    // Remove in progress downloader from queue
    [manager cancelForDelegate:self];
    
    self.image = placeholder;
    
    if (url)
    {
        [manager downloadWithURL:url delegate:self options:options];
    }
}

- (void)cancelCurrentBigImageLoad
{
    @synchronized(self)
    {
        [[SDWebImageManager sharedManager] cancelForDelegate:self];
    }
}

//此方法与SDWebImageCache中的- (NSString *)cachePathForKey:(NSString *)key方法要完全相同，因为在从文件系统中读图片时，读的是SDWebImageCache的目录下的文件
- (NSString *)pathForKey:(NSString *)key
{
    const char *str = [key UTF8String];
    unsigned char r[CC_MD5_DIGEST_LENGTH];
    CC_MD5(str, (CC_LONG)strlen(str), r);
    NSString *filename = [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
                          r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8], r[9], r[10], r[11], r[12], r[13], r[14], r[15]];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *diskCachePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"ImageCache"];
    return [diskCachePath stringByAppendingPathComponent:filename];
}

#pragma mark - SDWebImageManager Delegate
- (void)webImageManager:(SDWebImageManager *)imageManager didFinishWithImage:(UIImage *)image forURL:(NSURL *)url userInfo:(NSDictionary *)info
{
    self.image = image;
    [self setNeedsLayout];
    
    if (self.superview != nil) {
        [[[self class] big_sharedImageCache] setObject:image forKey:[url absoluteString]];
    }
    [[SDImageCache sharedImageCache] removeImageForKey:[url absoluteString] fromDisk:NO];

}

@end


@implementation BigImageCache

- (UIImage *)cachedImageForKey:(NSString *)key {
    
	return [self objectForKey:key];
}

- (void)cacheImage:(UIImage *)image
        forKey:(NSString *)key
{
    if (image && key) {
        [self setObject:image forKey:key];
    }
}

@end
