//
//  ZSTF3Engine+ShellC.h
//  ShellC
//
//  Created by LiZhenQu on 13-4-26.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTF3Engine.h"

@protocol ZSTF3EngineShellCDelegate <ZSTF3EngineDelegate>

@optional

- (void)getHomeCoverSucceed:(id)array;
- (void)getHomeCoverFailed:(int)resultCode;

- (void)getShellCContentDidSucceed:(id)userInfo;
- (void)getShellCContentDidFailed:(int)resultCode userInfo:(id)userInfo;

@end

@interface ZSTF3Engine (ShellC)

- (void)getHomeCoverWithDelegate:(id<ZSTF3EngineDelegate>)delegate;

- (void)getShellCContentUserInfo:(id)anUserInfo;

@end