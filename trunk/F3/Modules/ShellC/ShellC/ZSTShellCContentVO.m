//
//  ZSTShellCContentVO.m
//  ShellC
//
//  Created by LiZhenQu on 13-4-26.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTShellCContentVO.h"

@implementation NSDictionary (NullReplace)

- (id)valueForKeyNotNull:(NSString *)key
{
    id object = [self valueForKey:key];
    if ([object isKindOfClass:[NSNumber class]] ||
        [object isKindOfClass:[NSString class]] ||
        [object isKindOfClass:[NSArray class]] ||
        [object isKindOfClass:[NSDictionary class]]) {
        return object;
    }
    return nil;
}

@end


@implementation NewsCatagoryInfo

- (id)initWithDic:(NSDictionary *)dic
{
    if (![dic isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    self = [super init];
    if (self) {
        
        self.notice = [dic valueForKeyNotNull:@"notice"];
        self.categoryid = [[dic valueForKeyNotNull:@"categoryid"] integerValue];
        self.categoryname = [dic valueForKeyNotNull:@"categoryname"];
        self.ordernum = [[dic valueForKeyNotNull:@"ordernum"] integerValue];
        self.version = [[dic valueForKeyNotNull:@"version"] integerValue];
        self.defaultflag = [[dic valueForKeyNotNull:@"defaultflag"] boolValue];
        self.iconurl = [dic valueForKeyNotNull:@"iconurl"];
        
    }
    return self;
}

+ (NewsCatagoryInfo *)newsCatagoryInfoWithdic:(NSDictionary*)dic
{
    if (!dic) {
        return nil;
    }
    NewsCatagoryInfo* info = [[NewsCatagoryInfo alloc] initWithDic:dic];
    return info;
}

+ (NSMutableArray *)newsCatagoryListWithdic:(NSDictionary*)dic
{
    NSArray *array = [dic valueForKeyNotNull:@"info"];
    if (![array isKindOfClass:[NSArray class]]) {
        return nil;
    }
    
    NSMutableArray *result = [NSMutableArray array];
    for (int i=0; i<array.count; i++) {
        NSDictionary *dictionary = [array objectAtIndex:i];
        if (![dictionary isKindOfClass:[NSDictionary class]]) {
            continue;
        }
        
        NewsCatagoryInfo* info = [[NewsCatagoryInfo alloc] initWithDic:dictionary];
        if (info != nil) {
            [result addObject:info];
        }
    }
    return result;
}

@end


@implementation HomeCoverInfo

- (id)initWithDic:(NSDictionary *)dic{
    if (![dic isKindOfClass:[NSDictionary class]]) {
        return nil;
    }
    self = [super init];
    if (self) {
        self.coverId = [[dic valueForKeyNotNull:@"id"] intValue];
        self.title = [dic valueForKeyNotNull:@"title"];
        self.picURL = [dic valueForKeyNotNull:@"fileurl"];
        self.linkURL = [dic valueForKeyNotNull:@"linkurl"];
        self.redirectURL = [dic valueForKeyNotNull:@"redirecturl"];
        self.summary = [dic valueForKeyNotNull:@"summary"];
        self.Description = [dic valueForKeyNotNull:@"description"];
        self.orderNum = [[dic valueForKeyNotNull:@"ordernum"] intValue];
        self.addTime = [dic valueForKeyNotNull:@"addtime"];
        self.content = [dic valueForKey:@"content"];
    }
    return self;
}

+ (NSArray*)coverListWithDic:(NSDictionary *)dic
{
    NSArray* array = [dic valueForKeyNotNull:@"info"];
    if (![array isKindOfClass:[NSArray class]]) {
        return nil;
    }
    NSMutableArray* result = [NSMutableArray array];
    for (int i = 0; i < array.count; i++) {
        NSDictionary* mDic = [array objectAtIndex:i];
        HomeCoverInfo* info = [[HomeCoverInfo alloc] initWithDic:mDic];
        if (info) {
            [result addObject:info];
        }
    }
    return result;
}

@end

@implementation ZSTShellCContentVO

@synthesize title;
@synthesize summary;
@synthesize keyword;
@synthesize source;
@synthesize addtime;
@synthesize isfavorites;
@synthesize detailid;
@synthesize letter;
@synthesize imageurl;
@synthesize imagedesc;
@synthesize ordernum;
@synthesize officialurl;
@synthesize onlineservice;
@synthesize ServicePhone;


+ (id)voWithDic:(NSDictionary *)dic
{
    return [[[self alloc] initWithDic: dic] autorelease];
}

- (id)initWithDic:(NSDictionary *)dic
{
    if( (self=[super init])) {
        self.title =  [dic safeObjectForKey:@"title"];
        self.summary =  [dic safeObjectForKey:@"summary"];
        self.keyword =  [dic safeObjectForKey:@"keyword"];
        self.source =  [dic safeObjectForKey:@"source"];
        self.addtime =  [dic safeObjectForKey:@"addtime"];
        self.isfavorites =  [[dic safeObjectForKey:@"isfavorites"] boolValue];
        self.detailid =  [[dic safeObjectForKey:@"detailid"] intValue];
        self.letter =  [dic safeObjectForKey:@"letter"];
        self.imageurl =  [dic safeObjectForKey:@"imageurl"];
        self.imagedesc =  [dic safeObjectForKey:@"imagedesc"];
        self.ordernum =  [[dic safeObjectForKey:@"ordernum"] intValue];
        self.officialurl =  [dic safeObjectForKey:@"officialurl"];
        self.onlineservice =  [dic safeObjectForKey:@"onlineservice"];
        self.ServicePhone =  [dic safeObjectForKey:@"ServicePhone"];
        
        
    }
    return self;
}


- (void)dealloc
{
    self.title = nil;
    self.summary = nil;
    self.keyword = nil;
    self.source = nil;
    self.addtime = nil;
    self.imageurl = nil;
    self.imagedesc = nil;
    self.officialurl = nil;
    self.onlineservice = nil;
    self.ServicePhone = nil;
    
    [super dealloc];
}

@end


