//
//  ZSTShellCLinkViewController.h
//  ShellC
//
//  Created by LiZhenQu on 13-5-8.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTModuleBaseViewController.h"

@interface ZSTShellCLinkViewController : ZSTModuleBaseViewController<UIWebViewDelegate>
{
    UIWebView *webview;
    NSString *linkurl;
}

@property (nonatomic, retain) UIWebView *webview;
@property (nonatomic, retain) NSString *linkurl;

@end
