//
//  ZSTShellCLinkViewController.m
//  ShellC
//
//  Created by LiZhenQu on 13-5-8.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTShellCLinkViewController.h"

@interface ZSTShellCLinkViewController ()

@end

@implementation ZSTShellCLinkViewController

@synthesize webview;
@synthesize linkurl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
	
    NSURL *url = [NSURL URLWithString:self.linkurl];
    
    self.webview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 480 + (iPhone5?88:0))];
    [self.webview loadRequest:[NSURLRequest requestWithURL:url]];
    [self.webview setUserInteractionEnabled:YES];  //是否支持交互
    [self.webview setDelegate:self];
    self.webview.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.webview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [linkurl release];
    [webview stopLoading];
    [webview setDelegate:nil];
    [webview release];
    [super dealloc];
}


@end
