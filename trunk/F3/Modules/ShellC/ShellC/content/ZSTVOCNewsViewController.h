//
//  VOCNewsViewController.h
//  VoiceChina
//
//  Created by ZhangChuntao on 3/26/13.
//  Copyright (c) 2013 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeViewController.h"
#import "ZSTNewsCarouselView.h"
#import "ZSTShellCContentVO.h"
#import "ZSTF3Engine.h"
#import "ZSTF3Engine+ShellC.h"

@interface ZSTVOCNewsViewController : HomeViewController <NewsCarouselViewDataSource,NewsCarouselViewDelegate,ZSTF3EngineShellCDelegate>
{
    NewsCatagoryInfo *_catagoryInfo;
}

@property (strong, nonatomic) NewsCarouselView *carouselView;
@property (strong, nonatomic) NSArray *dataArray;

@end
