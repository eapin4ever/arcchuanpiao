//
//  NewsCarouselView.h
//  F3
//
//  Created by xuhuijun on 12-7-18.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZSTPageControl.h"

@class NewsCarouselView;

@protocol NewsCarouselViewDataSource <NSObject>

@optional

- (NSString*)carouseView:(NewsCarouselView*)carouselView titleForViewAtIndex:(NSInteger)index;
- (NSString*)carouseView:(NewsCarouselView *)carouselView headerForViewAtIndex:(int)index;
@required
- (NSUInteger)numberOfViewsInCarouselView:(NewsCarouselView *)newsCarouselView;
- (NSString*)carouseView:(NewsCarouselView *)carouselView picURLForViewAtIndex:(int)index;

@end

@protocol NewsCarouselViewDelegate <NSObject>

@optional
- (void)carouselView:(NewsCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;

@end

@interface NewsCarouselView : UIView <UIScrollViewDelegate>
{
    UIScrollView *_scrollView;
    UILabel *_describeLabel;
    ZSTPageControl *_pageControl;
    UIImageView* _titleBgImageView;
    NSTimer *_carouselTimer;
    
    NSInteger _totalPages;
    NSInteger _curPage;
    
    NSMutableArray *_curSource;
    
    id<NewsCarouselViewDataSource> _carouselDataSource;
    id<NewsCarouselViewDelegate>  _carouselDelegate;
}

- (void)getDisplayImagesWithCurpage:(NSInteger)page;
@property(nonatomic, assign) id<NewsCarouselViewDataSource> carouselDataSource;
@property(nonatomic, assign) id<NewsCarouselViewDelegate> carouselDelegate;

- (void)reloadData;


@end
