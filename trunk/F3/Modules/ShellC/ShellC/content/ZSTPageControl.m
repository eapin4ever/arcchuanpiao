//
//  ZSTPageControl.m
//  ShellC
//
//  Created by xuhuijun on 13-6-25.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTPageControl.h"

#define kDotSize	4.0f
#define kDotSpace		6.0f

@implementation ZSTPageControl

@synthesize type;
@synthesize onColor;
@synthesize offColor;
@synthesize onImage;
@synthesize offImage;
@synthesize dotSize;
@synthesize dotSpacing;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma mark -
#pragma mark drawRect

- (void)drawRect:(CGRect)rect
{
	// get the current context
	CGContextRef context = UIGraphicsGetCurrentContext() ;
	
	// save the context
	CGContextSaveGState(context) ;
	
	// allow antialiasing
	CGContextSetAllowsAntialiasing(context, TRUE) ;
	
	// get the caller's diameter if it has been set or use the default one
	CGFloat diameter = (dotSize > 0) ? dotSize : kDotSize ;
	CGFloat space = (dotSpacing > 0) ? dotSpacing : kDotSpace ;
	
	// geometry
	CGRect currentBounds = CGRectMake(self.frame.origin.x, self.frame.origin.y, 75, 12) ;
	CGFloat dotsWidth = self.numberOfPages * diameter + MAX(0, self.numberOfPages - 1) * space ;
	CGFloat x = CGRectGetMidX(currentBounds) - dotsWidth / 2 ;
	CGFloat y = CGRectGetMidY(currentBounds) - diameter / 2 ;
	
	// get the caller's colors it they have been set or use the defaults
	UIColor *drawOnColor = onColor ? onColor : [UIColor colorWithWhite: 1.0f alpha: 1.0f];
	UIColor *drawOffColor = offColor ? offColor : [UIColor colorWithWhite: 0.7f alpha: 0.5f];
    
    UIImage *drawOnImage = onImage ? onImage : [UIImage imageNamed:@"tabbar_badge_bg.png"];
	UIImage *drawOffImage = offImage ? offImage : [UIImage imageNamed:@"ipad_attachment_deleteImage.png"];
	
	// actually draw the dots
	for (int i = 0 ; i < numberOfPages ; i++)
	{
		CGRect dotRect = CGRectMake(x, y, diameter, diameter) ;
		
		if (i == currentPage)
		{
			if (type == TKPageControlTypeOnFullOffFull || self.type == TKPageControlTypeOnFullOffEmpty)
			{
				CGContextSetFillColorWithColor(context, drawOnColor.CGColor) ;
                CGContextFillEllipseInRect(context, CGRectInset(dotRect, -0.5f, -0.5f)) ;
                
			}else if (type == TKPageControlTypeOnImageOffImage){
                
                CGContextDrawImage(context, dotRect, drawOnImage.CGImage);
                
			}else{
                
				CGContextSetStrokeColorWithColor(context, drawOnColor.CGColor) ;
				CGContextStrokeEllipseInRect(context, dotRect) ;
                
			}
		}
		else
		{
			if (type == TKPageControlTypeOnEmptyOffEmpty || type == TKPageControlTypeOnFullOffEmpty)
			{
				CGContextSetStrokeColorWithColor(context, drawOffColor.CGColor) ;
				CGContextStrokeEllipseInRect(context, dotRect) ;
                
			}else if (type == TKPageControlTypeOnImageOffImage){
                
                CGContextDrawImage(context, dotRect, drawOffImage.CGImage);
                
            }else {
                
				CGContextSetFillColorWithColor(context, drawOffColor.CGColor) ;
				CGContextFillEllipseInRect(context, CGRectInset(dotRect, -0.5f, -0.5f)) ;
			}
		}
		
		x += diameter + space ;
	}
	
	// restore the context
	CGContextRestoreGState(context) ;
}


- (void)setNumberOfPages:(NSInteger)numOfPages
{
	// make sure the number of pages is positive
	numberOfPages = MAX(0, numOfPages) ;
	
	// we then need to update the current page
	currentPage = MIN(MAX(0, currentPage), numberOfPages - 1) ;
	
	// correct the bounds accordingly
	self.bounds = CGRectMake(self.frame.origin.x, self.frame.origin.y, 75, 12);
	
	// we need to redraw
	[self setNeedsDisplay] ;
	
	// depending on the user preferences, we hide the page control with a single element
	if (self.hidesForSinglePage && (numOfPages < 2))
		[self setHidden: YES] ;
	else
		[self setHidden: NO] ;
}

- (void)setDotSize:(CGFloat)aDiameter
{
	dotSize = aDiameter ;
	
	// correct the bounds accordingly
	self.bounds = CGRectMake(self.frame.origin.x, self.frame.origin.y, 75, 12);
	
	[self setNeedsDisplay] ;
}

- (void)setDotSpacing:(CGFloat)aSpace
{
	dotSpacing = aSpace ;
	
	// correct the bounds accordingly
	self.bounds = CGRectMake(self.frame.origin.x, self.frame.origin.y, 75, 12);
	
	[self setNeedsDisplay] ;
}


@end
