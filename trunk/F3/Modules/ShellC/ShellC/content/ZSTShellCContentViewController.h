//
//  NewsContentViewController.h
//  F3
//
//  Created by admin on 12-7-19.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MGTemplateEngine.h"
#import "ICUTemplateMatcher.h"
#import "TKHorizontalTableView.h"
//#import "ZSTF3Engine+ArticleD.h"
#import "ZSTShellCContentVO.h"
#import "TKAsynImageView.h"
#import "MTZoomWindowDelegate.h"
#import <MessageUI/MessageUI.h>
#import "ZSTModuleBaseViewController.h"

typedef enum
{
    FontSize_Small = 0,
    FontSize_Normal = 1,
    FontSize_Middle = 2,                        
    FontSize_Large = 3
} ZSTArticleAFontSize;

@interface ZSTShellCContentViewController : ZSTModuleBaseViewController <UIWebViewDelegate, MGTemplateEngineDelegate,
    TKHorizontalTableViewDelegate,TKHorizontalTableViewDataSource,MTZoomWindowDelegate,HJManagedImageVDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,MFMessageComposeViewControllerDelegate> {
    
    TKAsynImageView *selectedImageView;
    
    NSArray *newsArr;

    TKHorizontalTableView *hTableView;
    
    NSString *requestUrl;
        
    ZSTArticleAFontSize fontSize;
}
@property (nonatomic, retain) TKHorizontalTableView *hTableView;
@property (nonatomic,retain) NSArray *newsArr;
@property (nonatomic,retain) TKAsynImageView *selectedImageView;

@property (nonatomic ,retain) NSArray *contentVo;
@property (nonatomic, retain) HomeCoverInfo *info;


- (CGSize)displayRectForImage:(CGSize)imageSize;
@end
