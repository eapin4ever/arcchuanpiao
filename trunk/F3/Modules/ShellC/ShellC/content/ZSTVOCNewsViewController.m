//
//  VOCNewsViewController.m
//  VoiceChina
//
//  Created by ZhangChuntao on 3/26/13.
//  Copyright (c) 2013 zhangshangtong. All rights reserved.
//

#import "ZSTVOCNewsViewController.h"
#import "ZSTShellCContentViewController.h"
#import "NSStringAdditions.h"
#import "GTMBase64.h"
#import "ZSTShellCLinkViewController.h"
#import "ZSTMineViewController.h"

@interface ZSTVOCNewsViewController ()

@end

@implementation ZSTVOCNewsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark - NewsCarouselViewDataSource

- (NSUInteger)numberOfViewsInCarouselView:(NewsCarouselView *)newsCarouselView
{
    return [self.carouselArray count];
}

- (NSString*)carouseView:(NewsCarouselView*)carouselView titleForViewAtIndex:(NSInteger)index
{
    if ([self.carouselArray count] != 0) {
        HomeCoverInfo* info = [self.carouselArray objectAtIndex:index];
        return info.summary;
    }
    return nil;
}

- (NSString*)carouseView:(NewsCarouselView *)carouselView picURLForViewAtIndex:(int)index
{
    if ([self.carouselArray count] != 0) {
        HomeCoverInfo* info = [self.carouselArray objectAtIndex:index];
        return info.picURL;
    }
    return nil;
}

- (NSString*)carouseView:(NewsCarouselView *)carouselView headerForViewAtIndex:(int)index
{
    if ([self.carouselArray count] != 0) {
        HomeCoverInfo* info = [self.carouselArray objectAtIndex:index];
        return info.title;
    }
    return nil;
}

#pragma mark - NewsCarouselViewDelegate

- (void)carouselView:(NewsCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index
{
    HomeCoverInfo *info = [self.carouselArray objectAtIndex:index];
    
    if (info.linkURL) {
        if ([info.linkURL isEqualToString:@""]) {
        
            ZSTShellCContentViewController *viewController = [[[ZSTShellCContentViewController alloc] init] autorelease];
            viewController.hidesBottomBarWhenPushed = YES;
            viewController.info = info;
            [self.navigationController pushViewController:viewController animated:YES];

        } else {
            
            ZSTShellCLinkViewController *viewController = [[[ZSTShellCLinkViewController alloc] init] autorelease];
            viewController.linkurl = info.linkURL;
            viewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:viewController animated:YES];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.carouselView = [[NewsCarouselView alloc] initWithFrame:CGRectMake(0, 0, 320, self.view.height-(IS_IOS_7?20:0))];
    self.carouselView.carouselDataSource = self;
    self.carouselView.carouselDelegate = self;
//    self.carouselView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.carouselView];
//    [self readCache];
     [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在加载..." , nil)];
    [self.engine getHomeCoverWithDelegate:self];

}

- (void)mineAction:(id)sender
{
    ZSTMineViewController *controller = [[ZSTMineViewController alloc] initWithNibName:@"ZSTMineViewController" bundle:nil];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

- (void)readCache
{
//    id carouselInfo = [CacheUtil readCacheOfType:CACHE_HOME_NEWS_INFO];                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
//    id carousels = [HomeCoverInfo coverListWithDic:carouselInfo];
//    if (![carousels isKindOfClass:[NSArray class]]) {
//        carousels = [NSArray array];
//    }
//    if ([(NSArray *)carousels count] > 0) {
//        self.carouselArray = [NSMutableArray arrayWithArray:carousels];
//    }
//    [self.carouselView reloadData];
}

- (void)getHomeCoverSucceed:(id)array
{
    [TKUIUtil hiddenHUD];
    if (array) {
        self.carouselArray = [NSMutableArray arrayWithArray:array];
        [self.carouselView reloadData];
    }
}

- (void)getHomeCoverFailed:(int)resultCode
{
    [TKUIUtil hiddenHUD];
   [TKUIUtil alertInView:self.view withTitle:@"获取广告失败！" withImage:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
