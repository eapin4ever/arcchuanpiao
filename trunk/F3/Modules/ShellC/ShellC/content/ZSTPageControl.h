//
//  ZSTPageControl.h
//  ShellC
//
//  Created by xuhuijun on 13-6-25.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "TKPageControl.h"

@interface ZSTPageControl : TKPageControl

@property (nonatomic) TKPageControlType type ;

@property (nonatomic, retain) UIColor *onColor ;
@property (nonatomic, retain) UIColor *offColor ;

@property (nonatomic, retain) UIImage *onImage;
@property (nonatomic, retain) UIImage *offImage;


@property (nonatomic) CGFloat dotSize ;
@property (nonatomic) CGFloat dotSpacing ;

@end
