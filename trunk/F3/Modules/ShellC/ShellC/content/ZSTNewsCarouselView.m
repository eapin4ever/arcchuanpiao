//
//  NewsCarouselView.m
//  F3
//
//  Created by xuhuijun on 12-7-18.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTNewsCarouselView.h"
#import "TKAsynImageView.h"

#define maxItems 5

@implementation NewsCarouselView

@synthesize carouselDelegate = _carouselDelegate;
@synthesize carouselDataSource = _carouselDataSource;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self doAdditionalInit];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self doAdditionalInit];
    }
    return self;
}

- (void)doAdditionalInit
{
    self.backgroundColor = [UIColor clearColor];
    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    _scrollView.delegate = self;
    _scrollView.pagingEnabled = YES;
    _scrollView.scrollEnabled = YES;
    _scrollView.backgroundColor = [UIColor clearColor];
    _scrollView.showsHorizontalScrollIndicator = NO;
    _scrollView.showsVerticalScrollIndicator = NO;
    [self addSubview:_scrollView];
    
    _titleBgImageView = [[[UIImageView alloc] initWithFrame:CGRectMake( 0,self.frame.size.height - 75 - 44 - 44, self.frame.size.width, 75)] autorelease];
    _titleBgImageView.image = [ZSTModuleImage(@"module_shellc_scroll_txt_bg.png") stretchableImageWithLeftCapWidth:1 topCapHeight:0];
    _titleBgImageView.opaque = NO;
    _titleBgImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:_titleBgImageView];
    [self bringSubviewToFront:_titleBgImageView];
    
    UIImageView *pageBgImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, _titleBgImageView.frame.origin.y-16, 98, 17)];
    pageBgImage.image = ZSTModuleImage(@"module_shellc_pagecontrol_bg.png");
    pageBgImage.backgroundColor = [UIColor clearColor];
    [self addSubview:pageBgImage];
    
    UIImageView *markImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 22, 22)];
    markImage.image = ZSTModuleImage(@"module_shellc_markImage.png");
    markImage.backgroundColor = [UIColor clearColor];
    [pageBgImage addSubview:markImage];
    
    _describeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, _titleBgImageView.frame.size.width-20, _titleBgImageView.frame.size.height-17)];
    _describeLabel.text = @"";
    _describeLabel.backgroundColor = [UIColor clearColor];
    _describeLabel.font = [UIFont systemFontOfSize:13];
    _describeLabel.textColor = [UIColor whiteColor];
    [_titleBgImageView addSubview:_describeLabel];
        
    _pageControl = [[ZSTPageControl alloc] initWithFrame:CGRectMake(20,  4, 75, 17)];
    _pageControl.type = TKPageControlTypeOnImageOffImage;
    _pageControl.dotSize = 8;
    _pageControl.onImage = ZSTModuleImage(@"module_shellc_pagecontrol_dot_h.png");
    _pageControl.offImage = ZSTModuleImage(@"module_shellc_pagecontrol_dot.png");
    _pageControl.backgroundColor = [UIColor clearColor];
    _pageControl.userInteractionEnabled = NO;
    [pageBgImage addSubview:_pageControl];
    _curPage = 0;//轮播图得第一张
    

    _carouselTimer = [[NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(pageTurn) userInfo:nil repeats:YES] retain];
    
    _scrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    _pageControl.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    _describeLabel.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    pageBgImage.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    markImage.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    _titleBgImageView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [pageBgImage release];
    [markImage release];
}

- (void)reloadData
{
    _totalPages = [_carouselDataSource numberOfViewsInCarouselView:self];
    if (_totalPages == 0) {
        return;
    }
    _curPage = 0;
    if (_totalPages > maxItems) {
        _totalPages = maxItems;
    }
    
    _pageControl.numberOfPages = _totalPages;
    _pageControl.hidden = NO;
    
    if (_totalPages == 1) {
        _scrollView.scrollEnabled = NO;
        _pageControl.hidden = YES;
    }
    
    _scrollView.contentSize = CGSizeMake((self.frame.size.width) * 3, 0);
    
    [self loadData];
}

- (void)loadData
{
    _pageControl.currentPage = _curPage;
    
    if(_scrollView.subviews && _scrollView.subviews.count > 0) {
		[_scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    [self getDisplayImagesWithCurpage:_curPage];
    
    for (int i = 0; i < 3; i ++) {
        int index = [[_curSource objectAtIndex:i] intValue];
        UIButton *imageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        imageBtn.frame = CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, _scrollView.frame.size.width, _scrollView.frame.size.height);
        imageBtn.frame = CGRectOffset(imageBtn.frame, imageBtn.frame.size.width * i, 0);
        TKAsynImageView* imageView = [[[TKAsynImageView alloc] initWithFrame:imageBtn.frame] autorelease];
        [imageView clear];
        imageView.url = [NSURL URLWithString:[_carouselDataSource carouseView:self picURLForViewAtIndex:index]];
        [imageView loadImage];
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        [imageBtn addTarget:self action:@selector(carouselViewDidSelect:) forControlEvents:UIControlEventTouchUpInside];
        [_scrollView addSubview:imageView];
        [_scrollView addSubview:imageBtn];
    }
    
    _describeLabel.text = [_carouselDataSource carouseView:self titleForViewAtIndex:_pageControl.currentPage];
    [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width, 0)];
    
}

- (void)getDisplayImagesWithCurpage:(NSInteger)page {
    
    NSInteger pre = [self validPageValue:_curPage-1];
    NSInteger last = [self validPageValue:_curPage+1];
    if (_curSource) {
        _curSource = nil;
    }
    _curSource = [NSMutableArray arrayWithObjects:[NSNumber numberWithInteger:pre], [NSNumber numberWithInteger:_curPage], [NSNumber numberWithInteger:last], nil];
}

#pragma mark - TKAsynImageViewDelegate

- (void)carouselViewDidSelect:(UIButton *)sender
{
    //根据_pageControl.currentPage 打开响应的详细页
    
    if ([_carouselDelegate respondsToSelector:@selector(carouselView:didSelectedViewAtIndex:)]) {
        [_carouselDelegate carouselView:self didSelectedViewAtIndex:_pageControl.currentPage];
    }
}

#pragma mark - UIScrollViewDelegate


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [scrollView setContentOffset:CGPointMake(scrollView.frame.size.width, 0) animated:YES];
}

- (NSInteger)validPageValue:(NSInteger)value {
    
    if(value == -1) value = _totalPages - 1; //最后一也
    if(value == _totalPages) value = 0; //第一页
    //中间页
    
    return value;
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    
    int x = aScrollView.contentOffset.x;
    if(x >= (2*aScrollView.frame.size.width)) {
        _curPage = [self validPageValue:_curPage+1];
        [self loadData];
    }
    if(x <= 0) {
        _curPage = [self validPageValue:_curPage-1];
        [self loadData];
    }
    
}

//当页数变化时，改变scrollView的内容大小
- (void) pageTurn
{
    return;
    if (_carouselDataSource == nil) {
        return;
    }
    
    _totalPages = [_carouselDataSource numberOfViewsInCarouselView:self];
    if (_totalPages == 0) {
        return;
    }
    
    _describeLabel.text = [_carouselDataSource carouseView:self titleForViewAtIndex:_pageControl.currentPage];
    
    [_scrollView setContentOffset:CGPointMake((_scrollView.frame.size.width) * 2, 0.0f) animated:YES];
    
}

- (void)carouselViewStopAnimation
{
    if ([_carouselTimer isValid]) {
        [_carouselTimer invalidate];
    }
}

- (void)dealloc
{
    [_carouselTimer invalidate];
    [_carouselTimer release];
    [_describeLabel release];
    [_scrollView release];
    [_pageControl release];
    [super dealloc];
}
@end
