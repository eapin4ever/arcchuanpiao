//
//  ZSTCoverAView.h
//  F3
//
//  Created by xuhuijun on 13-1-14.
//  Copyright (c) 2013年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKAsynImageView.h"
#import "ZSTModuleBaseView.h"

@class ZSTCoverAView;
@class ChromeProgressBar;

@protocol ZSTCoverAViewDelegate <NSObject>

- (void)coverAViewDidClicked:(ZSTCoverAView *)coverAView;
- (void)coverAViewDidSlided:(ZSTCoverAView *)coverAView;
- (void)coverAViewDidDismiss:(ZSTCoverAView *)coverAView;



@end

@interface ZSTCoverAView : ZSTModuleBaseView <TKAsynImageViewDelegate,HJManagedImageVDelegate>

{
    NSMutableArray *imagesArray;
    NSMutableArray *imageUrls;
    float timeTransition;
    BOOL isLoop;
    id <ZSTCoverAViewDelegate> delegate;
    UIWindow *_window;
    BOOL isAnimating;
    NSTimer *_animationTimer;

    
}

@property (nonatomic, assign) float timeTransition;
@property (nonatomic, retain) NSMutableArray *imagesArray;
@property (nonatomic, retain) NSMutableArray *imageUrls;
@property (nonatomic, retain) UISwipeGestureRecognizer *recognizer;
@property (nonatomic, retain) UITapGestureRecognizer *tapRecognizer;
@property (nonatomic, retain) UIView *perView;
@property (nonatomic, retain) UIImageView *swipeImage;
@property (nonatomic, retain) ChromeProgressBar *progressView;
@property (nonatomic, assign) float remainTime;
@property (nonatomic, retain) NSNumber *currentNum;
@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *descriptionLabel;
@property (nonatomic, retain) NSArray *coverArray;




@property (nonatomic) BOOL isLoop;
@property (nonatomic,assign) id<ZSTCoverAViewDelegate> delegate;

- (void) animateWithURLs:(NSArray *)urls transitionDuration:(float)duration loop:(BOOL)shouldLoop;
- (void) coverAShow;

@end
