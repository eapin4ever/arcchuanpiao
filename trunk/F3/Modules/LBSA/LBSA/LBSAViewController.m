//
//  LBSAViewController.m
//  LBSA
//
//  Created by LiZhenQu on 13-4-12.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "LBSAViewController.h"
#import "ZSTModule.h"
#import "ZSTModuleManager.h"
#import "ZSTUtils.h"


@interface LBSAViewController ()
{
    NSString *_url;
}

@end

@implementation LBSAViewController

@synthesize webCanGoBack;
@synthesize lbsaWebview;
@synthesize webCanGoForward;
@synthesize scrollToolBarView;
@synthesize backButton;
@synthesize forwardButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.lbsaWebview = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 480-15+(iPhone5?88:0))];
    self.lbsaWebview.backgroundColor = [UIColor clearColor];
    
    _url = [[NSString stringWithFormat:@"http://mod.pmit.cn/lbsawap/list.htm?ecid=%@&moduleType=%@",[ZSTF3Preferences shared].ECECCID,[NSString stringWithFormat:@"%ld",self.moduleType]] retain];
	NSURL* url = [NSURL URLWithString:_url];//创建URL

    [self.lbsaWebview loadRequest:[NSURLRequest requestWithURL:url]];
    [self.lbsaWebview setUserInteractionEnabled:YES];  //是否支持交互
    [self.lbsaWebview setDelegate:self];
    self.lbsaWebview.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.lbsaWebview];
    
    UIButton *homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [homeButton setImage:[UIImage imageNamed:@"btn_webicon_home.png"] forState:UIControlStateNormal];
    [homeButton setImage:[UIImage imageNamed:@"btn_webicon_home_light.png"] forState:UIControlStateHighlighted];
    [homeButton addTarget:self action:@selector(home) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [refreshButton setImage:[UIImage imageNamed:@"btn_webicon_reload.png"]  forState:UIControlStateNormal];
    [refreshButton setImage:[UIImage imageNamed:@"btn_webicon_reload_light.png"] forState:UIControlStateHighlighted];
    [refreshButton addTarget:self action:@selector(refresh) forControlEvents:UIControlEventTouchUpInside];

    
    backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setImage:[UIImage imageNamed:@"btn_webicon_backward.png"]  forState:UIControlStateNormal];
    [backButton setImage:[UIImage imageNamed:@"btn_webicon_backward_light.png"] forState:UIControlStateHighlighted];
    [backButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
    forwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [forwardButton setImage:[UIImage imageNamed:@"btn_webicon_forward.png"] forState:UIControlStateNormal];
    [forwardButton setImage:[UIImage imageNamed:@"btn_webicon_forward_light.png"] forState:UIControlStateHighlighted];
    [forwardButton addTarget:self action:@selector(goForward) forControlEvents:UIControlEventTouchUpInside];
    
    scrollToolBarView = [[ZSTScrollToolBarView alloc] initWithFrame:CGRectMake(1+290, self.view.frame.size.height - 41, 320, 40) color:[UIColor blackColor] alpha:0.6];
    scrollToolBarView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
     scrollToolBarView.items = [NSArray arrayWithObjects:homeButton,refreshButton,backButton,forwardButton, nil];
    scrollToolBarView.delegate = self;
    [self.view addSubview:scrollToolBarView];

}

-(void)home
{
    if (lbsaWebview) {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:_url]];
        [lbsaWebview loadRequest:request];
    }

    [scrollToolBarView reclock];
}

-(void)refresh
{
    [lbsaWebview reload];
    [scrollToolBarView reclock];
}


-(void)goBack
{
    [lbsaWebview goBack];
    [scrollToolBarView reclock];
    
}
-(void)goForward
{
    [lbsaWebview goForward];
    [scrollToolBarView reclock];
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    backButton.enabled = [webView canGoBack];
    forwardButton.enabled = [webView canGoForward];
    
    webCanGoBack = [webView canGoBack];
    webCanGoForward = [webView canGoForward];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    backButton.enabled = [webView canGoBack];
    forwardButton.enabled = [webView canGoForward];
    
    webCanGoBack = [webView canGoBack];
    webCanGoForward = [webView canGoForward];
    
}

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request
 navigationType:(UIWebViewNavigationType)navigationType {
    //    if ([_delegate respondsToSelector:
    //         @selector(webController:webView:shouldStartLoadWithRequest:navigationType:)] &&
    //        ![_delegate webController:self webView:webView
    //       shouldStartLoadWithRequest:request navigationType:navigationType]) {
    //            return NO;
    //        }
    //
    //    [_loadingURL release];
    //    _loadingURL = [request.URL retain];
    
    if ([request.URL.scheme caseInsensitiveCompare:@"native"] == NSOrderedSame) {
        
        NSDictionary *params = [request.URL.absoluteString queryDictionaryUsingEncoding:NSUTF8StringEncoding];
        NSString *module_id = [params safeObjectForKey:@"native://?module_id"];
        NSString *module_type = [params safeObjectForKey:@"module_type"];
        NSString *title = [params safeObjectForKey:@"title"];
        
        NSMutableDictionary *moduleParams = [NSMutableDictionary dictionary];
        [moduleParams setSafeObject:module_type forKey:@"type"];
        
        ZSTModule *module = [[ZSTModuleManager shared] findModule:[module_id integerValue]];
        if (module) {
            UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:[module_id integerValue] withOptions:moduleParams];
            if (controller) {
                controller.hidesBottomBarWhenPushed = YES;
                controller.navigationItem.titleView = [ZSTUtils titleViewWithTitle:title];
                controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
                [self.navigationController pushViewController:controller animated:YES];
            }
        }
        
    }
    
    return YES;
}
#pragma mark ----------ScrollToolBarViewDelegate-----------


-(void)clickedPushOut
{
    [UIView beginAnimations:@"ScrollToolBarViewPushOut" context:nil];
    [UIView setAnimationDuration:0.6];
    
    CGRect scrollToolBarViewFrame = scrollToolBarView.frame;
    scrollToolBarViewFrame.origin.x -=  290;
    scrollToolBarView.frame = scrollToolBarViewFrame;
    
    [UIView commitAnimations];
    
    backButton.enabled = webCanGoBack;
    forwardButton.enabled = webCanGoForward;
}

-(void)clickedPushIn
{
    [UIView beginAnimations:@"ScrollToolBarViewPushIn" context:nil];
    [UIView setAnimationDuration:0.6];
    
    CGRect scrollToolBarViewFrame = scrollToolBarView.frame;
    scrollToolBarViewFrame.origin.x +=  290;
    scrollToolBarView.frame = scrollToolBarViewFrame;
    
    [UIView commitAnimations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [_url release];
    [lbsaWebview stopLoading];
    [lbsaWebview setDelegate:nil];
    [scrollToolBarView stopClock];
    [scrollToolBarView release];
    [lbsaWebview release];
    [super dealloc];
}

@end
