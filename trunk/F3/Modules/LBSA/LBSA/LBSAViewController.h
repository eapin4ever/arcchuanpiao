//
//  LBSAViewController.h
//  LBSA
//
//  Created by LiZhenQu on 13-4-12.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTModuleBaseViewController.h"
#import "ZSTScrollToolBarView.h"

@interface LBSAViewController : ZSTModuleBaseViewController<UIWebViewDelegate,ZSTScrollToolBarViewDelegate>
{
    UIWebView *lbsaWebview;
    
    UIButton *backButton;
    UIButton *forwardButton;
    ZSTScrollToolBarView *scrollToolBarView;
    BOOL webCanGoBack;
    BOOL webCanGoForward;
}

@property (nonatomic, retain) UIWebView *lbsaWebview;
@property (nonatomic, retain) UIButton *backButton;
@property (nonatomic, retain) UIButton *forwardButton;
@property (nonatomic, retain) ZSTScrollToolBarView *scrollToolBarView;
@property(nonatomic) BOOL webCanGoBack;
@property(nonatomic) BOOL webCanGoForward;

@end
