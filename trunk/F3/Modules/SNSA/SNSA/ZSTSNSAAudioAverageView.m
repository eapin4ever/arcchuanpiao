//
//  AudioAverageView.m
//  AudioVideo
//
//  Created by xuhuijun on 11-12-9.
//  Copyright 2011年 掌上通. All rights reserved.
//

#import "ZSTSNSAAudioAverageView.h"
#import <QuartzCore/QuartzCore.h>

#define isRetina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)

@interface ZSTSNSAAudioAverageView()
- (void)deviceOrientationDidChange:(NSNotification *)notification;
- (void)transformForCurrentOrientation:(BOOL)animated;
@end

@implementation ZSTSNSAAudioAverageView
@synthesize rect = _rect;
@synthesize topImageView = _topImageView;
@synthesize timelabel = _timeLabel;
@synthesize stopButton = _stopButton;
@synthesize pauseButton = _pauseButton;
@synthesize continueButton = _continueButton;
@synthesize topImageOriginHeight = _topImageOriginHeight;

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        _window.windowLevel = UIWindowLevelNormal;
        _window.hidden = NO;
        self.backgroundColor = [UIColor blackColor];
        self.alpha = 0.9;
        CALayer * layer = [self layer];
        [layer setMasksToBounds:YES];
        [layer setCornerRadius:8.0];
        [layer setBorderWidth:1.0];
        [layer setBorderColor:[[UIColor clearColor] CGColor]];
        
     
        
        _topImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"voice_tips_icon_hl.png"]];
        UIImageView *downImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"voice_tips_icon_nor.png"]];

        downImageView.frame = CGRectMake((CGRectGetHeight(frame)-downImageView.image.size.width)/2, 
                                         (CGRectGetWidth(frame)-downImageView.image.size.height)/2, 
                                         downImageView.image.size.width, 
                                         downImageView.image.size.height);
        _topImageView.frame = CGRectMake((CGRectGetHeight(frame)-_topImageView.image.size.width)/2, 
                                        (CGRectGetWidth(frame)-downImageView.image.size.height)/2, 
                                        _topImageView.image.size.width, 
                                        _topImageView.image.size.height);
        
        _topImageOriginHeight =_topImageView.image.size.height;
        
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_topImageView.frame), CGRectGetMaxY(_topImageView.frame), 100, 20)];
        _timeLabel.backgroundColor = [UIColor clearColor];
        _timeLabel.text = @"0:00";
        _timeLabel.textColor = [UIColor whiteColor];
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        
        _stopButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _stopButton.frame = CGRectMake(CGRectGetMaxX(frame)-36 ,-4, 36, 36);
        [_stopButton setImage:[UIImage imageNamed:@"stop_btn_icon.png"] forState:UIControlStateNormal];  
        
        _pauseButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _pauseButton.frame = CGRectMake(0 ,-4, 36, 36);
        [_pauseButton setImage:[UIImage imageNamed:@"pause_btn_icon.png"] forState:UIControlStateNormal]; 
        
        _continueButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _continueButton.frame = CGRectMake(0 ,-4, 36, 36);
        [_continueButton setImage:[UIImage imageNamed:@"continue_btn_icon.png"] forState:UIControlStateNormal]; 
        _continueButton.hidden = YES;
        
        [self addSubview:_timeLabel];
        [self addSubview:downImageView];
        [self addSubview:_topImageView];
        [self addSubview:_stopButton];
        [self addSubview:_pauseButton];
        [self addSubview:_continueButton];
        [downImageView release];
        
        [self transformForCurrentOrientation:NO];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(deviceOrientationDidChange:) 
                                                     name:UIDeviceOrientationDidChangeNotification object:nil];
    }
    return self;
}

-(UIImage *)getTopImageFromNewRect:(CGRect)rect
{
    UIImage *image = [UIImage imageNamed:@"voice_tips_icon_hl.png"];
    CGImageRef imageRef1 =image.CGImage;
    CGImageRef imageRef2 = CGImageCreateWithImageInRect(imageRef1,rect);
    UIImage*newImage = [UIImage imageWithCGImage:imageRef2];
    CGImageRelease(imageRef2);
    return newImage;
}

-(void)updateTopImageViewRect:(CGRect)rect
{
    _topImageView.image = [self getTopImageFromNewRect:rect];
    
    CGFloat topImageY = rect.origin.y;
    CGFloat imageMarginHeight = 24.75;//图片上方留白尺寸

    if (isRetina || iPhone5)
    {
        topImageY = topImageY/2;
    }
    
    CGRect TopImageViewFrame = self.topImageView.frame;
    TopImageViewFrame.size.height = _topImageOriginHeight-topImageY;
    TopImageViewFrame.origin.y = imageMarginHeight+topImageY;
    self.topImageView.frame = TopImageViewFrame;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [_window release];
    [_topImageView release];
    [_timeLabel release];
    [super dealloc];
}

- (void)show
{
    [NSThread sleepForTimeInterval:0.1];
    [_window addSubview:self];
    _window.windowLevel = UIWindowLevelStatusBar;
    _window.hidden = NO;
}

- (void)dismiss
{
    [self removeFromSuperview];
    _window.windowLevel = UIWindowLevelNormal;
    _window.hidden = YES;
    
}

#pragma mark -
#pragma mark Manual oritentation change

#define RADIANS(degrees) ((degrees * (float)M_PI) / 180.0f)

- (void)deviceOrientationDidChange:(NSNotification *)notification { 
    if (!self.superview) {
        return;
    }
    
    if ([self.superview isKindOfClass:[UIWindow class]]) {
        [self transformForCurrentOrientation:YES];
    }
}

- (void)transformForCurrentOrientation:(BOOL)animated {
    UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
    NSInteger degrees = 0;
    
    // Stay in sync with the superview
    
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        if (orientation == UIInterfaceOrientationLandscapeLeft) { degrees = -90; } 
        else { degrees = 90; }
        // Window coordinates differ!
        self.bounds = CGRectMake(0, 0, self.bounds.size.height, self.bounds.size.width);
    } else {
        if (orientation == UIInterfaceOrientationPortraitUpsideDown) { degrees = 180; } 
        else { degrees = 0; }
    }
        
    if (animated) {
        [UIView beginAnimations:nil context:nil];
    }
    [self setTransform:CGAffineTransformMakeRotation(RADIANS(degrees))];
    if (animated) {
        [UIView commitAnimations];
    }
}

@end
