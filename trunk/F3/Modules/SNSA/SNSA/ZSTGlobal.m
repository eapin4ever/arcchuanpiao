//
//  ZSTGlobal.h
//  YouYun
//
//  Created by luobin on 6/6/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTGlobal.h"

NSDictionary* nameAttributes()
{
    static NSDictionary *dictionary = nil;
    if (dictionary == nil) {
        UIFont *font = [UIFont systemFontOfSize:15];
        CTFontRef ctFont = CTFontCreateWithName((CFStringRef) font.fontName, font.pointSize, NULL); 
        
        CTTextAlignment alignment = kCTLeftTextAlignment;
        CGFloat paragraphSpacing = 1.0;
        CTLineBreakMode lineBreakMode = kCTLineBreakByCharWrapping;
        CTParagraphStyleSetting settings[] = {
            {kCTParagraphStyleSpecifierAlignment, sizeof(alignment), &alignment},						// Justified Text Alignment
            {kCTParagraphStyleSpecifierParagraphSpacing, sizeof(paragraphSpacing), &paragraphSpacing},	// 1.0 Paragraph Spacing
            {kCTParagraphStyleSpecifierLineBreakMode, sizeof(lineBreakMode), &lineBreakMode}            // line break
        };
        CTParagraphStyleRef paragraphStyle = CTParagraphStyleCreate(settings, sizeof(settings) / sizeof(settings[0]));
        
        dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:(id)ctFont, (NSString *)kCTFontAttributeName, (id)[UIColor colorWithRed:0.14 green:0.43 blue:0.53 alpha:1].CGColor, kCTForegroundColorAttributeName, (id)paragraphStyle, kCTParagraphStyleAttributeName, nil];
    }
    return dictionary;
}

NSDictionary* smallNameAttributes()
{
    static NSDictionary *dictionary = nil;
    if (dictionary == nil) {
        UIFont *font = [UIFont systemFontOfSize:13];
        CTFontRef ctFont = CTFontCreateWithName((CFStringRef) font.fontName, font.pointSize, NULL); 
        
        CTTextAlignment alignment = kCTLeftTextAlignment;
        CGFloat paragraphSpacing = 1.0;
        CTLineBreakMode lineBreakMode = kCTLineBreakByCharWrapping;
        CTParagraphStyleSetting settings[] = {
            {kCTParagraphStyleSpecifierAlignment, sizeof(alignment), &alignment},						// Justified Text Alignment
            {kCTParagraphStyleSpecifierParagraphSpacing, sizeof(paragraphSpacing), &paragraphSpacing},	// 1.0 Paragraph Spacing
            {kCTParagraphStyleSpecifierLineBreakMode, sizeof(lineBreakMode), &lineBreakMode}            // line break
        };
        CTParagraphStyleRef paragraphStyle = CTParagraphStyleCreate(settings, sizeof(settings) / sizeof(settings[0]));
        
        dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:(id)ctFont, (NSString *)kCTFontAttributeName, (id)[UIColor colorWithRed:0.14 green:0.43 blue:0.53 alpha:1].CGColor, kCTForegroundColorAttributeName, (id)paragraphStyle, kCTParagraphStyleAttributeName, nil];
    }
    return dictionary;
}

NSDictionary* contentAttributes()
{
    static NSDictionary *dictionary = nil;
    if (dictionary == nil) {
        UIFont *font = [UIFont systemFontOfSize:15];
        CTFontRef ctFont = CTFontCreateWithName((CFStringRef) font.fontName, font.pointSize, NULL); 
        
        CTTextAlignment alignment = kCTLeftTextAlignment;
        CGFloat paragraphSpacing = 1.0;
        CTLineBreakMode lineBreakMode = kCTLineBreakByCharWrapping;
        CTParagraphStyleSetting settings[] = {
            {kCTParagraphStyleSpecifierAlignment, sizeof(alignment), &alignment},						// Justified Text Alignment
            {kCTParagraphStyleSpecifierParagraphSpacing, sizeof(paragraphSpacing), &paragraphSpacing},	// 1.0 Paragraph Spacing
            {kCTParagraphStyleSpecifierLineBreakMode, sizeof(lineBreakMode), &lineBreakMode}            // line break
        };
        CTParagraphStyleRef paragraphStyle = CTParagraphStyleCreate(settings, sizeof(settings) / sizeof(settings[0]));
        
        dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:(id)ctFont, (NSString *)kCTFontAttributeName, (id)[UIColor blackColor].CGColor, kCTForegroundColorAttributeName, (id)paragraphStyle, kCTParagraphStyleAttributeName, nil];
    }
    return dictionary;
}

NSDictionary* smallContentAttributes()
{
    static NSDictionary *dictionary = nil;
    if (dictionary == nil) {
        UIFont *font = [UIFont systemFontOfSize:13];
        CTFontRef ctFont = CTFontCreateWithName((CFStringRef) font.fontName, font.pointSize, NULL); 
        
        CTTextAlignment alignment = kCTLeftTextAlignment;
        CGFloat paragraphSpacing = 1.0;
        CTLineBreakMode lineBreakMode = kCTLineBreakByCharWrapping;
        CTParagraphStyleSetting settings[] = {
            {kCTParagraphStyleSpecifierAlignment, sizeof(alignment), &alignment},						// Justified Text Alignment
            {kCTParagraphStyleSpecifierParagraphSpacing, sizeof(paragraphSpacing), &paragraphSpacing},	// 1.0 Paragraph Spacing
            {kCTParagraphStyleSpecifierLineBreakMode, sizeof(lineBreakMode), &lineBreakMode}            // line break
        };
        CTParagraphStyleRef paragraphStyle = CTParagraphStyleCreate(settings, sizeof(settings) / sizeof(settings[0]));
        
        dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:(id)ctFont, (NSString *)kCTFontAttributeName, (id)[UIColor blackColor].CGColor, kCTForegroundColorAttributeName, (id)paragraphStyle, kCTParagraphStyleAttributeName, nil];
    }
    return dictionary;
}

NSDictionary* timeAttributes()
{
    static NSDictionary *dictionary = nil;
    if (dictionary == nil) {
        UIFont *font = [UIFont systemFontOfSize:12];
        CTFontRef ctFont = CTFontCreateWithName((CFStringRef) font.fontName, font.pointSize, NULL); 
        
        CTTextAlignment alignment = kCTLeftTextAlignment;
        CGFloat paragraphSpacing = 1.0;
        CTLineBreakMode lineBreakMode = kCTLineBreakByCharWrapping;
        CTParagraphStyleSetting settings[] = {
            {kCTParagraphStyleSpecifierAlignment, sizeof(alignment), &alignment},						// Justified Text Alignment
            {kCTParagraphStyleSpecifierParagraphSpacing, sizeof(paragraphSpacing), &paragraphSpacing},	// 1.0 Paragraph Spacing
            {kCTParagraphStyleSpecifierLineBreakMode, sizeof(lineBreakMode), &lineBreakMode}            // line break
        };
        CTParagraphStyleRef paragraphStyle = CTParagraphStyleCreate(settings, sizeof(settings) / sizeof(settings[0]));
        
        dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:(id)ctFont, (NSString *)kCTFontAttributeName, (id)[UIColor colorWithWhite:0.7 alpha:1].CGColor, kCTForegroundColorAttributeName, (id)paragraphStyle, kCTParagraphStyleAttributeName, nil];
    }
    return dictionary;
}

NSDictionary* smallTimeAttributes()
{
    static NSDictionary *dictionary = nil;
    if (dictionary == nil) {
        UIFont *font = [UIFont systemFontOfSize:11];
        CTFontRef ctFont = CTFontCreateWithName((CFStringRef) font.fontName, font.pointSize, NULL); 
        
        CTTextAlignment alignment = kCTLeftTextAlignment;
        CGFloat paragraphSpacing = 1.0;
        CTLineBreakMode lineBreakMode = kCTLineBreakByCharWrapping;
        CTParagraphStyleSetting settings[] = {
            {kCTParagraphStyleSpecifierAlignment, sizeof(alignment), &alignment},						// Justified Text Alignment
            {kCTParagraphStyleSpecifierParagraphSpacing, sizeof(paragraphSpacing), &paragraphSpacing},	// 1.0 Paragraph Spacing
            {kCTParagraphStyleSpecifierLineBreakMode, sizeof(lineBreakMode), &lineBreakMode}            // line break
        };
        CTParagraphStyleRef paragraphStyle = CTParagraphStyleCreate(settings, sizeof(settings) / sizeof(settings[0]));
        
        dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:(id)ctFont, (NSString *)kCTFontAttributeName, (id)[UIColor colorWithWhite:0.7 alpha:1].CGColor, kCTForegroundColorAttributeName, (id)paragraphStyle, kCTParagraphStyleAttributeName, nil];
    }
    return dictionary;
}

