//
//  TagUITapGestureRecognizer.m
//  YouYun
//
//  Created by admin on 12-10-9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTTagUITapGestureRecognizer.h"

@implementation ZSTTagUITapGestureRecognizer

@synthesize tag;
@synthesize imageView;
@synthesize fileName;

- (void)dealloc
{
    self.imageView = nil;
    self.fileName = nil;
    [super dealloc];
}
@end
