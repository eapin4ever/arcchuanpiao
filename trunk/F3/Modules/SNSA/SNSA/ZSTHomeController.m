//
//  YouYunViewController.m
//  YouYun
//  
//  Created by luobin on 5/28/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//  

#import "ZSTHomeController.h"

#import "ZSTGroupChatroomController.h"
#import "ZSTGroupAddressBookController.h"
#import "ZSTMyProfileController.h"
#import "ZSTCircleEmptyTableViewCell.h"
#import "ZSTWebViewController.h"
#import "ZSTSNSAShowAllCircleViewController.h"
#import "ZSTSNSARegisterViewController.h"
#import "ZSTUtils.h"
#import "ZSTChatRoomViewController.h"
#import "ZSTLastListTableViewCell.h"
#import "ZSTAgreementViewController.h"
#import "BaseNavgationController.h"
#import "ZSTMineViewController.h"

@interface ZSTHomeController()<UIActionSheetDelegate, ZSTYouYunEngineDelegate, EGORefreshTableHeaderDelegate>

@property (nonatomic, retain) NSMutableArray *circles;

@property (nonatomic, retain) ZSTYouYunEngine *engine;

- (void)login;

- (void)autoRefresh;

- (void)refreshCirles;


@end

@implementation ZSTHomeController

@synthesize engine;
@synthesize circles;

- (void)moduleApplication:(ZSTModuleApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [application.dao createTableIfNotExistForSnsbModule];
}

- (void)dealloc
{
    TKRELEASE(_refreshHeaderView);
    self.circles = nil;
    self.engine = nil;
    [self removeObserver];
    [super dealloc];
}

- (void)autoRefresh
{
    [_refreshHeaderView autoRefreshOnScroll:_circleTableView animated:YES];
}


- (void)setttingButtonAction
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"取消",@"取消") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"我的名片", nil), nil];
    [actionSheet showFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
    [actionSheet release];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        ZSTMyProfileController *myProfileController = [[ZSTMyProfileController alloc] init];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:myProfileController];
        [self presentViewController:navController animated:YES completion:nil];
        [navController release];
        [myProfileController release];
        
    } else if (buttonIndex == 1) {
//        ZSTWebViewController *webViewController = [[ZSTWebViewController alloc] init];
//        [webViewController setURL:@"http://d.pengyouyun.cn/"];
//        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:webViewController];
//        webViewController.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"取消",@"取消") style:UIBarButtonItemStyleBordered target:navController action:@selector(dismissModalViewController)] autorelease];
//        [self presentViewController:navController animated:YES completion:nil];
//        [navController release];
//        [webViewController release];
    }
}

#pragma mark - ZSTYouYunEngineDelegate

- (void)requestDidFail:(NSError *)error method:(NSString *)method
{
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_circleTableView];
    _isRefreshing = NO;
}

- (void)getCirclesResponse:(NSArray *)theCircles
{
//    if (![self.circles isEqualToArray:theCircles]) {
    if (theCircles.count) {
        self.circles = [self calculateRows:theCircles];
        [TKDataCache setCacheData:self.circles andURLKey:[NSString stringWithFormat:@"%@_%@", ZSTHttpMethod_GetCircles, @(self.moduleType)]];
        [_circleTableView reloadData];
    }
    
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_circleTableView];
    _isRefreshing = NO;
}

#pragma mark --检查是否注册--
-(BOOL)checkRegister
{
//    [self.engine registerMsisdn:@"1313123456" verifyCode:@"12345"];
    return  YES;
}

- (UIBarButtonItem *) initWithSubviews
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(0, 0, 35, 35);
    [btn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(mineAction:) forControlEvents:UIControlEventTouchUpInside];
    
    btn.layer.cornerRadius = btn.frame.size.width / 2.0;
    btn.layer.borderColor = [UIColor clearColor].CGColor;
    btn.layer.masksToBounds = YES;
    
    NSString *path = nil;
    UIImage *image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
    NSDictionary *dic = [ZSTF3Preferences shared].avaterName;
    
    if (![ZSTF3Preferences shared].loginMsisdn || [ZSTF3Preferences shared].loginMsisdn.length == 0) {
        
        [ZSTF3Preferences shared].loginMsisdn = @"";
    }
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0) {
        
        image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
    } else {
        
        if (dic || [dic isKindOfClass:[NSDictionary class]]) {
            path = [dic valueForKey:[ZSTF3Preferences shared].loginMsisdn];
        }
        
        if (path && path.length > 0) {
            
            NSString *homeD = NSHomeDirectory();//获取Home路径
            NSString *pngFile = [path substringFromIndex:[path rangeOfString:@"tmp"].location];
            NSString *fileD = [homeD stringByAppendingPathComponent:pngFile];
            NSData *reader = [NSData dataWithContentsOfFile:fileD];
            image = [UIImage imageWithData:reader];
            
            if (!image) {
                
                image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
            }
            
        } else {
            
            image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
        }
    }
    
    [btn setImage:image forState:UIControlStateNormal];
    
    //暂时解决btn图片不能显示的问题
    UIImageView *iv = [[UIImageView alloc] initWithFrame:btn.imageView.frame];
    iv.image = image;
    [btn insertSubview:iv aboveSubview:btn.imageView];
    
    return [[[UIBarButtonItem alloc] initWithCustomView:btn] autorelease];
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{    
    [super viewDidLoad];
    
    _canDel = YES;
    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed: @"framework_top_bg.png"];
	// Do any additional setup after loading the view, typically from a nib.
//    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"名片",nil) target:self selector:@selector (setttingButtonAction)];
    
    _circleTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 460+(iPhone5?88:0)+(IS_IOS_7?20:0)) style:UITableViewStylePlain];
    //0 37 320 480-44-49-37-20
    _circleTableView.delegate = self;
    _circleTableView.dataSource = self;
    _circleTableView.backgroundColor = [UIColor whiteColor];
    _circleTableView.separatorStyle =  UITableViewCellSeparatorStyleNone;
    _circleTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview: _circleTableView];
    
    self.engine = [ZSTYouYunEngine engineWithDelegate:self];
    self.engine.moduleType = self.moduleType;
    _isRefreshing = NO;
    
    if ([TKKeychainUtils getUserid:userKeyChainIdentifier]) {
        self.circles = [TKDataCache getCacheDataByURLKey:[NSString stringWithFormat:@"%@_%@", ZSTHttpMethod_GetCircles, @(self.moduleType)]];
    }
    
    if (_refreshHeaderView == nil) {
        
        _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f-250.0, self.view.frame.size.width, 250.0)];
        _refreshHeaderView.layer.contents = (id) ZSTModuleImage(@"module_snsa_pulltorefresh_background.png").CGImage;

        _refreshHeaderView.delegate = self;
        [_circleTableView addSubview:_refreshHeaderView];
        [_refreshHeaderView refreshLastUpdatedDate];
        [self performSelector:@selector(autoRefresh) 
                   withObject:nil 
                   afterDelay:0.6];
    }
    
    UIImageView *shadow = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_snsa_top_column_shadow.png")];
    shadow.frame = CGRectMake(0, 0, 320, -4);
    [_circleTableView addSubview:shadow];
    [shadow release];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCirles) name:kNotificationLoginSucsess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(login) name:kNotificationLogOutSuccess object:nil];
    
    [self registerObserver];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
//    if (![[NSUserDefaults standardUserDefaults] valueForKey:FIRST_LAUNCH_SNSA]) {
//    
//        ZSTAgreementViewController *controller = [[ZSTAgreementViewController alloc] init];
//        controller.source = YES;
//        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
//        [self presentViewController:navController animated:YES completion:nil];
//        [navController release];
//        [controller release];
//        [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:FIRST_LAUNCH_SNSA];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//    }
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0)
    {
        ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
        controller.isFromSetting = YES;
        controller.delegate = self;
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
            
//            [self.navigationController popViewControllerAnimated:YES];
        }];
        [navicontroller release];
        [controller release];
        
        return;
    }
    
    //判断是不是注销过再重新登录
    if ([ZSTF3Preferences shared].isChange)
    {
        [self autoRefresh];
        [ZSTF3Preferences shared].isChange = NO;
    }
    
    self.navigationItem.rightBarButtonItem = [self initWithSubviews];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)refreshCirles
{
    self.circles = nil;
    [_circleTableView reloadData];
    
    [self performSelector:@selector(autoRefresh) 
               withObject:nil 
               afterDelay:0.6];
}

// 注册观察者
-(void)registerObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ChangeMessageCount:) name:[NSString stringWithFormat:@"%@_%@", @"ZSTSNSMessageCount", @(self.moduleType)] object:nil];
}

- (void) ChangeMessageCount:(NSNotification*)notify 
{
    NSMutableDictionary *dic = [circles objectAtIndex:_row];
    [dic setObject:@"0" forKey:@"NotLookMsg"];
    [TKDataCache setCacheData:circles andURLKey:[NSString stringWithFormat:@"%@_%@", ZSTHttpMethod_GetCircles, @(self.moduleType)]];
    
    [_circleTableView reloadData];
}

// 移除注册的观察者
-(void)removeObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[NSString stringWithFormat:@"%@_%@", @"ZSTSNSMessageCount", @(self.moduleType)] object:nil];
}

- (void)login
{
//    ZSTInputPhoneNumberController *registrationController = [[ZSTInputPhoneNumberController alloc] init];
//    [self presentModalViewController:[[[UINavigationController alloc] initWithRootViewController:registrationController] autorelease] animated:YES];
//    [registrationController release];    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)loginDidCancel
{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        if (![controller isKindOfClass:[self.rootViewController class]]) {
            
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
    NSNumber *shellId = [NSNumber numberWithInteger:[ZSTF3Preferences shared].shellId];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLoginCancel object:shellId];
}

- (void)loginFinish
{
    
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (self.circles == nil || ![self.circles count])? tableView.height : (139/2.0);
}
-(void)popViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row < self.circles.count) {
        
        ZSTChatRoomViewController *groupChatController = [[ZSTChatRoomViewController alloc] init];
        groupChatController.circle = [self.circles objectAtIndex:indexPath.row];
        groupChatController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:groupChatController animated:YES];
        [groupChatController release];
        
    } else if (self.circles.count == indexPath.row) {
        
        ZSTSNSAShowAllCircleViewController * showViewController = [[ZSTSNSAShowAllCircleViewController alloc]init];
        showViewController.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
        [self.navigationController pushViewController:showViewController animated:YES];
        [showViewController release];

    }
    
    _row = indexPath.row;
}

-(NSMutableArray *)calculateRows:(NSArray *) arr
{
    NSMutableArray * mutableArr = [NSMutableArray array];
    //    NSInteger count = mutableArr.count;
    for (int i = 0 ;i<arr.count;i++) {
        NSDictionary * dic = [arr objectAtIndex:i];
        if ([dic isKindOfClass:[NSDictionary class]]) {
            if (([[dic safeObjectForKey:@"AuditStatus"] integerValue] == 1) || ([[dic safeObjectForKey:@"DefaultCircle"] intValue] == 1)) {
                [mutableArr addObject:dic];
            }
        }
    }
    return mutableArr;
}

#pragma mark - UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (self.circles == nil || ![self.circles count])? 1 : 1 + [self.circles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == [self.circles count] && [self.circles count] > 0) {
        static NSString *identifier = @"ZSTLastListTableViewCell";
        ZSTLastListTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[ZSTLastListTableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        return  cell;
    }
    else if (self.circles == nil || ![self.circles count]) {
        
        static NSString *identifier = @"ZSTCircleEmptyTableViewCell";
        ZSTCircleEmptyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[[ZSTCircleEmptyTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
        }
        return cell;
        
    } else {
        
        static NSString *identifier = @"ZSTTableViewCellIdentifier";
        ZSTGroupTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[[ZSTGroupTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
            if (indexPath.row == 0)
            {
                cell.isDefault = YES;
            }
            [cell.addressBookButton addTarget:self action:@selector(addressBookAction:) forControlEvents:UIControlEventTouchUpInside];
            
        }
        cell.addressBookButton.tag = indexPath.row;
        cell.circle = [circles objectAtIndex:indexPath.row];
        cell.selectedBackgroundView = [[[UIImageView alloc] initWithImage:[ZSTModuleImage(@"module_snsa_circle_item_bg_p.png") stretchableImageWithLeftCapWidth:0 topCapHeight:0]] autorelease];

        return cell;
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"退出";
}
//纵向滑动时响应，出现编辑按钮
- (void)tableView:(UITableView *)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"Start editing!");
    
}
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{        //纵向滑动时出现编辑按钮
    if (indexPath.row == 0) {
        
        return NO;
    }
    
    return YES;
}

//让行可以移动
-(BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
- (void)tableView:(UITableView *)_tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{      //编辑完响应事件
    
    if (indexPath.row >= 1) {
        NSString *circleId = [[self.circles objectAtIndex:indexPath.row] safeObjectForKey:@"CID"];
        [self.engine quitCircle:circleId];
    }
}


-(void)delBtnClicked
{
    _canDel = NO;
}
-(void)delBtnCancled
{
    _canDel = YES;
}
-(BOOL)isCanDel
{
    return _canDel;
}
-(void)delCircle:(id) obj
{
    UIButton * btn = (UIButton *) obj;
    NSDictionary * dic = [circles objectAtIndex:btn.tag];
    [self.engine quitCircle:[dic safeObjectForKey:@"CID"]];
    [self autoRefresh];
}

-(void)quitCircleResponse
{
//    _canDel = YES;
    
    [TKUIUtil hiddenHUD];
    [TKUIUtil showHUD:self.view withText:@"退出成功！"];
    [TKUIUtil hiddenHUDAfterDelay:2];
    [_circleTableView reloadData];
}
-(void)quitCircleFailed
{
//    _canDel = YES;
    [TKUIUtil hiddenHUD];
    [TKUIUtil showHUD:self.view withText:@"退出失败！"];
    [TKUIUtil hiddenHUDAfterDelay:2];
}
- (void)addressBookAction:(UIButton *)button
{
    NSDictionary *circle = [circles objectAtIndex:button.tag];
    if (circle!=nil) {
        NSString *defaultCircle = [circle safeObjectForKey:@"DefaultCircle"];
        if (defaultCircle!=nil && 1==[defaultCircle integerValue]) {
            return;
        }
    }
    
    ZSTGroupAddressBookController *groupAddressBookController = [[ZSTGroupAddressBookController alloc] init];
    groupAddressBookController.circle = [circles objectAtIndex:button.tag];
    groupAddressBookController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:groupAddressBookController animated:YES];
    [groupAddressBookController release];
}

#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

#pragma mark EGORefreshTableHeaderDelegate Methods
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view {
    _isRefreshing = YES;
    NSDictionary *circle = nil;
    
     NSMutableString * cInfo = [NSMutableString string];
    NSString *maxMsgID = nil;
    if ([circles count] == 0) {
        [cInfo appendString:[NSString stringWithFormat:@"%@:%@",@"",@""]];
    } else {
        for (int i = 0; i < [circles count]; i++) {
            circle = [circles objectAtIndex:i];
//            maxMsgID = [self.dao getMaxTopicID:[circle objectForKey:@"CID"]];
            NSString *circleID = [circle safeObjectForKey:@"CID"];
            maxMsgID = [[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"SNSA_TOPIC_MAXMSGID_%@",circleID]];
            if (maxMsgID != nil) {
                 [cInfo appendString:[NSString stringWithFormat:@"%@:%@",[circle safeObjectForKey:@"CID"],maxMsgID]];
                if (i < [circles count]-1) {
                    [cInfo appendString:@","];
                }
            } else {
                [cInfo appendString:[NSString stringWithFormat:@"%@:%@",[circle safeObjectForKey:@"CID"],@"0"]];
                if (i < [circles count]-1) {
                    [cInfo appendString:@","];
                }
            }
        }
    }
    [self.engine getCircles:cInfo];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView *)view {
    return _isRefreshing;
}

- (NSDate *)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView *)view {
    return [NSDate date];
}

- (void)mineAction:(UIButton *)sender
{
    ZSTMineViewController *controller = [[ZSTMineViewController alloc] initWithNibName:@"ZSTMineViewController" bundle:nil];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

@end
