//
//  ZSTTableViewCell.m
//  YouYun
//
//  Created by luobin on 5/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTGroupTableViewCell.h"
#import "TKAsynImageView.h"
#import "ZSTUnreadMessageCountLabel.h"

#define AVTARBGSIZE  ((109/2.0))
#define AVTARSIZE    ((101/2.0))

@implementation ZSTGroupTableViewCell

@synthesize avtarImageView;
@synthesize groupNameLabel;
@synthesize unreadMessageCountLabel;
@synthesize desLabel;
@synthesize circle;
@synthesize newsImage;
@synthesize addressBookSeparatorImageView;
@synthesize addressBookButton;
@synthesize horizontalImg;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
//        UIImageView *avtarbg = [[[UIImageView alloc] initWithFrame:CGRectMake(((139/2.0)-AVTARBGSIZE)/2.0, ((139/2.0)-AVTARBGSIZE)/2.0, AVTARBGSIZE, AVTARBGSIZE)] autorelease];
//        avtarbg.image = [ZSTModuleImage(@"module_snsa_icon_frame.png") stretchableImageWithLeftCapWidth:2 topCapHeight:2];
//        avtarbg.backgroundColor = [UIColor clearColor];
//        [self addSubview:avtarbg];
        
        
        
//        avtarbg.layer.cornerRadius = 5;
//        avtarbg.clipsToBounds = YES;(AVTARBGSIZE-AVTARSIZE)/2.0,(AVTARBGSIZE-AVTARSIZE)/2.0 
        
        self.avtarImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(((139/2.0)-AVTARBGSIZE)/2.0, ((139/2.0)-AVTARBGSIZE)/2.0, AVTARSIZE, AVTARSIZE)] autorelease];
        self.avtarImageView.userInteractionEnabled = NO;
        avtarImageView.defaultImage = ZSTModuleImage(@"module_snsa_default_circle.png");
        [self addSubview:avtarImageView];
        
        self.avtarImageView.layer.cornerRadius = 4;
        self.avtarImageView.clipsToBounds = YES;

      //  [self.avtarImageView setMasksToBounds:YES];
       // [self.avtarImageView setCornerRadius:5.0];
        
        
        self.groupNameLabel = [[[UILabel alloc] initWithFrame:CGRectMake(75, 16, 170, 14)] autorelease];
        self.groupNameLabel.backgroundColor = [UIColor clearColor];
        self.groupNameLabel.font = [UIFont systemFontOfSize:14];
        self.groupNameLabel.textColor = [UIColor colorWithRed:0.17 green:0.25 blue:0.32 alpha:1];
        self.groupNameLabel.highlightedTextColor = [UIColor whiteColor];
        self.groupNameLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:1];
        self.groupNameLabel.shadowOffset = CGSizeMake(0, 1.0f);
        [self addSubview:self.groupNameLabel];
        
        self.unreadMessageCountLabel = [[[ZSTUnreadMessageCountLabel alloc] initWithFrame:CGRectMake(50, 48, 37/2.0 , 18)] autorelease];
        self.unreadMessageCountLabel.backgroundColor = [UIColor clearColor];
        self.unreadMessageCountLabel.font = [UIFont systemFontOfSize:10];
        self.unreadMessageCountLabel.textColor = [UIColor whiteColor];
        self.unreadMessageCountLabel.highlightedTextColor = [UIColor whiteColor];
        self.unreadMessageCountLabel.textAlignment = NSTextAlignmentCenter;
        self.unreadMessageCountLabel.hidden = YES;
        [self addSubview:self.unreadMessageCountLabel];
        
        self.desLabel = [[[UILabel alloc] initWithFrame:CGRectMake(75, 34, 170, 30)] autorelease];
        self.desLabel.numberOfLines = 2;
        self.desLabel.backgroundColor = [UIColor clearColor];
        self.desLabel.font = [UIFont systemFontOfSize:12];
        self.desLabel.textColor = [UIColor colorWithWhite:0.667 alpha:1];
        self.desLabel.highlightedTextColor = [UIColor whiteColor];
        self.desLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:1];
        self.desLabel.shadowOffset = CGSizeMake(0, 1.0f);
        [self addSubview:self.desLabel];
        
        UIView *rightView = [[[UIView alloc] initWithFrame:CGRectMake(248, 0, 72, (139/2))] autorelease];
        
        self.addressBookSeparatorImageView = [[[UIImageView alloc] init] autorelease];
        if (TKIsRetina()) {
            self.addressBookSeparatorImageView.frame = CGRectMake(0, 0, 1, (139/2));
        } else {
            self.addressBookSeparatorImageView.frame = CGRectMake(0, 0, 2, (139/2));
        }
        self.addressBookSeparatorImageView.backgroundColor = [UIColor clearColor];
        self.addressBookSeparatorImageView.image = [ZSTModuleImage(@"module_snsa_circle_list_split.png") stretchableImageWithLeftCapWidth:0 topCapHeight:0];
        [rightView addSubview:addressBookSeparatorImageView];
        
        self.addressBookButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.addressBookButton.titleLabel.font = [UIFont systemFontOfSize:12];
        self.addressBookButton.backgroundColor = [UIColor clearColor];
        CGFloat right = self.addressBookSeparatorImageView.right;
        self.addressBookButton.frame = CGRectMake(right, 0, 72 - right, (139/2));
//        self.addressBookButton.imageEdgeInsets = UIEdgeInsetsMake(0, 16, 30, 16);

        [self.addressBookButton setImage:ZSTModuleImage(@"module_snsa_circle_list_contact_n.png") forState:UIControlStateNormal];
        [self.addressBookButton setImage:ZSTModuleImage(@"module_snsa_circle_list_contact_p.png") forState:UIControlStateHighlighted];

        [self.addressBookButton setTitleColor:RGBCOLOR(195, 195, 195) forState:UIControlStateNormal];
        [self.addressBookButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(right, 12, 72 - right, (80/2))];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.btnImageIV = imageView;
        imageView.image = ZSTModuleImage(@"module_snsa_public_circle_list_contact_n.png");
        [rightView addSubview:imageView];
        
        [rightView addSubview:self.addressBookButton];
        
        [self addSubview:rightView];
        
        self.horizontalImg = [[[UIImageView alloc] initWithFrame:CGRectMake(0, (139/2), CGRectGetWidth(self.frame), 1)] autorelease];
        self.horizontalImg.image = [ZSTModuleImage(@"module_snsa_horizontal_divider.png") stretchableImageWithLeftCapWidth:0 topCapHeight:0];
        [self addSubview:self.horizontalImg];
        
    }
    
    return self;
}

- (void)centerImageAndTitle:(float)spacing
{    
    // get the size of the elements here for readability
    CGSize imageSize = self.addressBookButton.imageView.frame.size;
    CGSize titleSize = self.addressBookButton.titleLabel.frame.size;
    
    // get the height they will take up as a unit
    CGFloat totalHeight = (imageSize.height + titleSize.height + spacing);
    
    // raise the image and push it right to center it
    self.addressBookButton.imageEdgeInsets = UIEdgeInsetsMake(
                                            - floor(totalHeight - imageSize.height), 0.0f , -10, - floor(titleSize.width));
    
    // lower the text and push it left to center it
    self.addressBookButton.titleEdgeInsets = UIEdgeInsetsMake(
                                            0.0, - floor(imageSize.width), - floor(totalHeight - titleSize.height) - 10, 0.0);
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGSize size = [self.unreadMessageCountLabel.text sizeWithFont:self.unreadMessageCountLabel.font constrainedToSize:CGSizeMake(30, 16) lineBreakMode:NSLineBreakByCharWrapping];
    
    
    size = [self.groupNameLabel.text sizeWithFont:self.groupNameLabel.font constrainedToSize:CGSizeMake(156 - size.width, 16) lineBreakMode:NSLineBreakByCharWrapping];
    self.groupNameLabel.width = size.width;
    
    if (!self.isDefault)
    {
        [self.btnImageIV setImage:ZSTModuleImage(@"module_snsa_circle_list_contact_n.png")];
    }
    else
    {
        [self.btnImageIV setImage:ZSTModuleImage(@"module_snsa_public_circle_list_contact_n.png")];
    }
    
    [self centerImageAndTitle:4.f];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    
    self.addressBookButton.highlighted = NO;
    self.addressBookSeparatorImageView.hidden = highlighted;
}

- (void)setCircle:(NSDictionary *)theCircle
{
    if (circle != theCircle) {
        [circle release];
        circle = [theCircle retain];
        
      //  [self.avtarImageView clear];
        NSString *imageURL = [circle safeObjectForKey:@"CImgUrl"];
        if (imageURL == nil) {
            imageURL = @"";
        }
                
         self.avtarImageView.url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", imageURL]];
        [self.avtarImageView loadImage];
        
        self.groupNameLabel.text = [circle objectForKey:@"CName"];
        self.desLabel.text = [circle safeObjectForKey:@"Description"];
        //tableViewCell 人数
        [self.addressBookButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"%d", nil), [[circle safeObjectForKey:@"UserCount"] intValue]] forState:UIControlStateNormal];
        [self.addressBookButton setTitle:[NSString stringWithFormat:NSLocalizedString(@"%d", nil), [[circle safeObjectForKey:@"UserCount"] intValue]] forState:UIControlStateHighlighted];
        [self.addressBookButton setTitleColor:RGBCOLOR(37, 160, 45) forState:UIControlStateHighlighted];
        
        NSString *defaultCircle = [circle safeObjectForKey:@"DefaultCircle"];
        if (defaultCircle!=nil && 0==[defaultCircle integerValue]) {

            [self.addressBookButton setImage:ZSTModuleImage(@"module_snsa_circle_list_contact_n.png") forState:UIControlStateNormal];
            [self.addressBookButton setImage:ZSTModuleImage(@"module_snsa_circle_list_contact_p.png") forState:UIControlStateHighlighted];
            [self.addressBookButton setTitleColor:RGBCOLOR(37, 160, 45) forState:UIControlStateHighlighted];
        } else {
            [self.addressBookButton setImage:ZSTModuleImage(@"module_snsa_public_circle_list_contact_n.png") forState:UIControlStateNormal];
            [self.addressBookButton setImage:ZSTModuleImage(@"module_snsa_public_circle_list_contact_p.png") forState:UIControlStateHighlighted];
            [self.addressBookButton setTitleColor:RGBCOLOR(37, 160, 45) forState:UIControlStateHighlighted];
        }
        
        [self setNeedsLayout];
    }
    
    if ([[circle safeObjectForKey:@"NotLookMsg"] intValue] > 0) {
        self.unreadMessageCountLabel.hidden = NO;
        self.unreadMessageCountLabel.text = [NSString stringWithFormat:@"%d", [[circle safeObjectForKey:@"NotLookMsg"] intValue]];
        if ([[circle safeObjectForKey:@"NotLookMsg"] intValue] > 99) {
            self.unreadMessageCountLabel.text = @"...";
        }
    } else {
        self.unreadMessageCountLabel.hidden = YES;
    }

}


- (void)drawRect:(CGRect)rect
{
   // [[ZSTModuleImage(@"module_snsa_circle_item_bg_n.png") stretchableImageWithLeftCapWidth:0 topCapHeight:0] drawInRect:rect];
}

- (void)dealloc
{
    self.addressBookSeparatorImageView = nil;
    self.addressBookButton = nil;
    self.avtarImageView = nil;
    self.groupNameLabel = nil;
    self.newsImage = nil;
    self.unreadMessageCountLabel = nil;
    self.desLabel = nil;
    self.circle = nil;
    self.horizontalImg = nil;
    [super dealloc];
}

@end
