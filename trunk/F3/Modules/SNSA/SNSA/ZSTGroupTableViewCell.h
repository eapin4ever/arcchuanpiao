//
//  ZSTTableViewCell.h
//  YouYun
//
//  Created by luobin on 5/29/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TKAsynImageView;
@class ZSTUnreadMessageCountLabel;

@interface ZSTGroupTableViewCell : UITableViewCell

@property (nonatomic, retain) TKAsynImageView *avtarImageView;

@property (nonatomic, retain) UILabel *groupNameLabel;

@property (nonatomic, retain) UIImageView *newsImage;

@property (nonatomic, retain) ZSTUnreadMessageCountLabel *unreadMessageCountLabel;

@property (nonatomic, retain) UILabel *desLabel;

@property (nonatomic, retain) UIButton *addressBookButton;

@property (nonatomic, retain) UIImageView *addressBookSeparatorImageView;

@property (nonatomic, retain) UIImageView *horizontalImg;

@property (nonatomic, retain) NSDictionary *circle;

@property (nonatomic,assign) BOOL isDefault;

@property (nonatomic,strong) UIImageView *btnImageIV;

@end
