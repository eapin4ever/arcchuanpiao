
#import "ZSTDao+SNSA.h"
#import "ZSTSqlManager.h"
#import "TKUtil.h"

#define CREATE_MYFAVORITEINFO_CMD(moduleType) [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [Z_MESSAGE_%@] (\
                                                                            id INTEGER PRIMARY KEY AUTOINCREMENT,\
                                                                            MID  VARCHAR(50) unique,\
                                                                            CID VARCHAR(50),\
                                                                            UID VARCHAR(100) ,\
                                                                            MContent VARCHAR,\
                                                                            ImgUrl VARCHAR(100),\
                                                                            AddTime double \
                                                                            );", @(moduleType)]
#define CREATE_TABLE_TOPIC_CMD(moduleType)   [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [Z_TOPIC_%@] (\
                                                                            ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                            TopicID int unique, \
                                                                            AddTime double DEFAULT (getdate()), \
                                                                            ImgUrl VARCHAR(100), \
                                                                            TopicContent VARCHAR,\
                                                                            CircleId INTEGER,\
                                                                            UserId INTEGER ,\
                                                                            UserAvatar  VARCHAR(100),\
                                                                            UserName VARCHAR(50) \
                                                                            );", @(moduleType)]
#define CREATE_TABLE_COMMENTS_CMD(moduleType)  [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [Z_COMMENTS_%@] (\
                                                                            ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                            CommentId  int unique,\
                                                                            TopicID int , \
                                                                            AddTime double DEFAULT (getdate()), \
                                                                            ImgUrl VARCHAR(100), \
                                                                            CommentContent VARCHAR,\
                                                                            CircleId INTEGER,\
                                                                            UserId INTEGER ,\
                                                                            UserAvatar  VARCHAR(100),\
                                                                            UserName VARCHAR(50) \
                                                                            );", @(moduleType)]

@implementation ZSTDao(SNSA)

- (void)createTableIfNotExistForSnsbModule {
    [ZSTSqlManager executeUpdate:CREATE_MYFAVORITEINFO_CMD(self.moduleType)];
    [ZSTSqlManager executeUpdate:CREATE_TABLE_TOPIC_CMD(self.moduleType)];
    [ZSTSqlManager executeUpdate:CREATE_TABLE_COMMENTS_CMD(self.moduleType)];
//    [self addcolumnTableWithName:@"VideoUrl"];
    [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"alter table Z_TOPIC_%@ add column %@ VARCHAR  DEFAULT ''",@(self.moduleType),@"VideoUrl"]];
}

//- (BOOL) addcolumnTableWithName:(NSString *)name
//{
//    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"alter table Z_TOPIC_%@ add column %@ VARCHAR  DEFAULT ''",@(self.moduleType),name]];
//    return success;
//}

- (BOOL)addTopic:(NSInteger)topicId
         addTime:(NSDate *)time
          imgUrl:(NSString *)imgUrl
        videoUrl:(NSString *)videoUrl
         content:(NSString *)topicContent
        circleId:(NSInteger)circleId
          userId:(NSInteger)userId
      userAvatar:(NSString *)userAvatar
        userName:(NSString *)userName
{
    NSString *querySql = [NSString stringWithFormat:@"INSERT OR REPLACE INTO Z_TOPIC_%@ (TopicID, AddTime, ImgUrl, VideoUrl, TopicContent, CircleId, UserId, UserAvatar,                                UserName) VALUES (?,?,?,?,?,?,?,?,?);", @(self.moduleType)];
    BOOL value = [ZSTSqlManager executeUpdate:querySql,
                  [NSNumber numberWithInteger:topicId], 
                  [NSNumber numberWithDouble:[time timeIntervalSince1970]],
                  [TKUtil wrapNilObject:imgUrl],
                  [TKUtil wrapNilObject:videoUrl],
                  [TKUtil wrapNilObject:topicContent], 
                  [NSNumber numberWithInteger:circleId], 
                  [NSNumber numberWithInteger:userId], 
                  [TKUtil wrapNilObject:userAvatar], 
                  [TKUtil wrapNilObject:userName]
                  ];
    
    return value;
}

- (BOOL)addComment:(NSInteger)commentId
           topicId:(NSInteger)topicId
           addTime:(NSDate *)time
            imgUrl:(NSString *)imgUrl
    commentContent:(NSString *)commentContent
          circleId:(NSInteger)circleId
            userId:(NSInteger)userId
        userAvatar:(NSString *)userAvatar
          userName:(NSString *)userName
{
    NSString *querySql = [NSString stringWithFormat:@"INSERT OR REPLACE INTO Z_COMMENTS_%@ (CommentId, TopicID, AddTime, ImgUrl, CommentContent, CircleId, UserId, UserAvatar, UserName) VALUES (?,?,?,?,?,?,?,?,?);", @(self.moduleType)];
    BOOL value = [ZSTSqlManager executeUpdate:querySql,
                  [NSNumber numberWithInteger:commentId], 
                  [NSNumber numberWithInteger:topicId], 
                  [NSNumber numberWithDouble:[time timeIntervalSince1970]],
                  [TKUtil wrapNilObject:imgUrl], 
                  [TKUtil wrapNilObject:commentContent], 
                  [NSNumber numberWithInteger:circleId], 
                  [NSNumber numberWithInteger:userId], 
                  [TKUtil wrapNilObject:userAvatar], 
                  [TKUtil wrapNilObject:userName]
                  ];
    
    return value;
}

- (BOOL)addMessage:(NSString *)mID
          circleID:(NSString *)circleID
            userID:(NSString *)userID
           content:(NSString *)content
            imgUrl:(NSString *)imgUrl
           addTime:(NSDate *)time
{
    BOOL value = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO Z_MESSAGE_%@ (MID, CID, UID, MContent, ImgUrl, AddTime) VALUES (?,?,?,?,?,?);", @(self.moduleType)],
                  [TKUtil wrapNilObject:mID], [TKUtil wrapNilObject:circleID], [TKUtil wrapNilObject:userID], [TKUtil wrapNilObject:content], [TKUtil wrapNilObject:imgUrl], [NSNumber numberWithDouble:[time timeIntervalSince1970]]];
    
    if (!value) NSLog(@"insert favorite error..");
    return value;
}

- (BOOL)deleteMessage:(NSString *)mID
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM Z_MESSAGE_%@ WHERE mID = ?", @(self.moduleType)];
    if (![ZSTSqlManager executeUpdate:deleteSQL arguments:[NSArray arrayWithObjects:[TKUtil wrapNilObject:mID], nil]]) {
        NSLog(@"delete favorite error.. ID is: %@", mID);
        return NO;
    }
    return YES;
}

- (BOOL)messageExist:(NSString *)mID;
{
    NSUInteger count = 0;
    NSString *querySql = [NSString stringWithFormat:@"SELECT COUNT(ID) as cnt FROM Z_MESSAGE_%@ where mID = ?", @(self.moduleType)];
    NSArray *results = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:[TKUtil wrapNilObject:mID], nil]];
    for (id value in results) {
        count = [[value safeObjectForKey:@"cnt"] unsignedIntegerValue];
    }
    return count;
}

- (NSDictionary *)messagesOfCircle:(NSString *)circleID atIndex:(int)index
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, MID, CID, UID, MContent, ImgUrl, AddTime FROM Z_MESSAGE_%@ where CID = ? ORDER BY AddTime desc limit ?,?", @(self.moduleType)];
    NSArray *results = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:
                                                            [TKUtil wrapNilObject:circleID], 
                                                            [NSNumber numberWithInt:index], 
                                                            [NSNumber numberWithInt:1], 
                                                            nil]];
    if ([results count]) {
        return [results objectAtIndex:0];
    }
    return nil;
}

- (NSArray *)messagesOfCircle:(NSString *)circleID  atPage:(int)pageNum pageCount:(int)pageCount
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, MID, CID, UID, MContent, ImgUrl, AddTime FROM Z_MESSAGE_%@ where CID = ? ORDER BY AddTime desc limit ?,?", @(self.moduleType)];
    NSArray *results = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:
                                                            [TKUtil wrapNilObject:circleID], 
                                                            [NSNumber numberWithInt:(pageNum-1)*pageCount], 
                                                            [NSNumber numberWithInt:pageNum], 
                                                            nil]];
    return results;
}

- (NSUInteger)messagesCountOfCircle:(NSString *)circleID
{
    NSUInteger count = 0;
    NSArray *results = [ZSTSqlManager executeQuery:[NSString stringWithFormat:@"SELECT COUNT(ID) as cnt FROM Z_MESSAGE_%@ where CID = ?", @(self.moduleType)] arguments:[NSArray arrayWithObjects:[TKUtil wrapNilObject:circleID], nil]];
    for (id value in results) {
        count = [[value safeObjectForKey:@"cnt"] unsignedIntegerValue];
    }
    return count;
}

- (NSString *) getMaxTopicID:(NSString *)circleID
{
	//不能用max(mblogid)，因为mblogid ascii排序有问题
    NSArray *results = [ZSTSqlManager executeQuery:[NSString stringWithFormat:@"select TopicID from Z_TOPIC_%@ where CircleId = ? ORDER BY TopicID desc limit 1", @(self.moduleType)] arguments:[NSArray arrayWithObjects:[TKUtil wrapNilObject:circleID], nil]];
    if ([results count] > 0) {
        return [[results lastObject] safeObjectForKey:@"TopicID"];
    }
    return nil;
}

- (NSArray *)topicAndCommentOfCircle:(NSString *)circleId 
                             startId:(NSInteger)startTopic
                          topicCount:(NSInteger)topicCount
                        commentCount:(NSInteger)commentCount
{
    NSArray *topics = nil;
    if (startTopic == 0) {
        NSString *querySql = [NSString stringWithFormat:@"SELECT TopicID, AddTime, ImgUrl, TopicContent, CircleId, UserId, UserAvatar, UserName FROM Z_TOPIC_%@ WHERE CircleId=?  ORDER BY TopicID desc limit ?", @(self.moduleType)];
        topics = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:
                                                      [NSNumber numberWithInteger:[circleId integerValue]],
                                                      [NSNumber numberWithInteger:topicCount],
                                                      nil
                                                      ]];
    } else {
        NSString *querySql = [NSString stringWithFormat:@"SELECT TopicID, AddTime, ImgUrl, TopicContent, CircleId, UserId, UserAvatar, UserName FROM Z_TOPIC_%@ WHERE TopicID<?   AND CircleId=? ORDER BY TopicID desc limit ?", @(self.moduleType)];
        topics = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:
                                                      [NSNumber numberWithInteger:startTopic],
                                                      [NSNumber numberWithInteger:[circleId integerValue]],
                                                      [NSNumber numberWithInteger:topicCount],
                                                      nil
                                                      ]];
    }
    NSMutableArray *topicArray = [NSMutableArray array];
    for (NSDictionary *topic in topics) {
        //读取主题
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setSafeObject:[topic safeObjectForKey:@"TopicID"] forKey:@"MID"];
        [params setSafeObject:[topic safeObjectForKey:@"AddTime"] forKey:@"AddTime"];
        [params setSafeObject:[topic safeObjectForKey:@"ImgUrl"] forKey:@"ImgUrl"];
        [params setSafeObject:[topic safeObjectForKey:@"TopicContent"] forKey:@"MContent"];
        [params setSafeObject:[topic safeObjectForKey:@"CircleId"] forKey:@"CircleId"];
        [params setSafeObject:[topic safeObjectForKey:@"UserId"] forKey:@"UID"];
        [params setSafeObject:[topic safeObjectForKey:@"UserAvatar"] forKey:@"UAvatar"];
        [params setSafeObject:[topic safeObjectForKey:@"UserName"] forKey:@"UName"];
        
        //读取评论
        NSString *commentQuery = [NSString stringWithFormat:@"SELECT CommentId, TopicID, AddTime, ImgUrl, CommentContent, CircleId, UserId, UserAvatar, UserName FROM Z_COMMENTS_%@ WHERE TopicID=? ORDER BY CommentId desc limit ?", @(self.moduleType)];
        NSArray *comments = [ZSTSqlManager executeQuery:commentQuery arguments:[NSArray arrayWithObjects:
                                                                     [topic objectForKey:@"TopicID"],
                                                                     [NSNumber numberWithInteger:commentCount],
                                                                     nil
                                                                     ]];
        NSMutableArray *commentsArray = [NSMutableArray array];
        for (NSDictionary *comment in comments) {
            NSMutableDictionary *commentParams = [NSMutableDictionary dictionary];
            [commentParams setSafeObject:[comment safeObjectForKey:@"CommentId"] forKey:@"MID"];
            [commentParams setSafeObject:[comment safeObjectForKey:@"TopicID"] forKey:@"Parentid"];
            [commentParams setSafeObject:[comment safeObjectForKey:@"AddTime"] forKey:@"AddTime"];
            [commentParams setSafeObject:[comment safeObjectForKey:@"ImgUrl"] forKey:@"ImgUrl"];
            [commentParams setSafeObject:[comment safeObjectForKey:@"CommentContent"] forKey:@"MContent"];
            [commentParams setSafeObject:[comment safeObjectForKey:@"CircleId"] forKey:@"CircleId"];
            [commentParams setSafeObject:[comment safeObjectForKey:@"UserId"] forKey:@"UID"];
            [commentParams setSafeObject:[comment safeObjectForKey:@"UserAvatar"] forKey:@"UAvatar"];
            [commentParams setSafeObject:[comment safeObjectForKey:@"UserName"] forKey:@"UName"];
            
            [commentsArray insertObject:commentParams atIndex: 0];
        }
        NSInteger commentsCount = [commentsArray count];
        [params setSafeObject:@(commentsCount).stringValue forKey:@"CmtCount"];
        [params setSafeObject:commentsArray forKey:@"Cmts"];
        
        [topicArray addObject:params];
    }
    
    return topicArray;
}

- (BOOL)deleteTopicOfCircle:(NSString *)circleId
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM Z_TOPIC_%@ WHERE CircleId=?", @(self.moduleType)];
    if (![ZSTSqlManager executeUpdate:deleteSQL, [NSNumber numberWithInteger:[circleId integerValue]]]) {
        return NO;
    }
    
    return YES;
}

- (BOOL)deleteCommentOfCircle: (NSString *)circleId
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM Z_COMMENTS_%@ WHERE CircleId=?", @(self.moduleType)];
    if (![ZSTSqlManager executeUpdate:deleteSQL, [NSNumber numberWithInteger:[circleId integerValue]]]) {
        return NO;
    }
    
    return YES;
}

- (BOOL)saveComments:(NSArray *)comments circle:(NSString *)circleId
{
    if (comments==nil || circleId==nil) {
        return NO;
    }

    [ZSTSqlManager beginTransaction];
    for (int i=0; i<[comments count]; i++) {
        NSDictionary *comment = [comments objectAtIndex:i];
        NSString *addTime = [comment safeObjectForKey:@"AddTime"];
        [self addComment:[[comment objectForKey:@"MID"] integerValue]
                                  topicId:[[comment safeObjectForKey:@"Parentid"] integerValue]
                                  addTime:[NSDate dateWithTimeIntervalSince1970:[addTime doubleValue]]
                                   imgUrl:[comment safeObjectForKey:@"ImgUrl"]
                           commentContent:[comment safeObjectForKey:@"MContent"]
                                 circleId:[circleId integerValue]
                                   userId:[[comment safeObjectForKey:@"UID"] integerValue]
                               userAvatar:[comment safeObjectForKey:@"UAvatar"]
                                 userName:[comment safeObjectForKey:@"UName"]
         ];
    }
    [ZSTSqlManager commit];
    
    return YES;
}

@end
