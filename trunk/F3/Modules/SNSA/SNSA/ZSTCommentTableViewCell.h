//
//  ZSTCommentTableViewCell.h
//  YouYun
//
//  Created by luobin on 6/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ChatBubble.h"
#import "ZSTChatBubble.h"
#import "ZSTTagUITapGestureRecognizer.h"
//#import "SpeexDecoder.h"
#import "ImageAudioPlayer.h"
//#import "VoiceConverter.h"
//#import "SREmojiConvertor.h"

@interface ZSTCommentTableViewCell : UITableViewCell <AVAudioPlayerDelegate>
{
//    TKAsynImageView *_avtarImageView;
    UIImageView *_avtarImageView;
    NIAttributedLabel *_label;
    ImageAudioPlayer *audioPlayer;
    UIView *lineView;
    
    UIView *soundView;
}

@property (nonatomic, retain) NIAttributedLabel *_label;
@property (nonatomic, retain) UIView *lineView;
//@property (nonatomic, assign) TKAsynImageView *avtarImageView;
@property (nonatomic,assign) UIImageView *avtarImageView;

@property (nonatomic, assign) NSDictionary *comment;

@property (nonatomic, assign) NSDictionary *circle;
@property (nonatomic, assign) ImageAudioPlayer *audioPlayer;

@property (nonatomic, retain) UIView *soundView;
//+ (NSAttributedString *)attributedStringFromComment:(NSDictionary *)comment;
+ (NSAttributedString *)attributedStringFromComment:(NSDictionary *)comment content:(NSString*)content;
+ (CGSize)suggestSizeForComment:(NSDictionary *)comment;

@end
