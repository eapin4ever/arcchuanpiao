//
//  ChatBubble.m
//  YouYun
//
//  Created by admin on 12-9-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ChatBubble.h"



@implementation ChatBubble

@synthesize messageLabel;
@synthesize avtarImageView;

- (id)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        self.avtarImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(15, 12, kCommentHeadWidth, kCommentHeadWidth)] autorelease];
         avtarImageView.defaultImage = ZSTModuleImage(@"module_snsa_default_avatar.png");
        avtarImageView.backgroundColor = [UIColor colorWithWhite:0.95 alpha:1];
//        avtarImageView.needCropper = YES;
        avtarImageView.userInteractionEnabled = YES;
        CALayer * avtarImageViewLayer = [avtarImageView layer];
        [avtarImageViewLayer setMasksToBounds:YES];
        [avtarImageViewLayer setCornerRadius:5.0];
        [self addSubview:avtarImageView];
        [avtarImageView clear];

        self.messageLabel = [[[NIAttributedLabel alloc] init] autorelease];
        messageLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:messageLabel];
    }
    return self;
}

- (void)dealloc
{
    self.messageLabel = nil;
    self.avtarImageView = nil;
    [super dealloc];
}

+ (CGSize)suggestSizeForAttributedString:(NSAttributedString *)attributedString;
{
    CFAttributedStringRef attributedStringRef = (CFAttributedStringRef)attributedString;
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString(attributedStringRef);
	
	CGSize newSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0, 0),
                                                                  NULL, CGSizeMake(kChatBubbleWidth, CGFLOAT_MAX), nil);
	if (nil != framesetter) {
        CFRelease(framesetter);
        framesetter = nil;
    }
    return CGSizeMake(kChatBubbleWidth, MAX(ceil(newSize.height), 36));
}

//+ (CGSize)suggestSizeForTopic:(NSDictionary *)topic font:(UIFont *)font
//{
//    CGFloat height = 0.f;
//    NSString *imageURL = [topic objectForKey:@"ImgUrl"];
//    
//    if (imageURL != nil && ![imageURL isEmptyOrWhitespace]) {
//        
//        height += 5;
//    }
//    
//    NSString *content = [topic objectForKey:@"MContent"];
//    content = [content stringByTrimmingTrailingWhitespaceAndNewlineCharacters];
//    if (content != nil && ![content isEmptyOrWhitespace]) {
//        
//        height += 10;
//        if (imageURL != nil && ![imageURL isEmptyOrWhitespace]) {
//            CGSize size = [ZSTChatBubble suggestSizeForAttributedString:[ZSTChatBubble smallAttributedStringFromComment:topic]];
//            height += size.height;
//            
//        } else {
//            
//            CGSize size = [ZSTChatBubble suggestSizeForAttributedString:[ZSTChatBubble attributedStringFromComment:topic]];
//            height += size.height;
//        }
//    }
//    
//    NSArray *comments = [topic objectForKey:@"Cmts"];
//    for ( int i = 0; i< [comments count]; i++ ) {
//        height += 10;
//        NSDictionary *comment = [comments objectAtIndex:i];
//        CGSize size = [ZSTChatBubble suggestSizeForAttributedString:[ZSTChatBubble smallAttributedStringFromComment:comment]];
//        height += (size.height);
//    }
//    height += 10;
//    return CGSizeMake(kChatBubbleWidth, MAX(height, 46));
//}

@end
