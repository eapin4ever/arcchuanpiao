//
//  ZSTGroupAddressBookController.h
//  YouYun
//
//  Created by luobin on 5/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTGroupAddressBookController : UIViewController<ZSTYouYunEngineDelegate,UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

@property (nonatomic, retain) UITableView *tableView;

@property (nonatomic, retain) UISearchBar *searchBar;

@property (nonatomic, retain) UIView *disableViewOverlay;

@property (nonatomic, retain) NSDictionary *circle;

@property (nonatomic, retain) ZSTYouYunEngine *engine;

@property (nonatomic, retain) NSArray *users;

@property (nonatomic, retain) NSMutableArray *resultArray;

@property (nonatomic, retain) NSArray *dataArray;
    
@end
