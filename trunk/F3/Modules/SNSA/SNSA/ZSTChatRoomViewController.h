//
//  ZSTChatRoomViewController.h
//  SNSA
//
//  Created by LiZhenQu on 14-5-6.
//
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "ImageAudioPlayer.h"
#import "ZSTTopicCommentController.h"
#import "ZSTChatRoomCell.h"

#define kOneLimit 9 //一次取消息的条数


@interface UIImageView (ForScrollView)

@end

@protocol ChatRoomViewControllerDelegate <NSObject>

@required

-(void)backAction:(NSString*)cid;

@end


@interface ZSTChatRoomViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,ZSTYouYunEngineDelegate,EGORefreshTableHeaderDelegate, TKLoadMoreViewDelegate, UITextViewDelegate
,UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate,AVAudioRecorderDelegate,AVAudioPlayerDelegate,ZSTCommentSendFinishDelegate,ClickViewDelegate>
{
    UITableView *mTable;
    
    EGORefreshTableHeaderView *_refreshHeaderView;
    TKLoadMoreView *_loadMoreView;
    
    BOOL _isRefreshing;
    BOOL _isLoadingMore;
    BOOL _hasMore;
    
    UIImageView *voiceBg;
    UIImageView *btnicon;
    UIButton *recordBtn;
    
     double beginTime;
    NSInteger duration;
    
     BOOL isRecord;
    
     NSInteger row;
    
     int sendNum;    //记录发送次数
}

@property (nonatomic, retain) UITableView *mTable;
@property (nonatomic, retain) UIScrollView *faceView;
@property (nonatomic, retain) NSDictionary *circle;
@property (nonatomic, retain) NSMutableArray *messages;
@property (nonatomic, retain) ZSTYouYunEngine *engine;
@property (nonatomic, retain) id<ChatRoomViewControllerDelegate> delegate;

@property (nonatomic, retain) UIImageView *voiceBg;
@property (nonatomic, retain) UIImageView *btnicon;
@property (nonatomic, retain) UIButton *recordBtn;
@property (nonatomic, retain) TKPlaceHolderTextView *messageView;
@property (nonatomic, retain) UIImageView *picBg;
@property (nonatomic, retain) UIImagePickerController *iconPickerController;
@property (nonatomic, retain) UIImageView *uploadImageBg;
@property (nonatomic, retain) NSString *uploadImageUrl;
@property (nonatomic, retain) NSMutableArray *selectFaceArr;
@property (nonatomic, retain) ImageAudioPlayer *audioPlayer;
@property (nonatomic, retain) AVAudioRecorder *recorder;
@property (nonatomic, retain) UIImageView *recordImage;
@property (nonatomic, retain) NSString *recordFile;
@property (nonatomic, retain) UIView *keyboardCover;
@property (nonatomic, retain) UIView *tipView;

@property (nonatomic, retain) NSString *circleId;

@property (nonatomic, retain) NSString *uploadVideoUrl;

@end
