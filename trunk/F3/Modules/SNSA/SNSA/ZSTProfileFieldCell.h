//
//  ZSTProfileFieldCell.h
//  YouYun
//
//  Created by luobin on 6/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomSwitch.h"

@class ZSTProfileFieldCell;
typedef  void (^infoStatesBlock)(ZSTProfileFieldCell *cell,int states);


@interface ZSTProfileFieldCell : TextFieldCell<CustomSwitchDelegate>
{
    infoStatesBlock statesBlock;
}


@property(nonatomic) CustomCellBackgroundViewPosition position;
    
@property (nonatomic, strong) CustomSwitch *switchOne;

- (void)setInfoStatesBlock:(infoStatesBlock)block;


@end
