//
//  PopOver.h
//  SNSA
//
//  Created by LiZhenQu on 14-5-19.
//
//

#import <UIKit/UIKit.h>

@class PopOver;

@protocol PopOverDelegate <NSObject>

- (void)popOver:(PopOver *)popOver clickedButtonAtIndex:(NSInteger)buttonIndex;

@end


@interface PopOver : UIView

@property (nonatomic, strong) UIView* popOverContainer;
@property (nonatomic, strong) UIImageView* popBGImageView;
@property (nonatomic, strong) UIButton* cancelButton;
@property (nonatomic, strong) UIButton* confirmButton;
@property (nonatomic, strong) UILabel* contentLabel;
@property (nonatomic, strong) UILabel* titleLabel;
@property (nonatomic, strong) id<PopOverDelegate>delegate;

@property (nonatomic, strong) UIView* addCount;
@property (nonatomic, strong) UILabel* countLabel;


@property (nonatomic, strong) UIImage *popBGImag;


- (void)showPopOverWithTitle:(NSString *)title
                     message:(NSString *)message
             backgroundImage:(UIImage *)backgroundImage
                    delegate:(id)delegate
           cancelButtonTitle:(NSString *)cancelButtonTitle
           otherButtonTitles:(NSString *)otherButtonTitles,...NS_REQUIRES_NIL_TERMINATION;

- (void)showAddCountWithTitle:(NSString *)title
                      message:(NSString *)message
              backgroundImage:(UIImage *)backgroundImage
                     delegate:(id)delegate
            cancelButtonTitle:(NSString *)cancelButtonTitle
            otherButtonTitles:(NSString *)otherButtonTitles,...NS_REQUIRES_NIL_TERMINATION;


@end

