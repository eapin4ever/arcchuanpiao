//
//  PopOver.m
//  SNSA
//
//  Created by LiZhenQu on 14-5-19.
//
//

#import "PopOver.h"

@implementation PopOver

- (void) dealloc
{
    [_popOverContainer release];
    [_popBGImag release];
    [_titleLabel release];
    [_contentLabel release];
    
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UIView *bgView = [[UIView alloc] initWithFrame:frame];
        bgView.backgroundColor = [UIColor blackColor];
        bgView.alpha = 0.7f;
        [self addSubview:bgView];
        [bgView release];
        
        self.popOverContainer = [[UIView alloc] initWithFrame:CGRectMake(18, 100, 284, 200)];
        self.popOverContainer.backgroundColor = [UIColor clearColor];
        [self addSubview:self.popOverContainer];
        
        self.popBGImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, -6, 284, 200)];
        [self.popOverContainer addSubview:self.popBGImageView];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(71, 12, 142, 21)];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.font = [UIFont systemFontOfSize:17];
        self.titleLabel.textColor = RGBCOLOR(253, 114, 34);
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.popOverContainer addSubview:self.titleLabel];
        
        self.contentLabel = [[UILabel alloc] initWithFrame:CGRectMake(37, 42, 210, 129)];
        self.contentLabel.backgroundColor = [UIColor clearColor];
        self.contentLabel.font = [UIFont systemFontOfSize:17];
        [self.popOverContainer addSubview:self.contentLabel];
        
        UIImage *image = ZSTModuleImage(@"module_snsa_btn_jion.png");
        CGFloat capWidth = image.size.width / 2;
        CGFloat capHeight = image.size.height / 2;
        
        self.confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.confirmButton.frame = CGRectMake(155, 115, 92, 30);
        [self.confirmButton setTitle:@"我知道了" forState:UIControlStateNormal];
        [self.confirmButton setBackgroundImage:[image stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight] forState:UIControlStateNormal];
        self.confirmButton.tag = 1;
        self.confirmButton.titleLabel.textColor = [UIColor whiteColor];
        [self.popOverContainer addSubview:self.confirmButton];
        
        image = ZSTModuleImage(@"module_snsa_btn_gray.png");
        capWidth = image.size.width / 2;
        capHeight = image.size.height / 2;
        
        self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.cancelButton.frame = CGRectMake(37, 165, 92, 30);
        [self.cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [self.cancelButton setBackgroundImage:[image stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight] forState:UIControlStateNormal];
        self.cancelButton.tag = 0;
        self.cancelButton.titleLabel.textColor = [UIColor whiteColor];
//        [self.popOverContainer addSubview:self.cancelButton];
        
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */


- (void)showPopOverWithTitle:(NSString *)title
                     message:(NSString *)message
             backgroundImage:(UIImage *)backgroundImage
                    delegate:(id)delegate
           cancelButtonTitle:(NSString *)cancelButtonTitle//cancelbtn 始终保持灰色
           otherButtonTitles:(NSString *)otherButtonTitles,...//大于3个按钮，按钮都用彩色
{
    
    UIImage *image = ZSTModuleImage(@"module_snsa_btn_jion.png");
    CGFloat capWidth = image.size.width / 2;
    CGFloat capHeight = image.size.height / 2;
    
    self.delegate = delegate;
    if (backgroundImage) {
        self.popBGImag = backgroundImage;
    }else{
        self.popBGImag = ZSTModuleImage(@"module_snsa_popover_bg.png");
    }
    self.popBGImageView.image = [self.popBGImag stretchableImageWithLeftCapWidth:0 topCapHeight:77];
    
    CGSize size = [message sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(self.contentLabel.frame.size.width, 300)];
    CGRect frame = self.contentLabel.frame;
    float height = frame.origin.y;
    frame.size.height = size.height;
    self.contentLabel.frame = frame;
    
    NSMutableArray *btnArr = [NSMutableArray array];
    if ([cancelButtonTitle length] != 0
        && ![cancelButtonTitle isEqualToString:@""]
        && cancelButtonTitle != nil){
        [btnArr addObject:cancelButtonTitle];
    }
    va_list args;
    va_start(args, otherButtonTitles);
    for (NSString *str = otherButtonTitles; str != nil; str = va_arg(args,NSString*)) {
        [btnArr addObject: str];
    }
    va_end(args);
    
    if ([btnArr count] == 1  && ([cancelButtonTitle length] == 0 || cancelButtonTitle == nil)) {
        height += size.height + 10;
        frame = self.confirmButton.frame;
        if (height > 115) {
            frame.origin.y = height;
        }
        frame.origin.x = (self.popOverContainer.frame.size.width - frame.size.width)/2;
        self.confirmButton.frame = frame;
        self.cancelButton.hidden = YES;
        [self.confirmButton setTitle:[btnArr objectAtIndex:0] forState:UIControlStateNormal];
        [self.confirmButton addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
    }else if ([btnArr count] == 1)
    {
        height += size.height + 10 + 40;
        frame = self.cancelButton.frame;
        frame.origin.y = height;
        frame.origin.x = (self.popOverContainer.frame.size.width - frame.size.width)/2;
        self.cancelButton.frame = frame;
        self.confirmButton.hidden = YES;
        [self.cancelButton setTitle:[btnArr objectAtIndex:0] forState:UIControlStateNormal];
        [self.cancelButton addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
    }else if ([btnArr count] == 2 && [cancelButtonTitle length] != 0 && cancelButtonTitle != nil)
    {
        height += size.height + 10 + 40;
        frame = self.cancelButton.frame;
        frame.origin.y = height;
        self.cancelButton.frame = frame;
        [self.cancelButton setTitle:[btnArr objectAtIndex:0] forState:UIControlStateNormal];
        [self.cancelButton addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        frame = self.confirmButton.frame;
        frame.origin.y = height;
        self.confirmButton.frame = frame;
        [self.confirmButton setTitle:[btnArr objectAtIndex:1] forState:UIControlStateNormal];
        [self.confirmButton addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
         [self.confirmButton setBackgroundImage:[image stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight] forState:UIControlStateNormal];
        
    }else if ([btnArr count] == 3)
    {
        height += size.height + 10;
        frame = self.cancelButton.frame;
        frame.origin.y = height;
        frame.origin.x = 44;
        frame.size.height = frame.size.height-2;
        frame.size.width = 60;
        self.cancelButton.frame = frame;
        self.confirmButton.hidden = YES;
        [self.cancelButton setTitle:[btnArr objectAtIndex:0] forState:UIControlStateNormal];
        [self.cancelButton addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        for (int i = 1; i < [btnArr count]; i ++ ) {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
            btn.frame = CGRectMake(frame.origin.x + frame.size.width+10, frame.origin.y, frame.size.width, frame.size.height);
            btn.tag = i;
            btn.titleLabel.font = [UIFont boldSystemFontOfSize:15];
            [btn setTitle:[btnArr objectAtIndex:i] forState:UIControlStateNormal];
//            [btn setBackgroundImage:[UIImage imageNamed:@"button_buy.png"] forState:UIControlStateNormal];
            [btn setBackgroundImage:[image stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight] forState:UIControlStateNormal];
            [self.popOverContainer addSubview:btn];
            frame = btn.frame;
            height = btn.frame.origin.y;
        }
        
    }else if ([btnArr count] > 3)
    {
        
        height += size.height + 10;
        frame = self.cancelButton.frame;
        frame.origin.y = height;
        frame.origin.x = (self.popOverContainer.frame.size.width - frame.size.width)/2;
        self.cancelButton.frame = frame;
        self.confirmButton.hidden = YES;
        [self.cancelButton setTitle:[btnArr objectAtIndex:0] forState:UIControlStateNormal];
        [self.cancelButton addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        for (int i = 1; i < [btnArr count]; i ++ ) {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [btn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
            btn.frame = CGRectMake(frame.origin.x, frame.origin.y + frame.size.height +10, frame.size.width, frame.size.height);
            btn.tag = i;
            [btn setTitle:[btnArr objectAtIndex:i] forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont boldSystemFontOfSize:15];
           [btn setBackgroundImage:[image stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight] forState:UIControlStateNormal];
            [self.popOverContainer addSubview:btn];
            
            frame = btn.frame;
            height = btn.frame.origin.y;
        }
    }
    
    
    height += frame.size.height + 30 + 110/size.height;
    frame.origin.x = self.popOverContainer.frame.origin.x;
    frame = self.popBGImageView.frame;
    frame.size.height = height;
    
    self.popOverContainer.frame = frame;
    self.popOverContainer.center = self.center;
    self.popOverContainer.hidden = NO;
    self.contentLabel.text = message;
    self.titleLabel.text = title;
    [self bringSubviewToFront:self.popOverContainer];
    self.popOverContainer.transform = CGAffineTransformMakeScale(.1f, .1f);
    [UIView animateWithDuration:0.1f animations:^{
        self.popOverContainer.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:.1f animations:^{
            self.popOverContainer.transform = CGAffineTransformMakeScale(.9f, .9f);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:.1f animations:^{
                self.popOverContainer.transform = CGAffineTransformMakeScale(1, 1);
            }];
        }];
    }];
    
}

- (void)btnClicked:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(popOver:clickedButtonAtIndex:)]) {
        [self.delegate popOver:self clickedButtonAtIndex:sender.tag];
    }
    [self removeFromSuperview];
    
}

- (void)showAddCountWithTitle:(NSString *)title
                      message:(NSString *)message
              backgroundImage:(UIImage *)backgroundImage
                     delegate:(id)delegate
            cancelButtonTitle:(NSString *)cancelButtonTitle//cancelbtn 始终保持灰色
            otherButtonTitles:(NSString *)otherButtonTitles,...//大于3个按钮，按钮都用彩色

{
    [self showPopOverWithTitle:title
                       message:message
               backgroundImage:backgroundImage
                      delegate:delegate
             cancelButtonTitle:cancelButtonTitle//cancelbtn 始终保持灰色
             otherButtonTitles:otherButtonTitles,nil];
    [self.confirmButton removeTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self.confirmButton addTarget:self action:@selector(addCountBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)addCountBtnClicked:(UIButton *)sender
{
    if ([self.delegate respondsToSelector:@selector(popOver:clickedButtonAtIndex:)]) {
        [self.delegate popOver:self clickedButtonAtIndex:sender.tag];
    }
}




@end
