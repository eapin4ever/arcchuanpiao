//
//  ImageAudioPlayer.h
//  YouYun
//
//  Created by admin on 12-11-8.
//
//

#import <AVFoundation/AVFoundation.h>

@interface ImageAudioPlayer : AVAudioPlayer

@property (nonatomic,retain) UIImageView *imageView;
@property (nonatomic,retain) NSString *playingFile;
@end
