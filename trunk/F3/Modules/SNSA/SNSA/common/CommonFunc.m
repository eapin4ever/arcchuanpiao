//
//  CommonFunc.m
//  Ancient
//
//  Created by zj l on 12-4-9.
//  Copyright 2012年 . All rights reserved.
//

#import "CommonFunc.h"
#import "NSDate+NSDateEx.h"

@interface CommonFunc (PrivateMethods)

-(CGRect)getRectByURL:(NSString *)url;
-(CGSize)getStrWidthX:(NSString *)str nowSize:(CGSize)nowSize maxWidth:(CGFloat)maxWidth hasPic:(BOOL)hasPic;

@end


@implementation CommonFunc

@synthesize isDisplayDic;
//@dynamic nav;
@synthesize faceTilteArr;

static CommonFunc *shared_common = nil;

+ (CommonFunc *) sharedCommon{
    @synchronized(self){
        if (shared_common == nil) {
            [[self alloc] init];
        }
    }
    return shared_common;
}

+ (id) allocWithZone:(NSZone *)zone{
    @synchronized(self){
        if (shared_common == nil) {
            shared_common = [super allocWithZone:zone];
            return shared_common;
        }
    }
    return nil;
}

- (id)init
{
    self = [super init];
    if (self) {
        self.isDisplayDic = [NSMutableDictionary dictionary];
        
        self.faceTilteArr = [NSArray arrayWithObjects:
                             @"微笑",@"呲牙",@"大笑",@"舔嘴",@"斜视",@"愉快",@"高兴", @"晕",@"飞吻",@"伤心",
                             @"惊讶",@"哈欠",@"哦",@"财迷",@"大哭",@"害羞",@"呼噜",@"吐",@"可爱",@"汗",
                             @"怒",@"偷笑",@"酷",@"大骂",@"鄙视",@"抠鼻",@"色",@"鼓掌",@"思考",@"病了",
                             @"咒骂",@"右哼哼",@"左哼哼",@"嘘",@"委屈",@"可怜",@"抓狂",@"眨眼",@"衰",@"猪头",
                             @"赞",@"OK",@"牛",@"握手",@"弱",@"NO",@"勾引",@"胜利",@"爱心",@"心碎",
                             @"月亮",@"围观",@"蛋糕",@"咖啡",@"话筒",@"帅",@"囧",@"OUT",@"爱情",@"围脖",
                             @"帽子",@"手套",@"雪花",@"雪人",@"枫叶",@"照相",@"浮云",@"礼物",@"绿丝带",@"时间",
                             @"玫瑰",@"干杯", nil];
    }
    return self;
}

+(NSString *)getShort:(NSString *)number
{
    NSUInteger i = [number length];
    NSInteger k = 0;
    NSArray *au = [NSArray arrayWithObjects:@"T", @"B", @"M", @"K", nil];
    for (NSInteger j = 12; j > 0; j -= 3) {
        if (i > j) {
            i -= j;
            NSRange r = NSMakeRange (i, 1);
            NSString *z = [number substringToIndex:i];
            NSString *d = [number substringWithRange:r];
            NSString *u = [au objectAtIndex:k];
            if ([d integerValue] > 0) {
                return [NSString stringWithFormat:@"%@.%@%@", z, d, u];
            }
            
            return [NSString stringWithFormat:@"%@%@", z, u];
        }
        k++;
    }
    
    return number;
}

-(NSString *)stringForHourValue: (int) sec //for 00:00:00 format
{
    int seconds = sec;
    int hoursPart = seconds / 3600;
    int minutesPart = (seconds % 3600) / 60;
    int secondsPart = (seconds % 3600) % 60;
    NSString *hoursString = (hoursPart < 10) ?
    [NSString stringWithFormat:@"0%d", hoursPart] :
    [NSString stringWithFormat:@"%d", hoursPart];
    
    NSString *minutesString = (minutesPart < 10) ?
    [NSString stringWithFormat:@"0%d", minutesPart] :
    [NSString stringWithFormat:@"%d", minutesPart];
    
    NSString *secondsString = (secondsPart < 10) ?
    [NSString stringWithFormat:@"0%d", secondsPart] :
    [NSString stringWithFormat:@"%d", secondsPart];
    
    return [NSString stringWithFormat:@"%@:%@:%@", hoursString, minutesString, secondsString];
} 
//根据宽度来显示图片
-(NSInteger)getHeightByURL:(NSString *)url assignWidth:(NSInteger)assignWidth {
    
    NSArray *splitArr = [url componentsSeparatedByString:@"_"];
    NSString *lastStr = [splitArr objectAtIndex:[splitArr count]-1];
    if (!splitArr) {
        return 255;
    }
    
    NSRange r1 = [lastStr rangeOfString:@"x"];
    NSRange r2 = [lastStr rangeOfString:@"."];
    if (r1.length == 0 || r2.length == 0) {
        return 255;
    }
    NSRange r3 = [lastStr rangeOfString:@".spx"];
    if (lastStr!=nil && [lastStr length]>0 && r3.length==0) {
        NSString *width = [lastStr substringToIndex:r1.location];
        NSString *height = [lastStr substringWithRange:NSMakeRange(r1.location+1, r2.location - r1.location-1)];
        //重新计算大小Size
//        NSLog(@"%@ and %@",width,height);
        NSInteger newHeight = [height intValue] * assignWidth / [width intValue];
        return newHeight;
    }
    return 0;
}

-(CGSize)getSizeByURL:(NSString *)url assignWidth:(CGFloat)assignWidth {
    
    NSArray *splitArr = [url componentsSeparatedByString:@"_"];
    NSString *lastStr = [splitArr objectAtIndex:[splitArr count]-1];
    if (!splitArr) {
        return CGSizeMake(assignWidth, 255);
    }
    
    NSRange r1 = [lastStr rangeOfString:@"x"];
    NSRange r2 = [lastStr rangeOfString:@"."];
    if (r1.length == 0 || r2.length == 0) {
        return CGSizeMake(assignWidth, 255);
    }
    NSRange r3 = [lastStr rangeOfString:@".spx"];
    if (lastStr!=nil && [lastStr length]>0 && r3.length==0) {
        NSString *width = [lastStr substringToIndex:r1.location];
        NSString *height = [lastStr substringWithRange:NSMakeRange(r1.location+1, r2.location - r1.location-1)];
        //        NSLog(@"%@ and %@",width,height);
        if (width.floatValue<assignWidth) {
            return CGSizeMake(width.floatValue, height.floatValue);
        }
        CGFloat newHeight = [height intValue] * assignWidth / [width intValue];
        return CGSizeMake(assignWidth, newHeight);
    }
    return CGSizeZero;
}

//根据40*40来显示头像
-(CGRect)getRectByURL:(NSString *)url {
    
    NSArray *splitArr = [url componentsSeparatedByString:@"_"];
    if (!splitArr || [splitArr count]==1) {
        return CGRectNull;
    }
    NSString *lastStr = [splitArr objectAtIndex:[splitArr count]-1];
//    if ([lastStr isEqualToString:@"http://youyun.f3.cn:80/resource/thumbnail/"]) {
//        return CGRectNull;
//    }
    NSRange r1 = [lastStr rangeOfString:@"x"];
    NSRange r2 = [lastStr rangeOfString:@"."];
    if (r1.length == 0 || r2.length == 0) {
        return CGRectNull;
    }
    NSString *width = [lastStr substringToIndex:r1.location];
    NSString *height = [lastStr substringWithRange:NSMakeRange(r1.location+1, r2.location - r1.location - 1)];
    if ([width isKindOfClass:[NSNull class]] || [height isKindOfClass:[NSNull class]]) {
        return CGRectNull;
    }
    
    NSInteger x;
    NSInteger y;
    if ([width isEqualToString:height]) {//正方形
//        return CGRectMake(0, 0, [width intValue], [height intValue]);
        return CGRectNull;
    } else if ([width intValue] > [height intValue]) {//宽形
        x = ([width floatValue] - [height floatValue]) *.5;
        return CGRectMake(x, 0, [height floatValue], [height floatValue]);
    } else if ([width intValue] < [height intValue]) {//长形
        y = ([height floatValue] - [width floatValue]) *.5;
        return CGRectMake(0, y, [width floatValue], [width floatValue]);
    }
//    return CGRectMake(0, 0, [width intValue], [height intValue]);
    return CGRectNull;
}

-(UIImage *)getHeadImage:(UIImage*)image withURL:(NSString*)url{
    CGRect rerect = [self getRectByURL:url];
    if (!CGRectIsNull(rerect)) {
        if (rerect.origin.x != rerect.origin.y) {
            CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], rerect);
            return [UIImage imageWithCGImage:imageRef];
        }
    }
    return nil;
}

-(NSArray*)sortArrInKey:(NSArray*)arr {
    if (!arr || [arr count]==0) {
        return nil;
    }
    
    NSArray *sortArr = [arr sortedArrayUsingComparator:^(id obj1,id obj2){
        NSNumber *value1 = [obj1 safeObjectForKey:@"MID"];
        NSNumber *value2 = [obj2 safeObjectForKey:@"MID"];
        if ([value1 longValue] < [value2 longValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        } else {
            return (NSComparisonResult)NSOrderedAscending;
        }
    }];
    
    return sortArr;
}

-(void)updateUid:(NSString *)uid{
//    NSString *uid = [dic objectForKey:@"uid"];
//    [TKKeychainUtils createKeychainValueForUserid:uid withToken:@"" withName:@"" withAccountType:0 withAvtarURL:@"" forIdentifier:userKeyChainIdentifier];
    
    [[NSUserDefaults standardUserDefaults] setObject:uid forKey:userKeyChainIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize]; 
}

-(NSString*)getCurrentUid {
//    return [TKKeychainUtils getUserid:userKeyChainIdentifier];    
    return [[NSUserDefaults standardUserDefaults] objectForKey:userKeyChainIdentifier];
}

-(void)updateMyMessageId:(NSString *)mid{
    NSString *uid = [self getCurrentUid];
    NSString *key = [NSString stringWithFormat:@"%@_%@_%@",userKeyChainIdentifier,@"MESSAGEID",uid];
    [[NSUserDefaults standardUserDefaults] setObject:mid forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*)getMyMessageId{
    NSString *uid = [self getCurrentUid];
    NSString *key = [NSString stringWithFormat:@"%@_%@_%@",userKeyChainIdentifier,@"MESSAGEID",uid];
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

-(void)updateMyCommentId:(NSString *)cid{
    NSString *uid = [self getCurrentUid];
    NSString *key = [NSString stringWithFormat:@"%@_%@_%@",userKeyChainIdentifier,@"COMMENTID",uid];
    [[NSUserDefaults standardUserDefaults] setObject:cid forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*)getMyCommentId{
    NSString *uid = [self getCurrentUid];
    NSString *key = [NSString stringWithFormat:@"%@_%@_%@",userKeyChainIdentifier,@"COMMENTID",uid];
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

-(void)updateMyMetionId:(NSString *)mid{
    NSString *uid = [self getCurrentUid];
    NSString *key = [NSString stringWithFormat:@"%@_%@_%@",userKeyChainIdentifier,@"MENTION",uid];
    [[NSUserDefaults standardUserDefaults] setObject:mid forKey:key];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSString*)getMyMetionId{
    NSString *uid = [self getCurrentUid];
    NSString *key = [NSString stringWithFormat:@"%@_%@_%@",userKeyChainIdentifier,@"MENTION",uid];
    return [[NSUserDefaults standardUserDefaults] objectForKey:key];
}

-(void)updateUVO:(NSDictionary *)uvo{
    NSEnumerator * enumeratorKey = [uvo keyEnumerator];
    //快速枚举遍历所有KEY的值
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:uvo];
    for (NSString *object in enumeratorKey) {
        NSNumber *value = [uvo objectForKey:object];
        if (value == NULL) {
            [dic setObject:@"" forKey:object];
        }
        if (value == nil) {
            [dic setObject:@"" forKey:object];
        }
        if ([value isEqual:[NSNull null]]) {
            [dic setObject:@"" forKey:object];            
        }
    }
    [[NSUserDefaults standardUserDefaults] setObject:dic forKey:uservoIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(NSDictionary *)getCurrentUVO{
    return [[NSUserDefaults standardUserDefaults] objectForKey:uservoIdentifier];
}


-(void)updateCircles:(NSArray*) arr {
    [TKDataCache setCacheData:arr andURLKey:ZSTHttpMethod_GetCircles];
}

-(NSDictionary*)getCircleByCID:(NSString*)cid {
    
    NSArray *circles = [TKDataCache getCacheDataByURLKey:ZSTHttpMethod_GetCircles];
    for (int i=0; i<[circles count]; i++) {
        NSDictionary *dic = [circles objectAtIndex:i];
        if ([[[dic safeObjectForKey:@"CID"] stringValue] isEqualToString:cid]) {
            return dic;
        }
    }
    return nil;
}

-(void)updateAllMessage:(NSString*)uid msg:(NSArray*)msg{
//    NSString *url = [NSString stringWithFormat:@"%@_%@",ZSTHttpMethod_GetPublicMessages,uid];
//    [TKDataCache setCacheData:msg andURLKey:url];
}

//-(NSArray*)getAllMessage:(NSString*)uid{
//    NSString *url = [NSString stringWithFormat:@"%@_%@",ZSTHttpMethod_GetPublicMessages,uid];
//    NSArray *circles = [TKDataCache getCacheDataByURLKey:url];
//    return circles;
//}

-(void)updateMyMessage:(NSString*)uid msg:(NSArray*)msg{
//    NSString *url = [NSString stringWithFormat:@"%@_%@",ZSTHttpMethod_GetMyMessages,uid];
//    [TKDataCache setCacheData:msg andURLKey:url];
}

//-(NSArray*)getMyMessage:(NSString*)uid{
//    NSString *url = [NSString stringWithFormat:@"%@_%@",ZSTHttpMethod_GetMyMessages,uid];
//    NSArray *circles = [TKDataCache getCacheDataByURLKey:url];
//    return circles;
//}

- (void)dealloc
{
    self.isDisplayDic = nil;
    self.faceTilteArr = nil;
    [super dealloc];
}

-(BOOL)searchResult:(NSString *)contactName searchText:(NSString *)searchT{
	NSComparisonResult result = [contactName compare:searchT options:NSCaseInsensitiveSearch
											   range:NSMakeRange(0, searchT.length)];
	if (result == NSOrderedSame)
		return YES;
	else
		return NO;
}

-(NSString *)getFirstLetter:(NSString*)sectionName {
    NSUInteger location = [@"0123456789" rangeOfString:sectionName].location;
    if (sectionName == nil || [sectionName length]==0 || location != NSNotFound) {
        return @"#";
    } else if(isalpha([sectionName characterAtIndex:0])){
        sectionName = [[NSString stringWithFormat:@"%c", [sectionName characterAtIndex:0]] uppercaseString];
    } else if([self searchResult:sectionName searchText:@"曾"])
        sectionName = @"Z";
    else if([self searchResult:sectionName searchText:@"解"])
        sectionName = @"X";
    else if([self searchResult:sectionName searchText:@"仇"])
        sectionName = @"Q";
    else if([self searchResult:sectionName searchText:@"朴"])
        sectionName = @"P";
    else if([self searchResult:sectionName searchText:@"查"])
        sectionName = @"Z";
    else if([self searchResult:sectionName searchText:@"能"])
        sectionName = @"N";
    else if([self searchResult:sectionName searchText:@"乐"])
        sectionName = @"Y";
    else if([self searchResult:sectionName searchText:@"单"])
        sectionName = @"S";
    else
        sectionName = [[NSString stringWithFormat:@"%c",pinyinFirstLetter([sectionName characterAtIndex:0])] uppercaseString];
    
    return sectionName;
}

- (id)customControllerWithRootViewController:(UIViewController *)root {
    UINavigationController *nav = [[[NSBundle mainBundle] loadNibNamed:@"NavigationController" owner:self options:nil] objectAtIndex:0];
    [nav setViewControllers:[NSArray arrayWithObject:root]];
    [nav retain];
    return [nav autorelease];
}

-(BOOL)isExistNetWork {
    BOOL isExistenceNetwork = YES;
    Reachability *r = [Reachability reachabilityWithHostName:@"www.apple.com"];
    switch ([r currentReachabilityStatus]) {
        case NotReachable:
            isExistenceNetwork=NO; //   NSLog(@"没有网络");
            break;
        case ReachableViaWWAN:
            isExistenceNetwork=YES;//   NSLog(@"正在使用3G网络");
            break;
        case ReachableViaWiFi:
            isExistenceNetwork=YES;//  NSLog(@"正在使用wifi网络");
            break;
    }
    if (!isExistenceNetwork) {
        UIAlertView *myalert = [[UIAlertView alloc]
                                initWithTitle:NSLocalizedString(@"提示", @"提示")
                                message:NSLocalizedString(@"貌似网络不畅哦！", nil)
                                delegate:self
                                cancelButtonTitle:NSLocalizedString(@"确定", @"确定")
                                otherButtonTitles:nil];
        
        [myalert show];
        
        [myalert release];
    }
    return isExistenceNetwork;
}

-(BOOL)isAlertNewTips:(AlertTips)tips{
    //第一次调这个方法，初始化
    NSMutableDictionary *isAlert = [[NSUserDefaults standardUserDefaults] objectForKey:alertNewTips];
    if (!isAlert) {
        isAlert = [NSMutableDictionary dictionary];
        [isAlert setObject:[NSNumber numberWithBool:NO] forKey:[NSString stringWithFormat:@"%d",LeftSilde]];
        [isAlert setObject:[NSNumber numberWithBool:NO] forKey:[NSString stringWithFormat:@"%d",MainList]];
        [isAlert setObject:[NSNumber numberWithBool:NO] forKey:[NSString stringWithFormat:@"%d",ChatList]];
        [isAlert setObject:[NSNumber numberWithBool:NO] forKey:[NSString stringWithFormat:@"%d",PeopleList]];
        [isAlert setObject:[NSNumber numberWithBool:NO] forKey:[NSString stringWithFormat:@"%d",isRegUser]];
        [[NSUserDefaults standardUserDefaults] setObject:isAlert forKey:alertNewTips];
        [[NSUserDefaults standardUserDefaults] synchronize];
        return NO;
    }
    NSNumber *alert = [isAlert valueForKey:[NSString stringWithFormat:@"%d",tips]];
    return [alert boolValue];
}

-(void)saveAlertNewTips:(AlertTips)tips{
    NSDictionary *isAlert = (NSDictionary *)[[NSUserDefaults standardUserDefaults] objectForKey:alertNewTips];
    //存到本地之后，很可能不再是NSMutableDictionary
    NSMutableDictionary *temp = [NSMutableDictionary dictionaryWithDictionary:isAlert];
//    if (!temp) {
//        temp = [NSMutableDictionary dictionary];
//        [temp setObject:[NSNumber numberWithBool:NO] forKey:[NSString stringWithFormat:@"%d",LeftSilde]];
//        [temp setObject:[NSNumber numberWithBool:NO] forKey:[NSString stringWithFormat:@"%d",MainList]];
//        [temp setObject:[NSNumber numberWithBool:NO] forKey:[NSString stringWithFormat:@"%d",ChatList]];
//        [temp setObject:[NSNumber numberWithBool:NO] forKey:[NSString stringWithFormat:@"%d",PeopleList]];
//        [temp setObject:[NSNumber numberWithBool:NO] forKey:[NSString stringWithFormat:@"%d",isRegUser]];
//    }
    NSString *key = [NSString stringWithFormat:@"%d",tips];
    [temp setObject:[NSNumber numberWithBool:YES] forKey:key];
    
    [[NSUserDefaults standardUserDefaults] setObject:temp forKey:alertNewTips];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(NSArray*)getFaceOrigin:(NSString *)message maxWidth:(CGFloat )maxWidth hasPic:(BOOL)hasPic{
    //判断是否包含表情
    NSRange r = [message rangeOfString:@"[face"];
    
    if (r.length>0 && r.location!=NSNotFound) {
        NSMutableArray *originArr = [NSMutableArray array];
        CGFloat widthX = 0.0,heightY = 1.0;
        NSArray *array = [message componentsSeparatedByString:@"["];
        for (int i=0; i<[array count]; i++) {
            
            NSString *splitStr = [array objectAtIndex:i]; //切分的字符串
            NSRange r1 = [splitStr rangeOfString:@"face"];
            NSRange r2 = [splitStr rangeOfString:@"]"];

            if (r1.length>0 && r2.length>0 ) {
                
                NSString *fn = [splitStr substringToIndex:r2.location]; //表情名称
                NSString *other = [splitStr substringFromIndex:r2.location+1];
                
                NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:
                                     [NSNumber numberWithFloat:widthX],@"l",
                                     [NSNumber numberWithFloat:heightY],@"h",
                                     fn,@"n", nil];
                [originArr addObject:dic];
                
                CGSize nowSize = [self getStrWidthX:other nowSize:CGSizeMake(widthX, heightY) maxWidth:maxWidth hasPic:hasPic];
                widthX = nowSize.width;
                heightY = nowSize.height;
                
                widthX += 20.448 - 1.5;
                if (widthX>maxWidth-20.448 + 1.5) { //表情换行
                    widthX = 0;
                    if (hasPic) {
                        heightY += (kFaceSize-2);
                    } else {
                        heightY += (kFaceSize+2-2);
                    }
                }
            } else if([splitStr length]>0) {
                CGSize nowSize = [self getStrWidthX:splitStr nowSize:CGSizeMake(widthX, heightY) maxWidth:maxWidth hasPic:hasPic];
                widthX = nowSize.width;
                heightY = nowSize.height;
            }
        }
        return originArr;
    }
    return nil;
}
//private
-(CGSize)getStrWidthX:(NSString*)str nowSize:(CGSize)nowSize maxWidth:(CGFloat)maxWidth hasPic:(BOOL)hasPic{
    CGFloat nowWidth = nowSize.width;
    CGFloat nowHeight = nowSize.height;
    BOOL lastLineHasAlph = NO; //上一行中是否有英文字母
    NSUInteger length = [str length];
    for (int j=0; j<length; ++j)
    {
        NSRange range = NSMakeRange(j, 1);
        NSString *subString = [str substringWithRange:range];
        const char *cString = [subString UTF8String];
        if (strlen(cString) == 3)
        {
//            nowWidth += 16;
            //                        NSLog(@"汉字:%@", subString);
        } else { //小写8.592 空格4.448 大写10.368 数字8.896001
            //                        NSString *regex = @"[0-9]";
            //                        NSString *regex2 = @"[a-z]";
            //                        NSString *regex3 = @"[A-Z]";
            //                        NSPredicate *numberTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
            //                        NSPredicate *numberTest2 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex2];
            //                        NSPredicate *numberTest3 = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex3];
            //                        if([numberTest evaluateWithObject:subString]) {
            //                            widthX += 8.896001;
            //                        } else if ([numberTest2 evaluateWithObject:subString]){
            //                            widthX += 8.592;
            //                        } else if ([numberTest3 evaluateWithObject:subString]){
            //                            widthX += 10.368;
            //                        } else if ([@" " isEqualToString:subString] || [@":" isEqualToString:subString]) {
            //                            widthX += 4.448;
            //                        } else if ([@"=" isEqualToString:subString]) {
            //                            widthX += 9.6;
            lastLineHasAlph = YES;
        }
        NSAttributedString *ss = [[NSAttributedString alloc] initWithString:subString attributes:contentAttributes()];
        CGFloat f = [ZSTChatBubble getWidthForAttributedString:ss];
        nowWidth += f;
        
        if (nowWidth>maxWidth) { //换行
            nowWidth = f;
            if (hasPic) {
                nowHeight += (kFaceSize-2);
            } else {
                if (lastLineHasAlph) {
                    nowHeight += (kFaceSize+2);
                } else {
                    nowHeight += (kFaceSize-2);
                }
            }
            lastLineHasAlph = NO;
        }
    }
    if (nowWidth>(maxWidth-kFaceSize+2)) { //文字换行，权宜之计
        nowWidth = 0;
        if (hasPic) {
            nowHeight += (kFaceSize-2);
        } else {
            if (lastLineHasAlph) {
                nowHeight += (kFaceSize+2);
            } else {
                nowHeight += (kFaceSize-2);
            }
        }
        lastLineHasAlph = NO;
    }
    return CGSizeMake(nowWidth, nowHeight);
}

-(NSString *)getDisplayStr:(NSString*)originStr {
    if (!originStr) {
        return @"";
    }
    
    NSString *pContent = [NSString stringWithString:originStr];
    NSArray *array = [originStr componentsSeparatedByString:@"["];
    if ([array count]>1) {
        for (int i=0; i<[array count]; i++) {
            NSString *splitStr = [array objectAtIndex:i];
            NSRange r1 = [splitStr rangeOfString:@"face"];
            NSRange r2 = [splitStr rangeOfString:@"]"];
            if (r1.length>0 && r2.length>0 ) {
                NSString *fn = [splitStr substringToIndex:r2.location];
                NSString *ss = @"　 ";//一个全角一个半角
                pContent = [pContent stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"[%@]",fn] withString:ss];
            }
        }
    }
    return pContent;
}

-(NSString *)copyConvertStr:(NSString *)pContent {
    NSString *content = [NSString stringWithString:pContent];
    NSArray *array = [content componentsSeparatedByString:@"["];
    if ([array count]>1) {
        for (int i=0; i<[array count]; i++) {
            NSString *splitStr = [array objectAtIndex:i];
            NSRange r1 = [splitStr rangeOfString:@"face"];
            NSRange r2 = [splitStr rangeOfString:@"]"];
            if (r1.length>0 && r2.length>0 ) {
                NSString *fn = [splitStr substringToIndex:r2.location];
                NSString *ss = [[CommonFunc sharedCommon].faceTilteArr objectAtIndex:i-1];
                content = [content stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"[%@]",fn] withString:[NSString stringWithFormat:@"[%@]",ss]];
            }
        }
    }
    return content;
}

-(CGFloat)calculateHeightForCell:(NSDictionary*)cellDic maxWidth:(CGFloat)maxWidth faceSize:(CGFloat)faceSize{
    NSNumber *fileType = [cellDic safeObjectForKey:@"Type"];
    CGFloat height = 10.0;          //init height,cause' the cell background has 10 pixels more than chatBackground 
    if (fileType.intValue == 2) {   
        height += 58;               //voice height is 58.
    } else {
        //face
        NSString *content = [cellDic safeObjectForKey:@"MContent"];
        NSString *pContent = [self getDisplayStr:content];
        
        NSString *imageURL = [cellDic safeObjectForKey:@"ImgUrl"];
        NSAttributedString *attributedString;
        CGSize size;
        if (imageURL != nil && ![imageURL isEmptyOrWhitespace]) {
            height += 4.0;
            height += [self getHeightByURL:imageURL assignWidth:245];
            attributedString = [self getMessageLabelString:cellDic isSmall:YES content:pContent addName:YES];
            size = [ZSTChatBubble suggestSizeForCmtAttributedString:attributedString];
        } else {
            attributedString = [self getMessageLabelString:cellDic isSmall:NO content:pContent addName:YES];
            size = [ZSTChatBubble suggestSizeForAttributedString:attributedString];
        }
        height += (size.height+15);
    }
    //评论
    NSMutableArray *comments = [cellDic safeObjectForKey:@"Cmts"];
    
    for ( int i = 0; i< [comments count]; i++ ) {
        height += 10;
        NSDictionary *comment = [comments objectAtIndex:i];
        CGSize size = [ZSTChatBubble suggestSizeForAttributedString:[ZSTChatBubble smallAttributedStringFromComment:comment]];
        height += (size.height);
    }
    
    if (comments.count > 2) {
        
        height += 20;
    }
    
    return height+10.0; // it is 10 pixels distance from the bottom.
}

-(NSAttributedString *)getMessageLabelString:(NSDictionary *)dic isSmall:(BOOL)isSmall content:(NSString *)content addName:(BOOL)addName {
    
    NSString *userName;
    
    if (dic && ![dic isKindOfClass:[NSNull class]]) {
        
        userName = [[CommonFunc sharedCommon] trimStr:[dic safeObjectForKey:@"New_UName"]];
        if ([self isPureInt:userName] && userName.length >= 11)
        {
            userName = [userName stringByReplacingCharactersInRange:NSMakeRange(5, 4) withString:@"****"];
        }
    } else {
        
        userName = @"";
    }
    
    if (content == nil || [content isEqualToString:@""]) {
        content = @" ";
    }
    double time = [[dic safeObjectForKey:@"AddTime"] doubleValue];
//    NSString *timeStr = [TKUtil timeIntervalSince1970:time];
    NSDate* createdAt = [NSDate dateWithTimeIntervalSince1970:time];
    NSString *timeStr = [NSDate dateStringWithDate:createdAt];
    
    NSMutableAttributedString *attributedString = [[[NSMutableAttributedString alloc] init] autorelease];
    NSAttributedString *nameAttributedString;
    NSAttributedString *contentAttributedString;
    NSAttributedString *timeAttributedString;
    if (isSmall) {
        nameAttributedString = [[NSAttributedString alloc] initWithString:userName?[NSString stringWithFormat:@"%@: ", userName]:@" : " attributes:smallNameAttributes()];
        contentAttributedString = [[NSAttributedString alloc] initWithString:content attributes:smallContentAttributes()];
        timeAttributedString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", timeStr] attributes:smallTimeAttributes()];
    } else {
        nameAttributedString = [[NSAttributedString alloc] initWithString:userName?[NSString stringWithFormat:@"%@: ", userName]:@" : " attributes:nameAttributes()];
        contentAttributedString = [[NSAttributedString alloc] initWithString:content attributes:contentAttributes()];
        timeAttributedString = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"\n%@", timeStr] attributes:timeAttributes()];
    }
    if (addName) {
        [attributedString appendAttributedString:nameAttributedString];
    }
    [attributedString appendAttributedString:contentAttributedString];
    if (addName) {
        [attributedString appendAttributedString:timeAttributedString];
    }
    
    [nameAttributedString release];
    [contentAttributedString release];
    [timeAttributedString release];
    
    return attributedString;
}
-(NSString *)trimStr:(NSString *)str {
    NSString *s = [str stringByReplacingOccurrencesOfString:@"\n" withString: @""];
    return s;
}

- (BOOL)isPureInt:(NSString *)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    NSInteger val;
    return [scan scanInteger:&val] && [scan isAtEnd];
}


@end
