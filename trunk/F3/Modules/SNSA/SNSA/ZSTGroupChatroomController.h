//
//  ZSTGroupChatController.h
//  YouYun
//
//  Created by luobin on 5/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTTopicCommentController.h"
#import <AVFoundation/AVFoundation.h>

@interface ZSTGroupChatroomController : TKPageTableViewController<ZSTYouYunEngineDelegate, ZSTCommentSendFinishDelegate>
{
    NSInteger row;
}

@property (nonatomic, retain) NSDictionary *circle;

@property (nonatomic, retain) ZSTYouYunEngine *engine;

@property (retain) AVAudioSession *session;


@end
