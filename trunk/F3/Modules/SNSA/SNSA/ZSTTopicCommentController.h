//
//  ZSTTopicChatController.h
//  YouYun
//
//  Created by luobin on 6/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZSTCommentSendFinishDelegate <NSObject>

- (void)commentSendFinished:(NSDictionary *)topic row:(NSInteger)row;

@end

@interface ZSTTopicCommentController : UIViewController<ZSTYouYunEngineDelegate>
{
    UIView *containerView;
    TKGrowingTextView *textView;
//    UITextView *textView;
    BOOL _showSendingCell;
    
    CGFloat lastContentOffsetY;
    
    UIButton *_sendBtn;
}

@property (nonatomic, retain) NSString *CID;
@property (nonatomic, retain) NSDictionary *topic;

@property (nonatomic, retain) ZSTYouYunEngine *engine;

@property (nonatomic, assign) id<ZSTCommentSendFinishDelegate> delegate;

@property (nonatomic, assign) NSUInteger row;

@property (nonatomic, assign) BOOL isDefaultCircle;

@end
