//
//  ZSTCommunicator+YouYun.m
//  YouYun
//
//  Created by luobin on 6/5/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//f

#import "ZSTSNSCommunicator+YouYun.h"

//NSString *const URL_SERVER_BASE_PATH = @"http://youyun.f3.cn:80";
//NSString *const URL_SERVER_BASE_PATH = @"http://mod.pmit.cn/SNSApi/SNSB/";
NSString *const URL_SERVER_BASE_PATH = @"http://mod.pmit.cn/SNSa/api/";//最新接口
//NSString *const URL_SERVER_BASE_PATH = @"http://192.168.21.10:90/SNSa/api/";//测试接口

NSString *const URL_SERVER_PATH_UPLOAD_FILE = @"UploadFile";

NSString *const URL_SERVER_PATH_UPLOAD_VIDEO = @"UploadVideo";

NSString *const URL_SERVER_PATH_GET_FILE = @"/GetFile.aspx";

NSString *const URL_SERVER_PATH_APP_API = @"/query.ashx";

NSString *const  ZSTHttpMethod_AddCircle = @"AddCircle"; 

NSString *const  ZSTHttpMethod_GetCircles = @"GetCirclesList";

NSString *const  ZSTHttpMethod_GetUsersByCID = @"GetUsersByCID";
NSString *const  ZSTHttpMethod_QuitCircle = @"QuitCircle";
NSString *const  ZSTHttpMethod_SendMessage = @"SendMessage";
NSString *const  ZSTHttpMethod_GetMessages = @"GetMessages";
NSString *const  ZSTHttpMethod_GetUserByUID = @"GetUserByUID";
NSString *const  ZSTHttpMethod_UpdateUserByUID = @"UpdateUserByUID";
NSString *const  ZSTHttpMethod_UserRegClient = @"UserRegClient";
NSString *const  ZSTHttpMethod_SendVerifyCode = @"SendVerifyCode";
NSString *const  ZSTHttpMethod_GetCommentsByMID = @"GetCmtsByMID";

