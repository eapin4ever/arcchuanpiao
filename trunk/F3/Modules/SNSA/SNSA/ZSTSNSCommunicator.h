
#import <Foundation/Foundation.h>
#import "ASINetworkQueue.h"
#import "ZSTSNSCommunicator+YouYun.h"

typedef enum _ZSTCommunicatorState {
    ZSTCommunicatorStateUnloaded = 0,
    ZSTCommunicatorStateDownloading,
    ZSTCommunicatorStateLoaded,
    ZSTCommunicatorStateFailed,
} ZSTCommunicatorState;

@protocol ZSTSNSCommunicatorDelegate <NSObject>
@optional
// Dictionary or array, default dictionary
- (void)requestDidSucceed:(id)results userInfo:(id)userInfo;

- (void)requestDidFail:(NSError *)error userInfo:(id)userInfo;
@end

@interface ZSTSNSCommunicator : NSObject {
  @private
    ASINetworkQueue *networkQueue;
    NSMutableSet *uploadRequestsSet;
    BOOL showErrorAlert;
}

SINGLETON_INTERFACE(ZSTSNSCommunicator);

@property (nonatomic, assign) BOOL showErrorAlert;
@property (nonatomic, copy) NSArray *prefixParams;

// ############################ URL request methods ############################
/* Open API post信息请求接口 */
// default: auto add prefixParams
// delegate: HHCommunicatorDelegate
- (void)openAPIPostToMethod:(NSString *)method
                 postParams:(NSDictionary *)params
                   delegate:(id<ZSTSNSCommunicatorDelegate>)delegate
               failSelector:(SEL)failSelector
            succeedSelector:(SEL)succeedSelector;

/* 
 * Open API post信息请求接口
 *
 * needUserAuth 请求是不是需要自动添加用户授权信息
 * delegate: HHCommunicatorDelegate
 */
- (void)openAPIPostToMethod:(NSString *)method
                 postParams:(NSDictionary *)params
           needUserAuth:(BOOL)need
                   delegate:(id<ZSTSNSCommunicatorDelegate>)delegate
               failSelector:(SEL)failSelector
            succeedSelector:(SEL)succeedSelector;


/**
 *	@brief	Open API post信息请求接口
 *
 *	@param 	method 	
 *	@param 	params 	
 *	@param 	need 	
 *	@param 	delegate 	
 *	@param 	failSelector 	
 *	@param 	error 	
 *	@param 	succeedSelector 	
 *	@param 	results 	
 *	@param 	userInfo 	
 */
- (void)openAPIPostToMethod:(NSString *)method
                 postParams:(NSDictionary *)params
               needUserAuth:(BOOL)need
                   delegate:(id<ZSTSNSCommunicatorDelegate>)delegate
               failSelector:(SEL)failSelector
            succeedSelector:(SEL)succeedSelector
                   userInfo:(id)userInfo;


- (void)openAPIPostToMethod:(NSString *)method
                 postParams:(NSDictionary *)params
               needUserAuth:(BOOL)need
                   delegate:(id<ZSTSNSCommunicatorDelegate>)delegate
                   userInfo:(id)userInfo;


/* 文件上传接口 */
// Path is whole path, contain the image name
// e.g. : "/image/to/path/name.jpg"
- (void)uploadFilePath:(NSString *)path
              delegate:(id<ZSTSNSCommunicatorDelegate>)delegate
          failSelector:(SEL)failSelector
       succeedSelector:(SEL)succeedSelector
      progressDelegate:(id)progress
              userInfo:(id)userInfo; // progressDelegate is an UIProgressView, if needn't please set nil

// upload file data
- (void)uploadFileData:(NSData *)data
                suffix:(NSString *)suffix
              delegate:(id<ZSTSNSCommunicatorDelegate>)delegate
          failSelector:(SEL)failSelector
       succeedSelector:(SEL)succeedSelector
      progressDelegate:(id)progress
              userInfo:(id)userInfo;

- (void)uploadVideoData:(NSData *)data
                suffix:(NSString *)suffix
              delegate:(id<ZSTSNSCommunicatorDelegate>)delegate
          failSelector:(SEL)failSelector
       succeedSelector:(SEL)succeedSelector
      progressDelegate:(id)progress
              userInfo:(id)userInfo;

// ############################ URL request methods ############################


// 放弃请求.
- (void)cancelByDelegate:(id)delegate;
- (void)cancelUploadByDelegate:(id)delegate;

@end

#pragma mark - 
@interface ZSTSNSCommunicatorUserInfo : NSObject {
    id delegate;
    SEL failSelector;
    // TODO: uid should change name to requestid 
    NSString *uid;
    SEL succeedSelector;
    id userInfo;
}

@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) SEL failSelector;
@property (nonatomic, assign) SEL succeedSelector;
@property (nonatomic, retain) NSString *uid;
@property (nonatomic, retain) id userInfo;
@end

