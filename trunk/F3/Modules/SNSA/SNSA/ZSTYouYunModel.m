//
//  ZSTYouYunModel.m
//  YouYun
//
//  Created by luobin on 7/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTYouYunModel.h"

@implementation NSDictionary(Topic)

- (int)z_ID
{
    return [[self safeObjectForKey:@"mID"] intValue];
}

- (NSString *)z_mID
{
    return [self safeObjectForKey:@"MID"];
}

- (NSString *)z_circleID
{
    return [self safeObjectForKey:@"circleID"];
}

- (NSString *)z_userID
{
    return [self safeObjectForKey:@"UID"];
}

- (NSString *)z_content
{
    return [self safeObjectForKey:@"MContent"];
}

- (NSString *)z_imgUrl
{
    return [self safeObjectForKey:@"ImgUrl"];
}

- (NSString *)z_vedioUrl
{
    return [self safeObjectForKey:@"VedioUrl"];
}

- (NSDate *)z_time
{
    double time = [[self safeObjectForKey:@"AddTime"] doubleValue];
    return [NSDate dateWithTimeIntervalSince1970:time];
}

- (NSString *)z_name
{
    return [self safeObjectForKey:@"UName"];
}

@end
