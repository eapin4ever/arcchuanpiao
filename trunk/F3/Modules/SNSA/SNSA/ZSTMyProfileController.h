//
//  ZSTMyProfileController.h
//  YouYun
//
//  Created by luobin on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef  void (^saveBlocked)(int type);

@interface ZSTMyProfileController : UIViewController
{
     CGFloat lastContentOffsetY;
    
    saveBlocked _btnSaveBlock;
    
    BOOL isAlert;
}

@property (nonatomic, assign) BOOL keyboardHasShow;

@property (nonatomic, retain) NSString *promptStr;

@property (nonatomic, assign) BOOL isComplete;

-(void)setBlock:(saveBlocked) btnClickedBlock;

@end
