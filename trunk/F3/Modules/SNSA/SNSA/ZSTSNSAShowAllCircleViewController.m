//
//  ZSTSNSAShowAllCircleViewController.m
//  SNSA
//
//  Created by zhangwanqiang on 14-4-25.
//
//

#import "ZSTSNSAShowAllCircleViewController.h"
#import "ZSTCircleEmptyTableViewCell.h"
#import "ZSTShowAllCircleTableViewCell.h"
#import "ZSTSNSARegisterViewController.h"
#import "ZSTRegisterViewController.h"
#import "ZSTMyProfileController.h"
#import "ZSTChatRoomViewController.h"
#import "CommonFunc.h"
#import "ZSTPersonalViewController.h"

@interface ZSTSNSAShowAllCircleViewController ()

@end

@implementation ZSTSNSAShowAllCircleViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed: @"framework_top_bg.png"];
	// Do any additional setup after loading the view, typically from a nib.
//    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"名片",nil) target:self selector:@selector (setttingButtonAction)];
    
    
    _circleTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 460+(iPhone5?88:0)+(IS_IOS_7?20:0)) style:UITableViewStylePlain];
    //0 37 320 480-44-49-37-20
    _circleTableView.delegate = self;
    _circleTableView.dataSource = self;
    _circleTableView.backgroundColor = [UIColor clearColor];
    _circleTableView.separatorStyle =  UITableViewCellSeparatorStyleNone;
    _circleTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview: _circleTableView];
    
    self.engine = [ZSTYouYunEngine engineWithDelegate:self];
    self.engine.moduleType = self.moduleType;
    _isRefreshing = NO;

//    if ([TKKeychainUtils getUserid:userKeyChainIdentifier]) {
//        self.circles = [TKDataCache getCacheDataByURLKey:[NSString stringWithFormat:@"%@_%d", ZSTHttpMethod_GetCircles, self.moduleType]];
//    }

    if (_refreshHeaderView == nil) {
        
        _refreshHeaderView = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f-250.0, self.view.frame.size.width, 250.0)];
        _refreshHeaderView.layer.contents = (id) ZSTModuleImage(@"module_snsa_pulltorefresh_background.png").CGImage;
        
        _refreshHeaderView.delegate = self;
        [_circleTableView addSubview:_refreshHeaderView];
        [_refreshHeaderView refreshLastUpdatedDate];
        [self performSelector:@selector(autoRefresh)
                   withObject:nil
                   afterDelay:0.6];
    }
    
    UIImageView *shadow = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_snsa_top_column_shadow.png")];
    shadow.frame = CGRectMake(0, 0, 320, -4);
    [_circleTableView addSubview:shadow];
    [shadow release];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshCirles) name:kNotificationLoginSucsess object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(login) name:kNotificationLogOutSuccess object:nil];
    
    [self registerObserver];
}

- (void)login
{
    
}

- (void)refreshCirles
{
    self.circles = nil;
    [_circleTableView reloadData];
    
    [self performSelector:@selector(autoRefresh)
               withObject:nil
               afterDelay:0.6];
}
#pragma mark - UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (self.circles == nil || ![self.circles count])? 1 : [self.circles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.circles == nil || ![self.circles count]) {
        
        static NSString *identifier = @"ZSTCircleEmptyTableViewCell";
        ZSTCircleEmptyTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[[ZSTCircleEmptyTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
        }
        return cell;
        
    }
    else {
         
        static NSString *identifier = @"ZSTTableViewCellIdentifier";
        ZSTShowAllCircleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[[ZSTShowAllCircleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
//            [cell.addressBookButton addTarget:self action:@selector(addressBookAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        cell.tag = indexPath.row;
        NSDictionary * dic = [_circles objectAtIndex:indexPath.row];
        cell.circle = dic;
        cell.selectedBackgroundView = [[[UIImageView alloc] initWithImage:[ZSTModuleImage(@"module_snsa_circle_item_bg_p.png") stretchableImageWithLeftCapWidth:0 topCapHeight:0]] autorelease];
        [cell setBlock:^(NSInteger tag) {
           
//            NSLog(@"--- block 输出 %d ---",tag);
////           NSString * phoneNumber = [ZSTF3Preferences shared].phoneNumber;
//            NSString * phoneNumber = [ZSTF3Preferences shared].loginMsisdn;
//            if (!phoneNumber||phoneNumber.length<11) {
//               
//                ZSTRegisterViewController * reg = [[ZSTRegisterViewController alloc]init];
//                reg.delegate = self;
//                 UINavigationController * nav = [[UINavigationController alloc]initWithRootViewController:reg];
//                [reg release];
////                reg.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
//                [self presentModalViewController:nav animated:YES];
//                [nav release];
//            }
//            else
//            {
                switch (tag) {
                    case 0:
                    {
//                        NSDictionary *userDic = [[CommonFunc sharedCommon] getCurrentUVO];
                        
//                        if (!userDic || ![userDic isKindOfClass:[NSNull class]]) {
//                            
//                            NSString *address = [userDic safeObjectForKey:@"Address"];
//                            NSString *email = [userDic safeObjectForKey:@"Email"];
//                            
//                            if (!address || address.length == 0 || !email || email.length == 0) {
//                                
//                                ZSTMyProfileController *myProfileController = [[ZSTMyProfileController alloc] init];
//                                myProfileController.isComplete = YES;
//                                __block __weak ZSTSNSAShowAllCircleViewController *wself = self;
//                                [myProfileController setBlock:^(int type) {
//                                    
//                                    if (type == 1) {
//                                        
//                                        [wself.engine joinCircle:[dic safeObjectForKey:@"CID"]];
//                                    }
//                                    [wself autoRefresh];
//                                }];
//                                UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:myProfileController];
//                                [self presentViewController:navController animated:YES completion:nil];
//                                [navController release];
//                                [myProfileController release];
//        
//                            } else {
//                            
//                                [self.engine joinCircle:[dic safeObjectForKey:@"CID"]];
//                                [TKUIUtil showHUD:self.view withText:@"提交中"];
//                            }
//                        }
                        
                        [self.engine joinCircle:[dic safeObjectForKey:@"CID"] flag:0];
                        [TKUIUtil showHUD:self.view withText:@"提交中"];

                    }

                        break;
                    case 1:
                        
                        break;
                    case 2:
                        
                        break;
                    case 3:
                    {
                        PopOver *popOver = [[PopOver alloc] initWithFrame:CGRectMake(0, -44, 320, 480+(iPhone5?88:0)+(IS_IOS_7?24:0))];
                        popOver.tag = indexPath.row;
                        [self.view addSubview:popOver];
                        
                        [popOver showPopOverWithTitle:@"提示"
                                              message:@"审核失败"
                                      backgroundImage:nil
                                             delegate:self
                                    cancelButtonTitle:nil
                                    otherButtonTitles:@"我知道了", nil];
                    }
                        
                        break;
                    default:
                        break;
                }
//            }
            
        }];
       
        return cell;
    }
}

- (void)registerDidSucceed
{
//    NSLog(@"-------- 调用成功 --------");
    ZSTMyProfileController *myProfileController = [[ZSTMyProfileController alloc] init];
    myProfileController.isComplete = YES;
    [myProfileController setBlock:^(int type) {
        
        [self autoRefresh];
    }];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:myProfileController];
    [self presentViewController:navController animated:YES completion:nil];
    [navController release];
    [myProfileController release];

}

-(void)jionCircleSuccess
{
    [TKUIUtil hiddenHUD];
//     [TKUIUtil alertInWindow:@"加入成功" withImage:nil];
    [self autoRefresh];
}
-(void)jionCircleFailed
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:@"失败了" withImage:nil];
    [_circleTableView reloadData];
}
- (void)setttingButtonAction
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"取消",@"取消") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"我的名片", nil), nil];
    [actionSheet showFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
    [actionSheet release];
}
#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        ZSTPersonalViewController *myProfileController = [[ZSTPersonalViewController alloc] init];
        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:myProfileController];
        [self presentViewController:navController animated:YES completion:nil];
        [navController release];
        [myProfileController release];
        
    } else if (buttonIndex == 1) {
        //        ZSTWebViewController *webViewController = [[ZSTWebViewController alloc] init];
        //        [webViewController setURL:@"http://d.pengyouyun.cn/"];
        //        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:webViewController];
        //        webViewController.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"取消",@"取消") style:UIBarButtonItemStyleBordered target:navController action:@selector(dismissModalViewController)] autorelease];
        //        [self presentViewController:navController animated:YES completion:nil];
        //        [navController release];
        //        [webViewController release];
    }
}

#pragma mark - UIAlertView delegate

- (void)popOver:(PopOver *)popOver clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1) {
        
//        ZSTPersonalViewController *myProfileController = [[ZSTPersonalViewController alloc] init];
//        UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:myProfileController];
//        [self presentViewController:navController animated:YES completion:nil];
//        [navController release];
//        [myProfileController release];
        
         NSDictionary * dic = [_circles objectAtIndex:popOver.tag];
         [self.engine joinCircle:[dic safeObjectForKey:@"CID"] flag:1];
    }
}

- (void)autoRefresh
{
    [_refreshHeaderView autoRefreshOnScroll:_circleTableView animated:YES];
}

-(void)registerObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ChangeMessageCount:) name:[NSString stringWithFormat:@"%@_%@", @"ZSTSNSMessageCount", @(self.moduleType)] object:nil];
}

- (void) ChangeMessageCount:(NSNotification*)notify
{
    NSMutableDictionary *dic = [_circles objectAtIndex:_row];
    [dic setObject:@"0" forKey:@"NotLookMsg"];
    [TKDataCache setCacheData:_circles andURLKey:[NSString stringWithFormat:@"%@_%@", ZSTHttpMethod_GetCircles, @(self.moduleType)]];
    
    [_circleTableView reloadData];
}

-(NSMutableArray *)calculateRows:(NSArray *) arr
{
    NSMutableArray * mutableArr = [NSMutableArray arrayWithArray:arr];
    //    NSInteger count = mutableArr.count;
    for (int i = 0 ;i<mutableArr.count;i++) {
        NSDictionary * dic = [mutableArr objectAtIndex:i];
        
        if ([dic isKindOfClass:[NSDictionary class]]) {
            if ([[dic safeObjectForKey:@"AuditStatus"] integerValue] == 1) {
                [mutableArr removeObject:dic];
                i--;
            }
        }
    }
    return mutableArr;
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (self.circles == nil || ![self.circles count])? tableView.height : (139/2.0);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    if (![self checkRegister] && self.circles.count > indexPath.row) {
//        NSLog(@"填写信息");
//        ZSTSNSARegisterViewController * registerViewController = [[ZSTSNSARegisterViewController alloc]init];
//        [self.navigationController pushViewController:registerViewController animated:YES];
//    }
//    else if (self.circles.count == indexPath.row) {
//        ZSTSNSAShowAllCircleViewController * showAllCircleViewController = [[ZSTSNSAShowAllCircleViewController alloc]init];
//        [self.navigationController pushViewController:showAllCircleViewController animated:YES];
//        
//    }
    if (self.circles != nil && [self.circles count]) {
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        int status = [[[_circles objectAtIndex:indexPath.row] safeObjectForKey:@"AuditStatus"] intValue];
        switch (status) {
            case 0:
                
                break;
            case 1:
            {
                ZSTChatRoomViewController *groupChatController = [[ZSTChatRoomViewController alloc] init];
                groupChatController.circle = [_circles objectAtIndex:indexPath.row];
                groupChatController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:groupChatController animated:YES];
                [groupChatController release];
                
            }
                break;
            case 2:
                [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"等待审核中，稍后再试", nil) withImage:nil];
                break;
            case 3:
                
                break;
            default:
                break;
        }
        
        _row = indexPath.row;
    }
}
#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}
- (void)getCirclesResponse:(NSArray *)theCircles
{
    if (![self.circles isEqualToArray:theCircles]) {
//        [TKDataCache setCacheData:theCircles andURLKey:[NSString stringWithFormat:@"%@_%d", ZSTHttpMethod_GetCircles, self.moduleType]];
        self.circles = [self calculateRows:theCircles];
        [_circleTableView reloadData];
    }
    
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:_circleTableView];
    _isRefreshing = NO;
}
#pragma mark EGORefreshTableHeaderDelegate Methods
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView *)view {
    _isRefreshing = YES;
    NSDictionary *circle = nil;
    
    NSMutableString * cInfo = [NSMutableString string];
    NSString *maxMsgID = nil;
    if ([_circles count] == 0) {
        [cInfo appendString:[NSString stringWithFormat:@"%@:%@",@"",@""]];
    } else {
        for (int i = 0; i < [_circles count]; i++) {
            circle = [_circles objectAtIndex:i];
            maxMsgID = [self.dao getMaxTopicID:[circle safeObjectForKey:@"CID"]];
            if (maxMsgID != nil) {
                [cInfo appendString:[NSString stringWithFormat:@"%@:%@",[circle safeObjectForKey:@"CID"],maxMsgID]];
                if (i < [_circles count]-1) {
                    [cInfo appendString:@","];
                }
            } else {
                [cInfo appendString:[NSString stringWithFormat:@"%@:%@",[circle safeObjectForKey:@"CID"],@"0"]];
                if (i < [_circles count]-1) {
                    [cInfo appendString:@","];
                }
            }
        }
    }
    
    NSLog(@"%@",cInfo);
    [self.engine getCircles:cInfo];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView *)view {
    return _isRefreshing;
}

- (NSDate *)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView *)view {
    return [NSDate date];
}


// 移除注册的者
-(void)removeObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[NSString stringWithFormat:@"%@_%@", @"ZSTSNSMessageCount", @(self.moduleType)] object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)dealloc
{
    [super dealloc];
}

@end
