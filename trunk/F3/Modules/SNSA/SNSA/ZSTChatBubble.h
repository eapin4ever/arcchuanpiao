//
//  ZSTChatTopArrowCell.h
//  YouYun
//
//  Created by luobin on 5/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef enum 
{
    ArrowDirection_Top,
    ArrowDirection_Left
} ArrowDirection;

@interface ZSTChatBubble : UIControl 
{
    NSMutableArray *_avatarImageViews;
    NSMutableArray *_attributedLabels;
}

@property (nonatomic, retain) NSDictionary *topic;

@property (nonatomic, assign) ArrowDirection arrowDirection;

@property (nonatomic, retain) UIFont *font;

- (id)initWithTopic:(NSDictionary *)topic;

+ (CGSize)suggestSizeForTopic:(NSDictionary *)topic font:(UIFont *)font;

+ (CGSize)suggestSizeForAttributedString:(NSAttributedString *)attributedString;

+ (CGSize)suggestSizeForAttributedString:(NSAttributedString *)attributedString width:(CGFloat)width;

+ (NSAttributedString *)attributedStringFromComment:(NSDictionary *)comment;

+(CGFloat) getWidthForAttributedString:(NSAttributedString *)attributedString;

+ (CGSize)suggestSizeForCmtAttributedString:(NSAttributedString *)attributedString;

+ (NSAttributedString *)smallAttributedStringFromComment:(NSDictionary *)comment;
    
@end
