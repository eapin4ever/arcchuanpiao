//
//  ZSTContactDetailHeaderCellCell.m
//  YouYun
//
//  Created by luobin on 6/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTContactDetailHeaderCell.h"
#import "FXLabel.h"
#import "ZSTSexSwitch.h"
#import <QuartzCore/QuartzCore.h>

@interface ZSTContactDetailHeaderCell()

@property (nonatomic, retain) TKAsynImageView *avtarImageView;
@property (nonatomic, retain) FXLabel *nameLabel;
@property (nonatomic, retain) UIImageView *adminImageView;
@property (nonatomic, retain) UIImageView *stateImageView;
@property (nonatomic, retain) UILabel *locationLabel;
@property (nonatomic, retain) ZSTSexSwitch *sexSwitch;

@end

@implementation ZSTContactDetailHeaderCell

@synthesize avtarImageView;
@synthesize nameLabel;
@synthesize adminImageView;
@synthesize stateImageView;
@synthesize locationLabel;
@synthesize sexSwitch;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        UIImageView *headerView = [[UIImageView alloc]  initWithFrame:CGRectMake(0, 0, 320, 100)];
        headerView.image = ZSTModuleImage(@"module_snsa_contact_detail_top_bg.png");
        
        UIImageView *avtarbg = [[[UIImageView alloc] initWithFrame:CGRectMake(20, 27, 49, 49)] autorelease];
        avtarbg.image = [ZSTModuleImage(@"module_snsa_icon_frame.png") stretchableImageWithLeftCapWidth:2 topCapHeight:2];
        avtarbg.backgroundColor = [UIColor clearColor];
        //        avtarbg.image = ZSTModuleImage(@"module_snsa_icon_frame.png");
        [headerView addSubview:avtarbg];

        
        self.avtarImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(2, 2, 45, 45)] autorelease];
        avtarImageView.defaultImage = ZSTModuleImage(@"module_snsa_default_avatar.png");
//        CALayer * avtarImageViewLayer = [avtarImageView layer];
//        [avtarImageViewLayer setMasksToBounds:YES];
//        [avtarImageViewLayer setCornerRadius:5.0];
        [avtarbg addSubview:avtarImageView];
        
        //联系人名称
        self.nameLabel = [[[FXLabel alloc] initWithFrame:CGRectMake(85, 28, 200, 14)] autorelease];
        self.nameLabel.backgroundColor = [UIColor clearColor];
        self.nameLabel.font = [UIFont systemFontOfSize:14];
        self.nameLabel.textColor = [UIColor colorWithRed:0.17 green:0.25 blue:0.32 alpha:1];
        self.nameLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:1];
        self.nameLabel.shadowOffset = CGSizeMake(0, 1.0f);
        self.nameLabel.shadowBlur = 1.0f;
        self.nameLabel.innerShadowColor = [UIColor colorWithWhite:0.0f alpha:0.8f];
        self.nameLabel.innerShadowOffset = CGSizeMake(1.0f, 1.0f);
        [headerView addSubview:nameLabel];
        
        //是否是管理员
        self.adminImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 29, 12.5, 12.5)] autorelease];
        self.adminImageView.image = ZSTModuleImage(@"module_snsa_master_head_icon.png");
//        [headerView addSubview:self.adminImageView];
        
        //是否已注册
        self.stateImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 29, 12.5, 12.5)] autorelease];
        self.stateImageView.image = ZSTModuleImage(@"module_snsa_install_state.png");
//        [headerView addSubview:self.stateImageView];
        
        self.sexSwitch = [[[ZSTSexSwitch alloc] initWithFrame:CGRectMake(85, 60, (393/2.0), (51/2.0))] autorelease];
        [headerView addSubview:self.sexSwitch];
        
        [self addSubview:headerView];
        [headerView release];
    }
    return self;
}

- (void)layoutSubviews
{
    CGSize size = [nameLabel.text sizeWithFont:self.nameLabel.font constrainedToSize:CGSizeMake(200, 14) lineBreakMode:NSLineBreakByCharWrapping];
    self.nameLabel.width = size.width;
    self.adminImageView.frame = CGRectMake(CGRectGetMaxX(nameLabel.frame) + 5, 29, 12.5, 12.5);
    self.stateImageView.frame = CGRectMake(CGRectGetMaxX(nameLabel.frame) + 22.5, 29, 12.5, 12.5);
}

- (void)dealloc
{
    self.avtarImageView = nil;
    self.nameLabel = nil;
    self.adminImageView = nil;
    self.stateImageView = nil;
    self.locationLabel = nil;
    self.sexSwitch = nil;
    [super dealloc];
}

@end
