//
//  ZSTGroupChatTableViewCell.h
//  YouYun
//
//  Created by luobin on 5/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TKPageTableViewController.h"

@class ZSTChatBubble;

@interface ZSTGroupChatTableViewCell : TKPageTableViewCell<UIActionSheetDelegate,MTZoomWindowDelegate>

@property (nonatomic, retain) TKAsynImageView *avtarImageView;

@property (nonatomic, retain) TKAsynImageView *pictureImageView;

@property (nonatomic, retain) ZSTChatBubble *chatBubble;

- (CGSize)getImageSize:(NSString *)imageUrl;

@end
