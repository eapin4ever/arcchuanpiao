//
//  HHCommentController.h
//  YouYun
//
//  Created by luo bin on 12-6-7.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "ZSTSNSAAttachmentView.h"
@class ZSTYouYunEngine;
@class MBProgressHUD;
@class ZSTSNSAAudioAverageView;

// Completion handler for DETweetComposeViewController
typedef void (^ZSTCreateTopicControllerCompletionHandler)(BOOL done); 

@interface ZSTCreateTopicController : UIViewController<UITextViewDelegate, ZSTYouYunEngineDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate, UINavigationControllerDelegate,AVAudioRecorderDelegate,ZSTAttachmentViewDelegate>
{
    AVAudioRecorder *recorder;
	AVAudioSession *session;
    
    NSString *_filePath;
    
    ZSTSNSAAudioAverageView *meter;
    NSTimer *recordTimer;
}

@property (nonatomic, retain) TKPlaceHolderTextView *textView;

@property (nonatomic, retain) UILabel *countLabel;

@property (nonatomic, retain) UIButton *pictureBtn;

@property (nonatomic, retain) UIButton * voiceBtn;

@property (nonatomic, retain) UIButton *sendBtn;

@property (nonatomic, retain) NSString *circleID;

@property (nonatomic, retain) ZSTYouYunEngine *engine;

@property (nonatomic, copy) ZSTCreateTopicControllerCompletionHandler completionHandler;

@property (nonatomic, retain) UIImagePickerController *iconPickerController;

@property (retain) AVAudioSession *session;
@property (retain) AVAudioRecorder *recorder;

- (void)sendButtonAction:(id)sender;

@end

@protocol ZSTHHCommentControllerDelegate <NSObject>

@optional

- (void)commentControllerDidFinish:(ZSTCreateTopicController *)commentController;

- (void)commentControllerDidCancel:(ZSTCreateTopicController *)commentController;

@end
