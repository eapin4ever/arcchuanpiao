//
//  ZSTGlobal+VideoC.h
//  VideoC
//
//  Created by LiZhenQu on 14-9-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <Foundation/Foundation.h>

#define VideoCBaseURL @"http://wxwap.pmit.cn/"

//#define VideoCBaseURL @"http://192.168.5.222:8885/"

#define GetVideoCHome VideoCBaseURL @"video/video!listLayOut.action?"

#define GetVideCategory VideoCBaseURL @"video/category!list.action?"

#define GetVideoCDetail VideoCBaseURL @"video!getVideoDetail.action?"

