//
//  ZSTVideoCHomeViewController.h
//  VideoC
//
//  Created by LiZhenQu on 14-9-2.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTModuleBaseViewController.h"
#import "ZSTAbnormalDataView.h"

@interface ZSTVideoCHomeViewController : ZSTModuleBaseViewController<ZSTAbnormalDataViewDelegate>

@property (nonatomic, retain) ZSTAbnormalDataView *abnormalDataView;

@end
