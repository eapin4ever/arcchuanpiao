//
//  ZSTVideoCVO.h
//  VideoC
//
//  Created by LiZhenQu on 14-9-2.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoCatalog : NSObject

@property (nonatomic, retain) NSString *catalogId;
@property (nonatomic, retain) NSString *catalogName;
@property (nonatomic, retain) NSString *videoUrl;
@property (nonatomic, assign) int duration;
@property (nonatomic, retain) NSString *author;

- (id)initWithDic:(NSDictionary *)dic;

@end

@interface ZSTVideoCVO : NSObject

@property (nonatomic, retain) NSString *videoId;
@property (nonatomic, retain) NSString *videoName;
@property (nonatomic, retain) NSString *videoImgUrl;
@property (nonatomic, retain) NSString *videoDescr;
@property (nonatomic, retain) NSMutableArray *catalogArray;

+ (id)zstVideoCWithDic:(NSDictionary *)dic;

@end
