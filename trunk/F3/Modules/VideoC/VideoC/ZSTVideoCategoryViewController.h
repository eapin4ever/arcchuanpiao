//
//  ZSTVideoCategoryViewController.h
//  VideoC
//
//  Created by LiZhenQu on 14-9-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTAbnormalDataView.h"

@interface ZSTVideoCategoryViewController : UIViewController<ZSTAbnormalDataViewDelegate>

@property (nonatomic, retain) ZSTAbnormalDataView *abnormalDataView;

@end
