//
//  ZSTVideoCVO.m
//  VideoC
//
//  Created by LiZhenQu on 14-9-2.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTVideoCVO.h"

@implementation VideoCatalog

- (id) initWithDic:(NSDictionary *)dic
{
    if (![dic isKindOfClass:[NSDictionary class]]) {
        
        return nil;
    }
    
    self = [super init];
    if (self) {
        
        self.catalogId = [dic safeObjectForKey:@"catalogId"];
        self.catalogName = [dic safeObjectForKey:@"catalogName"];
        self.videoUrl = [dic safeObjectForKey:@"videoUrl"];
        self.duration = [[dic safeObjectForKey:@"videoDuration"] intValue];
        self.author = [dic safeObjectForKey:@"author"];
    }
    
    return self;
}

@end

@implementation ZSTVideoCVO

+ (id)zstVideoCWithDic:(NSDictionary *)dic
{
    if (![dic isKindOfClass:[NSDictionary class]]) {
        
        return nil;
    }

    ZSTVideoCVO *info = [[ZSTVideoCVO alloc] init];
    info.videoId = [dic safeObjectForKey:@"videoId"];
    info.videoName = [dic safeObjectForKey:@"videoName"];
    info.videoImgUrl = [dic safeObjectForKey:@"videoImageUrl"];
    info.videoDescr = [dic safeObjectForKey:@"videoDescr"];
    info.catalogArray = [NSMutableArray array];
    
    NSArray *array = [NSArray array];
    array = [dic safeObjectForKey:@"videoCatalog"];
    if (![array isKindOfClass:[NSArray class]]) {
        return nil;
    }
    
    for(int i = 0; i < array.count; i++) {
        
        NSDictionary *mdic = [array objectAtIndex:i];
        
        VideoCatalog *catalog = [[VideoCatalog alloc] initWithDic:mdic];
        [info.catalogArray addObject:catalog];
    }
    
    return info;
}

@end
