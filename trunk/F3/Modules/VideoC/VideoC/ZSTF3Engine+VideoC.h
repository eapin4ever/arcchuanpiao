//
//  ZSTF3Engine+VideoC.h
//  VideoC
//
//  Created by LiZhenQu on 14-9-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTF3Engine.h"
#import "ZSTVideoCVO.h"

@protocol ZSTF3EngineVideoCDelegate <ZSTF3EngineDelegate>

@optional

- (void)getVideoCDetailDidSucceed:(ZSTVideoCVO *)vo;
- (void)getVideoCDetailDidFailed:(NSString *)message;

- (void)downVideoDidSucceed:(NSDictionary *)dic;
- (void)dowmVideoDidFailed:(NSString *)message;

@end


@interface ZSTF3Engine (VideoC)


- (void)getVideoCDetailWithDelegete:(id<ZSTF3EngineVideoCDelegate>)delegate
                            videoId:(NSString *)videoid;

- (void)downloadWithVideoId:(NSString *)videoid path:(NSString *)path url:(NSString *)url;

@end