//
//  ZSTHeadTableViewCell.m
//  VideoC
//
//  Created by LiZhenQu on 14-8-29.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTHeadTableViewCell.h"

@implementation ZSTHeadTableViewCell
@synthesize detailImgView;
@synthesize descriptionLabel;
@synthesize titleLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    
        detailImgView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(9, 40, 90, 90)];
        detailImgView.image = ZSTModuleImage(@"module_videoc_defaut.png");
        [self.contentView addSubview:detailImgView];
        
         _btn = [UIButton buttonWithType:UIButtonTypeCustom];
        _btn.frame = detailImgView.frame;
        [_btn setImage:ZSTModuleImage(@"module_videoc_title_play.png") forState:UIControlStateNormal];
        [_btn setImageEdgeInsets:UIEdgeInsetsMake(16, 16, 16, 16)];
        [_btn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_btn];
       
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(9, 10, 300, 21)];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont systemFontOfSize:18];
        titleLabel.textColor = RGBCOLOR(43, 43, 43);
        [self.contentView addSubview:titleLabel];
        
        descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(detailImgView.frame) + 9, 45, 200, 21)];
        descriptionLabel.backgroundColor = [UIColor clearColor];
        descriptionLabel.textColor = RGBCOLOR(56, 56, 56);
        descriptionLabel.font = [UIFont systemFontOfSize:13];
        [self.contentView addSubview:descriptionLabel];
        
        _line = [[UILabel alloc] initWithFrame:CGRectMake(0, 141, 320, 1)];
        _line.backgroundColor = RGBCOLOR(186, 186, 186);
        [self.contentView addSubview:_line];
        
        _cellHeight = 122.f;
    }
    return self;
}

- (void)configCell:(ZSTVideoCVO*)nvo
{
    if (!nvo) {
        
        return;
    }
    [titleLabel setNumberOfLines:0];
    titleLabel.lineBreakMode = UILineBreakModeWordWrap;
    titleLabel.text = nvo.videoName;
    CGSize size = [titleLabel.text sizeWithFont:[UIFont systemFontOfSize:18] constrainedToSize:CGSizeMake(300, 60) lineBreakMode:NSLineBreakByCharWrapping];
    titleLabel.frame = CGRectMake(9,10, 300, size.height);
    
    detailImgView.frame = CGRectMake(9, CGRectGetMaxY(titleLabel.frame) + 10, 90, 90);
    
    [descriptionLabel setNumberOfLines:0];
    descriptionLabel.lineBreakMode = UILineBreakModeWordWrap;
    descriptionLabel.text = nvo.videoDescr;
    size = [descriptionLabel.text sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:CGSizeMake(140, 80) lineBreakMode:NSLineBreakByCharWrapping];
    descriptionLabel.frame = CGRectMake(CGRectGetMaxX(detailImgView.frame) + 9, detailImgView.frame.origin.y + 5, 200, size.height);
    
    [detailImgView clear];
    detailImgView.url = [NSURL URLWithString:nvo.videoImgUrl];
    [detailImgView loadImage];
    
    _btn.frame = detailImgView.frame;
    _cellHeight = CGRectGetMaxY(detailImgView.frame) + 10;
    _line.frame = CGRectMake(0, _cellHeight-1, 320, 1);
}

- (CGFloat)cellHeight
{
    return _cellHeight;
}


- (void)setPlayBlock:(PlayBlocked)block
{
    _playBlocked = [block copy];
}

- (void)clickAction:(id)sender
{
    _playBlocked(self);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


@end
