//
//  ZSTDao+VideoC.m
//  VideoC
//
//  Created by LiZhenQu on 14-9-18.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTDao+VideoC.h"
#import "ZSTSqlManager.h"
#import "TKUtil.h"

#define CREATE_TABLE_CMD(moduleType) [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [VideoCList_%@] (\
                                                                    ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                    catalogid VARCHAR DEFAULT '', \
                                                                    catalogname VARCHAR DEFAULT '',\
                                                                    videourl VARCHAR DEFAULT '', \
                                                                    author VARCHAR DEFAULT '', \
                                                                    duration INTEGER  DEFAULT 0, \
                                                                    isdown INTEGER  DEFAULT 0\
                                                                    );", @(moduleType)]

TK_FIX_CATEGORY_BUG(VideoC)

@implementation ZSTDao (VideoC)

- (void)createTableIfNotExistForVideoCModule
{
    [ZSTSqlManager executeSqlWithSqlStrings:CREATE_TABLE_CMD(self.moduleType)];
}

- (BOOL)addVideo:(NSString *)videoid
     catalogName:(NSString *)catalogname
        videoUrl:(NSString *)videourl
        duration:(NSInteger)duration
          author:(NSString *)author
{
    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO VideoCList_%@ (catalogid, catalogname, videourl, duration, author) VALUES (?,?,?,?,?)", @(self.moduleType)],
                    [TKUtil wrapNilObject:videoid],
                    [TKUtil wrapNilObject:catalogname],
                    [TKUtil wrapNilObject:videourl],
                    [NSNumber numberWithInteger:duration],
                    [TKUtil wrapNilObject:author]];
    return success;

}

- (BOOL)setVideoC:(NSString *)videocid isDown:(BOOL)isdown
{
    NSString *updateSQL = [NSString stringWithFormat:@"UPDATE VideoCList_%@ set isdown = ?  WHERE catalogid = ?", @(self.moduleType)];
    if (![ZSTSqlManager executeUpdate:updateSQL, [NSNumber numberWithInt:isdown? 1 : 0], videocid]) {
        
        return NO;
    }
    return YES;
}

- (BOOL)getVideoCIsDown:(NSString *)videocid
{
    return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"SELECT isdown FROM VideoCList_%@ where catalogid = ?", @(self.moduleType)], videocid];
    
}

@end
