//
//  SandBoxPath.m
//  
//
//  Created by LiZhenQu on 14-8-29.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "SandBoxPath.h"

@implementation SandBoxPath


+ (NSString *)pathForDocuments
{
   return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];

}

+ (NSString *)pathForLibrary
{
    return  [NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}


+ (NSString *)pathForCaches
{
   return  [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
}

+ (NSString *)pathForTmp
{
    return  NSTemporaryDirectory();
}


#pragma mark --------------------------------------------------------------


+ (NSString *)pathForVideoC
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *aacPath = [[self pathForCaches] stringByAppendingPathComponent:@"VideoC"];
    if (![fm fileExistsAtPath:aacPath]) {
        NSDictionary *attributes = [NSDictionary dictionaryWithObject: [NSNumber numberWithUnsignedLong: 0777] forKey: NSFilePosixPermissions];
        [fm createDirectoryAtPath:aacPath withIntermediateDirectories: YES attributes: attributes error: nil];
    }
    return aacPath;
}

+ (NSString *)pathForTemp
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *aacPath = [[self pathForCaches] stringByAppendingPathComponent:@"Temp"];
    if (![fm fileExistsAtPath:aacPath]) {
        NSDictionary *attributes = [NSDictionary dictionaryWithObject: [NSNumber numberWithUnsignedLong: 0777] forKey: NSFilePosixPermissions];
        [fm createDirectoryAtPath:aacPath withIntermediateDirectories: YES attributes: attributes error: nil];
    }
    return aacPath;
}

@end
