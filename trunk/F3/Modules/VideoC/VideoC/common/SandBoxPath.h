//
//  SandBoxPath.h
//
//
//  Created by LiZhenQu on 14-8-29.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface SandBoxPath : NSObject

//基础路径
+ (NSString *)pathForDocuments;

+ (NSString *)pathForLibrary;

+ (NSString *)pathForCaches;

+ (NSString *)pathForTmp;


#pragma mark --------------------------------------------------------------

//文件路径

+ (NSString *)pathForVideoC;

+ (NSString *)pathForTemp;


@end
