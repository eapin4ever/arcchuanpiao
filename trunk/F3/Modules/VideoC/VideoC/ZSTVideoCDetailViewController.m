//
//  ZSTVideoCDetailViewController.m
//  VideoC
//
//  Created by LiZhenQu on 14-8-29.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTVideoCDetailViewController.h"
#import "ZSTHeadTableViewCell.h"
#import "ZSTListTableViewCell.h"
#import "SandBoxPath.h"
#import "Reachability.h"
#import "ZSTDao+VideoC.h"

@interface ZSTVideoCDetailViewController ()
{
    UIView *bottomView;
    
    NSInteger states;
    
    ASINetworkQueue *netWorkQueue;
    
    UIAlertView *wifiAlertView;
    
    UIAlertView *G2Alertview;
}

@property (nonatomic, retain) ASINetworkQueue *netWorkQueue;

@end

@implementation ZSTVideoCDetailViewController
@synthesize videoCVo;
@synthesize videoId;
@synthesize netWorkQueue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
     self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.dataArray = [[NSMutableArray alloc] initWithCapacity:10];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 460+(iPhone5?88:0)) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.separatorStyle =  UITableViewCellSeparatorStyleNone;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:self.tableView];
    
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 480+(iPhone5?88:0), 320, 67)];
    bottomView.backgroundColor = RGBCOLOR(29, 29, 29);
    bottomView.hidden = YES;
    bottomView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:bottomView];
    
    _imgView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(10, 8, 50, 50)];
    _imgView.image = ZSTModuleImage(@"module_videoc_defaut_img.png");
    [bottomView addSubview:_imgView];
    
    _playingProgress = [[UISlider alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imgView.frame) + 4, 0+(IS_IOS_7?10:0), 235, 2)];
    UIImage *minImage = ZSTModuleImage(@"module_videoc_progress.png");
    UIImage *maxImage = ZSTModuleImage(@"module_videoc_progress_track.png");
    UIImage *tumbImage= ZSTModuleImage(@"module_videoc_progress_metal_screw.png");
    [_playingProgress setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [_playingProgress setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [_playingProgress setThumbImage:tumbImage forState:UIControlStateNormal];
    _playingProgress.minimumValue = 0.0;
    _playingProgress.continuous = NO;
    [_playingProgress setThumbImage:tumbImage forState:UIControlStateHighlighted];
    _playingProgress.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [bottomView addSubview:_playingProgress];
    
    _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imgView.frame) + 6, 20, 160, 18)];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.font = [UIFont systemFontOfSize:16];
    _titleLabel.textColor = [UIColor whiteColor];
    [bottomView addSubview:_titleLabel];
    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(_titleLabel.frame.origin.x, CGRectGetMaxY(_titleLabel.frame) + 5, 40, 18)];
    label1.backgroundColor = [UIColor clearColor];
    label1.textColor = RGBCOLOR(160, 159, 159);
    label1.font = [UIFont systemFontOfSize:13];
    label1.text = @"作者：";
    [bottomView addSubview:label1];
    
    _authorLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(label1.frame) + 2, CGRectGetMaxY(_titleLabel.frame) + 5, 50, 18)];
    _authorLabel.backgroundColor = [UIColor clearColor];
    _authorLabel.font = [UIFont systemFontOfSize:13];
    _authorLabel.textColor = RGBCOLOR(160, 159, 159);
    [bottomView addSubview:_authorLabel];
    
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_authorLabel.frame) + 2, CGRectGetMaxY(_titleLabel.frame) + 5, 40, 18)];
    label2.backgroundColor = [UIColor clearColor];
    label2.textColor = RGBCOLOR(160, 159, 159);
    label2.font = [UIFont systemFontOfSize:13];
    label2.text = @"时长：";
    [bottomView addSubview:label2];
    
    _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(label2.frame), CGRectGetMaxY(_titleLabel.frame) + 5, 50, 18)];
    _timeLabel.backgroundColor = [UIColor clearColor];
    _timeLabel.font = [UIFont systemFontOfSize:13];
    _timeLabel.textColor = RGBCOLOR(160, 159, 159);
    [bottomView addSubview:_timeLabel];
    
//    _playbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _playbtn = [[PMRepairButton alloc] init];
    _playbtn.frame = CGRectMake(225, 15, 44, 44);
    [_playbtn addTarget:self action:@selector(playAction:) forControlEvents:UIControlEventTouchUpInside];
    [_playbtn setImage:ZSTModuleImage(@"module_videoc_bottom_play.png") forState:UIControlStateNormal];
    [bottomView addSubview:_playbtn];
    _playbtn.hidden = NO;
    _playbtn.enabled = YES;
    
//    _pausebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _pausebtn = [[PMRepairButton alloc] init];
    _pausebtn.frame = _playbtn.frame;
    [_pausebtn addTarget:self action:@selector(pauseAction:) forControlEvents:UIControlEventTouchUpInside];
    [_pausebtn setImage:ZSTModuleImage(@"module_videoc_bottom_pause.png") forState:UIControlStateNormal];
    [bottomView addSubview:_pausebtn];
    _pausebtn.hidden = YES;
    _pausebtn.enabled = NO;
    
    _nextbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _nextbtn.frame = CGRectMake(CGRectGetMaxX(_playbtn.frame), 15, 44, 44);
    [_nextbtn addTarget:self action:@selector(nextAction:) forControlEvents:UIControlEventTouchUpInside];
    [_nextbtn setImage:ZSTModuleImage(@"module_videoc_bottom_next.png") forState:UIControlStateNormal];
    [bottomView addSubview:_nextbtn];
    
    
    [self prepareToPlay];
}

- (void)viewWillAppear:(BOOL)animated
{
    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在加载..." , nil)];
    [self.engine getVideoCDetailWithDelegete:self videoId:self.videoId];
    
    [_tableView reloadData];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    if ([timer isValid]) {
        
        [timer invalidate];
    }
    
    if (self.player) {
        
        [self.player stop];
        [self.player.view removeFromSuperview];
        self.player = nil;
    }
    
    [self hideAbnormalDataview];
}

-(int)typeNetWork {
    int isDownLoad = 0;
    Reachability *r = [Reachability reachabilityWithHostName:@"www.apple.com"];
    switch ([r currentReachabilityStatus]) {
        case NotReachable:
           //   NSLog(@"没有网络");
            isDownLoad = 0;
            break;
        case ReachableViaWWAN:
           //   NSLog(@"正在使用3G网络");
            isDownLoad = 1;
            break;
        case ReachableViaWiFi:
            //  NSLog(@"正在使用wifi网络");
            isDownLoad = 2;
            break;
    }
    
    return isDownLoad;
}

- (void)prepareToPlay
{
    
    NSError *error;
    
    if (![[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error])
	{
		NSLog(@"Error: %@", [error localizedDescription]);
		return;
	}
    
    [[AVAudioSession sharedInstance] setActive:YES error:nil];

    if (!self.player) {
        self.player = [[MPMoviePlayerController alloc] init];
        self.player.controlStyle = MPMovieControlStyleNone;
        [self.player.view setFrame:CGRectMake(0, 0, 1, 1)];
        self.player.view.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.player.view];
    }
    
    // 注册一个播放结束的通知
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(musicFinishedCallback:)
                                                 name:MPMoviePlayerPlaybackDidFinishNotification
                                               object:self.player];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(musicDurationAvailableCallback:)
                                                 name:MPMovieDurationAvailableNotification
                                               object:self.player];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(musicPlaybackStateChangeCallback:)
                                                 name:MPMoviePlayerPlaybackStateDidChangeNotification
                                               object:self.player];

}

- (void) play
{
    if (states != 0) {
        ZSTListTableViewCell *Cell = (ZSTListTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:states inSection:0]];
        [Cell stop];
    }
    
    states = 1;
    
    ZSTListTableViewCell *tableCell = (ZSTListTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:states inSection:0]];
    
    [self completionFinished:tableCell];
    [tableCell playing];
    
     _nextbtn.enabled = YES;
    
    if (states == self.dataArray.count) {
        
        _nextbtn.enabled = NO;
    }
}

- (void) playAction:(id)sender
{
    _playbtn.hidden = YES;
    _playbtn.enabled = NO;
    _pausebtn.hidden = NO;
    _pausebtn.enabled = YES;
    
//    [self prepareToPlay];
    
//    [self.player play];
    
    ZSTListTableViewCell *tableCell = (ZSTListTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:states inSection:0]];
    [tableCell playing];
    
    NSString *path = [SandBoxPath pathForVideoC];
    NSString *videoFile = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp3",tableCell.videoVo.catalogId]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:videoFile]) {
        
        self.player.contentURL = [NSURL fileURLWithPath:videoFile];
        
    } else {
        
        NSError * error = nil;
        [fileManager removeItemAtPath:videoFile error:&error];
        
        self.player.contentURL = [NSURL URLWithString:tableCell.videoVo.videoUrl];
        [NSThread detachNewThreadSelector:@selector(download:) toTarget:self withObject:tableCell];
    }
    
    [self.player play];

    _nextbtn.enabled = YES;
    if (states == self.dataArray.count) {
        
        _nextbtn.enabled = NO;
    }
}

- (void) pauseAction:(id)sender
{
    _playbtn.hidden = NO;
    _playbtn.enabled = YES;
    _pausebtn.hidden = YES;
    _pausebtn.enabled = NO;
    [self.player pause];
    
    ZSTListTableViewCell *tableCell = (ZSTListTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:states inSection:0]];
    [tableCell stop];
}

- (void) nextAction:(id)sender
{
    _playbtn.hidden = YES;
    _playbtn.enabled = NO;
    _pausebtn.hidden = NO;
    _pausebtn.enabled = YES;
    _nextbtn.enabled = YES;
    
    if (states >= self.dataArray.count) {
        
        _pausebtn.hidden = YES;
        _playbtn.hidden = NO;
        _playbtn.enabled = YES;
        _nextbtn.enabled = NO;
        
        states = self.dataArray.count;
        
        ZSTListTableViewCell *tableCell = (ZSTListTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:states inSection:0]];
        [tableCell stop];
        
        return;
    }
    
    ZSTListTableViewCell *tableCell = (ZSTListTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:states inSection:0]];
    [tableCell stop];
    
    VideoCatalog *info = [self.dataArray objectAtIndex:states];
    _titleLabel.text = info.catalogName;
    _authorLabel.text = info.author;
    
//    double totaltime = self.player.duration;
//    int minute = totaltime / 60;
//    int second = (int)totaltime % 60;
//    _timeLabel.text = [NSString stringWithFormat:@"%d'%d\"",minute,second];
    
    int min = info.duration / 60;
    int sec = info.duration % 60;
    if (min > 0 ) {
        if (sec > 0) {
            _timeLabel.text = [NSString stringWithFormat:@"%d\'%d\"",min,sec];
        } else {
            
            _timeLabel.text = [NSString stringWithFormat:@"%d\'",min];
        }
    } else {
        
        _timeLabel.text = [NSString stringWithFormat:@"%d\"",sec];
    }
    
    states++;
    
    ZSTListTableViewCell *cell = (ZSTListTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:states inSection:0]];
    [cell playing];
    
    NSString *path = [SandBoxPath pathForVideoC];
    NSString *videoFile = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp3",cell.videoVo.catalogId]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:videoFile]) {
        
        self.player.contentURL = [NSURL fileURLWithPath:videoFile];
        
    } else {
        
        NSError * error = nil;
        [fileManager removeItemAtPath:videoFile error:&error];
        
        self.player.contentURL = [NSURL URLWithString:info.videoUrl];
        [NSThread detachNewThreadSelector:@selector(download:) toTarget:self withObject:tableCell];
    }

//    [self prepareToPlay];
//    self.player.contentURL = [NSURL URLWithString:info.videoUrl];
    [self.player play];
    
    self.playingProgress.value = 0;
    self.playingProgress.maximumValue = self.player.duration;
    
    if (states == self.dataArray.count) {
        
        _nextbtn.enabled = NO;
    }
}

- (void) download:(ZSTListTableViewCell *)cell
{
    NSString *path = [SandBoxPath pathForVideoC];
//    NSString *temp = [SandBoxPath pathForTemp];
    NSString *videoFile = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp3",cell.videoVo.catalogId]];
//    NSString *tempFile = [temp stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp3",cell.videoVo.catalogId]];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:videoFile]) {
        
        return;
    } else {
        
        NSError * error = nil;
        [fileManager removeItemAtPath:videoFile error:&error];
    }
    
//    ASINetworkQueue *que = [[ASINetworkQueue alloc] init];
//    self.netWorkQueue = que;
//    [que release];
//    
//    [self.netWorkQueue reset];
//    [self.netWorkQueue setShowAccurateProgress:YES];
//    [self.netWorkQueue go];
//    
//    ASIHTTPRequest *request = [[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:cell.videoVo.videoUrl]];
//    request.didFinishSelector = @selector(DidFinish);
//
//    //设置文件保存路径
//	[request setDownloadDestinationPath:videoFile];
//	//设置临时文件路径
////	[request setTemporaryFileDownloadPath:tempFile];
//    //设置进度条的代理,
//	[request setDownloadProgressDelegate:self];
//	//设置是是否支持断点下载
//	[request setAllowResumeForFileDownloads:NO];
//    
//    [request setUserInfo:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:[cell tag]],@"videoID",nil]];
//    
//    [self.netWorkQueue addOperation:request];
//	//收回request
//	[request release];
    
////    NSString *urlString=@"http://imgtest.itrends.com.cn:8080/media/video/57d_604f0_c9b69.mp4";
//    
//    cell.videoVo.videoUrl = [cell.videoVo.videoUrl stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
//    
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//    
//    [request setURL:[NSURL URLWithString: cell.videoVo.videoUrl]];
//    
//    [request setCachePolicy:NSURLRequestUseProtocolCachePolicy];
//    
//    [request setTimeoutInterval: 60];
//    [request setHTTPMethod:@"POST"];
//    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
//    [NSURLConnection sendAsynchronousRequest:request
//                                       queue:queue
//                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error){
//                               if (error) {
//                                   NSLog(@"Httperror:%@%d", error.localizedDescription,error.code);
//                               }else{
//                            
//                                   [data writeToFile:videoFile atomically:YES];
//                                   
//                                   NSString *fileName = [videoFile substringToIndex:(videoFile.length-4)];
//                                   NSArray *strArray = [fileName componentsSeparatedByString:@"/"];
//                                   NSString *videoId = [strArray lastObject];
//
//                               }
//                           }];
//    [request setHTTPShouldHandleCookies:YES];
    
    [self.engine downloadWithVideoId:cell.videoVo.catalogId path:videoFile url:cell.videoVo.videoUrl];
}

- (void)musicFinishedCallback:(NSNotification*)notify
{
    //视频播放对象
    MPMoviePlayerController* thePlayer = [notify object];
    
    //销毁播放通知
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:thePlayer];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMovieDurationAvailableNotification
                                                  object:thePlayer];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackStateDidChangeNotification
                                                  object:thePlayer];
//    if (self.player) {
//        [self.player release];
//        [self.player.view removeFromSuperview];
//        self.player = nil;
//    }
    
    [self nextAction:nil];
}

- (void)musicDurationAvailableCallback:(NSNotification *)notify
{
    
}

- (void)musicPlaybackStateChangeCallback:(NSNotification *)notify
{
    switch (self.player.playbackState) {
        case MPMoviePlaybackStateStopped:
            NSLog(@"MPMoviePlaybackStateStopped");
        {
            _playbtn.hidden = NO;
            _playbtn.enabled = YES;
            _pausebtn.hidden = YES;
            _pausebtn.enabled = NO;
            
            ZSTListTableViewCell *tableCell = (ZSTListTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:states inSection:0]];
            [tableCell stop];
        }
            break;
        case MPMoviePlaybackStatePlaying:
            NSLog(@"MPMoviePlaybackStatePlaying");
            if([timer isValid])
            {
                [timer invalidate];
                timer = nil;
            }
            _playbtn.hidden = YES;
            _playbtn.enabled = NO;
            _pausebtn.hidden = NO;
            _pausebtn.enabled = YES;
            timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(refreshTimeLableValueAndProgress) userInfo:nil repeats:YES];
            break;
        case MPMoviePlaybackStatePaused:
            NSLog(@"MPMoviePlaybackStatePaused");
            
            break;
        case MPMoviePlaybackStateInterrupted:
            NSLog(@"MPMoviePlaybackStateInterrupted");
            break;
        case MPMoviePlaybackStateSeekingForward:
            NSLog(@"MPMoviePlaybackStateSeekingForward");
            break;
        case MPMoviePlaybackStateSeekingBackward:
            NSLog(@"MPMoviePlaybackStateSeekingBackward");
            break;
        default:
            break;
    }
}

- (void)refreshTimeLableValueAndProgress
{
	NSTimeInterval currentTime = [self.player currentPlaybackTime];
    if (fabs(currentTime-self.playingProgress.value) > 5) {
        return;
    }
    
    [self updatePlayProgress:currentTime];
}

- (void)updatePlayProgress:(NSTimeInterval)currentTime
{
    self.playingProgress.value = currentTime;
}

- (void)setAnimations:(ZSTListTableViewCell *)cell
{
    if (states != cell.tag && states != 0) {
        
        ZSTListTableViewCell *tableCell = (ZSTListTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:states inSection:0]];
        [tableCell stop];
        
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:states inSection:0];
//        NSArray *indexArray = [NSArray arrayWithObject:indexPath];
//        [_tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationAutomatic];
    }

    //加入播放动画效果
    CALayer *transitionLayer = [[CALayer alloc] init];
    [CATransaction begin];
    [CATransaction setValue:(id)kCFBooleanTrue forKey:kCATransactionDisableActions];
    transitionLayer.opacity = 1.0;
    transitionLayer.contents = (id)cell.musicImgView.layer.contents;
    transitionLayer.frame = [[UIApplication sharedApplication].keyWindow convertRect:cell.musicImgView.bounds fromView:cell.musicImgView];
    [[UIApplication sharedApplication].keyWindow.layer addSublayer:transitionLayer];
    [CATransaction commit];
    
    //路径曲线
    UIBezierPath *movePath = [UIBezierPath bezierPath];
    [movePath moveToPoint:transitionLayer.position];
    CGPoint toPoint = CGPointMake(cell.musicImgView.center.x, bottomView.center.y+120);
    [movePath addLineToPoint:toPoint];

    //关键帧
    CAKeyframeAnimation *positionAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    positionAnimation.path = movePath.CGPath;
    positionAnimation.removedOnCompletion = YES;
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.beginTime = CACurrentMediaTime();
    group.duration = 1.5;
    group.animations = [NSArray arrayWithObjects:positionAnimation,nil];
    group.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    group.delegate = self;
    group.fillMode = kCAFillModeForwards;
    group.removedOnCompletion = NO;
    group.autoreverses= NO;
    
    [transitionLayer addAnimation:group forKey:@"opacity"];

    [self performSelector:@selector(completionFinished:) withObject:cell afterDelay:0.3f];
}

- (void) completionFinished:(ZSTListTableViewCell *)cell
{
    if (self.dataArray.count <= 0) {
        
        return;
    }
    
    [UIView animateWithDuration:0.4 animations:^{
        self.tableView.frame = CGRectMake(0, 0, 320, 460+(iPhone5?88:0)-44-68);
        bottomView.hidden = NO;
        bottomView.frame = CGRectMake(0, CGRectGetMaxY(self.tableView.frame)+1, 320, 67);
    }];
    
    self.playingProgress.value = 0;
//    [self prepareToPlay];
    self.player.contentURL = [NSURL URLWithString:cell.videoVo.videoUrl];
    self.playingProgress.maximumValue = cell.videoVo.duration;

    states = cell.tag;
    
    _playbtn.hidden = YES;
    _playbtn.enabled = NO;
    _pausebtn.hidden = NO;
    _pausebtn.enabled = YES;
    _nextbtn.enabled = YES;
    
    VideoCatalog *info = [self.dataArray objectAtIndex:(states-1)];
    _titleLabel.text = info.catalogName;
    _authorLabel.text = info.author;
    
    int min = info.duration / 60;
    int sec = info.duration % 60;
    if (min > 0 ) {
        if (sec > 0) {
            _timeLabel.text = [NSString stringWithFormat:@"%d\'%d\"",min,sec];
        } else {
            
            _timeLabel.text = [NSString stringWithFormat:@"%d\'",min];
        }
    } else {
        
        _timeLabel.text = [NSString stringWithFormat:@"%d\"",sec];
    }
    
    NSString *path = [SandBoxPath pathForVideoC];
    NSString *videoFile = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.mp3",info.catalogId]];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager fileExistsAtPath:videoFile]) {
        
        self.player.contentURL = [NSURL fileURLWithPath:videoFile];
        
    } else {
        
        NSError * error = nil;
        [fileManager removeItemAtPath:videoFile error:&error];

        self.player.contentURL = [NSURL URLWithString:info.videoUrl];
        ZSTListTableViewCell *cell = (ZSTListTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:states inSection:0]];
        [NSThread detachNewThreadSelector:@selector(download:) toTarget:self withObject:cell];
    }
    
    [self.player play];
    
    if (states == self.dataArray.count) {
        
        _nextbtn.enabled = NO;
    }
}

- (void)showAbnormalDataview
{
    _abnormalDataView = [[ZSTAbnormalDataView alloc] init];
    [_abnormalDataView setFrame:CGRectMake(0, 64, 320, 480 + (iPhone5?88:0)- 44 - (IS_IOS_7?20:0))];
    [_abnormalDataView initWithSubViews];
    _abnormalDataView.delegate = self;
    [self.view.window addSubview:_abnormalDataView];
    [self.view.window bringSubviewToFront:_abnormalDataView];
}

- (void)hideAbnormalDataview
{
    if (_abnormalDataView) {
        
        [_abnormalDataView removeFromSuperview];
        _abnormalDataView = nil;
    }
}

- (void)ZSTAbnormalDataView:(ZSTAbnormalDataView *)view reloadData:(id)sender
{
    [self hideAbnormalDataview ];
    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在加载..." , nil)];
    [self.engine getVideoCDetailWithDelegete:self videoId:self.videoId];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *headCellIdentifier = @"ZSTHeadTableViewCell";
    static NSString *listCellIdentifier = @"ZSTListTableViewCell";
    
    if (indexPath.row == 0) {
        
        ZSTHeadTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:headCellIdentifier];
        if (cell == nil)
        {
            cell = [[ZSTHeadTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:headCellIdentifier];
        }
        
        [cell configCell:videoCVo];
        
        [cell setPlayBlock:^(ZSTHeadTableViewCell *cell) {
            
            [self play];
        }];
        
        CGRect frame = cell.frame;
        frame.size.height = cell.cellHeight;
        cell.frame = frame;
        
        cell.tag = indexPath.row;
        
        return cell;
    } else {
        
        ZSTListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:listCellIdentifier];
        if (cell == nil)
        {
            cell = [[ZSTListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:listCellIdentifier];
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        }
        
        [cell stop];
        cell.tag = indexPath.row;
        
        if (states == indexPath.row && !_pausebtn.hidden) {
            
            [cell playing];
        }
        
        [cell setClickBlock:^(ZSTListTableViewCell *cell, NSInteger tag,int type) {
            
            if (type == 0) {
                
//                if ([self typeNetWork] == 0) {
//                    
//                }else if ([self typeNetWork] == 1) {
//                    
////                    [TKUIUtil alertInWindow:@"3G网络，采用在线播放" withImage:nil];
//                   G2Alertview = [[UIAlertView alloc]
//                                          initWithTitle:nil
//                                          message:NSLocalizedString(@"您正使用3G/2G网络，播放音乐将会产生较大的流量，确定要继续播放？播放过程中，将会自动下载，以便于您下次播放。", nil)
//                                          delegate:self
//                                          cancelButtonTitle:NSLocalizedString(@"取消", nil)
//                                          otherButtonTitles:NSLocalizedString(@"继续播放", nil), NSLocalizedString(@"下次不再提示", nil),nil];
//                    [G2Alertview show];
//                     G2Alertview.tag = indexPath.row;
//                    [G2Alertview release];
//
//                    
//                } else if ([self typeNetWork] == 2) {
//                    
////                    [TKUIUtil alertInWindow:@"wifi网络，开启自动缓存" withImage:nil];
//                    
//                    wifiAlertView = [[UIAlertView alloc]
//                                          initWithTitle:nil
//                                          message:NSLocalizedString(@"播放过程中，将会自动下载，以便于您下次播放。", nil)
//                                          delegate:self
//                                          cancelButtonTitle:NSLocalizedString(@"取消", nil)
//                                          otherButtonTitles:NSLocalizedString(@"我知道了", nil), NSLocalizedString(@"下次不再提示", nil),nil];
//                    [wifiAlertView show];
//                     wifiAlertView.tag = indexPath.row;
//                    [wifiAlertView release];
//                }
                
                [self setAnimations:cell];
            } else if (type == 1) {
                
                [self.player pause];
                _playbtn.hidden = NO;
                _playbtn.enabled = YES;
                _pausebtn.hidden = YES;
                _pausebtn.enabled = NO;
            }
        }];
        
        VideoCatalog *info = [self.dataArray objectAtIndex:(indexPath.row-1)];
        
       BOOL isdown = [self.dao getVideoCIsDown:info.catalogId];
        
        if (isdown) {
            
            cell.statesLabel.text = @"已下载";
        }
        
        [cell configCell:info];
        
        return cell;
    }
    
    return nil;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        
        ZSTHeadTableViewCell *cell = (ZSTHeadTableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:@"ZSTHeadTableViewCell"];
        if (cell == nil)
        {
            cell = [[ZSTHeadTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ZSTHeadTableViewCell"];
        }

        [cell configCell:videoCVo];
        return cell.cellHeight;
    }
    return 80.f;
}

- (void)request:(ASIHTTPRequest *)request didReceiveResponseHeaders:(NSDictionary *)responseHeaders {
	NSLog(@"didReceiveResponseHeaders-%@",[responseHeaders valueForKey:@"Content-Length"]);
    
    NSLog(@"contentlength=%f",request.contentLength/1024.0/1024.0);
    int bookid = [[request.userInfo objectForKey:@"videoID"] intValue];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    float tempConLen = [[userDefaults objectForKey:[NSString stringWithFormat:@"videoID_%d_contentLength",bookid]] floatValue];
    NSLog(@"tempConLen=%f",tempConLen);
    //如果没有保存,则持久化他的内容大小
    if (tempConLen == 0) {//如果没有保存,则持久化他的内容大小
        [userDefaults setObject:[NSNumber numberWithFloat:request.contentLength/1024.0/1024.0] forKey:[NSString stringWithFormat:@"videoID_%d_contentLength",bookid]];
    }
}

//ASIHTTPRequestDelegate,下载完成时,执行的方法
- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSLog(@"%@",request.userInfo);
}


- (void)getVideoCDetailDidSucceed:(ZSTVideoCVO *)vo
{
     [TKUIUtil hiddenHUD];
    [self hideAbnormalDataview];
    videoCVo = vo;
    
    if (!vo || vo.catalogArray.count <= 0) {
        
        [self showAbnormalDataview];
    }
    
    self.dataArray = videoCVo.catalogArray;
    
    [_imgView clear];
    _imgView.url = [NSURL URLWithString:vo.videoImgUrl];
    [_imgView loadImage];
    
     [self.tableView reloadData];
}

- (void)getVideoCDetailDidFailed:(NSString *)message
{
    [self hideAbnormalDataview];
    [TKUIUtil hiddenHUD];
//    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"加载失败", nil) withImage:nil];
    [self showAbnormalDataview];
}

- (void)downVideoDidSucceed:(NSDictionary *)dic
{
    if (!dic) {
        
        return;
    }
    NSString *videoid = [[dic safeObjectForKey:@"Params"] safeObjectForKey:@"videoId"];
    
    NSLog(@"----------%@-------",videoid);
    
    if (videoid && ![videoid isEqualToString:@"<null>"]) {
    
        [self.dao setVideoC:videoid isDown:1];
    }
    
     [self.tableView reloadData];
}

- (void)dowmVideoDidFailed:(NSString *)message
{
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
            
    } else  {
            
        ZSTListTableViewCell *cell = (ZSTListTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:alertView.tag inSection:0]];
            
        [self setAnimations:cell];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
