//
//  ZSTListTableViewCell.m
//  VideoC
//
//  Created by LiZhenQu on 14-8-29.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTListTableViewCell.h"

@implementation ZSTListTableViewCell
@synthesize videoVo;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.contentView.backgroundColor = [UIColor clearColor];
        
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 22, 160, 18)];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.font = [UIFont systemFontOfSize:16];
        _titleLabel.textColor = RGBCOLOR(43, 43, 43);
        [self.contentView addSubview:_titleLabel];
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(_titleLabel.frame) + 6, 30, 18)];
        label1.backgroundColor = [UIColor clearColor];
        label1.textColor = RGBCOLOR(160, 159, 159);
        label1.font = [UIFont systemFontOfSize:11];
        label1.text = @"作者:";
        [self.contentView addSubview:label1];
        
        _authorLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(label1.frame), CGRectGetMaxY(_titleLabel.frame) + 6, 80, 18)];
        _authorLabel.backgroundColor = [UIColor clearColor];
        _authorLabel.font = [UIFont systemFontOfSize:11];
        _authorLabel.textColor = RGBCOLOR(160, 159, 159);
        [self.contentView addSubview:_authorLabel];
        
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_authorLabel.frame) + 5, CGRectGetMaxY(_titleLabel.frame) + 6, 30, 18)];
        label2.backgroundColor = [UIColor clearColor];
        label2.textColor = RGBCOLOR(160, 159, 159);
        label2.font = [UIFont systemFontOfSize:11];
        label2.text = @"时长:";
        [self.contentView addSubview:label2];
        
        _timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(label2.frame), CGRectGetMaxY(_titleLabel.frame) + 6, 40, 18)];
        _timeLabel.backgroundColor = [UIColor clearColor];
        _timeLabel.font = [UIFont systemFontOfSize:11];
        _timeLabel.textColor = RGBCOLOR(160, 159, 159);
        [self.contentView addSubview:_timeLabel];
        
        _statesLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_timeLabel.frame), CGRectGetMaxY(_titleLabel.frame) + 6, 60, 18)];
        _statesLabel.backgroundColor = [UIColor clearColor];
        _statesLabel.font = [UIFont systemFontOfSize:11];
        _statesLabel.textColor = RGBCOLOR(160, 159, 159);
        _statesLabel.text = @"未下载";
        [self.contentView addSubview:_statesLabel];
        
//        _playbtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _playbtn = [[PMRepairButton alloc] init];
        _playbtn.frame = CGRectMake(251, 18, 44, 44);
        [_playbtn setImage:ZSTModuleImage(@"module_videoc_list_play.png") forState:UIControlStateNormal];
        _playbtn.hidden = NO;
        [_playbtn addTarget:self action:@selector(playAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_playbtn];
        
//        _pausebtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _pausebtn = [[PMRepairButton alloc] init];
        _pausebtn.frame = CGRectMake(251, 18, 44, 44);
        [_pausebtn setImage:ZSTModuleImage(@"module_videoc_list_pause.png") forState:UIControlStateNormal];
        _pausebtn.hidden = YES;
        [_pausebtn addTarget:self action:@selector(pauseAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_pausebtn];
        
        [_musicImgView = [UIImageView alloc] initWithFrame:CGRectMake(250, 40, 20, 19)];
        _musicImgView.image = ZSTModuleImage(@"module_videoc_yinfu.png");
        _musicImgView.hidden = YES;
        [self.contentView bringSubviewToFront:_musicImgView];
        [self.contentView addSubview:_musicImgView];
        
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, 79, 320, 1)];
        line.backgroundColor = RGBCOLOR(186, 186, 186);
        [self.contentView addSubview:line];
    }
    return self;
}

- (void)configCell:(VideoCatalog*)nvo
{
    if (!nvo) {
        
        return;
    }
    
    videoVo = nvo;
    
    _titleLabel.text = nvo.catalogName;
    _authorLabel.text = nvo.author;
    
    int min = nvo.duration/60;
    int sec = nvo.duration%60;
    if (min > 0 ) {
        if (sec > 0) {
            _timeLabel.text = [NSString stringWithFormat:@"%d\'%d\"",min,sec];
        } else {
            
            _timeLabel.text = [NSString stringWithFormat:@"%d\'",min];
        }
    } else {
        
        _timeLabel.text = [NSString stringWithFormat:@"%d\"",sec];
    }
}

- (void) setClickBlock:(clickBlocked)block
{
    _playBlocked = [block copy];
}

- (void)playAction:(id)sender
{
    
    _playbtn.userInteractionEnabled = NO;
    _playbtn.userInteractionEnabled = NO;
    
    [self performSelector:@selector(afterPlayAction) withObject:nil  afterDelay:1];
    
//    _pausebtn.hidden = NO;
//    _pausebtn.enabled = YES;
//    _playbtn.hidden = YES;
//    _playbtn.enabled = NO;
//    _musicImgView.frame = CGRectMake(250, 40, 20, 19);
//    [self.contentView bringSubviewToFront:_musicImgView];
//    [_musicImgView becomeFirstResponder];
    
//    CABasicAnimation* rotationAnimation;
//    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.x"];
//    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
//    [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    rotationAnimation.duration = 0.3;
//    rotationAnimation.RepeatCount = 1000;//你可以设置到最大的整数值
//    rotationAnimation.cumulative = NO;
//    rotationAnimation.removedOnCompletion = NO;
//    rotationAnimation.fillMode = kCAFillModeForwards;
//    [_musicImgView.layer addAnimation:rotationAnimation forKey:@"Rotation"];
    
//    _playBlocked(self,self.tag,0);
    
}

- (void) afterPlayAction
{
    _playbtn.userInteractionEnabled = YES;
    _pausebtn.userInteractionEnabled = YES;
    
    _pausebtn.hidden = NO;
    _pausebtn.enabled = YES;
    _playbtn.hidden = YES;
    _playbtn.enabled = NO;
    _musicImgView.frame = CGRectMake(250, 40, 20, 19);
    [self.contentView bringSubviewToFront:_musicImgView];
    [_musicImgView becomeFirstResponder];
    
    NSLog(@"self.tag = %@",@(self.tag));
    
    _playBlocked(self,self.tag,0);
}

- (void)pauseAction:(id)sender
{
//    _playbtn.userInteractionEnabled = NO;
//    _playbtn.userInteractionEnabled = NO;
//
//    _pausebtn.hidden = YES;
//    _pausebtn.enabled = NO;
//    _playbtn.hidden = NO;
//    _playbtn.enabled = YES;
//    
//    _playBlocked(self,self.tag,1);
    
     [self performSelector:@selector(afterPauseAction) withObject:nil  afterDelay:1];
}

- (void) afterPauseAction
{
    _playbtn.userInteractionEnabled = YES;
    _pausebtn.userInteractionEnabled = YES;
    
    _pausebtn.hidden = YES;
    _pausebtn.enabled = NO;
    _playbtn.hidden = NO;
    _playbtn.enabled = YES;
    
    _playBlocked(self,self.tag,1);

}

- (void) playing
{
    _pausebtn.userInteractionEnabled = NO;
    
     [_pausebtn performSelector:@selector(setUserInteractionEnabled:)  withObject:[NSNumber  numberWithBool:YES]  afterDelay:0.5];
     NSLog(@"self.tag = %@",@(self.tag));
    _pausebtn.hidden = NO;
    _pausebtn.enabled = YES;
    _playbtn.hidden = YES;
    _playbtn.enabled = NO;
}

- (void) stop
{
//    _playbtn.userInteractionEnabled = NO;
//    
//    [_playbtn performSelector:@selector(setUserInteractionEnabled:)  withObject:[NSNumber  numberWithBool:YES]  afterDelay:0.5];
    
    _pausebtn.hidden = YES;
    _pausebtn.enabled = NO;
    _playbtn.hidden = NO;
    _playbtn.enabled = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
