//
//  ZSTAbnormalDataView.m
//  VideoC
//
//  Created by LiZhenQu on 14-9-11.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTAbnormalDataView.h"

@implementation ZSTAbnormalDataView
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void) initWithSubViews
{
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(121, 40, 76, 78)];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.image = ZSTModuleImage(@"module_videoc_failded.png");
    [self addSubview:imageView];

    
    UILabel *textlabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 130, 300, 21)];
    textlabel.backgroundColor = [UIColor clearColor];
    textlabel.font = [UIFont systemFontOfSize:14];
    textlabel.textColor = RGBCOLOR(159, 159, 159);
    textlabel.textAlignment = NSTextAlignmentCenter;
    textlabel.text = @"亲，数据加载失败";
    [self addSubview:textlabel];

    
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(70, 180, 20, 20)];
    label1.backgroundColor = [UIColor clearColor];
    label1.font = [UIFont systemFontOfSize:12];
    label1.text = @"1";
    label1.textColor = RGBCOLOR(159, 159, 159);
    label1.textAlignment = NSTextAlignmentCenter;
    label1.layer.borderWidth = 1;
    label1.layer.cornerRadius = label1.frame.size.width / 2;
    label1.layer.borderColor = [UIColor grayColor].CGColor;
    label1.layer.masksToBounds = YES;
    [self addSubview:label1];

    
    UILabel *txtlabel1 = [[UILabel alloc] initWithFrame:CGRectMake(100, 180, 200, 21)];
    txtlabel1.backgroundColor = [UIColor clearColor];
    txtlabel1.font = [UIFont systemFontOfSize:13];
    txtlabel1.textColor  = RGBCOLOR(159, 159, 159);
    txtlabel1.text = @"请检查网络是否正常";
    [self addSubview:txtlabel1];
    
    UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(70, 205, 20, 20)];
    label2.backgroundColor = [UIColor clearColor];
    label2.font = [UIFont systemFontOfSize:12];
    label2.text = @"2";
    label2.textColor = RGBCOLOR(159, 159, 159);
    label2.textAlignment = NSTextAlignmentCenter;
    label2.layer.borderWidth = 1;
    label2.layer.cornerRadius = label2.frame.size.width / 2;
    label2.layer.borderColor = [UIColor grayColor].CGColor;
    label2.layer.masksToBounds = YES;
    [self addSubview:label2];
    
    UILabel *txtlabel2 = [[UILabel alloc] initWithFrame:CGRectMake(100, 205, 200, 21)];
    txtlabel2.backgroundColor = [UIColor clearColor];
    txtlabel2.font = [UIFont systemFontOfSize:13];
    txtlabel2.textColor  = RGBCOLOR(159, 159, 159);
    txtlabel2.text = @"手机存储空间是否足够";
    [self addSubview:txtlabel2];
    
    UILabel *label3 = [[UILabel alloc] initWithFrame:CGRectMake(70, 230, 20, 20)];
    label3.backgroundColor = [UIColor clearColor];
    label3.font = [UIFont systemFontOfSize:12];
    label3.text = @"3";
    label3.textColor = RGBCOLOR(159, 159, 159);
    label3.textAlignment = NSTextAlignmentCenter;
    label3.layer.borderWidth = 1;
    label3.layer.cornerRadius = label3.frame.size.width / 2;
    label3.layer.borderColor = [UIColor grayColor].CGColor;
    label3.layer.masksToBounds = YES;
    [self addSubview:label3];
    
    UILabel *txtlabel3 = [[UILabel alloc] initWithFrame:CGRectMake(100, 230, 200, 21)];
    txtlabel3.backgroundColor = [UIColor clearColor];
    txtlabel3.font = [UIFont systemFontOfSize:13];
    txtlabel3.textColor  = RGBCOLOR(159, 159, 159);
    txtlabel3.text = @"管理员偷懒，没有发布任何内容哦";
    [self addSubview:txtlabel3];
    
    UILabel *label4 = [[UILabel alloc] initWithFrame:CGRectMake(70, 256, 20, 20)];
    label4.backgroundColor = [UIColor clearColor];
    label4.font = [UIFont systemFontOfSize:12];
    label4.text = @"4";
    label4.textAlignment = NSTextAlignmentCenter;
    label4.textColor = RGBCOLOR(159, 159, 159);
    label4.layer.borderWidth = 1;
    label4.layer.cornerRadius = label4.frame.size.width / 2;
    label4.layer.borderColor = [UIColor grayColor].CGColor;
    label4.layer.masksToBounds = YES;
    
    UILabel *txtlabel4 = [[UILabel alloc] initWithFrame:CGRectMake(100, 256, 200, 21)];
    txtlabel4.backgroundColor = [UIColor clearColor];
    txtlabel4.font = [UIFont systemFontOfSize:13];
    txtlabel4.textColor = RGBCOLOR(159, 159, 159);
    txtlabel4.text = @"稍后试试吧";
    [self addSubview:txtlabel4];

    
    UIImage *imageN = ZSTModuleImage(@"module_videoc_btn_jion.png");
    CGFloat capWidth = imageN.size.width / 2;
    CGFloat capHeight = imageN.size.height / 2;
    imageN = [imageN stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    UIButton *reloadbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    reloadbtn.frame = CGRectMake(60, 300, 200, 34);
    [reloadbtn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
    [reloadbtn setBackgroundImage:imageN forState:UIControlStateNormal];
    [reloadbtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    reloadbtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [reloadbtn setTitle:@"重新加载" forState:UIControlStateNormal];
    
    [self addSubview:reloadbtn];
}

- (void)clickAction:(id)sender
{
    if (delegate && [delegate respondsToSelector:@selector(ZSTAbnormalDataView:reloadData:)]) {
        
        [delegate ZSTAbnormalDataView:self reloadData:sender];
    }
}

@end
