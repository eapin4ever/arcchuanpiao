//
//  ZSTF3Engine+VideoC.m
//  VideoC
//
//  Created by LiZhenQu on 14-9-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTF3Engine+VideoC.h"
#import "ZSTGlobal+VideoC.h"
#import "ZSTDao+VideoC.h"
#import "ZSTSqlManager.h"

@implementation ZSTF3Engine (VideoC)

- (void)getVideoCDetailWithDelegete:(id<ZSTF3EngineVideoCDelegate>)delegate
                            videoId:(NSString *)videoid
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:videoid forKey:@"videoId"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[[NSString stringWithFormat:@"%@videoId=%@",GetVideoCDetail,videoid] stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getVideoCResponse:userInfo:)
                                       userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:YES];

}

- (void)getVideoCResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (![[response.jsonResponse safeObjectForKey:@"status"] intValue]) {
        
        ZSTVideoCVO *info = [ZSTVideoCVO zstVideoCWithDic:[response.jsonResponse safeObjectForKey:@"datas"]];
        
        if ([self isValidDelegateForSelector:@selector(getVideoCDetailDidSucceed:)]) {
            
            [self.delegate getVideoCDetailDidSucceed:info];
            
            //入库
            [ZSTSqlManager beginTransaction];
            for (VideoCatalog *cateinfo in info.catalogArray) {
                
              BOOL i =  [self.dao addVideo:cateinfo.catalogId
                       catalogName:cateinfo.catalogName
                          videoUrl:cateinfo.videoUrl
                          duration:cateinfo.duration
                            author:cateinfo.author];
                
                NSLog(@"-------------%d----------????",i);
               
            }
            [ZSTSqlManager commit];

        }
    } else {
        
        if ([self isValidDelegateForSelector:@selector(getVideoCDetailDidFailed:)]) {
            
            [self.delegate getVideoCDetailDidFailed:[response.jsonResponse safeObjectForKey:@"msg"]];
        }
    }
}

- (void)downloadWithVideoId:(NSString *)videoid path:(NSString *)path url:(NSString *)url
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:videoid forKey:@"videoId"];
    
    [[ZSTCommunicator shared] openAPIDownLoadToPath:url
                                           filepath:path
                                             target:self
                                           selector:@selector(downVideoCResponse:userInfo:)
                                           userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]];
}

- (void)downVideoCResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"userInfo = %@",userInfo);
    
    
    if ([self isValidDelegateForSelector:@selector(downVideoDidSucceed:)]) {
        
        [self.delegate downVideoDidSucceed:userInfo];
    }

}

@end