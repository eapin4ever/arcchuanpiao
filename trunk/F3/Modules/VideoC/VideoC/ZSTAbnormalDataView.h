//
//  ZSTAbnormalDataView.h
//  VideoC
//
//  Created by LiZhenQu on 14-9-11.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZSTAbnormalDataView;

@protocol ZSTAbnormalDataViewDelegate <NSObject>

- (void)ZSTAbnormalDataView:(ZSTAbnormalDataView *)view reloadData:(id)sender;

@end

@interface ZSTAbnormalDataView : UIView

@property (nonatomic, assign) id<ZSTAbnormalDataViewDelegate>delegate;

- (void) initWithSubViews;

@end
