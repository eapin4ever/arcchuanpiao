//
//  ZSTVideoCHomeViewController.m
//  VideoC
//
//  Created by LiZhenQu on 14-9-2.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTVideoCHomeViewController.h"
#import "ZSTVideoCDetailViewController.h"
#import "ZSTGlobal+VideoC.h"
#import "ZSTVideoCategoryViewController.h"
#import "ZSTDao+VideoC.h"

@interface ZSTVideoCHomeViewController ()<UIWebViewDelegate>
{
    UIWebView *webView;
    NSString *videoId;
    NSString *url;
}

@end

@implementation ZSTVideoCHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)moduleApplication:(ZSTModuleApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [application.dao createTableIfNotExistForVideoCModule];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
     self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"分类",nil) target:self selector:@selector (categoryAction)];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[NSNumber numberWithInteger:self.moduleType] forKey:@"module_type"];
    [params setObject:[NSNumber numberWithInteger:self.moduleType] forKey:@"moduletype"];
    
    webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 460+(iPhone5?88:0)+(IS_IOS_7?20:0))];
    webView.backgroundColor = [UIColor clearColor];
    NSString *string = [NSString stringWithFormat:@"%@ecid=%@&msisdn=%@",GetVideoCHome,[ZSTF3Preferences shared].ECECCID,[ZSTF3Preferences shared].loginMsisdn];
    url = [string stringByAddingQuery:params];
    [webView setUserInteractionEnabled:YES];  //是否支持交互
    [webView setDelegate:self];
    webView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:webView];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [webView stopLoading];
    [self hideAbnormalDataview];
}

- (void)showAbnormalDataview
{
    [webView stopLoading];
    _abnormalDataView = [[ZSTAbnormalDataView alloc] init];
    [_abnormalDataView setFrame:CGRectMake(0, 0, 320, 480 + (iPhone5?88:0)- 44 - (IS_IOS_7?20:0))];
    [_abnormalDataView initWithSubViews];
    _abnormalDataView.delegate = self;
    [self.view addSubview:_abnormalDataView];
    [self.view bringSubviewToFront:_abnormalDataView];
}

- (void)hideAbnormalDataview
{
    if (_abnormalDataView) {
        
        [_abnormalDataView removeFromSuperview];
        _abnormalDataView = nil;
    }
}

- (void)ZSTAbnormalDataView:(ZSTAbnormalDataView *)view reloadData:(id)sender
{
    [self hideAbnormalDataview ];
    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在加载..." , nil)];
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在加载..." , nil)];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [TKUIUtil hiddenHUD];
    [self hideAbnormalDataview];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [TKUIUtil hiddenHUD];
    
    [self showAbnormalDataview];
}


- (void)categoryAction
{
    ZSTVideoCategoryViewController *controller = [[ZSTVideoCategoryViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}


- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType
{
#ifdef DEBUG
    NSLog(@"%@",[[request URL] description]);
#endif
    
    NSString *urlString = [[request URL] absoluteString];
    NSArray *urlComps = [urlString componentsSeparatedByString:@"//"];
    if([urlComps count] && [[[urlComps objectAtIndex:1] lowercaseString] isEqualToString:@"video"]) {
        
        NSArray *arrFucnameAndParameter = [(NSString*)[urlComps objectAtIndex:2] componentsSeparatedByString:@":"];
        NSString *funcStr = [arrFucnameAndParameter objectAtIndex:0];
        if ([funcStr isEqualToString:@"videoDetail"] && arrFucnameAndParameter.count > 1) {
            
            videoId = [arrFucnameAndParameter objectAtIndex:1];
            
            ZSTVideoCDetailViewController *controller = [[ZSTVideoCDetailViewController alloc] init];
            controller.videoId = videoId;
            controller.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:controller animated:YES];
        }
        
        return NO;
    }
    
    return YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
