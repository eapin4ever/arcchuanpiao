//
//  ZSTListTableViewCell.h
//  VideoC
//
//  Created by LiZhenQu on 14-8-29.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTVideoCVO.h"
#import <PMRepairButton.h>

@class ZSTListTableViewCell;
typedef  void (^clickBlocked)(ZSTListTableViewCell *cell,NSInteger tag,int type);

@interface ZSTListTableViewCell : UITableViewCell
{
    clickBlocked _playBlocked;
    
    VideoCatalog *videoVo;
}

@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *authorLabel;
@property (nonatomic, retain) UILabel *timeLabel;
@property (nonatomic, retain) PMRepairButton *playbtn;
@property (nonatomic, retain) PMRepairButton *pausebtn;
@property (nonatomic, retain) UILabel *statesLabel;

@property (nonatomic, retain) UIImageView *musicImgView;

@property (nonatomic, retain) VideoCatalog *videoVo;

- (void) setClickBlock:(clickBlocked)block;

- (void) playing;

- (void) stop;

- (void)configCell:(VideoCatalog*)nvo;

@end
