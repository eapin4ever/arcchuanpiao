//
//  ZSTVideoCDetailViewController.h
//  VideoC
//
//  Created by LiZhenQu on 14-8-29.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "ZSTVideoCVO.h"
#import "ZSTF3Engine+VideoC.h"
#import "TKAsynImageView.h"
#import "ZSTAbnormalDataView.h"
#import <PMRepairButton.h>

@interface ZSTVideoCDetailViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,AVAudioSessionDelegate,ZSTF3EngineVideoCDelegate,ZSTAbnormalDataViewDelegate,ASIHTTPRequestDelegate,UIAlertViewDelegate>
{
    NSTimer *timer;
    NSString *videoId;
}

@property (nonatomic, retain) UITableView *tableView;

@property (nonatomic, retain) NSMutableArray *dataArray;

@property (nonatomic, retain) TKAsynImageView *imgView;
@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *authorLabel;
@property (nonatomic, retain) UILabel *timeLabel;
@property (nonatomic, retain) PMRepairButton *pausebtn;
@property (nonatomic, retain) PMRepairButton *playbtn;
@property (nonatomic, retain) UIButton *nextbtn;
@property (nonatomic, retain) UISlider *playingProgress;

@property (nonatomic, retain) ZSTAbnormalDataView *abnormalDataView;

@property (nonatomic, retain) MPMoviePlayerController *player;

@property (nonatomic, retain) ZSTVideoCVO *videoCVo;

@property (nonatomic, retain) NSString *videoId;

@end
