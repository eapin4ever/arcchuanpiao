//
//  ZSTDao+VideoC.h
//  VideoC
//
//  Created by LiZhenQu on 14-9-18.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTDao (VideoC)

- (void)createTableIfNotExistForVideoCModule;

/**
 *	@brief 添加新闻
 *
 *  @param 	msgID           新闻ID
 *	@param 	categoryID      分类ID
 *	@param 	title           新闻标题
 *	@param 	source         新闻信息摘要
 *	@param 	iconurl      ICon图片编号
 *	@param 	orderNum        新闻信息来源
 *  @param  addTime         添加时间
 *	@return	操作是否成功
 */
- (BOOL)addVideo:(NSString *)videoid
     catalogName:(NSString *)catalogname
        videoUrl:(NSString *)videourl
        duration:(NSInteger)duration
          author:(NSString *)author;

/**
 *	@brief  设置新闻是否已读
 *
 *  @param  isRead         设置是否已读
 *	@return	操作是否成功
 */
- (BOOL)setVideoC:(NSString *)videocid isDown:(BOOL)isdown;

/**
 *	@brief  获取新闻是否已读
 *
 *  @param  isRead         设置是否已读
 *	@return	新闻是否已读
 */
- (BOOL)getVideoCIsDown:(NSString *)videocid;

@end
