//
//  ZSTHeadTableViewCell.h
//  VideoC
//
//  Created by LiZhenQu on 14-8-29.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTVideoCVO.h"
#import "TKAsynImageView.h"

@class ZSTHeadTableViewCell;
typedef  void (^PlayBlocked)(ZSTHeadTableViewCell *cell);

@interface ZSTHeadTableViewCell : UITableViewCell
{
    TKAsynImageView *detailImgView;
    UILabel *titleLabel;
    UILabel *descriptionLabel;
    
    PlayBlocked _playBlocked;
}

@property (nonatomic, retain) TKAsynImageView *detailImgView;
@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UILabel *descriptionLabel;
@property (nonatomic, retain) UILabel *line;
@property (nonatomic, retain) UIButton *btn;

@property (nonatomic, assign) CGFloat cellHeight;

- (void)configCell:(ZSTVideoCVO*)nvo;

- (void)setPlayBlock:(PlayBlocked)block;

@end
