    //
//  contact.m
//  Net_Information
//
//  Created by huqinghe on 11-6-5.
//  Copyright 2011 Shinetechchina.com. All rights reserved.
//

#import "ContactData.h"
#import "pinyin.h"
#import "ContactPickerViewController.h"
#import "StringUtil.h"
#import "ChineseToPinyin.h"
#import "ZSTLogUtil.h"

@implementation ContactPickerViewController

@synthesize contacts = _contacts;
@synthesize filteredArray = _filteredArray;
@synthesize searchBar = _searchBar;
@synthesize contactTableview = _contactTableview;
@synthesize myArray;
@synthesize createNMSDialogViewController = _createNMSDialogViewController;
@synthesize contactInfoArray = _contactInfoArray;
@synthesize delegate= _delegate;
@synthesize searchDC = _searchDC;
@synthesize contactNameArray = _contactNameArray;
@synthesize sectionArray = _sectionArray;

@synthesize aBPersonNav = _aBPersonNav;
@synthesize aBNewPersonNav = _aBNewPersonNav;

#define BARBUTTON(TITLE, SELECTOR)		[[[UIBarButtonItem alloc] initWithTitle:TITLE style:UIBarButtonItemStylePlain target:self action:SELECTOR] autorelease]//UIBarButtonItem

- (void)setContactInfoArray:(NSArray *)contactInfoArray
{
    [_contactInfoArray release];
    _contactInfoArray = [contactInfoArray mutableCopy];
}

-(void)confirm
{
    if (_contactInfoArray != nil) {
        [self dismissViewControllerAnimated:YES completion:nil];
        
        if ([_delegate respondsToSelector:@selector(contactPickerViewDidFinishSelect:)]) //确定以后把所选择的号码传到显示界面
        {
            [_delegate contactPickerViewDidFinishSelect:self];
        }
    }
  
}

-(void)cancel
{
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([_delegate respondsToSelector:@selector(contactPickerViewCancelSelect:)]) //确定以后把所选择的号码传到显示界面
    {
        [_delegate contactPickerViewCancelSelect:self];
    }
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_contactTableview resignFirstResponder];   
    
}


-(void)initData{
    _contacts = [[NSMutableArray arrayWithCapacity: self.contacts.count] retain];
    self.sectionArray = [NSMutableArray array];
	for (int i = 0; i < 27; i++) 
        [self.sectionArray addObject:[NSMutableArray array]];
    
    NSArray *contacts = [ABContactsHelper contacts];//接收电话簿所有信息

  
	for (ABContact *contact in contacts)
	{
		for (NSString *number in contact.phoneArray) 
		{
			ContactInfo *contactInfo = [[ContactInfo alloc] init];

			contactInfo.name = contact.contactName;
			
            number = [number stringByReplacingOccurrencesOfString: @"(" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @")" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @"\r" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @"\n" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @"\t" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @"-" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @" " withString: @""];
            number = [number stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            contactInfo.phoneNum = number;
            
			[_contacts addObject: contactInfo];
				
            NSString *sectionName;
            if (contact.contactName==nil || [contact.contactName length] == 0)
                sectionName = @"#";
            else if(isalpha([contact.contactName characterAtIndex:0])){
                sectionName = [[NSString stringWithFormat:@"%c", [contact.contactName characterAtIndex:0]] uppercaseString];
            }
            else if([ContactData searchResult:contact.contactName searchText:@"曾"])
                sectionName = @"Z";
            else if([ContactData searchResult:contact.contactName searchText:@"解"])
                sectionName = @"X";
            else if([ContactData searchResult:contact.contactName searchText:@"仇"])
                sectionName = @"Q";
            else if([ContactData searchResult:contact.contactName searchText:@"朴"])
                sectionName = @"P";
            else if([ContactData searchResult:contact.contactName searchText:@"查"])
                sectionName = @"Z";
            else if([ContactData searchResult:contact.contactName searchText:@"能"])
                sectionName = @"N";
            else if([ContactData searchResult:contact.contactName searchText:@"乐"])
                sectionName = @"Y";
            else if([ContactData searchResult:contact.contactName searchText:@"单"])
                sectionName = @"S";
            else
                sectionName = [[NSString stringWithFormat:@"%c",pinyinFirstLetter([contact.contactName characterAtIndex:0])] uppercaseString];
            
            NSUInteger firstLetter = [ALPHA rangeOfString:[sectionName substringToIndex:1]].location;
            if (firstLetter != NSNotFound) {
                [[self.sectionArray objectAtIndex:firstLetter] addObject:contactInfo];
            }
            [contactInfo release];
        }
	}
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"联系人";
    
    if ([_delegate respondsToSelector:@selector(contactPickerViewDidBeginSelect:)]) {
        [_delegate contactPickerViewDidBeginSelect:self];
    }

	UIImageView *imageview =[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"backgroundimage.png"]];
	imageview.frame = CGRectMake(0, 0, 320, 480);
	[self.view addSubview:imageview];
	[imageview release];
	
	
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"取消",@"取消") style:UIBarButtonItemStylePlain target:self action:@selector(cancel)];
    self.navigationItem.leftBarButtonItem = cancelButton;
    [cancelButton release];
    
    _confirmButton = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"确定", @"") style:UIBarButtonItemStylePlain target:self action:@selector(confirm)];
    self.navigationItem.rightBarButtonItem = _confirmButton;
    _confirmButton.enabled = ([_contactInfoArray count] != 0);
    [_confirmButton release];
	
	
	_contactTableview = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 480-44) style:UITableViewStylePlain];
	_contactTableview.backgroundColor = [UIColor whiteColor];
	_contactTableview.dataSource = self;
	_contactTableview.delegate = self;
    
  
   
    if (_contactInfoArray == nil) {
        _contactInfoArray = [[[NSMutableArray alloc] init] retain];
    }
    
	//create searchBar
	_searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 44.0f)];
	_searchBar.delegate = self;
	//_searchBar.showsCancelButton = YES;
	_searchBar.placeholder = @"输入字母、汉字或电话号码搜索";
	_searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
	_searchBar.autocapitalizationType = UITextAutocapitalizationTypeNone;
	_searchBar.keyboardType = UIKeyboardTypeAlphabet;
	self.contactTableview.tableHeaderView = self.searchBar;
    
  	// Create the search display controller
	self.searchDC = [[[UISearchDisplayController alloc] initWithSearchBar:self.searchBar contentsController:self] autorelease];
	self.searchDC.searchResultsDataSource = self;
	self.searchDC.searchResultsDelegate = self;	
    
    NSMutableArray *filterearray =  [[NSMutableArray alloc] init];
	self.filteredArray = filterearray;
	[filterearray release];
	
    _tempArrayContactInfoArray = [[NSMutableArray arrayWithCapacity:10] retain];
    
    [self initData];//获取电话本信息
    
	[self.view addSubview:self.contactTableview];
    [ZSTLogUtil logUserAction:NSStringFromClass([ContactPickerViewController class])];
	 
}

- (void)viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	[self.contactTableview reloadData];
}

#pragma mark -
#pragma mark UITabelViewDataSource


- (NSInteger)numberOfSectionsInTableView:(UITableView *)aTableView 
{
	if(aTableView == self.contactTableview) return 27;
	return 1; 
}


- (NSArray *)sectionIndexTitlesForTableView:(UITableView *)aTableView 
{
	if (aTableView == self.contactTableview)  // regular table
	{
		NSMutableArray *indices = [NSMutableArray arrayWithObject:UITableViewIndexSearch];
		for (int i = 0; i < 27; i++) 
			if ([(NSMutableArray *)[self.sectionArray objectAtIndex:i] count])
				[indices addObject:[[ALPHA substringFromIndex:i] substringToIndex:1]];
		//[indices addObject:@"\ue057"]; // <-- using emoji
		return indices;
	}
	else return nil; // search table
}


- (NSInteger)tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
	if (title == UITableViewIndexSearch) 
	{
		[self.contactTableview scrollRectToVisible:self.searchBar.frame animated:NO];
		return -1;
	}
	return [ALPHA rangeOfString:title].location;
}



- (NSString *)tableView:(UITableView *)aTableView titleForHeaderInSection:(NSInteger)section
{
	if (aTableView == self.contactTableview) 
	{
		if ([(NSMutableArray *)[self.sectionArray objectAtIndex:section] count] == 0) return nil;
		return [NSString stringWithFormat:@"%@", [[ALPHA substringFromIndex:section] substringToIndex:1]];
	}
	else return nil;
}


- (NSInteger)tableView:(UITableView *)aTableView numberOfRowsInSection:(NSInteger)section 
{
	
	// Normal table
	if (aTableView == self.contactTableview) {
          return [(NSMutableArray *)[self.sectionArray objectAtIndex:section] count];
    }else{
           return self.filteredArray.count;
           [self.contactTableview reloadData];
    }
 
}

- (UITableViewCell *)tableView:(UITableView *)aTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
	UITableViewCellStyle style =  UITableViewCellStyleSubtitle;
	UITableViewCell *cell = [aTableView dequeueReusableCellWithIdentifier:@"ContactCell"];
	if (!cell) 
    {
        cell = [[[UITableViewCell alloc] initWithStyle:style reuseIdentifier:@"ContactCell"] autorelease];
    }
    
    ContactInfo *contact;
	
	// Retrieve the crayon and its color
	if (aTableView == self.contactTableview){
		contact = (ContactInfo *)[(NSMutableArray *)[self.sectionArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    }
	else
    {
		contact = (ContactInfo *)[self.filteredArray objectAtIndex:indexPath.row];
    }
	cell.textLabel.text = contact.name;
	
    cell.detailTextLabel.text = contact.phoneNum;
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    

    
    for (ContactInfo *contactInfo in self.contactInfoArray)
    { 
     
        if ([contact.phoneNum isEqualToString:contactInfo.phoneNum] && [contact.name isEqualToString:contact.name])

        {
            cell.accessoryType = UITableViewCellAccessoryCheckmark;
            [_tempArrayContactInfoArray addObject:contact];
        }
    }
    

	return cell;
}

#pragma mark -
#pragma mark UITabelViewDelegate


- (void)tableView:(UITableView *)aTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [aTableView cellForRowAtIndexPath: indexPath];
    
	ContactInfo *contactInfo;
    
    if (cell.accessoryType)
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
        if (aTableView == self.contactTableview){
             contactInfo = (ContactInfo *)[(NSMutableArray *)[self.sectionArray objectAtIndex: indexPath.section] objectAtIndex:indexPath.row];

        }else{
            contactInfo = (ContactInfo *)[self.filteredArray objectAtIndex: indexPath.row];
        }
        
        if ([_tempArrayContactInfoArray count]) {
            _contactInfoArray = _tempArrayContactInfoArray;
        }
            
        if ([_contactInfoArray containsObject:contactInfo]) {
            [_contactInfoArray removeObject:contactInfo];
        }

    }else{
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        if (aTableView == self.contactTableview) {
            contactInfo = (ContactInfo *)[(NSMutableArray *)[self.sectionArray objectAtIndex: indexPath.section] objectAtIndex:indexPath.row];

        }else{
            contactInfo = (ContactInfo *)[self.filteredArray objectAtIndex: indexPath.row];
        }
 
        if(![_contactInfoArray containsObject:contactInfo])

        {
            [_contactInfoArray addObject:contactInfo];

        }
    }

    _confirmButton.enabled = ([_contactInfoArray count] != 0);//这里将选择的联系人放入一个数组！！！！
}

#pragma mark -
#pragma mark UISearchBarDelegate

// Via Jack Lucky
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    
	[self.searchBar setText:@""]; 
	[self.searchBar setFrame:CGRectMake(0.0f, 0.0f, 320.0f, 44.0f)];
	self.contactTableview.tableHeaderView = self.searchBar;	
}



-(NSMutableArray*)searchByCondition:(NSString*)condition//搜索功能实现
{	
    // Search table
    
    if (self.filteredArray) {
        [self.filteredArray removeAllObjects];
    }
    
    BOOL isAlpha = [condition isAlpha];
    
    BOOL isNumber = [condition isNumber];
    
    for (ContactInfo *contactInfo in self.contacts) 
    { 
        if(contactInfo.name !=nil && [ContactData searchResult:contactInfo.name searchText:condition]){
            
            [self.filteredArray addObject:contactInfo];
            
        }else if(isNumber && [ContactData searchResult:contactInfo.phoneNum searchText:condition]){
            [self.filteredArray addObject:contactInfo];
            
        }else if (isAlpha && contactInfo.name != nil) 
        {
            NSString *pinYin = [ChineseToPinyin pinyinFromChiniseString:contactInfo.name];
            if (pinYin !=nil && [ContactData searchResult:pinYin searchText:[condition uppercaseString]]) {
               [self.filteredArray addObject:contactInfo]; 
            }
            
        }
    }
    
    return self.filteredArray;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
	
	if([searchText length] == 0)
	{
		self.sectionArray = self.sectionArray;
		[self.contactTableview reloadData];
		return;
	}
	
	self.filteredArray = [self searchByCondition:searchText];
	
	[self.contactTableview reloadData];
	
}




/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations.
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc. that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    
	[_filteredArray release];
	_contactInfoArray = nil;
    [_contactArray release];
    _contacts = nil;
    [_searchBar release];
	[_contactTableview release];
    [super dealloc];
    
}


@end
