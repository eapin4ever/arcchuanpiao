//
//  BusinessAppWebView.m
//  F3
//
//  Created by xuhuijun on 12-3-9.
//  Copyright 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTBusinessAppWebViewController.h"
#import "ZSTLogUtil.h"
#import "TKUIUtil.h"
#import "ZSTUtils.h"
#import "ZSTModule.h"
#import "ZSTModuleManager.h"

#define isRetina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

#define kCurrntWidth 320
#define kCurrntHeight 480
#define kRetinaWidth 640
#define kRetinaHeight 960


@implementation ZSTBusinessAppWebViewController

@synthesize appInfo = _appInfo;
@synthesize url = _url;
@synthesize webCanGoBack;
@synthesize webCanGoForward;

- (void)dealloc
{
    [_scrollToolBarView stopClock];
    [_scrollToolBarView release];
    [_businessAppWebView release];
    [_url release];
    [super dealloc];
}

#pragma mark ------------- button selector------------------------------
-(void)home
{
    [_businessAppWebView loadRequest:[NSURLRequest requestWithURL:_url]];  
    [_scrollToolBarView reclock];
    
}

-(void)refresh
{
    [_businessAppWebView reload];
    [_scrollToolBarView reclock];
}
-(void)goBack
{
    [_businessAppWebView goBack];
    [_scrollToolBarView reclock];
    
}
-(void)goForward
{
    [_businessAppWebView goForward];
    [_scrollToolBarView reclock];
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:self.appInfo.Name];
    
    ZSTF3Preferences *dataManager = [ZSTF3Preferences shared];
    
    self.view.backgroundColor = [UIColor whiteColor];
    _businessAppWebView = [[UIWebView alloc] init];
    _businessAppWebView.frame = CGRectMake(0, 0, 320, 480-20-44+(iPhone5?88:0));
    _businessAppWebView.delegate = self;
    _businessAppWebView.scalesPageToFit = YES;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:dataManager.loginMsisdn forKey:@"mobile"];
    [params setObject:[UIDevice machine] forKey:@"ua"];
    [params setObject:[NSNumber numberWithInt:isRetina?kRetinaWidth:kCurrntWidth] forKey:@"w"];
    [params setObject:[NSNumber numberWithInt:isRetina?kRetinaHeight:kCurrntHeight] forKey:@"h"];
    if ([self.appInfo.Url rangeOfString:@"ec"].location == NSNotFound) {
        [params setObject:dataManager.ECECCID forKey:@"ec"];
    }
    if ([self.appInfo.Url rangeOfString:@"module_type"].location == NSNotFound) {
        [params setObject:[NSNumber numberWithInteger:self.appInfo.ModuleType] forKey:@"module_type"];
    }
    [params setObject:dataManager.platform forKey:@"platform"];
    NSString *currentAppUrlStr = [self.appInfo.Url stringByAddingQuery:params];

    
       _url = [[NSURL URLWithString:[currentAppUrlStr stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]] retain];
    [_businessAppWebView loadRequest:[NSURLRequest requestWithURL:_url]];
    [self.view addSubview:_businessAppWebView];
    [ZSTLogUtil logUserAction:NSStringFromClass([ZSTBusinessAppWebViewController class])];
    
    UIButton *homeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [homeButton setImage:[UIImage imageNamed:@"btn_webicon_home.png"] forState:UIControlStateNormal];
    [homeButton setImage:[UIImage imageNamed:@"btn_webicon_home_light.png"] forState:UIControlStateHighlighted];
    [homeButton addTarget:self action:@selector(home) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [refreshButton setImage:[UIImage imageNamed:@"btn_webicon_reload.png"]  forState:UIControlStateNormal];
    [refreshButton setImage:[UIImage imageNamed:@"btn_webicon_reload_light.png"] forState:UIControlStateHighlighted];
    [refreshButton addTarget:self action:@selector(refresh) forControlEvents:UIControlEventTouchUpInside];
    
    _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_backButton setImage:[UIImage imageNamed:@"btn_webicon_backward.png"]  forState:UIControlStateNormal];
    [_backButton setImage:[UIImage imageNamed:@"btn_webicon_backward_light.png"] forState:UIControlStateHighlighted];
    [_backButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    
    _forwardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [_forwardButton setImage:[UIImage imageNamed:@"btn_webicon_forward.png"] forState:UIControlStateNormal];
    [_forwardButton setImage:[UIImage imageNamed:@"btn_webicon_forward_light.png"] forState:UIControlStateHighlighted];
    [_forwardButton addTarget:self action:@selector(goForward) forControlEvents:UIControlEventTouchUpInside];
    
    _scrollToolBarView = [[ZSTScrollToolBarView alloc] initWithFrame:CGRectMake(1+290, 480-105+(iPhone5?88:0), 325, 40) color:[UIColor blackColor] alpha:0.6];
    _scrollToolBarView.items = [NSArray arrayWithObjects:homeButton,refreshButton,_backButton,_forwardButton, nil];
    _scrollToolBarView.delegate = self;
    [self.view addSubview:_scrollToolBarView];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [TKUIUtil hiddenHUD];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked && [request.URL.scheme caseInsensitiveCompare:@"native"] == NSOrderedSame) {
        
        NSDictionary *params = [request.URL.absoluteString queryDictionaryUsingEncoding:NSUTF8StringEncoding];
        NSString *module_id = [params safeObjectForKey:@"native://?module_id"];
        NSString *module_type = [params safeObjectForKey:@"module_type"];
        NSString *title = [params safeObjectForKey:@"title"];
        
        NSMutableDictionary *moduleParams = [NSMutableDictionary dictionary];
        [moduleParams setSafeObject:module_type forKey:@"type"];
        
        ZSTModule *module = [[ZSTModuleManager shared] findModule:[module_id integerValue]];
        if (module) {
            UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:[module_id integerValue] withOptions:moduleParams];
            if (controller) {
                controller.hidesBottomBarWhenPushed = YES;
                controller.navigationItem.titleView = [ZSTUtils titleViewWithTitle:title];
                controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
                [self.navigationController pushViewController:controller animated:YES];
            }
        }

    }

    [TKUIUtil showHUD:self.view];
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    _backButton.enabled = [webView canGoBack];
    _forwardButton.enabled = [webView canGoForward];
    
    webCanGoBack = [webView canGoBack];
    webCanGoForward = [webView canGoForward];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    _backButton.enabled = [webView canGoBack];
    _forwardButton.enabled = [webView canGoForward];
    
    webCanGoBack = [webView canGoBack];
    webCanGoForward = [webView canGoForward];
    
    [ZSTLogUtil logSysConnect:[NSURL URLWithString:self.appInfo.Url] result:YES response:nil];
    [TKUIUtil hiddenHUD];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{   
    [ZSTLogUtil logSysConnect:[NSURL URLWithString:self.appInfo.Url] result:NO response:[NSString stringWithFormat:@"%@ %@", @(error.code), error.localizedDescription]];
    
    if (([error.domain isEqualToString:@"NSURLErrorDomain"] && error.code == -999) ||
        ([error.domain isEqualToString:@"WebKitErrorDomain"] && error.code == 102)) {
        [TKUIUtil hiddenHUD];
    }else{
        [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"网络错误", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]];
        [TKUIUtil hiddenHUDAfterDelay:2];
    }
}

#pragma mark ----------ScrollToolBarViewDelegate-----------


-(void)clickedPushOut
{
    [UIView beginAnimations:@"ScrollToolBarViewPushOut" context:nil];
    [UIView setAnimationDuration:0.6];
    
    CGRect scrollToolBarViewFrame = _scrollToolBarView.frame;
    scrollToolBarViewFrame.origin.x -=  290;
    _scrollToolBarView.frame = scrollToolBarViewFrame;
    
    [UIView commitAnimations];
    
    _backButton.enabled = webCanGoBack;
    _forwardButton.enabled = webCanGoForward;
}

-(void)clickedPushIn
{
    [UIView beginAnimations:@"ScrollToolBarViewPushIn" context:nil];
    [UIView setAnimationDuration:0.6];
    
    CGRect scrollToolBarViewFrame = _scrollToolBarView.frame;
    scrollToolBarViewFrame.origin.x +=  290;
    _scrollToolBarView.frame = scrollToolBarViewFrame;
    
    [UIView commitAnimations];
}
@end
