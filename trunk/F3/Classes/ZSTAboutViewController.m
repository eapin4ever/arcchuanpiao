    //
//  AboutView.m
//  Net_Information
//
//  Created by huqinghe on 11-6-9.
//  Copyright 2011 Shinetechchina.com. All rights reserved.
//

#import "ZSTAboutViewController.h"
#import "TKUIUtil.h"
#import "ZSTWebViewController.h"
#import "ZSTLogUtil.h"
#import "ZSTUtils.h"
#import "ZSTWebViewController.h"
#import "ZSTAgreementViewController.h"

#define BACKGROUND_PURPLE_COLOR	[UIColor colorWithRed:0.82f green:0.957f blue:0.996f alpha:1.0f]
#define SYSTEM_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define SCREEN_BOUNDS [[UIScreen mainScreen] bounds]
#define WIDTH SCREEN_BOUNDS.size.width
#define HEIGHT SCREEN_BOUNDS.size.height

@implementation ZSTAboutViewController

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.titleView = [ZSTUtils logoView];
	UIImageView *imageview = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon.png"]];
	imageview.frame = CGRectMake((320-57)/2+2, 55, 57, 57);
	[self.view addSubview:imageview];
	[imageview release];
    
    UILabel *displayNamelabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 120, 320, 30)];
    displayNamelabel.backgroundColor = [UIColor clearColor];
    displayNamelabel.textAlignment = NSTextAlignmentCenter;
    displayNamelabel.font = [UIFont boldSystemFontOfSize:20];
    NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    displayNamelabel.text = appDisplayName;
    [self.view addSubview:displayNamelabel];
	[displayNamelabel release];
    
    UILabel *versionlabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 150, 320, 30)];
    versionlabel.font = [UIFont systemFontOfSize:14];
    versionlabel.backgroundColor = [UIColor clearColor];
    versionlabel.textColor = [ZSTUtils colorFromHexColor:@"#616161"];
    versionlabel.textAlignment = NSTextAlignmentCenter;
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    versionlabel.text = [NSString stringWithFormat:NSLocalizedString(@"版本:%@", nil), version];
    [self.view addSubview:versionlabel];
	[versionlabel release];
    
    UIButton *agreementbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    agreementbtn.frame = CGRectMake(0, 242+(IS_IOS_7?20:0)+(iPhone5?68:0), 320, 30);
    [agreementbtn setTitle:@"服务条款" forState:UIControlStateNormal];
    agreementbtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [agreementbtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [agreementbtn addTarget:self action:@selector(agreementAction:) forControlEvents:UIControlEventTouchUpInside];
    agreementbtn.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:agreementbtn];

    
    NSString *settingAbout = [[[NSBundle mainBundle] infoDictionary] safeObjectForKey:@"setting_about"];
    NSArray *aboutArr = [settingAbout componentsSeparatedByString:@","];
    NSString *webTitle = [aboutArr objectAtIndex:0];
    NSString *supportStr = [aboutArr objectAtIndex:1];

    UIButton *webBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    webBtn.frame = CGRectMake(0, HEIGHT - 64 - 130,WIDTH,30);
    [webBtn setTitle: [webTitle length] ? webTitle : @"portal.pmit.cn" forState:UIControlStateNormal];
    [webBtn setTitleColor:[ZSTUtils colorFromHexColor:@"#616161"] forState:UIControlStateNormal];
    webBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [webBtn addTarget:self action:@selector(webAction:) forControlEvents:UIControlEventTouchUpInside];
    webBtn.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [self.view addSubview:webBtn];
    
    UILabel *supportLB = [[UILabel alloc] initWithFrame:CGRectMake(0, HEIGHT - 64 - 100, WIDTH, 30)];
    supportLB.textAlignment = NSTextAlignmentCenter;
    supportLB.font = [UIFont systemFontOfSize:14.0f];
    supportLB.text = [NSString stringWithFormat:@"版权所有：%@",[supportStr length] ? supportStr : @""];
    [self.view addSubview:supportLB];
    supportLB.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [supportLB release];
    
    UILabel *techSupportLB = [[UILabel alloc] initWithFrame:CGRectMake(0, HEIGHT - 64 - 70, WIDTH, 30)];
    techSupportLB.textAlignment = NSTextAlignmentCenter;
    techSupportLB.font = [UIFont systemFontOfSize:14.0f];
    techSupportLB.text = @"技术支持：广州市加减信息技术有限公司";
    [self.view addSubview:techSupportLB];
    techSupportLB.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [techSupportLB release];
    
    UILabel *copyRightLable = [[UILabel alloc] initWithFrame:CGRectMake(0, HEIGHT - 40 - 64,WIDTH,30)];
    copyRightLable.font = [UIFont systemFontOfSize:14.0f];
    copyRightLable.backgroundColor = [UIColor clearColor];
    copyRightLable.textAlignment = NSTextAlignmentCenter;
    copyRightLable.text = @"Copyright ©2009-2015 All Rights Reserved";
    [self.view addSubview:copyRightLable];
    copyRightLable.autoresizingMask = UIViewAutoresizingFlexibleBottomMargin;
    [copyRightLable release];
    
    [ZSTLogUtil logUserAction:NSStringFromClass([ZSTAboutViewController class])];
}

- (void)agreementAction:(UIButton *)sender
{
    NSLog(@"服务条款");
    
    ZSTAgreementViewController *controller = [[ZSTAgreementViewController alloc] init];
    controller.source = NO;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navController animated:YES completion:nil];

    [navController release];
    [controller release];
}

- (void)webAction:(UIButton *)sender
{
    NSString *settingAbout = [[[NSBundle mainBundle] infoDictionary] safeObjectForKey:@"setting_about"];
    NSArray *aboutArr = [settingAbout componentsSeparatedByString:@","];
    NSString *webTitle = [aboutArr objectAtIndex:0];
    if ([webTitle length] == 0) {
        webTitle = @"";
    }
    ZSTWebViewController *webView = [[ZSTWebViewController alloc] init];
    [webView setURL:webTitle];
    webView.type = 99999999;
    webView.hidesBottomBarWhenPushed = YES;
    webView.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    [self.navigationController pushViewController:webView animated:YES];
    [webView release];
}

- (void)dealloc
{
    [super dealloc];
}


@end
