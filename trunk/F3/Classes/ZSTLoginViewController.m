//
//  ZSTLoginViewController.m
//  F3
//
//  Created by LiZhenQu on 14-10-22.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTLoginViewController.h"
#import "ZSTF3RegisterViewController.h"

@interface ZSTLoginViewController ()

@end

@implementation ZSTLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.bgImeView.image = ZSTModuleImage(@"module_login_bg.jpg");
    self.bgview.alpha = 0.5;
    
    UIImage *imageN = ZSTModuleImage(@"module_login_btn_green_n.png");
    UIImage *ImageP = ZSTModuleImage(@"module_login_btn_green_p.png");
    CGFloat capWidth = imageN.size.width / 2;
    CGFloat capHeight = imageN.size.height / 2;
    imageN = [imageN stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    ImageP = [ImageP stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    [self.loginbtn setBackgroundImage:imageN forState:UIControlStateNormal];
    [self.loginbtn setBackgroundImage:ImageP forState:UIControlStateHighlighted];
    
    self.engine = [[[ZSTF3Engine alloc] init] autorelease];
    self.engine.delegate = self;
    
     if ([ZSTF3Preferences shared].MCRegistType != MCRegistType_ForceRegister) {
         
          self.cancelbtn.hidden = YES;
          self.backbtn.hidden = YES;
         if (_isFromSetting) {
             self.backbtn.hidden = NO;
         } else {
             self.cancelbtn.hidden = NO;
         }
     }
    
//    _avaterImgView.layer.borderWidth = 1;
    _avaterImgView.layer.cornerRadius = _avaterImgView.frame.size.width / 2.0;
    _avaterImgView.layer.borderColor = [UIColor clearColor].CGColor;
    _avaterImgView.layer.masksToBounds = YES;
    
//    [_avaterImgView setContentScaleFactor:[[UIScreen mainScreen] scale]];
//   _avaterImgView.contentMode =  UIViewContentModeScaleAspectFill;
//    _avaterImgView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
//    _avaterImgView.clipsToBounds  = YES;
    
    [_delegate retain];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    NSString *path = nil;
    NSDictionary *dic = [ZSTF3Preferences shared].avaterName;
    
    if (![ZSTF3Preferences shared].loginMsisdn || [ZSTF3Preferences shared].loginMsisdn.length == 0) {
        
        [ZSTF3Preferences shared].loginMsisdn = @"";
    }
    
    if (dic || [dic isKindOfClass:[NSDictionary class]]) {
        path = [dic safeObjectForKey:[ZSTF3Preferences shared].loginMsisdn];
    }
    
    if (path && path.length > 0) {
        
        NSData *reader = [NSData dataWithContentsOfFile:path];
        UIImage *image = [UIImage imageWithData:reader];
        _avaterImgView.image = image;
    }
    
    if (!_avaterImgView.image || path == nil) {
        
        _avaterImgView.image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
    }
    
    [self showAnimations];
    
    if ([ZSTF3Preferences shared].CarrierType != 10) {
        
        self.registerbtn.hidden = NO;
    } else {
        
        [self.nameField becomeFirstResponder];
        self.registerbtn.hidden = YES;
        
        CGRect frame = self.forgotbtn.frame;
        frame.origin.x = (self.view.frame.size.width - frame.size.width) / 2;
        self.forgotbtn.frame = frame;
    }
    
     [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)showAnimations
{
    [UIView beginAnimations:@"HideArrow" context:nil];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
    [UIView setAnimationDuration:0.8];
    [UIView setAnimationDelay:0.8];
    self.bgview.alpha = 1.0;
    [UIView commitAnimations];
}

- (BOOL)validateMobile:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,183,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[2378]|7[0-9]|4[0-9])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (IBAction)closeKeyboard:(id)sender
{
    [self.nameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
    self.passwordbtn.hidden = YES;
    self.namebtn.hidden = YES;                         
}

- (IBAction)deleteAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    if (button.tag == 0) {
    
        self.passwordField.text = @"";
        self.passwordField.secureTextEntry = NO;
    } else if (button.tag == 1) {
        
        self.nameField.text = @"";
    }
}

- (IBAction)loginAction:(id)sender
{
    [self.nameField resignFirstResponder];
    [self.passwordField resignFirstResponder];
    
    NSString *phoneNumberStr = [self.nameField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *passWordStr = [self.passwordField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (phoneNumberStr.length == 0) {
        [TKUIUtil alertInWindow:@"手机号" withImage:nil];
        return;
    } else if(![self validateMobile:phoneNumberStr]) {
        
        UIAlertView* alertView = [[UIAlertView alloc]
                                  initWithTitle:nil
                                  message:@"您输入的手机号格式有误，请重试"
                                  delegate:self
                                  cancelButtonTitle:@"确定"
                                  otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    if (passWordStr.length == 0) {
        
        [TKUIUtil alertInWindow:@"密码" withImage:nil];
        return;
    }
   
    [TKUIUtil showHUD:self.view];
    [self.engine loginWithMsisdn:phoneNumberStr password:passWordStr];
}

- (IBAction)registerAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    int type = 0;
    
    if (button.tag == 0) {
        type = 2;
        
    } else if (button.tag == 1) {
        type = 1;
    }

    ZSTF3RegisterViewController *controller = [[ZSTF3RegisterViewController alloc] initWithNibName:@"ZSTF3RegisterViewController" bundle:nil];
    controller.type = type;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

-(IBAction)dismissView
{
    if (_delegate && [_delegate respondsToSelector:@selector(loginDidCancel)]) {
        
        [self.navigationController dismissViewControllerAnimated:YES completion:^(void) {
        }];
        
         [_delegate loginDidCancel];
        
        return;
    }
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)loginFinish
{
    if (_delegate && [_delegate respondsToSelector:@selector(loginDidFinish)]) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
        [_delegate loginDidFinish];
        return;
    }
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark ------------------UITextFieldDelegate---------------------

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.nameField.tag == 0 && textField == self.nameField && textField.text.length - range.length + string.length > 11) {
        return NO;
    }
    
    //    if (self.passwordField.tag == 1 && textField == self.passwordField && textField.text.length - range.length + string.length > 16) {
    //        return NO;
    //    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.nameField) {
        
        if ([self.nameField.text isEqualToString:@"手机号"]) {
            self.nameField.text = @"";
        }
        self.namebtn.hidden = NO;
        self.passwordbtn.hidden = YES;
    }
    
    if (textField == self.passwordField) {
        
        self.passwordbtn.hidden = NO;
        self.passwordField.text = @"";
        self.passwordField.secureTextEntry = YES;
        self.namebtn.hidden = YES;
    }
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (textField == self.nameField && self.nameField.text.length == 0) {
        
        self.nameField.text = @"手机号";
    }
    
    if (textField == self.nameField && self.nameField.text.length > 0) {
        
        NSString *path = nil;
        NSDictionary *dic = [ZSTF3Preferences shared].avaterName;
        
        if (dic && [dic isKindOfClass:[NSDictionary class]]) {
            path = [dic safeObjectForKey:self.nameField.text];
        }
        
        if (path && path.length > 0) {
            
            NSData *reader = [NSData dataWithContentsOfFile:path];
            UIImage *image = [UIImage imageWithData:reader];
            _avaterImgView.image = image;
        }
        
        if (!_avaterImgView.image || path == nil) {
            
            _avaterImgView.image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
        }

    }
    
    if (textField == self.passwordField && self.passwordField.text.length == 0) {
        
       self.passwordField.text = @"密码";
       self.passwordField.secureTextEntry = NO;
    }
}

#pragma mark ------------------ZSTF3EngineDelegate---------------------

- (void)loginDidSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    
    [ZSTF3Preferences shared].loginMsisdn = self.nameField.text;
    [ZSTF3Preferences shared].UserId = [[response safeObjectForKey:@"data"] safeObjectForKey:@"UserId"];
    
    if ([ZSTF3Engine syncPushNotificationParams])
    {
        NSLog(@"推送绑定成功");
    }
    else
    {
        NSLog(@"推送绑定失败");
    }
    
    [self loginFinish];
}

- (void)loginDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:response withImage:nil];
}

- (void) viewWillUnload
{
    [super viewWillUnload];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    self.nameField = nil;
    self.passwordField = nil;
    self.loginbtn = nil;
    self.passwordbtn = nil;
    self.namebtn = nil;
    [_avaterImgView release];
     [super dealloc];
}

@end
