//
//  ZSTPersonalWebViewController.m
//  F3
//
//  Created by LiZhenQu on 14/11/12.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTPersonalWebViewController.h"

@interface ZSTPersonalWebViewController ()

@end

@implementation ZSTPersonalWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    if (_url) {
        [self setURL:_url];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_webView stopLoading];
    [_webView setDelegate:nil];
    [TKUIUtil hiddenHUD];
}


- (void)setURL:(NSString *)url
{
    url = [self normalizeURL:url];
    [url retain];
    [_url release];
    _url = url;
    
    if (_webView) {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
        [_webView loadRequest:request];
    }
}

- (NSString *)normalizeURL:(NSString *)url
{
    if ([url length] != 0 && [url rangeOfString:@"://"].location == NSNotFound) {
        url = [NSString stringWithFormat:@"http://%@", url];
    }
    return url;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [TKUIUtil showHUD:self.view];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [TKUIUtil hiddenHUD];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [_webView setDelegate:nil];
}

- (void) dealloc
{
    self.webView = nil;
   
    [super dealloc];
}

@end
