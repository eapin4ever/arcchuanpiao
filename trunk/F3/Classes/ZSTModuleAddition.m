//
//  UIViewController+Module.m
//  F3Engine
//
//  Created by luobin on 12-9-14.
//
//

#import "ZSTModuleAddition.h"
#import <objc/runtime.h>
#import "ZSTModuleDelegate.h"
#import "SinaWeiboConstants.h"
#import "ZSTF3ClientAppDelegate.h"
#import "ZSTUtils.h"

static char moduleTypeKey;
static char engineKey;
static char daoKey;

static char SinaWeiboKey;
static char QQKey;
static char TencentWeiboKey;
//static char WXEngineKey;

//TK_FIX_CATEGORY_BUG(Module)
@implementation UIViewController (Module)

+(void)initialize
{
    static BOOL initialize = NO;
    if (!initialize) {
        TKReplaceMethods(self, @selector(orignalPresentModalViewController:animated:), @selector(presentModalViewController:animated:));
        TKReplaceMethods(self, @selector(presentModalViewController:animated:), @selector(doPresentModalViewController:animated:));
        initialize = YES;
        
        if ([[UIViewController class] instancesRespondToSelector:@selector(presentViewController:animated:completion:)]) {
            TKReplaceMethods(self, @selector(orignalPresentViewController:animated:completion:), @selector(presentViewController:animated:completion:));
            TKReplaceMethods(self, @selector(presentViewController:animated:completion:), @selector(doPresentViewController:animated:completion:));
        }
    }
}

- (void)setModuleType:(NSInteger)moduleType {
    objc_setAssociatedObject(self, &moduleTypeKey, [NSNumber numberWithInteger:moduleType], OBJC_ASSOCIATION_RETAIN);
}

- (NSInteger)moduleType {
    NSNumber *moduleType = objc_getAssociatedObject(self, &moduleTypeKey);
    if (moduleType) {
        return [moduleType integerValue];
    }
    if ([self conformsToProtocol:NSProtocolFromString(@"ZSTModuleDelegate")]) {
        return [(id<ZSTModuleDelegate>)self application].moduleType;
    }
    if (self.ttPreviousViewController) {
        return self.ttPreviousViewController.moduleType;
    }
    if (self.navigationController) {
        return self.navigationController.moduleType;
    }
    if (self.parentViewController) {
        return self.parentViewController.moduleType;
    }
    return self.view.superview.viewController.moduleType;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)orignalPresentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion
{
    
}

- (void)doPresentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion
{
    viewControllerToPresent.moduleType = self.moduleType;
    viewControllerToPresent.dao = self.dao;
    [self orignalPresentViewController:viewControllerToPresent animated:flag completion:completion];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)orignalPresentModalViewController:(UIViewController *)modalViewController animated:(BOOL)animated
{
    
}

- (void)doPresentModalViewController:(UIViewController *)modalViewController animated:(BOOL)animated
{
    modalViewController.moduleType = self.moduleType;
    modalViewController.dao = self.dao;
    [self orignalPresentModalViewController:modalViewController animated:animated];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (ZSTF3Engine *)engine
{
    ZSTF3Engine *engine = objc_getAssociatedObject(self, &engineKey);
    if (engine == nil) {
        engine = [[[ZSTF3Engine alloc] init] autorelease];
        engine.delegate = self;
        engine.moduleType = self.moduleType;
        engine.dao = self.dao;
        objc_setAssociatedObject(self, &engineKey, engine, OBJC_ASSOCIATION_RETAIN);
    }
    return engine;
}


- (ZSTDao *)dao
{
    ZSTDao *dao = objc_getAssociatedObject(self, &daoKey);
    if (dao) {
        return dao;
    }
    if ([self conformsToProtocol:NSProtocolFromString(@"ZSTModuleDelegate")]) {
        return [(id<ZSTModuleDelegate>)self application].dao;
    }
    if (self.ttPreviousViewController) {
        return self.ttPreviousViewController.dao;
    }
    
    if (self.navigationController) {
        return self.navigationController.dao;
    }
    if (self.parentViewController) {
        return self.parentViewController.dao;
    }
    return self.view.superview.viewController.dao;
}

- (void)setDao:(ZSTDao *)dao
{
    objc_setAssociatedObject(self, &daoKey, dao, OBJC_ASSOCIATION_RETAIN);
}


- (SinaWeibo *)sinaWeiboEngine
{
    SinaWeibo *engine = objc_getAssociatedObject(self, &SinaWeiboKey);
    if (engine == nil) {
        ZSTF3Preferences *pre = [ZSTF3Preferences shared];
        NSString *callbackScheme = [NSString stringWithFormat:@"f3%@://",pre.ECECCID];
        NSArray *infoArr = [pre.SinaWeiBo componentsSeparatedByString:@"|"];
        engine = [[SinaWeibo alloc] initWithAppKey:[infoArr objectAtIndex:0] appSecret:[infoArr objectAtIndex:1] appRedirectURI:[infoArr objectAtIndex:2] ssoCallbackScheme:callbackScheme andDelegate:self];
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSDictionary *sinaweiboInfo = [defaults objectForKey:@"SinaWeiboAuthData"];
        if ([sinaweiboInfo objectForKey:@"AccessTokenKey"] && [sinaweiboInfo objectForKey:@"ExpirationDateKey"] && [sinaweiboInfo objectForKey:@"UserIDKey"])
        {
            engine.accessToken = [sinaweiboInfo objectForKey:@"AccessTokenKey"];
            engine.expirationDate = [sinaweiboInfo objectForKey:@"ExpirationDateKey"];
            engine.userID = [sinaweiboInfo objectForKey:@"UserIDKey"];
        }
        SharedAppDelegate.sinaWeiboEngine = engine;
        
        objc_setAssociatedObject(self, &SinaWeiboKey, engine, OBJC_ASSOCIATION_RETAIN);
    }
    return engine;
}

- (void)setSinaWeiboEngine:(SinaWeibo *)sinaWeiboEngine
{
    objc_setAssociatedObject(self, &SinaWeiboKey, sinaWeiboEngine, OBJC_ASSOCIATION_RETAIN);
}


- (TencentOAuth *)QQEngine
{
    TencentOAuth *tencentOAuth = objc_getAssociatedObject(self, &QQKey);
    if (tencentOAuth == nil) {
        
        ZSTF3Preferences *pre = [ZSTF3Preferences shared];
        NSArray *infoArr = [pre.QQ componentsSeparatedByString:@"|"];        
        NSString *appid = [infoArr objectAtIndex:0];
        tencentOAuth = [[TencentOAuth alloc] initWithAppId:appid andDelegate:self];
        SharedAppDelegate.QQEngine = tencentOAuth;
        objc_setAssociatedObject(self, &QQKey, tencentOAuth, OBJC_ASSOCIATION_RETAIN);
    }
    return tencentOAuth;
}

- (void)setQQEngine:(TencentOAuth *)QQEngine
{
    objc_setAssociatedObject(self, &QQKey, QQEngine, OBJC_ASSOCIATION_RETAIN);
}


- (TCWBEngine *)tencentWeiboEngine
{
    TCWBEngine *client = objc_getAssociatedObject(self, &TencentWeiboKey);
    if (client == nil) {
        ZSTF3Preferences *pre = [ZSTF3Preferences shared];
        NSArray *infoArr = [pre.TWeiBo componentsSeparatedByString:@"|"];
        
        client = [[TCWBEngine alloc] initWithAppKey:[infoArr objectAtIndex:0] andSecret:[infoArr objectAtIndex:1] andRedirectUrl:[infoArr objectAtIndex:2]];
        SharedAppDelegate.tWeiboEngine = client;
        objc_setAssociatedObject(self, &TencentWeiboKey, client, OBJC_ASSOCIATION_RETAIN);
    }
    return client;
}

- (void)setTencentWeiboEngine:(TCWBEngine *)tencentWeiboEngine
{
    objc_setAssociatedObject(self, &TencentWeiboKey, tencentWeiboEngine, OBJC_ASSOCIATION_RETAIN);
}


#pragma mark - tencent新增必须实现的协议方法
- (void)tencentDidNotLogin:(BOOL)cancelled
{
    NSLog(@"tencentDidNotLogin");
}

- (void)tencentDidNotNetWork
{
    NSLog(@"tencentDidNotNetWork");
}

- (void)tencentDidLogin
{
    NSLog(@"tencentDidLogin");
}

@end

@implementation UIView (Module)


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (ZSTF3Engine *)engine
{
    ZSTF3Engine *engine = objc_getAssociatedObject(self, &engineKey);
    if (engine == nil) {
        engine = [[[ZSTF3Engine alloc] init] autorelease];
        engine.delegate = self;
        engine.dao = self.dao;
        engine.moduleType = self.moduleType;
        objc_setAssociatedObject(self, &engineKey, engine, OBJC_ASSOCIATION_RETAIN);
    }
    return engine;
}

- (void)setModuleType:(NSInteger)moduleType {
    objc_setAssociatedObject(self, &moduleTypeKey, [NSNumber numberWithInteger:moduleType], OBJC_ASSOCIATION_RETAIN);
}

- (NSInteger)moduleType {
    NSNumber *moduleType = objc_getAssociatedObject(self, &moduleTypeKey);
    if (moduleType) {
        return [moduleType integerValue];
    }
    if ([self conformsToProtocol:NSProtocolFromString(@"ZSTModuleDelegate")]) {
        return [(id<ZSTModuleDelegate>)self application].moduleType;
    }

    return self.superview.moduleType;
}

- (ZSTDao *)dao
{
    ZSTDao *dao = objc_getAssociatedObject(self, &daoKey);
    if (dao) {
        return dao;
    }
    if ([self conformsToProtocol:NSProtocolFromString(@"ZSTModuleDelegate")]) {
        return [(id<ZSTModuleDelegate>)self application].dao;
    }

    return self.superview.dao;
}


- (void)setDao:(ZSTDao *)dao
{
    objc_setAssociatedObject(self, &daoKey, dao, OBJC_ASSOCIATION_RETAIN);
}

@end

