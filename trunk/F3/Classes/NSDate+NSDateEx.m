//
//  NSDate+NSDateEx.m
//  Circle
//  日期工具，格式化时间
//  Created by Yunfei Bai on 11-12-26.
//  Copyright (c) 2011年 __MyCompanyName__. All rights reserved.
//

#import "NSDate+NSDateEx.h"

@implementation NSDate (NSDateEx)
#pragma mark ------------------　时　间　-----------------
+(NSInteger) minutesSinceMidnight: (NSDate *)date
{
    NSCalendar *gregorian = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    unsigned unitFlags =  NSHourCalendarUnit | NSMinuteCalendarUnit;
    NSDateComponents *components = [gregorian components:unitFlags fromDate:date];
    
    return 60 * [components hour] + [components minute];    
}


+(NSInteger) secondsSinceMidnight: (NSDate *)date
{
    NSCalendar *gregorian = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    unsigned unitFlags =  NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    NSDateComponents *components = [gregorian components:unitFlags fromDate:date];
    NSInteger hour = [components hour];
    NSInteger minute = [components minute];
    NSInteger second = [components second];
    return 60 * 60 * hour + 60 * minute + second;    
}

+(NSString *) dateStringWithDate:(NSDate*) date
{
    NSTimeInterval interval = [date timeIntervalSince1970];
    return [NSDate dateStringWithTimeInterval:interval];
}

+(NSString *) dateStringWithTimeInterval:(NSTimeInterval) timeInterval
{
    //最后一次联网服务器时间戳
//    double lastTimeStamp = [[GloadData sharedInstance]._localData._serverAndClientTimestamp doubleValue];
    //最后一次联网客户端时间戳
    //double lastNetRequestClientDate = [[GloadData sharedInstance]._localData._clientTimestamp doubleValue];
    double lastTimeStamp = [[NSDate date] timeIntervalSince1970];
    
    NSString *resultString = nil;
    
    NSTimeInterval currentTimeInterval = lastTimeStamp;

    //获得今天凌晨到现在已经过去的时间
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* component = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
//    没有使用
//    NSDate* date = [calendar dateFromComponents:component];
    NSDateFormatter* formatter = [[[NSDateFormatter alloc] init] autorelease];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
//    没有使用到
//    NSTimeInterval midnightTimeInterval = [date timeIntervalSince1970];
    
    NSDate *timeDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateComponents * components = [calendar components:NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit |
									 NSMinuteCalendarUnit | NSSecondCalendarUnit
                                                fromDate: timeDate];
    
//    NSTimeInterval offsetSinceMidnight = timeInterval - midnightTimeInterval;
//    NSTimeInterval timeOffset = currentTimeInterval - timeInterval;
    
    if ([components year] < [component year]) {
        
        NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
        [formatter setDateFormat: @"yyyy-MM-dd HH:mm"];
        NSDate *targetDate = [NSDate dateWithTimeIntervalSince1970: timeInterval];
        resultString = [formatter stringFromDate: targetDate];
        
    } else {
        
        if ([components month] < [component month]) {
            
            NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
            [formatter setDateFormat: @"MM-dd HH:mm"];
            NSDate *targetDate = [NSDate dateWithTimeIntervalSince1970: timeInterval];
            resultString = [formatter stringFromDate: targetDate];
        } else {
            
            if ([components day] < [component day]) {
                
                resultString = [NSString stringWithFormat:@"%@天前",@([component day] - [components day])];
            } else {
                
                if (currentTimeInterval - timeInterval <= 60*60*24) {
                    NSTimeInterval timeOffset = currentTimeInterval - timeInterval;
                    if (timeOffset < 60)
                    {
                        resultString = [NSString stringWithFormat:@"刚刚"];
                    }
                    else if(timeOffset < 60 * 60)
                    {
                        resultString = [NSString stringWithFormat: @"%.0lf分钟前", timeOffset / 60];
                    }
                    else
                    {
                        resultString = [NSString stringWithFormat: @"%.0lf小时前", timeOffset / 60 / 60];
                    }
                }
            }
        }
    }
    
    return resultString;
}


+ (NSString *)stringFromDate:(NSDate *)date{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init]; 
    
    //zzz表示时区，zzz可以删除，这样返回的日期字符将不包含时区信息 +0000。
    
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss zzz"];
    
    NSString *destDateString = [dateFormatter stringFromDate:date];
    
    [dateFormatter release];
    
    return destDateString;
    
}


+ (NSDate *)dateFromString:(NSString *)dateString{
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    
    [dateFormatter setDateFormat: @"yyyy-MM-dd"]; 
    
    
    NSDate *destDate= [dateFormatter dateFromString:dateString];
    
    [dateFormatter release];
    
    return destDate;
}
@end
