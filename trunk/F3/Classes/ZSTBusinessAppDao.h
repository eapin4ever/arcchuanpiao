//
//  BusinessAppDao.h
//  F3
//
//  Created by xuhuijun on 12-3-8.
//  Copyright 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZSTBusinessAppInfo.h"

@interface ZSTBusinessAppDao : NSObject {
    
    
}
+ (ZSTBusinessAppDao *)shareBusinessAppDao;

//插入或替换一个应用
- (BOOL)insertOrReplaceBusinessAppInfo:(ZSTBusinessAppInfo *)info;

//插入应用
- (BOOL)insertBusinessAppInfo:(ZSTBusinessAppInfo *)info;

//获取所有应用
- (NSArray *) getBusinessAppnfos;

//获取应用的图标
- (ZSTBusinessAppInfo *)getBusinessAppInfoByAppId:(NSString *)appId;

//删除应用
- (void)deleteBusinessAppInfo:(NSString *)appId;

//删除所有应用
- (void)deleteAllBusinessAppInfo;

//修改应用的屏蔽状态
- (BOOL)setBusinessAppInfo:(NSString *)appId shielded:(BOOL)isShielded;

- (NSData *)getIcon:(NSString *)appId;

@end
