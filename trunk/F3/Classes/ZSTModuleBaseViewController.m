//
//  ZSTModulController.m
//  F3
//
//  Created by luobin on 7/10/12.
//  Copyright (c) 2012 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTModuleBaseViewController.h"
#import "ZSTModuleAddition.h"

@implementation ZSTModuleBaseViewController

@synthesize application;
@synthesize iconImageName;
@synthesize tabTitle;
@synthesize titleColor;

- (id)init
{
    self = [super init];
    if (self) {
    }
    return self;
}

- (void)dealloc
{
    self.application = nil;
    self.iconImageName = nil;
    self.tabTitle = nil;
    self.titleColor = nil;
    [super dealloc];
}

- (NSString *)iconImageName
{
    return iconImageName;
}

- (NSString *)tabTitle
{
    return tabTitle;
}

- (NSString *)titleColor
{
    return titleColor;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    if (IS_IOS_7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
    }

    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed: @"framework_top_bg.png"];
    
    UIImageView *generalImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]];
    generalImg.frame = CGRectMake(0, 0, 320, 480+(iPhone5?88:0));
    generalImg.userInteractionEnabled = YES;
    generalImg.contentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:generalImg];
    [generalImg release];
}


-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return (1 << UIInterfaceOrientationPortrait);
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIViewController *)rootViewController;
{
    return self;
}

@end
