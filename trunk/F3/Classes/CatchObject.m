//
//  CatchObject.m
//  catchException
//
//  Created by pmit on 15/7/29.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "CatchObject.h"
#import "ZSTF3Preferences.h"

@implementation CatchObject

void uncaughtExceptionHandler(NSException *exception)
{
    //异常的堆栈信息
    NSArray *stackArray = [exception callStackSymbols];
    //出现异常的原因
    NSString *reason = [exception reason];
    //异常名称
    NSString *name = [exception name];
    NSString *exceptionInfo = [NSString stringWithFormat:@"Exceptionreason：%@nExceptionname：%@nExceptionstack：%@",name,reason,stackArray];
    NSLog(@"exception --> %@",exceptionInfo);
    if ([[ZSTF3Preferences shared].ECECCID isEqualToString:@"602781"])
    {
        NSString *syserror = [NSString stringWithFormat:@"mailto://824243782@qq.com?subject=bug报告&body=感谢您的配合!<br><br><br>"
                              "Error Detail:<br>%@<br>--------------------------<br>%@<br>---------------------<br>%@",
                              name,reason,[stackArray componentsJoinedByString:@"<br>"]];
        NSURL *url = [NSURL URLWithString:[syserror stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        [[UIApplication sharedApplication] openURL:url];
    }

    NSMutableArray *tmpArr=[NSMutableArray arrayWithArray:stackArray];
    [tmpArr insertObject:reason atIndex:0];
}
@end
