//
//  ZSTSettingCell.h
//  F3
//
//  Created by LiZhenQu on 13-12-11.
//  Copyright (c) 2013年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "TKTableViewCell.h"

@interface ZSTSettingCell : TKTableViewCell

- (void) initWithData;

@end
