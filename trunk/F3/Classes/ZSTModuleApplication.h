//
//  ZSTModuleApplication.h
//  F3
//
//  Created by luobin on 12-10-30.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZSTModule.h"
#import "ZSTDao.h"

@protocol ZSTModuleDelegate;
@interface ZSTModuleApplication : NSObject

@property (nonatomic, retain) ZSTModule *module;
@property (nonatomic, assign) id<ZSTModuleDelegate> delegate;
@property (nonatomic, copy) NSDictionary *launchOptions;
@property (nonatomic, assign) NSInteger moduleType;
@property (nonatomic, retain) ZSTDao *dao;

- (id)initWithModule:(ZSTModule *)module;
- (UIViewController *)launchWithOptions:(NSDictionary *)launchOptions;
- (UIView *)launchViewWithOptions:(NSDictionary *)launchOptions;
- (UIViewController *)moduleSettingViewController;

@end
