//
//  ZSTShareTableViewCell.m
//  F3
//
//  Created by LiZhenQu on 14-10-29.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTShareTableViewCell.h"

@implementation ZSTShareTableViewCell

- (void)awakeFromNib {
    
    self.rightLabel.clipsToBounds = YES;
    self.rightLabel.layer.cornerRadius = 10;//设置那个圆角的有多圆
    self.rightLabel.layer.masksToBounds = YES;//设为NO去试试
    
    UIImageView *rightImgView = [[UIImageView alloc] initWithFrame:CGRectMake(285, (self.frame.size.height - 12)/2.0, 7, 12)];
    rightImgView.image = ZSTModuleImage(@"module_setting_icon_right.png");
    [self addSubview:rightImgView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
