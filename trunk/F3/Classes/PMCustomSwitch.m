//
//  PMCustomSwitch.m
//  F3
//
//  Created by pmit on 15/6/14.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "PMCustomSwitch.h"

@interface PMCustomSwitch()

@property (nonatomic,retain) UIView *customSwitchView;

@end

@implementation PMCustomSwitch

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        self.backgroundColor = [UIColor whiteColor];
        
//        _customSwitch = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
//        [self addSubview:_customSwitch];
//        
//        _onButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        _onButton.frame = CGRectMake(20, 0, 73, 44);
//        [_customSwitch addSubview:_onButton];
//        
//        _offButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        _offButton.frame = CGRectMake(20, 0, 73, 44);
//        [_customSwitch addSubview:_offButton];
        _customSwitchView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [self addSubview:_customSwitchView];
        _customSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 73, 44)];
        [_customSwitch addTarget:self action:@selector(switchValueChange:) forControlEvents:UIControlEventValueChanged];
        [_customSwitchView addSubview:_customSwitch];
        
    }
    return self;
}

- (void)switchValueChange:(UISwitch *)sender
{
    if (sender.isOn)
    {
        self.status = PMCustomSwitchStatusOn;
    }
    else
    {
        self.status = PMCustomSwitchStatusOff;
    }
}

- (void)setStatus:(PMCustomSwitchStatus)status
{
    _status = status;
    if (status == PMCustomSwitchStatusOn)
    {
        _customSwitch.on = YES;
    }
    else
    {
        _customSwitch.on = NO;
    }
    
    if ([_delegate respondsToSelector:@selector(zstcustomSwitchSetStatus:)]) {
        [_delegate zstcustomSwitchSetStatus:_status];
    }
}


@end
