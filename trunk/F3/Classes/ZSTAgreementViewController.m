//
//  ZSTAgreementViewController.m
//  F3
//
//  Created by LiZhenQu on 14-5-22.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTAgreementViewController.h"
#import "ZSTUtils.h"

@interface ZSTAgreementViewController ()<UIWebViewDelegate>

@property (nonatomic, retain) UIWebView *webView;
@property (nonatomic, retain) UIImageView *markImgView;


@end

@implementation ZSTAgreementViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed: @"framework_top_bg.png"];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"使用条款和隐私政策", nil)];
    
    _webView = [[[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 480+(iPhone5?88:0)-(self.source?60:0))] autorelease];
    _webView.backgroundColor = [UIColor clearColor];
    [_webView setUserInteractionEnabled:YES];  //是否支持交互
    [_webView setDelegate:self];
    _webView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:_webView];

    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",@"http://bo.pmit.cn/Mod/BackOffice/web/SNSA/Agreement.html"]];//创建URL
    
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
}

- (void) agreeAction
{
     _markImgView.image = ZSTModuleImage(@"module_snsa_orderinfo_pay_on.png");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)webViewDidStartLoad:(UIWebView *)webView //网页加载时调用
{
}

- (void)webViewDidFinishLoad:(UIWebView *)webView //网页完成加载时调用
{
    if (self.source) {
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_webView.frame), 320, 60)];
        bottomView.backgroundColor = RGBCOLOR(241, 241, 241);
        [self.view addSubview:bottomView];
        
        _markImgView = [[UIImageView alloc] initWithFrame:CGRectMake(80, 20, 15, 15)];
        _markImgView.image = ZSTModuleImage(@"module_snsa_orderinfo_pay_off.png");
        [bottomView addSubview:_markImgView];
        
        UILabel *textlal = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_markImgView.frame), 20, 160, 20)];
        textlal.backgroundColor = [UIColor clearColor];
        textlal.textColor = [UIColor blackColor];
        textlal.text = @"我已阅读并同意协议";
        textlal.font = [UIFont systemFontOfSize:16];
        textlal.textAlignment = NSTextAlignmentCenter;
        [bottomView addSubview:textlal];
        [textlal release];
        
        UITapGestureRecognizer *singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(agreeAction)];
        singleRecognizer.numberOfTapsRequired = 1;
        [bottomView addGestureRecognizer:singleRecognizer];
        
        [bottomView release];
    } else {
        
        self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (dismissModalViewController)];
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [_webView setDelegate:nil];
}

- (void) dealloc
{
    [_markImgView release];
    [super dealloc];
}

@end
