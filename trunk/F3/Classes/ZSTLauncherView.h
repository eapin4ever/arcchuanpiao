//
//  LauncherViewController.h
//  F3
//
//  Created by luobin、许慧君 on 11/9/11.
//  Copyright 2011 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZSTLauncherButtonView.h"
#import "ZSTLaucherItem.h"

@class ZSTLauncherView;

@protocol ZSTLauncherViewDataSource <NSObject>

-(NSUInteger)numberOfItemsForLauncherView:(ZSTLauncherView *)launcherView;

-(ZSTLaucherItem *)launcherView:(ZSTLauncherView *)launcherView itemForIndex: (NSUInteger)index;

@end

@protocol ZSTLauncherViewDelegate <NSObject>

@optional

-(void)launcherView:(ZSTLauncherView *)launcherView didClickItemAtIndex:(NSUInteger)index;

-(void)launcherView:(ZSTLauncherView *)launcherView didClickDeleteButtonAtIndex:(NSUInteger)index;

@end

@interface ZSTLauncherView : UIView<ZSTLauncherButtonViewDelegate, UIScrollViewDelegate,HJManagedImageVDelegate> {
    
    NSMutableArray *_btns;
    NSArray *_items;
    UIScrollView *_scrollView;
    UIPageControl *_pageControl;    
    BOOL _isLongPress;
    BOOL _editing;
    
    NSUInteger _columnCount;
    NSUInteger _rowCount;
    
    BOOL _isHorizontal;
    BOOL _largeIconVertical;
    BOOL _midIconVertical;
    
    
    id<ZSTLauncherViewDataSource> _dataSource;
    id<ZSTLauncherViewDelegate> _delegate;
}

@property (nonatomic, assign) id<ZSTLauncherViewDataSource> dataSource;
@property (nonatomic, assign) id<ZSTLauncherViewDelegate> delegate;
@property (nonatomic) NSUInteger columnCount;

@property (nonatomic) NSUInteger rowCount;
@property (nonatomic) BOOL isHorizontal;
@property (nonatomic) BOOL largeIconVertical;
@property (nonatomic) BOOL midIconVertical;



- (void)reloadData;

- (void)selected:(NSUInteger)itemIndex;

- (void)setEnable:(BOOL)enable atIndex:(NSUInteger)index;

- (void)setBadgeNumber:(NSUInteger)badgeNumber atIndex:(NSUInteger)index;

//开始编辑
-(void)beginEditing;

//停止编辑
-(void)endEditing;

@end


