//
//  ZSTLoginViewController.h
//  F3
//
//  Created by LiZhenQu on 14-10-22.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LoginDelegate <NSObject>

@optional

- (void)loginDidFinish;

- (void)loginDidCancel;

- (void)loginDidSucceed;

@end

@interface ZSTLoginViewController : UIViewController<UITextFieldDelegate,ZSTF3EngineDelegate>

@property (nonatomic, retain) IBOutlet UIImageView *bgImeView;
@property (nonatomic, retain) IBOutlet UIImageView *avaterImgView;
@property (nonatomic, retain) IBOutlet UITextField *nameField;
@property (nonatomic, retain) IBOutlet UIButton *namebtn;
@property (nonatomic, retain) IBOutlet UITextField *passwordField;
@property (nonatomic, retain) IBOutlet UIButton *passwordbtn;
@property (nonatomic, retain) IBOutlet UIButton *loginbtn;
@property (nonatomic, retain) IBOutlet UIButton *cancelbtn;
@property (nonatomic, retain) IBOutlet UIButton *backbtn;
@property (nonatomic, retain) IBOutlet UIButton *registerbtn;

@property (nonatomic, retain) IBOutlet UIButton *forgotbtn;
@property (nonatomic, retain) IBOutlet UIView *bgview;

@property (nonatomic, assign)id<LoginDelegate>delegate;

@property(nonatomic, assign) BOOL isFromSetting;

@property(nonatomic, retain) ZSTF3Engine *engine;

@end
