
#define CELL_HEIGHT 61.1666667

//#define WEIBO_PATH  @"http://wap.hbbkt.f3.cn/mzwz_new/ashx/GetListForClient.ashx?type_id=31"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// 列表更新通知
#define NotificationName_NMSListUpdated @"NMSListUpdated"
#define NotificationName_OutBoxNMSListUpdated @"OutBoxNMSListUpdated"
#define NotificationName_SwitchNMSListUpdated @"SwitchNMSListUpdated"

#define NotificationName_NMSDownload_Complete @"NMSDownloadComplete"

#define NotificationName_RegisterNMS_OK @"RegisterNMS_OK"
#define NotificationName_RegisterNMS_Failure @"RegisterNMS_Failure"
#define NotificationName_UpdateBusiness_Complete @"UpdateBusinessComplete"


#define NotificationName_ForceHttpPoll @"ForceHttpPoll"
#define NotificationName_PushBMessage @"PushBMessage"
#define NotificationName_PushViewController @"PushViewController"



#define NotificationName_UnreadNMSChanged @"UnreadNMSChanged"
#define NotificationName_GroupUnreadNMSChanged @"GroupUnreadNMSChanged"
#define NotificationName_InboxUnreadNMSChanged @"InboxUnreadNMSChanged"

#define NotificationName_LoginMsisdnChange @"LoginMsisdnChange"

#define NotificationName_IpadWebButtonEnabled @"ipadWebButtonEnabled"

#define NotificationName_OrientationDidChange @"orientationDidChange"

#define NotificationName_MessageSendSucessed @"MessageSendSucessed"

#define NotificationName_MessageSendFaild @"MessageSendFaild"

#define NotificationName_WXShareSucceed @"WX_share_succeed"
#define NotificationName_WXShareFaild   @"WX_share_faild"

#define NotificationName_F3EngneDidStartSuccessed @"F3EngneDidStartSuccessed"
#define NotificationName_F3EngneDidStartFailed @"F3EngneDidStartFailed"

#define NotificationName_UpdateECMobileClientParamsSuccessed @"updateECMobileClientParamsSuccessed"

#define postNotificationOnMainThreadNoWait(notificationName) \
[[NSNotificationCenter defaultCenter] performSelectorOnMainThread:@selector(postNotificationName:object:) withObject:(notificationName) waitUntilDone:NO]

// 更新时间
#define NMS_CHECK_REGISTER_INTERVAL 15

//用户日志最新上传时间
#define LatestSubmitClientLogDate @"latestSubmitClientLogDate"


#define SYNCCOMPANYAPP_URL_PATH  @"/SyncCompanyApp"

#define F3_BASE_URL [[NSBundle mainBundle] localizedStringForKey:@"Global_ServerDomain" value:@"http://mod.pmit.cn/FrameworkA" table:nil]

//#define SUBMITUserLOG_URL_PATH  [F3_BASE_URL stringByAppendingString:@"/SubmitUserLog"]
#define SUBMITUserLOG_URL_PATH @"http://mod.pmit.cn/pushb/SubmitUserLog"

#define WELCOME_URL F3_BASE_URL @"/help/help.zip"

#define VERIFICATION_URL [F3_BASE_URL stringByAppendingString:@"/GetRegVerificationCode"]

#define REGISTER_URL [F3_BASE_URL stringByAppendingString:@"/RegistMobileClient"]

#define SUBMITADVICE_URL   [F3_BASE_URL stringByAppendingString:@"/SubmitAdviceToECECC"]

#define CHECKVERSION_URL   [F3_BASE_URL stringByAppendingString:@"/CheckClientVersion"]

#define ECMOBILECLIENTPARAMS_URL   [F3_BASE_URL stringByAppendingString:@"/GetECMobileClientParams"]

#define SYNCPUSHNOTIFICATIONPARAM_URL  @"http://mod.pmit.cn/pushb/SyncPushNotification"

//#define SUBMITSYSTEMLOG_URL_PATH  [F3_BASE_URL stringByAppendingString:@"/SubmitSystemLog"]

#define SUBMITSYSTEMLOG_URL_PATH @"http://mod.pmit.cn/pushb/SubmitSystemLog"

#define REQUEST_CODE_OK 1

// ################################# 腐蚀线 ##################################

#define PersonalBaseURL @"http://wxwap.pmit.cn/app"
//#define PersonalBaseURL @"http://192.168.5.222:8885/app"
//#define PersonalBaseURL @"http://192.168.1.15:8088/app"


#define UpdateECClientVisitInfo [F3_BASE_URL stringByAppendingString:@"/UpdateECClientVisitInfo"]

#define GetECClientParams [F3_BASE_URL stringByAppendingString:@"/GetECClientParams"]

#define AutomaticLogin PersonalBaseURL @"/app_terminal_login!autoLogin.action"

#define Login PersonalBaseURL @"/app_terminal_login!login.action"

#define Register PersonalBaseURL @"/app_terminal_login!register.action"

#define GetVerificationCode PersonalBaseURL @"/app_terminal_login!getCaptcha.action"

#define CheckVerificationCode PersonalBaseURL @"/app_terminal_login!checkCaptcha.action"

#define UpdatePassword PersonalBaseURL @"/app_terminal_login!updatePassword.action"

#define UpdateMsisdn  PersonalBaseURL @"/app_terminal_login!updateMsisdn.action"

#define GetUserInfo PersonalBaseURL @"/app_terminal_user!getUserInfo.action"

#define CheckPassword PersonalBaseURL @"/app_terminal_login!hasPwd.action"

#define UpdateUserInfo PersonalBaseURL @"/app_terminal_user!updateUserInfo.action"

#define GetPoint PersonalBaseURL @"/point!getPoint.action"

#define UpdatePoint PersonalBaseURL @"/point!updatePoint.action"

#define UploadFile PersonalBaseURL @"/upload!index.action"

#define Logout PersonalBaseURL @"/app_terminal_login!logout.action"

#define PointDetail @"http://wxwap.pmit.cn/user_center!pointListLayOut.action?ecid=%@&userId=%@&clientType=%@&msisdn=%@"

// ################################  notification  ################################

//消息清理时间通知
#define NotificationName_CleanupMessages @"CleanupMessages"

#define NotificationName_RemaingTimeChange @"RemaingTimeChange"

#define NotificationName_Progress @"Progress"


////////////////////////////////////////////////////  shell 配置参数 ////////////////////////////////////////////////////

//特殊的moduleid
#define shellbHomeID -5
#define shellcHomeID -4 
#define shelleHomeID -6
#define shellfHomeID -7
#define shellgHomeID -8
#define shellpHomeID -9
#define shellsHomeID -10
#define shellcID 20
#define shellbID 14
#define coverID 13
#define shelleID 36
#define shellfID 37
#define shellgID 38
#define shellpID 42
#define shellsID 43

#define ShellRootViewController @"Shell"
#define CoverView @"Cover"


#define FIRST_LAUNCH_APP @"FIRST_LAUNCH_APP"
#define FIRST_LAUNCH_SETTING @"FIRST_LAUNCH_SETTING"
