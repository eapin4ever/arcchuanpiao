//
//  CatchObject.h
//  catchException
//
//  Created by pmit on 15/7/29.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CatchObject : NSObject

void uncaughtExceptionHandler(NSException *exception);

@end
