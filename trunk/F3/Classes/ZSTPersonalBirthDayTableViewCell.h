//
//  ZSTPersonalBirthDayTableViewCell.h
//  InfantCloud
//
//  Created by LiZhenQu on 14-7-31.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZSTPersonalBirthDayTableViewCell;

@protocol ZSTTextFieldCellDelegate <NSObject>
@optional
- (void) textFieldCellDidBeginEditing:(ZSTPersonalBirthDayTableViewCell*)cell;
- (void) textFieldCellTextDidChange:(ZSTPersonalBirthDayTableViewCell*)cell;
- (void) textFieldCellDidEndEditing:(ZSTPersonalBirthDayTableViewCell*)cell;
@end

@interface ZSTPersonalBirthDayTableViewCell : UITableViewCell<UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UITextField *contentField;
@property (nonatomic, strong) IBOutlet UILabel *remindLabel;

@property(nonatomic, assign) id<ZSTTextFieldCellDelegate> delegate;


@end
