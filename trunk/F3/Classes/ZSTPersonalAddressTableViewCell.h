//
//  ZSTPersonalClassTableViewCell.h
//  InfantCloud
//
//  Created by LiZhenQu on 14-8-1.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTPersonalAddressTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *contentLabel;
@property (nonatomic, strong) IBOutlet UILabel *divider;

@property (nonatomic, assign) CGFloat cellHeight;

- (void)configCell:(NSString *)string;

@end
