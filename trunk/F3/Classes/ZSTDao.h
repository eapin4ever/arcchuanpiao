//
//  ZSTModuleBaseDao.h
//  F3Engine
//
//  Created by luobin on 12-9-14.
//
//

#import <Foundation/Foundation.h>

@interface ZSTDao : NSObject

@property (nonatomic, assign) NSInteger moduleType;

+ (ZSTDao *)daoWithModuleType:(NSInteger)moduleType;

- (NSString *)tableName:(NSString *)name;

- (void)createTableIfNotExist DEPRECATED_ATTRIBUTE;


@end
