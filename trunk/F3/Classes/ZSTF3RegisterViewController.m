//
//  ZSTF3RegisterViewController.m
//  F3
//
//  Created by LiZhenQu on 14-10-23.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTF3RegisterViewController.h"
#import "ZSTAgreementViewController.h"
#import "ZSTNewPassWordViewController.h"
#import "ZSTPasswordRemindViewController.h"

#define TIMER_COUNT_NUM 80

#define  kZSTRegister_Register          1        //注册
#define  kZSTRegister_Forget            2        //找回密码
#define  kZSTRegister_ChangePhoneNum    3        //修改手机号
#define  kZSTRegister_ChangePassword    4        //修改密码

@interface ZSTF3RegisterViewController ()
{
    int timerCount;

    BOOL isAgree;
    
    int OperType;
}

@property (nonatomic, strong) NSTimer *timer;

@end

@implementation ZSTF3RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.naviImgView.image = [UIImage imageNamed:@"framework_top_bg.png"];
    
    NSString *title = @"返回";
    CGSize size = [title sizeWithFont:[UIFont systemFontOfSize:14]
                    constrainedToSize:CGSizeMake(121, 28)
                        lineBreakMode:NSLineBreakByTruncatingTail];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(12, 7, size.width + 20, 30);
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    btn.titleLabel.shadowOffset = CGSizeMake(0, -0.5f);
    [btn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[[UIImage imageNamed:@"TKCommonLib.bundle/NaivagationBar/btn_navigationBaritemBack_normal.png"] stretchableImageWithLeftCapWidth:3   topCapHeight:9] forState:UIControlStateNormal];
    [btn setBackgroundImage:[[UIImage imageNamed:@"TKCommonLib.bundle/NaivagationBar/btn_navigationBaritemBack_pressed.png"] stretchableImageWithLeftCapWidth:30 topCapHeight:9] forState:UIControlStateHighlighted];
    [btn setBackgroundImage:[[UIImage imageNamed:@"TKCommonLib.bundle/NaivagationBar/btn_navigationBaritemBack_disable.png"] stretchableImageWithLeftCapWidth:30 topCapHeight:9] forState:UIControlStateDisabled];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.naviView addSubview:btn];
    
    isAgree = NO;
    [self.choicebtn setImage:ZSTModuleImage(@"module_orderinfo_pay_on.png") forState:UIControlStateNormal];
    
    UIImage *imageN = ZSTModuleImage(@"module_login_btn_green_n.png");
    UIImage *ImageP = ZSTModuleImage(@"module_login_btn_green_p.png");
    CGFloat capWidth = imageN.size.width / 2;
    CGFloat capHeight = imageN.size.height / 2;
    imageN = [imageN stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    ImageP = [ImageP stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    [self.verificationCodebtn setBackgroundImage:imageN forState:UIControlStateNormal];
    [self.verificationCodebtn setBackgroundImage:ImageP forState:UIControlStateHighlighted];
    [self.alertbtn setBackgroundImage:imageN forState:UIControlStateNormal];
    [self.alertbtn setBackgroundImage:ImageP forState:UIControlStateHighlighted];
    
    self.engine = [[[ZSTF3Engine alloc] init] autorelease];
    self.engine.delegate = self;
    
//     NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
//    self.agreementLabel.text = [NSString stringWithFormat:@"《%@用户协议》",appDisplayName];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [self.phoneNumField becomeFirstResponder];
    
    // 实现代理
    self.phoneNumField.delegate = self;
    self.verificationCodeField.delegate = self;
    
    switch (self.type) {
        case 1:
            OperType = 1;
            self.titleLabel.text = @"注册";
            break;
        case 2:
            OperType = 2;
            self.titleLabel.text = @"找回密码";
            break;
        case kZSTRegister_ChangePhoneNum:
            OperType = 3;
            self.titleView.hidden = YES;
            self.titleText.hidden = NO;
            self.titleLabel.text = @"更改手机号";
            break;
        case kZSTRegister_ChangePassword:
            OperType = 2;
            self.titleView.hidden = NO;
            self.titleText.hidden = YES;
            self.phoneNumField.text = self.phoneNum;
            self.phoneNumField.textColor = RGBCOLOR(198, 198, 198);
            self.phoneNumField.enabled = NO;
            self.titleLabel.text = @"修改密码";
            break;
        default:
            break;
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
     [self.timer invalidate];
}

- (void)backAction:(id)sender
{
    if (_isfromSetting) {
        
        [self.navigationController dismissViewControllerAnimated:YES completion:^(void) {
            
        }];
    } else {
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (BOOL)validateMobile:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,181,189,1700,1709,1705
     */
    NSString * MOBILE = @"^1(3[0-9]|4[0-9]|5[0-35-9]|7[0-9]|8[0125-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349|700|705|709)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}


- (IBAction)showAgreeAction:(id)sender
{
    ZSTAgreementViewController *controller = [[ZSTAgreementViewController alloc] init];
    controller.source = NO;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navController animated:YES completion:nil];
    [navController release];
    [controller release];
}

- (IBAction)agreeAction:(id)sender
{
    if (isAgree) {
        
        self.verificationCodebtn.enabled = YES;
        [self.choicebtn setImage:ZSTModuleImage(@"module_orderinfo_pay_on.png") forState:UIControlStateNormal];
        isAgree = NO;
    } else {
        
        self.verificationCodebtn.enabled = NO;
        [self.choicebtn setImage:ZSTModuleImage(@"module_orderinfo_pay_off.png") forState:UIControlStateNormal];
        isAgree = YES;
    }
}

- (IBAction)getVerificationCode:(id)sender
{
    [self.phoneNumField resignFirstResponder];
    
    NSString *phoneNumberStr = [self.phoneNumField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (phoneNumberStr.length == 0) {
        [TKUIUtil alertInWindow:@"请输入手机号" withImage:nil];
        return;
    } else if(![self validateMobile:phoneNumberStr]) {
        
        UIAlertView* alertView = [[UIAlertView alloc]
                                  initWithTitle:nil
                                  message:@"您输入的手机号格式有误，请重试"
                                  delegate:self
                                  cancelButtonTitle:@"确定"
                                  otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    
    timerCount = TIMER_COUNT_NUM;
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(changeTimerNum) userInfo:nil repeats:YES];
    [self.timer fire];
    
      [TKUIUtil showHUD:self.view];
     [self.engine getVerificationCodeWithMsisdn:phoneNumberStr OperType:OperType];
}

- (void)changeTimerNum
{
    if (timerCount > 0) {
        self.alertbtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [self.alertbtn setTitle:[NSString stringWithFormat:@"%d 秒",timerCount] forState:UIControlStateDisabled];
        [self.alertbtn setEnabled:NO];
        timerCount--;
    }else {
        [self.alertbtn setTitle:@"重新获取验证码" forState: UIControlStateNormal];
        self.alertbtn.titleLabel.font = [UIFont systemFontOfSize:12];
        [self.alertbtn setEnabled:YES];
        [self.timer invalidate];
    }
}

- (IBAction)sumbitAction:(id)sender
{
    [self.phoneNumField resignFirstResponder];
    [self.verificationCodeField resignFirstResponder];
    
    NSString *phoneNumberStr = [self.phoneNumField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *verificationCodeStr = [self.verificationCodeField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (phoneNumberStr.length == 0) {
        [TKUIUtil alertInWindow:@"请输入手机号" withImage:nil];
        return;
    } else if(![self validateMobile:phoneNumberStr]) {
        
        UIAlertView* alertView = [[UIAlertView alloc]
                                  initWithTitle:nil
                                  message:@"您输入的手机号格式有误，请重试"
                                  delegate:self
                                  cancelButtonTitle:@"确定"
                                  otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    if (verificationCodeStr.length == 0) {
        [TKUIUtil alertInWindow:@"请输入验证码" withImage:nil];
        return;
    }
    
    [TKUIUtil showHUD:self.view];
    
    [self.engine checkVerificationCodeWithMsisdn:phoneNumberStr verificationCode:verificationCodeStr OperType:OperType];
}

#pragma mark ------------------UITextFieldDelegate---------------------

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.phoneNumField && textField.text.length - range.length + string.length > 11) {
        return NO;
    }
    
    if (textField == self.verificationCodeField && textField.text.length - range.length + string.length > 6) {
        return NO;
    }
    
    return YES;
}

#pragma mark ------------------ZSTF3EngineDelegate---------------------

- (void)getVerificationCodeDidSucceed:(NSString*)response
{
    [TKUIUtil hiddenHUD];
}

- (void)getVerificationCodeDidFailed:(NSString*)message
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:message withImage:nil];
    
    timerCount = TIMER_COUNT_NUM;
    [self.alertbtn setTitle:@"获取验证码" forState: UIControlStateNormal];
    self.alertbtn.titleLabel.font = [UIFont systemFontOfSize:12];
    [self.alertbtn setEnabled:YES];
    [self.timer invalidate];
}

- (void)reloadData
{
    [self sumbitAction:nil];
}

- (void)checkVerificationCodeDidSucceed:(NSString*)response
{
    if (self.type == kZSTRegister_ChangePhoneNum) {
        
        [self.engine updateMsisdnWithMsisdn:self.phoneNumField.text userId:[ZSTF3Preferences shared].UserId verificationCode:self.verificationCodeField.text OperType:kZSTRegister_ChangePhoneNum];
        
        return;
    }
    
    [TKUIUtil hiddenHUD];
    
    ZSTNewPassWordViewController *controller = [[ZSTNewPassWordViewController alloc] init];
    controller.phoneNum = self.phoneNumField.text;
    controller.type = self.type;
    controller.verificationCode = self.verificationCodeField.text;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

- (void)checkVerificationCodeDidFailed:(NSString*)message
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:message withImage:nil];
}

- (void)updateMSisdnDidSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    [ZSTF3Preferences shared].loginMsisdn = self.phoneNumField.text;
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (void)updateMSisdnDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:response withImage:nil];
}

- (void)registerDidsucceed:(NSDictionary *)response
{
    
}

- (void)registerDidFailed:(NSString *)response
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void) dealloc
{
    if ([_timer isValid]) {
        
        [_timer invalidate];
    }
    
    self.phoneNumField = nil;
    self.verificationCodeField = nil;
    self.verificationCodebtn = nil;
    self.alertbtn = nil;
    self.choicebtn = nil;
    self.naviView = nil;
    self.naviImgView = nil;
    [_verificationCodebtn release];
     [super dealloc];
}

@end
