//
//  RegisterManager.h
//  F3
//
//  Created by 9588 on 10/24/11.
//  Copyright 2011 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ZSTRegisterTimer : NSObject {
    NSTimer *_timeInterval;
}

+(ZSTRegisterTimer *) sharedTimer;

- (void)start;

- (void)reset;

- (BOOL)isTiming;

- (int)remainingTime;

@end
