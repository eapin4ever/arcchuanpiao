//
//  BusinessAppInfo.m
//  F3
//
//  Created by xuhuijun on 12-3-8.
//  Copyright 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTBusinessAppInfo.h"


@implementation ZSTBusinessAppInfo

@synthesize ID;
@synthesize AppID;
@synthesize Name;
@synthesize ECECCID;
@synthesize IconKey;
@synthesize Introduce;
@synthesize Version;
@synthesize Status;
@synthesize appOrder;
@synthesize Url;
@synthesize Local;
@synthesize ModuleID;
@synthesize ModuleType;
@synthesize InterfaceUrl;

-(NSString *) description
{
	NSMutableString *desc = [[NSMutableString alloc] initWithFormat:
                             @"ID = %@, AppID = %@, Name = %@, ECECCID = %@, IconKey = %@, Introduce = %@, Version = %@, Status = %@ , appOrder = %@ , Url = %@ , Local = %@, ModuleID = %@, ModuleType == %@, InterfaceUrl = %@",
                             @(ID),AppID,Name,ECECCID,IconKey,Introduce,Version,self.Status,@(appOrder),Url,Local,@(ModuleID), @(ModuleType),InterfaceUrl];
	return [desc autorelease];
}

- (void)dealloc
{
    self.AppID = nil;
    self.Name = nil;
    self.ECECCID = nil;
    self.IconKey = nil;
    self.Introduce = nil;
    self.Version = nil;
    self.Status = nil;
    self.Url = nil;
    self.Local = nil;
    self.InterfaceUrl = nil;
    [super dealloc];
}


@end
