//
//  EditView.m
//  F3
//
//  Created by xuhuijun on 11-12-5.
//  Copyright 2011年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTEditView.h"

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

@implementation ZSTEditView

@synthesize window = _window;

- (id)initWithImage:(UIImage *)image
{
    self = [super initWithImage:image];
    if (self) {
        
        _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        _window.windowLevel = UIWindowLevelNormal;
        _window.hidden = YES;
        
        self.userInteractionEnabled = YES;
        
    }
    
    return self;
}

- (void)dealloc
{
//    [_window release];
    [super dealloc];
}

- (void)show
{
    UIButton *label = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 320, 480+(iPhone5?88:0))];
    [label addTarget:self action:@selector(dismiss) forControlEvents:UIControlEventTouchUpInside];
    label.backgroundColor = [UIColor blackColor];
    [_window addSubview:label];
    
    [_window addSubview:self];
    _window.windowLevel = UIWindowLevelStatusBar;
    _window.hidden = NO;
    _window.alpha = 0.3;
    label.alpha = 0.0;

    [UIView beginAnimations:@"show" context:nil];
    [UIView setAnimationDuration:0.3];
    label.alpha = 0.3;
    _window.alpha = 1.0;
    [UIView commitAnimations];
    [label release];
   
}

- (void)dismiss
{
    [self removeFromSuperview];
    _window.windowLevel = UIWindowLevelNormal;
    _window.hidden = YES;

}

@end
