//
//  ZSTLegacyResponse.m
//  
//
//  Created by luobin on 4/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTLegacyResponse.h"
#import "ZSTLegicyCommunicator.h"
#import "ASIHTTPRequest.h"
#import "ZSTLogUtil.h"
#import "ElementParser.h"

#define kTKRequestCodeOK 1

#define kTKRequestUserInfo @"kTKRequestUserInfo"

@interface ZSTLegacyResponse()

@property (nonatomic, retain) ASIHTTPRequest *request;
@property (nonatomic, retain) id userInfo;
@property (nonatomic, retain) NSError *error;
@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSString *stringResponse;
@property (nonatomic, retain) DocumentRoot *xmlResponse;
@property (nonatomic, retain) id jsonResponse;

@end

@implementation ZSTLegacyResponse
@synthesize userInfo;
@synthesize error;
@synthesize stringResponse;
@synthesize xmlResponse;
@synthesize request;
@synthesize type;
@synthesize jsonResponse;

- (void)loadInfoFromRequest:(ASIHTTPRequest *)theRequest
{
    if (request == theRequest) {
        return;
    }
    [request release];
    request = [theRequest retain];
    
    TKCommunicatorUserInfo *theUserInfo = [[request userInfo] objectForKey:kTKRequestUserInfo];
    self.userInfo = theUserInfo.userInfo;
    self.stringResponse = request.responseString;
    self.type = [[request.url.absoluteString lastPathComponent] stringByDeletingPathExtension];
    
    if (request.error) {
        self.error = [NSError errorWithDomain:ZSTHttpRequestErrorDomain 
                                         code:[request.error code] 
                                     userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"ZSTCommunicator request failed.", NSLocalizedDescriptionKey, 
                                               request.url,
                                               NSURLErrorKey,
                                               request.error, NSUnderlyingErrorKey,
                                               nil]
                      ];
        
        TKDERROR(@"request fetch has occur an error.., \n  the request error code : %@, and localizedDescription: %@", @([request.error code]), request.error.localizedDescription);
        
        //记录联网日志
        [ZSTLogUtil logSysConnect:request.url result:NO response:[NSString stringWithFormat:@"%@ %@", @([request.error code]), request.error.localizedDescription]];
        return;
    }
    
    ElementParser *parser = [[[ElementParser alloc] init] autorelease];
    DocumentRoot *root = [parser parseXML:request.responseString];
    
    self.xmlResponse = root;
    
    Element *resultElement = [root selectElement:@"Response Result"];
    if (!resultElement) {
        resultElement = [root selectElement:@"MessageInfo Result"];
    }
    if (!resultElement) {
        self.error = [NSError errorWithDomain:ZSTHttpRequestErrorDomain 
                                         code:ZSTResultCode_Failure
                                     userInfo:[NSDictionary dictionaryWithObjectsAndKeys:@"Result element not found.", NSLocalizedDescriptionKey, 
                                               request.url,
                                               NSURLErrorKey,
                                               nil]];
        TKDERROR(@"request fetch has occur an error.., \n  Result element not found.");
        
        //记录联网日志
        [ZSTLogUtil logSysConnect:request.url result:NO response:@"Result element not found."];
        return;
    }
    
    int resultCode = [[resultElement contentsNumber] intValue];
    if (resultCode == kTKRequestCodeOK) {
        TKDINFO(@"request fetch has succeed.., \n and the responseString are : \n%@", request.responseString);
        self.error = nil;
        //记录联网日志
        [ZSTLogUtil logSysConnect:request.url result:YES response:nil];
        
    } else {
        
        NSString *resultDesc = [[root selectElement:@"Response ResultDesc"] contentsText];
        if (!resultDesc) {
            resultDesc = @"";
        }
        TKDERROR(@"request fetch has occur an error.., \n  the resultCode is : %d, and ResultDesc is %@", resultCode, resultDesc);
        
        self.error = [NSError errorWithDomain:ZSTHttpRequestErrorDomain 
                                         code:resultCode
                                     userInfo:[NSDictionary dictionaryWithObjectsAndKeys:resultDesc, NSLocalizedDescriptionKey,
                                               request.url,
                                               NSURLErrorKey,
                                               nil]];
        //记录联网日志
        [ZSTLogUtil logSysConnect:request.url result:NO response:[NSString stringWithFormat:@"%d %@", resultCode, resultDesc]];
    }
}

- (id)initWithRequest:(ASIHTTPRequest *)theRequest
{
    self = [super init];
    if (self) {
        [self loadInfoFromRequest:theRequest];
    }
    return self;
}

+ (id)responseWithRequest:(ASIHTTPRequest *)request
{
    return [[[ZSTLegacyResponse alloc] initWithRequest:request] autorelease];
}

+ (id)responseWithXmlResponse:(DocumentRoot *)xmlResponse 
               stringResponse:(NSString *)stringResponse 
                        error:(NSError *)error 
                     userInfo:(id)userInfo
{
    ZSTLegacyResponse *response = [[ZSTLegacyResponse alloc] init];
    response.xmlResponse = xmlResponse;
    response.stringResponse = stringResponse;
    response.error = error;
    response.userInfo = userInfo;
    return [response autorelease];
}

+ (id)responseWithJSONResponse:(id)jsonResponse 
                stringResponse:(NSString *)stringResponse 
                         error:(NSError *)error 
                      userInfo:(id)userInfo
{
    ZSTLegacyResponse *response = [[ZSTLegacyResponse alloc] init];
    response.jsonResponse = jsonResponse;
    response.stringResponse = stringResponse;
    response.error = error;
    response.userInfo = userInfo;
    return [response autorelease];
}

-(void)dealloc
{
    TKRELEASE(request);
    self.jsonResponse = nil;
    self.type = nil;
    self.userInfo = nil;
    self.error = nil;
    self.stringResponse = nil;
    self.xmlResponse = nil;
    [super dealloc];
}

@end
