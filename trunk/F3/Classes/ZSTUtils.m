//
//  ZSTUtils.m
//  F3
//
//  Created by luo bin on 11/9/11.
//  Copyright 2011 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTUtils.h"
#import <AudioToolbox/AudioToolbox.h>
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import "ABContactsHelper.h"
#import "ZSTF3Preferences.h"

static void AudioServicesSystemSoundCompletionCallback(  SystemSoundID  ssID, void* clientData)
{
    AudioServicesDisposeSystemSoundID(ssID);
}

@implementation ZSTUtils

+(void) showAlertTitle: (NSString *) title message: (NSString *) message
{
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle: title
                                                     message: message
                                                    delegate:self
                                           cancelButtonTitle:NSLocalizedString(@"确定", @"")
                                           otherButtonTitles:nil] ;
    [alert show];
    [alert release];
}

+(void)playAlertSound:(NSString *)name withExtension:extension
{
    
    SystemSoundID soundFileObject;
    
    NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:extension];
    NSURL *url = [NSURL fileURLWithPath:path];
    
    // Create a system sound object representing the sound file
    AudioServicesCreateSystemSoundID (
                                      (CFURLRef)url,
                                      &soundFileObject
                                      );
    // Add sound completion callback
    AudioServicesAddSystemSoundCompletion (soundFileObject, NULL, NULL, AudioServicesSystemSoundCompletionCallback, NULL);
    // Play the audio
    AudioServicesPlaySystemSound(soundFileObject);
    //AudioServicesPlayAlertSound(soundFileObject);
}


+(NSString *)makeDocumentRelativePath:(NSString *)absolutePath
{
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    //NSAssert([absolutePath hasPrefix:docPath], @"absolutePath:'%@' must has Prefix of docPath:'%@'", absolutePath, docPath);
    NSString *relativePath = [absolutePath substringFromIndex:[docPath length]];
    return [NSString stringWithFormat:@"documents://%@", relativePath];
}

+ (BOOL)isImagePath:(NSString *)path
{
    NSString *pathExtension = [[path pathExtension] lowercaseString];
    NSArray *imageExtension = [NSArray arrayWithObjects:@"png", @"jpg", @"jpeg" ,@"tif", @"gif", @"ico", @"cur", @"xbm", @"bmp", nil];
    if ([imageExtension containsObject:pathExtension]) {
        return YES;
    }
    return NO;
}

+ (BOOL)isImageURL:(NSURL *)url
{
    NSString *pathExtension = [[[url absoluteString] pathExtension] lowercaseString];
    NSArray *imageExtension = [NSArray arrayWithObjects:@"png", @"jpg", @"jpeg" ,@"tif", @"gif", @"ico", @"cur", @"xbm", @"bmp", nil];
    if ([imageExtension containsObject:pathExtension]) {
        return YES;
    }
    return NO;
}

+ (BOOL)isHTMLPath:(NSString *)path
{
    NSString *pathExtension = [[path pathExtension] lowercaseString];
    if ([pathExtension isEqualToString:@"html"] || [pathExtension isEqualToString:@"htm"]) {
        return YES;
    }
    return NO;
}

+(NSDateComponents *) dateComponents:(NSDate *)date
{
	NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents * components = [calendar components:NSYearCalendarUnit| NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit |
									 NSMinuteCalendarUnit | NSSecondCalendarUnit
                                                fromDate: date];
	return components;
}

+(NSDateComponents *) dateComponents
{
	return [ZSTUtils dateComponents:[NSDate date]];
}

+(NSString *) getCurrentDate
{
	NSDateComponents * components = [ZSTUtils dateComponents];
	
	NSString *s = [NSString stringWithFormat: @"%ld-%02ld-%02ld %02ld:%02ld:%02ld",[components year], [components month], [components day], [components hour], [components minute], [components second]];
	
	return s;
}

//转换日期格式成yyyy-MM-dd HH:mm:ss的标准格式
//etc. 2011-2-2 12:02:03  --->  2011-02-02 12:02:03
+(NSDate *)convertToDate:(NSString *)string
{
    NSDateComponents *components = [[NSDateComponents alloc] init];
    
    string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    NSArray *array = [string componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if ([array count] == 2) {
        NSString *date = [array objectAtIndex:0];
        NSArray *dateDrray = [date componentsSeparatedByString:@"-"];
        if ([dateDrray count] == 3) {
            int year = [[dateDrray objectAtIndex:0] intValue];
            int month = [[dateDrray objectAtIndex:1] intValue];
            int day = [[dateDrray objectAtIndex:2] intValue];
            [components setYear:year];
            [components setMonth:month];
            [components setDay:day];
        }
        
        NSString *time = [array objectAtIndex:1];
        array = [time componentsSeparatedByString:@":"];
        if ([array count] == 3) {
            int hour = [[array objectAtIndex:0] intValue];
            int minute = [[array objectAtIndex:1] intValue];
            int second = [[array objectAtIndex:2] intValue];
            [components setHour:hour];
            [components setMinute:minute];
            [components setSecond:second];
        }
    }
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *date = [gregorian dateFromComponents:components];
    
    [gregorian release];
    [components release];
    
    return date;
}

+(NSString *)formatDate:(NSDate *)date format:(NSString *)format
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:format];
    NSString *dateString = [formatter stringFromDate:date];
    [formatter release];
    return dateString;
}

+(BOOL)isInOneDate:(NSDate *)date1 date2:(NSDate *)date2
{
    NSDateComponents * components1 = [ZSTUtils dateComponents:date1];
    NSDateComponents * components2 = [ZSTUtils dateComponents:date2];
    return [components1 day] == [components2 day] && [components1 month] == [components2 month] && [components1 year] == [components2 year];
}

+ (id)objectForArray:(id)v
{
    return v == nil?[NSNull null]:v;
}

+ (UIColor *)colorFromHexColor:(NSString *)hexColor
{
    if (![hexColor hasPrefix:@"#"]) {
        return [UIColor whiteColor];
    }
    
    if ([hexColor length] < 6) {
        return [UIColor whiteColor];
    }
    hexColor = [hexColor substringFromIndex:[hexColor length] - 6];
    
	unsigned int red,green,blue;
	NSRange range;
	range.length = 2;
	
	range.location = 0;
	[[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&red];
	
	range.location = 2;
	[[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&green];
	
	range.location = 4;
	[[NSScanner scannerWithString:[hexColor substringWithRange:range]] scanHexInt:&blue];
	
	return [UIColor colorWithRed:(float)(red/255.0f) green:(float)(green / 255.0f) blue:(float)(blue / 255.0f) alpha:1.0f];
}

+(CGSize)supportHighestResolution
{
    return [UIScreen instancesRespondToSelector:@selector(currentMode)] ? [[UIScreen mainScreen] currentMode].size : CGSizeMake(320, 480);
}

+ (NSString *)getNameWithPhoneNumber:(NSString *)phoneNumber
{
    NSArray *contacts = [ABContactsHelper contacts];//接收电话簿所有信息
    
	for (ABContact *contact in contacts)
	{
		for (NSString *number in contact.phoneArray)
		{
            number = [number stringByReplacingOccurrencesOfString: @"(" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @")" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @"\r" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @"\n" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @"\t" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @"-" withString: @""];
            number = [number stringByReplacingOccurrencesOfString: @" " withString: @""];
            number = [number stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            if ([phoneNumber isEqualToString:number]) {
                return contact.contactName;
            }
		}
	}
    return nil;
}

+(void)playNMSAlert
{
    if ([ZSTF3Preferences shared].sound)
    {
        [ZSTUtils playAlertSound:@"sms-received1" withExtension:@"caf"];
        
    }
    if ([ZSTF3Preferences shared].shake)
    {
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }
    
}

+ (NSString *)pathForLogs
{
    NSFileManager *fm = [NSFileManager defaultManager];
    
    NSString *logsDir = [[self pathForECECC] stringByAppendingPathComponent:@"logs"];
    
    if (![fm fileExistsAtPath:logsDir]) {
        NSDictionary *attributes = [NSDictionary dictionaryWithObject: [NSNumber numberWithUnsignedLong: 0777] forKey: NSFilePosixPermissions];
        NSError *theError = nil;
        
        [fm createDirectoryAtPath:logsDir withIntermediateDirectories: YES attributes: attributes error: &theError];
    }
    
    return logsDir;
}

+ (NSString *)pathForInbox
{
    NSFileManager *fm = [NSFileManager defaultManager];
    
    NSString *inboxDir = [[self pathForECECC] stringByAppendingPathComponent:@"inbox"];
    
    if (![fm fileExistsAtPath:inboxDir]) {
        NSDictionary *attributes = [NSDictionary dictionaryWithObject: [NSNumber numberWithUnsignedLong: 0777] forKey: NSFilePosixPermissions];
        NSError *theError = nil;
        
        [fm createDirectoryAtPath:inboxDir withIntermediateDirectories: YES attributes: attributes error: &theError];
    }
    
    return inboxDir;
}

+ (NSString *)pathForDownloadCache
{
    NSFileManager *fm = [NSFileManager defaultManager];
    
    NSString *downloadCacheDir = [[self pathForECECC] stringByAppendingPathComponent:@"downloadCache"];
    
    if (![fm fileExistsAtPath:downloadCacheDir]) {
        NSDictionary *attributes = [NSDictionary dictionaryWithObject: [NSNumber numberWithUnsignedLong: 0777] forKey: NSFilePosixPermissions];
        NSError *theError = nil;
        
        [fm createDirectoryAtPath:downloadCacheDir withIntermediateDirectories: YES attributes: attributes error: &theError];
    }
    
    return downloadCacheDir;
}

+ (NSString *)pathForCover
{
    NSFileManager *fm = [NSFileManager defaultManager];
    
    NSString *coverCacheDir = [[self pathForECECC] stringByAppendingPathComponent:@"coverCache"];
    
    if (![fm fileExistsAtPath:coverCacheDir]) {
        NSDictionary *attributes = [NSDictionary dictionaryWithObject: [NSNumber numberWithUnsignedLong: 0777] forKey: NSFilePosixPermissions];
        NSError *theError = nil;
        
        [fm createDirectoryAtPath:coverCacheDir withIntermediateDirectories: YES attributes: attributes error: &theError];
    }
    
    return coverCacheDir;
}

+ (NSString *)pathForOutbox
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *outboxDir = [[self pathForECECC] stringByAppendingPathComponent:@"outbox"];
    
    if (![fm fileExistsAtPath:outboxDir]) {
        NSDictionary *attributes = [NSDictionary dictionaryWithObject: [NSNumber numberWithUnsignedLong: 0777] forKey: NSFilePosixPermissions];
        NSError *theError = NULL;
        
        [fm createDirectoryAtPath:outboxDir withIntermediateDirectories: YES attributes: attributes error: &theError];
    }
    return outboxDir;
}

+ (NSString *)pathForTempFinderOfOutbox

{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *tempFinderDir = [[self pathForOutbox] stringByAppendingPathComponent:@"tempFinder"];
    
    if (![fm fileExistsAtPath:tempFinderDir]) {
        NSDictionary *attributes = [NSDictionary dictionaryWithObject: [NSNumber numberWithUnsignedLong: 0777] forKey: NSFilePosixPermissions];
        NSError *theError = NULL;
        
        [fm createDirectoryAtPath:tempFinderDir withIntermediateDirectories: YES attributes: attributes error: &theError];
    }
    return tempFinderDir;
}
+ (NSString *)pathForTempFinderOfSNSA

{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *tempFinderDir = [NSString stringWithFormat:@"%@/Library/Caches/%@",NSHomeDirectory(),@"tempFinder"];
    
    if (![fm fileExistsAtPath:tempFinderDir]) {
        NSDictionary *attributes = [NSDictionary dictionaryWithObject: [NSNumber numberWithUnsignedLong: 0777] forKey: NSFilePosixPermissions];
        NSError *theError = NULL;
        
        [fm createDirectoryAtPath:tempFinderDir withIntermediateDirectories: YES attributes: attributes error: &theError];
    }
    return tempFinderDir;
}

+ (NSString *)pathForSendFileOfOutbox:(NSInteger)ID
{
    //    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *fileName = @(ID).stringValue;
    NSString *sendFileDir = [[self pathForOutbox] stringByAppendingPathComponent:fileName];
    
    //    if (![fm fileExistsAtPath:sendFileDir]) {
    //        NSDictionary *attributes = [NSDictionary dictionaryWithObject: [NSNumber numberWithUnsignedLong: 0777] forKey: NSFilePosixPermissions];
    //        NSError *theError = NULL;
    //
    //        [fm createDirectoryAtPath:sendFileDir withIntermediateDirectories: YES attributes: attributes error: &theError];
    //    }
    return sendFileDir;
}

+(NSString *) getECECCID
{
    NSString *ECECCID = [[NSUserDefaults standardUserDefaults] stringForKey:@"ECECCID"];
    
    if (ECECCID == nil || [ECECCID length] == 0) {
        
        NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
        NSArray *separatedStrings = [bundleIdentifier componentsSeparatedByString:@"."];
        NSString *ECECCIDStr = [separatedStrings lastObject];
        
        if (![ECECCIDStr isEqualToString:@"f3"]) {
            ECECCID = ECECCIDStr;
        } else {
            ECECCID = @"100831";
        }
        [[NSUserDefaults standardUserDefaults] setObject:ECECCID forKey:@"ECECCID"];
    }
    return ECECCID;
}

+ (NSString *)pathForECECC
{
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    docPath= [docPath stringByAppendingPathComponent:[ZSTUtils getECECCID]];
    if (![fm fileExistsAtPath:docPath]) {
        NSDictionary *attributes = [NSDictionary dictionaryWithObject: [NSNumber numberWithUnsignedLong: 0777] forKey: NSFilePosixPermissions];
        [fm createDirectoryAtPath:docPath withIntermediateDirectories: YES attributes: attributes error: nil];
    }
    return docPath;
}

+(NSString *)httpSever:(NSString *)url
{
    NSInteger port = [ZSTF3Preferences shared].httpServerPort;
    if (port == 0 || port == 80) {
        return [NSString stringWithFormat:@"%@%@",[ZSTF3Preferences shared].httpServer, url];
    } else {
        return [NSString stringWithFormat:@"%@:%@%@",[ZSTF3Preferences shared].httpServer, @(port), url];
    }
}

+ (UIColor *)getBGColor
{
    NSString *hexColor = [[NSBundle mainBundle] localizedStringForKey:@"GP_BGColor" value:@"#c3d1d5" table:nil];
    return [ZSTUtils colorFromHexColor:hexColor];
}

+ (UIColor *)getNavigationTintColor
{
    NSString *hexColor = [[NSBundle mainBundle] localizedStringForKey:@"GP_NavigationTintColor" value:@"#c3d1d5" table:nil];
    return [ZSTUtils colorFromHexColor:hexColor];
}

+ (UIColor *)getNavigationTextColor
{
    NSString *hexColor = [[NSBundle mainBundle] localizedStringForKey:@"GP_nav_top_n_textColor" value:@"#ffffff" table:nil];
    return [ZSTUtils colorFromHexColor:hexColor];
    
}

+ (NSString*)convertToTime:(NSString*)message
{
    NSString* time = nil;
	NSCalendar* calendar= [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSCalendarUnit unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
	NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    
    double dd = [message doubleValue];
    NSDate* createdAt = [NSDate dateWithTimeIntervalSince1970:dd];
    NSDateComponents *nowComponents = [calendar components:unitFlags fromDate:[NSDate date]];
    NSDateComponents *createdAtComponents = [calendar components:unitFlags fromDate:createdAt];
	if([nowComponents year] == [createdAtComponents year] &&
       [nowComponents month] == [createdAtComponents month] &&
       [nowComponents day] == [createdAtComponents day])
    {//今天
		
		int time_long = [createdAt timeIntervalSinceNow];
        if (time_long > 0) {
            time_long = 0;
        }
		
		if (time_long >-60*60) {//一小时之内
			int min = -time_long/60;
			if (min == 0) {
				min = 1;
			}
			time = [[[NSString alloc]initWithFormat:NSLocalizedString(@"%d分钟前",@""),min] autorelease];
			
		}else {
			[dateFormatter setDateFormat:NSLocalizedString(@"'今天'HH:mm",@"")];
			time = [dateFormatter stringFromDate:createdAt];
		}
    }
    else
    {//前天及以前
		NSLocale *cnLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
		[dateFormatter setLocale:cnLocale];
		[cnLocale release];
		[dateFormatter setDateFormat:NSLocalizedString(@"M月d日' 'HH:mm",@"")];
		time = [dateFormatter stringFromDate:createdAt];
    }
    [calendar release];
    [dateFormatter release];
	
    return time;
}

+(UIButton *)customButtonTitle:(NSString *)title nImage:(UIImage *)nImage pImage:(UIImage *)pImage titleFontSize:(float)size conerRsdius:(float)rsdius
{
    
    UIButton *customButton = [UIButton buttonWithType:UIButtonTypeCustom];
    customButton.frame = CGRectMake(0, 0, 50, 26);
    [customButton setBackgroundImage:nImage forState:UIControlStateNormal];
    [customButton setBackgroundImage:pImage forState:UIControlStateHighlighted];
    [customButton setTitle:title forState:UIControlStateNormal];
    customButton.titleLabel.font = [UIFont boldSystemFontOfSize:size];
    
    NSString *n_hexColor = [[NSBundle mainBundle] localizedStringForKey:@"GP_btn_n_textColor" value:@"#ffffff" table:nil];
    [customButton setTitleColor:[ZSTUtils colorFromHexColor:n_hexColor] forState:UIControlStateNormal];
    NSString *p_hexColor = [[NSBundle mainBundle] localizedStringForKey:@"GP_btn_p_textColor" value:@"#ffffff" table:nil];
    [customButton setTitleColor:[ZSTUtils colorFromHexColor:p_hexColor] forState:UIControlStateHighlighted];
    
    CALayer * iconImageViewLayer = [customButton layer];
    [iconImageViewLayer setMasksToBounds:YES];
    [iconImageViewLayer setCornerRadius:rsdius];
    [iconImageViewLayer setBorderWidth:1.0];
    [iconImageViewLayer setBorderColor:[[UIColor clearColor] CGColor]];
    
    return customButton;
}

+ (UIView *)logoView
{
    UIImage *logoImage = [UIImage imageNamed:@"framework_top_banner_txt.png"];
    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:logoImage];
    logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    logoImageView.tag = 765;
    logoImageView.frame = CGRectMake(-170/2.0, -25/2.0, 170, 25);
    
    //需要使用view搭载imageView才能显示图片，原因不明。
    UIView *bgView = [[UIView alloc] init];
    [bgView addSubview:logoImageView];
    [logoImageView release];
    
    return [bgView autorelease];
}

+ (UIImageView *)shellClogoView
{
    UIImage *logoImage = [UIImage imageNamed:@"framework_top_banner_txt.png"];
    UIImageView *logoImageView = [[UIImageView alloc] initWithImage:logoImage];
    logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    logoImageView.tag = 765;
    logoImageView.frame = CGRectMake(0, 0, 170, 25);
    return [logoImageView autorelease];
}

+ (UIView *)titleViewWithTitle:(NSString *)title
{
    UILabel *titlelal = [[UILabel alloc] initWithFrame:CGRectMake(-(170)/2.0, -25/2.0, 170, 25)];
    titlelal.backgroundColor = [UIColor clearColor];
    titlelal.textAlignment = NSTextAlignmentCenter;
    titlelal.font = [UIFont boldSystemFontOfSize:18];
    titlelal.textColor = [ZSTUtils getNavigationTextColor];
    titlelal.text = title;
    
    UIView *bgView = [[UIView alloc] init];
    [bgView addSubview:titlelal];
    [titlelal release];
    
    return [bgView autorelease];
}

+ (NSString *) imageDateString
{
	NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
	formatter.dateFormat = @"YYMMdd_HHmmss";
	return [[formatter stringFromDate:[NSDate date]] stringByAppendingString:@".jpg"];
}
+ (NSString *) audioDateString
{
	NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
	formatter.dateFormat = @"YYMMdd_HHmmss";
	return [[formatter stringFromDate:[NSDate date]] stringByAppendingString:@".wav"];
}

+ (NSString *) videoDateString
{
    NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
	formatter.dateFormat = @"YYMMdd_HHmmss";
	return [[formatter stringFromDate:[NSDate date]] stringByAppendingString:@".mp4"];
}

+ (NSInteger)fileSize:(NSString *)path
{
    NSDictionary * attributes = [[NSFileManager defaultManager] attributesOfItemAtPath:path error:nil];
    NSNumber *theFileSize = [attributes objectForKey:NSFileSize];
    NSInteger size = [theFileSize intValue]/1024;
    return size;
}

+ (NSString *) setNeedReloadMessageType
{
    NSDateFormatter *dateformat=[[[NSDateFormatter alloc] init] autorelease];
    [dateformat setDateFormat:@"YYMMdd_HHmmss"];
    NSDate *date = [NSDate date];
    [[NSDate date] timeIntervalSince1970];
    NSString * currntUpdateDate = [dateformat stringFromDate:date];
    [[NSUserDefaults standardUserDefaults] setObject:currntUpdateDate forKey:@"CurrntUpdateDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    return currntUpdateDate;
}

+ (BOOL)messageTypeIsUpdate
{
    NSUserDefaults * userDefault = [NSUserDefaults standardUserDefaults];
    NSString *currntUpdateDate = [userDefault objectForKey:@"CurrntUpdateDate"];
    NSString *perUpdateDate = [userDefault objectForKey:@"perUpdateDate"];
    if ([currntUpdateDate isEqualToString:perUpdateDate]) {
        return NO;
    }else{
        [userDefault setObject:currntUpdateDate forKey:@"perUpdateDate"];
        return YES;
    }
}

+ (NSString *) convertToStandardTime:(NSString *)timeString
{
    NSArray *timeArray = [timeString componentsSeparatedByString:@":"];
    NSMutableArray *convertTimeArray = [NSMutableArray array];
    if ([timeArray count] == 2) {
        
        for (int i = 0; i< [timeArray count]; i++) {
            
            NSString *aTime = [timeArray objectAtIndex:i];
            if ([aTime length] == 1) {
                aTime = [NSString stringWithFormat:@"0%@",aTime];
            }
            [convertTimeArray addObject:aTime];
        }
        [convertTimeArray addObject:@"00"];
        
    }
    
    NSString *standardTime = [convertTimeArray componentsJoinedByString:@":"];
    return standardTime;
}

+ (NSString *)md5Signature:(NSDictionary *)params{
    NSString *password = [ZSTF3Preferences shared].loginPassword;
    if (password==nil) {
        password = @"";
    }
    
    NSMutableString *result = [NSMutableString string];
    
    NSArray *keys = [params allKeys];
    keys = [keys sortedArrayUsingSelector:@selector(compare:)];
    
    for (NSString *key in keys) {
        [result appendString:[NSString stringWithFormat:@"%@=%@&", [key lowercaseString], [params objectForKey:key]]];
    }
    
    [result appendString:[NSString stringWithFormat:@"loginpassword=%@", password]];
    return [[TKUtil md5:result] uppercaseString];
}

+ (NSDictionary *)lowerKeys:(NSDictionary *)params
{
    NSString *password = [ZSTF3Preferences shared].loginPassword;
    if (password==nil) {
        password = @"";
    }
    
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    NSArray *keys = [params allKeys];
    keys = [keys sortedArrayUsingSelector:@selector(compare:)];
    
    for (NSString *key in keys) {
        [result setSafeObject:[params objectForKey:key] forKey:[key lowercaseString]];
    }
    return (NSDictionary *)result;
}


+ (BOOL)plistAddURLName:(NSString *)urlName URLSchemes:(NSString *)urlScheme
{
    NSString *plistPath = [[NSBundle mainBundle] pathForResource:@"Info" ofType:@"plist"];
    NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    
    NSMutableArray *URLTypes = [NSMutableArray arrayWithArray:[dictionary objectForKey: @"CFBundleURLTypes"]];
    NSArray *urlSchemes = [NSArray arrayWithObject:urlScheme];
    NSDictionary *urlTypeDic = [NSDictionary dictionaryWithObjectsAndKeys:urlName,@"CFBundleURLName",urlSchemes,@"CFBundleURLSchemes", nil];
    [URLTypes addObject:urlTypeDic];
    
    [dictionary setValue:(NSArray *)URLTypes forKey:@"CFBundleURLTypes"];
    BOOL isOK = [dictionary writeToFile:plistPath atomically:YES];
    return isOK;
}


@end
