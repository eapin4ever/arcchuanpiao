//
//  ZSTModuleDelegate.h
//  F3Engine
//
//  Created by luobin on 7/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ZSTModuleApplication.h"

@protocol ZSTModuleDelegate <NSObject>

@property (nonatomic, retain) ZSTModuleApplication *application;
@property (assign, readonly) UIViewController *rootViewController;
@property (assign, readonly) UIView *rootView;


@optional

- (void)moduleApplication:(ZSTModuleApplication *)application willFinishLaunchingWithOptions:(NSDictionary *)launchOptions;
- (void)moduleApplication:(ZSTModuleApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

@end