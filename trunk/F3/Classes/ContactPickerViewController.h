//
//  contact.h
//  Net_Information
//
//  Created by huqinghe on 11-6-5.
//  Copyright 2011 Shinetechchina.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactInfo.h"
#import "ABContactsHelper.h"

@class ZSTCreateNMSDialogViewController;

@protocol ContactViewControllerDelegate;

@interface ContactPickerViewController : UIViewController <UITableViewDelegate,
UITableViewDataSource, UISearchBarDelegate>
{
	NSArray *iphone;
    NSArray *myArray;
	NSMutableArray *_filteredArray;
	NSMutableArray *_contacts;
    NSMutableArray *_contactArray;
    NSMutableArray *_contactInfoArray;
    NSMutableArray *_tempArrayContactInfoArray;
    NSMutableArray *_contactNameArray;
    NSMutableDictionary *_contactNameDic;
    NSMutableArray *_sectionArray;
    
	UISearchBar *_searchBar;
    UISearchDisplayController *_searchDC;
	
	UITableView *_contactTableview;
	
    ZSTCreateNMSDialogViewController *_createNMSDialogViewController;
    id<ContactViewControllerDelegate> _delegate;
    
    UIBarButtonItem *_confirmButton;
    
    UINavigationController *_aBPersonNav;
	UINavigationController *_aBNewPersonNav;
}

@property (retain) NSArray *contacts;
@property (retain) NSArray *myArray;
@property (retain) NSMutableArray *filteredArray;
@property (retain) UISearchBar *searchBar;
@property (retain) UITableView *contactTableview;
@property (retain) UISearchDisplayController *searchDC;
@property (retain) NSMutableArray *sectionArray;

@property (retain) NSMutableArray *contactNameArray;

//@property (copy, nonatomic) NSArray *contactNumberArray;
@property (copy, nonatomic)  NSArray *contactInfoArray;
@property (nonatomic, assign) id<ContactViewControllerDelegate>delegate;
@property (retain, nonatomic) ZSTCreateNMSDialogViewController *createNMSDialogViewController;

@property (retain) UINavigationController *aBPersonNav;
@property (retain) UINavigationController *aBNewPersonNav;
@end

@protocol ContactViewControllerDelegate <NSObject>

@optional

- (void)contactPickerViewDidFinishSelect:(ContactPickerViewController *)contactViewController;
- (void)contactPickerViewDidBeginSelect:(ContactPickerViewController *)contactViewController;
- (void)contactPickerViewCancelSelect:(ContactPickerViewController *)contactViewController;

@end