//
//  BusinessAppWebView.h
//  F3
//
//  Created by xuhuijun on 12-3-9.
//  Copyright 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTBusinessAppInfo.h"
#import "ZSTScrollToolBarView.h"

@interface ZSTBusinessAppWebViewController : UIViewController <UIWebViewDelegate,ZSTScrollToolBarViewDelegate>
{
    ZSTBusinessAppInfo *_appInfo;
    
    UIWebView *_businessAppWebView;
    NSURL *_url;
    
    BOOL webCanGoBack;
    BOOL webCanGoForward;
    
    UIButton *_backButton;
    UIButton *_forwardButton;
    
    ZSTScrollToolBarView *_scrollToolBarView;
}

@property (nonatomic, retain) ZSTBusinessAppInfo *appInfo;
@property(nonatomic,retain) NSURL *url;
@property(nonatomic) BOOL webCanGoBack;
@property(nonatomic) BOOL webCanGoForward;
@end
