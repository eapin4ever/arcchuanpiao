////
////  BusinessApplicationViewController.m
////  F3
////
////  Created by xuhuijun on 12-3-8.
////  Copyright 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
////
//
//#import "ZSTBusinessApplicationViewController.h"
//#import "ZSTF3Preferences.h"
//#import "ZSTUtils.h"
//#import "TKUIUtil.h"
//#import "ZSTLogUtil.h"
//#import "ZSTBusinessAppInfo.h"
//#import "ZSTBusinessAppDao.h"
//#import "ZSTBusinessAppWebViewController.h"
//#import "ZSTLogUtil.h"
//#import "ElementParser.h"
//#import "ZSTModuleManager.h"
//
//@implementation ZSTBusinessApplicationViewController
//
//- (void)dealloc
//{
//    [[ZSTLegicyCommunicator shared] cancelByDelegate:self];
//    [_appView release];
//    [_activityView release];_activityView = nil;
//    [super dealloc];
//}
//
//- (void)updateView 
//{
//    [_businessAppsInfo release];
//    _businessAppsInfo = (NSMutableArray *)[[[ZSTBusinessAppDao shareBusinessAppDao] getBusinessAppnfos] retain];
//    [_appView reloadData];
//    
//}
//
//- (void)progressWillStart
//{
//    _activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
//    _activityView.frame = CGRectMake(280.0f, 12.0f, 20.0f, 20.0f);
//    [_activityView startAnimating];
//    [self.navigationController.navigationBar addSubview:_activityView];
//}
//
//- (void)reloadAppsData
//{
//    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:3];
//    [params setObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"LoginMsisdn"];
//    [params setObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECECCID"];
//    [params setObject:@"2" forKey:@"NativeTypes"];
//    [params setObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
//    [self progressWillStart];
//    
//    NSString *SyncCompanyAppString = [ZSTUtils httpSever:SYNCCOMPANYAPP_URL_PATH];
//    
//    [[ZSTLegicyCommunicator shared] openAPIPostToMethod:SyncCompanyAppString replacedParams:params delegate:self];
//}
//
//#pragma mark - ViewDidLoad
//
//- (void)viewDidLoad
//{
//    [super viewDidLoad];
//    self.view.backgroundColor = [UIColor whiteColor];
//    self.navigationItem.titleView = [ZSTUtils logoView];
//    [ZSTUtils setNeedReloadMessageType];
//    
//    _appView = [[ZSTLauncherView alloc] init];
//    _appView.columnCount = 2;
//    _appView.rowCount = 2;
//    _appView.isHorizontal = NO;
//    _appView.frame = CGRectMake(0, 0, 320, 480-20-44-49);
//    _appView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"bg.png"]];
//    _appView.layer.contents = (id) [UIImage imageNamed:@"bg.png"].CGImage;
//
//    _appView.delegate = self;
//    _appView.dataSource = self;
//
//	[self.view addSubview:_appView];
//    
//    [self updateView];
//    [self reloadAppsData];
//    
//    [ZSTLogUtil logUserAction:NSStringFromClass([ZSTBusinessApplicationViewController class])];
//
//}
//
//#pragma mark -ZSTLauncherViewDataSource
//
//-(NSUInteger)numberOfItemsForLauncherView:(ZSTLauncherView *)launcherView
//{
//    return [_businessAppsInfo count];
//}
//
//-(ZSTLaucherItem *)launcherView:(ZSTLauncherView *)launcherView itemForIndex: (NSUInteger)index
//{
//    ZSTBusinessAppInfo *info = [_businessAppsInfo objectAtIndex:index];
//    
//    ZSTLaucherItem *item = [ZSTLaucherItem itemWithTitle:info.Name icon:[NSString stringWithFormat:GETFILEIMAGE_PATH,info.IconKey] url:info.Url];
//    item.enable = YES;
//    return item;
//}
//
//#pragma mark - ZSTLauncherViewDelegate
//
//-(void)launcherView:(ZSTLauncherView *)launcherView didClickItemAtIndex:(NSUInteger)index
//{
//    ZSTBusinessAppInfo *info = [_businessAppsInfo objectAtIndex:index];
//    BOOL launchModule = NO;
//    NSInteger moduleID = info.ModuleID;
//    
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setSafeObject:info.AppID forKey:@"AppID"];
//    [params setSafeObject:info.Name forKey:@"Name"];
//    [params setSafeObject:info.InterfaceUrl forKey:@"InterfaceUrl"];
//    [params setSafeObject:[NSNumber numberWithInteger:info.ModuleType] forKey:@"type"];
//    
//    ZSTModule *module = [[ZSTModuleManager shared] findModule:moduleID];
//    if (module) {
//        UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:moduleID withOptions:params];
//        if (controller) {
//            controller.hidesBottomBarWhenPushed = YES;
//            controller.navigationItem.titleView = [ZSTUtils titleViewWithTitle:info.Name];
//            controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:@"返回" target:self selector:@selector (popViewController)];
//            [self.navigationController pushViewController:controller animated:YES];
//            launchModule = YES;
//        }
//    }
//    if (!launchModule) {
//        ZSTBusinessAppWebViewController *appWebViewController = [[ZSTBusinessAppWebViewController alloc] init];
//        appWebViewController.appInfo = info;
//        appWebViewController.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:appWebViewController animated:YES];
//        [appWebViewController release];
//    }
//}
//
//#pragma mark - ZSTLegicyCommunicatorDelegate
//
//- (void)requestDidSucceed:(ZSTLegacyResponse *)response
//{
//    DocumentRoot *element = response.xmlResponse;
//    NSArray *appElements = [element selectElements:@"Response Apps App"];
//    if ([appElements count]) {
//        //表现为异步就是在这里就显示默认图片
//        [[ZSTBusinessAppDao shareBusinessAppDao] deleteAllBusinessAppInfo];
//    }
//    for (Element *element in appElements) {
//        NSString *appID = [[element selectElement:@"AppID"] contentsText];
//        if (appID != nil) {
//            ZSTBusinessAppInfo *info = [[ZSTBusinessAppInfo alloc] init];
//            
//            info.AppID = appID;
//            info.Name = [[element selectElement:@"Name"] contentsText];
//            info.ECECCID = [[element selectElement:@"ECECCID"] contentsText];
//            info.IconKey = [[element selectElement:@"IconKey"] contentsText];
//            info.Introduce = [[element selectElement:@"Introduce"] contentsText];
//            info.Version = [[element selectElement:@"Version"] contentsText];
//            info.Status = [[element selectElement:@"Status"] contentsText];
//            info.appOrder = [[[element selectElement:@"Order"] contentsNumber] intValue];
//            info.Url = [[element selectElement:@"Url"] contentsText];
//            info.Local = [[element selectElement:@"Local"] contentsText];
//            info.ModuleID = [[[element selectElement:@"ModuleID"] contentsNumber] integerValue];
//            info.ModuleType = [[[element selectElement:@"ModuleType"] contentsNumber] integerValue];
//            info.InterfaceUrl = [[element selectElement:@"InterfaceUrl"] contentsText];
//            [[ZSTBusinessAppDao shareBusinessAppDao] insertOrReplaceBusinessAppInfo:info];
//            [info release];
//        }
//    }
//    
//    [self updateView];
//    
//    if ([_activityView isAnimating]) {
//        [_activityView stopAnimating];
//    }
//}
//
//- (void)requestDidFail:(ZSTLegacyResponse *)response
//{
//    if ([_activityView isAnimating]) {
//        [_activityView stopAnimating];
//    }
//    [TKUIUtil showHUDInView:self.view withText:@"网络错误" withImage:[UIImage imageNamed:@"icon_warning.png"]];
//    [TKUIUtil hiddenHUDAfterDelay:2];
//}
//
//@end
