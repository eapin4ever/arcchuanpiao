//
//  ZSTTabbarXMLParser.h
//  F3Engine
//
//  Created by luobin on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTShell : NSObject

SINGLETON_INTERFACE(ZSTShell)

/**
 *	@brief	从shell_config.xml中获取shell配置信息
 *
 *	@return 返回配置参数
 */

+ (NSDictionary *)initWithShell;

/**
 *	@brief	从shell.xml中获取Tabbar配置信息
 *
 *	@return
 */
+ (NSArray *)viewControllerForTabBarController;

//读取ShellG中的TabBar
+ (NSArray *)viewControllerForShellGTabBarController;

/**
 *	@brief	从shell.xml中获取Tabbar配置信息
 *
 *	@return
 */
+ (NSArray *)viewControllerForShellETabBarController;

/**
 *	@brief	从config.xml中获取Tabbar配置信息
 *
 *	@return
 */
+ (NSArray *)shellCViewControllerForTabBarController;

/**
 *	@brief	从config.xml中获取Tabbar配置信息
 *
 *	@return
 */

+ (NSArray *)shellFviewControllerForTabBarController;


/**
 *	@brief	从config.xml中获取Tabbar配置信息
 *
 *	@return
 */

+ (NSArray *)viewControllerForShellPTabBarController;

/**
 *	@brief	从config.xml中获取Tabbar配置信息
 *
 *	@return
 */

+ (NSArray *)viewControllerForShellSTabBarController;


/**
 *	@brief	重新初始化shellc首页
 *
 *	@return
 */

+(UIViewController *)reloadShellCFirstControllerModulID:(NSInteger)modulID interfaceUrl:(NSString *)interfaceUrl type:(NSInteger)type;

/**
 *	@brief	判断是否支持模块
 *
 *	@return 是否支持模块
 */
+ (BOOL)isModuleAvailable:(NSInteger)moduleID;

/**
 *	@brief	从shell_config.xml中获取模块儿设置项配置信息//暂时未实现
 *
 *	@return
 */
+ (NSMutableArray *)settingForModule;

/**
 *	@brief	设置通知得打开方式tabbar 或者imageButton
 *
 *	@return
 */
+(void)getPushOpentype:(NSString *)content;

@end
