//
//  ZSTLaucherItem.h
//  F3
//
//  Created by luobin on 11/10/11.
//  Copyright 2011 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ZSTLaucherItem : NSObject {
    NSString *_title;
    NSString *_icon;
    NSUInteger _badgeNumber;
    BOOL _enable;
    BOOL _canDelete;
    NSString *_url;
    NSString *_introduce;
}

+(id)itemWithTitle:(NSString *)title icon:(NSString *)icon url:(NSString *)url;

@property (nonatomic, retain) NSString *title;

@property (nonatomic, retain) NSString *introduce;

@property (nonatomic, retain) NSString *url;

@property (nonatomic, retain) NSString *icon;

@property (nonatomic, assign) NSUInteger badgeNumber;

@property (nonatomic, assign) BOOL enable;

@property (nonatomic, assign) BOOL canDelete;

- (void)setItemIntroduce:(NSString *)introduce;


@end
