//
//  ZSTGroupChatTableViewCell.m
//  YouYun
//
//  Created by luobin on 5/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTWeiBoChatTableViewCell.h"
#import "TKImageProcessorUtil.h"
#import "TKAsynImageView.h"
#import "ZSTWeiBoInfo.h"
#import "ZSTPictureViewer.h"
#import "ZSTUtils.h"
#import "MTZoomWindow.h"
#define kZSTMessageMaxHeight 600
#define kZSTParagraphSpace 5

#define isRetina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)


@implementation ZSTWeiBoChatTableViewCell

@synthesize avtarImageView;
@synthesize pictureImageView;
@synthesize contentLabel;
@synthesize dateLabel;
@synthesize pictureIcon;
@synthesize rowNumber;
@synthesize name;
@synthesize fowardCountLabel;
@synthesize commentCountLabel;
@synthesize platformLabel;
@synthesize isAttachment;
@synthesize attachmentContentLabel;
@synthesize attachmentPictureImageView;
@synthesize attachmentImageView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.isAttachment = NO;
        
        self.avtarImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(10, 10, 40, 40)] autorelease];
        CALayer * avtarImageViewLayer = [avtarImageView layer];
        [avtarImageViewLayer setMasksToBounds:YES];
        [avtarImageViewLayer setCornerRadius:5.0];
        self.avtarImageView.defaultImage = [UIImage imageNamed:@"avatar_default.png"];
        [self addSubview:avtarImageView];
    
        self.name = [[[UILabel alloc] init] autorelease];
        self.name.backgroundColor = [UIColor clearColor];
        self.name.font = [UIFont systemFontOfSize:12];
        self.name.textAlignment = UITextAlignmentLeft;
        [self addSubview:self.name];
        
        self.dateLabel = [[[UILabel alloc] init] autorelease];
        self.dateLabel.backgroundColor = [UIColor clearColor];
        self.dateLabel.textAlignment = UITextAlignmentLeft;
        self.dateLabel.font = [UIFont systemFontOfSize:10];
        self.dateLabel.textColor = [UIColor colorWithWhite:0.7 alpha:1];
        self.dateLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:1];
        self.dateLabel.shadowOffset = CGSizeMake(0, 1.0f);
        [self addSubview:self.dateLabel];
        
//        self.pictureIcon = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"timeline_image_icon.png"]] autorelease];
//        [self.dateLabel addSubview:pictureIcon];
        
        self.contentLabel = [[[UILabel alloc] init] autorelease];
        self.contentLabel.numberOfLines = 0;
        self.contentLabel.backgroundColor = [UIColor clearColor];
        self.contentLabel.font = [UIFont systemFontOfSize:13];
        self.contentLabel.textColor = [UIColor colorWithWhite:0.06 alpha:1];
        self.contentLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:1];
        self.contentLabel.shadowOffset = CGSizeMake(0, 1.0f);
        [self addSubview:self.contentLabel];
        
        self.pictureImageView = [[[TKAsynImageView alloc] init] autorelease];
        [self.pictureImageView addTarget:self action:@selector(showImage:) forControlEvents:UIControlEventTouchUpInside];
        self.pictureImageView.backgroundColor = [UIColor clearColor];
        self.pictureImageView.showLoadingWheel = YES;
        self.pictureImageView.defaultImage = [UIImage imageNamed:@"messages_photo_default_image@2x.png"];
        [self addSubview:self.pictureImageView];
        
//        self.platformLabel = [[[UILabel alloc] init] autorelease];
//        self.platformLabel.font = [UIFont systemFontOfSize:10];
//        self.platformLabel.textColor = [UIColor grayColor];
//        self.platformLabel.backgroundColor = [UIColor clearColor];
//        self.platformLabel.textAlignment = UITextAlignmentRight;
//        [self addSubview:self.platformLabel];
        
        self.fowardCountLabel = [[[UIImageView alloc] init] autorelease];
        self.fowardCountLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:self.fowardCountLabel];
        
        self.commentCountLabel = [[[UIImageView alloc] init] autorelease];
        self.commentCountLabel.backgroundColor = [UIColor clearColor];
        [self addSubview:self.commentCountLabel];
        
        self.attachmentImageView = [[[UIImageView alloc] init] autorelease];
        self.attachmentImageView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.attachmentImageView];
        
        self.attachmentContentLabel = [[[UILabel alloc] init] autorelease];
        self.attachmentContentLabel.numberOfLines = 0;
        self.attachmentContentLabel.backgroundColor = [UIColor clearColor];
        self.attachmentContentLabel.font = [UIFont systemFontOfSize:12];
        self.attachmentContentLabel.textColor = [UIColor colorWithWhite:0.06 alpha:1];
        self.attachmentContentLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:1];
        self.attachmentContentLabel.shadowOffset = CGSizeMake(0, 1.0f);
        [self addSubview:self.attachmentContentLabel];
        
        self.attachmentPictureImageView = [[[TKAsynImageView alloc] init] autorelease];
        [self.attachmentPictureImageView addTarget:self action:@selector(showImage:) forControlEvents:UIControlEventTouchUpInside];
        self.attachmentPictureImageView.backgroundColor = [UIColor clearColor];
        self.attachmentPictureImageView.defaultImage = [UIImage imageNamed:@"messages_photo_default_image@2x.png"];
        [self addSubview:self.attachmentPictureImageView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
}

- (UIImage *)createImage:(UIImage *)image withTitle:(NSString *)title size:(CGSize)targetSize
{
   CGSize CurrentTargetSize = CGSizeMake(targetSize.width + 11, targetSize.height+1);
    
    UIGraphicsBeginImageContext(CurrentTargetSize);
    CGContextRef context =  UIGraphicsGetCurrentContext();
    CGContextSetAllowsAntialiasing(context, YES);
    CGContextSetShouldAntialias(context, YES);
    
    [image drawInRect:CGRectMake(0, 0, 10, 11)];

    CGContextSetFillColorWithColor(context, [UIColor grayColor].CGColor);
    [title drawInRect:CGRectMake(11, 0, targetSize.width, 11) withFont:[UIFont systemFontOfSize:10]];
    
    if (isRetina) {
        CGContextSetInterpolationQuality(context, kCGInterpolationHigh);
    }

    UIImage* resultImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();

    return resultImage;
}


- (void)setWeiBoInfo:(ZSTWeiBoInfo *)weiBoInfo
{
    self.name.text = weiBoInfo.name;
    
    [self.avtarImageView clear];
    self.avtarImageView.url = [NSURL URLWithString:weiBoInfo.avatarString];
    [self.avtarImageView loadImage];
    
    self.dateLabel.text = [ZSTUtils convertToTime:[NSString stringWithFormat:@"%f",[[weiBoInfo.time dateByAddingTimeInterval:-8*60*60]timeIntervalSince1970]]];

    self.contentLabel.text = weiBoInfo.content;
    self.platformLabel.text = [NSString stringWithFormat:@"来自%@",weiBoInfo.Platform];
    
    CGSize fowardSize = [weiBoInfo.forwardCount sizeWithFont:[UIFont systemFontOfSize:10] constrainedToSize:CGSizeMake(80, 10) lineBreakMode:UILineBreakModeTailTruncation];
    CGSize commentSize = [weiBoInfo.commentCount sizeWithFont:[UIFont systemFontOfSize:10] constrainedToSize:CGSizeMake(80, 10) lineBreakMode:UILineBreakModeTailTruncation];
    
    if ([weiBoInfo.forwardCount isEqualToString:@"0"]) {
        self.fowardCountLabel.image = nil;
    }else{
         self.fowardCountLabel.image =[self createImage:[UIImage imageNamed:@"timeline_retweet_count_icon.png"] withTitle:weiBoInfo.forwardCount size:CGSizeMake(fowardSize.width , fowardSize.height)]; 
    }
    
    if ([weiBoInfo.commentCount isEqualToString:@"0"]) {
        self.commentCountLabel.image = nil;
    }else{
        self.commentCountLabel.image = [self createImage:[UIImage imageNamed:@"timeline_comment_count_icon.png"] withTitle:weiBoInfo.commentCount size:CGSizeMake(commentSize.width , commentSize.height)];  
    }
    
    self.pictureImageView.url = nil;
    self.attachmentContentLabel.text = nil;
    self.attachmentPictureImageView.url = nil;
    self.isAttachment = NO;
    
    if ([weiBoInfo.bisforward isEqualToString:@"True"] && [weiBoInfo.bisforward length] != 0) {
        
        self.attachmentContentLabel.text = weiBoInfo.boriginalcontent;
        [self.attachmentPictureImageView clear];
        if (weiBoInfo.thumbnail_pic != nil && [weiBoInfo.thumbnail_pic length] != 0) {
            self.attachmentPictureImageView.url = [NSURL URLWithString:weiBoInfo.thumbnail_pic];
            self.attachmentPictureImageView.bmiddle_pic_url = [NSURL URLWithString:weiBoInfo.bmiddle_pic];
            self.attachmentPictureImageView.original_pic_url = [NSURL URLWithString:weiBoInfo.original_pic];
            [self.attachmentPictureImageView loadImage];
        }

        self.isAttachment = YES;  
    }else{
        [self.pictureImageView clear];
        if (weiBoInfo.thumbnail_pic != nil && [weiBoInfo.thumbnail_pic length] != 0) {
            self.pictureImageView.url = [NSURL URLWithString:weiBoInfo.thumbnail_pic];
            self.pictureImageView.bmiddle_pic_url = [NSURL URLWithString:weiBoInfo.bmiddle_pic];
            self.pictureImageView.original_pic_url = [NSURL URLWithString:weiBoInfo.original_pic];
            [self.pictureImageView loadImage];
        }
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    float height = 0;
    
    self.name.frame = CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 10, 10, 150, 20);
    self.platformLabel.frame = CGRectMake(CGRectGetMaxX(self.name.frame) , 10, 100, 20);

//    if (self.pictureImageView.url != nil || self.isAttachment) {
//        CGSize dateSize = [self.dateLabel.text sizeWithFont:[UIFont systemFontOfSize:10] constrainedToSize:CGSizeMake(95, 20) lineBreakMode:UILineBreakModeTailTruncation];
//        self.pictureIcon.frame = CGRectMake(self.dateLabel.frame.size.width - dateSize.width - 20, 2.5, 15, 15);
//    }else{
//        self.pictureIcon.frame = CGRectZero;
//    }
    
    height = CGRectGetMaxY(self.name.frame);
    
    CGSize size = [self.contentLabel.text sizeWithFont:self.contentLabel.font constrainedToSize:CGSizeMake(245, CGFLOAT_MAX) lineBreakMode:UILineBreakModeCharacterWrap];
    if (![self.contentLabel.text isEqualToString:@""] && self.contentLabel.text != nil && [self.contentLabel.text length] != 0) {
        self.contentLabel.frame = CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 10, height + 2, 245, size.height);
        height = CGRectGetMaxY(self.contentLabel.frame);
    }else{
        self.contentLabel.frame = CGRectZero;
    }
    
    if (self.pictureImageView.url != nil ) 
    {
        self.pictureImageView.frame = CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 25,  height + kZSTParagraphSpace, 80, 90);
        height = CGRectGetMaxY(self.pictureImageView.frame);
    }else {
        self.pictureImageView.frame = CGRectZero;
    }  
    
    float attachmentTop = height;

    CGSize attachmentContentSize = [self.attachmentContentLabel.text sizeWithFont:self.attachmentContentLabel.font constrainedToSize:CGSizeMake(225, CGFLOAT_MAX) lineBreakMode:UILineBreakModeCharacterWrap];

    if (![self.attachmentContentLabel.text isEqualToString:@""] && self.attachmentContentLabel.text != nil && [self.attachmentContentLabel.text length] != 0) {
        self.attachmentContentLabel.frame = CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 20, height + 20, 225, attachmentContentSize.height);
        height = CGRectGetMaxY(self.attachmentContentLabel.frame);
    }else{
        self.attachmentContentLabel.frame = CGRectZero;
    }
    
    if (self.attachmentPictureImageView.url != nil ) 
    {
        self.attachmentPictureImageView.frame = CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 35,  height + kZSTParagraphSpace, 80, 90);
        height = CGRectGetMaxY(self.attachmentPictureImageView.frame);
    }else 
    {
        self.attachmentPictureImageView.frame = CGRectZero;
    } 
    
    if (self.isAttachment) {
        self.attachmentImageView.frame = CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 10, attachmentTop + 10, 245, height - attachmentTop);
        self.attachmentImageView.image = [[UIImage imageNamed:@"chat_top_arrow.png"] stretchableImageWithLeftCapWidth:18 topCapHeight:12]; 
    }else{
        self.attachmentImageView.frame  = CGRectZero;
    }
    
    self.dateLabel.frame =  CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 10, height + (self.isAttachment ? 20 : 10), 95, 10);

    if (self.commentCountLabel.image != nil) {
        self.commentCountLabel.frame = CGRectMake(320 - self.commentCountLabel.image.size.width - 10,height + (self.isAttachment ? 20 : 10), self.commentCountLabel.image.size.width, 10);
    }
    if (self.fowardCountLabel.image != nil) {
        self.fowardCountLabel.frame = CGRectMake(320 - self.commentCountLabel.image.size.width - self.fowardCountLabel.image.size.width - 15, height + (self.isAttachment ? 20 : 10), self.fowardCountLabel.image.size.width, 10);
    }

 
}

- (CGSize)displayRectForImage:(TKAsynImageView *)sender
{
    CGRect screenRect = TKScreenBounds();
    CGSize imageSize = sender.imageView.image.size;
    if (imageSize.width/ imageSize.height >= screenRect.size.width/ screenRect.size.height) {
        CGFloat height = screenRect.size.width * imageSize.height / imageSize.width;
        return CGSizeMake(screenRect.size.width, height);
    } else {
        CGFloat width = screenRect.size.height * imageSize.width / imageSize.height;
        return CGSizeMake(width, screenRect.size.height);
    }
}

- (void)showImage:(TKAsynImageView *)sender
{
    CGRect imageFrame = [sender.imageView convertRect:sender.bounds toView:self.window];//没有找到根本原因
    TKAsynImageView *pictureViewer;
    if (isRetina) {
        pictureViewer  = [[TKAsynImageView alloc] initWithFrame:CGRectMake((imageFrame.origin.x)*2, (imageFrame.origin.y)*2, imageFrame.size.width*2, imageFrame.size.height * 2)];
    }else{
        pictureViewer = [[TKAsynImageView alloc] initWithFrame:CGRectMake((imageFrame.origin.x), (imageFrame.origin.y), imageFrame.size.width, imageFrame.size.height )];
    }

    pictureViewer.tag = 11111;
    pictureViewer.defaultImage = sender.imageView.image;
    pictureViewer.imageView.zoomedSize = [self displayRectForImage:pictureViewer];
    pictureViewer.wrapInScrollviewWhenZoomed = YES;
    [pictureViewer clear];
    pictureViewer.url = [sender.bmiddle_pic_url retain];
    pictureViewer.showLoadingWheel = YES;
    [pictureViewer loadImage];
    [pictureViewer.imageView zoomIn];
}


- (void)dealloc
{
    self.avtarImageView = nil;
    self.pictureImageView = nil;
    self.contentLabel = nil;
    self.dateLabel = nil;
    self.name = nil;
    self.platformLabel = nil;
    self.fowardCountLabel = nil;
    self.commentCountLabel = nil;
    self.attachmentContentLabel = nil;
    self.attachmentPictureImageView = nil;
    self.attachmentImageView = nil;
    
    [super dealloc];
}

@end
