//
//  BusinessAppInfo.h
//  F3
//
//  Created by xuhuijun on 12-3-8.
//  Copyright 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ZSTBusinessAppInfo : NSObject 

@property(nonatomic, assign) NSInteger ID;
@property(nonatomic, copy) NSString *AppID;
@property(nonatomic, copy) NSString *Name;
@property(nonatomic, copy) NSString *ECECCID;
@property(nonatomic, copy) NSString *IconKey;
@property(nonatomic, copy) NSString *Introduce;
@property(nonatomic, copy) NSString *Version;
@property(nonatomic, copy) NSString *Status;
@property(nonatomic, assign) NSInteger appOrder;
@property(nonatomic, copy) NSString *Url;
@property(nonatomic, copy) NSString *Local;
@property(nonatomic, assign) NSInteger ModuleID;
@property(nonatomic, assign) NSInteger ModuleType;
@property(nonatomic, copy) NSString *InterfaceUrl;
@end
