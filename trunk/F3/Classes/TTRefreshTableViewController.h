//
//  TTRefreshTableViewController.h
//  TravelGuide
//
//  Created by Ma Jianglin on 12/27/12.
//  Copyright (c) 2012 Ma Jianglin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EGORefreshTableHeaderView.h"
#import "MoreButton.h"
#import "ZSTModuleBaseViewController.h"
#import "ColumnBar.h"

@interface TTRefreshTableViewController : ZSTModuleBaseViewController <UITableViewDataSource, UITableViewDelegate, EGORefreshTableHeaderDelegate,ColumnBarDelegate,ColumnBarDataSource>
{
	BOOL _loading;  //是否正在加载中
    
    int _pageIndex; //当前加载的页数，页面索引
    int _pageSize;  //每页数据条数
    
    BOOL _hasMore;  //是否有更多数据，是否长更多按钮
    
    EGORefreshTableHeaderView *_refreshHeaderView;  //下拽刷新的试图
    MoreButton *_moreButton;                        //更多按钮，需要变更加载状态
}

@property(nonatomic, retain) ColumnBar *topbar;   //顶部控制栏
@property(nonatomic, retain) NSMutableArray *categories;         //分类数组
@property(nonatomic, retain) UITableView *tableView;   //表视图
@property(nonatomic, retain) NSMutableArray *dataArray;         //数据数组

@property(nonatomic, retain) UITableViewCell *moreCell;         //更多按钮所在的行

- (void)aotuLoadData;
- (void)doneLoadingData;
- (void)reloadView;
@end;