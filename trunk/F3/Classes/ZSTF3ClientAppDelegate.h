//
//  F3ClientAppDelegate.h
//
//
//  Created by luobin on 2012-04-28.
//  Copyright 2012 ZhangShangTong Stock Co. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTHelpViewController.h"
#import "ZSTRegisterViewController.h"
#import "ZSTCoverAView.h"
#import "SinaWeibo.h"
#import "TCWBEngine.h"
#import "WXApi.h"
#import <TencentOpenAPI/TencentOAuth.h>
#import "ZSTLoginViewController.h"

@class ZSTRootViewController;
@class ZSTF3Engine;

@interface ZSTF3ClientAppDelegate : NSObject <UIApplicationDelegate, ZSTHelpViewControllerDelegate, RegisterDelegate,ZSTCoverAViewDelegate,WXApiDelegate,LoginDelegate> {
    NSString * _clientVersionUrl;
    BOOL isShowHelp;
    
    int status;
}

@property (nonatomic, retain) UIWindow *window;
@property (nonatomic, retain) UIViewController *rootController;
@property (nonatomic, retain) ZSTCoverAView *coverView;
@property (nonatomic, retain) ZSTF3Engine *f3Engine;
@property (retain, nonatomic) SinaWeibo *sinaWeiboEngine;
@property (retain, nonatomic) TCWBEngine *tWeiboEngine;
@property (nonatomic, retain) TencentOAuth *QQEngine;


-(BOOL)checkNetwork;

-(void)initErrorViewController;

-(void)registerByManualAnimated:(BOOL)animated;

-(void)showHelp;

-(void)startAllService;

-(void)applicationSetUp;

@end
