//
//  ScrollToolBarView.h
//  F3
//
//  Created by xuhuijun on 11-11-14.
//  Copyright 2011年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMRepairButton.h"

@protocol ZSTScrollToolBarViewDelegate <NSObject>

-(void)clickedPushOut;
-(void)clickedPushIn;

@end

@interface ZSTScrollToolBarView : UIView
{
    BOOL _isTakeBack;
    
    PMRepairButton *_pushButton;

    NSArray *_items;
    
    NSArray *_imageItems;
    
    CGFloat _alpha;
    
    id<ZSTScrollToolBarViewDelegate>_delegate;
    
    int intervalWidth;
    int intervalHeght;

}

@property (nonatomic)           BOOL isTakeBack;
@property (nonatomic,assign)    id<ZSTScrollToolBarViewDelegate>delegate;
@property (nonatomic,retain)    NSArray *items; //最好不要超过7个
@property (nonatomic,retain)    NSArray *imageItems;

- (void)stopClock;
-(id)initWithFrame:(CGRect)frame  color:(UIColor *)customColor  alpha:(CGFloat)alpha; 
-(void)reclock;//重新计时（5秒）
@end
