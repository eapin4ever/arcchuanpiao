//
//  PMRepairImageView.m
//  F3
//
//  Created by pmit on 15/5/8.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "PMRepairImageView.h"

@interface PMRepairImageView()
@property(strong,nonatomic)UIImageView *iv;
@end

@implementation PMRepairImageView

- (void)setImage:(UIImage *)image1
{
    if(!_iv)
    {
        _iv = [[[UIImageView alloc] initWithFrame:self.frame] autorelease];
    }
    
    _iv.image = image1;
    
    UIView *superView = self.superview;
    
    NSInteger index = [superView.subviews indexOfObject:self];
    [superView insertSubview:_iv atIndex:index];
    
    //这里如果是作为登录时的背景图的话，位置会有点问题。需要特殊处理
    if(CGRectGetWidth(_iv.bounds) == CGRectGetWidth(superView.bounds) && (CGRectGetHeight(_iv.bounds) == CGRectGetHeight(superView.bounds)))
    {
        [superView sendSubviewToBack:_iv];
    }
    
    [self removeFromSuperview];
}

- (void)setValue:(id)value forKeyPath:(NSString *)keyPath
{
    if([keyPath isEqualToString:@"image"])
    {
        [self setImage:value];
    }
}
@end
