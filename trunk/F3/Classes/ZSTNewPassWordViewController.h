//
//  ZSTNewPassWordViewController.h
//  F3
//
//  Created by LiZhenQu on 14-10-23.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTNewPassWordViewController : UIViewController<ZSTF3EngineDelegate>

@property (nonatomic, assign) int type;
@property (nonatomic, retain) IBOutlet UIView *naviView;
@property (nonatomic, retain) IBOutlet UIImageView *naviImgView;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;

@property (nonatomic, retain) IBOutlet UITextField *passwordField;
@property (nonatomic, retain) IBOutlet UITextField *passwordagainField;
@property (nonatomic, retain) IBOutlet UIButton *sumbitBtn;
@property (nonatomic, retain) IBOutlet UIButton *choicebtn;

@property (nonatomic, retain) IBOutlet UILabel *agreementLabel;

@property (nonatomic, retain) ZSTF3Engine *engine;

@property (nonatomic, retain) NSString *phoneNum;
@property (nonatomic, retain) NSString *verificationCode;

@end
