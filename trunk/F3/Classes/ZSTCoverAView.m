//
//  ZSTCoverAView.m
//  F3
//
//  Created by xuhuijun on 13-1-14.
//  Copyright (c) 2013年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTCoverAView.h"
#import "ZSTUtils.h"

#define imageBufer 5

@interface ZSTCoverAView ()

@property (nonatomic) BOOL animationInCurse;

- (void) _startInternetAnimations:(NSArray *)urls;
@end


@implementation ZSTCoverAView


@synthesize imagesArray,timeTransition, isLoop,imageUrls;
@synthesize animationInCurse, delegate;

- (void)dealloc
{
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.masksToBounds = YES;
    }
    return self;
}

- (void) animateWithURLs:(NSArray *)urls transitionDuration:(float)duration loop:(BOOL)shouldLoop
{
    self.imagesArray      = [[NSMutableArray alloc] init];
    self.imageUrls        = [NSMutableArray arrayWithArray:urls];
    self.timeTransition   = duration;
    self.isLoop           = shouldLoop;
    self.animationInCurse = NO;
        
    self.layer.masksToBounds = YES;
    
    [self performSelector:@selector(_startInternetAnimations:) withObject:urls];    
}

- (void) _startInternetAnimations:(NSArray *)urls
{
    int bufferSize = (imageBufer < urls.count) ? imageBufer : urls.count;

    for (int i = 0; i < bufferSize; i ++) {
        
        NSString *imagePath = [[ZSTUtils pathForCover] stringByAppendingPathComponent:[NSString stringWithFormat:@"cover_%d.png",index]];
        UIImage *image = nil;
        if (imagePath) {
             image = [UIImage imageWithContentsOfFile:imagePath];
        }
        
        TKAsynImageView *coverImageView = [[TKAsynImageView alloc] initWithFrame:self.frame];
        coverImageView.backgroundColor = [UIColor lightGrayColor];
        coverImageView.defaultImage = image;
        coverImageView.adjustsImageWhenHighlighted = NO;
        coverImageView.asynImageDelegate = self;
        coverImageView.callbackOnSetImage = self;
        [coverImageView clear];
        coverImageView.url = [NSURL URLWithString:[urls objectAtIndex:i]];
        [coverImageView loadImage];
        
        UISwipeGestureRecognizer *recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
        [recognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
        [coverImageView addGestureRecognizer:recognizer];
        [recognizer release];
        
        [self.imagesArray addObject:coverImageView];
        [coverImageView release];
    }
    
    [self performSelector:@selector(_animate:) withObject:[NSNumber numberWithInt:0]];
}

- (void) _animate:(NSNumber*)num
{
    [UIView animateWithDuration:self.timeTransition delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        
        TKAsynImageView *currentImage = [self.imagesArray objectAtIndex:[num intValue]];
        TKAsynImageView *perImage = [self.imagesArray objectAtIndex:(([num intValue]-1) == -1 ? ([self.imagesArray count]-1) : ([num intValue]-1))];
        [self addSubview:currentImage];
        [self bringSubviewToFront:perImage];

        [UIView animateWithDuration:self.timeTransition animations:^{
            perImage.alpha = 0.0;
            [self performSelector:@selector(_removeImage:) withObject:perImage afterDelay:self.timeTransition];
        }];
        
    } completion:^(BOOL finished) {
        
        if ([num intValue] != ([self.imagesArray count]-1)) {
            [self performSelector:@selector(_animate:) withObject:[NSNumber numberWithInt:([num intValue]+1)] afterDelay:self.timeTransition];
        }else if (self.isLoop){
            [self performSelector:@selector(_animate:)  withObject:[NSNumber numberWithInt:0] afterDelay:self.timeTransition];
        }else{
            [self removeFromSuperview];
        }
    }];

}

- (void)_removeImage:(TKAsynImageView *)asynImage
{
    [asynImage removeFromSuperview];
    asynImage.alpha = 1.0;
}

#pragma mark - HJManagedImageVDelegate

-(void) managedImageSet:(HJManagedImageV*)mi
{
    NSUInteger index = [self.imageUrls indexOfObject:[NSString stringWithFormat:@"%@", mi.url]];
    NSString *imagePath = [[ZSTUtils pathForCover] stringByAppendingPathComponent:[NSString stringWithFormat:@"cover_%d.png",index]];
    NSData* data = UIImagePNGRepresentation(mi.image);
    [data writeToFile:imagePath atomically:YES];
}

#pragma mark - TKAsynImageViewDelegate

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{
    if ([self.delegate respondsToSelector:@selector(coverAViewDidClicked:)]) {
        [self.delegate coverAViewDidClicked:self];
    }
    [self removeFromSuperview];
}

- (void)handleSwipeFrom:(UIGestureRecognizer *)gesture
{
    
    if ([self.delegate respondsToSelector:@selector(coverAViewDidSlided:)]) {
        [self.delegate coverAViewDidSlided:self];
    }
    [self removeFromSuperview];
}

@end
