//
//  ZSTTabbarXMLParser.m
//  F3Engine
//
//  Created by luobin on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTShell.h"
#import "ElementParser.h"
#import "TKTabBarItem.h"
#import "ZSTUtils.h"
#import "ZSTModuleManager.h"
#import "ZSTModuleDelegate.h"
#import "ZSTModuleBaseViewController.h"
#import "BaseNavgationController.h"
#import "ZSTGlobal+F3.h"
#import "ZSTF3Preferences.h"


@implementation ZSTShell

SINGLETON_IMPLEMENTATION(ZSTShell)

+ (NSDictionary *)initWithShell
{
    NSMutableDictionary *shellOptions = [NSMutableDictionary dictionary];
    
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"shell_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    Element *shellElement = [root selectElement:@"ShellID"];
    Element *coverElement = [root selectElement:@"CoverID"];
    Element *showHelpElement = [root selectElement:@"ShowHelp"];
    
    [ZSTF3Preferences shared].isShowHelp = [[[showHelpElement selectElement:@"ShowHelp"] contentsText] boolValue];

    NSInteger shellid = [[[shellElement selectElement:@"ShellID"] contentsNumber] intValue];
    NSAssert(shellid ,@"launching shell not be null.");
    
    [ZSTF3Preferences shared].shellId = shellid;
    
    NSInteger coverid = [[[coverElement selectElement:@"CoverID"] contentsNumber] intValue];

    UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:shellid withOptions:nil];
    [shellOptions setSafeObject:controller forKey:ShellRootViewController];
    
    
    if ([NSStringFromClass([controller class]) isEqualToString:@"ZSTShellDRootViewController"]) {
        BaseNavgationController *nav = [[BaseNavgationController alloc] initWithRootViewController:controller];
        [shellOptions setSafeObject:nav forKey:ShellRootViewController];
    }
    
    if ([NSStringFromClass([controller class]) isEqualToString:@"ZSTShellRRootViewController"]) {
        BaseNavgationController *nav = [[BaseNavgationController alloc] initWithRootViewController:controller];
        [shellOptions setSafeObject:nav forKey:ShellRootViewController];
    }
    

    if (coverid) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:[NSNumber numberWithInt:12] forKey:@"type"];
        UIView *cover = [[ZSTModuleManager shared] launchModuleView:coverid withOptions:params];
        [shellOptions setSafeObject:cover forKey:CoverView];
    }
    
    [parser release];
    return (NSDictionary *)shellOptions;
}

+(void)getPushOpentype:(NSString *)content
{
    ZSTF3Preferences *preference = [ZSTF3Preferences shared];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSArray *ImageButtonItemElements = [root selectElements:@"ImageButton Item"];
    for (NSInteger i = 0; i < [ImageButtonItemElements count]; i++) {
        Element *itemElement = [ImageButtonItemElements objectAtIndex:i];
        int moduleID = [[[itemElement selectElement:@"ModuleId"] contentsNumber] intValue];
        if (moduleID == 22 || moduleID == -1) {
            preference.pushOpentype = PushOpentype_ImageButton;
            int moduleType = [[[itemElement selectElement:@"ModuleType"] contentsNumber] intValue];
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setObject:[NSNumber numberWithInt:moduleType] forKey:@"type"];
            ZSTModuleBaseViewController *controller = (ZSTModuleBaseViewController*)[[ZSTModuleManager shared] launchModuleApplication:moduleID withOptions:params];
            preference.pushViewController = controller;
        }
    }
    
    NSArray *TabbarItemElements = [root selectElements:@"Tabbar Item"];
    for (NSInteger i = 0; i < [TabbarItemElements count]; i++) {
        Element *itemElement = [TabbarItemElements objectAtIndex:i];
        int moduleID = [[[itemElement selectElement:@"ModuleId"] contentsNumber] intValue];
        if (moduleID == 22 || moduleID == -1) {
            preference.pushOpentype = PushOpentype_TabBar;
            preference.pushSelectIndex = i;
        }
    }
}

+ (UIViewController *)viewControllerWithElement:(Element *)itemElement atIndex:(NSInteger)index
{
    NSString *tabbarName = [[itemElement selectElement:@"Title"] contentsText];
    NSString *tabTitleColor = [[itemElement selectElement:@"TitleColor"] contentsText];
    
    UIImage *tabbarIconNImage = [UIImage imageNamed:[NSString stringWithFormat:@"framework_bottom_tabbar%@_n.png", @(index + 1)]];
    UIImage *tabbarIconPImage = [UIImage imageNamed:[NSString stringWithFormat:@"framework_bottom_tabbar%@_p.png", @(index + 1)]];
    
    if (IS_IOS_7) {
        
        tabbarIconNImage = [tabbarIconNImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        tabbarIconPImage = [tabbarIconPImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
    int moduleID = [[[itemElement selectElement:@"ModuleId"] contentsNumber] intValue];
    int moduleType = [[[itemElement selectElement:@"ModuleType"] contentsNumber] intValue];
    
    if (moduleID != coverID) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:[[itemElement selectElement:@"InterfaceUrl"] contentsText] forKey:@"InterfaceUrl"];
        [params setObject:[NSNumber numberWithInt:moduleType] forKey:@"type"];
        
        UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:moduleID withOptions:params];
        controller.navigationItem.titleView = [ZSTUtils logoView];
        UINavigationController *navContrller = [[[UINavigationController alloc] initWithRootViewController: controller] autorelease];
        navContrller.navigationBar.backgroundImage = [UIImage imageNamed:@"framework_top_bg.png"];
        navContrller.tabBarItem = [[[TKTabBarItem alloc] initWithTitle:tabbarName
                                                       unselectedImage:tabbarIconNImage
                                                         selectedImage:tabbarIconPImage
                                                             textColor:tabTitleColor
                                                                   tag:1] autorelease];
//        navContrller.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 5, 5, 5);
        navContrller.navigationBar.tintColor = [ZSTUtils getNavigationTintColor];

        return navContrller;
    }
    return nil;
}

+ (NSArray *)viewControllerForTabBarController
{
    NSMutableArray *viewControllers = [NSMutableArray array];
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module_shellb_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    [self getPushOpentype:content];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSArray *itemElements = [root selectElements:@"Tabbar Item"];
    for (NSInteger i = 1; i < [itemElements count]; i++) {
        Element *itemElement = [itemElements objectAtIndex:i];
        UIViewController *controller = [self viewControllerWithElement:itemElement atIndex:i];
        if (controller != nil) {
            [viewControllers addObject:controller];
        }
    }

    NSString *tabbarName = [[[itemElements objectAtIndex:0] selectElement:@"Title"] contentsText];
    NSString *tabTitleColor = [[[itemElements objectAtIndex:0] selectElement:@"TitleColor"] contentsText];
    UIImage *tabbarIconNImage = [UIImage imageNamed:[NSString stringWithFormat:@"framework_bottom_tabbar1_n.png"]];
    UIImage *tabbarIconPImage = [UIImage imageNamed:[NSString stringWithFormat:@"framework_bottom_tabbar1_p.png"]];
    
    if (IS_IOS_7) {
        
        tabbarIconNImage = [tabbarIconNImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        tabbarIconPImage = [tabbarIconPImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
    int moduleID = shellbHomeID;
    int moduleType = shellbHomeID;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[NSNumber numberWithInt:moduleType] forKey:@"type"];
    
    UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:moduleID withOptions:params];
    controller.navigationItem.titleView = [ZSTUtils logoView];
    UINavigationController *navContrller = [[[UINavigationController alloc] initWithRootViewController: controller] autorelease];
    navContrller.navigationBar.backgroundImage = [UIImage imageNamed:@"framework_top_bg.png"];
    navContrller.tabBarItem = [[[TKTabBarItem alloc] initWithTitle:tabbarName
                                                   unselectedImage:tabbarIconNImage
                                                     selectedImage:tabbarIconPImage
                                                         textColor:tabTitleColor
                                                               tag:1] autorelease];
//    navContrller.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    navContrller.navigationBar.tintColor = [ZSTUtils getNavigationTintColor];
    
    [viewControllers insertObject:navContrller atIndex:0];

    [parser release];
    return viewControllers;
}
+ (NSArray *)viewControllerForShellGTabBarController
{
    
    NSMutableArray *viewControllers = [NSMutableArray array];
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module_shellg_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    [self getPushOpentype:content];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSArray *itemElements = [root selectElements:@"Tabbar Item"];
    for (NSInteger i = 1; i < [itemElements count]; i++) {
        Element *itemElement = [itemElements objectAtIndex:i];
        UIViewController *controller = [self viewControllerWithElement:itemElement atIndex:i];
        if (controller != nil) {
            [viewControllers addObject:controller];
        }
    }
    
    NSString *tabbarName = [[[itemElements objectAtIndex:0] selectElement:@"Title"] contentsText];
    NSString *tabTitleColor = [[[itemElements objectAtIndex:0] selectElement:@"TitleColor"] contentsText];
    UIImage *tabbarIconNImage = [UIImage imageNamed:[NSString stringWithFormat:@"framework_bottom_tabbar1_n.png"]];
    UIImage *tabbarIconPImage = [UIImage imageNamed:[NSString stringWithFormat:@"framework_bottom_tabbar1_p.png"]];
    
    if (IS_IOS_7) {
        
        tabbarIconNImage = [tabbarIconNImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        tabbarIconPImage = [tabbarIconPImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
    int moduleID = shellgHomeID;
    int moduleType = shellgHomeID;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[NSNumber numberWithInt:moduleType] forKey:@"type"];
    
    UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:moduleID withOptions:params];
    controller.navigationItem.titleView = [ZSTUtils logoView];
    UINavigationController *navContrller = [[[UINavigationController alloc] initWithRootViewController: controller] autorelease];
    navContrller.navigationBar.backgroundImage = [UIImage imageNamed:@"framework_top_bg.png"];
    navContrller.tabBarItem = [[[TKTabBarItem alloc] initWithTitle:tabbarName
                                                   unselectedImage:tabbarIconNImage
                                                     selectedImage:tabbarIconPImage
                                                         textColor:tabTitleColor
                                                               tag:1] autorelease];
//    navContrller.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    navContrller.navigationBar.tintColor = [ZSTUtils getNavigationTintColor];
    
    [viewControllers insertObject:navContrller atIndex:0];
    
    
    [parser release];
    return viewControllers;
}
+ (NSArray *)viewControllerForShellETabBarController
{
    NSMutableArray *viewControllers = [NSMutableArray array];
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module_shelle_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    [self getPushOpentype:content];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSArray *itemElements = [root selectElements:@"Tabbar Item"];
    for (NSInteger i = 1; i < [itemElements count]; i++) {
        Element *itemElement = [itemElements objectAtIndex:i];
        UIViewController *controller = [self viewControllerWithElement:itemElement atIndex:i];
        if (controller != nil) {
            [viewControllers addObject:controller];
        }
    }
    
    NSString *tabbarName = [[[itemElements objectAtIndex:0] selectElement:@"Title"] contentsText];
    NSString *tabTitleColor = [[[itemElements objectAtIndex:0] selectElement:@"TitleColor"] contentsText];
    UIImage *tabbarIconNImage = [UIImage imageNamed:[NSString stringWithFormat:@"framework_bottom_tabbar1_n.png"]];
    UIImage *tabbarIconPImage = [UIImage imageNamed:[NSString stringWithFormat:@"framework_bottom_tabbar1_p.png"]];
    
    if (IS_IOS_7) {
        
        tabbarIconNImage = [tabbarIconNImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        tabbarIconPImage = [tabbarIconPImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
    int moduleID = shelleHomeID;
    int moduleType = shelleHomeID;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[NSNumber numberWithInt:moduleType] forKey:@"type"];
    
    UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:moduleID withOptions:params];
    controller.navigationItem.titleView = [ZSTUtils logoView];
    UINavigationController *navContrller = [[[UINavigationController alloc] initWithRootViewController: controller] autorelease];
    navContrller.navigationBar.backgroundImage = [UIImage imageNamed:@"framework_top_bg.png"];
    navContrller.tabBarItem = [[[TKTabBarItem alloc] initWithTitle:tabbarName
                                                   unselectedImage:tabbarIconNImage
                                                     selectedImage:tabbarIconPImage
                                                         textColor:tabTitleColor
                                                               tag:1] autorelease];
//    navContrller.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    navContrller.navigationBar.tintColor = [ZSTUtils getNavigationTintColor];
    
    [viewControllers insertObject:navContrller atIndex:0];
    
    
    [parser release];
    return viewControllers;
}

+ (NSArray *)viewControllerForShellPTabBarController
{
    NSMutableArray *viewControllers = [NSMutableArray array];
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module_shellp_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    [self getPushOpentype:content];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSArray *itemElements = [root selectElements:@"Tabbar Item"];
    for (NSInteger i = 1; i < [itemElements count]; i++) {
        Element *itemElement = [itemElements objectAtIndex:i];
        UIViewController *controller = [self viewControllerWithElement:itemElement atIndex:i];
        if (controller != nil) {
            [viewControllers addObject:controller];
        }
    }
    
    NSString *tabbarName = [[[itemElements objectAtIndex:0] selectElement:@"Title"] contentsText];
    NSString *tabTitleColor = [[[itemElements objectAtIndex:0] selectElement:@"TitleColor"] contentsText];
    UIImage *tabbarIconNImage = [UIImage imageNamed:[NSString stringWithFormat:@"framework_bottom_tabbar1_n.png"]];
    UIImage *tabbarIconPImage = [UIImage imageNamed:[NSString stringWithFormat:@"framework_bottom_tabbar1_p.png"]];
    
    if (IS_IOS_7) {
        
        tabbarIconNImage = [tabbarIconNImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        tabbarIconPImage = [tabbarIconPImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
    int moduleID = shellpHomeID;
    int moduleType = shellpHomeID;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[NSNumber numberWithInt:moduleType] forKey:@"type"];
    
    UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:moduleID withOptions:params];
    controller.navigationItem.titleView = [ZSTUtils logoView];
    UINavigationController *navContrller = [[[UINavigationController alloc] initWithRootViewController: controller] autorelease];
    navContrller.navigationBar.backgroundImage = [UIImage imageNamed:@"framework_top_bg.png"];
    navContrller.tabBarItem = [[[TKTabBarItem alloc] initWithTitle:tabbarName
                                                   unselectedImage:tabbarIconNImage
                                                     selectedImage:tabbarIconPImage
                                                         textColor:tabTitleColor
                                                               tag:1] autorelease];
    //    navContrller.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    navContrller.navigationBar.tintColor = [ZSTUtils getNavigationTintColor];
    
    [viewControllers insertObject:navContrller atIndex:0];
    
    [parser release];
    return viewControllers;
}

+ (NSArray *)viewControllerForShellSTabBarController
{
    NSMutableArray *viewControllers = [NSMutableArray array];
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module_shells_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    [self getPushOpentype:content];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSString *tabbarTitle = @"首页";
    NSString *tabbartitleColor = @"#ffffff";
    
    NSArray *itemElements = [root selectElements:@"Tabbar Item"];
    for (NSInteger i = 0; i < [itemElements count]; i++) {
        
        if (i == 1) {
            
            tabbarTitle = [[[itemElements objectAtIndex:i] selectElement:@"Title"] contentsText];
            tabbartitleColor = [[[itemElements objectAtIndex:i] selectElement:@"TitleColor"] contentsText];
            
        } else {
        
            Element *itemElement = [itemElements objectAtIndex:i];
            UIViewController *controller = [self shellSViewControllerWithElement:itemElement atIndex:i];
            if (controller != nil) {
                [viewControllers addObject:controller];
            }
        }
    }
    
    int moduleID = shellsHomeID;
    int moduleType = shellsHomeID;
    
    NSString *tabbarIconNImage = @"Module.bundle/module_shells_tabbar_mainicon2_n.png";
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[NSNumber numberWithInt:moduleType] forKey:@"type"];
    
    ZSTModuleBaseViewController *vieController = (ZSTModuleBaseViewController*)[[ZSTModuleManager shared] launchModuleApplication:moduleID withOptions:params];
    vieController.navigationItem.titleView = [ZSTUtils logoView];
    UINavigationController *navContrller = [[[UINavigationController alloc] initWithRootViewController: vieController] autorelease];
    navContrller.navigationBar.backgroundImage = [UIImage imageNamed:@"Module.bundle/module_shells_top_bg.png"];
    
    vieController.iconImageName = tabbarIconNImage;
    vieController.tabTitle = tabbarTitle;
    vieController.titleColor = tabbartitleColor;
    navContrller.navigationBar.tintColor = [ZSTUtils getNavigationTintColor];
    
    [viewControllers insertObject:navContrller atIndex:1];
    return viewControllers;
}

+ (UIViewController *)shellSViewControllerWithElement:(Element *)itemElement atIndex:(NSInteger)index
{
    NSString *tabbarIconNImage = [NSString stringWithFormat:@"Module.bundle/module_shells_tabbar_icon%@_n.png", @(index+1)];
    
    int moduleID = [[[itemElement selectElement:@"ModuleId"] contentsNumber] intValue];
    int moduleType = [[[itemElement selectElement:@"ModuleType"] contentsNumber] intValue];
    NSString *title = [[itemElement selectElement:@"Title"] contentsText];
    NSString *titleColor = [[itemElement selectElement:@"TitleColor"] contentsText];
    
    
    if (moduleID != coverID) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:[NSNumber numberWithInt:moduleType] forKey:@"type"];
        [params setObject:[[itemElement selectElement:@"InterfaceUrl"] contentsText] forKey:@"InterfaceUrl"];
        
        ZSTModuleBaseViewController *controller = (ZSTModuleBaseViewController*)[[ZSTModuleManager shared] launchModuleApplication:moduleID withOptions:params];
        controller.navigationItem.titleView = [ZSTUtils logoView];
        UINavigationController *navContrller = [[[UINavigationController alloc] initWithRootViewController: controller] autorelease];
        navContrller.navigationBar.backgroundImage = [UIImage imageNamed:@"framework_top_bg.png"];
        
        controller.iconImageName = tabbarIconNImage;
        controller.tabTitle = title;
        controller.titleColor = titleColor;
//        navContrller.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 5, 5, 5);
        navContrller.navigationBar.tintColor = [ZSTUtils getNavigationTintColor];
        
        return navContrller;
    }
    return nil;
}

+ (NSArray *)shellFviewControllerForTabBarController
{
    NSMutableArray *viewControllers = [NSMutableArray array];
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module_shellf_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    [self getPushOpentype:content];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSArray *itemElements = [root selectElements:@"Tabbar Item"];
    for (NSInteger i = 1; i < [itemElements count]; i++) {
        Element *itemElement = [itemElements objectAtIndex:i];
        UIViewController *controller = [self viewControllerWithElement:itemElement atIndex:i];
        if (controller != nil) {
            [viewControllers addObject:controller];
        }
    }
    
    NSString *tabbarName = [[[itemElements objectAtIndex:0] selectElement:@"Title"] contentsText];
    NSString *tabTitleColor = [[[itemElements objectAtIndex:0] selectElement:@"TitleColor"] contentsText];
    UIImage *tabbarIconNImage = [UIImage imageNamed:[NSString stringWithFormat:@"framework_bottom_tabbar1_n.png"]];
    UIImage *tabbarIconPImage = [UIImage imageNamed:[NSString stringWithFormat:@"framework_bottom_tabbar1_p.png"]];
    
    if (IS_IOS_7) {
        
        tabbarIconNImage = [tabbarIconNImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        tabbarIconPImage = [tabbarIconPImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
    int moduleID = shellfHomeID;
    int moduleType = shellfHomeID;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[NSNumber numberWithInt:moduleType] forKey:@"type"];
    
    UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:moduleID withOptions:params];
    controller.navigationItem.titleView = [ZSTUtils logoView];
    UINavigationController *navContrller = [[[UINavigationController alloc] initWithRootViewController: controller] autorelease];
    navContrller.navigationBar.backgroundImage = [UIImage imageNamed:@"framework_top_bg.png"];
    navContrller.tabBarItem = [[[TKTabBarItem alloc] initWithTitle:tabbarName
                                                   unselectedImage:tabbarIconNImage
                                                     selectedImage:tabbarIconPImage
                                                         textColor:tabTitleColor
                                                               tag:1] autorelease];
//    navContrller.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    navContrller.navigationBar.tintColor = [ZSTUtils getNavigationTintColor];
    
    [viewControllers insertObject:navContrller atIndex:0];
    
    
    [parser release];
    return viewControllers;
}

+ (UIViewController *)shellCViewControllerWithElement:(Element *)itemElement atIndex:(NSInteger)index
{
    NSString *tabbarIconNImage = [NSString stringWithFormat:@"Module.bundle/module_shellc_tabbar_icon%@_n.png", @(index + 1)];
    
    int moduleID = [[[itemElement selectElement:@"ModuleId"] contentsNumber] intValue];
    int moduleType = [[[itemElement selectElement:@"ModuleType"] contentsNumber] intValue];
    NSString *title = [[itemElement selectElement:@"Title"] contentsText];
    NSString *titleColor = [[itemElement selectElement:@"TitleColor"] contentsText];
    
    NSLog(@"---- %@ ---",titleColor);

    if (moduleID != coverID) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:[NSNumber numberWithInt:moduleType] forKey:@"type"];
        [params setObject:[[itemElement selectElement:@"InterfaceUrl"] contentsText] forKey:@"InterfaceUrl"];

        ZSTModuleBaseViewController *controller = (ZSTModuleBaseViewController*)[[ZSTModuleManager shared] launchModuleApplication:moduleID withOptions:params];
        controller.navigationItem.titleView = [ZSTUtils logoView];
        UINavigationController *navContrller = [[[UINavigationController alloc] initWithRootViewController: controller] autorelease];
        navContrller.navigationBar.backgroundImage = [UIImage imageNamed:@"framework_top_bg.png"];
        
        controller.iconImageName = tabbarIconNImage;
        controller.tabTitle = title;
        controller.titleColor = titleColor;
//        navContrller.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 5, 5, 5);
        navContrller.navigationBar.tintColor = [ZSTUtils getNavigationTintColor];
        
        return navContrller;
    }
    return nil;
}

+ (NSArray *)shellCViewControllerForTabBarController
{
    NSMutableArray *viewControllers = [NSMutableArray array];
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"module_shellc_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    [self getPushOpentype:content];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSArray *itemElements = [root selectElements:@"Tabbar Item"];
    NSInteger index;
    for (NSInteger i = 0; i < [itemElements count]; i++) {
        Element *itemElement = [itemElements objectAtIndex:i];
        index = i;
        if (i >= 2) {
            index = i + 1;
        }
        UIViewController *controller = [self shellCViewControllerWithElement:itemElement atIndex:index];
        if (controller != nil) {
            [viewControllers addObject:controller];
        }
    }
    [parser release];
    
    int moduleID = shellcHomeID;
    int moduleType = shellcHomeID;
    NSString *tabbarIconNImage = @"Module.bundle/module_shellc_tabbar_mainicon3_n.png";
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[NSNumber numberWithInt:moduleType] forKey:@"type"];
    
    ZSTModuleBaseViewController *vieController = (ZSTModuleBaseViewController*)[[ZSTModuleManager shared] launchModuleApplication:moduleID withOptions:params];
    vieController.navigationItem.titleView = [ZSTUtils logoView];
    UINavigationController *navContrller = [[[UINavigationController alloc] initWithRootViewController: vieController] autorelease];
    navContrller.navigationBar.backgroundImage = [UIImage imageNamed:@"Module.bundle/module_shellc_top_bg.png"];
    
    vieController.iconImageName = tabbarIconNImage;
//    navContrller.tabBarItem.imageInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    navContrller.navigationBar.tintColor = [ZSTUtils getNavigationTintColor];
    
    [viewControllers insertObject:navContrller atIndex:2];
    return viewControllers;
}


+(UIViewController *)reloadShellCFirstControllerModulID:(NSInteger)modulID interfaceUrl:(NSString *)interfaceUrl type:(NSInteger)type
{
    NSMutableDictionary *moduleParams = [NSMutableDictionary dictionary];
    if (modulID != shellcHomeID || modulID != coverID) {
        if (modulID == 12 || modulID == 24) {
            [moduleParams setObject:interfaceUrl forKey:@"InterfaceUrl"];
        }
        [moduleParams setObject:[NSNumber numberWithInteger:type] forKey:@"type"];
    }
     
    ZSTModule *module = [[ZSTModuleManager shared] findModule:modulID];
    if (module) {
        ZSTModuleBaseViewController *controller = (ZSTModuleBaseViewController*)[[ZSTModuleManager shared] launchModuleApplication:modulID withOptions:moduleParams];
        if (controller) {
            return controller;
        }
       }
    return nil;
}

+ (BOOL)isModuleAvailable:(NSInteger)moduleID
{
    BOOL supportModule = NO;
    NSString *content = [NSString stringWithContentsOfFile:
                         [[NSBundle mainBundle] pathForResource:
                          [NSString stringWithFormat:@"shell_config"] ofType:@"xml"]
                                                  encoding:NSUTF8StringEncoding error:nil];

    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSArray *supportModuleElements = [root selectElements:@"ModuleList ModuleID"];
    for (NSInteger i = 0; i < [supportModuleElements count]; i++) {
        NSInteger supportModuleID = [[[supportModuleElements objectAtIndex:i] contentsNumber] integerValue];
        if (moduleID == supportModuleID) {
            supportModule = YES;
            break;
        }
    }
    [parser release];
    return supportModule;
}




+ (NSDictionary *)settingViewControllerWithElement:(Element *)itemElement
{
    if (![[[itemElement selectElement:@"SettingName"] contentsText] isEmptyOrWhitespace]) {
        NSMutableDictionary *settingDic = [NSMutableDictionary dictionary];
        int moduleID = [[[itemElement selectElement:@"ModuleId"] contentsNumber] intValue];
        int moduleType = [[[itemElement selectElement:@"ModuleType"] contentsNumber] intValue];
        
        [settingDic setSafeObject:[[itemElement selectElement:@"SettingName"] contentsText]  forKey:@"SettingName"];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:[NSNumber numberWithInt:moduleType] forKey:@"type"];
        
        UIViewController *controller =[[ZSTModuleManager shared] launchModuleSetting:moduleID withOptions:params];
        controller.navigationItem.titleView = [ZSTUtils logoView];
        [settingDic setSafeObject:controller forKey:@"ModuleSettingVC"];
        
        return settingDic;
    }
    return nil;
}



+ (NSMutableArray *)settingForModule//这里我希望返回view的name 和id 可以找到指向的setting 组成一个字典
{
    NSMutableArray *settings = [NSMutableArray array];
    
    NSString *content = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"shell_config" ofType:@"xml"] encoding:NSUTF8StringEncoding error:nil];
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:content];
    
    NSArray *itemElements = [root selectElements:@"ImageButton Item"];
    for (NSInteger i = 0; i < [itemElements count]; i++) {
        Element *itemElement = [itemElements objectAtIndex:i];
        NSDictionary *settingDic = [self settingViewControllerWithElement:itemElement];
        if (settingDic != nil && [settingDic count]) {
            [settings addObject:settingDic];
        }
    }
    [parser release];
    return settings;
    
}


@end
