//
//  ZSTPersonalHeaherCell.h
//  InfantCloud
//
//  Created by LiZhenQu on 14-7-18.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface ZSTPersonalHeaderCell : UITableViewCell

//@property (nonatomic, strong) TKAsynImageView *avatarImgView;
@property (nonatomic,strong) UIImageView *avatarImgView;
@property (nonatomic, strong) IBOutlet UIButton *avatarbtn;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;

@end
