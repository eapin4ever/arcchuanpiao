//
//  ZSTResponse.h
//  
//
//  Created by luobin on 4/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZSTLegacyResponse.h"

@class ASIHTTPRequest;

@interface ZSTResponse : ZSTLegacyResponse

@property (nonatomic, retain) ASIHTTPRequest *request;
@property (nonatomic, retain) id userInfo;
@property (nonatomic, retain) NSString *errorMsg;
@property (nonatomic, assign) int resultCode;
@property (nonatomic, retain) NSString *stringResponse;
@property (nonatomic, retain) id jsonResponse;

@end
