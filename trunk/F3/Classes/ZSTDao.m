//
//  ZSTModuleBaseDao.m
//  F3Engine
//
//  Created by luobin on 12-9-14.
//
//

#import "ZSTDao.h"
#import <objc/runtime.h>

@implementation ZSTDao
@synthesize moduleType;

+ (ZSTDao *)daoWithModuleType:(NSInteger)moduleType
{
    ZSTDao *dao = [[ZSTDao alloc] init];
    dao.moduleType = moduleType;
    return [dao autorelease];
}

- (NSString *)tableName:(NSString *)name
{
    return [NSString stringWithFormat:@"%@_%@", name, @(self.moduleType)];
}

- (void)createTableIfNotExist
{
    unsigned int outCount;
    Method *m = class_copyMethodList(self.class, &outCount);
    for (int i = 0; i < outCount; i++) {
        Method method = m[i];
        SEL sel = method_getName(method);
        NSString *methodName = [NSString stringWithUTF8String:sel_getName(sel)];
        if ([methodName hasPrefix:@"createTableIfNotExist"] && [methodName compare:@"createTableIfNotExist"] != NSOrderedSame) {
            [self performSelector:sel];
        }
    }
}

@end
