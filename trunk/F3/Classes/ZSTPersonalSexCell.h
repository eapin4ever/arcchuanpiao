//
//  ZSTPersonalSexCell.h
//  InfantCloud
//
//  Created by LiZhenQu on 14-7-18.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTCustomSwitch.h"
#import "PMCustomSwitch.h"

@class ZSTPersonalSexCell;

@protocol PersonalSexCellDelegate <NSObject>
@optional
- (void) cell:(ZSTPersonalSexCell *)cell sexSelection:(int)sex;
@end

@interface ZSTPersonalSexCell : UITableViewCell <PMCustomSwitchDelegate>

//@property (nonatomic, strong) ZSTCustomSwitch *sexSwitch;
@property (nonatomic,strong) PMCustomSwitch *sexSwitch;

@property (nonatomic, assign) id<PersonalSexCellDelegate>delegate;


@end
