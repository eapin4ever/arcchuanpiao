//
//  ZSTPictureShow.h
//  F3
//
//  Created by xuhuijun on 12-3-31.
//  Copyright 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ZSTCustomView;

@interface ZSTPictureViewer : UIView <UIGestureRecognizerDelegate,UIScrollViewDelegate>
{
    CGAffineTransform rotationTransform;
    UIImage *_image;
    UIImageView *_imageView;
    UIScrollView *_scrollView;
    ZSTCustomView *_backgroudView;
}

@property (nonatomic, retain)UIImage *image;

- (id)initWithWindow:(UIWindow *)window ;

- (id)initWithView:(UIView *)view;

+ (ZSTPictureViewer *)showPicture:(UIImage *)image addedTo:(UIView *)view animated:(BOOL)animated;

@end
