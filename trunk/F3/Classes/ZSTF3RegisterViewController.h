//
//  ZSTF3RegisterViewController.h
//  F3
//
//  Created by LiZhenQu on 14-10-23.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMRepairButton.h"

@interface ZSTF3RegisterViewController : UIViewController<UITextFieldDelegate,ZSTF3EngineDelegate>

@property (nonatomic, assign) int type;
@property (nonatomic, retain) NSString *phoneNum;
@property (nonatomic, assign) BOOL isfromSetting;

@property (nonatomic, retain) IBOutlet UIView *naviView;
@property (nonatomic, retain) IBOutlet UIImageView *naviImgView;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;

@property (nonatomic, retain) IBOutlet UITextField *phoneNumField;
@property (nonatomic, retain) IBOutlet UITextField *verificationCodeField;
//@property (nonatomic, retain) IBOutlet UIButton *verificationCodebtn;
@property (retain, nonatomic) IBOutlet PMRepairButton *verificationCodebtn;
@property (nonatomic, retain) IBOutlet UIButton *choicebtn;
@property (nonatomic, retain) IBOutlet UIButton *alertbtn;

@property (nonatomic, retain) IBOutlet UIView *titleView;
@property (nonatomic, retain) IBOutlet UITextView *titleText;
@property (nonatomic, retain) IBOutlet UILabel *agreementLabel;

@property (nonatomic, retain) ZSTF3Engine *engine;


@end
