//
//  PMRepairButton.m
//  F3
//
//  Created by pmit on 15/5/9.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "PMRepairButton.h"

@implementation PMRepairButton
{
    UIImageView *norIv;
    UIImageView *hiIv;
    UIImageView *seIv;
}

//由于某种不明原因，图片无法显示。
//但如果使用代码创建的对象则可以正常显示。
//所以这里使用代码创建一个对象代替imageView
- (void)setImage:(UIImage *)image forState:(UIControlState)state
{
    //首先执行super方法，保证imageView有创建
    [super setImage:image forState:state];
    
    if(state == UIControlStateNormal)
    {
        if(norIv == nil)
        {
            norIv = [[[UIImageView alloc] initWithFrame:self.imageView.frame] autorelease];
            [self insertSubview:norIv aboveSubview:self.imageView];
        }
        norIv.image = image;
    }
    else if(state == UIControlStateHighlighted)
    {
        if(hiIv == nil)
        {
            hiIv = [[[UIImageView alloc] initWithFrame:self.imageView.frame] autorelease];
            [self insertSubview:hiIv aboveSubview:self.imageView];
        }
        hiIv.image = image;
        hiIv.hidden = YES;
    }
    else if (state == UIControlStateSelected)
    {
        if (seIv == nil)
        {
            seIv = [[[UIImageView alloc] initWithFrame:self.imageView.frame] autorelease];
            [self insertSubview:seIv aboveSubview:self.imageView];
        }
        seIv.image = image;
        seIv.hidden = YES;
    }
    
}


//- (void)setBackgroundImage:(UIImage *)image forState:(UIControlState)state
//{
//    UIImageView *bgIv = [[[UIImageView alloc] initWithFrame:self.bounds] autorelease];
//    bgIv.image = image;
//    [self insertSubview:bgIv atIndex:0];
//}


//以下的是通过xib设置 key-value 来调用
- (void)setNorIm:(UIImage *)norIm
{
    [self setImage:norIm forState:UIControlStateNormal];
}

- (void)setHiIm:(UIImage *)hiIm
{
    [self setImage:hiIm forState:UIControlStateHighlighted];
}

- (void)setBgIm:(UIImage *)bgIm
{
    [self setBackgroundImage:bgIm forState:UIControlStateNormal];
}

- (void)setHighlighted:(BOOL)highlighted
{
    [super setHighlighted:highlighted];
    if(hiIv)
    {
        if(highlighted)
            hiIv.hidden = NO;
        else
            norIv.hidden = YES;
    }
}

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    if (seIv)
    {
        if (selected)
        {
            seIv.hidden = NO;
            norIv.hidden = YES;
        }
        else
        {
            seIv.hidden = YES;
            norIv.hidden = NO;
        }
    }
}

@end
