  //
//  ELButtonView.m
//  CustomButton
//
//  Created by Xie Wei on 11-6-29.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#define RADIANS(degrees) ((degrees * M_PI) / 180.0)
#import "ZSTLauncherButtonView.h"
#import "ZSTBadgeView.h"
#import "ZSTSqlManager.h"
#import "ZSTLongPressButton.h"
#import <QuartzCore/QuartzCore.h>
#import <SDWebImage/UIImageView+WebCache.h>

#define kIphoneBlacklabel 99
#define kIphoneWhiteLabel 98

#define isPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)

@implementation ZSTLauncherButtonView

@synthesize editing = _editing;
@synthesize delegate = _delegate;
@synthesize canDelete = _canDelete;
@synthesize isHorizontal = _isHorizontal;
@synthesize largeIconVertical = _largeIconVertical;
@synthesize midIconVertical = _midIconVertical;

-(UIImage *)getImageFromNewRect:(UIImage *)theImage
{
    CGRect rect = CGRectMake(10, 10, theImage.size.width - 20, theImage.size.height - 22);
    CGImageRef imageRef1 = theImage.CGImage;
    CGImageRef imageRef2 = CGImageCreateWithImageInRect(imageRef1,rect);
    UIImage*newImage = [UIImage imageWithCGImage:imageRef2];
    CGImageRelease(imageRef2);
    return newImage;
}

-(void) setFrame:(CGRect)frame
{
    [super setFrame: frame];
    if (!_countView)
    {
        self.backgroundColor = [UIColor clearColor];
        [self resignFirstResponder];
        _editing = NO;
        
//        _iconView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
        _iconView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 70, 70)];
//        _iconView.callbackOnSetImage = self;
        [_iconView resignFirstResponder];
        _iconView.backgroundColor = [UIColor clearColor];
        [self addSubview:_iconView];

        _longPressBtn = [ZSTLongPressButton  buttonWithType:UIButtonTypeCustom];
        _longPressBtn.frame = CGRectMake(0, 0, 70, 70);
        [_longPressBtn setBackgroundImage:[UIImage imageNamed:@"clearButtonImage.png"] forState:UIControlStateHighlighted];
        [_longPressBtn addTarget:self action:@selector(buttonLongPressAction:) forControlEvents:UIControlEventLongPress];
        [_longPressBtn addTarget:self action:@selector(buttonClickAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview: _longPressBtn];
        
//        _countView = [[ZSTBadgeView alloc] initWithImage: [UIImage imageNamed: @"number_bg.png"]];
//        _countView.backgroundColor = [UIColor clearColor];
//        _countView.center = CGPointMake(CGRectGetMaxX(_longPressBtn.frame), _longPressBtn.frame.origin.y+8);
//        [self setBadgeNumber:0];
//        [self addSubview:_countView];
        
        _badgeNumIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"number_bg.png"]];
        _badgeNumIV.backgroundColor = [UIColor clearColor];
        _badgeNumIV.center = CGPointMake(CGRectGetMaxX(_longPressBtn.frame), _longPressBtn.frame.origin.y+8);
        [self addSubview:_badgeNumIV];
        
        _badgeLB = [[UILabel alloc] initWithFrame:CGRectMake(1, 0, 20, 20)];
        _badgeLB.textAlignment = NSTextAlignmentCenter;
        _badgeLB.textColor = [UIColor whiteColor];
        _badgeLB.font = [UIFont boldSystemFontOfSize:15];
        _badgeLB.backgroundColor = [UIColor clearColor];
        [self setBadgeNumber:0];
        [_badgeNumIV addSubview:_badgeLB];
        
        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height-30, frame.size.width, 20)];
        _nameLabel.backgroundColor = [UIColor clearColor];
        _nameLabel.textAlignment = NSTextAlignmentCenter;
        _nameLabel.font = [UIFont systemFontOfSize:12];
        [self addSubview:_nameLabel];
        
        _deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_deleteButton addTarget:self action:@selector(deleteButtonClickAction:) forControlEvents:UIControlEventTouchUpInside];
        _deleteButton.alpha = 0.0;
        _deleteButton.frame = CGRectMake(0, 0, 24*1.2, 24*1.2);
        _deleteButton.center = CGPointMake(CGRectGetMinX(_longPressBtn.frame), CGRectGetMinY(_longPressBtn.frame));
        [_deleteButton setBackgroundImage:[UIImage imageNamed: @"badge-minus.png"] forState:UIControlStateNormal];
        [self addSubview:_deleteButton];
        
        _enableButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _enableButton.alpha = 0.0;
        _enableButton.frame = CGRectMake(0, 0, 24, 24);
        _enableButton.center = CGPointMake(CGRectGetMaxX(_longPressBtn.frame), CGRectGetMaxY(_longPressBtn.frame)-2);
        [_enableButton setBackgroundImage:[UIImage imageNamed:@"badge-cancel.png"]  forState:UIControlStateNormal];
        _enableButton.userInteractionEnabled = NO;
        [self addSubview:_enableButton];
        
      }
}

- (void)buttonLongPressAction:(id)sender
{
    if (_isHorizontal) {
        if ([_delegate respondsToSelector:@selector(launcherButtonDidLongPress:)]) {
            [_delegate launcherButtonDidLongPress:self];
        }
        if ([_delegate respondsToSelector:@selector(launcherButtonDidClickDeleteButton:)]) {
            [_delegate launcherButtonDidClickDeleteButton:self];
        }      
    }
}

- (void)buttonClickAction:(id)sender
{
    if ([_delegate respondsToSelector:@selector(launcherButtonDidClick:)]) {
        [_delegate launcherButtonDidClick:self];
    }
}

- (void)deleteButtonClickAction:(id)sender
{
    if ([_delegate respondsToSelector:@selector(launcherButtonDidClickDeleteButton:)]) {
        [_delegate launcherButtonDidClickDeleteButton:self];
    }
}

//- (void)doDisapear
//{
//    _shouldDisapear = NO;
//    self.transform = CGAffineTransformMakeScale(1, 1);
//    [UIView beginAnimations:@"iconScalingDown" context:nil];
//    [UIView setAnimationBeginsFromCurrentState:NO];
//    [UIView setAnimationDuration:0.5];
//    [UIView setAnimationDelegate:self];
//    [UIView setAnimationDidStopSelector:@selector(viewDisappear:finished:context:)];
//    self.transform = CGAffineTransformMakeScale(0.01, 0.01);
//    [UIView commitAnimations];//缩小
//}


- (void)beginEditingAnimated:(BOOL)animated
{
    if (!_editing) {
        _editing = YES;
        if (_canDelete) {
            if (animated) {
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:0.5];
            }
            _deleteButton.alpha = 1.0;
            if (animated) {
                [UIView commitAnimations];
            }
        }
        if ([_delegate respondsToSelector:@selector(launcherButtonDidBeginEditing:)]) {
            [_delegate launcherButtonDidBeginEditing:self];
        }
    }
}
-(void) endEditingAnimated:(BOOL)animated
{   
    _editing = NO;
    self.transform = CGAffineTransformIdentity;
    
    if (animated) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
    }
    _deleteButton.alpha = 0;
    if (animated) {
        [UIView commitAnimations];
    }
    
    if ([_delegate respondsToSelector:@selector(launcherButtonDidEndEditing:)]) {
        [_delegate launcherButtonDidEndEditing:self];
    }
}

- (void)reloadView
{
    if (!_isHorizontal) {//iphone 竖排
        _longPressBtn.frame = CGRectMake(0, 0, 120, 120);
        //        _iconView.frame = CGRectMake(1, 0, 120-2, 120-2);
        _badgeNumIV.frame = CGRectMake(1, 0, 120-2, 120-2);
        _badgeLB.frame = _badgeNumIV.frame;
        _nameLabel.frame = CGRectMake(1, self.frame.size.height-22, self.frame.size.width-2, 20);
        _nameLabel.hidden = YES;
        
    }else if (_isHorizontal)//iphone 横排
    {
        _longPressBtn.frame = CGRectMake(0, 0, 80, 80);
        _iconView.frame = CGRectMake(4, 3, 80-8, 80-8);
        _countView.center = CGPointMake(CGRectGetMaxX(_longPressBtn.frame), _longPressBtn.frame.origin.y+4);
        _nameLabel.frame = CGRectMake(0, self.frame.size.height, self.frame.size.width, 25);
        _nameLabel.textColor = [UIColor blackColor];
        
    }
    
    _nameLabel.alpha = 1;
    
}

-(void)setIcon:(NSString *)imageUrl
{
    [self reloadView];
    if ([imageUrl isEqualToString:PERSONAL_IMAGE]) {
        _iconView.image = [UIImage imageNamed:@"personal.png"];
    }else {
//        [_iconView clear];
//        _iconView.url = [NSURL URLWithString:imageUrl];
//        [_iconView loadImage];
        [_iconView setImageWithURL:[NSURL URLWithString:imageUrl] placeholderImage:[UIImage imageNamed:@"personal.png"]];
    }
}

- (void)setMessageIntroduce:(NSString *)introduce
{
//    if (!_isHorizontal) {
//        UILabel *introduceLabel = [[UILabel alloc] initWithFrame:CGRectMake(1, self.frame.size.height-22, self.frame.size.width-2, 20)];
//        introduceLabel.backgroundColor = [UIColor blackColor];
//        introduceLabel.alpha = 0.5;
//        introduceLabel.font = [UIFont boldSystemFontOfSize:11];
//        introduceLabel.textColor = [UIColor whiteColor];
//        [self insertSubview:introduceLabel belowSubview:_nameLabel];
//    }    
    if (_isHorizontal) {
        _nameLabel.text = introduce;

    }
}

-(void)setBadgeNumber:(NSInteger)count
{
    if (0 == count)
    {
        _badgeNumIV.hidden = YES;
        _badgeLB.text = @"0";
    }
    else if (count >= 10)
    {
        _badgeNumIV.hidden = NO;
        _badgeLB.text = @"••••";
    }
    else
    {
        _badgeNumIV.hidden = NO;
        _badgeLB.text = @(count).stringValue;
    }
    //    if (0 == count)
    //    {
    //        _badgeNumIV.hidden = YES;
    //        _badgeLB.text = @"0";
    //    }
    //    else
    //    {
    //        _badgeNumIV.hidden = NO;
    //        _badgeLB.text = @(count).stringValue;
    //    }
}

-(void)setEnable:(BOOL)enable
{
    [self setEnable:enable animated:YES];
}

-(void)setEnable:(BOOL)enable animated:(BOOL)animated
{
    if (animated) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
    }
    _enableButton.alpha = enable? 0.0 : 1.0;
    if (animated) {
        [UIView commitAnimations];
    }
}

- (BOOL)isEnable
{
    return _enableButton.alpha == 0;
}

#pragma mark HJManagedImageVDelegate

-(void) managedImageSet:(HJManagedImageV*)mi
{
    if (mi.image != nil && _isHorizontal && mi.image.size.width != 0) {
        _iconView.image = [self getImageFromNewRect:mi.image];
    }else if(mi.image != nil && !_isHorizontal){
        self.adorn = nil;
    }
}


- (void)dealloc {
    [_iconView release];
    [_countView release];
    [_nameLabel release];
    [super dealloc];
}
@end
