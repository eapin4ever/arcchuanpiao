//
//  ContactInfo.h
//  HelloWorld
//
//  Created by huqinghe on 11-6-5.
//  Copyright 2011 Shinetechchina.com. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ContactInfo : NSObject {
    int _recordID;
	NSString *_name;
	NSString *_phoneNum;
    
	
}

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *phoneNum;
@property (nonatomic, assign) int recordID;

- (BOOL)isEqual:(id)object;
@end
